// change image file relationship
MATCH (img:E38)-[r:P106]->(file:D9)
MERGE (img)<-[:P67]-(file)
DELETE r
RETURN img, file;

// change digital representation of objects/buildings
MATCH (e22:E22)<-[r1:P67]-(dobj:D1)-[r2:P106]->(file:D9)
MERGE (e22)<-[:P67]-(file)-[:P106]->(dobj)
DELETE r1, r2
RETURN e22, file, dobj;

// change digital representation of terrains
MATCH (place:E53)<-[r1:P67]-(dobj:D1)-[r2:P106]->(file:D9)
MERGE (place)<-[:P67]-(file)
DELETE r1, r2, dobj
RETURN place, file;

// change map file relationship
MATCH (place:E53)<-[:P161]-(:E92)<-[:P67]-(img:E38)-[r:P106]->(file:D9)
MERGE (img)<-[:P67]-(file)
DELETE r
RETURN place, img, file;
