// change wikidata-aat relationship
MATCH (wiki:Wikidata)-[r:P67]->(aat:AAT)
MERGE (wiki)-[:refers_to_aat]->(aat)
DELETE r
RETURN wiki, aat;
