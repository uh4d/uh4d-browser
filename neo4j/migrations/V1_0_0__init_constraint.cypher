// create constraint
CREATE CONSTRAINT uh4d_unique_id IF NOT EXISTS FOR (n:UH4D) REQUIRE n.id IS UNIQUE;
