// change needsValidation to pending
MATCH (n)
WHERE exists(n.needsValidation)
SET n.pending = n.needsValidation
REMOVE n.needsValidation;
