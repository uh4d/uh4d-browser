# <img alt="Logo" src="apps/frontend/src/assets/kirche_ws2.png" width="50"/> UH4D Browser

This is the code base of the 4D Browser of the _UrbanHistory4D_ project.

Live demo: https://4dbrowser.urbanhistory4d.org

More information about the project: www.urbanhistory4d.org

## Getting started

### Docker Compose

- First, download and install [Docker](https://docs.docker.com/engine/install/) and [Docker Compose](https://docs.docker.com/compose/install/) (supporting compose files of at least version 3.4).
- Make sure the docker daemon runs in the background (if the command `docker ps` runs without errors, you should be fine)
- the docker-compose.yml uses profiles, to build and run them use
  ```bash
    docker-compose --profile <profile-name> up --build
  ```
- at this point, the profiles "production" and "development" are supported
- the "production" profile uses nginx to host the frontend and has static containers and apps.
- the "development" profile uses nx to serve the frontend, and it's application are volatile and can be rebuilt on the fly

In general, development also works without Docker. In this case, you need to host Neo4j database and compile binaries (for 3D model upload) manually.

Continue with either [Development](#development) or [Deployment](#deployment).

## Development

Ensure that [Node.js](https://nodejs.org/en/) and NPM is installed.

Install all dependencies:

```bash
$ npm install
```

Copy `.env-dist` to `.env` and set environment variables accordingly.
Most of them don't need to be changed.
But the paths to the data folders must be set (`NEO4J_DIR`, `DIR_DATA`), preferably outside the repository with absolute paths.

Set `ENABLE_IMPORT=true` in your `.env` to prepare the sample data with the following command to not start with an empty database.
When running the docker containers, it will download and extract the files and the database dump.

It can also be manually triggered by:

```bash
$ docker compose up import-downloader-development
```

You have 2 options to run the development setup:

1. Serve apps locally

```bash
# run Neo4j server
$ docker compose up neo4j-development

# run apps
$ npm run serve
```

2. Run full containerized setup

```bash
# run containers (alias to `docker compose up`)
$ npm run start:docker
```

Now, you should be able to access:

- Frontend: http://localhost:4200
- Backend: http://localhost:3001/api

## Deployment

Clone the repository:

```bash
$ git clone --branch master https://gitlab.com/uh4d/uh4d-browser.git
```

_TODO: Copy data (on first setup)_

Copy `.env-dist` to `.env` and set environment variables accordingly.

Run following commands to build and serve the application:

```bash
# build/start containers
$ docker compose --profile production up --build -d

# stop containers
$ docker compose --profile production stop
```

If you have [Node.js](https://nodejs.org/en/) and NPM installed on your server, the above commands can be shortened:

```bash
# build/start containers
$ npm run prod:deploy

# stop containers
$ npm run prod:stop
```

## NX monorepo

<a alt="Nx logo" href="https://nx.dev" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/nrwl/nx/master/images/nx-logo.png" width="45"></a>

✨ **This workspace has been generated by [Nx, a Smart, fast and extensible build system.](https://nx.dev)** ✨

### Understand this workspace

Run `nx graph` to see a diagram of the dependencies of the projects.

### Remote caching

Run `npx nx connect-to-nx-cloud` to enable [remote caching](https://nx.app) and make CI faster.

### Further help

Visit the [Nx Documentation](https://nx.dev) to learn more.
