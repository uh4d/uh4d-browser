#!/usr/bin/env bash

if [[ ! -f "/backups/IMPORT_NOT_NEEDED" ]] && [[ "${ENABLE_IMPORT}" == "true" ]]; then
  if [[ ! -z "${IMPORT_ARCHIVE_FILE}" ]] && [[ ! -z "${IMPORT_URL}" ]] && [[ ! -z "${IMPORT_DUMP_FILE}" ]]; then
    if [[ -z "$(find /data -type f -exec file --mime-type {} \+ | grep \".*images/.*\" | wc -l)" ]]; then
      if [[ ! -f "/data/${IMPORT_ARCHIVE_FILE}" ]]; then
        wget -O /data/"${IMPORT_ARCHIVE_FILE}" "${IMPORT_URL}${IMPORT_ARCHIVE_FILE}"
      fi
      7z x -o/data /data/"${IMPORT_ARCHIVE_FILE}" -aoa
      rm /data/"${IMPORT_ARCHIVE_FILE}"
    fi
    if [[ ! -f "/backups/${IMPORT_DUMP_FILE}" ]]; then
      wget -O /backups/"${IMPORT_DUMP_FILE}" "${IMPORT_URL}${IMPORT_DUMP_FILE}"
    fi
  else
    error "Empty IMPORT_ARCHIVE_FILE, IMPORT_DUMP_FILE or IMPORT_URL provided"
  fi
fi
