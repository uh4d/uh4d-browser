#!/usr/bin/env bash

if [[ ! -f "/backups/IMPORT_NOT_NEEDED" ]] && [[ "${ENABLE_IMPORT}" == "true" ]]; then
  neo4j-admin load --database="${NEO4J_DATABASE}" --from=/backups/"${IMPORT_DUMP_FILE}" --force
  rm  /backups/"${IMPORT_DUMP_FILE}"
  touch /backups/IMPORT_NOT_NEEDED
fi

source /startup/docker-entrypoint.sh neo4j
