/**
 * Remove all illegal/problematic characters from filename.
 * If `includeExtension` is `true`, last `.` and following characters will also be affected.
 */
export function removeIllegalFileChars(
  fileName: string,
  includeExtension = false,
): string {
  if (!includeExtension) {
    const index = fileName.lastIndexOf('.');
    if (index !== -1) {
      return replaceChars(fileName.slice(0, index)) + fileName.slice(index);
    }
  }
  return replaceChars(fileName);
}

function replaceChars(str: string): string {
  return str.replace(/\s+/g, '_').replace(/[^a-z0-9\-_]/gi, '');
}
