import { removeIllegalFileChars } from './remove-illegal-file-chars';

describe('remove illegal file chars', () => {
  it('should remove illegal file characters', () => {
    expect(removeIllegalFileChars('Te stä§.123')).toBe('Te_st.123');
    expect(removeIllegalFileChars('Ae4n-0123 jn123 üo #&/+12 /')).toBe(
      'Ae4n-0123_jn123_o_12_',
    );
    expect(removeIllegalFileChars('Meißen 20.12.jpg')).toBe('Meien_2012.jpg');
  });

  it('should remove illegal file characters from complete string', () => {
    expect(removeIllegalFileChars('Te stä§.123', true)).toBe('Te_st123');
    expect(removeIllegalFileChars('Meißen 20.12.jpg', true)).toBe(
      'Meien_2012jpg',
    );
  });
});
