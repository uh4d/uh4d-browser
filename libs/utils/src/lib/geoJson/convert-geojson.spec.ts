import {
  convertGeoFeatureToLocation,
  convertLocationToGeoFeature,
  PointFeature,
} from './convert-geojson';

describe('convert-geojson', () => {
  const feature = {
    type: 'Feature',
    properties: {
      radius: 403.01567382285253,
    },
    geometry: {
      type: 'Point',
      coordinates: [13.732653, 51.051704],
    },
  };
  const location = {
    latitude: 51.051704,
    longitude: 13.732653,
    radius: 403.01567382285253,
  };

  it('should convert location to geo feature', () => {
    expect(convertLocationToGeoFeature(location)).toEqual(feature);
  });

  it('should convert geo feature to location', () => {
    expect(convertGeoFeatureToLocation(feature as PointFeature)).toEqual(
      location,
    );
  });
});
