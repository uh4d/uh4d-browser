import { Feature, Point } from 'geojson';

type CircleLocation = { latitude: number; longitude: number; radius: number };
export type PointFeature = Feature<Point, { radius?: number }>;

export function convertLocationToGeoFeature(
  location: CircleLocation,
): PointFeature {
  return {
    type: 'Feature',
    properties: {
      radius: location.radius ?? 0,
    },
    geometry: {
      type: 'Point',
      coordinates: [location.longitude, location.latitude],
    },
  };
}

export function convertGeoFeatureToLocation(
  value: PointFeature,
): CircleLocation {
  const [longitude, latitude] = value.geometry.coordinates;
  return {
    latitude,
    longitude,
    radius: value.properties.radius ?? 0,
  };
}
