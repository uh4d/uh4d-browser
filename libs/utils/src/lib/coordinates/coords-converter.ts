import { Euler, Matrix4, Quaternion, Vector3 } from 'three';
import * as utm from 'utm';
import { LocationDto } from '@uh4d/dto/interfaces';
import { LocationOrientationDto } from '@uh4d/dto/interfaces/custom/spatial';
import { Coordinates } from './coordinates';

/**
 * Service used to convert coordinates/positions from one system to another (local Cartesian, WGS-84, UTM).
 */
export class CoordinatesConverter {
  /**
   * Origin of local coordinate system in UTM.
   */
  private origin = new Vector3();
  private latitude = 0;
  private longitude = 0;
  private zoneNum = 0;
  private zoneLetter = '';

  /**
   * Set origin of local coordinate system.
   */
  setOrigin(latitude: number, longitude: number, altitude: number): void {
    const { easting, northing, zoneNum, zoneLetter } = utm.fromLatLon(
      latitude,
      longitude,
    );

    this.origin = new Vector3(easting, altitude, northing);
    this.latitude = latitude;
    this.longitude = longitude;
    this.zoneNum = zoneNum;
    this.zoneLetter = zoneLetter;
  }

  /**
   * Get origin in global WGS-84 coordinates.
   */
  getOrigin(): LocationDto {
    return {
      latitude: this.latitude,
      longitude: this.longitude,
      altitude: this.origin.y,
    };
  }

  /**
   * Convert local Cartesian position to global WGS-84 coordinates.
   */
  toLatLon(position: Vector3): LocationDto;
  /**
   * Convert local transformation matrix to global WGS-84 coordinates and angles.
   */
  toLatLon(matrix: Matrix4): LocationOrientationDto;
  toLatLon(
    vectorOrMatrix: Vector3 | Matrix4,
  ): LocationDto | LocationOrientationDto {
    if (vectorOrMatrix instanceof Vector3) {
      const utmPosition = vectorOrMatrix
        .clone()
        .multiply(new Vector3(1, 1, -1))
        .add(this.origin);

      const { latitude, longitude } = utm.toLatLon(
        utmPosition.x,
        utmPosition.z,
        this.zoneNum,
        this.zoneLetter,
      );

      return { latitude, longitude, altitude: utmPosition.y };
    } else {
      const utmPosition = new Vector3();
      const q = new Quaternion();
      vectorOrMatrix.decompose(utmPosition, q, new Vector3());
      utmPosition.multiply(new Vector3(1, 1, -1)).add(this.origin);
      const rotation = new Euler().setFromQuaternion(q, 'YXZ');

      const { latitude, longitude } = utm.toLatLon(
        utmPosition.x,
        utmPosition.z,
        this.zoneNum,
        this.zoneLetter,
      );

      return {
        latitude,
        longitude,
        altitude: utmPosition.y,
        omega: rotation.x,
        phi: rotation.y,
        kappa: rotation.z,
      };
    }
  }

  /**
   * Convert global WGS-84 coordinates to local Cartesian position.
   */
  fromLatLon(location: LocationDto): Vector3;
  /**
   * Convert global WGS-84 coordinates to local Cartesian position.
   */
  fromLatLon(latitude: number, longitude: number, altitude: number): Vector3;
  fromLatLon(
    locationOrLatitude: LocationDto | number,
    longitude?: number,
    altitude?: number,
  ): Vector3 {
    const location: LocationDto =
      typeof locationOrLatitude === 'number'
        ? {
            latitude: locationOrLatitude,
            longitude: longitude || 0,
            altitude: altitude || 0,
          }
        : locationOrLatitude;

    const { easting, northing } = utm.fromLatLon(
      location.latitude,
      location.longitude,
      this.zoneNum,
    );

    return new Vector3(easting, location.altitude, northing)
      .sub(this.origin)
      .multiply(new Vector3(1, 1, -1));
  }

  /**
   * Web Mercator (EPSG:3857) to WGS84 and local Cartesian.
   */
  fromWebMercator(x: number, y: number, altitude = 0): Vector3 {
    const coords = Coordinates.fromWebMercator(x, y, altitude);
    return this.fromLatLon(coords);
  }
}
