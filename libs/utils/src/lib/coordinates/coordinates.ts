import * as utm from 'utm';
import { MathUtils, Vector2, Vector3 } from 'three';
import { LocationDto } from '@uh4d/dto/interfaces';

type InputLocation = Pick<LocationDto, 'latitude' | 'longitude'> & {
  altitude?: number;
};

/**
 * Class to operate with WGS-84 coordinates.
 */
export class Coordinates {
  latitude: number;
  longitude: number;
  altitude: number;

  constructor(location: InputLocation);
  constructor(latitude?: number, longitude?: number, altitude?: number);
  constructor(
    locationOrLatitude: InputLocation | number = 0,
    longitude = 0,
    altitude = 0,
  ) {
    if (typeof locationOrLatitude === 'number') {
      this.latitude = locationOrLatitude;
      this.longitude = longitude;
      this.altitude = altitude;
    } else {
      this.latitude = locationOrLatitude.latitude;
      this.longitude = locationOrLatitude.longitude;
      this.altitude = locationOrLatitude.altitude || 0;
    }
  }

  /**
   * Return as Object.
   */
  toJSON(): LocationDto {
    return {
      latitude: this.latitude,
      longitude: this.longitude,
      altitude: this.altitude,
    };
  }

  /**
   * Set from Location object.
   */
  fromJSON(json: Partial<LocationDto>) {
    this.latitude = json.latitude || 0;
    this.longitude = json.longitude || 0;
    this.altitude = json.altitude || 0;
  }

  /**
   * Clone coordinates.
   */
  clone(): Coordinates {
    return new Coordinates(this.latitude, this.longitude, this.altitude);
  }

  /**
   * Copy values from coordinates.
   */
  copy(coords: Coordinates): void {
    this.latitude = coords.latitude;
    this.longitude = coords.longitude;
    this.altitude = coords.altitude;
  }

  /**
   * Add Cartesian position in meters.
   */
  add(position: Vector3): Coordinates {
    const { easting, northing, zoneNum, zoneLetter } = utm.fromLatLon(
      this.latitude,
      this.longitude,
    );

    const translation = new Vector3(easting, this.altitude, northing)
      .multiply(new Vector3(1, 1, -1))
      .add(position)
      .multiply(new Vector3(1, 1, -1));

    const { latitude, longitude } = utm.toLatLon(
      translation.x,
      translation.z,
      zoneNum,
      zoneLetter,
    );

    this.latitude = latitude;
    this.longitude = longitude;
    this.altitude = translation.y;

    return this;
  }

  /**
   * Compute WGS-84 bounding box (west, east, north, south) from this point (lat, lon) and radius.
   */
  computeBoundingBox(radius: number): {
    west: number;
    east: number;
    north: number;
    south: number;
  } {
    const { easting, northing, zoneNum, zoneLetter } = utm.fromLatLon(
      this.latitude,
      this.longitude,
    );

    const position = new Vector2(easting, northing);

    const v1 = new Vector2(-radius, -radius).add(position);
    const v2 = new Vector2(radius, radius).add(position);

    const { latitude: south, longitude: west } = utm.toLatLon(
      v1.x,
      v1.y,
      zoneNum,
      zoneLetter,
    );
    const { latitude: north, longitude: east } = utm.toLatLon(
      v2.x,
      v2.y,
      zoneNum,
      zoneLetter,
    );

    return { west, east, north, south };
  }

  /**
   * Compute Cartesian coordinates relative to coordinates that are the origin of the local Cartesian space.
   */
  toCartesian(origin: Coordinates): Vector3 {
    const originUtm = utm.fromLatLon(origin.latitude, origin.longitude);
    const originPosition = new Vector3(
      originUtm.easting,
      origin.altitude,
      originUtm.northing,
    );

    const { easting, northing } = utm.fromLatLon(
      this.latitude,
      this.longitude,
      originUtm.zoneNum,
    );
    const position = new Vector3(easting, this.altitude, northing);

    return position.sub(originPosition).multiply(new Vector3(1, 1, -1));
  }

  /**
   * Distance between two WGS84 points calculated with the Haversine formula.
   */
  distanceTo(point: Coordinates): number {
    // convert degrees to radians
    const lat1 = this.latitude * MathUtils.DEG2RAD;
    const lat2 = point.latitude * MathUtils.DEG2RAD;
    const dLon = (point.longitude - this.longitude) * MathUtils.DEG2RAD;
    const dLat = (point.latitude - this.latitude) * MathUtils.DEG2RAD;

    const radius = 6371000; // average radius of earth

    const a =
      Math.pow(Math.sin(dLat / 2), 2) +
      Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dLon / 2), 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    return radius * c;
  }

  /**
   * Web Mercator (EPSG:3857) to WGS84.
   * https://alastaira.wordpress.com/2011/01/23/the-google-maps-bing-maps-spherical-mercator-projection/
   */
  static fromWebMercator(x: number, y: number, altitude = 0): Coordinates {
    const longitude = (x / 20037508.34) * 180;
    const latitude =
      (180 / Math.PI) *
      (2 * Math.atan(Math.exp((-y / 20037508.34) * Math.PI)) - Math.PI / 2);

    return new Coordinates(latitude, longitude, altitude);
  }
}
