import { Coordinates } from './coordinates';

describe('Coordinates', () => {
  it('should return correct distance between two WGS84 points in meters', () => {
    const point1 = new Coordinates(59.3293371, -13.4877472);
    const point2 = new Coordinates(59.3225525, -13.4619422);

    const distance = point1.distanceTo(point2);

    expect(Math.round(distance)).toBe(1647);
  });
});
