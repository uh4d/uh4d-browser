import { RoleType, Uh4dRoles } from '@uh4d/config';
import { GeofenceDto } from '@uh4d/dto/interfaces';
import { Location2D } from '@uh4d/dto/interfaces/custom/spatial';
import { Coordinates } from '../coordinates/coordinates';

export class BasicAuthUser {
  readonly id: string;
  name: string;
  role?: RoleType;
  geofences?: GeofenceDto[];

  constructor(data: {
    id: string;
    name: string;
    role?: RoleType;
    geofences?: GeofenceDto[];
  }) {
    ({
      id: this.id,
      name: this.name,
      role: this.role,
      geofences: this.geofences,
    } = data);
  }

  isAdmin(): boolean {
    return !!Uh4dRoles.getRoles(this.role).ADMIN;
  }

  isModerator(): boolean {
    return !!Uh4dRoles.getRoles(this.role).MODERATOR;
  }

  isCreator(): boolean {
    return !!Uh4dRoles.getRoles(this.role).CREATOR;
  }

  /**
   * Check if location is within one of the user's geofences.
   */
  isWithinGeofence(location: Location2D): boolean {
    if (!this.geofences) return false;
    const coords = new Coordinates(location);
    return this.geofences.some((geofence) => {
      const distance = new Coordinates(geofence).distanceTo(coords);
      return distance < geofence.radius + (location.radius || 0);
    });
  }
}
