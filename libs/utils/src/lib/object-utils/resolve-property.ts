/**
 * Resolve object property from string path.
 */
export function resolveProperty<T = any>(
  obj: any,
  path: string,
): T | undefined {
  return path
    .split('.')
    .reduce((prev, curr) => (prev ? prev[curr] : null), obj);
}
