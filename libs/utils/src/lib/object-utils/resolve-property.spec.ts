import { resolveProperty } from './resolve-property';

describe('resolve object property from string path', () => {
  const entity = {
    name: 'foobar',
    date: {
      from: '2018-05-01',
      to: '2018-05-31',
    },
    status: 0,
  };

  it('should resolve property', () => {
    expect(resolveProperty(entity, 'name')).toBe('foobar');
    expect(resolveProperty(entity, 'date')).toEqual({
      from: '2018-05-01',
      to: '2018-05-31',
    });
    expect(resolveProperty(entity, 'date.from')).toBe('2018-05-01');
    expect(resolveProperty(entity, 'status')).toBe(0);
  });

  it('should resolve null for unset property', () => {
    expect(resolveProperty(entity, 'description')).toBe(undefined);
    expect(resolveProperty(entity, 'date.value')).toBe(undefined);
  });
});
