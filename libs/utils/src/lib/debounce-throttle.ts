/**
 * Creates and return a new debounced version of the passed function, which will postpone its execution
 * until after **delay** milliseconds have elapsed since the last time it was invoked. Useful for
 * implementing behavior that should only happen *after* the input has stopped arriving. For example:
 * recalculating a layout after the window has stopped being resized.
 * @param callback - The function we want to debounce.
 * @param delay - Number of  milliseconds to wait before invoking the debounced function.
 * @param leading - If true, the function is triggered on the leading instead of the trailing edge
 * of the **delay** interval. Useful for circumstances like preventing accidental double-clicks on
 * a "submit" button from firing a second time.
 * @return A debounced version of the passed function. Any arguments passed to this function will be
 * also passed. The returned function also has a `cancel()` method, which can be used in case you want
 * to reset the current debounce state. This will prevent the function from being triggered even after
 * **delay** milliseconds have passed from last input. In case **leading** is `true`, the next user
 * input will trigger the debounce.
 */
export function debounce<F extends any[], R>(
  callback: (...args: F) => R,
  delay: number,
  leading = false,
): ((...args: F) => R) & { cancel: () => void } {
  let timeout: number | undefined;
  let context: any;
  let args: IArguments;
  let result: R;

  const later = () => {
    timeout = undefined;

    if (leading !== true) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      result = callback.apply(context, args);
    }
  };

  const debounceFn = function (this: any) {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    context = this;
    // eslint-disable-next-line prefer-rest-params
    args = arguments;

    const callNow = leading && !timeout;

    if (timeout) {
      clearTimeout(timeout);
    }

    timeout = window.setTimeout(later, delay);

    if (callNow) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      result = callback.apply(context, args);
    }

    return result;
  };

  debounceFn.cancel = () => {
    clearTimeout(timeout);
    timeout = undefined;
  };

  return debounceFn;
}

/**
 * Create and return a new throttled version of the passed function, which will trigger its execution
 * only every **delay** milliseconds. This is useful to tear down fast iterative events, like `window.resize`.
 * @param callback - The function we want to throttle.
 * @param delay - Number of milliseconds to wait between each input before invoking the function.
 * @param leading - If true, the function will be invoked at the startup.
 * @param trailing - If true, the function will be invoked at end of the operation.
 * @return A throttled version of the passed function. Any arguments passed to this function will be
 * also passed. The returned function also has a `cancel()` method, which can be used in case you want
 * to reset the current throttle state. This will cause that the next input will trigger the function immediately.
 */
export function throttle<T>(
  callback: (args: IArguments) => T,
  delay: number,
  leading = false,
  trailing = true,
): (args?: IArguments) => T {
  let timeout: number | undefined;
  let context: any;
  let args: IArguments | undefined;
  let result: T;
  let previous = 0;

  const later = () => {
    previous = leading === false ? 0 : new Date().getTime();
    timeout = undefined;

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    result = callback.apply(context, args);

    if (!timeout) {
      context = args = undefined;
    }
  };

  const throttleFn = function (this: any) {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    context = this;
    // eslint-disable-next-line prefer-rest-params
    args = arguments;

    const now = new Date().getTime();

    if (!previous && leading === false) {
      previous = now;
    }

    const remaining = delay - (now - previous);

    if (remaining <= 0 || remaining > delay) {
      if (timeout) {
        clearTimeout(timeout);
        timeout = undefined;
      }

      previous = now;

      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      result = callback.apply(context, args);

      if (!timeout) {
        context = args = undefined;
      }
    } else if (!timeout && trailing !== false) {
      timeout = window.setTimeout(later, remaining);
    }

    return result;
  };

  throttleFn.cancel = () => {
    clearTimeout(timeout);
    previous = 0;
    timeout = context = args = undefined;
  };

  return throttleFn;
}
