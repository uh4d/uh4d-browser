import * as md5 from 'md5';
import { Color } from 'three';

/**
 * Convert string to HEX color.
 */
export function stringToColor(str: string, lighten = false): string {
  const hex = '#' + md5(str).slice(0, 6);
  return lighten
    ? '#' + new Color(hex).multiplyScalar(0.5).addScalar(0.5).getHexString()
    : hex;
}
