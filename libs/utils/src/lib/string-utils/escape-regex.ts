/**
 * Replace special RegExp characters.
 */
export function escapeRegex(str: string): string {
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#escaping
  return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}
