/**
 * Compare two strings that can be `null` or `undefined`.
 * A string that is `null` or `undefined` will be sorted backward.
 */
export function stringCompare(a?: string | null, b?: string | null): number {
  if (a && b) {
    return a.localeCompare(b);
  }
  if (a && !b) {
    return -1;
  }
  if (!a && b) {
    return 1;
  }
  return 0;
}
