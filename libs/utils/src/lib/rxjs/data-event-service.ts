import { merge, Subject } from 'rxjs';

export abstract class DataEventService<T> {
  private readonly createSubject = new Subject<T>();
  private readonly updateSubject = new Subject<T>();
  private readonly removeSubject = new Subject<T>();

  readonly created$ = this.createSubject.asObservable();
  readonly patched$ = this.updateSubject.asObservable();
  readonly removed$ = this.removeSubject.asObservable();

  /**
   * Fires when any update has been triggered (create, patch, and remove).
   */
  readonly update$ = merge(this.created$, this.patched$, this.removed$);

  created(value: T): void {
    this.createSubject.next(value);
  }

  updated(value: T): void {
    this.updateSubject.next(value);
  }

  removed(value: T): void {
    this.removeSubject.next(value);
  }
}
