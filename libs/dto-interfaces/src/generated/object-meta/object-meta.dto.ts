export interface ObjectMetaDto {
  zenodo_doi: string | null;
  misc: string | null;
  address: string | null;
  formerAddress: string[];
  tags: string[];
}
