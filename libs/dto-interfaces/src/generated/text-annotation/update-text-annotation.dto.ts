import { UpdateNormDataNodeDto } from '../norm-data-node/update-norm-data-node.dto';

export interface UpdateTextAnnotationDto {
  value?: string;
  start?: number;
  end?: number;
  aat?: UpdateNormDataNodeDto[];
  wikidata?: UpdateNormDataNodeDto[];
}
