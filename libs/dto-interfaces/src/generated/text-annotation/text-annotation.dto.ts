import { NormDataNodeDto } from '../norm-data-node/norm-data-node.dto';

export interface TextAnnotationDto {
  id: string;
  value: string;
  start: number;
  end: number;
  aat: NormDataNodeDto[];
  wikidata: NormDataNodeDto[];
}
