import { CreateNormDataNodeDto } from '../norm-data-node/create-norm-data-node.dto';

export interface CreateTextAnnotationDto {
  value: string;
  start: number;
  end: number;
  aat: CreateNormDataNodeDto[];
  wikidata: CreateNormDataNodeDto[];
}
