import { UpdateObjectDateDto } from '../object-date/update-object-date.dto';
import { CreateOriginDto } from '../origin/create-origin.dto';

export interface UpdateObjectDto {
  name?: string;
  date?: UpdateObjectDateDto;
  origin?: CreateOriginDto;
  vrcity_forceTextures?: boolean | null;
  vrcity_noEdges?: boolean | null;
  osm_overlap?: number[];
}
