import { ObjectDateDto } from '../object-date/object-date.dto';
import { OriginDto } from '../origin/origin.dto';
import { LocationDto } from '../location/location.dto';
import { ObjectFileDto } from '../object-file/object-file.dto';
import { UserEventDto } from '../user-event/user-event.dto';

export interface ObjectDto {
  id: string;
  name: string;
  date: ObjectDateDto;
  origin: OriginDto;
  location: LocationDto;
  file: ObjectFileDto;
  vrcity_forceTextures: boolean | null;
  vrcity_noEdges: boolean | null;
  osm_overlap: number[];
  pending: boolean | null;
  declined: boolean | null;
  uploadedBy: UserEventDto | null;
  editedBy: UserEventDto | null;
}
