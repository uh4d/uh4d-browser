import { CreateObjectDateDto } from '../object-date/create-object-date.dto';
import { CreateOriginDto } from '../origin/create-origin.dto';

export interface CreateObjectDto {
  name: string;
  date: CreateObjectDateDto;
  origin: CreateOriginDto;
  vrcity_forceTextures?: boolean | null;
  vrcity_noEdges?: boolean | null;
  osm_overlap: number[];
}
