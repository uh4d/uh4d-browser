import { LocationDto } from '../location/location.dto';

export interface TextObjectDto {
  id: string;
  name: string;
  location: LocationDto;
}
