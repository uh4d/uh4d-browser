import { MapFileDto } from '../map-file/map-file.dto';
import { LocationDto } from '../location/location.dto';

export interface MapDto {
  id: string;
  date: string;
  file: MapFileDto;
  location: LocationDto;
}
