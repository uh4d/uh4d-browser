import { UpdateMapFileDto } from '../map-file/update-map-file.dto';
import { UpdateLocationDto } from '../location/update-location.dto';

export interface UpdateMapDto {
  date?: string;
  file?: UpdateMapFileDto;
  location?: UpdateLocationDto;
}
