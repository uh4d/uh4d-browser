import { CreateMapFileDto } from '../map-file/create-map-file.dto';
import { CreateLocationDto } from '../location/create-location.dto';

export interface CreateMapDto {
  date: string;
  file: CreateMapFileDto;
  location: CreateLocationDto;
}
