export interface UpdatePoseDto {
  tx?: number;
  ty?: number;
  tz?: number;
  qx?: number;
  qy?: number;
  qz?: number;
  qw?: number;
  params?: number[];
}
