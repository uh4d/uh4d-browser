import { TerrainLocationDto } from '../terrain-location/terrain-location.dto';
import { TerrainFileDto } from '../terrain-file/terrain-file.dto';

export interface TerrainDto {
  id: string;
  name: string;
  location: TerrainLocationDto;
  file: TerrainFileDto;
}
