import { UpdateTerrainLocationDto } from '../terrain-location/update-terrain-location.dto';
import { UpdateTerrainFileDto } from '../terrain-file/update-terrain-file.dto';

export interface UpdateTerrainDto {
  name?: string;
  location?: UpdateTerrainLocationDto;
  file?: UpdateTerrainFileDto;
}
