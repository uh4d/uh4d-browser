import { CreateTerrainLocationDto } from '../terrain-location/create-terrain-location.dto';
import { CreateTerrainFileDto } from '../terrain-file/create-terrain-file.dto';

export interface CreateTerrainDto {
  name: string;
  location: CreateTerrainLocationDto;
  file: CreateTerrainFileDto;
}
