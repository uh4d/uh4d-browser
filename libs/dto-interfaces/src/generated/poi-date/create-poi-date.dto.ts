export interface CreatePoiDateDto {
  from?: string | null;
  to?: string | null;
  objectBound: boolean;
}
