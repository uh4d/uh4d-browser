export interface PoiDateDto {
  from: string | null;
  to: string | null;
  objectBound: boolean;
}
