export interface UpdatePoiDateDto {
  from?: string | null;
  to?: string | null;
  objectBound?: boolean;
}
