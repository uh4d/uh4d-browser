import { CreateImageDateDto } from '../image-date/create-image-date.dto';
import { CreateCameraDto } from '../camera/create-camera.dto';

export interface CreateImageDto {
  title: string;
  author?: string | null;
  owner?: string | null;
  date?: CreateImageDateDto | null;
  camera: CreateCameraDto;
  spatialStatus: number;
  vrcity_useAsTextureFrom?: Date | null;
  vrcity_useAsTextureTo?: Date | null;
  vrcity_projectionDistance?: number | null;
  restrictedAccess?: boolean | null;
}
