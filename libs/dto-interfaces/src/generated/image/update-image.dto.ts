import { UpdateImageDateDto } from '../image-date/update-image-date.dto';
import { CreateCameraDto } from '../camera/create-camera.dto';

export interface UpdateImageDto {
  title?: string;
  author?: string | null;
  owner?: string | null;
  date?: UpdateImageDateDto | null;
  camera?: CreateCameraDto;
  spatialStatus?: number;
  vrcity_useAsTextureFrom?: Date | null;
  vrcity_useAsTextureTo?: Date | null;
  vrcity_projectionDistance?: number | null;
  restrictedAccess?: boolean | null;
}
