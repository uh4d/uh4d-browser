import { ImageDateDto } from '../image-date/image-date.dto';
import { ImageFileDto } from '../image-file/image-file.dto';
import { CameraDto } from '../camera/camera.dto';
import { UserEventDto } from '../user-event/user-event.dto';

export interface ImageEntity {
  id: string;
  title: string;
  author: string | null;
  owner: string | null;
  date: ImageDateDto | null;
  file: ImageFileDto;
  camera: CameraDto;
  spatialStatus: number;
  vrcity_useAsTextureFrom: Date | null;
  vrcity_useAsTextureTo: Date | null;
  vrcity_projectionDistance: number | null;
  restrictedAccess: boolean | null;
  annotationsAvailable: boolean;
  pending: boolean | null;
  declined: boolean | null;
  uploadedBy: UserEventDto | null;
  editedBy: UserEventDto | null;
}
