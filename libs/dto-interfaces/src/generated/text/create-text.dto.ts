import { CreateImageDateDto } from '../image-date/create-image-date.dto';
import { CreateTextFileDto } from '../text-file/create-text-file.dto';
import { CreateTextObjectDto } from '../text-object/create-text-object.dto';

export interface CreateTextDto {
  title: string;
  authors: string[];
  date?: CreateImageDateDto | null;
  bibtex?: string | null;
  doi?: string | null;
  url?: string | null;
  file: CreateTextFileDto;
  objects: CreateTextObjectDto[];
  tags: string[];
}
