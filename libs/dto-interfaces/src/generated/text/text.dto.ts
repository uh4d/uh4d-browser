import { ImageDateDto } from '../image-date/image-date.dto';
import { TextFileDto } from '../text-file/text-file.dto';
import { TextObjectDto } from '../text-object/text-object.dto';
import { UserEventDto } from '../user-event/user-event.dto';

export interface TextDto {
  id: string;
  title: string;
  authors: string[];
  date: ImageDateDto | null;
  bibtex: string | null;
  doi: string | null;
  url: string | null;
  file: TextFileDto;
  objects: TextObjectDto[];
  tags: string[];
  annotationsAvailable: boolean;
  pending: boolean | null;
  declined: boolean | null;
  uploadedBy: UserEventDto | null;
  editedBy: UserEventDto | null;
}
