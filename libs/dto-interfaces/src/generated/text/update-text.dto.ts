import { UpdateImageDateDto } from '../image-date/update-image-date.dto';
import { UpdateTextFileDto } from '../text-file/update-text-file.dto';
import { UpdateTextObjectDto } from '../text-object/update-text-object.dto';

export interface UpdateTextDto {
  title?: string;
  authors?: string[];
  date?: UpdateImageDateDto | null;
  bibtex?: string | null;
  doi?: string | null;
  url?: string | null;
  file?: UpdateTextFileDto;
  objects?: UpdateTextObjectDto[];
  tags?: string[];
}
