import { UserEventDto } from '../user-event/user-event.dto';

export interface TourDto {
  id: string;
  title: string;
  createdBy: UserEventDto | null;
  editedBy: UserEventDto | null;
}
