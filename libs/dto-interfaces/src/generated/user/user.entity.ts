import { RoleType } from '@uh4d/config';
import { GeofenceDto } from '../geofence/geofence.dto';

export interface UserEntity {
  id: string;
  name: string;
  email: string;
  phone: string;
  password: string;
  lastLogin: Date | null;
  authSecret: string;
  refreshToken: string | null;
  pending: boolean | null;
  role: RoleType | null;
  geofences: GeofenceDto[];
}
