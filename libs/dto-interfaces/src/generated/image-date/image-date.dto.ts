export interface ImageDateDto {
  value: string;
  from: Date;
  to: Date;
  display: string;
}
