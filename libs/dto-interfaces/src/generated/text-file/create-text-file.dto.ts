export interface CreateTextFileDto {
  path: string;
  file: string;
  preview: string;
  thumb: string;
  original: string;
  type: string;
  pdf?: string | null;
  txt?: string | null;
  json?: string | null;
  bib?: string | null;
}
