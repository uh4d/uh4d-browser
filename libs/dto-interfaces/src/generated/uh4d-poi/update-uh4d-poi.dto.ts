import { UpdateLocationDto } from '../location/update-location.dto';
import { CreatePoiDateDto } from '../poi-date/create-poi-date.dto';

export interface UpdateUh4dPoiDto {
  title?: string;
  content?: string;
  location?: UpdateLocationDto;
  date?: CreatePoiDateDto;
  objectId?: string | null;
  camera?: number[];
  timestamp?: Date | null;
  damageFactors?: string[];
}
