import { CreateLocationDto } from '../location/create-location.dto';
import { CreatePoiDateDto } from '../poi-date/create-poi-date.dto';

export interface CreateUh4dPoiDto {
  title: string;
  content: string;
  location: CreateLocationDto;
  date: CreatePoiDateDto;
  objectId?: string | null;
  camera: number[];
  timestamp?: Date | null;
  damageFactors: string[];
}
