import { LocationDto } from '../location/location.dto';
import { PoiDateDto } from '../poi-date/poi-date.dto';
import { UserEventDto } from '../user-event/user-event.dto';

export interface Uh4dPoiDto {
  id: string;
  title: string;
  content: string;
  location: LocationDto;
  type: string;
  date: PoiDateDto;
  objectId: string | null;
  camera: number[];
  timestamp: Date | null;
  damageFactors: string[];
  pending: boolean | null;
  declined: boolean | null;
  createdBy: UserEventDto | null;
  editedBy: UserEventDto | null;
}
