import { UpdateNormDataNodeDto } from '../norm-data-node/update-norm-data-node.dto';

export interface UpdateObjectAnnotationDto {
  objectName?: string;
  bbox?: number[];
  volume?: number;
  aat?: UpdateNormDataNodeDto[];
  wikidata?: UpdateNormDataNodeDto[];
}
