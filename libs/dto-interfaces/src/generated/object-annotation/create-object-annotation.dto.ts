import { CreateNormDataNodeDto } from '../norm-data-node/create-norm-data-node.dto';

export interface CreateObjectAnnotationDto {
  objectName: string;
  bbox: number[];
  volume: number;
  aat: CreateNormDataNodeDto[];
  wikidata: CreateNormDataNodeDto[];
}
