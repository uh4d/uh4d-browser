import { NormDataNodeDto } from '../norm-data-node/norm-data-node.dto';

export interface ObjectAnnotationDto {
  id: string;
  objectName: string;
  bbox: number[];
  volume: number;
  aat: NormDataNodeDto[];
  wikidata: NormDataNodeDto[];
}
