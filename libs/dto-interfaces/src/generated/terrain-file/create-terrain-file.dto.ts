export interface CreateTerrainFileDto {
  path: string;
  file: string;
  type: string;
}
