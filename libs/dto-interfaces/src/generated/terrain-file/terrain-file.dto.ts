export interface TerrainFileDto {
  path: string;
  file: string;
  type: string;
}
