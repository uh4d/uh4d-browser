export interface UpdateTerrainFileDto {
  path?: string;
  file?: string;
  type?: string;
}
