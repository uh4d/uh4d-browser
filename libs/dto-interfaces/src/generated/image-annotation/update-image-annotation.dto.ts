import { UpdateNormDataNodeDto } from '../norm-data-node/update-norm-data-node.dto';

export interface UpdateImageAnnotationDto {
  polylist?: number[][][];
  bbox?: number[];
  area?: number;
  aat?: UpdateNormDataNodeDto[];
  wikidata?: UpdateNormDataNodeDto[];
}
