import { NormDataNodeDto } from '../norm-data-node/norm-data-node.dto';

export interface ImageAnnotationDto {
  id: string;
  polylist: number[][][];
  bbox: number[];
  area: number;
  aat: NormDataNodeDto[];
  wikidata: NormDataNodeDto[];
}
