import { CreateNormDataNodeDto } from '../norm-data-node/create-norm-data-node.dto';

export interface CreateImageAnnotationDto {
  polylist: number[][][];
  bbox: number[];
  area: number;
  aat: CreateNormDataNodeDto[];
  wikidata: CreateNormDataNodeDto[];
}
