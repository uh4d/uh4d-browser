export interface CreatePipelineImageDateDto {
  from: Date;
  to: Date;
}
