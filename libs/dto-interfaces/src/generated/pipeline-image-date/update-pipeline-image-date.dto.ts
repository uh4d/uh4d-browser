export interface UpdatePipelineImageDateDto {
  from?: Date;
  to?: Date;
}
