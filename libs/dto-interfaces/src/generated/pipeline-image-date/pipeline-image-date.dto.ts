export interface PipelineImageDateDto {
  from: Date;
  to: Date;
}
