import { UpdateLocationDto } from '../location/update-location.dto';

export interface UpdateGeneralPoiDto {
  title?: string;
  content?: string;
  location?: UpdateLocationDto;
  type?: string;
}
