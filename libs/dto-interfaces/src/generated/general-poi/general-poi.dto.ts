import { LocationDto } from '../location/location.dto';

export interface GeneralPoiDto {
  id: string;
  title: string;
  content: string;
  location: LocationDto;
  type: string;
}
