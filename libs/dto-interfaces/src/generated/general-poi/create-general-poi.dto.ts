import { CreateLocationDto } from '../location/create-location.dto';

export interface CreateGeneralPoiDto {
  title: string;
  content: string;
  location: CreateLocationDto;
  type: string;
}
