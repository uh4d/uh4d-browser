import { PoseDto } from '../pose/pose.dto';
import { PipelineImageDateDto } from '../pipeline-image-date/pipeline-image-date.dto';

export interface PipelineImageDto {
  id: string;
  width: number;
  height: number;
  pose: PoseDto | null;
  image: string;
  pkl: string | null;
  date: PipelineImageDateDto | null;
  spatialStatus: number;
}
