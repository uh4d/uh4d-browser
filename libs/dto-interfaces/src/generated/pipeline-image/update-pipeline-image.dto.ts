import { UpdatePoseDto } from '../pose/update-pose.dto';
import { UpdatePipelineImageDateDto } from '../pipeline-image-date/update-pipeline-image-date.dto';

export interface UpdatePipelineImageDto {
  width?: number;
  height?: number;
  pose?: UpdatePoseDto | null;
  image?: string;
  pkl?: string | null;
  date?: UpdatePipelineImageDateDto | null;
  spatialStatus?: number;
}
