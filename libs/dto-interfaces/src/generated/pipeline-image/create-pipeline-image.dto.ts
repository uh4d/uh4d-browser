import { CreatePoseDto } from '../pose/create-pose.dto';
import { CreatePipelineImageDateDto } from '../pipeline-image-date/create-pipeline-image-date.dto';

export interface CreatePipelineImageDto {
  width: number;
  height: number;
  pose?: CreatePoseDto | null;
  image: string;
  pkl?: string | null;
  date?: CreatePipelineImageDateDto | null;
  spatialStatus: number;
}
