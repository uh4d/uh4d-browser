export interface CreateTerrainLocationDto {
  latitude: number;
  longitude: number;
  altitude: number;
  west: number;
  east: number;
  north: number;
  south: number;
}
