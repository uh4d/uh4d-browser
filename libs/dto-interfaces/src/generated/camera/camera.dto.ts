export interface CameraDto {
  latitude: number;
  longitude: number;
  altitude: number;
  radius: number | null;
  omega: number | null;
  phi: number | null;
  kappa: number | null;
  ck: number | null;
  offset: number[];
  k: number[];
}
