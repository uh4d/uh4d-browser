export interface CreateUserEventDto {
  userId: string;
  username: string;
  timestamp: string;
}
