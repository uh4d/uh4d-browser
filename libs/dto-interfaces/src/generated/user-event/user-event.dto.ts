export interface UserEventDto {
  userId: string;
  username: string;
  timestamp: string;
}
