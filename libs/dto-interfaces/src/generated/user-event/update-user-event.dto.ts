export interface UpdateUserEventDto {
  userId?: string;
  username?: string;
  timestamp?: string;
}
