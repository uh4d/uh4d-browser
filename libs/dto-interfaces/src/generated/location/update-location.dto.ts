export interface UpdateLocationDto {
  latitude?: number;
  longitude?: number;
  altitude?: number;
}
