export interface CreateLocationDto {
  latitude: number;
  longitude: number;
  altitude: number;
}
