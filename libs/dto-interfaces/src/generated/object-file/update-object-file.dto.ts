export interface UpdateObjectFileDto {
  path?: string;
  file?: string;
  original?: string;
  type?: string;
  lowRes?: string | null;
  noEdges?: string | null;
  noEdgesLowRes?: string | null;
}
