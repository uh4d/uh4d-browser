export interface CreateObjectFileDto {
  path: string;
  file: string;
  original: string;
  type: string;
  lowRes?: string | null;
  noEdges?: string | null;
  noEdgesLowRes?: string | null;
}
