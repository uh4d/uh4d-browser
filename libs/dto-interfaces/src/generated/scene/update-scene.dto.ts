export interface UpdateSceneDto {
  name?: string;
  latitude?: number;
  longitude?: number;
  altitude?: number;
}
