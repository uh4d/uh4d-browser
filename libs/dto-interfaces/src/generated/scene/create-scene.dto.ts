export interface CreateSceneDto {
  name: string;
  latitude: number;
  longitude: number;
  altitude: number;
}
