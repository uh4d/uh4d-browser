export interface SceneDto {
  id: string;
  name: string;
  latitude: number;
  longitude: number;
  altitude: number;
}
