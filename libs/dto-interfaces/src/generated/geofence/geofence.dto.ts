export interface GeofenceDto {
  latitude: number;
  longitude: number;
  radius: number;
}
