export interface CreateGeofenceDto {
  latitude: number;
  longitude: number;
  radius: number;
}
