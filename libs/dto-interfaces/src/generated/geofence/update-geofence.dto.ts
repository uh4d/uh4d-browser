export interface UpdateGeofenceDto {
  latitude?: number;
  longitude?: number;
  radius?: number;
}
