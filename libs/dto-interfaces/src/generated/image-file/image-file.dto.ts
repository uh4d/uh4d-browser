export interface ImageFileDto {
  path: string;
  original: string;
  preview: string;
  thumb: string;
  tiny: string;
  texture: string;
  type: string;
  width: number;
  height: number;
  pkl: string | null;
}
