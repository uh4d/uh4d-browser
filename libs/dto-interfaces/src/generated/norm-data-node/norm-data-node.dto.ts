export interface NormDataNodeDto {
  id: string;
  identifier: string;
  label: string | null;
  weight: number | null;
}
