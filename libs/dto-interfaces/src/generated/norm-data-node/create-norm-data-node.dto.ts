export interface CreateNormDataNodeDto {
  id: string;
  identifier: string;
  label?: string | null;
  weight?: number | null;
}
