export interface UpdateNormDataNodeDto {
  id?: string;
  identifier?: string;
  label?: string | null;
  weight?: number | null;
}
