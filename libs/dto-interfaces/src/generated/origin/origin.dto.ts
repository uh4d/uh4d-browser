export interface OriginDto {
  latitude: number;
  longitude: number;
  altitude: number;
  omega: number;
  phi: number;
  kappa: number;
  scale: number[];
}
