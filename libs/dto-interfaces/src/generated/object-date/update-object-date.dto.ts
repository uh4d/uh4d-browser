export interface UpdateObjectDateDto {
  from?: string | null;
  to?: string | null;
}
