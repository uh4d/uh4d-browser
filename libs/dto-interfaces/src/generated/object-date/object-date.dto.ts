export interface ObjectDateDto {
  from: string | null;
  to: string | null;
}
