export interface CreateObjectDateDto {
  from?: string | null;
  to?: string | null;
}
