export interface ImageMetaDto {
  description: string | null;
  misc: string | null;
  captureNumber: string | null;
  permalink: string | null;
  tags: string[];
}
