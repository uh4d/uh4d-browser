export interface UpdateImageMetaDto {
  description?: string | null;
  misc?: string | null;
  permalink?: string | null;
  tags?: string[];
}
