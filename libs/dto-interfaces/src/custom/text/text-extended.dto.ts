import { TextDto } from '@uh4d/dto/interfaces';

export interface TextExtendedDto extends TextDto {
  annotations?: { [key: string]: { id: string; similarity: number }[] };
  relevance?: number;
}
