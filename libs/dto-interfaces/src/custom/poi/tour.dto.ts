import { LocationDto, UserEventDto } from '@uh4d/dto/interfaces';

export interface TourDto {
  id: string;
  title: string;
  pois: {
    id: string;
    title: string;
    location: LocationDto;
  }[];
  createdBy?: UserEventDto;
}
