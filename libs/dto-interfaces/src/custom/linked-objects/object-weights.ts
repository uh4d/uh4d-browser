export interface ObjectWeights {
  coverageWeight: number;
  distanceWeight: number;
  weight: number;
}
