import { LocationDto, OriginDto } from '@uh4d/dto/interfaces';

export interface IntersectionsPayload {
  modelPath: string;
  origin: OriginDto;
  location: LocationDto;
}
