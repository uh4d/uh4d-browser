import { OverpassPayload } from '@uh4d/dto/interfaces/custom/osm/overpass';
import { LocationDto, TerrainDto } from '@uh4d/dto/interfaces';

export interface OsmGenerationPayload {
  lat: number;
  lon: number;
  folder: string;
  osmData: OverpassPayload;
  terrainPath: string;
  terrainData?: TerrainDto;
  isCustomTerrain?: boolean;
}

export interface OsmGeneratedDto {
  location: LocationDto;
  cachedFile: string;
}
