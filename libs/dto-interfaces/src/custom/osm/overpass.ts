export interface OsmNode {
  id: number;
  type: 'node';
  lat: number;
  lon: number;
}

export interface OsmTags {
  building?: string;
  'building:levels'?: string;
  height?: string;
  'roof:shape'?: string;
  [key: string]: string | undefined;
}

export interface OsmWay {
  id: number;
  type: 'way';
  nodes: number[];
  tags?: OsmTags;
}

export interface OsmMember {
  type: string;
  ref: number;
  role: 'outer' | 'inner' | 'part' | string;
}

export interface OsmRelation {
  id: number;
  type: 'relation';
  members: OsmMember[];
  tags?: OsmTags;
}

export interface OverpassPayload {
  version: number;
  generator: string;
  elements: (OsmNode | OsmWay | OsmRelation)[];
}
