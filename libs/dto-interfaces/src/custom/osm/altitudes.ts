import { TerrainDto } from '@uh4d/dto/interfaces';

export interface AltitudesPayload {
  lat: number;
  lon: number;
  osmPath: string;
  modelPath: string;
  terrainData?: TerrainDto;
  isCustomTerrain?: boolean;
}
