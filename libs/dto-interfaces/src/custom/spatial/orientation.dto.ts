export interface OrientationDto {
  omega: number;
  phi: number;
  kappa: number;
}
