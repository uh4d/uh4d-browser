import { LocationDto } from '@uh4d/dto/interfaces/location';
import { OrientationDto } from './orientation.dto';

export type LocationOrientationDto = LocationDto & OrientationDto;
