export interface Location2D {
  latitude: number;
  longitude: number;
  radius?: number | null;
}
