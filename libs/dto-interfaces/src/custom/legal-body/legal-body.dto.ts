import { PersonDto } from '@uh4d/dto/interfaces/person';

export type LegalBodyDto = PersonDto;
