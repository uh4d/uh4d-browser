export interface ProjectionSourceImage {
  id: string;
  matrix: number[];
  ck: number;
  width: number;
  height: number;
}

export interface ProjectionSourceObject {
  id: string;
  file: string;
  matrix: number[];
}

export interface ProjectionPayload {
  image: ProjectionSourceImage;
  objects: ProjectionSourceObject[];
  terrain?: ProjectionSourceObject;
}
