import { ObjectDto } from '@uh4d/dto/interfaces/object';
import { ObjectMetaDto } from '@uh4d/dto/interfaces/object-meta';

export type ObjectFullDto = ObjectDto & ObjectMetaDto;
