import { CreateObjectFileDto } from '@uh4d/dto/interfaces';

export type ConvertedFilesDto = Omit<CreateObjectFileDto, 'path' | 'original'>;
