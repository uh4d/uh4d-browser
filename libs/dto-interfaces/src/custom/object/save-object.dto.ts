import { LocationOrientationDto } from '@uh4d/dto/interfaces/custom/spatial';
import { TempObjectDto } from './temp-object.dto';

export type SaveObjectDto = TempObjectDto &
  LocationOrientationDto & { scale: number[] };
