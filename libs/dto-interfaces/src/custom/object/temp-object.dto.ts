import { CreateObjectFileDto } from '@uh4d/dto/interfaces';

export interface TempObjectDto extends CreateObjectFileDto {
  id: string;
  name: string;
}
