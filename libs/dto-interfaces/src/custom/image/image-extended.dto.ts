import { ImageDto } from '@uh4d/dto/interfaces';

export interface ImageExtendedDto extends ImageDto {
  objects?: { [key: string]: number };
  annotations?: { [key: string]: { id: string; similarity: number }[] };
  relevance?: number;
}
