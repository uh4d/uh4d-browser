import { ImageDto } from '@uh4d/dto/interfaces/image';
import { ImageMetaDto } from '@uh4d/dto/interfaces/image-meta';

export type ImageFullDto = ImageDto & ImageMetaDto;
