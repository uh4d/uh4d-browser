export interface ImageFileUpdateDto {
  url?: string;
  type?: string;
  mime?: string;
  width?: number;
  height?: number;
  wUnits?: string;
  hUnits?: string;
  length?: number;
  updateAvailable: boolean;
  reason?: string;
}
