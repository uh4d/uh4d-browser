import { ImageDto } from '@uh4d/dto/interfaces';

export interface RephotoWithTypeDto
  extends Pick<
    ImageDto,
    | 'id'
    | 'title'
    | 'file'
    | 'camera'
    | 'author'
    | 'date'
    | 'spatialStatus'
    | 'pending'
    | 'declined'
  > {
  type: string;
}
