export interface NormDataDescriptionDto {
  description: string;
  imageUrl: string | null;
}
