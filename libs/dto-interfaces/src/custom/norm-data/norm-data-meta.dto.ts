import { ImageDto } from '@uh4d/dto/interfaces/image';
import { TextDto } from '@uh4d/dto/interfaces/text';
import { NormDataNodeDto } from '@uh4d/dto/interfaces/norm-data-node';

interface ConnectedImageDto
  extends Pick<ImageDto, 'id' | 'title' | 'file' | 'date' | 'author'> {
  numberOfAnnotations: number;
}

interface ConnectedTextDto
  extends Pick<TextDto, 'id' | 'title' | 'file' | 'date' | 'authors'> {
  numberOfAnnotations: number;
}

export interface NormDataMetaDto {
  wikidata?: NormDataNodeDto;
  aat?: NormDataNodeDto;
  images: ConnectedImageDto[];
  texts: ConnectedTextDto[];
}
