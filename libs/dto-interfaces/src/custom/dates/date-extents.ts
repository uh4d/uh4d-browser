export interface DateExtents {
  images: {
    min: string;
    max: string;
  } | null;
  objects: {
    min: string;
    max: string;
  } | null;
}
