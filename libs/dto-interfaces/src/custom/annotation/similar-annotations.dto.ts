import { TextDto } from '@uh4d/dto/interfaces/text';
import { ImageDto } from '@uh4d/dto/interfaces/image';
import { ObjectDto } from '@uh4d/dto/interfaces/object';

export interface SimilarAnnotationsDto {
  images: SimilarRecord<ImageDto>[];
  texts: SimilarRecord<TextDto>[];
  objects: SimilarRecord<ObjectDto>[];
}

export interface SimilarRecord<
  T extends ImageDto | TextDto | ObjectDto = ImageDto | TextDto | ObjectDto,
> {
  id: string;
  title: string;
  file: T['file'];
  maxSimilarity: number;
  annotations: {
    id: string;
    similarity: number;
  }[];
}
