export interface SimilarAnnotationsRecord {
  id: string;
  maxSimilarity: number;
  annotations: {
    id: string;
    similarity: number;
  }[];
}

export interface CombinedSimilarAnnotationsDto {
  id: string;
  combinedSimilarity: number;
  annotationRecords: Record<string, Omit<SimilarAnnotationsRecord, 'id'>>;
}
