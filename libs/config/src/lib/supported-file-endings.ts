import * as mime from 'mime';

// define unknown file mime-types
// https://www.digipres.org/formats/mime-types/
mime.define(
  {
    'application/octet-stream': ['fbx'],
    'application/x-3ds': ['3ds'],
    'application/x-blend': ['blend'],
  },
  true,
);

const supportedImageFileEndings = [
  '.png',
  '.jpg',
  '.jpeg',
  // '.gif',
  // '.bmp',
  '.tiff',
  '.tif',
  // '.webp',
];

const supportedDocumentFileEndings = ['.pdf'];

const supportedModelFileEndings = [
  '.obj',
  // '.dae',
  // '.fbx',
  // '.stl',
  // '.3ds',
  '.gltf',
  '.glb',
  '.zip',
];

const supportedSharpFileEndings = [
  '.png',
  '.jpg',
  '.jpeg',
  '.gif',
  '.tiff',
  '.tif',
  '.svg',
  '.webp',
];

const supportedMediaTypes = ['image', 'model', 'document', 'sharp'] as const;
export type MediaType = (typeof supportedMediaTypes)[number];

/**
 * Get all file endings that are supported by UH4D.
 * If `type` is provided, get all file endings that are supported for the type of media.
 */
export function getSupportedFileEndings(type?: MediaType): string[] {
  switch (type) {
    case 'image':
      return supportedImageFileEndings.slice();
    case 'document':
      return supportedDocumentFileEndings.slice();
    case 'model':
      return supportedModelFileEndings.slice();
    case 'sharp':
      return supportedSharpFileEndings.slice();
    default:
      return Array.from(
        new Set([
          ...supportedImageFileEndings,
          ...supportedDocumentFileEndings,
          ...supportedModelFileEndings,
          ...supportedSharpFileEndings,
        ]),
      );
  }
}

/**
 * Get all file endings that are supported by UH4D excluding input types.
 * @param excluding MediaTypes which file endings should not be included
 * @returns
 * @example getSupportedFileEndingsExcept('model') => all file endings except model file types
 */
export function getSupportedFileEndingsExcept(
  excluding: MediaType[],
): string[] {
  return supportedMediaTypes
    .filter((m) => !excluding.includes(m))
    .flatMap((m) => getSupportedFileEndings(m));
}

/**
 * Check if fileEnding `.xyz` is supported by UH4D.
 */
export function isFileEndingSupported(
  fileEnding: string,
  type?: MediaType,
): boolean {
  let fe = fileEnding.toLowerCase();
  if (!fileEnding.startsWith('.')) {
    fe = '.' + fe;
  }
  return getSupportedFileEndings(type).includes(fe);
}
