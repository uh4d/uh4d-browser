export const damageFactors = [
  { key: 'physical-forces', label: 'Physical forces' },
  { key: 'fire', label: 'Fire' },
  { key: 'vandalism', label: 'Theft + vandalism' },
  { key: 'dissociation', label: 'Dissociation' },
  { key: 'water', label: 'Water (flood / heavy rain)' },
  { key: 'light', label: 'Light + radiation' },
  { key: 'temperature', label: 'Harmful temperature' },
  { key: 'humidity', label: 'Harmful humidity' },
  { key: 'biological-attack', label: 'Biological attack' },
  { key: 'hazardous-substances', label: 'Hazardous substances' },
];
