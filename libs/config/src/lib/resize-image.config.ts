import { FitEnum, FormatEnum } from 'sharp';

export const resizeImageTypes = [
  'preview',
  'thumb',
  'tiny',
  'mobile',
  'texture',
] as const;
export type ResizeImageType = (typeof resizeImageTypes)[number];

type ResizeImageSizes = Record<
  ResizeImageType,
  {
    width: number;
    height: number;
    quality: number;
    format: keyof FormatEnum;
    fit: keyof FitEnum;
  }
>;

export class ResizeImageConfig {
  static readonly sizes: ResizeImageSizes = {
    preview: {
      width: 2048,
      height: 2048,
      quality: 80,
      format: 'webp',
      fit: 'outside',
    },
    thumb: {
      width: 200,
      height: 200,
      quality: 80,
      format: 'webp',
      fit: 'outside',
    },
    tiny: {
      width: 64,
      height: 64,
      quality: 80,
      format: 'webp',
      fit: 'fill',
    },
    mobile: {
      width: 512,
      height: 512,
      quality: 60,
      format: 'webp',
      fit: 'fill',
    },
    texture: {
      width: 1024,
      height: 1024,
      quality: 60,
      format: 'webp',
      fit: 'fill',
    },
  };

  static createFilename(file: string, imageSize: ResizeImageType): string {
    return (
      file.replace(/\.[^./]+$/, '') +
      `_${imageSize}.${this.sizes[imageSize].format}`
    );
  }
}
