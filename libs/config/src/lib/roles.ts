export type RoleType = 'guest' | 'user' | 'creator' | 'moderator' | 'admin';

const permissions = {
  GUEST: [],
  USER: ['uploadImages', 'uploadObjects', 'uploadTexts', 'createPOIs'],
  CREATOR: [
    'validateContent',
    'manageImages',
    'manageObjects',
    'duplicateObjects',
    'manageTexts',
    'managePOIs',
    'createTours',
    'manageTours',
  ],
  MODERATOR: [
    'validateContent',
    'manageImages',
    'manageObjects',
    'duplicateObjects',
    'manageTexts',
    'managePOIs',
    'createTours',
    'manageTours',
  ],
  ADMIN: ['manageAccounts'],
} as const;

export type PermissionType =
  // | (typeof permissions.GUEST)[number]
  | (typeof permissions.USER)[number]
  | (typeof permissions.CREATOR)[number]
  | (typeof permissions.MODERATOR)[number]
  | (typeof permissions.ADMIN)[number];

export class Uh4dRoles {
  static getRoles(
    role?: string | null,
  ): Partial<Record<Uppercase<RoleType>, PermissionType[]>> {
    switch (role) {
      case 'user':
        return {
          USER: [...permissions.USER],
        };
      case 'creator':
        return {
          USER: [...permissions.USER],
          CREATOR: [...permissions.CREATOR],
        };
      case 'moderator':
        return {
          USER: [...permissions.USER],
          MODERATOR: [...permissions.MODERATOR],
        };
      case 'admin':
        return {
          USER: [...permissions.USER],
          MODERATOR: [...permissions.MODERATOR],
          ADMIN: [...permissions.ADMIN],
        };
      default:
        return {
          GUEST: [...permissions.GUEST],
        };
    }
  }

  static getPermissions(role?: string | null): PermissionType[] {
    const roles = Uh4dRoles.getRoles(role);
    return Object.values(roles).flat();
  }
}
