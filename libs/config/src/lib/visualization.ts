export type VisualizationType =
  | 'heatmap'
  | 'vectorfield'
  | 'windmap'
  | 'radialfan'
  | 'none';

export interface IVizMeta {
  label: string;
  description: string;
  imgSrc: string;
}

export const vizMeta = new Map<VisualizationType, IVizMeta>([
  [
    'heatmap',
    {
      label: 'Heat Map',
      description: 'Identify aggregations of images.',
      imgSrc: 'assets/viz/heatmap.jpg',
    },
  ],
  [
    'vectorfield',
    {
      label: 'Vector Field',
      description: 'Identify aggregations of orientations of images.',
      imgSrc: 'assets/viz/vectorfield.jpg',
    },
  ],
  [
    'windmap',
    {
      label: 'Particles',
      description:
        'Identify aggregations of orientations of images. Similar to Vector Field, but fancier.',
      imgSrc: 'assets/viz/particles.jpg',
    },
  ],
  [
    'radialfan',
    {
      label: 'Radial Fan',
      description:
        'Identify aggregations of orientations of images for each cluster.',
      imgSrc: 'assets/viz/radialfan.jpg',
    },
  ],
]);
