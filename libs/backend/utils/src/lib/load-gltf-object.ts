import { Euler, Matrix4, Object3D, Quaternion, Vector3 } from 'three';
import { loadGltf } from 'node-three-gltf';
import { OrientationDto } from '@uh4d/dto/interfaces/custom/spatial';

interface OrientationAndScale extends OrientationDto {
  scale?: number[];
}

/**
 * Load object and set orientation (angles) and scale.
 */
export async function loadGltfObject(
  filePath: string,
  matrixOrOrientation?: Matrix4 | OrientationAndScale,
): Promise<Object3D> {
  const gltf = await loadGltf(filePath);
  const obj = gltf.scene.children[0];

  if (matrixOrOrientation instanceof Matrix4) {
    obj.applyMatrix4(matrixOrOrientation);
    obj.matrixAutoUpdate = false;
  } else if (matrixOrOrientation) {
    const euler = new Euler(
      matrixOrOrientation.omega,
      matrixOrOrientation.phi,
      matrixOrOrientation.kappa,
      'YXZ',
    );
    const scale = new Vector3(1, 1, 1);
    if (matrixOrOrientation.scale) scale.fromArray(matrixOrOrientation.scale);
    const matrix = new Matrix4().compose(
      new Vector3(),
      new Quaternion().setFromEuler(euler),
      scale,
    );
    obj.applyMatrix4(matrix);
    obj.matrixAutoUpdate = false;
  }

  obj.updateMatrixWorld(true);

  return obj;
}
