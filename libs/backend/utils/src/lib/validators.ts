import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

@ValidatorConstraint({ name: 'string-or-number', async: false })
export class IsNumberOrString implements ValidatorConstraintInterface {
  validate(value: unknown): Promise<boolean> | boolean {
    return typeof value === 'number' || typeof value === 'string';
  }

  defaultMessage(): string {
    return '($value) must be number or string!';
  }
}
