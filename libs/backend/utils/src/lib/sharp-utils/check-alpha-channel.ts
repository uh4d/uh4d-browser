import * as sharp from 'sharp';

/**
 * Check if the image file has an alpha channel or any transparent pixels.
 */
export async function checkAlphaChannel(file: string): Promise<boolean>;
export async function checkAlphaChannel(file: sharp.Sharp): Promise<boolean>;
export async function checkAlphaChannel(
  file: string | sharp.Sharp,
): Promise<boolean> {
  const image = typeof file === 'string' ? sharp(file) : file;
  const metadata = await image.metadata();

  if (metadata.channels !== 4) return false;

  const { data, info } = await image
    .raw()
    .toBuffer({ resolveWithObject: true });
  if (info.channels < 4) return false;

  for (let i = 0, l = info.width * info.height * 4; i < l; i += 4) {
    if (data[i + 3] !== 255) return true;
  }

  return false;
}
