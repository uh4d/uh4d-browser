import { Transform } from 'class-transformer';

/**
 * Transform value to Boolean.
 * It will resolve to `true` if the value equals
 * `true`, `'true'`, `'1'`, or `1`.
 */
export function ToBoolean(): PropertyDecorator {
  return Transform(
    ({ value }) =>
      value === 'true' || value === true || value === '1' || value === 1,
  );
}
