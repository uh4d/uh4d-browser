import { spawn } from 'child_process';
import { Logger } from '@nestjs/common';

/**
 * Spawn process and log its stdout/stderr output.
 */
export function spawnAndLog(
  command: string,
  args: string[],
  logger: Logger | Console,
  loglevel: 'info' | 'error' | 'fatal' | 'verbose' = 'info',
): Promise<void> {
  const logStdout = ['info', 'verbose'].includes(loglevel);
  const logStderr = ['error', 'verbose'].includes(loglevel);

  const childProcess = spawn(command, args);

  if (logStdout) {
    childProcess.stdout.setEncoding('utf8');
    childProcess.stdout.on('data', (data) => {
      logger.debug(data.toString().trim());
    });
  }

  const errorLog: string[] = [];

  childProcess.stderr.setEncoding('utf8');
  childProcess.stderr.on('data', (data) => {
    const log = data.toString().trim();
    if (logStderr) logger.error(log);
    else errorLog.push(log);
  });

  return new Promise<void>((resolve, reject) => {
    childProcess.on('close', (code) => {
      if (code) {
        errorLog.forEach((log) => logger.error(log));
        reject(`${command} ${args[0]} exited with code ${code}`);
      } else {
        resolve();
      }
    });
    childProcess.on('error', () => {
      errorLog.forEach((log) => logger.error(log));
      reject();
    });
  });
}
