import { fork, ForkOptions } from 'child_process';

/**
 * Fork node process and send payload.
 */
export function forkProcess<T = unknown>(
  modulePath: string,
  args: string[],
  payload: any,
  options: ForkOptions = { execPath: 'node', execArgv: [] },
): Promise<T> {
  const childProcess = fork(modulePath, args, options);

  // send payload
  childProcess.on('spawn', () => {
    childProcess.send(payload);
  });

  return new Promise<T>((resolve, reject) => {
    childProcess.on('message', resolve);
    childProcess.on('close', (code) => {
      if (code) {
        reject(`childProcess exited with code ${code}`);
      }
    });
    childProcess.on('error', reject);
  });
}
