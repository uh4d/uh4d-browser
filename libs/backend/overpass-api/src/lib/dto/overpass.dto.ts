import { ApiExtraModels, ApiProperty, getSchemaPath } from '@nestjs/swagger';
import { OverpassPayload } from '@uh4d/dto/interfaces/custom/osm';

class Osm3s {
  @ApiProperty({ required: false, format: 'date' })
  timestamp_osm_base: string;
  @ApiProperty({ required: false })
  copyright: string;
}

export class OsmNode {
  @ApiProperty({ required: false, format: 'integer' })
  id: number;
  @ApiProperty({ required: false, enum: ['node'] })
  type: 'node';
  @ApiProperty({ required: false, format: 'float' })
  lat: number;
  @ApiProperty({ required: false, format: 'float' })
  lon: number;
}

class OsmTags {
  @ApiProperty({ required: false, nullable: true })
  building?: string;
  @ApiProperty({ required: false, nullable: true })
  'building:levels'?: string;
  @ApiProperty({ required: false, nullable: true })
  height?: string;
  @ApiProperty({ required: false, nullable: true })
  'roof:shape'?: string;
  [key: string]: string | undefined;
}

export class OsmWay {
  @ApiProperty({ required: false, format: 'integer' })
  id: number;
  @ApiProperty({ required: false, enum: ['way'] })
  type: 'way';
  @ApiProperty({
    required: false,
    type: 'array',
    items: { type: 'number', format: 'integer' },
  })
  nodes: number[];
  @ApiProperty({ required: false, type: OsmTags, nullable: true })
  tags?: OsmTags;
}

class OsmMember {
  @ApiProperty({ required: false, enum: ['way'] })
  type: string;
  @ApiProperty({ required: false, format: 'integer' })
  ref: number;
  @ApiProperty({ required: false, enum: ['inner', 'outer', 'outline', 'part'] })
  role: 'outer' | 'inner' | 'part' | string;
}

export class OsmRelation {
  @ApiProperty({ required: false, format: 'integer' })
  id: number;
  @ApiProperty({ required: false, enum: ['relation'] })
  type: 'relation';
  @ApiProperty({ required: false, type: [OsmMember] })
  members: OsmMember[];
  @ApiProperty({ required: false, type: OsmTags, nullable: true })
  tags?: OsmTags;
}

@ApiExtraModels(OsmNode, OsmWay, OsmRelation)
export class OverpassDto implements OverpassPayload {
  @ApiProperty({ required: false })
  version: number;
  @ApiProperty({ required: false })
  generator: string;
  @ApiProperty({ required: false })
  osm3s: Osm3s;
  @ApiProperty({
    required: false,
    type: 'array',
    items: {
      anyOf: [
        { $ref: getSchemaPath(OsmNode) },
        { $ref: getSchemaPath(OsmWay) },
        { $ref: getSchemaPath(OsmRelation) },
      ],
    },
  })
  elements: (OsmNode | OsmWay | OsmRelation)[];
}
