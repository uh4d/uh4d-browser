import {
  IsArray,
  IsDefined,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { OverpassPayload } from '@uh4d/dto/interfaces/custom/osm';

class OsmItem {
  @IsNotEmpty()
  @IsNumber()
  id: number;
  @IsNotEmpty()
  @IsString()
  type: 'node' | 'way' | 'relation';
}

class OsmNode extends OsmItem {
  @IsNotEmpty()
  @IsString()
  override type: 'node';
  @IsNotEmpty()
  @IsNumber()
  lat: number;
  @IsNotEmpty()
  @IsNumber()
  lon: number;
}

class OsmTags {
  @IsOptional()
  @IsString()
  building?: string;
  @IsOptional()
  @IsString()
  'building:levels'?: string;
  @IsOptional()
  @IsString()
  height?: string;
  @IsOptional()
  @IsString()
  'roof:shape'?: string;
  [key: string]: string | undefined;
}

class OsmWay extends OsmItem {
  @IsNotEmpty()
  @IsString()
  override type: 'way';
  @IsNotEmpty()
  @IsArray()
  @IsNumber({}, { each: true })
  nodes: number[];
  @IsOptional()
  @ValidateNested()
  @Type(() => OsmTags)
  tags?: OsmTags;
}

class OsmMember {
  @IsNotEmpty()
  @IsString()
  type: string;
  @IsNotEmpty()
  @IsNumber()
  ref: number;
  @IsDefined()
  @IsString()
  role: 'outer' | 'inner' | 'part' | string;
}

class OsmRelation extends OsmItem {
  @IsNotEmpty()
  @IsString()
  override type: 'relation';
  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => OsmMember)
  members: OsmMember[];
  @IsOptional()
  @ValidateNested()
  @Type(() => OsmTags)
  tags?: OsmTags;
}

export class OverpassPayloadDto implements OverpassPayload {
  version: number;
  generator: string;
  @IsNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => OsmItem, {
    discriminator: {
      property: 'type',
      subTypes: [
        { value: OsmNode, name: 'node' },
        { value: OsmWay, name: 'way' },
        { value: OsmRelation, name: 'relation' },
      ],
    },
    keepDiscriminatorProperty: true,
  })
  elements: (OsmNode | OsmWay | OsmRelation)[];
}
