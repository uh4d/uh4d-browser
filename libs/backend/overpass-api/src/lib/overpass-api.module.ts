import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { OverpassApiService } from './overpass-api.service';

@Module({
  imports: [HttpModule],
  providers: [OverpassApiService],
  exports: [OverpassApiService],
})
export class OverpassApiModule {}
