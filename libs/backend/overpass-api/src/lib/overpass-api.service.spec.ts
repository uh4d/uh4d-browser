import { Test, TestingModule } from '@nestjs/testing';
import { OverpassApiService } from './overpass-api.service';

describe('OverpassApiService', () => {
  let service: OverpassApiService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OverpassApiService],
    }).compile();

    service = module.get<OverpassApiService>(OverpassApiService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
