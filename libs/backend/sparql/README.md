# backend-sparql

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test backend-sparql` to execute the unit tests via [Jest](https://jestjs.io).
