export interface SparqlResults<T extends { [key: string]: unknown }> {
  head: {
    vars: (keyof T)[];
  };
  results: {
    bindings: {
      [K in keyof T]: {
        type: string;
        value: T[K];
        'xml:lang'?: string;
      };
    }[];
  };
}
