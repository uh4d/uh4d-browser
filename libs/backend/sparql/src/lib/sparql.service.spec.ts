import { Test } from '@nestjs/testing';
import { SparqlService } from './sparql.service';

describe('SparqlService', () => {
  let service: SparqlService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [SparqlService],
    }).compile();

    service = module.get(SparqlService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
