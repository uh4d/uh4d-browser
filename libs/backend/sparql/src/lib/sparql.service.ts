import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { SparqlResults } from './sparql.types';

@Injectable()
export class SparqlService {
  constructor(private readonly http: HttpService) {}

  /**
   * Query Wikidata SPARQL endpoint.
   */
  async queryWikidata<T extends { [key: string]: unknown }>(sparql: string) {
    const response = await this.http.axiosRef.get<SparqlResults<T>>(
      'https://query.wikidata.org/sparql?query=' + encodeURIComponent(sparql),
      { headers: { Accept: 'application/sparql-results+json' } },
    );

    return response.data.results.bindings;
  }

  /**
   * Query Getty AAT SPARQL endpoint.
   */
  async queryGettyAAT<T extends { [key: string]: unknown }>(sparql: string) {
    const response = await this.http.axiosRef.get<SparqlResults<T>>(
      'https://vocab.getty.edu/sparql.json?query=' + encodeURIComponent(sparql),
      { headers: { Accept: 'application/sparql-results+json' } },
    );

    return response.data.results.bindings;
  }
}
