import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { Camera, Object3D, PerspectiveCamera, Vector2, Vector3 } from 'three';
import {
  GenericItem,
  ImageItem,
  PoiItem,
  VirtualProjectorCamera,
} from '@uh4d/three';
import { ImageDto, ObjectDto, Uh4dPoiDto } from '@uh4d/dto/interfaces';
import { TempObjectDto } from '@uh4d/dto/interfaces/custom/object';
import { FlyControls } from 'three/examples/jsm/controls/FlyControls';

type ViewportActionEvent =
  | {
      key:
        | 'animate'
        | 'align-north'
        | 'home-view'
        | 'cluster-update'
        | 'shading-update'
        | 'isolation-exit'
        | 'isolation-zoom'
        | 'preview-exit'
        | 'spatialize-abort'
        | 'spatialize-end'
        | 'poi-start'
        | 'poi-end'
        | 'screenshot';
    }
  | {
      key: 'cluster-enable';
      enabled: boolean;
    }
  | {
      key: 'osm-enable';
      enabled: boolean;
    }
  | {
      key: 'controls-set';
      type: string;
    }
  | {
      key: 'controls-config';
      config: Partial<Record<keyof FlyControls, number>>;
    }
  | {
      key: 'isolation-start';
      item: ImageItem;
    }
  | {
      key: 'preview-start';
      item: ImageItem;
    }
  | {
      key: 'projective-setup';
      item: ImageItem;
    }
  | {
      key: 'projective-start';
      item: ImageItem;
      camera: VirtualProjectorCamera;
    }
  | {
      key: 'projective-exit';
      camera: VirtualProjectorCamera;
    }
  | {
      key: 'spatialize-start';
      image: ImageDto;
      camera: PerspectiveCamera;
    }
  | {
      key: 'poi-form';
      object: ObjectDto | null;
      position: Vector3;
    }
  | {
      key: 'poi-open';
      poi: Uh4dPoiDto;
    }
  | {
      key: 'poi-camera';
      poi: PoiItem;
    }
  | {
      key: 'object-temp';
      file: TempObjectDto;
    }
  | {
      key: 'transform-start';
      object: Object3D;
      mode: 'new' | 'update';
    }
  | {
      key: 'transform-end';
      keepObject: boolean;
    }
  | {
      key: 'transform-update';
      object: ObjectDto;
    };

@Injectable({
  providedIn: 'root',
})
export class ViewportEventsService {
  callAction$ = new Subject<ViewportActionEvent>();
  cameraUpdate$ = new Subject<Camera>();
  tooltip$ = new Subject<{ item: GenericItem; position: Vector2 } | void>();
  contextmenu$ = new Subject<{ item: GenericItem; position: Vector2 } | void>();

  tourCollect$ = new BehaviorSubject<Uh4dPoiDto[] | null>(null);
}
