import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class GlobalRouteParamsService {
  map = new Map<string, string>();

  get(paramId: string): string | undefined {
    return this.map.get(paramId);
  }

  set(paramId: string, value: string) {
    this.map.set(paramId, value);
  }
}
