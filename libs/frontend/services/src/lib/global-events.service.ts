import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { VisualizationGradientConfig } from '@uh4d/frontend/visualization';
import { VisualizationType } from '@uh4d/config';
import { CameraDto, ImageDto, TextDto } from '@uh4d/dto/interfaces';

type GlobalEvent =
  | {
      key: 'spatialize';
      image: ImageDto;
    }
  | {
      key: 'visualization';
      type: VisualizationType;
    }
  | {
      key: 'gradient-update';
      config: VisualizationGradientConfig;
    }
  | {
      key: 'set-camera-view';
      camera: CameraDto;
    }
  | {
      key: 'camera-tween-complete';
    }
  | {
      key: 'load-temp-image';
      image: ImageDto;
    }
  | {
      key: 'other';
    };

@Injectable({
  providedIn: 'root',
})
export class GlobalEventsService {
  callAction$ = new Subject<GlobalEvent>();
  imageUpdate$ = new Subject<ImageDto | void>();
  textUpdate$ = new Subject<TextDto | void>();
}
