import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  BehaviorSubject,
  filter,
  lastValueFrom,
  map,
  of,
  ReplaySubject,
  switchMap,
  take,
} from 'rxjs';
import { formatISO, isAfter, isBefore } from 'date-fns';
import {
  DatesApiService,
  ImagesApiService,
  MapsApiService,
  ObjectsApiService,
  PoisApiService,
  TerrainApiService,
  TextsApiService,
} from '@uh4d/frontend/api-service';
import {
  ImageDto,
  MapDto,
  ObjectDto,
  SceneDto,
  TerrainDto,
  TextDto,
  Uh4dPoiDto,
} from '@uh4d/dto/interfaces';
import { SpecialModeService } from './special-mode.service';

interface ISearchParams {
  q: string[];
  from: string | null;
  to: string | null;
  undated: string | null;
  useAsTexture: string | null;
  model: string | null;
  map: string | null;
}

@Injectable({
  providedIn: 'root',
})
export class SearchManagerService {
  public images$ = new BehaviorSubject<ImageDto[]>([]);
  public texts$ = new BehaviorSubject<TextDto[]>([]);
  public models$ = new BehaviorSubject<ObjectDto[]>([]);
  public map$ = new ReplaySubject<MapDto | null>(1);
  public pois$ = new BehaviorSubject<Uh4dPoiDto[]>([]);
  public terrain$ = new ReplaySubject<string | TerrainDto>(1);
  public maps$ = new ReplaySubject<MapDto[] | null>(1);
  public dateExtent$ = new ReplaySubject<{ min: string; max: string } | null>(
    1,
  );

  public imagesLoading$ = new BehaviorSubject<boolean>(false);
  public textsLoading$ = new BehaviorSubject<boolean>(false);
  public modelsLoading$ = new BehaviorSubject<boolean>(false);

  private scene!: SceneDto;

  private params: ISearchParams = {
    q: [],
    from: null,
    to: null,
    undated: null,
    useAsTexture: null,
    model: null,
    map: null,
  };

  public params$ = new BehaviorSubject<ISearchParams>(this.params);

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private imagesApiService: ImagesApiService,
    private textsApiService: TextsApiService,
    private objectsApiService: ObjectsApiService,
    private datesApiService: DatesApiService,
    private poisApiService: PoisApiService,
    private terrainApiService: TerrainApiService,
    private mapsApiService: MapsApiService,
    private specialMode: SpecialModeService,
  ) {
    // listen to query parameters
    this.activatedRoute.queryParamMap.subscribe((params) => {
      // image parameters
      let imageParamsUpdated = false;
      let textParamsUpdated = false;
      let poiParamsUpdated = false;
      let modelParamsUpdated = false;

      const qParams = params.getAll('q');
      if (
        qParams.length !== this.params.q.length ||
        !qParams.every((value) => this.params.q.includes(value))
      ) {
        this.params.q = qParams;
        imageParamsUpdated = true;
        textParamsUpdated = true;
        poiParamsUpdated = true;
        modelParamsUpdated = true;
      }

      if (params.get('from') !== this.params.from) {
        this.params.from = params.get('from');
        imageParamsUpdated = true;
        textParamsUpdated = true;
      }
      if (params.get('to') !== this.params.to) {
        this.params.to = params.get('to');
        imageParamsUpdated = true;
        textParamsUpdated = true;
      }
      if (params.get('undated') !== this.params.undated) {
        this.params.undated = params.get('undated');
        imageParamsUpdated = true;
        textParamsUpdated = true;
      }
      if (params.get('useAsTexture') !== this.params.useAsTexture) {
        this.params.useAsTexture = params.get('useAsTexture');
        imageParamsUpdated = true;
      }

      // model parameters
      if (params.get('model') !== this.params.model) {
        this.params.model = params.get('model');
        modelParamsUpdated = true;
        poiParamsUpdated = true;
      }

      // map parameters
      let mapParamsUpdated = false;

      if (params.get('map') !== this.params.map) {
        this.params.map = params.get('map');
        mapParamsUpdated = true;
      }

      if (
        imageParamsUpdated ||
        textParamsUpdated ||
        modelParamsUpdated ||
        mapParamsUpdated
      ) {
        this.params$.next(this.params);
      }

      if (imageParamsUpdated) {
        this.queryImages();
      }

      if (textParamsUpdated) {
        this.queryTexts();
      }

      if (modelParamsUpdated) {
        this.queryModels();
      }

      if (poiParamsUpdated) {
        this.queryPois();
      }

      if (mapParamsUpdated) {
        this.maps$
          .pipe(
            filter((v): v is MapDto[] => !!v),
            take(1),
            map((maps) => maps.find((m) => m.date === this.params.map)),
          )
          .subscribe((mapData) => {
            this.map$.next(mapData || null);
          });
      }
    });
  }

  setSceneData(data: SceneDto) {
    this.scene = data;
    this.queryTerrain();
    this.queryDateExtent();
  }

  queryImages() {
    if (this.isExploreRoute()) {
      this.imagesLoading$.next(true);
      this.imagesApiService
        .query({
          lat: this.scene.latitude,
          lon: this.scene.longitude,
          r: 5000,
          q: this.params.q,
          from: this.params.from || undefined,
          to: this.params.to || undefined,
          undated: this.params.undated || undefined,
        })
        .subscribe((results) => {
          this.imagesLoading$.next(false);
          this.images$.next(results);
        });
    } else {
      // omit if not in /explore route
    }
  }

  queryTexts() {
    if (this.isExploreRoute()) {
      this.textsLoading$.next(true);
      this.textsApiService
        .query({
          lat: this.scene.latitude,
          lon: this.scene.longitude,
          r: 5000,
          q: this.params.q,
          from: this.params.from || undefined,
          to: this.params.to || undefined,
        })
        .subscribe((results) => {
          this.textsLoading$.next(false);
          this.texts$.next(results);
        });
    } else {
      // omit if not in /explore route
    }
  }

  queryModels() {
    if (this.isExploreRoute()) {
      this.modelsLoading$.next(true);
      this.objectsApiService
        .query({
          date: this.params.model || undefined,
          lat: this.scene.latitude,
          lon: this.scene.longitude,
          r: 5000,
          q: this.params.q,
        })
        .subscribe((results) => {
          this.modelsLoading$.next(false);
          this.models$.next(results);
        });
    } else {
      // omit if not in /explore route
    }
  }

  queryPois() {
    if (this.specialMode.inExhibitionMode) return;
    if (this.isExploreRoute()) {
      this.poisApiService
        .query({
          date: this.params.model || undefined,
          lat: this.scene.latitude,
          lon: this.scene.longitude,
          r: 5000,
          q: this.params.q,
        })
        .subscribe((results) => {
          this.pois$.next(results);
        });
    }
  }

  queryTerrain() {
    this.maps$.next(null);
    this.terrainApiService
      .query({
        lat: this.scene.latitude,
        lon: this.scene.longitude,
      })
      .pipe(
        switchMap((results) => {
          if (results[0]) {
            this.queryMaps(results[0].id);
            return of(results[0]);
          } else {
            this.maps$.next([]);
            return this.terrainApiService.requestElevationApi({
              lat: this.scene.latitude,
              lon: this.scene.longitude,
              r: 2500,
            });
          }
        }),
      )
      .subscribe((terrain) => {
        this.terrain$.next(terrain);
      });
  }

  queryMaps(terrainId: string) {
    this.mapsApiService.query(terrainId).subscribe((results) => {
      this.maps$.next(results);
    });
  }

  queryDateExtent() {
    this.dateExtent$.next(null);
    this.datesApiService
      .getExtent({
        lat: this.scene.latitude,
        lon: this.scene.longitude,
        r: 5000,
      })
      .subscribe((result) => {
        let min = formatISO(new Date(), { representation: 'date' });
        let max = '1900-01-01';
        if (result.images) {
          if (isBefore(result.images.min, min)) min = result.images.min;
          if (isAfter(result.images.max, max)) max = result.images.max;
        }
        if (result.objects) {
          if (isBefore(result.objects.min, min)) min = result.objects.min;
          if (isAfter(result.objects.max, max)) max = result.objects.max;
        }
        this.dateExtent$.next({ min, max });
      });
  }

  queryOSMFile(terrainOrPath: TerrainDto | string) {
    const terrain =
      typeof terrainOrPath === 'string' ? terrainOrPath : terrainOrPath.id;

    return lastValueFrom(
      this.terrainApiService.queryOSMFile({
        terrain,
        lat: this.scene.latitude,
        lon: this.scene.longitude,
        r: 1000,
      }),
    );
  }

  /**
   * Set query parameter for string search.
   */
  async setSearchParam(value: string[], add = false, remove = false) {
    let qParams = value;

    if (add) {
      qParams = this.params.q.concat(value);
    } else if (remove) {
      qParams = this.params.q.filter((q) => !value.includes(q));
    }

    // remove duplicates
    qParams = [...new Set(qParams)];

    if (
      qParams.length === this.params.q.length &&
      qParams.every((q) => this.params.q.includes(q))
    ) {
      return;
    }

    await this.router.navigate([this.getExploreRoute()], {
      queryParams: {
        q: qParams,
      },
      queryParamsHandling: 'merge',
    });
  }

  /**
   * Set query parameters for image dates.
   */
  async setImageDates(
    from: string,
    to: string,
    undated: boolean,
    useAsTexture: boolean,
  ) {
    return this.router.navigate([this.getExploreRoute()], {
      queryParams: {
        from,
        to,
        undated: undated ? '1' : '0',
        useAsTexture: useAsTexture ? '1' : undefined,
      },
      queryParamsHandling: 'merge',
    });
  }

  /**
   * Set query parameter for model date.
   */
  async setModelDate(date: string) {
    return this.router.navigate([this.getExploreRoute()], {
      queryParams: {
        model: date,
      },
      queryParamsHandling: 'merge',
    });
  }

  /**
   * Set query parameter for map date.
   */
  async setMapDate(date: string) {
    return this.router.navigate([this.getExploreRoute()], {
      queryParams: {
        map: date,
      },
      queryParamsHandling: 'merge',
    });
  }

  private isExploreRoute() {
    return /^\/explore/.test(this.router.url);
  }

  private getExploreRoute() {
    if (this.isExploreRoute()) {
      const index = this.router.url.indexOf('?');
      return index > -1 ? this.router.url.slice(0, index) : this.router.url;
    } else {
      return '/explore';
    }
  }
}
