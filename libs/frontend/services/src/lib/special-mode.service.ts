import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class SpecialModeService {
  public inExhibitionMode = false;

  constructor(private readonly route: ActivatedRoute) {
    this.route.queryParamMap.subscribe((params) => {
      const exhibition = params.get('exhibition');
      this.inExhibitionMode = exhibition === '1';
    });
  }
}
