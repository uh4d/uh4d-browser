import { CommonModule } from '@angular/common';
import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnDestroy,
} from '@angular/core';
import { combineLatest, ReplaySubject, Subject, takeUntil } from 'rxjs';
import {
  Circle,
  FeatureGroup,
  Map,
  MapOptions,
  Marker,
  tileLayer,
} from 'leaflet';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { Location2D } from '@uh4d/dto/interfaces/custom/spatial';
import { defaultIcon } from '../marker-icons';

/**
 * Interactive map to display and select GeoJson features.
 */
@Component({
  selector: 'uh4d-map-interactive',
  standalone: true,
  imports: [CommonModule, LeafletModule],
  templateUrl: './map-interactive.component.html',
  styleUrls: ['./map-interactive.component.sass'],
})
export class MapInteractiveComponent implements AfterViewInit, OnDestroy {
  options: MapOptions = {
    layers: [
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
      }),
    ],
    center: [0, 0],
    zoom: 15,
    attributionControl: false,
  };

  map: Map | null = null;

  showMap = false;

  private layers: FeatureGroup[] = [];
  // private selectedLayers: ResourceGeoJsonLayer[] = [];

  private items$ = new ReplaySubject<Location2D[]>(1);
  private mapReady$ = new ReplaySubject<boolean>(1);
  private itemsChanged$ = new Subject<void>();
  private unsubscribe$ = new Subject<void>();

  @Input({ required: true }) set location(value: Location2D | Location2D[]) {
    this.items$.next(Array.isArray(value) ? value : [value]);
  }

  // @Input() set selected(value: ProjectResourceEntity[]) {
  //   this.selectLayers(value);
  // }
  //
  // @Output() selectedChange = new EventEmitter<ProjectResourceEntity[]>();

  /**
   * Control if layers are selectable.
   * @default true
   */
  @Input() interactive = true;

  /**
   * Control how user can navigate the map.
   *
   * - `all` - allow dragging and zooming via controls and mouse wheel
   * - `noScroll` - disable mouse wheel zooming
   * - `none` - disallow any navigation, results in a static map
   * @default all
   */
  @Input() navigation: 'all' | 'none' | 'noScroll' = 'all';

  private resizeObserver = new ResizeObserver(() => {
    this.map?.invalidateSize();
  });

  constructor(private readonly elementRef: ElementRef<HTMLElement>) {}

  ngAfterViewInit(): void {
    switch (this.navigation) {
      case 'noScroll':
        this.options.scrollWheelZoom = false;
        break;
      case 'none':
        this.options.zoomControl = false;
        this.options.dragging = false;
        this.options.scrollWheelZoom = false;
        this.options.doubleClickZoom = false;
    }

    // init map just after the view has been initialized,
    // otherwise computed center is not at the center of the container
    setTimeout(() => {
      this.showMap = true;
    });

    this.resizeObserver.observe(this.elementRef.nativeElement);

    combineLatest({
      items: this.items$,
      ready: this.mapReady$,
    })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(({ items }) => {
        this.setupGeoJsonLayers(items);
      });
  }

  onMapReady(map: Map) {
    this.map = map;
    this.mapReady$.next(true);
  }

  private setupGeoJsonLayers(items: Location2D[]) {
    // clear old layers
    this.layers.forEach((layer) => {
      this.map?.removeLayer(layer);
    });
    this.layers = [];
    this.itemsChanged$.next();

    items.forEach((item) => {
      const layer =
        (item.radius || 0) > 0
          ? new Circle([item.latitude, item.longitude], {
              radius: item.radius ?? undefined,
              interactive: this.interactive,
            })
          : new Marker([item.latitude, item.longitude], { icon: defaultIcon });

      const layerGroup = new FeatureGroup([layer]);
      this.map?.addLayer(layerGroup);
      this.layers.push(layerGroup);

      // if (this.interactive) {
      //   fromEvent<LayerEvent>(layer, 'click')
      //     .pipe(takeUntil(this.itemsChanged$))
      //     .subscribe((event) => {
      //       this.onFeatureClick(event.target);
      //     });
      // }
    });

    this.fitBounds();
  }

  private fitBounds() {
    if (!this.map || this.layers.length === 0) return;

    const bounds = this.layers[0].getBounds();
    for (let i = 1, l = this.layers.length; i < l; i++) {
      bounds.extend(this.layers[i].getBounds());
    }

    this.map.fitBounds(bounds);
  }

  // private onFeatureClick(layer: FeatureGroup) {
  //   if (layer.userData.entity) {
  //     this.selectLayers([layer.userData.entity]);
  //     // this.selectedChange.emit([layer.userData.entity]);
  //   }
  // }
  //
  // private selectLayers(items: ProjectResourceEntity[]) {
  //   this.selectedLayers.forEach((layer) => {
  //     layer.highlight(false);
  //   });
  //   this.selectedLayers = [];
  //
  //   items.forEach((item) => {
  //     const layer = this.layers.find((l) => l.userData?.entity?.id === item.id);
  //     if (layer) {
  //       layer.highlight(true);
  //       this.selectedLayers.push(layer);
  //     }
  //   });
  // }

  ngOnDestroy() {
    this.resizeObserver.disconnect();
    this.items$.complete();
    this.mapReady$.complete();
    this.itemsChanged$.next();
    this.itemsChanged$.complete();
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
