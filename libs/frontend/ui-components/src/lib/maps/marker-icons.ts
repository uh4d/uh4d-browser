import { Icon, icon } from 'leaflet';

export const defaultIcon = icon({
  ...Icon.Default.prototype.options,
  iconUrl: 'assets/leaflet/marker-icon.png',
  iconRetinaUrl: 'assets/leaflet/marker-icon-2x.png',
  shadowUrl: 'assets/leaflet/marker-shadow.png',
});

export const redIcon = icon({
  ...Icon.Default.prototype.options,
  iconUrl: 'assets/leaflet/marker-icon-red.png',
  shadowUrl: 'assets/leaflet/marker-shadow.png',
});
