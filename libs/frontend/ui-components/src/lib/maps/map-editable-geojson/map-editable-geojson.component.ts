import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  Circle,
  Control,
  DomEvent,
  DomUtil,
  geoJSON,
  LatLng,
  latLng,
  Layer,
  LayerEvent,
  LayerGroup,
  LeafletEvent,
  LeafletMouseEvent,
  Map,
  MapOptions,
  tileLayer,
} from 'leaflet';
import 'leaflet-editable';
import {
  debounceTime,
  fromEvent,
  merge,
  ReplaySubject,
  Subject,
  takeUntil,
} from 'rxjs';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { PointFeature } from '@uh4d/utils';

/**
 * Interactive map to create new GeoJson features.
 * Supported features: circle.
 */
@Component({
  selector: 'uh4d-map-editable-geojson',
  standalone: true,
  imports: [CommonModule, LeafletModule],
  templateUrl: './map-editable-geojson.component.html',
  styleUrls: ['./map-editable-geojson.component.sass'],
})
export class MapEditableGeojsonComponent
  implements OnInit, AfterViewInit, OnDestroy
{
  /**
   * Map zoom level.
   */
  @Input() zoom = 12;

  private geoJson$ = new ReplaySubject<PointFeature[]>(1);

  @Input() set geoJson(value: PointFeature[]) {
    this.geoJson$.next(value);
  }

  @Output() geoJsonChange = new EventEmitter<PointFeature[]>();

  options: MapOptions = {
    layers: [
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
      }),
    ],
    zoom: 15,
    editable: true,
    attributionControl: false,
  };

  map: Map | null = null;

  showMap = false;

  tooltipStyle = {
    left: 0,
    top: 0,
    visible: false,
  };
  tooltipMessage = '';

  private unsubscribe$ = new Subject<void>();

  constructor(private readonly cdr: ChangeDetectorRef) {}

  ngOnInit() {
    this.setMapPosition();
  }

  ngAfterViewInit(): void {
    // init map just after the view has been initialized,
    // otherwise computed center is not at the center of the container
    setTimeout(() => {
      this.showMap = true;
    });
  }

  onMapReady(map: Map) {
    this.map = map;

    this.geoJson$.pipe(takeUntil(this.unsubscribe$)).subscribe((value) => {
      this.setGeoJson(value);
    });

    // listen to important events
    fromEvent<LayerEvent>(this.map, 'editable:created')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((event) => {
        this.prepareLayer(event.layer);
      });
    merge(
      fromEvent<LeafletEvent>(this.map, 'editable:editing'),
      fromEvent<LeafletEvent>(this.map, 'editable:drawing:commit'),
    )
      .pipe(takeUntil(this.unsubscribe$), debounceTime(500))
      .subscribe(() => {
        this.toGeoJson();
      });

    // events for displaying tooltip
    fromEvent<LeafletMouseEvent>(this.map, 'mousemove')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((event) => {
        this.tooltipStyle.left = event.containerPoint.x + 20;
        this.tooltipStyle.top = event.containerPoint.y - 10;
        if (this.tooltipStyle.visible) {
          this.cdr.detectChanges();
        }
      });
    fromEvent<LayerEvent>(this.map, 'editable:drawing:start')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((event) => {
        this.tooltipStyle.visible = true;
        this.updateTooltipMessage(event.layer);
      });
    fromEvent<LayerEvent>(this.map, 'editable:drawing:end')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        this.tooltipStyle.visible = false;
      });
    fromEvent<LayerEvent>(this.map, 'editable:drawing:clicked')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((event) => {
        this.updateTooltipMessage(event.layer);
      });

    // add controls
    const circleControl = Control.extend({
      options: {
        position: 'topleft',
      },
      onAdd: () => {
        const container = DomUtil.create('div', 'leaflet-control leaflet-bar'),
          link = DomUtil.create('a', '', container);

        link.href = '#';
        link.title = 'Create new circle';
        link.innerHTML = '⬤';
        DomEvent.on(link, 'click', DomEvent.stop).on(link, 'click', () => {
          this.map?.editTools.startCircle();
        });

        return container;
      },
    });
    this.map.addControl(new circleControl());

    // const polygonControl = Control.extend({
    //   options: {
    //     position: 'topleft',
    //   },
    //   onAdd: () => {
    //     const container = DomUtil.create('div', 'leaflet-control leaflet-bar'),
    //       link = DomUtil.create('a', '', container);
    //
    //     link.href = '#';
    //     link.title = 'Create a new polygon';
    //     link.innerHTML = '▰';
    //     DomEvent.on(link, 'click', DomEvent.stop).on(link, 'click', () => {
    //       this.map?.editTools.startPolygon();
    //     });
    //
    //     return container;
    //   },
    // });
    // this.map.addControl(new polygonControl());
  }

  private setMapPosition(): void {
    // set default map location to Dresden
    const lat = 51.049329;
    const lon = 13.738144;

    // set center and zoom
    if (!this.map) {
      this.options.center = latLng(lat, lon);
      this.options.zoom = this.zoom;
    } else {
      this.map.setView(latLng(lat, lon));
    }
  }

  private prepareLayer(layer: Layer) {
    fromEvent<LeafletMouseEvent>(layer, 'click')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((event) => {
        this.onLayerClick(event);
      });
  }

  private onLayerClick(event: LeafletMouseEvent) {
    // remove layer on CTRL+click
    if (event.originalEvent.ctrlKey || event.originalEvent.metaKey) {
      // @ts-expect-error missing type declarations
      const fl = this.map?.editTools.featuresLayer;
      fl.removeLayer(event.target);

      this.toGeoJson();
    }
  }

  private updateTooltipMessage(layer: Layer) {
    if (layer instanceof Circle) {
      this.tooltipMessage = 'Click on the map to draw a circle.';
    } else {
      this.tooltipMessage = 'unknown';
    }
  }

  private setGeoJson(features: PointFeature[]) {
    this.map?.editTools.stopDrawing();

    // remove any old features
    // @ts-expect-error missing type declarations
    const fl = this.map?.editTools.featuresLayer as LayerGroup;
    const layers = fl.getLayers();
    layers.forEach((layer) => {
      fl.removeLayer(layer);
    });

    // add new features
    features.forEach((feature) => {
      geoJSON(feature, {
        pointToLayer: (geoJsonPoint: PointFeature, latlng: LatLng): Layer => {
          const layer = new Circle(latlng, geoJsonPoint.properties.radius ?? 0);
          layer.addTo(fl);
          layer.enableEdit();
          this.prepareLayer(layer);
          return layer;
        },
      });
    });
    this.fitBounds();
  }

  private toGeoJson() {
    // @ts-expect-error missing type declarations
    const fl = this.map?.editTools.featuresLayer as LayerGroup;
    const layers = fl.getLayers();

    const features: PointFeature[] = [];

    layers.forEach((l) => {
      // @ts-expect-error missing type declarations
      const isDrawing = l.editor.drawing();
      // skip if not yet finished
      if (isDrawing) return;

      if (!(l instanceof Circle)) return;

      const geoJson = l.toGeoJSON() as PointFeature;

      const r = l.getRadius();
      // workaround: skip if not yet drawn by user (start radius = 10)
      if (r === 10) return;
      if (!geoJson.properties) geoJson.properties = {};
      geoJson.properties['radius'] = r;

      features.push(geoJson);
    });

    this.geoJsonChange.emit(features);
  }

  private fitBounds() {
    if (!this.map) return;
    // @ts-expect-error missing type declarations
    const fl = this.map?.editTools.featuresLayer as LayerGroup;
    const layers = fl.getLayers() as Circle[];

    if (layers.length === 0) return;

    const bounds = layers[0].getBounds();
    for (let i = 1, l = layers.length; i < l; i++) {
      bounds.extend(layers[i].getBounds());
    }

    this.map.fitBounds(bounds);
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
