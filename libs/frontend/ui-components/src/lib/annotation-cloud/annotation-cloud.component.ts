import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import * as d3 from 'd3';
import * as d3Cloud from 'd3-cloud';
import {
  ImageAnnotationDto,
  NormDataNodeDto,
  ObjectAnnotationDto,
  TextAnnotationDto,
} from '@uh4d/dto/interfaces';
import { stringToColor } from '@uh4d/utils';
import { ContextMenuService } from '@uh4d/frontend/context-menu';

type IAnnotation = ImageAnnotationDto | TextAnnotationDto | ObjectAnnotationDto;

type IWord = Required<Pick<d3Cloud.Word, 'text' | 'size'>> &
  Pick<d3Cloud.Word, 'x' | 'y'> & { id: string; color: string };

@Component({
  selector: 'uh4d-annotation-cloud',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './annotation-cloud.component.html',
  styleUrls: ['./annotation-cloud.component.sass'],
})
export class AnnotationCloudComponent {
  identifiers = new Map<string, NormDataNodeDto & { count: number }>();

  @ViewChild('svgRoot', { static: true }) svgRoot!: ElementRef<SVGSVGElement>;

  @Input() set annotations(value: IAnnotation[]) {
    this.extractIdentifier(value);
    this.computeCloud();
  }

  constructor(
    private readonly contextMenuService: ContextMenuService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
  ) {}

  private extractIdentifier(data: IAnnotation[]): void {
    this.identifiers.clear();
    data.forEach((ann) => {
      ann.wikidata.forEach((wiki) => {
        const identifierObj = this.identifiers.get(wiki.id);
        if (identifierObj) {
          identifierObj.count++;
        } else {
          this.identifiers.set(wiki.id, { ...wiki, count: 1 });
        }
      });
    });
  }

  private async computeCloud() {
    await new Promise((resolve) => setTimeout(resolve, 500));

    const maxSize = [...this.identifiers.values()].reduce(
      (maxCount, id) => Math.max(maxCount, id.count),
      0,
    );
    const containerWidth = d3
      .select(this.svgRoot.nativeElement)
      .node()!
      .getBoundingClientRect().width;

    const layout = d3Cloud<IWord>()
      .size([containerWidth || 400, 600])
      .words(
        [...this.identifiers.values()].map((id) => {
          return {
            id: id.id,
            text: `${id.label} (${id.count})`,
            size: 10 + ((30 - 10) * id.count) / maxSize,
            color: stringToColor(id.identifier + id.label, true),
          };
        }),
      )
      .padding(5)
      .rotate(0)
      // .spiral('rectangular')
      .fontSize((d) => d.size!)
      .on('end', (words) => this.draw(words));
    layout.start();
  }

  private draw(words: IWord[]) {
    const dimensionY = words.reduce(
      ({ min, max }, word) => ({
        min: Math.min(min, word.y! - word.size / 2),
        max: Math.max(max, word.y! + word.size / 2),
      }),
      { min: 0, max: 0 },
    );

    const svg = d3.select(this.svgRoot.nativeElement);
    const container = svg.select<SVGGElement>('g');
    const { width, height } = svg.node()!.getBoundingClientRect();
    const updatedHeight = -dimensionY.min + dimensionY.max + 10;

    svg
      .transition()
      .duration(0)
      .delay(updatedHeight < height ? 1000 : 0)
      .attr('height', updatedHeight);
    container
      .transition()
      .duration(container.attr('transform') ? 1000 : 0)
      .attr('transform', `translate(${width / 2},${-dimensionY.min + 5})`);

    const texts = container
      .selectAll<SVGTextElement, IWord>('text')
      .data(words, (d) => d.id);
    texts
      .enter()
      .append('text')
      .attr('id', (d) => d.id)
      .attr('transform', (d) => `translate(${d.x},${d.y})`)
      .style('fill', (d) => d.color)
      .attr('font-size', 1)
      .attr('fill-opacity', 1e-6)
      .attr('text-anchor', 'middle')
      .on('contextmenu', (event, d) => this.rightClickWord(event, d))
      .text((d) => d.text)
      .transition()
      .duration(500)
      .attr('font-size', (d) => d.size)
      .attr('fill-opacity', 1);

    texts
      .transition()
      .duration(1000)
      .attr('transform', (d) => `translate(${d.x},${d.y})`)
      .attr('font-size', (d) => d.size)
      .text((d) => d.text);

    texts
      .exit()
      .transition()
      .duration(500)
      .attr('font-size', 1)
      .attr('fill-opacity', 1e-6)
      .remove();
  }

  private rightClickWord(event: MouseEvent, d: IWord): void {
    this.contextMenuService.open(event, {
      title: d.text,
      actions: [
        {
          icon: 'info-circle',
          label: 'Open details...',
          click: () => {
            this.router.navigate(
              [
                '/explore',
                this.route.snapshot.paramMap.get('scene'),
                'normdata',
                d.id,
              ],
              {
                queryParamsHandling: 'preserve',
              },
            );
          },
        },
      ],
    });
  }
}
