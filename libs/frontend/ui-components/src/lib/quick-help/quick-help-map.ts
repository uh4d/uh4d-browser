// html property to build path to file: `assets/help/<item.html>.html`
export interface QuickHelpItem {
  title: string;
  html: string;
}

export const QuickHelpMap = new Map<string, QuickHelpItem>()
  .set('search-bar', { title: 'Search', html: 'search-bar' })
  .set('time-slider', { title: 'Time Slider', html: 'time-slider' })
  .set('navigation', { title: 'Navigation', html: 'navigation' })
  .set('cluster-distance', {
    title: 'Cluster Distance',
    html: 'cluster-distance',
  });
