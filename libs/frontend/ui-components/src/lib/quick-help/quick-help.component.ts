import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import { QuickHelpItem, QuickHelpMap } from './quick-help-map';

@Component({
  selector: 'uh4d-quick-help',
  standalone: true,
  imports: [CommonModule, Uh4dIconsModule, PopoverModule],
  templateUrl: './quick-help.component.html',
  styleUrls: ['./quick-help.component.sass'],
})
export class QuickHelpComponent implements OnInit {
  @Input({ required: true }) key!: string;

  help!: QuickHelpItem;
  content = '';

  constructor(private readonly http: HttpClient) {}

  ngOnInit(): void {
    if (!this.key) {
      throw new Error('No key specified for uh4d-quick-help');
    }

    this.help = QuickHelpMap.get(this.key)!; // ToDo: proper type key
  }

  onShown() {
    this.http
      .get(`assets/help/${this.help.html}.html`, { responseType: 'text' })
      .subscribe({
        next: (html) => {
          this.content = html;
        },
        error: (error) => {
          console.error(error);
        },
      });
  }
}
