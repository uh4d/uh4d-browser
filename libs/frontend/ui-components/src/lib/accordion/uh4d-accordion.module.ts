import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CdkAccordionModule } from '@angular/cdk/accordion';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import { AccordionItemComponent } from './accordion-item.component';

@NgModule({
  declarations: [AccordionItemComponent],
  imports: [CommonModule, CdkAccordionModule, Uh4dIconsModule],
  exports: [CdkAccordionModule, AccordionItemComponent],
})
export class Uh4dAccordionModule {}
