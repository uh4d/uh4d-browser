import { Component, Input } from '@angular/core';
import { CDK_ACCORDION, CdkAccordionItem } from '@angular/cdk/accordion';

@Component({
  selector: 'uh4d-accordion-item',
  templateUrl: './accordion-item.component.html',
  styleUrls: ['./accordion-item.component.sass'],
  exportAs: 'uh4dAccordionItem',
  providers: [{ provide: CDK_ACCORDION, useValue: undefined }],
})
export class AccordionItemComponent extends CdkAccordionItem {
  @Input() label = '';
}
