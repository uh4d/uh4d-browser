import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import { UserEventDto } from '@uh4d/dto/interfaces';

@Component({
  selector: 'uh4d-record-user-metadata',
  standalone: true,
  imports: [CommonModule, TooltipModule, Uh4dIconsModule],
  templateUrl: './record-user-metadata.component.html',
  styleUrls: ['./record-user-metadata.component.sass'],
})
export class RecordUserMetadataComponent {
  @Input() metadata: {
    createdBy?: UserEventDto | null;
    uploadedBy?: UserEventDto | null;
    editedBy?: UserEventDto | null;
    pending?: boolean | null;
    declined?: boolean | null;
  } = {};

  @Input()
  alignment: 'left' | 'center' | 'right' = 'left';
}
