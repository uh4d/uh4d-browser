import { ImageDateDto, UserEventDto } from '@uh4d/dto/interfaces';

export interface RecordItem {
  id: string;
  title: string;
  date?: ImageDateDto | null;
  author?: string;
  authors?: string[];
  file?: {
    path: string;
    thumb: string;
  };
  placeholder?: string;
  spatialStatus?: number;
  annotationsAvailable?: boolean;
  relevance?: number;
  uploadedBy?: UserEventDto | null;
  createdBy?: UserEventDto | null;
  pending?: boolean | null;
  declined?: boolean | null;
}

export const recordTypes = ['image', 'text', 'object', 'poi'] as const;
export type RecordType = (typeof recordTypes)[number];

export const validationStates = ['accepted', 'pending', 'declined'] as const;
export type ValidationStatus = (typeof validationStates)[number];
