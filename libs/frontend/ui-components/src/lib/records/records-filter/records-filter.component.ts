import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { Subject, takeUntil } from 'rxjs';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';

export interface RecordsFilterValue {
  sortBy: 'title' | 'author' | 'date' | 'relevance';
  sortDesc: boolean;
  itemsPerPage: 20 | 50 | 100;
  listStyle: 'card' | 'list';
  filterSpatialized: boolean;
  filterAnnotated: boolean;
}

@Component({
  selector: 'uh4d-records-filter',
  standalone: true,
  imports: [
    CommonModule,
    BsDropdownModule,
    ButtonsModule,
    ReactiveFormsModule,
    Uh4dIconsModule,
  ],
  templateUrl: './records-filter.component.html',
  styleUrls: ['./records-filter.component.sass'],
})
export class RecordsFilterComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();

  /**
   * Form group to control property to sort, items per page, and list style.
   */
  form = new FormGroup({
    sortBy: new FormControl<string>('date'),
    sortDesc: new FormControl<boolean>(false),
    itemsPerPage: new FormControl<number>(20),
    listStyle: new FormControl<string>('card'),
    filterSpatialized: new FormControl<boolean>(false),
    filterAnnotated: new FormControl<boolean>(false),
  });

  @Input() headerExtraTpl: TemplateRef<any> | null = null;

  @Input() extraFilter = false;

  @Input() set filterValue(value: Partial<typeof this.form.value>) {
    this.form.patchValue(value, { emitEvent: false });
  }

  @Output() filterValueChange = new EventEmitter<typeof this.form.value>();

  ngOnInit() {
    this.form.valueChanges
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((value) => {
        this.filterValueChange.emit(value);
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
