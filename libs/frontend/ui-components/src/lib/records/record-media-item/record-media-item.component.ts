import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxCutModule } from 'ngx-cut';
import { FormatImageDatePipe } from '@uh4d/frontend/pipes';
import { RecordCardItemComponent } from '../record-card-item/record-card-item.component';

@Component({
  selector: 'uh4d-record-media-item',
  standalone: true,
  imports: [CommonModule, NgxCutModule, FormatImageDatePipe],
  templateUrl: './record-media-item.component.html',
  styleUrls: ['./record-media-item.component.sass'],
})
export class RecordMediaItemComponent extends RecordCardItemComponent {}
