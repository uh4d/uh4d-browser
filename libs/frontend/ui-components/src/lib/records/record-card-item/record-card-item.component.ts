import { Component, Input, TemplateRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxCutModule } from 'ngx-cut';
import { FormatImageDatePipe } from '@uh4d/frontend/pipes';
import { RecordItem } from '../records.types';

@Component({
  selector: 'uh4d-record-card-item',
  standalone: true,
  imports: [CommonModule, NgxCutModule, FormatImageDatePipe],
  templateUrl: './record-card-item.component.html',
  styleUrls: ['./record-card-item.component.sass'],
})
export class RecordCardItemComponent<T extends RecordItem = RecordItem> {
  @Input({ required: true }) item!: T;

  @Input() cardExtraTpl: TemplateRef<{ $implicit: T }> | null = null;

  @Input() showMetadataDate = false;

  @Input() similarity?: number;

  printAuthor(item: T): string | undefined {
    return item.authors ? item.authors.join(', ') : item.author;
  }

  imageUrl(): string {
    if (this.item.file?.thumb)
      return 'data/' + this.item.file.path + this.item.file.thumb;
    if (this.item.placeholder) return this.item.placeholder;
    return '';
  }
}
