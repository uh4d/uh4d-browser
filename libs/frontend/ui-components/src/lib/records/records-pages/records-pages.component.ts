import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { resolveProperty, stringCompare } from '@uh4d/utils';
import {
  RecordsFilterComponent,
  RecordsFilterValue,
} from '../records-filter/records-filter.component';
import { RecordCardItemComponent } from '../record-card-item/record-card-item.component';
import { RecordMediaItemComponent } from '../record-media-item/record-media-item.component';
import { RecordItem } from '../records.types';

/**
 * Component that lists all queried image/text items.
 * The user can choose the list style, the number of entries per page, and the property to sort the entries.
 * A pagination lets users navigate through the list.
 *
 * ![Screenshot](../assets/image-list.png)
 */
@Component({
  selector: 'uh4d-records-pages',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    FontAwesomeModule,
    PaginationModule,
    RecordsFilterComponent,
    RecordCardItemComponent,
    RecordMediaItemComponent,
  ],
  templateUrl: './records-pages.component.html',
  styleUrls: ['./records-pages.component.sass'],
})
export class RecordsPagesComponent<T extends RecordItem = RecordItem> {
  @Input({ required: true }) set records(value: T[]) {
    this.itemsRaw = value;
    this.sortItems();
  }

  @Input() set loading(value: boolean) {
    this.isLoading = value;
  }

  @Input() loadingText = 'Querying...';

  @Input() extraFilter = false;

  @Input() showMetadataDate = false;

  @Input() cardsAlignment: 'start' | 'end' | 'between' = 'between';

  @Input() headerExtraTpl: TemplateRef<any> | null = null;

  @Input() recordExtraTpl?: TemplateRef<{ $implicit: T }>;

  @Output() recordClick = new EventEmitter<T>();

  /**
   * Image entries as received from the API.
   */
  itemsRaw: T[] = [];
  /**
   * Image entries sorted and filtered by {@link #filter filter} and pagination.
   */
  itemsSorted: T[] = [];

  /**
   * Current pagination page.
   */
  currentPage = 1;

  /**
   * Flag indicating if application is currently querying API for items.
   */
  isLoading = false;

  filter: RecordsFilterValue = {
    sortBy: 'date',
    sortDesc: false,
    itemsPerPage: 20,
    listStyle: 'card',
    filterSpatialized: false,
    filterAnnotated: false,
  };

  /**
   * Reference to container element of the list.
   */
  @ViewChild('listBodyElement', { static: true }) listBodyElement!: ElementRef;

  /**
   * Sort {@link #itemsRaw itemsRaw} considering {@link #filter filter} and {@link #currentPage currentPage}.
   */
  sortItems(): void {
    let copy = this.itemsRaw.slice(0);

    // filter items
    copy = copy.filter(
      (item) =>
        (this.filter.filterSpatialized
          ? item.spatialStatus && item.spatialStatus >= 3
          : true) &&
        (this.filter.filterAnnotated ? item.annotationsAvailable : true),
    );

    // set compare order according to sortBy
    let compareOrder: string[] = [];
    switch (this.filter.sortBy) {
      case 'relevance':
        compareOrder = ['relevance', 'date.from', 'author', 'title'];
        break;
      case 'author':
        compareOrder = ['author', 'date.from', 'title'];
        break;
      case 'title':
        compareOrder = ['title', 'date.from', 'author'];
        break;
      case 'date':
        compareOrder = ['date.from', 'author', 'title'];
    }

    // perform sorting
    copy.sort((a, b) => {
      for (const compareProp of compareOrder) {
        if (compareProp === 'relevance') {
          if (!a.relevance || !b.relevance) continue;
          return b.relevance - a.relevance;
        }
        const result = stringCompare(
          resolveProperty<string>(a, compareProp),
          resolveProperty<string>(b, compareProp),
        );
        if (result !== 0) {
          return result;
        }
      }
      return 0;
    });

    if (this.filter.sortDesc) {
      copy.reverse();
    }

    // filter list according to itemsPerPage and currentPage
    this.itemsSorted = copy.slice(
      (this.currentPage - 1) * this.filter.itemsPerPage,
      this.currentPage * this.filter.itemsPerPage,
    );
  }

  /**
   * Pagination change event listener.
   * Scroll list container back to top and update {@link #itemsSorted itemsSorted}.
   */
  onPageChange(): void {
    this.listBodyElement.nativeElement.scrollTop = 0;

    setTimeout(() => {
      this.sortItems();
    });
  }
}
