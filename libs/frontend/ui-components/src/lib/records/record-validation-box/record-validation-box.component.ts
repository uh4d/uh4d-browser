import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import { RecordItem } from '../records.types';

@Component({
  selector: 'uh4d-record-validation-box',
  standalone: true,
  imports: [CommonModule, Uh4dIconsModule],
  templateUrl: './record-validation-box.component.html',
  styleUrls: ['./record-validation-box.component.sass'],
})
export class RecordValidationBoxComponent {
  @Input() record!: Pick<RecordItem, 'uploadedBy' | 'pending' | 'declined'>;

  @Input() isLoading = false;

  @Output() validate = new EventEmitter<boolean>();
}
