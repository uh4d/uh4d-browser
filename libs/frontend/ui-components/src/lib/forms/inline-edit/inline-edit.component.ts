import {
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable, Observer, switchMap } from 'rxjs';
import { NotificationsService } from 'angular2-notifications';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import { AbstractValueAccessor } from '../abstract-value-accessor';

@Component({
  selector: 'uh4d-inline-edit',
  standalone: true,
  imports: [CommonModule, FormsModule, TypeaheadModule, Uh4dIconsModule],
  templateUrl: './inline-edit.component.html',
  styleUrls: ['./inline-edit.component.sass'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InlineEditComponent),
      multi: true,
    },
  ],
})
export class InlineEditComponent
  extends AbstractValueAccessor<string>
  implements OnInit
{
  @Input() placeholder = 'unknown';
  @Input() type = 'text';
  @Input() pattern = '';
  @Input() enabled = true;
  @Input() suggestionsFn?: (query: string) => Observable<string[]>;
  @Output() saved = new EventEmitter<string>();

  @ViewChild('parentElement', { static: false })
  inputParentElement?: ElementRef;

  private preValue = '';

  editing = false;
  suggestions$?: Observable<string[]>;

  constructor(private notify: NotificationsService) {
    super();
  }

  ngOnInit(): void {
    if (this.suggestionsFn) {
      this.suggestions$ = new Observable((observer: Observer<string>) => {
        observer.next(this.value);
      }).pipe(switchMap(this.suggestionsFn));
    }
  }

  edit() {
    this.preValue = this.value;
    this.editing = true;
    setTimeout(() => {
      this.inputParentElement?.nativeElement.children[0].focus();
    });
  }

  save() {
    if (this.pattern) {
      if (!new RegExp(this.pattern).test(this.value)) {
        this.notify.warn('Input does not match pattern: ' + this.pattern);
        return;
      }
    }

    this.saved.emit(this.value);
    this.editing = false;
  }

  cancel() {
    this.value = this.preValue;
    this.editing = false;
  }
}
