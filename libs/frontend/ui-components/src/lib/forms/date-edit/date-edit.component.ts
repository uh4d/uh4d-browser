import {
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import { FormatImageDatePipe } from '@uh4d/frontend/pipes';
import { ImageDateDto } from '@uh4d/dto/interfaces';
import { AbstractValueAccessor } from '../abstract-value-accessor';

@Component({
  selector: 'uh4d-date-edit',
  standalone: true,
  imports: [CommonModule, FormsModule, Uh4dIconsModule, FormatImageDatePipe],
  templateUrl: './date-edit.component.html',
  styleUrls: ['./date-edit.component.sass'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DateEditComponent),
      multi: true,
    },
  ],
})
export class DateEditComponent extends AbstractValueAccessor<ImageDateDto> {
  @Input() placeholder = 'unknown';
  @Input() enabled = true;
  @Output() saved = new EventEmitter<ImageDateDto>();

  @ViewChild('parentElement', { static: false })
  inputParentElement?: ElementRef;

  private preValue = '';

  editing = false;

  edit() {
    if (!this.value) {
      this.value = { value: '' } as ImageDateDto;
    }
    this.preValue = this.value.value;
    this.editing = true;
    setTimeout(() => {
      this.inputParentElement?.nativeElement.children[0].focus();
    });
  }

  save() {
    this.saved.emit(this.value);
    this.editing = false;
  }

  cancel() {
    this.value.value = this.preValue;
    this.editing = false;
  }
}
