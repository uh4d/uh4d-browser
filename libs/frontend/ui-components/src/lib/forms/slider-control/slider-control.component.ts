import {
  Component,
  forwardRef,
  Input,
  OnInit,
  TemplateRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, NG_VALUE_ACCESSOR } from '@angular/forms';
import { nanoid } from 'nanoid';
import { AbstractValueAccessor } from '../abstract-value-accessor';

@Component({
  selector: 'uh4d-slider-control',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './slider-control.component.html',
  styleUrls: ['./slider-control.component.sass'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SliderControlComponent),
      multi: true,
    },
  ],
})
export class SliderControlComponent
  extends AbstractValueAccessor<number>
  implements OnInit
{
  id = '';

  @Input() identifier?: string;
  @Input() label?: string;
  @Input() min = 0;
  @Input() max = 100;
  @Input() step = 5;
  @Input() left?: TemplateRef<any>;
  @Input() right?: TemplateRef<any>;
  @Input() type: 'inline' | 'default' = 'default';

  ngOnInit(): void {
    this.id = this.identifier || 'id_' + nanoid(9);
  }
}
