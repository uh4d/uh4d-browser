import {
  Component,
  EventEmitter,
  forwardRef,
  Input,
  Output,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ControlValueAccessor,
  FormsModule,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { TagData, TagifyModule } from 'ngx-tagify';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';

@Component({
  selector: 'uh4d-tags-edit',
  standalone: true,
  imports: [CommonModule, FormsModule, Uh4dIconsModule, TagifyModule],
  templateUrl: './tags-edit.component.html',
  styleUrls: ['./tags-edit.component.sass'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TagsEditComponent),
      multi: true,
    },
  ],
})
export class TagsEditComponent implements ControlValueAccessor {
  @Input() placeholder = 'No tags';
  @Input() enabled = true;
  @Input() suggestionsFn?: (query: string) => Observable<string[]>;
  @Output() saved = new EventEmitter<string[]>();

  private valueData: { value: string }[] = [];
  private preValue: { value: string }[] = [];
  private onChange: any = Function.prototype;
  private onTouched: any = Function.prototype;

  editing = false;
  suggestions$ = new Subject<TagData[]>();

  get value(): { value: string }[] {
    return this.valueData;
  }

  set value(v: { value: string }[]) {
    if (v !== this.valueData) {
      this.valueData = v;
      this.onChange(v.map((val) => val.value));
    }
  }

  writeValue(value: string[]) {
    this.valueData = value ? value.map((v) => ({ value: v })) : [];
  }

  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }

  printValue() {
    return this.valueData.map((v) => v.value).join(', ');
  }

  edit() {
    this.preValue = this.valueData;
    this.editing = true;
  }

  onInput(input: string) {
    if (this.suggestionsFn) {
      this.suggestionsFn(input).subscribe((results) => {
        this.suggestions$.next(results.map((t) => ({ value: t })));
      });
    }
  }

  save() {
    this.saved.emit(this.valueData.map((v) => v.value));
    this.editing = false;
  }

  cancel() {
    this.value = this.preValue;
    this.editing = false;
  }
}
