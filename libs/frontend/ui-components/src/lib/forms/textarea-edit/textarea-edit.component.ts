import {
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MarkdownModule } from 'ngx-markdown';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import { AbstractValueAccessor } from '../abstract-value-accessor';

@Component({
  selector: 'uh4d-textarea-edit',
  standalone: true,
  imports: [CommonModule, FormsModule, Uh4dIconsModule, MarkdownModule],
  templateUrl: './textarea-edit.component.html',
  styleUrls: ['./textarea-edit.component.sass'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextareaEditComponent),
      multi: true,
    },
  ],
})
export class TextareaEditComponent extends AbstractValueAccessor<string> {
  @Input() placeholder = 'Empty';
  @Input() enabled = true;
  @Output() saved = new EventEmitter<string>();

  @ViewChild('inputElement', { static: false }) inputElement?: ElementRef;

  private preValue = '';

  editing = false;

  edit() {
    this.preValue = this.value;
    this.editing = true;
    setTimeout(() => {
      this.inputElement?.nativeElement.focus();
    });
  }

  save() {
    this.saved.emit(this.value);
    this.editing = false;
  }

  cancel() {
    this.value = this.preValue;
    this.editing = false;
  }
}
