import {
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import { CitationPipe } from '@uh4d/frontend/pipes';
import { AbstractValueAccessor } from '../abstract-value-accessor';

@Component({
  selector: 'uh4d-citation-edit',
  standalone: true,
  imports: [CommonModule, FormsModule, Uh4dIconsModule, CitationPipe],
  templateUrl: './citation-edit.component.html',
  styleUrls: ['./citation-edit.component.sass'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CitationEditComponent),
      multi: true,
    },
  ],
})
export class CitationEditComponent extends AbstractValueAccessor<string> {
  @Input() placeholder = 'Empty';
  @Input() enabled = true;
  @Output() saved = new EventEmitter<string>();

  @ViewChild('inputElement', { static: false })
  inputElement?: ElementRef<HTMLTextAreaElement>;

  private preValue = '';
  editing = false;

  edit() {
    this.preValue = this.value;
    this.editing = true;
    setTimeout(() => {
      this.inputElement?.nativeElement.focus();
    });
  }

  save() {
    this.saved.emit(this.value);
    this.editing = false;
  }

  cancel() {
    this.value = this.preValue;
    this.editing = false;
  }
}
