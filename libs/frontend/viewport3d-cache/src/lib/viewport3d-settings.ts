import { ColorManagement, Vector3 } from 'three';

ColorManagement.enabled = true;

export enum ShadingKey {
  Color,
  Grey,
  Transparent,
  XRay,
  Projective,
}

export const Shadings = new Map<ShadingKey, string>([
  [ShadingKey.Color, 'Color/Textured'],
  [ShadingKey.Grey, 'Grey'],
  [ShadingKey.Transparent, 'Transparent'],
  [ShadingKey.XRay, 'X-Ray'],
]);

export const Settings = {
  defaults: {
    FOV: 35,
    NEAR: 2,
    FAR: 10000,
    backgroundColor: 0x666666,
    selectionColor: 0xfc4e2a,
    highlightColor: 0xffff44,
    objectColor: 0xdddddd,
    edgeColor: 0x333333,
    viewpoint: {
      cameraPosition: new Vector3(0, 600, 1000),
      controlsTarget: new Vector3(0, 0, 0),
    },
  },

  images: {
    opacity: 1.0,
    scale: 6,
    clusterDistance: 10,
    clusterEnabled: true,
  },

  objects: {
    shading: ShadingKey.Color,
    showEdges: true,
    showOsmBuildings: true,
  },

  terrain: {
    opacity: 1.0,
  },
};
