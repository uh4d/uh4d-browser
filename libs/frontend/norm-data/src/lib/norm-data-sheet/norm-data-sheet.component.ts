import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { NormDataApiService } from '@uh4d/frontend/api-service';
import { NormDataMetaDto } from '@uh4d/dto/interfaces/custom/norm-data';
import {
  RecordCardItemComponent,
  Uh4dAccordionModule,
} from '@uh4d/frontend/ui-components';

@Component({
  selector: 'uh4d-norm-data-sheet',
  standalone: true,
  imports: [
    CommonModule,
    RouterLink,
    Uh4dAccordionModule,
    RecordCardItemComponent,
  ],
  templateUrl: './norm-data-sheet.component.html',
  styleUrls: ['./norm-data-sheet.component.sass'],
})
export class NormDataSheetComponent implements OnInit {
  data!: NormDataMetaDto;
  title = '';
  wikiImageUrl = '';
  descWiki = '';
  descAAT = '';

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly normDataApiService: NormDataApiService,
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.data = data['normData'];
      console.log(this.data);

      if (this.data) {
        const str = this.data.wikidata?.label || this.data.aat?.label || '';
        this.title = str.charAt(0).toUpperCase() + str.slice(1);
      }

      this.queryDescriptions();
    });
  }

  queryDescriptions() {
    this.descWiki = this.descAAT = this.wikiImageUrl = '';

    if (this.data.wikidata) {
      this.normDataApiService
        .getDescription(this.data.wikidata.id)
        .subscribe((result) => {
          this.descWiki = result.description;
          this.wikiImageUrl = result.imageUrl
            ? result.imageUrl + '?width=240'
            : '';
        });
    }

    if (this.data.aat) {
      this.normDataApiService
        .getDescription(this.data.aat.id)
        .subscribe((result) => {
          this.descAAT = result.description;
        });
    }
  }

  openImage(id: string) {
    this.router.navigate(
      ['/explore', this.route.snapshot.paramMap.get('scene'), 'image', id],
      {
        queryParamsHandling: 'preserve',
      },
    );
  }

  openText(id: string) {
    this.router.navigate(
      ['/explore', this.route.snapshot.paramMap.get('scene'), 'text', id],
      {
        queryParamsHandling: 'preserve',
      },
    );
  }
}
