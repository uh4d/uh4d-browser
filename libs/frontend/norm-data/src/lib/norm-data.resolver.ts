import { ResolveFn, Router } from '@angular/router';
import { inject } from '@angular/core';
import { catchError, EMPTY } from 'rxjs';
import { NormDataApiService } from '@uh4d/frontend/api-service';
import { NormDataMetaDto } from '@uh4d/dto/interfaces/custom/norm-data';

export const normDataResolver: ResolveFn<NormDataMetaDto> = (route) => {
  const router = inject(Router);
  const identifier = route.paramMap.get('identifier');

  if (!identifier) {
    return EMPTY;
  }

  return inject(NormDataApiService)
    .get(identifier)
    .pipe(
      catchError(() => {
        let url = '';
        let parent = route.parent;
        while (parent) {
          if (parent.url[0]) {
            url = parent.url[0].path + '/' + url;
          }
          parent = parent.parent;
        }
        router.navigate([url], {
          queryParamsHandling: 'preserve',
        });
        return EMPTY;
      }),
    );
};
