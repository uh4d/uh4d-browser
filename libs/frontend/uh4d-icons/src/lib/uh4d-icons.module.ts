import { NgModule } from '@angular/core';
import {
  FaIconLibrary,
  FontAwesomeModule,
} from '@fortawesome/angular-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { faMarkdown } from '@fortawesome/free-brands-svg-icons';
import {
  faBuildingCustom,
  faCamSpatialEmpty,
  faCamSpatialFog,
  faCamSpatialSlash,
  faCamSpatialSolid,
  faFilterCustom,
  faFilterCustomSlash,
  faImgAnnotation,
  faMapMarkerAltSlash,
  faMapMarkerAltSlashDashed,
  faMapMarkerLight,
  faScreenshot,
  faTourFlag,
  faTourFlagPlus,
} from './icons';

@NgModule({
  imports: [FontAwesomeModule],
  exports: [FontAwesomeModule],
})
export class Uh4dIconsModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas, far);
    library.addIcons(
      faMarkdown,
      faMapMarkerLight,
      faMapMarkerAltSlash,
      faMapMarkerAltSlashDashed,
      faFilterCustom,
      faFilterCustomSlash,
      faBuildingCustom,
      faScreenshot,
      faTourFlag,
      faTourFlagPlus,
      faCamSpatialSolid,
      faCamSpatialEmpty,
      faCamSpatialSlash,
      faCamSpatialFog,
      faImgAnnotation,
    );
  }
}
