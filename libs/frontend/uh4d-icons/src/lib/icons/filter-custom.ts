import { IconDefinition, IconName } from '@fortawesome/fontawesome-svg-core';

export const faFilterCustom: IconDefinition = {
  prefix: 'fas',
  iconName: 'filter-custom' as IconName,
  icon: [
    512,
    512,
    [],
    'f605',
    'M29.7,83.7l168.2,168.2v238.2c0,17.7,20,27.9,34.3,17.9l72.7-50.9c5.8-4.1,9.3-10.8,9.3-17.9V251.9L482.3,83.7 c13.7-13.7,4-37.3-15.4-37.3H45.1C25.7,46.5,15.9,70,29.7,83.7z',
  ],
};
