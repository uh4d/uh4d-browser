import { IconDefinition, IconName } from '@fortawesome/fontawesome-svg-core';

export const faFilterCustomSlash: IconDefinition = {
  prefix: 'fas',
  iconName: 'filter-custom-slash' as IconName,
  icon: [
    512,
    512,
    [],
    'f605',
    'M466.9,46.5c19.4,0,29.2,23.5,15.4,37.3L314.2,251.9v187.4c0,7.1-3.5,13.8-9.3,17.9L232.1,508c-14.4,10-34.3-0.1-34.3-17.9 V295.7L447.1,46.5H466.9z M30.8,355.7c5,5,11.6,7.5,18.1,7.5s13.1-2.5,18.1-7.5L377.2,45.6c10-10,10-26.3,0-36.3 c-10-10-26.3-10-36.3,0l-37.2,37.2H45.1c-19.4,0-29.1,23.5-15.4,37.3L148,202.1L30.8,319.4C20.7,329.4,20.7,345.7,30.8,355.7z',
  ],
};
