import { IconDefinition, IconName } from '@fortawesome/fontawesome-svg-core';

export const faCamSpatialEmpty: IconDefinition = {
  prefix: 'fas',
  iconName: 'cam-spatial-empty' as IconName,
  icon: [
    512,
    512,
    [],
    'f605',
    'M398.9,466.9H45.2C20.2,466.9,0,446.6,0,421.7V150.6c0-24.9,20.2-45.2,45.2-45.2H128l11.7-31c6.6-17.6,23.4-29.3,42.3-29.3h123.3c15.7,0,29.7,9.7,35.2,24.5l13.5,35.8h82.8c24.9,0,45.2,20.2,45.2,45.2v187.3c-9.1-4.7-19.3-7.7-30.1-8.3V150.6c0-8.3-6.8-15.1-15.1-15.1H333.2l-20.8-55.4c-1.1-2.9-4-4.9-7.1-4.9H182c-6.3,0-12,4-14.1,9.8l-18.9,50.5H45.2c-8.3,0-15.1,6.8-15.1,15.1v271.1c0,8.3,6.8,15.1,15.1,15.1H380C384,444.7,390.1,454.1,398.9,466.9z M440.9,508.5c-48.8-70.8-57.9-78-57.9-104c0-35.6,28.9-64.5,64.5-64.5s64.5,28.9,64.5,64.5c0,26-9.1,33.3-57.9,104C450.9,513.2,444.1,513.2,440.9,508.5L440.9,508.5z M447.5,431.4c14.8,0,26.9-12,26.9-26.9c0-14.8-12-26.9-26.9-26.9s-26.9,12-26.9,26.9C420.6,419.3,432.7,431.4,447.5,431.4z',
  ],
};
