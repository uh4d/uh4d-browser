import { IconDefinition, IconName } from '@fortawesome/fontawesome-svg-core';

export const faTourFlagPlus: IconDefinition = {
  prefix: 'fas',
  iconName: 'tour-flag-plus' as IconName,
  icon: [
    300,
    300,
    [],
    'f605',
    'M270.3,82.8H166V12.6h104.2L300,48.1L270.3,82.8z M270.2,127H166v70.2h104.3l29.8-34.8L270.2,127z M0,106.4l29.8,35.5H134V71.6H29.8L0,106.4z M138.3,295.9h23.4V4.1h-23.4V295.9z M123.9,220.1H75.8V172H48.1v48.1H0v27.7h48.1v48.1h27.7v-48.1h48.1V220.1z',
  ],
};
