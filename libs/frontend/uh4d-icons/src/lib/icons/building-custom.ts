import { IconDefinition, IconName } from '@fortawesome/fontawesome-svg-core';

export const faBuildingCustom: IconDefinition = {
  prefix: 'fas',
  iconName: 'building-custom' as IconName,
  icon: [
    300,
    300,
    [],
    'f605',
    'M90,206.7c-16.5,0-30,13.5-30,30V300h60v-63.3C120,220.1,106.5,206.7,90,206.7z M236.7,1.4l-63.3,63.3v92 c0,1.8-1.5,3.3-3.3,3.3H0v140h53.3v-63.3c0-20.2,16.4-36.7,36.7-36.7s36.7,16.4,36.7,36.7V300H300V64.7L236.7,1.4z',
  ],
};
