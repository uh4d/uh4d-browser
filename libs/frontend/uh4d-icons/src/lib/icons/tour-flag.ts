import { IconDefinition, IconName } from '@fortawesome/fontawesome-svg-core';

export const faTourFlag: IconDefinition = {
  prefix: 'fas',
  iconName: 'tour-flag' as IconName,
  icon: [
    300,
    300,
    [],
    'f605',
    'M270.3,82.8H166V12.6h104.2L300,48.1L270.3,82.8z M270.2,127H166v70.2h104.3l29.8-34.8L270.2,127z M0,106.4l29.8,35.5H134V71.6H29.8L0,106.4z M138.3,295.9h23.4V4.1h-23.4V295.9z',
  ],
};
