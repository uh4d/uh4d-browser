import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import {
  BehaviorSubject,
  combineLatestWith,
  debounceTime,
  filter,
  fromEvent,
  ReplaySubject,
  Subject,
  takeUntil,
} from 'rxjs';
import {
  AmbientLight,
  Box3,
  BufferGeometry,
  DirectionalLight,
  Line,
  LineBasicMaterial,
  LineSegments,
  MathUtils,
  Matrix4,
  Mesh,
  MeshLambertMaterial,
  MeshStandardMaterial,
  Object3D,
  PerspectiveCamera,
  Points,
  Raycaster,
  Scene,
  Sphere,
  SRGBColorSpace,
  Vector2,
  Vector3,
  WebGLRenderer,
} from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { OBJExporter } from 'three/examples/jsm/exporters/OBJExporter';
import { LocationOrientationDto } from '@uh4d/dto/interfaces/custom/spatial';
import { NormDataNodeDto, ObjectAnnotationDto } from '@uh4d/dto/interfaces';
import { ContextMenuService } from '@uh4d/frontend/context-menu';
import { Cache, coordsConverter } from '@uh4d/frontend/viewport3d-cache';
import { ViewportDefaults } from '@uh4d/config';

/**
 * Component to view and inspect a single 3D object loaded from a glTF file.
 *
 * ![Screenshot](../assets/object-viewer.png)
 */
@Component({
  selector: 'uh4d-object-viewer',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './object-viewer.component.html',
  styleUrls: ['./object-viewer.component.sass'],
})
export class ObjectViewerComponent implements OnInit, OnDestroy {
  /**
   * @ignore
   */
  private unsubscribe$ = new Subject<void>();

  /**
   * Observable as part to listen to `path` input binding updates.
   * @private
   */
  private path$ = new BehaviorSubject<string>('');

  private annotations$ = new ReplaySubject<ObjectAnnotationDto[]>(1);
  private objectReady$ = new ReplaySubject<boolean>(1);

  /**
   * Reference to the `canvas` element.
   */
  @ViewChild('canvasElement', { static: true }) canvasElement!: ElementRef;

  /**
   * Input binding to set the path to the glTF file.
   * If the value changes, existing object will be disposed and the new glTF file will be loaded.
   * @param value Path to glTF file
   */
  @Input({ required: true })
  set path(value: string) {
    this.path$.next(value);
  }

  /**
   * Input binding to set the position, rotation, and scale of the 3D object.
   */
  @Input() origin:
    | (LocationOrientationDto & { scale?: number | number[] })
    | null = null;

  @Input() set annotations(value: ObjectAnnotationDto[]) {
    this.annotations$.next(value);
  }

  @Output() annotationQueried = new EventEmitter<ObjectAnnotationDto>();

  /**
   * Width of the component/canvas.
   * @private
   */
  private screenWidth = 1;
  /**
   * Height of the component/canvas.
   * @private
   */
  private screenHeight = 1;

  private scene!: Scene;
  private renderer!: WebGLRenderer;
  private camera!: PerspectiveCamera;
  private controls!: OrbitControls;
  private directionalLight!: DirectionalLight;

  /**
   * 3D object instance loaded from the glTF file.
   * @private
   */
  private object: Object3D | null = null;

  private selectedSubObject: Object3D | null = null;
  private isMovingControls = false;

  showTooltip = false;
  annotationData: ObjectAnnotationDto[] = [];

  tooltipPosition = { left: 0, top: 0 };
  wikidata: NormDataNodeDto[] = [];
  aat: NormDataNodeDto[] = [];

  constructor(
    private readonly element: ElementRef,
    private readonly ngZone: NgZone,
    private readonly contextMenuService: ContextMenuService,
    private readonly route: ActivatedRoute,
  ) {}

  /**
   * Invoked after the component is instantiated.
   * It will initialize the 3D scene (renderer, camera, controls, etc.)
   * and listen to `path` input binding updates.
   */
  ngOnInit(): void {
    this.resizeViewer();

    this.scene = new Scene();
    this.scene.add(new AmbientLight(0xffffff, 1.5));
    this.directionalLight = new DirectionalLight(0xffffff, 1.5);
    this.scene.add(this.directionalLight);

    this.renderer = new WebGLRenderer({
      antialias: true,
      alpha: true,
      canvas: this.canvasElement.nativeElement,
    });
    this.renderer.setClearColor(0xffffff, 0.0);

    this.camera = new PerspectiveCamera(
      35,
      this.screenWidth / this.screenHeight,
      1,
      2000,
    );
    this.camera.position.set(0, 1, 2);

    this.ngZone.runOutsideAngular(() => {
      this.controls = new OrbitControls(
        this.camera,
        this.canvasElement.nativeElement,
      );
      fromEvent(this.controls, 'change')
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(() => {
          this.isMovingControls = true;
          this.render();
        });
      fromEvent(this.controls, 'end')
        .pipe(takeUntil(this.unsubscribe$), debounceTime(100))
        .subscribe(() => {
          this.isMovingControls = false;
        });
      fromEvent<PointerEvent>(this.canvasElement.nativeElement, 'mouseup')
        .pipe(
          takeUntil(this.unsubscribe$),
          filter(() => !this.isMovingControls),
        )
        .subscribe((event) => {
          this.selectFromClick(event);
          switch (event.button) {
            case 0:
              this.leftClickAnnotation(event);
              break;
            case 2:
              this.rightClickAnnotation(event);
              break;
          }
        });
    });

    this.path$.pipe(takeUntil(this.unsubscribe$)).subscribe((path) => {
      this.loadObject(path);
    });

    this.annotations$
      .pipe(
        takeUntil(this.unsubscribe$),
        combineLatestWith(this.objectReady$),
        filter(([data, ready]) => ready),
        debounceTime(100),
      )
      .subscribe(([data]) => {
        this.annotationData = data;
        this.selectFromFragment();
      });
  }

  /**
   * Render the scene (invoked after controls change).
   * @private
   */
  private render(): void {
    this.directionalLight.position.set(4, 4, 4);
    const lightMatrix = new Matrix4().makeRotationFromQuaternion(
      this.camera.quaternion,
    );
    this.directionalLight.position.applyMatrix4(lightMatrix);

    this.renderer.render(this.scene, this.camera);
  }

  /**
   * Load a glTF file, add it to the scene, and set camera position to fit the viewport.
   * @param path Path to glTF file
   * @private
   */
  private async loadObject(path: string): Promise<void> {
    if (!['gltf', 'glb'].includes(path.split('.').pop()!)) {
      return;
    }

    this.setSelected(null);
    this.disposeObject();

    try {
      const gltf = await Cache.gltfLoader.loadAsync(path);
      this.object = gltf.scene.children[0];

      if (this.origin) {
        this.object.position.copy(
          coordsConverter.fromLatLon(
            this.origin.latitude,
            this.origin.longitude,
            this.origin.altitude,
          ),
        );
        this.object.rotation.set(
          this.origin.omega,
          this.origin.phi,
          this.origin.kappa,
          'YXZ',
        );
        if (this.origin.scale) {
          if (Array.isArray(this.origin.scale)) {
            this.object.scale.fromArray(this.origin.scale);
          } else {
            this.object.scale.set(
              this.origin.scale,
              this.origin.scale,
              this.origin.scale,
            );
          }
        }
      }

      this.scene.add(this.object);

      this.object.traverse((obj) => {
        if (obj instanceof Mesh) {
          obj.layers.enable(1);
          (Array.isArray(obj.material) ? obj.material : [obj.material]).forEach(
            (mat: MeshStandardMaterial) => {
              mat.color.convertSRGBToLinear();
              if (mat.map) {
                mat.map.colorSpace = SRGBColorSpace;
              }
            },
          );
        } else if (obj instanceof LineSegments) {
          (
            obj as LineSegments<BufferGeometry, LineBasicMaterial>
          ).material.dispose();
          obj.material = Cache.materials.get('edgesMat');
        }
      });

      // compute camera position to fit the viewport
      const boundingBox = new Box3();
      boundingBox.expandByObject(this.object);
      const sphere = boundingBox.getBoundingSphere(new Sphere());

      const s = new Vector3().subVectors(
        this.camera.position,
        this.controls.target,
      );
      const h =
        sphere.radius / Math.tan((this.camera.fov / 2) * MathUtils.DEG2RAD);
      const pos = new Vector3().addVectors(sphere.center, s.setLength(h));

      this.camera.position.copy(pos);
      this.controls.target.copy(sphere.center);

      this.resizeViewer();

      this.objectReady$.next(true);
    } catch (e) {
      console.error("Couldn't load gltf file", e);
    }
  }

  /**
   * Remove the object from the scene and dispose geometry and materials.
   * @private
   */
  private disposeObject(): void {
    if (!this.object) return;

    this.scene.remove(this.object);

    this.object.traverse((obj) => {
      if (obj instanceof Mesh || obj instanceof LineSegments) {
        obj.geometry.dispose();
        (Array.isArray(obj.material) ? obj.material : [obj.material]).forEach(
          (mat) => {
            Cache.materials.disposeMaterial(mat);
          },
        );
      }
    });

    this.object = null;
    this.objectReady$.next(false);
  }

  /**
   * Transform mouse/screen coordinates into viewport coordinates.
   */
  private getViewportPosition(event: PointerEvent): Vector2 {
    return new Vector2(
      (event.offsetX / this.screenWidth) * 2 - 1,
      -(event.offsetY / this.screenHeight) * 2 + 1,
    );
  }

  /**
   * Transform viewport coordinates into screen coordinates.
   */
  private getScreenPosition(pos: Vector2 | Vector3): Vector2 {
    return new Vector2(
      (this.screenWidth * (pos.x + 1)) / 2,
      (this.screenHeight * (-pos.y + 1)) / 2,
    );
  }

  private selectFromClick(event: PointerEvent): void {
    if (!this.object) return;

    const mouse = this.getViewportPosition(event);
    const raycaster = new Raycaster();
    raycaster.layers.set(1);
    raycaster.setFromCamera(mouse, this.camera);

    const intersections = raycaster.intersectObject(this.object, true);
    this.setSelected(intersections[0] ? intersections[0].object : null);
  }

  private selectFromFragment() {
    const fragment = this.route.snapshot.fragment;
    if (!fragment) return;

    const ann = this.annotationData.find((d) => d.id === fragment);
    if (!ann) return;

    const obj = this.object?.getObjectByName(ann.objectName);
    if (!obj) return;

    this.setSelected(obj);

    // determine tooltip position
    const bbox = new Box3();
    bbox.expandByObject(obj);
    const center = bbox.getCenter(new Vector3());
    const screenPos = this.getScreenPosition(center.project(this.camera));
    this.leftClickAnnotation({ offsetX: screenPos.x, offsetY: screenPos.y });
  }

  private setSelected(obj: Object3D | null): void {
    if (obj === this.selectedSubObject) return;

    if (this.selectedSubObject !== null && obj !== this.selectedSubObject) {
      this.selectedSubObject.traverse((child) => {
        if (child instanceof Mesh) {
          if (child.userData['originalMat']) {
            child.material.dispose();
            child.material = child.userData['originalMat'];
          }
        }
      });
      this.wikidata = [];
      this.aat = [];
      this.selectedSubObject = null;
      this.showTooltip = false;
    }

    if (obj) {
      const annotation = this.annotationData.find(
        (ann) => ann.objectName === obj.name,
      );
      if (annotation) {
        obj.traverse((child) => {
          if (child instanceof Mesh) {
            child.userData['originalMat'] = child.material;
            child.material = new MeshLambertMaterial({
              color: ViewportDefaults.highlightColor,
            });
          }
        });
        obj.userData['annotation'] = annotation;
        this.wikidata = annotation.wikidata;
        this.aat = annotation.aat;
        this.selectedSubObject = obj;
      }
    }

    this.render();
  }

  private leftClickAnnotation(event: {
    offsetX: number;
    offsetY: number;
  }): void {
    if (!this.selectedSubObject) return;
    this.showTooltip = true;
    this.tooltipPosition.left = event.offsetX + 10;
    this.tooltipPosition.top = event.offsetY - 5;
  }

  private rightClickAnnotation(event: PointerEvent): void {
    if (!this.selectedSubObject) return;
    this.showTooltip = false;
    const annotation = this.selectedSubObject.userData[
      'annotation'
    ] as ObjectAnnotationDto;

    this.contextMenuService.open(event, {
      title: annotation.aat[0].label || undefined,
      actions: [
        {
          icon: 'filter-custom',
          label: 'Search by annotation',
          click: () => {
            this.annotationQueried.emit(annotation);
          },
        },
        {
          icon: 'external-link-alt',
          label: 'Wikidata entry...',
          click: () => {
            window.open(
              'https://www.wikidata.org/wiki/' +
                annotation.wikidata[0].identifier,
              '_blank',
            );
          },
        },
        {
          icon: 'external-link-alt',
          label: 'AAT entry...',
          click: () => {
            window.open(
              'https://www.getty.edu/vow/AATFullDisplay?find=&logic=AND&note=&subjectid=' +
                annotation.aat[0].identifier,
              '_blank',
            );
          },
        },
      ],
    });
  }

  /**
   * Export 3D object as OBJ file.
   * Only meshes are exported.
   */
  exportObjectAsOBJ(): string {
    if (!this.object) throw new Error('Object not set');

    // clone object and keep only meshes
    const objClone = this.object.clone(true);
    const discardObjs: Object3D[] = [];

    objClone.traverse((child) => {
      if ((child as Line).isLine || (child as Points).isPoints) {
        discardObjs.push(child);
      }
    });

    for (const child of discardObjs) {
      const parent = child.parent;
      if (parent) parent.remove(child);
    }

    // parse object to OBJ string
    const exporter = new OBJExporter();
    return exporter.parse(objClone);
  }

  /**
   * Update renderer size and camera projection matrix (invoked at [ngOnInit](#ngOnInit) and `window:resize` event).
   * @private
   */
  @HostListener('window:resize')
  private resizeViewer(): void {
    this.screenWidth = this.element.nativeElement.offsetWidth;
    this.screenHeight = this.element.nativeElement.offsetHeight;

    if (this.camera) {
      this.camera.aspect = this.screenWidth / this.screenHeight;
      this.camera.updateProjectionMatrix();
    }

    if (this.renderer) {
      this.renderer.setSize(this.screenWidth, this.screenHeight);
      this.render();
    }
  }

  /**
   * Invoked before the component gets destroyed.
   * The renderer, controls, and the object's geometry and material will be disposed.
   */
  ngOnDestroy(): void {
    this.path$.complete();
    this.annotations$.complete();
    this.objectReady$.complete();
    this.unsubscribe$.next();
    this.unsubscribe$.complete();

    this.disposeObject();

    this.controls.dispose();

    this.renderer.forceContextLoss();
    this.renderer.dispose();
  }
}
