import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Params, Router, RouterLink } from '@angular/router';
import { combineLatest, finalize, Subject, takeUntil, tap } from 'rxjs';
import { add, differenceInDays } from 'date-fns';
import { NgxPermissionsModule } from 'ngx-permissions';
import { NotificationsService } from 'angular2-notifications';
import {
  InlineEditComponent,
  MapInteractiveComponent,
  RecordValidationBoxComponent,
} from '@uh4d/frontend/ui-components';
import { AuthService } from '@uh4d/frontend/auth';
import { ObjectsApiService } from '@uh4d/frontend/api-service';
import { ObjectFullDto } from '@uh4d/dto/interfaces/custom/object';
import { ObjectMetadataComponent } from '../object-metadata/object-metadata.component';
import { ObjectDataService } from '../object-data.service';
import { ObjectViewerComponent } from '../object-viewer/object-viewer.component';

@Component({
  selector: 'uh4d-object-sheet-compact',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    InlineEditComponent,
    MapInteractiveComponent,
    NgxPermissionsModule,
    RecordValidationBoxComponent,
    RouterLink,
    ObjectMetadataComponent,
    ObjectViewerComponent,
  ],
  templateUrl: './object-sheet-compact.component.html',
  styleUrls: ['./object-sheet-compact.component.sass'],
})
export class ObjectSheetCompactComponent implements OnInit, OnDestroy {
  private unsubscribe$: Subject<void> = new Subject();

  object!: ObjectFullDto;

  canEditObject = false;
  canValidate = false;

  isUpdating = false;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly authService: AuthService,
    private readonly objectsApiService: ObjectsApiService,
    private readonly objectDataService: ObjectDataService,
    private readonly notify: NotificationsService,
  ) {}

  ngOnInit() {
    combineLatest({
      // get object data from resolver
      data: this.route.data.pipe(
        tap((data) => {
          this.object = data['object'];
          console.log(this.object);
        }),
      ),
      user: this.authService.user$,
    })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(({ user }) => {
        this.canEditObject =
          !!user &&
          user.canEditItem(this.object.location, this.object.uploadedBy);
        this.canValidate = !!user && user?.canEditItem(this.object.location);
      });
  }

  updateName(): void {
    // name property is required and cannot be empty
    if (!this.object.name.length) {
      this.notify.error('Object must have a name!');
      return;
    }

    this.objectsApiService
      .patch(this.object.id, { name: this.object.name })
      .subscribe((result) => {
        this.object.editedBy = result.editedBy;
        this.objectDataService.updated(this.object);
      });
  }

  validateObject(accept: boolean) {
    this.isUpdating = true;
    this.objectsApiService
      .updateValidationStatus(this.object.id, false, !accept)
      .pipe(
        finalize(() => {
          this.isUpdating = false;
        }),
      )
      .subscribe((result) => {
        this.object.pending = result.pending;
        this.object.declined = result.declined;
        this.objectDataService.updated(this.object);
      });
  }

  openScene() {
    const queryParams: Params = {};
    if (this.object.pending) {
      queryParams['q'] = 'pending';
    } else if (this.object.declined) {
      queryParams['q'] = 'declined';
    }

    const from = this.object.date.from;
    const to = this.object.date.to;
    if (from && !to) {
      queryParams['date'] = from;
    } else if (!from && to) {
      queryParams['date'] = to;
    } else if (from && to) {
      const diff = differenceInDays(from, to);
      queryParams['date'] = add(from, { days: diff / 2 });
    }

    const url = this.router.serializeUrl(
      this.router.createUrlTree(
        [
          '/explore',
          `${this.object.origin.latitude},${this.object.origin.longitude}`,
        ],
        { queryParams },
      ),
    );

    window.open(url, '_blank');
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
