import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {
  combineLatest,
  map,
  Observable,
  of,
  ReplaySubject,
  Subject,
  takeUntil,
} from 'rxjs';
import { isAfter, isBefore } from 'date-fns';
import { NotificationsService } from 'angular2-notifications';
import {
  InlineEditComponent,
  RecordUserMetadataComponent,
  TagsEditComponent,
  TextareaEditComponent,
} from '@uh4d/frontend/ui-components';
import { AuthService } from '@uh4d/frontend/auth';
import {
  AddressesApiService,
  ObjectsApiService,
  TagsApiService,
} from '@uh4d/frontend/api-service';
import { ObjectFullDto } from '@uh4d/dto/interfaces/custom/object';
import { UpdateObjectDto, UpdateObjectMetaDto } from '@uh4d/dto/interfaces';
import { ObjectDataService } from '../object-data.service';

@Component({
  selector: 'uh4d-object-metadata',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    InlineEditComponent,
    RecordUserMetadataComponent,
    TextareaEditComponent,
    TagsEditComponent,
  ],
  templateUrl: './object-metadata.component.html',
  styleUrls: ['./object-metadata.component.sass'],
})
export class ObjectMetadataComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();
  private object$ = new ReplaySubject<ObjectFullDto>(1);

  @Input({ required: true }) set object(value: ObjectFullDto) {
    this.object$.next(value);
  }

  record!: ObjectFullDto;

  canEditObject = false;

  constructor(
    private readonly authService: AuthService,
    private readonly objectsApiService: ObjectsApiService,
    private readonly tagsApiService: TagsApiService,
    private readonly addressesApiService: AddressesApiService,
    private readonly notify: NotificationsService,
    private readonly objectDataService: ObjectDataService,
  ) {}

  ngOnInit() {
    combineLatest({
      object: this.object$,
      user: this.authService.user$,
    })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(({ object, user }) => {
        this.record = object;
        this.canEditObject =
          !!user && user.canEditItem(object.location, object.uploadedBy);
      });
  }

  /**
   * Update metadata of the object. Only one property can be updated at once.
   * @param prop Name of property that should be updated
   */
  updateObject(
    prop: keyof UpdateObjectDto | keyof UpdateObjectMetaDto | 'from' | 'to',
  ): void {
    switch (prop) {
      case 'name':
        if (!this.record.name) {
          this.notify.error('Object must have a name!');
          return;
        }
        break;
      case 'from':
        if (this.record.date.from) {
          if (
            this.record.date.to &&
            isAfter(this.record.date.from, this.record.date.to)
          ) {
            this.notify.error('Date must be before deconstruction date!');
            return;
          }
        }
        prop = 'date';
        break;
      case 'to':
        if (this.record.date.to) {
          if (
            this.record.date.from &&
            isBefore(this.record.date.to, this.record.date.from)
          ) {
            this.notify.error('Date must be after erection date!');
            return;
          }
        }
        prop = 'date';
        break;
      case 'tags':
        console.log(this.record.tags);
    }

    this.objectsApiService
      .patch(this.record.id, { [prop]: this.record[prop] })
      .subscribe((result) => {
        this.record.editedBy = result.editedBy;
        this.objectDataService.updated(this.record);
      });
  }

  /**
   * Method passed to typeahead to query addresses.
   * @param input Typed input
   */
  queryAddresses(input: string): Observable<string[]> {
    if (input) {
      return this.addressesApiService
        .query(input)
        .pipe(map((results) => results.map((r) => r.value)));
    }

    return of([]);
  }

  /**
   * Method passed to typeahead to query tags.
   * @param input Typed input
   */
  queryTags(input: string): Observable<string[]> {
    if (input) {
      return this.tagsApiService
        .query(input)
        .pipe(map((results) => results.map((r) => r.value)));
    }

    return of([]);
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
