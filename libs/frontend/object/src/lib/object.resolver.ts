import { ResolveFn, Router } from '@angular/router';
import { inject } from '@angular/core';
import { catchError, EMPTY } from 'rxjs';
import { ObjectsApiService } from '@uh4d/frontend/api-service';
import { ObjectFullDto } from '@uh4d/dto/interfaces/custom/object';

/**
 * When entering the route, query database for object entry with given `objectId` parameter.
 * If no entry found, navigate back to parent page.
 */
export const objectResolver: ResolveFn<ObjectFullDto> = (route) => {
  const router = inject(Router);
  const objectId = route.paramMap.get('objectId');

  if (!objectId) {
    return EMPTY;
  }

  return inject(ObjectsApiService)
    .get(objectId)
    .pipe(
      catchError(() => {
        let url = '';
        let parent = route.parent;
        while (parent) {
          if (parent.url[0]) {
            url = parent.url[0].path + '/' + url;
          }
          parent = parent.parent;
        }
        router.navigate([url], {
          queryParamsHandling: 'preserve',
        });
        return EMPTY;
      }),
    );
};
