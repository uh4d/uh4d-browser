import {
  Component,
  HostListener,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { combineLatest, finalize, Subject, takeUntil, tap } from 'rxjs';
import { BsModalService } from 'ngx-bootstrap/modal';
import { NotificationsService } from 'angular2-notifications';
import { NgxPermissionsModule } from 'ngx-permissions';
import { ConfirmModalService } from '@uh4d/frontend/confirm-modal';
import {
  SearchManagerService,
  ViewportEventsService,
} from '@uh4d/frontend/services';
import { AuthService } from '@uh4d/frontend/auth';
import {
  AnnotationsApiService,
  ObjectsApiService,
} from '@uh4d/frontend/api-service';
import { ObjectAnnotationDto } from '@uh4d/dto/interfaces';
import {
  SimilarAnnotationsDto,
  SimilarRecord,
} from '@uh4d/dto/interfaces/custom/annotation';
import { ObjectFullDto } from '@uh4d/dto/interfaces/custom/object';
import { Cache } from '@uh4d/frontend/viewport3d-cache';
import {
  AccordionItemComponent,
  AnnotationCloudComponent,
  InlineEditComponent,
  RecordCardItemComponent,
  RecordValidationBoxComponent,
  Uh4dAccordionModule,
} from '@uh4d/frontend/ui-components';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import {
  TextListComponent,
  TextUploadModalComponent,
} from '@uh4d/frontend/text';
import { ObjectViewerComponent } from '../object-viewer/object-viewer.component';
import { ObjectMetadataComponent } from '../object-metadata/object-metadata.component';
import { ObjectDataService } from '../object-data.service';

/**
 * Component that shows all the metadata of an object.
 * In edit mode, the metadata can be updated.
 * The object as an 3D model is displayed within an interactive object viewer.
 *
 * ![Screenshot](../assets/object-sheet.png)
 */
@Component({
  selector: 'uh4d-object-sheet',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    RouterLink,
    NgxPermissionsModule,
    Uh4dIconsModule,
    Uh4dAccordionModule,
    InlineEditComponent,
    AnnotationCloudComponent,
    RecordCardItemComponent,
    ObjectViewerComponent,
    ObjectMetadataComponent,
    RecordValidationBoxComponent,
    TextListComponent,
  ],
  templateUrl: './object-sheet.component.html',
  styleUrls: ['./object-sheet.component.sass'],
})
export class ObjectSheetComponent implements OnInit, OnDestroy {
  /**
   * @ignore
   */
  private unsubscribe$ = new Subject<void>();

  /**
   * Object database entry.
   */
  object!: ObjectFullDto;
  /**
   * Flag that if `true` will unhide buttons to edit metadata.
   */
  canEditObject = false;

  canValidate = false;

  /**
   * Flag that if `true` will unhide some buttons with more crucial operations.
   */
  expertMode = false;

  /**
   * Reference to the object viewer component.
   */
  @ViewChild(ObjectViewerComponent) objectViewer!: ObjectViewerComponent;

  annotations: ObjectAnnotationDto[] = [];

  similarRecords: SimilarAnnotationsDto | null = null;

  @ViewChild('annotationAccordionItem', { static: true })
  annotationAccordionItem!: AccordionItemComponent;

  isUpdating = false;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly notify: NotificationsService,
    private readonly objectsApiService: ObjectsApiService,
    private readonly searchManager: SearchManagerService,
    private readonly confirmService: ConfirmModalService,
    private readonly authService: AuthService,
    private readonly viewportEvents: ViewportEventsService,
    private readonly modalService: BsModalService,
    private readonly annotationsApiService: AnnotationsApiService,
    private readonly objectDataService: ObjectDataService,
  ) {}

  /**
   * Invoked after the component is instantiated.
   * Get object data from resolver and listen to permission updates.
   */
  ngOnInit(): void {
    combineLatest({
      data: this.route.data.pipe(
        tap((data) => {
          this.object = data['object'];
          console.log(this.object);
          this.similarRecords = null;
          this.loadAnnotations();
        }),
      ),
      user: this.authService.user$,
    })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(({ user }) => {
        this.canEditObject =
          !!user &&
          user.canEditItem(this.object.location, this.object.uploadedBy);
        this.canValidate = !!user && user?.canEditItem(this.object.location);
      });
  }

  private loadAnnotations() {
    this.objectsApiService
      .queryAnnotations(this.object.id)
      .subscribe((annotations) => {
        this.annotations = annotations;
      });
  }

  /**
   * Return to 3D viewport and jump to object that it fits the viewport.
   */
  zoomObject(): void {
    const item = Cache.objects.getByName(this.object.id);

    if (item) {
      this.router.navigate(['../..'], {
        relativeTo: this.route,
        queryParamsHandling: 'preserve',
      });
      item.focus();
    }
  }

  updateName(): void {
    // name property is required and cannot be empty
    if (!this.object.name.length) {
      this.notify.error('Object must have a name!');
      return;
    }

    this.objectsApiService
      .patch(this.object.id, { name: this.object.name })
      .subscribe((result) => {
        this.object.editedBy = result.editedBy;
        this.objectDataService.updated(this.object);
      });
  }

  validateObject(accept: boolean) {
    this.isUpdating = true;
    this.objectsApiService
      .updateValidationStatus(this.object.id, false, !accept)
      .pipe(
        finalize(() => {
          this.isUpdating = false;
        }),
      )
      .subscribe((result) => {
        this.object.pending = result.pending;
        this.object.declined = result.declined;
        this.objectDataService.updated(this.object);
      });
  }

  /**
   * Open modal to upload a new associated text.
   */
  openTextUploadModal(): void {
    const modalRef = this.modalService.show(TextUploadModalComponent, {
      class: 'modal-lg',
      ignoreBackdropClick: true,
    });
    modalRef.content!.objectIds = [this.object.id];
  }

  /**
   * Toggle {@link #expertMode expertMode} when user presses <kbd>Shift</kbd> + <kbd>Alt</kbd> + <kbd>D</kbd>.
   * @private
   */
  @HostListener('document:keyup.shift.alt.d')
  private toggleExpertMode(): void {
    this.expertMode = !this.expertMode;
  }

  /**
   * Return to 3D viewport and enter object transformation mode.
   */
  updateTransform(): void {
    this.router.navigate(['../..'], {
      relativeTo: this.route,
      queryParamsHandling: 'preserve',
    });

    this.viewportEvents.callAction$.next({
      key: 'transform-update',
      object: this.object,
    });
  }

  /**
   * Duplicate the database entry (reference to 3D model files will remain the same).
   * The user has to confirm this operation.
   * If successful, enter object sheet of duplicate.
   */
  async duplicateObject(): Promise<void> {
    const confirmed = await this.confirmService.confirm({
      message:
        'This operation will duplicate the database entry, such that metadata can be altered. The 3D model remains the same. Do you want to continue?',
    });

    if (!confirmed) return;

    this.objectsApiService.duplicate(this.object).subscribe((result) => {
      this.router.navigate(['..', result.id], {
        relativeTo: this.route,
        queryParamsHandling: 'preserve',
      });
    });
  }

  /**
   * Remove object from database.
   * The user has to confirm this operation.
   * After successful removal, return to 3D viewport.
   */
  async removeObject(): Promise<void> {
    const confirmed = await this.confirmService.confirm({
      message:
        'This operation will remove the object from database and delete all corresponding files. Do you want to continue?',
    });

    if (!confirmed) return;

    this.objectsApiService.remove(this.object.id).subscribe(() => {
      this.searchManager.queryModels();
      this.router.navigate(['../..'], {
        relativeTo: this.route,
        queryParamsHandling: 'preserve',
      });
    });
  }

  downloadObject(): void {
    // get obj data
    const objData = this.objectViewer.exportObjectAsOBJ();

    // create mock element and trigger click event
    const a = document.createElement('a');
    a.setAttribute(
      'href',
      'data:text/plain;charset=utf-8,' + encodeURIComponent(objData),
    );
    a.setAttribute('download', this.object.name + '.obj');
    a.click();
  }

  querySimilarAnnotations(annotation: ObjectAnnotationDto) {
    this.annotationAccordionItem.open();
    this.annotationsApiService
      .querySimilarAnnotations(annotation.id)
      .subscribe((results) => {
        this.similarRecords = results;
      });
  }

  openRecord(type: 'image' | 'text' | 'object', record: SimilarRecord) {
    this.router.navigate(
      ['/explore', this.route.snapshot.paramMap.get('scene'), type, record.id],
      {
        queryParamsHandling: 'preserve',
        fragment: record.annotations[0].id,
      },
    );
  }

  /**
   * Invoked before the component gets destroyed.
   */
  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
