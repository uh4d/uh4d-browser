import { Injectable } from '@angular/core';
import { ObjectDto } from '@uh4d/dto/interfaces';
import { DataEventService } from '@uh4d/utils';

@Injectable({
  providedIn: 'root',
})
export class ObjectDataService extends DataEventService<ObjectDto> {}
