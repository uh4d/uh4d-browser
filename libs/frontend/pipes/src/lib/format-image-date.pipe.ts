import { Pipe, PipeTransform } from '@angular/core';
import { ImageDateDto } from '@uh4d/dto/interfaces';
import { addYears, format } from 'date-fns';

@Pipe({
  name: 'formatImageDate',
  standalone: true,
})
export class FormatImageDatePipe implements PipeTransform {
  transform(value: ImageDateDto | string): string {
    if (typeof value === 'string') {
      return value;
    }

    if (!value || !value.display) {
      return 'unknown';
    }

    switch (value.display) {
      case 'YYYY/YYYY':
        return format(value.from, 'yyyy') + '/' + format(value.to, 'yyyy');
      case 'around YYYY':
        return 'around ' + format(addYears(value.from, 5), 'yyyy');
      case 'before YYYY':
        return 'before ' + format(value.to, 'yyyy');
      case 'after YYYY':
        return 'after ' + format(value.from, 'yyyy');
      default:
        return format(
          value.from,
          value.display.replace(/[YD]/g, (v) => v.toLowerCase()),
        );
    }
  }
}
