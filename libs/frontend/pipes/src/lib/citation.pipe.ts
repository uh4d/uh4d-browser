import { Pipe, PipeTransform } from '@angular/core';
import { Cite } from '@citation-js/core';
import '@citation-js/plugin-bibtex';
import '@citation-js/plugin-csl';
import linkifyHtml from 'linkify-html';

/**
 * Format bibtex data as reference style (e.g. APA).
 */
@Pipe({
  name: 'citation',
  standalone: true,
})
export class CitationPipe implements PipeTransform {
  transform(value: string, style = 'apa'): string {
    const plainHtml = new Cite(value).get({
      format: 'string',
      type: 'html',
      style: 'citation-apa',
      lang: 'en-US',
    });
    return linkifyHtml(plainHtml, { target: '_blank' });
    // return new Cite(value).format('bibliography', {
    //   format: 'html', template: style, lang: 'en-US'
    // });
  }
}
