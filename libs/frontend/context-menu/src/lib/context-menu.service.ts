import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { IconProp } from '@fortawesome/fontawesome-svg-core';

export interface ContextMenuAction {
  icon: IconProp | string;
  label: string;
  click: () => void;
}

export interface ContextMenuOptions {
  title?: string;
  actions: ContextMenuAction[];
}

@Injectable({
  providedIn: 'root',
})
export class ContextMenuService {
  private _subject$ = new Subject<{
    event: MouseEvent;
    options: ContextMenuOptions;
  }>();

  contextClick$ = this._subject$.asObservable();

  open(event: MouseEvent, options: ContextMenuOptions): void {
    event.preventDefault();
    this._subject$.next({ event, options });
  }

  close(): void {
    this._subject$.next(null);
  }
}
