import { Component, OnInit } from '@angular/core';
import {
  ContextMenuAction,
  ContextMenuOptions,
  ContextMenuService,
} from './context-menu.service';

@Component({
  selector: 'uh4d-context-menu',
  templateUrl: './context-menu.component.html',
  styleUrls: ['./context-menu.component.sass'],
})
export class ContextMenuComponent implements OnInit {
  position = { left: 0, top: 0 };
  options: ContextMenuOptions | null = null;

  constructor(private readonly contextMenuService: ContextMenuService) {}

  ngOnInit(): void {
    this.contextMenuService.contextClick$.subscribe((value) => {
      if (value) {
        this.position.left = value.event.pageX - 20;
        this.position.top = value.event.pageY - 20;
        this.options = value.options;
      } else {
        this.options = null;
      }
    });
  }

  onClick(action: ContextMenuAction) {
    action.click();
    this.contextMenuService.close();
  }

  onMouseLeave(): void {
    this.contextMenuService.close();
  }
}
