import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import { ContextMenuComponent } from './context-menu.component';

@NgModule({
  declarations: [ContextMenuComponent],
  imports: [CommonModule, Uh4dIconsModule],
  exports: [ContextMenuComponent],
})
export class ContextMenuModule {}
