import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { NgxPermissionsModule } from 'ngx-permissions';
import { combineLatest, finalize, Subject, takeUntil, tap } from 'rxjs';
import { NotificationsService } from 'angular2-notifications';
import {
  ImageFileUpdateDto,
  ImageFullDto,
} from '@uh4d/dto/interfaces/custom/image';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import {
  AnnotationCloudComponent,
  InlineEditComponent,
  RecordCardItemComponent,
  RecordValidationBoxComponent,
  Uh4dAccordionModule,
} from '@uh4d/frontend/ui-components';
import { AuthService } from '@uh4d/frontend/auth';
import { ImagesApiService } from '@uh4d/frontend/api-service';
import { ImageAnnotationDto } from '@uh4d/dto/interfaces';
import { ConfirmModalService } from '@uh4d/frontend/confirm-modal';
import {
  SimilarAnnotationsDto,
  SimilarRecord,
} from '@uh4d/dto/interfaces/custom/annotation';
import {
  GlobalEventsService,
  SearchManagerService,
} from '@uh4d/frontend/services';
import { Cache } from '@uh4d/frontend/viewport3d-cache';
import { ImageMetadataComponent } from '../image-metadata/image-metadata.component';
import { ImageRephotosListComponent } from '../image-rephotos-list/image-rephotos-list.component';
import { ImageSvgViewerComponent } from '../image-svg-viewer/image-svg-viewer.component';
import { ImageCaptionExperimentalComponent } from '../image-caption-experimental/image-caption-experimental.component';
import { ImageDataService } from '../image-data.service';

@Component({
  selector: 'uh4d-image-sheet',
  standalone: true,
  imports: [
    CommonModule,
    Uh4dIconsModule,
    FormsModule,
    NgxPermissionsModule,
    Uh4dAccordionModule,
    RecordValidationBoxComponent,
    RecordCardItemComponent,
    RouterLink,
    InlineEditComponent,
    AnnotationCloudComponent,
    ImageMetadataComponent,
    ImageSvgViewerComponent,
    ImageCaptionExperimentalComponent,
    ImageRephotosListComponent,
  ],
  templateUrl: './image-sheet.component.html',
  styleUrls: ['./image-sheet.component.sass'],
})
export class ImageSheetComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();

  image!: ImageFullDto;

  canEditImage = false;
  canValidate = false;

  /**
   * Flag that if `true` will unhide some buttons with more crucial operations.
   */
  expertMode = false;

  isUpdating = false;

  /**
   * Metadata about file update.
   */
  fileUpdate: ImageFileUpdateDto | null = null;

  annotations: ImageAnnotationDto[] = [];

  similarRecords: SimilarAnnotationsDto | null = null;

  constructor(
    private readonly authService: AuthService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly imagesApiService: ImagesApiService,
    private readonly imageDataService: ImageDataService,
    private readonly notify: NotificationsService,
    private readonly confirmService: ConfirmModalService,
    private readonly searchManager: SearchManagerService,
    private readonly globalEvents: GlobalEventsService,
  ) {}

  ngOnInit() {
    combineLatest({
      // get image data from resolver
      data: this.route.data.pipe(
        tap((data) => {
          this.image = data['image'];
          console.log(this.image);
          this.similarRecords = null;
          this.loadAnnotations();
        }),
      ),
      user: this.authService.user$,
    })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(({ user }) => {
        this.canEditImage =
          !!user && user.canEditItem(this.image.camera, this.image.uploadedBy);
        this.canValidate = !!user && user?.canEditItem(this.image.camera);
        if (this.canEditImage) {
          this.checkFileUpdate();
        }
      });
  }

  private loadAnnotations() {
    this.imagesApiService
      .queryAnnotations(this.image.id)
      .subscribe((annotations) => {
        this.annotations = annotations;
      });
  }

  /**
   * Return to 3D viewport and jump to the camera location.
   */
  zoomImage(): void {
    if (!this.image.camera) {
      return;
    }

    const item = Cache.images.getByName(this.image.id);

    if (item) {
      this.router.navigate(['../..'], {
        relativeTo: this.route,
        queryParamsHandling: 'preserve',
      });
      item.focus();
    }
  }

  /**
   * Return to 3D viewport and enter spatialization mode.
   */
  spatialize(): void {
    this.router.navigate(['../..'], {
      relativeTo: this.route,
      queryParamsHandling: 'preserve',
    });

    this.globalEvents.callAction$.next({
      key: 'spatialize',
      image: this.image,
    });
  }

  /**
   * Update title of the image.
   */
  updateTitle(): void {
    // title property is required and cannot be empty
    if (!this.image.title.length) {
      this.notify.error('Image must have a title!');
      return;
    }

    // patch updates
    this.imagesApiService
      .patch(this.image.id, { title: this.image.title })
      .subscribe((result) => {
        this.image.editedBy = result.editedBy;
        // signal image update to application
        this.imageDataService.updated(this.image);
      });
  }

  validateImage(accept: boolean) {
    this.isUpdating = true;
    this.imagesApiService
      .updateValidationStatus(this.image.id, false, !accept)
      .pipe(
        finalize(() => {
          this.isUpdating = false;
        }),
      )
      .subscribe((result) => {
        this.image.pending = result.pending;
        this.image.declined = result.declined;
        this.imageDataService.updated(this.image);
      });
  }

  /**
   * Check if an image update (bigger resolution) is available.
   */
  private checkFileUpdate() {
    // only check for SLUB images
    if (!this.image.permalink) return;

    this.imagesApiService.checkFileUpdate(this.image.id).subscribe({
      next: (result) => {
        console.log(result);
        this.fileUpdate = result;
      },
      error: () => {
        console.log('No update available.');
      },
    });
  }

  /**
   * Trigger an update of the image file (exchange file with new one).
   */
  updateFile(): void {
    this.isUpdating = true;

    this.imagesApiService
      .updateFile(this.image.id)
      .pipe(
        finalize(() => {
          this.isUpdating = false;
        }),
      )
      .subscribe({
        next: (result) => {
          this.image.file = result;
          this.imageDataService.updated(this.image);
          this.fileUpdate = null;
        },
        error: () => {
          this.notify.error('File update failure!');
        },
      });
  }

  searchAnnotation(annotation: ImageAnnotationDto) {
    this.searchManager.setSearchParam(['ann:' + annotation.id], true, false);
  }

  openRecord(type: 'image' | 'text' | 'object', record: SimilarRecord) {
    this.router.navigate(
      ['/explore', this.route.snapshot.paramMap.get('scene'), type, record.id],
      {
        queryParamsHandling: 'preserve',
        fragment: record.annotations[0].id,
      },
    );
  }

  /**
   * Rotate image by 90 degree.
   * The user has to confirm this operation.
   */
  async rotateImage(angle: number): Promise<void> {
    const confirmed = await this.confirmService.confirm({
      message:
        'This operation will rotate the image (including thumbnail and preview images). Orientation, projections, or annotations may not be correct anymore! Do you want to continue?',
    });

    if (!confirmed) return;

    this.isUpdating = true;

    this.imagesApiService
      .rotateImageFile(this.image.id, angle)
      .pipe(
        finalize(() => {
          this.isUpdating = false;
        }),
      )
      .subscribe({
        next: (response) => {
          // add query parameter to force image reload
          response.thumb = response.thumb + '?t=' + Date.now();
          this.image.file = response;
          this.imageDataService.updated(this.image);
        },
        error: () => {
          this.notify.error('File update failure!');
        },
      });
  }

  /**
   * Trigger regeneration of thumbnails.
   */
  updateThumbnails(): void {
    this.isUpdating = true;

    this.imagesApiService
      .updateThumbnails(this.image.id)
      .pipe(
        finalize(() => {
          this.isUpdating = false;
        }),
      )
      .subscribe({
        next: (response) => {
          // add query parameter to force image reload
          response.thumb = response.thumb + '?t=' + Date.now();
          this.image.file = response;
          this.imageDataService.updated(this.image);
        },
        error: () => {
          this.notify.error('File update failure!');
        },
      });
  }

  linkToObjects() {
    this.isUpdating = true;

    this.imagesApiService
      .linkToObjects(this.image.id)
      .pipe(
        finalize(() => {
          this.isUpdating = false;
        }),
      )
      .subscribe({
        next: (response) => {
          console.log(response);
          this.notify.success('Image successfully linked to objects!');
        },
        error: () => {
          this.notify.error('Link to objects failed!');
        },
      });
  }

  /**
   * Remove image from database.
   * The user has to confirm this operation.
   * After successful removal, return to 3D viewport.
   */
  async removeImage(): Promise<void> {
    const confirmed = await this.confirmService.confirm({
      message:
        'This operation will remove the image from database and delete all corresponding files. Do you want to continue?',
    });

    if (!confirmed) return;

    this.imagesApiService.remove(this.image.id).subscribe(() => {
      this.searchManager.queryImages();
      this.router.navigate(['../..'], {
        relativeTo: this.route,
        queryParamsHandling: 'preserve',
      });
    });
  }

  /**
   * Toggle {@link #expertMode expertMode} when user presses <kbd>Shift</kbd> + <kbd>Alt</kbd> + <kbd>D</kbd>.
   */
  @HostListener('document:keyup.shift.alt.d')
  private toggleExpertMode(): void {
    this.expertMode = !this.expertMode;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
