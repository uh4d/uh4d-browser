import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {
  combineLatest,
  map,
  Observable,
  of,
  ReplaySubject,
  Subject,
  takeUntil,
} from 'rxjs';
import { NotificationsService } from 'angular2-notifications';
import {
  DateEditComponent,
  InlineEditComponent,
  RecordUserMetadataComponent,
  TagsEditComponent,
  TextareaEditComponent,
} from '@uh4d/frontend/ui-components';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import { AuthService } from '@uh4d/frontend/auth';
import {
  ImagesApiService,
  LegalBodiesApiService,
  PersonsApiService,
  TagsApiService,
} from '@uh4d/frontend/api-service';
import { ImageFullDto } from '@uh4d/dto/interfaces/custom/image';
import { UpdateImageDto, UpdateImageMetaDto } from '@uh4d/dto/interfaces';
import { ImageDataService } from '../image-data.service';

@Component({
  selector: 'uh4d-image-metadata',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    Uh4dIconsModule,
    RecordUserMetadataComponent,
    InlineEditComponent,
    DateEditComponent,
    TextareaEditComponent,
    TagsEditComponent,
  ],
  templateUrl: './image-metadata.component.html',
  styleUrls: ['./image-metadata.component.sass'],
})
export class ImageMetadataComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();
  private image$ = new ReplaySubject<ImageFullDto>(1);

  @Input({ required: true }) set image(value: ImageFullDto) {
    this.image$.next(value);
  }

  record!: ImageFullDto;

  canEditImage = false;

  constructor(
    private readonly authService: AuthService,
    private readonly notify: NotificationsService,
    private readonly imagesApiService: ImagesApiService,
    private readonly personsApiService: PersonsApiService,
    private readonly legalBodiesApiService: LegalBodiesApiService,
    private readonly tagsApiService: TagsApiService,
    private readonly imageDataService: ImageDataService,
  ) {
    this.queryAuthors = this.queryAuthors.bind(this);
    this.queryLegalBodies = this.queryLegalBodies.bind(this);
    this.queryTags = this.queryTags.bind(this);
  }

  ngOnInit() {
    combineLatest({
      image: this.image$,
      user: this.authService.user$,
    })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(({ image, user }) => {
        this.record = image;
        this.canEditImage =
          !!user && user.canEditItem(image.camera, image.uploadedBy);
      });
  }

  /**
   * Update metadata of the image. Only one property can be updated at once.
   * @param prop Name of property that should be updated
   */
  updateImage(prop: keyof UpdateImageDto | keyof UpdateImageMetaDto): void {
    switch (prop) {
      case 'title':
        // title property is required and can not be empty
        if (!this.record.title.length) {
          this.notify.error('Image must have a title!');
          return;
        }
        break;
      case 'permalink':
        // trim URL
        this.record.permalink = this.record.permalink?.trim() || null;
        break;
      default:
        // replace empty string with null on optional fields
        if (this.record[prop] === '') {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          this.record[prop] = null;
        }
    }

    // patch updates
    this.imagesApiService
      .patch(this.record.id, { [prop]: this.record[prop] })
      .subscribe({
        next: (result) => {
          this.record.editedBy = result.editedBy;
          if (prop === 'date') {
            this.record.date = result['date'] as ImageFullDto['date'];
          }
          // signal image update to application
          this.imageDataService.updated(this.record);
          // this.globalEvents.imageUpdate$.next(this.image);
        },
        error: (error) => {
          if (error.status === 400) {
            const msg = error.error?.message?.[0];
            this.notify.error('Invalid input!', msg);
          } else {
            this.notify.error(
              'Something went wrong!',
              'See Browser Console for more details.',
            );
          }
        },
      });
  }

  /**
   * Method passed to typeahead to query author names.
   * @param input Typed input
   */
  queryAuthors(input: string): Observable<string[]> {
    if (input) {
      return this.personsApiService
        .query(input)
        .pipe(map((results) => results.map((r) => r.value)));
    }

    return of([]);
  }

  /**
   * Method passed to typeahead to query name of owners/legal bodies.
   * @param input Typed input
   */
  queryLegalBodies(input: string): Observable<string[]> {
    if (input) {
      return this.legalBodiesApiService
        .query(input)
        .pipe(map((results) => results.map((r) => r.value)));
    }

    return of([]);
  }

  /**
   * Method passed to typeahead to query tags.
   * @param input Typed input
   */
  queryTags(input: string): Observable<string[]> {
    if (input) {
      return this.tagsApiService
        .query(input)
        .pipe(map((results) => results.map((r) => r.value)));
    }

    return of([]);
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
