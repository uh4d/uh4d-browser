import { inject } from '@angular/core';
import { ResolveFn, Router } from '@angular/router';
import { catchError, EMPTY } from 'rxjs';
import { ImageFullDto } from '@uh4d/dto/interfaces/custom/image';
import { ImagesApiService } from '@uh4d/frontend/api-service';

/**
 * When entering the route, query database for image entry with given `imageId` parameter.
 * If no entry found, navigate back to parent page.
 */
export const imageResolver: ResolveFn<ImageFullDto> = (route) => {
  const router = inject(Router);
  const imageId = route.paramMap.get('imageId');

  if (!imageId) {
    return EMPTY;
  }

  return inject(ImagesApiService)
    .get(imageId)
    .pipe(
      catchError(() => {
        let url = '';
        let parent = route.parent;
        while (parent) {
          if (parent.url[0]) {
            url = parent.url[0].path + '/' + url;
          }
          parent = parent.parent;
        }
        router.navigate([url], {
          queryParamsHandling: 'preserve',
        });
        return EMPTY;
      }),
    );
};
