import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FileUploader, FileUploadModule } from 'ng2-file-upload';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { NotificationsService } from 'angular2-notifications';
import { SearchManagerService } from '@uh4d/frontend/services';
import { JwtTokenService } from '@uh4d/frontend/auth';
import { coordsConverter } from '@uh4d/frontend/viewport3d-cache';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import { ImagePreviewDirective } from '@uh4d/frontend/directives';

/**
 * Modal to upload a multiple images at once.
 */
@Component({
  selector: 'uh4d-image-upload-modal',
  standalone: true,
  imports: [
    CommonModule,
    FileUploadModule,
    ProgressbarModule,
    Uh4dIconsModule,
    ImagePreviewDirective,
  ],
  templateUrl: './image-upload-modal.component.html',
  styleUrls: ['./image-upload-modal.component.sass'],
})
export class ImageUploadModalComponent implements OnInit {
  // config uploader
  uploader: FileUploader = new FileUploader({
    url: 'api/images',
    itemAlias: 'uploadImage',
    allowedFileType: ['image'],
    authToken: 'Bearer ' + this.jwtTokenService.getAccessToken(),
  });

  hasFileOverDropZone = false;

  constructor(
    private readonly modalRef: BsModalRef,
    private readonly notify: NotificationsService,
    private readonly searchManager: SearchManagerService,
    private readonly jwtTokenService: JwtTokenService,
  ) {}

  ngOnInit(): void {
    // listen to uploader events
    this.uploader.onWhenAddingFileFailed = (fileItem, filter) => {
      switch (filter.name) {
        case 'fileType':
          this.notify.warn(
            `File format ${fileItem.type} not supported!`,
            'Only common images are supported.',
          );
          break;
        default:
          this.notify.error('Unknown error!', 'See console.');
      }

      console.warn('onWhenAddingFileFailed', fileItem, filter);
    };

    this.uploader.onBuildItemForm = (fileItem, form) => {
      const origin = coordsConverter.getOrigin();
      form.append('latitude', origin.latitude);
      form.append('longitude', origin.longitude);
      // from this upload modal, we cannot define an exact location
      // hence big radius to define region
      form.append('radius', 10000);
    };

    this.uploader.onErrorItem = (fileItem, response, status) => {
      this.notify.error('Error while uploading!');
      console.error('onErrorItem', fileItem, response, status);
    };

    this.uploader.onCompleteAll = () => {
      this.searchManager.queryImages();
    };
  }

  /**
   * Start uploading all files.
   */
  uploadAll(): void {
    this.uploader.uploadAll();
  }

  /**
   * Check if there are any file items that are ready for upload.
   */
  isUploadEnabled(): boolean {
    return (
      this.uploader.queue.filter(
        (file) => !(file.isUploading || file.isUploaded),
      ).length > 0
    );
  }

  /**
   * Close modal.
   */
  close(): void {
    this.modalRef.hide();
  }
}
