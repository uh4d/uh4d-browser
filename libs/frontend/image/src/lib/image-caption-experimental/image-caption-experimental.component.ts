import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { finalize } from 'rxjs';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import { ImagesApiService } from '@uh4d/frontend/api-service';
import { ImageDto } from '@uh4d/dto/interfaces';

@Component({
  selector: 'uh4d-image-caption-experimental',
  standalone: true,
  imports: [CommonModule, Uh4dIconsModule],
  templateUrl: './image-caption-experimental.component.html',
  styleUrls: ['./image-caption-experimental.component.sass'],
})
export class ImageCaptionExperimentalComponent {
  @Input({ required: true }) set image(value: ImageDto) {
    this.value = value;
    this.imageCaption = null;
    this.showEmptyCaptionHint = false;
  }

  value!: ImageDto;
  imageCaption: string | null = null;
  showEmptyCaptionHint = false;

  isLoading = false;

  constructor(private readonly imagesApiService: ImagesApiService) {}

  generateCaption() {
    this.isLoading = true;
    this.imagesApiService
      .getImageCaption(this.value.id)
      .pipe(
        finalize(() => {
          this.isLoading = false;
        }),
      )
      .subscribe((response) => {
        this.imageCaption = response.caption;
        this.showEmptyCaptionHint = !response.caption;
      });
  }
}
