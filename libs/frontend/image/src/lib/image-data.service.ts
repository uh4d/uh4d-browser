import { Injectable } from '@angular/core';
import { ImageDto } from '@uh4d/dto/interfaces';
import { DataEventService } from '@uh4d/utils';

@Injectable({
  providedIn: 'root',
})
export class ImageDataService extends DataEventService<ImageDto> {}
