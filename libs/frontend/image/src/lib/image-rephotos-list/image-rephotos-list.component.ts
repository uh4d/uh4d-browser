import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import {
  combineLatest,
  filter,
  map,
  mergeMap,
  ReplaySubject,
  Subject,
  take,
  takeUntil,
} from 'rxjs';
import { ImageDto } from '@uh4d/dto/interfaces';
import { ImagesApiService } from '@uh4d/frontend/api-service';
import { RephotoWithTypeDto } from '@uh4d/dto/interfaces/custom/image';
import { RecordCardItemComponent } from '@uh4d/frontend/ui-components';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import { Cache } from '@uh4d/frontend/viewport3d-cache';
import { GlobalEventsService } from '@uh4d/frontend/services';
import { AuthService } from '@uh4d/frontend/auth';

@Component({
  selector: 'uh4d-image-rephotos-list',
  standalone: true,
  imports: [CommonModule, RouterLink, Uh4dIconsModule, RecordCardItemComponent],
  templateUrl: './image-rephotos-list.component.html',
  styleUrls: ['./image-rephotos-list.component.sass'],
})
export class ImageRephotosListComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();

  private image$ = new ReplaySubject<ImageDto>(1);

  @Input({ required: true }) set image(value: ImageDto) {
    this.image$.next(value);
  }

  @Output() recordClick = new EventEmitter<RephotoWithTypeDto>();

  hasRephoto: RephotoWithTypeDto[] = [];
  rephotoOf: RephotoWithTypeDto[] = [];

  canEditImage = false;

  constructor(
    private readonly imagesApiService: ImagesApiService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly globalEvents: GlobalEventsService,
    private readonly authService: AuthService,
  ) {}

  ngOnInit() {
    combineLatest({
      data: this.image$,
      user: this.authService.user$,
    })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(({ data, user }) => {
        this.loadRephotos(data.id);
        this.canEditImage =
          !!user && user.canEditItem(data.camera, data.uploadedBy);
      });
  }

  private loadRephotos(imageId: string) {
    this.imagesApiService.getRephotos(imageId).subscribe((rephotos) => {
      this.hasRephoto = rephotos.filter((r) => r.type === 'has-rephoto');
      this.rephotoOf = rephotos.filter((r) => r.type === 'rephoto-of');
    });
  }

  zoomRephoto(event: MouseEvent, rephoto: RephotoWithTypeDto) {
    event.stopPropagation();
    let item = Cache.images.getByName(rephoto.id);

    if (!item) {
      this.globalEvents.callAction$.next({
        key: 'load-temp-image',
        image: rephoto as unknown as ImageDto,
      });
      item = Cache.images.getByName(rephoto.id);
    }

    if (item) {
      this.router.navigate(['../..'], {
        relativeTo: this.route,
        queryParamsHandling: 'preserve',
      });
      item.toggle(true);
      setTimeout(() => {
        item!.focus();
      }, 200);
    }
  }

  async adoptRephotoSpatial(event: MouseEvent, rephoto: RephotoWithTypeDto) {
    event.stopPropagation();
    // set rephotos orientation
    await this.router.navigate(['../..'], {
      relativeTo: this.route,
      queryParamsHandling: 'preserve',
    });
    this.globalEvents.callAction$.next({
      key: 'set-camera-view',
      camera: rephoto.camera,
    });

    // open spatialization mode
    this.image$
      .pipe(
        take(1),
        mergeMap((image) =>
          this.globalEvents.callAction$.pipe(
            filter((event) => event.key === 'camera-tween-complete'),
            take(1),
            map(() => image),
          ),
        ),
      )
      .subscribe((image) => {
        this.globalEvents.callAction$.next({
          key: 'spatialize',
          image,
        });
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
