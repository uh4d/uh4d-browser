import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Params, Router, RouterLink } from '@angular/router';
import { combineLatest, finalize, Subject, takeUntil, tap } from 'rxjs';
import { NotificationsService } from 'angular2-notifications';
import { NgxPermissionsModule } from 'ngx-permissions';
import {
  InlineEditComponent,
  MapInteractiveComponent,
  RecordValidationBoxComponent,
} from '@uh4d/frontend/ui-components';
import { ImageFullDto } from '@uh4d/dto/interfaces/custom/image';
import { AuthService } from '@uh4d/frontend/auth';
import { ImagesApiService } from '@uh4d/frontend/api-service';
import { ImageSvgViewerComponent } from '../image-svg-viewer/image-svg-viewer.component';
import { ImageMetadataComponent } from '../image-metadata/image-metadata.component';
import { ImageDataService } from '../image-data.service';

@Component({
  selector: 'uh4d-image-sheet-compact',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ImageSvgViewerComponent,
    InlineEditComponent,
    RouterLink,
    ImageMetadataComponent,
    NgxPermissionsModule,
    RecordValidationBoxComponent,
    MapInteractiveComponent,
  ],
  templateUrl: './image-sheet-compact.component.html',
  styleUrls: ['./image-sheet-compact.component.sass'],
})
export class ImageSheetCompactComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();

  image!: ImageFullDto;

  canEditImage = false;
  canValidate = false;

  isUpdating = false;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly authService: AuthService,
    private readonly imagesApiService: ImagesApiService,
    private readonly imageDataService: ImageDataService,
    private readonly notify: NotificationsService,
  ) {}

  ngOnInit() {
    combineLatest({
      // get image data from resolver
      data: this.route.data.pipe(
        tap((data) => {
          this.image = data['image'];
          console.log(this.image);
        }),
      ),
      user: this.authService.user$,
    })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(({ user }) => {
        this.canEditImage =
          !!user && user.canEditItem(this.image.camera, this.image.uploadedBy);
        this.canValidate = !!user && user?.canEditItem(this.image.camera);
        // if (this.canEditImage) {
        //   this.checkFileUpdate();
        // }
      });
  }

  /**
   * Update title of the image.
   */
  updateTitle(): void {
    // title property is required and can not be empty
    if (!this.image.title.length) {
      this.notify.error('Image must have a title!');
      return;
    }

    // patch updates
    this.imagesApiService
      .patch(this.image.id, { title: this.image.title })
      .subscribe((result) => {
        this.image.editedBy = result.editedBy;
        // signal image update to application
        this.imageDataService.updated(this.image);
      });
  }

  validateImage(accept: boolean) {
    this.isUpdating = true;
    this.imagesApiService
      .updateValidationStatus(this.image.id, false, !accept)
      .pipe(
        finalize(() => {
          this.isUpdating = false;
        }),
      )
      .subscribe((result) => {
        this.image.pending = result.pending;
        this.image.declined = result.declined;
        this.imageDataService.updated(this.image);
      });
  }

  openScene() {
    const queryParams: Params = {};
    if (this.image.pending) {
      queryParams['q'] = 'pending';
    } else if (this.image.declined) {
      queryParams['q'] = 'declined';
    }
    if (this.image.date?.from) {
      queryParams['from'] = this.image.date.from;
    }
    if (this.image.date?.to) {
      queryParams['to'] = this.image.date.to;
    }

    const url = this.router.serializeUrl(
      this.router.createUrlTree(
        [
          '/explore',
          `${this.image.camera.latitude},${this.image.camera.longitude}`,
        ],
        { queryParams },
      ),
    );

    window.open(url, '_blank');
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
