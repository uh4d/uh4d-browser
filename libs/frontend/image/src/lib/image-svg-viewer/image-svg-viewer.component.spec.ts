import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageSvgViewerComponent } from './image-svg-viewer.component';

describe('ImageSvgViewerComponent', () => {
  let component: ImageSvgViewerComponent;
  let fixture: ComponentFixture<ImageSvgViewerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImageSvgViewerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ImageSvgViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
