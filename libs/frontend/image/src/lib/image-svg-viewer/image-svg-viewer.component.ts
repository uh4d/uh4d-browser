import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  ViewChild,
} from '@angular/core';
import { CommonModule, Location } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  BehaviorSubject,
  combineLatest,
  ReplaySubject,
  Subject,
  takeUntil,
} from 'rxjs';
import * as d3 from 'd3';
import { Vector2 } from 'three';
import { NgxPermissionsService } from 'ngx-permissions';
import { stringToColor } from '@uh4d/utils';
import {
  ImageAnnotationDto,
  ImageFileDto,
  NormDataNodeDto,
} from '@uh4d/dto/interfaces';
import { ContextMenuService } from '@uh4d/frontend/context-menu';
import { SimilarAnnotationsRecord } from '@uh4d/dto/interfaces/custom/annotation';

@Component({
  selector: 'uh4d-image-svg-viewer',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './image-svg-viewer.component.html',
  styleUrls: ['./image-svg-viewer.component.sass'],
})
export class ImageSvgViewerComponent implements AfterViewInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();
  private file$ = new ReplaySubject<ImageFileDto>(1);
  private restricted$ = new BehaviorSubject<boolean>(false);
  private annotations$ = new ReplaySubject<ImageAnnotationDto[]>(1);

  @ViewChild('svgRoot', { static: true }) svgRoot!: ElementRef<SVGSVGElement>;

  @Input() set file(value: ImageFileDto) {
    this.file$.next(value);
  }

  /**
   * If set `true`, the viewer will only display low-resolution thumbnail image.
   */
  @Input() set restricted(value: boolean) {
    this.restricted$.next(value);
  }

  @Input() set annotations(value: ImageAnnotationDto[]) {
    this.annotations$.next(value);
  }

  @Output() annotationQueried = new EventEmitter<ImageAnnotationDto>();

  private canvasWidth = 0;
  private canvasHeight = 0;
  private canvasAspect = 1;
  private imageWidth = 0;
  private imageHeight = 0;
  private imageAspect = 1;

  private svg!: d3.Selection<SVGSVGElement, undefined, any, undefined>;
  private container!: d3.Selection<
    SVGGElement,
    undefined,
    SVGGElement,
    undefined
  >;
  private image!: d3.Selection<
    SVGImageElement,
    undefined,
    SVGGElement,
    undefined
  >;
  private group!: d3.Selection<SVGGElement, undefined, SVGGElement, undefined>;
  private zoom!: d3.ZoomBehavior<SVGSVGElement, undefined>;

  showAnnotations = false;
  annotationData: ImageAnnotationDto[] = [];

  tooltipPosition = { left: 0, top: 0 };
  activeAnnotation: ImageAnnotationDto | null = null;
  wikidata: NormDataNodeDto[] = [];
  aat: NormDataNodeDto[] = [];

  private resizeObserver = new ResizeObserver(() => {
    this.resize();
  });

  constructor(
    private readonly permissionsService: NgxPermissionsService,
    private readonly contextMenuService: ContextMenuService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly location: Location,
  ) {}

  ngAfterViewInit() {
    this.svg = d3.select(this.svgRoot.nativeElement);
    this.container = this.svg.select('g');
    this.image = this.container.append<SVGImageElement>('svg:image');
    this.group = this.container.append('g');

    this.resize();
    this.resizeObserver.observe(this.svgRoot.nativeElement);

    this.zoom = d3
      .zoom<SVGSVGElement, undefined>()
      .constrain((transform) => this.constrainZoom(transform))
      .on('zoom', (event) => {
        this.container.attr('transform', event.transform);
      });
    this.svg.call(this.zoom);
    this.svg.on('click', (event) => this.clickBackground(event));

    combineLatest({
      file: this.file$,
      restrict: this.restricted$,
    })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(({ file, restrict }) => {
        this.clearAnnotations();
        this.loadImage(file, restrict);
      });

    this.annotations$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((annotationData) => {
        this.annotationData = annotationData;
        if (this.route.snapshot.fragment) this.showAnnotations = true;
        if (this.showAnnotations) {
          this.drawAnnotations(annotationData);
        }
      });
  }

  private loadImage(file: ImageFileDto, restrict = false): void {
    const fileName = restrict
      ? file.thumb
      : /\.tiff?$/i.test(file.original)
      ? file.preview
      : file.original;
    this.image
      .attr('xlink:href', 'data/' + file.path + fileName)
      .attr('width', file.width)
      .attr('height', file.height);

    this.imageWidth = file.width;
    this.imageHeight = file.height;
    this.imageAspect = file.width / file.height;
    this.canvasAspect = this.canvasWidth / this.canvasHeight;

    setTimeout(() => {
      const initTransform = this.getInitialTransform();
      if (initTransform) {
        this.setScaleExtent(initTransform.k);
        this.zoom.transform(this.svg, initTransform);
      }
    });
  }

  private constrainZoom(transform: d3.ZoomTransform): d3.ZoomTransform {
    const imageWidth = this.imageWidth * transform.k;
    const imageHeight = this.imageHeight * transform.k;
    const maxX = this.canvasWidth - imageWidth;
    const maxY = this.canvasHeight - imageHeight;

    let x = transform.x;
    let y = transform.y;

    if (imageWidth < this.canvasWidth) {
      x = Math.max(0, Math.min(maxX, x));
    } else {
      x = Math.min(0, Math.max(maxX, x));
    }
    if (imageHeight < this.canvasHeight) {
      y = Math.max(0, Math.min(maxY, y));
    } else {
      y = Math.min(0, Math.max(maxY, y));
    }

    return new d3.ZoomTransform(transform.k, x, y);
  }

  /**
   * Get zoom scale extend according to canvas and image dimensions.
   */
  private getInitialTransform(): d3.ZoomTransform | null {
    if (this.imageWidth === 0 || this.imageHeight === 0) return null;

    if (this.imageAspect > this.canvasAspect) {
      const gap = (this.canvasHeight - this.canvasWidth / this.imageAspect) / 2;
      return new d3.ZoomTransform(this.canvasWidth / this.imageWidth, 0, gap);
    } else {
      const gap = (this.canvasWidth - this.canvasHeight * this.imageAspect) / 2;
      return new d3.ZoomTransform(this.canvasHeight / this.imageHeight, gap, 0);
    }
  }

  /**
   * Set zoom scale constraint.
   */
  private setScaleExtent(k: number): void {
    this.zoom.scaleExtent([k, k * 20]);
  }

  private resize() {
    const bbox = this.svg.node()?.getBoundingClientRect();
    if (!bbox) return;
    this.canvasWidth = bbox.width;
    this.canvasHeight = bbox.height;

    const initTransform = this.getInitialTransform();
    const containerNode = this.container.node();
    if (initTransform && containerNode) {
      this.setScaleExtent(initTransform.k);
      const currentTransform = d3.zoomTransform(containerNode);
      const finalTransform = this.constrainZoom(
        new d3.ZoomTransform(
          Math.max(currentTransform.k, initTransform.k),
          currentTransform.x,
          currentTransform.y,
        ),
      );
      this.svg.call(this.zoom.transform, finalTransform);
    }
  }

  onShowAnnotationsChange(): void {
    if (this.showAnnotations) {
      this.drawAnnotations(this.annotationData);
    } else {
      this.clearAnnotations();
    }
  }

  private clearAnnotations() {
    this.group.selectAll('g.annotation-poly').remove();
    this.deselectAnnotation();
  }

  private drawAnnotations(annotationData: ImageAnnotationDto[]) {
    // remove old paths
    this.clearAnnotations();

    // bigger annotations should be in the background
    annotationData.sort((a, b) => b.area - a.area);

    // get weighted annotations from navigation history
    type StateAnnotationRecords = {
      annotations?: Record<string, SimilarAnnotationsRecord['annotations']>;
    };
    const weightedAnnotations = (
      this.location.getState() as StateAnnotationRecords
    ).annotations;

    // draw paths
    const lineGenerator = d3
      .line()
      .x((d) => d[0] * this.imageWidth)
      .y((d) => d[1] * this.imageHeight)
      .curve(d3.curveLinearClosed);

    this.group
      .selectAll('path')
      .data<ImageAnnotationDto>(annotationData)
      .enter()
      .append('g')
      .attr('id', (d) => d.id)
      .attr('class', 'annotation-poly')
      .each((d, index, group) => {
        let color = '';
        if (weightedAnnotations) {
          const t = Object.values(weightedAnnotations).reduce((acc, curr) => {
            const wa = curr.find((ann) => ann.id === d.id);
            return Math.max(acc, wa ? wa.similarity : 0);
          }, 0);
          color = d3.interpolateViridis(t);
        } else {
          const identifierCode = d.wikidata[0]
            ? d.wikidata[0].identifier + d.wikidata[0].label
            : d.aat[0]
            ? d.aat[0].identifier + d.aat[0].label
            : '';
          color = stringToColor(identifierCode);
        }

        const polyGroup = d3.select(group[index]);
        d.polylist.forEach((poly) => {
          polyGroup
            .append('path')
            .style('fill', color)
            .style('stroke', color)
            .attr('d', lineGenerator(poly as [number, number][]));
        });
      })
      .on('click', (event, d) => this.clickAnnotation(event, d))
      .on('mouseover', (event, d) => this.mouseoverAnnotation(d))
      .on('mouseleave', (event, d) => this.mouseleaveAnnotation(d))
      .on('contextmenu', (event, d) => this.rightClickAnnotation(event, d));

    // highlight annotation from fragment
    const fragment = this.route.snapshot.fragment;
    if (fragment) {
      const ann = this.annotationData.find((d) => d.id === fragment);
      if (ann) {
        setTimeout(() => {
          const rect = d3
            .select<SVGGElement, ImageAnnotationDto>('#' + ann.id)
            .node()!
            .getBBox();
          const transform = d3.zoomTransform(this.container.node()!);
          this.tooltipPosition.left =
            (rect.x + rect.width / 2) * transform.k + transform.x;
          this.tooltipPosition.top =
            (rect.y + rect.height / 2) * transform.k + transform.y - 5;

          this.selectAnnotation(ann);
        }, 200);
      }
    }
  }

  private clickAnnotation(event: MouseEvent, d: ImageAnnotationDto): void {
    if (this.activeAnnotation !== d) {
      this.deselectAnnotation();
      this.selectAnnotation(d);
    }
    // set tooltip position
    const [x, y] = d3.pointer(event, this.svg.node());
    this.tooltipPosition.left = x + 10;
    this.tooltipPosition.top = y - 5;
  }

  private clickBackground(event: MouseEvent): void {
    if (event.target instanceof Element && event.target.nodeName !== 'path') {
      this.deselectAnnotation();
    }
  }

  private rightClickAnnotation(event: MouseEvent, d: ImageAnnotationDto): void {
    this.contextMenuService.open(event, {
      title: d.wikidata[0]?.label || d.aat[0]?.label || 'Annotation',
      actions: [
        {
          icon: 'filter-custom',
          label: 'Search by annotation',
          click: () => {
            this.annotationQueried.emit(d);
          },
        },
        {
          icon: 'info-circle',
          label: 'Open details...',
          click: () => {
            this.router.navigate(
              [
                '/explore',
                this.route.snapshot.paramMap.get('scene'),
                'normdata',
                d.wikidata[0]?.id || d.aat[0]?.id,
              ],
              {
                queryParamsHandling: 'preserve',
              },
            );
          },
        },
      ],
    });
  }

  private selectAnnotation(d: ImageAnnotationDto): void {
    const el = d3.select('#' + d.id);
    el.classed('active', true);
    this.wikidata = d.wikidata;
    this.aat = d.aat;
    this.activeAnnotation = d;
  }

  private deselectAnnotation(): void {
    if (!this.activeAnnotation) return;
    const el = d3.select('#' + this.activeAnnotation.id);
    el.classed('active', false);
    this.wikidata = [];
    this.aat = [];
    this.activeAnnotation = null;
  }

  private mouseoverAnnotation(d: ImageAnnotationDto) {
    if (!this.permissionsService.getPermission('showDebug')) return;

    // render debugging lines
    const center = new Vector2(
      (d.bbox[0] + d.bbox[2] / 2) * this.imageWidth,
      (d.bbox[1] + d.bbox[3] / 2) * this.imageHeight,
    );

    const group = d3.select('#' + d.id);
    group
      .selectAll('g.weights')
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      .data<{ id: string; weight: number }>(d.neighbors)
      .enter()
      .append('g')
      .attr('id', (dn) => dn.id)
      .attr('class', 'weights')
      .each((dn, index, group) => {
        const neighbor = this.annotationData.find((ann) => ann.id === dn.id);
        if (!neighbor) return;
        const nCenter = new Vector2(
          (neighbor.bbox[0] + neighbor.bbox[2] / 2) * this.imageWidth,
          (neighbor.bbox[1] + neighbor.bbox[3] / 2) * this.imageHeight,
        );
        const lineGroup = d3.select(group[index]);
        lineGroup
          .append('line')
          .attr('x1', center.x)
          .attr('y1', center.y)
          .attr('x2', nCenter.x)
          .attr('y2', nCenter.y);
        lineGroup
          .append('text')
          .attr('x', nCenter.x)
          .attr('y', nCenter.y)
          .text(dn.weight.toFixed(3));
      });
  }

  private mouseleaveAnnotation(d: ImageAnnotationDto) {
    const group = d3.select('#' + d.id);
    group.selectAll('g.weights').remove();
  }

  ngOnDestroy(): void {
    this.resizeObserver.disconnect();
    this.file$.complete();
    this.restricted$.complete();
    this.annotations$.complete();
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
