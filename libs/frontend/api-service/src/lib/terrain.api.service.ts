import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { TerrainDto } from '@uh4d/dto/interfaces';
import { OsmGeneratedDto } from '@uh4d/dto/interfaces/custom/osm';
import { BaseApiService } from './base.api.service';

@Injectable({
  providedIn: 'root',
})
export class TerrainApiService extends BaseApiService {
  override readonly routeBase = '/terrain';

  constructor(private readonly http: HttpClient) {
    super();
  }

  query(params: { lat?: number; lon?: number }): Observable<TerrainDto[]> {
    return this.http.get<TerrainDto[]>(this.baseUrl(), { params });
  }

  get(id: string): Observable<TerrainDto> {
    return this.http.get<TerrainDto>(this.baseUrl(id));
  }

  requestElevationApi(params: {
    lat: number;
    lon: number;
    r: number;
  }): Observable<string> {
    return this.http
      .get<any>(this.apiBaseUrl + '/elevation', {
        params: {
          imageryProvider: 'MapBox-SatelliteStreet',
          textureQuality: 3,
          ...params,
        },
      })
      .pipe(
        map(
          (value) => `${this.apiBaseUrl}/elevation${value.assetInfo.modelFile}`,
        ),
      );
  }

  queryOSMFile(params: {
    terrain: string;
    lat: number;
    lon: number;
    r: number;
  }) {
    return this.http.get<OsmGeneratedDto>(this.apiBaseUrl + '/osm', {
      params,
    });
  }
}
