import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  ObjectDto,
  ObjectAnnotationDto,
  UpdateObjectDto,
} from '@uh4d/dto/interfaces';
import {
  ObjectFullDto,
  SaveObjectDto,
} from '@uh4d/dto/interfaces/custom/object';
import { DataUtilsService } from './utils/data-utils.service';
import { BaseApiService } from './base.api.service';

@Injectable({
  providedIn: 'root',
})
export class ObjectsApiService extends BaseApiService {
  override readonly routeBase = '/objects';

  constructor(
    private readonly http: HttpClient,
    private readonly utils: DataUtilsService,
  ) {
    super();
  }

  query(params?: {
    date?: string;
    lat?: number;
    lon?: number;
    r?: number;
    q?: string[];
    scene?: string;
  }): Observable<ObjectDto[]> {
    return this.http
      .get<ObjectDto>(this.baseUrl(), { params })
      .pipe(this.utils.catchError());
  }

  get(id: string): Observable<ObjectFullDto> {
    return this.http
      .get<ObjectFullDto>(this.baseUrl(id))
      .pipe(this.utils.catchError());
  }

  patch(id: string, body: UpdateObjectDto): Observable<ObjectFullDto> {
    return this.http
      .patch<ObjectFullDto>(this.baseUrl(id), body)
      .pipe(this.utils.catchError());
  }

  remove(id: string): Observable<void> {
    return this.http.delete<void>(this.baseUrl(id));
  }

  updateValidationStatus(
    id: string,
    pending: boolean,
    declined = false,
  ): Observable<ObjectFullDto> {
    return this.http.patch<ObjectFullDto>(this.baseUrl(id) + '/validation', {
      pending,
      declined,
    });
  }

  duplicate(obj: ObjectDto): Observable<ObjectDto> {
    return this.http
      .post<ObjectDto>(this.baseUrl(obj.id) + '/duplicate', {
        name: obj.name,
      })
      .pipe(this.utils.catchError());
  }

  save(
    body: SaveObjectDto, //TempObjectDto & LocationOrientationDto & { matrix: number[] },
  ): Observable<ObjectFullDto> {
    return this.http
      .post<ObjectFullDto>(this.baseUrl(), body)
      .pipe(this.utils.catchError());
  }

  removeUpload(id: string): Observable<void> {
    return this.http
      .delete(this.baseUrl() + `/tmp/${id}`)
      .pipe(this.utils.catchError());
  }

  queryAnnotations(id: string): Observable<ObjectAnnotationDto[]> {
    return this.http.get<ObjectAnnotationDto[]>(
      this.baseUrl(id) + '/annotations',
    );
  }
}
