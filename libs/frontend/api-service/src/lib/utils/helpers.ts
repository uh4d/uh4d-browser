import { Params } from '@angular/router';

export function stripEmptyParams(params?: Params) {
  if (!params) return;
  Object.entries(params).forEach(([key, value]) => {
    if (value === undefined || value === null) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      delete params[key];
    }
  });
}
