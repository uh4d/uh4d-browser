import { Injectable } from '@angular/core';
import {
  catchError,
  EMPTY,
  ObservableInput,
  ObservedValueOf,
  OperatorFunction,
} from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { NotificationsService } from 'angular2-notifications';

@Injectable({
  providedIn: 'root',
})
export class DataUtilsService {
  constructor(private notify: NotificationsService) {}

  catchError<T, O extends ObservableInput<any>>(
    msg?: string,
  ): OperatorFunction<T, T | ObservedValueOf<O>> {
    return catchError((err: HttpErrorResponse) => {
      console.error(err);
      this.notify.error(msg || 'An error occurred!', err.error.message);
      return EMPTY;
    });
  }
}
