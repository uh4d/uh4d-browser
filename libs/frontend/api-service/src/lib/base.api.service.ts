export abstract class BaseApiService {
  readonly apiBaseUrl = '/api';
  readonly routeBase: string = '';

  baseUrl(id?: string) {
    return this.apiBaseUrl + this.routeBase + (id ? `/${id}` : '');
  }
}
