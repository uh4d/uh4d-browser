import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DateExtents } from '@uh4d/dto/interfaces/custom/dates';
import { BaseApiService } from './base.api.service';

@Injectable({
  providedIn: 'root',
})
export class DatesApiService extends BaseApiService {
  override readonly routeBase = '/dates';

  constructor(private readonly http: HttpClient) {
    super();
  }

  getExtent(params: {
    lat?: number;
    lon?: number;
    r?: number;
  }): Observable<DateExtents> {
    return this.http.get<DateExtents>(this.baseUrl() + '/extent', {
      params,
    });
  }
}
