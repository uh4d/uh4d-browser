import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, tap } from 'rxjs';
import { SimilarAnnotationsDto } from '@uh4d/dto/interfaces/custom/annotation';
import {
  ImageAnnotationDto,
  ObjectAnnotationDto,
  TextAnnotationDto,
} from '@uh4d/dto/interfaces';
import { DataUtilsService } from './utils/data-utils.service';
import { BaseApiService } from './base.api.service';

@Injectable({
  providedIn: 'root',
})
export class AnnotationsApiService extends BaseApiService {
  override readonly routeBase = '/annotations';

  constructor(
    private readonly http: HttpClient,
    private readonly utils: DataUtilsService,
  ) {
    super();
  }

  get(
    id: string,
  ): Observable<ImageAnnotationDto | TextAnnotationDto | ObjectAnnotationDto> {
    return this.http
      .get<ImageAnnotationDto | TextAnnotationDto | ObjectAnnotationDto>(
        this.baseUrl(id),
      )
      .pipe(this.utils.catchError());
  }

  querySimilarAnnotations(id: string): Observable<SimilarAnnotationsDto> {
    return this.http
      .get<SimilarAnnotationsDto>(this.baseUrl(id) + '/similar')
      .pipe(
        tap((result) => {
          // sort annotation by similarity
          result.images.forEach((img) => {
            img.annotations.sort((a, b) => b.similarity - a.similarity);
          });
          result.texts.forEach((text) => {
            text.annotations.sort((a, b) => b.similarity - a.similarity);
          });
          result.objects.forEach((obj) => {
            obj.annotations.sort((a, b) => b.similarity - a.similarity);
          });
        }),
        this.utils.catchError(),
      );
  }
}
