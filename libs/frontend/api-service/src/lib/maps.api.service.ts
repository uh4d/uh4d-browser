import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MapDto } from '@uh4d/dto/interfaces';
import { BaseApiService } from './base.api.service';

@Injectable({
  providedIn: 'root',
})
export class MapsApiService extends BaseApiService {
  override readonly routeBase = '/terrain';

  constructor(private readonly http: HttpClient) {
    super();
  }

  query(terrainId: string, params?: { date?: string }): Observable<MapDto[]> {
    return this.http.get<MapDto[]>(this.baseUrl(terrainId) + '/maps', {
      params,
    });
  }
}
