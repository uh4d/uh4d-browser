import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  ImageExtendedDto,
  ImageFileUpdateDto,
  ImageFullDto,
  RephotoWithTypeDto,
} from '@uh4d/dto/interfaces/custom/image';
import {
  ImageAnnotationDto,
  ImageFileDto,
  UpdateImageDto,
  UpdateImageMetaDto,
} from '@uh4d/dto/interfaces';
import { DataUtilsService } from './utils/data-utils.service';
import { BaseApiService } from './base.api.service';
import { stripEmptyParams } from './utils/helpers';

@Injectable({
  providedIn: 'root',
})
export class ImagesApiService extends BaseApiService {
  override readonly routeBase = '/images';

  constructor(
    private readonly http: HttpClient,
    private readonly utils: DataUtilsService,
  ) {
    super();
  }

  query(params: {
    q?: string[];
    from?: string;
    to?: string;
    undated?: string;
    lat?: number;
    lon?: number;
    r?: number;
  }): Observable<ImageExtendedDto[]> {
    stripEmptyParams(params);
    return this.http
      .get<ImageExtendedDto[]>(this.baseUrl(), { params })
      .pipe(this.utils.catchError());
  }

  get(id: string): Observable<ImageFullDto> {
    return this.http.get<ImageFullDto>(this.baseUrl(id));
  }

  patch(
    id: string,
    body: UpdateImageDto & UpdateImageMetaDto,
  ): Observable<ImageFullDto> {
    return this.http.patch<ImageFullDto>(this.baseUrl(id), body);
  }

  remove(id: string): Observable<void> {
    return this.http.delete<void>(this.baseUrl(id));
  }

  updateValidationStatus(
    id: string,
    pending: boolean,
    declined = false,
  ): Observable<ImageFullDto> {
    return this.http.patch<ImageFullDto>(this.baseUrl(id) + '/validation', {
      pending,
      declined,
    });
  }

  checkFileUpdate(id: string): Observable<ImageFileUpdateDto> {
    return this.http.get<ImageFileUpdateDto>(this.baseUrl(id) + '/file/check');
  }

  updateFile(id: string): Observable<ImageFileDto> {
    return this.http.get<ImageFileDto>(this.baseUrl(id) + '/file/update');
  }

  rotateImageFile(id: string, angle: number): Observable<ImageFileDto> {
    return this.http.post<ImageFileDto>(this.baseUrl(id) + '/file/rotate', {
      angle,
    });
  }

  updateThumbnails(id: string): Observable<ImageFileDto> {
    return this.http.post<ImageFileDto>(
      this.baseUrl(id) + '/file/thumbnails',
      {},
    );
  }

  queryAnnotations(id: string): Observable<ImageAnnotationDto[]> {
    return this.http.get<ImageAnnotationDto[]>(
      this.baseUrl(id) + '/annotations',
    );
  }

  getImageCaption(id: string): Observable<{ caption: string }> {
    return this.http.get<{ caption: string }>(this.baseUrl(id) + '/caption');
  }

  linkToObjects(id: string) {
    return this.http.post(this.baseUrl(id) + '/objectWeights', null);
  }

  getRephotos(id: string): Observable<RephotoWithTypeDto[]> {
    return this.http.get<RephotoWithTypeDto[]>(
      `${this.apiBaseUrl}/rephoto/${id}`,
    );
  }
}
