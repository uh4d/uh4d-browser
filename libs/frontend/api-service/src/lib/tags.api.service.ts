import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseApiService } from './base.api.service';

@Injectable({
  providedIn: 'root',
})
export class TagsApiService extends BaseApiService {
  override readonly routeBase = '/tags';

  constructor(private readonly http: HttpClient) {
    super();
  }

  query(search?: string): Observable<{ value: string }[]> {
    return this.http.get<{ value: string }[]>(this.baseUrl(), {
      params: search ? { search } : {},
    });
  }
}
