import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  LocationDto,
  Uh4dPoiDto,
  UpdateUh4dPoiDto,
} from '@uh4d/dto/interfaces';
import { BaseApiService } from './base.api.service';
import { stripEmptyParams } from './utils/helpers';

@Injectable({
  providedIn: 'root',
})
export class PoisApiService extends BaseApiService {
  override readonly routeBase = '/pois';

  constructor(private readonly http: HttpClient) {
    super();
  }

  query(params?: {
    date?: string;
    lat?: number;
    lon?: number;
    r?: number;
    q?: string | string[];
    scene?: string;
  }): Observable<Uh4dPoiDto[]> {
    stripEmptyParams(params);
    return this.http.get<Uh4dPoiDto[]>(this.baseUrl(), { params });
  }

  get(id: string): Observable<Uh4dPoiDto> {
    return this.http.get<Uh4dPoiDto>(this.baseUrl(id));
  }

  create(body: {
    title: string;
    content: string;
    location: LocationDto;
    date: { from?: string; to?: string; objectBound: boolean };
    objectId?: string;
    camera?: number[];
    damageFactors?: string[];
  }): Observable<Uh4dPoiDto> {
    return this.http.post<Uh4dPoiDto>(this.baseUrl(), {
      ...body,
      type: 'uh4d',
    });
  }

  patch(id: string, body: UpdateUh4dPoiDto): Observable<Uh4dPoiDto> {
    return this.http.patch<Uh4dPoiDto>(this.baseUrl(id), body);
  }

  remove(id: string): Observable<void> {
    return this.http.delete<void>(this.baseUrl(id));
  }

  updateValidationStatus(
    id: string,
    pending: boolean,
    declined = false,
  ): Observable<Uh4dPoiDto> {
    return this.http.patch<Uh4dPoiDto>(this.baseUrl(id) + '/validation', {
      pending,
      declined,
    });
  }
}
