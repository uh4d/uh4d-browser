import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  NormDataDescriptionDto,
  NormDataMetaDto,
} from '@uh4d/dto/interfaces/custom/norm-data';
import { DataUtilsService } from './utils/data-utils.service';
import { BaseApiService } from './base.api.service';

@Injectable({
  providedIn: 'root',
})
export class NormDataApiService extends BaseApiService {
  override readonly routeBase = '/normdata';

  constructor(
    private readonly http: HttpClient,
    private readonly utils: DataUtilsService,
  ) {
    super();
  }

  get(id: string): Observable<NormDataMetaDto> {
    return this.http
      .get<NormDataMetaDto>(this.baseUrl(id))
      .pipe(this.utils.catchError());
  }

  getDescription(id: string): Observable<NormDataDescriptionDto> {
    return this.http
      .get<NormDataDescriptionDto>(this.baseUrl(id) + '/description')
      .pipe(this.utils.catchError());
  }
}
