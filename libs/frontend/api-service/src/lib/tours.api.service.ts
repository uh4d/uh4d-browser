import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TourDto } from '@uh4d/dto/interfaces/custom/poi';
import { BaseApiService } from './base.api.service';

@Injectable({
  providedIn: 'root',
})
export class ToursApiService extends BaseApiService {
  override readonly routeBase = '/tours';

  constructor(private readonly http: HttpClient) {
    super();
  }

  query(params?: {
    lat?: number;
    lon?: number;
    r?: number;
  }): Observable<TourDto[]> {
    return this.http.get<TourDto[]>(this.baseUrl(), { params });
  }

  create(title: string, pois: string[]): Observable<TourDto> {
    return this.http.post<TourDto>(this.baseUrl(), {
      title,
      pois,
    });
  }

  remove(id: string): Observable<void> {
    return this.http.delete<void>(this.baseUrl(id));
  }
}
