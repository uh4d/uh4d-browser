import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  GeofenceDto,
  ImageDto,
  ObjectDto,
  TextDto,
  Uh4dPoiDto,
  UserDto,
} from '@uh4d/dto/interfaces';
import { BaseApiService } from './base.api.service';

@Injectable({
  providedIn: 'root',
})
export class UsersApiService extends BaseApiService {
  override readonly routeBase = '/users';

  constructor(private readonly http: HttpClient) {
    super();
  }

  query(): Observable<UserDto[]> {
    return this.http.get<UserDto[]>(this.baseUrl());
  }

  getProfileData(): Observable<UserDto> {
    return this.http.get<UserDto>(this.baseUrl() + '/profile');
  }

  updateUserRole(userId: string, role: string | null): Observable<void> {
    return this.http.patch<void>(this.baseUrl(userId) + '/role', { role });
  }

  updateUserGeofence(
    userId: string,
    geofences: GeofenceDto[],
  ): Observable<void> {
    return this.http.patch<void>(this.baseUrl(userId) + '/geofence', {
      geofences,
    });
  }

  getUserImages(
    userId: string,
    params: { q: string[] },
  ): Observable<ImageDto[]> {
    return this.http.get<ImageDto[]>(this.baseUrl(userId) + `/images`, {
      params,
    });
  }

  getUserTexts(userId: string, params: { q: string[] }): Observable<TextDto[]> {
    return this.http.get<TextDto[]>(this.baseUrl(userId) + `/texts`, {
      params,
    });
  }

  getUserObjects(
    userId: string,
    params: { q: string[] },
  ): Observable<ObjectDto[]> {
    return this.http.get<ObjectDto[]>(this.baseUrl(userId) + `/objects`, {
      params,
    });
  }

  getUserPois(
    userId: string,
    params: { q: string[] },
  ): Observable<Uh4dPoiDto[]> {
    return this.http.get<Uh4dPoiDto[]>(this.baseUrl(userId) + `/pois`, {
      params,
    });
  }
}
