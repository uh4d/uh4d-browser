import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  TextAnnotationDto,
  TextDto,
  UpdateTextDto,
} from '@uh4d/dto/interfaces';
import { TextExtendedDto } from '@uh4d/dto/interfaces/custom/text';
import { DataUtilsService } from './utils/data-utils.service';
import { BaseApiService } from './base.api.service';
import { stripEmptyParams } from './utils/helpers';

@Injectable({
  providedIn: 'root',
})
export class TextsApiService extends BaseApiService {
  override readonly routeBase = '/texts';

  constructor(
    private readonly http: HttpClient,
    private readonly utils: DataUtilsService,
  ) {
    super();
  }

  query(params: {
    q?: string[];
    from?: string;
    to?: string;
    lat?: number;
    lon?: number;
    r?: number;
  }): Observable<TextExtendedDto[]> {
    stripEmptyParams(params);
    return this.http
      .get<TextExtendedDto>(this.baseUrl(), { params })
      .pipe(this.utils.catchError());
  }

  get(id: string): Observable<TextDto> {
    return this.http.get<TextDto>(this.baseUrl(id));
  }

  patch(id: string, body: UpdateTextDto): Observable<TextDto> {
    return this.http.patch<TextDto>(this.baseUrl(id), body);
  }

  remove(id: string): Observable<void> {
    return this.http.delete<void>(this.baseUrl(id));
  }

  updateValidationStatus(
    id: string,
    pending: boolean,
    declined = false,
  ): Observable<TextDto> {
    return this.http.patch<TextDto>(this.baseUrl(id) + '/validation', {
      pending,
      declined,
    });
  }

  queryAnnotations(id: string): Observable<TextAnnotationDto[]> {
    return this.http.get<TextAnnotationDto[]>(
      this.baseUrl(id) + '/annotations',
    );
  }
}
