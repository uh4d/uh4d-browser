import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PersonDto } from '@uh4d/dto/interfaces';
import { BaseApiService } from './base.api.service';

@Injectable({
  providedIn: 'root',
})
export class PersonsApiService extends BaseApiService {
  override readonly routeBase = '/persons';

  constructor(private readonly http: HttpClient) {
    super();
  }

  query(search?: string): Observable<PersonDto[]> {
    return this.http.get<PersonDto[]>(this.baseUrl(), {
      params: search ? { search } : {},
    });
  }

  get(id: string): Observable<PersonDto> {
    return this.http.get<PersonDto>(this.baseUrl(id));
  }
}
