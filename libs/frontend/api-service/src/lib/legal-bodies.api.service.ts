import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LegalBodyDto } from '@uh4d/dto/interfaces/custom/legal-body';
import { BaseApiService } from './base.api.service';

@Injectable({
  providedIn: 'root',
})
export class LegalBodiesApiService extends BaseApiService {
  override readonly routeBase = '/legalbodies';

  constructor(private readonly http: HttpClient) {
    super();
  }

  query(search?: string): Observable<LegalBodyDto[]> {
    return this.http.get<LegalBodyDto[]>(this.baseUrl(), {
      params: search ? { search } : {},
    });
  }

  get(id: string): Observable<LegalBodyDto> {
    return this.http.get<LegalBodyDto>(this.baseUrl(id));
  }
}
