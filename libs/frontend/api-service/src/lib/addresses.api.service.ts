import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseApiService } from './base.api.service';

@Injectable({
  providedIn: 'root',
})
export class AddressesApiService extends BaseApiService {
  override readonly routeBase = '/addresses';

  constructor(private readonly http: HttpClient) {
    super();
  }

  query(search?: string): Observable<{ id: string; value: string }[]> {
    return this.http.get<{ id: string; value: string }[]>(this.baseUrl(), {
      params: search ? { search } : {},
    });
  }
}
