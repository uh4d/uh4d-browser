import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { SpecialModeService } from '@uh4d/frontend/services';

@Directive({
  selector: '[uh4dExhibitionMode]',
  standalone: true,
})
export class ExhibitionModeDirective {
  private hasView = false;

  constructor(
    private readonly templateRef: TemplateRef<any>,
    private readonly viewContainer: ViewContainerRef,
    private readonly specialMode: SpecialModeService,
  ) {}

  @Input() set uh4dExhibitionMode(condition: boolean) {
    if (!this.specialMode.inExhibitionMode) {
      if (!condition && !this.hasView) {
        this.viewContainer.createEmbeddedView(this.templateRef);
        this.hasView = true;
      } else if (condition && this.hasView) {
        this.viewContainer.clear();
        this.hasView = false;
      }
    } else {
      if (condition && !this.hasView) {
        this.viewContainer.createEmbeddedView(this.templateRef);
        this.hasView = true;
      } else if (!condition && this.hasView) {
        this.viewContainer.clear();
        this.hasView = false;
      }
    }
  }
}
