import { APP_INITIALIZER, FactoryProvider } from '@angular/core';
import { catchError, of } from 'rxjs';
import { AuthService } from './auth.service';

/**
 * Refresh token (if any) before any controller logic starts.
 */
export function initializeAuth(authService: AuthService) {
  return () =>
    authService.refreshToken().pipe(
      // catch error to start app on success or failure
      catchError(() => of()),
    );
}

export const authInitializer: FactoryProvider = {
  provide: APP_INITIALIZER,
  useFactory: initializeAuth,
  deps: [AuthService],
  multi: true,
};
