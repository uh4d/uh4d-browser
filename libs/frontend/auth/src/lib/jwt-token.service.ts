import { Injectable } from '@angular/core';
import { jwtDecode } from 'jwt-decode';
import { RoleType } from '@uh4d/config';
import { GeofenceDto } from '@uh4d/dto/interfaces';
import { TokenPayload, TokensDto } from './jwt.types';
import { AuthUser } from './auth-user';

const ACCESS_TOKEN_KEY = 'uh4d-access-token';
const REFRESH_TOKEN_KEY = 'uh4d-refresh-token';

@Injectable({
  providedIn: 'root',
})
export class JwtTokenService {
  private accessToken: string | null = null;
  private refreshToken: string | null = null;

  private decodedToken?: TokenPayload;

  setTokens(tokens: TokensDto | null) {
    if (tokens) {
      localStorage.setItem(ACCESS_TOKEN_KEY, tokens.accessToken);
      localStorage.setItem(REFRESH_TOKEN_KEY, tokens.refreshToken);
      this.accessToken = tokens.accessToken;
      this.refreshToken = tokens.refreshToken;
    } else {
      localStorage.removeItem(ACCESS_TOKEN_KEY);
      localStorage.removeItem(REFRESH_TOKEN_KEY);
      this.accessToken = null;
      this.refreshToken = null;
    }
  }

  getAccessToken(): string | null {
    if (!this.accessToken) {
      this.accessToken = localStorage.getItem(ACCESS_TOKEN_KEY);
    }
    return this.accessToken;
  }

  getRefreshToken(): string | null {
    if (!this.refreshToken) {
      this.refreshToken = localStorage.getItem(REFRESH_TOKEN_KEY);
    }
    return this.refreshToken;
  }

  decodeToken() {
    if (this.accessToken) {
      this.decodedToken = jwtDecode(this.accessToken);
    }
  }

  getUser(): AuthUser | null {
    this.decodeToken();
    return this.decodedToken
      ? new AuthUser({
          id: this.decodedToken.sub,
          name: this.decodedToken.username,
          role: this.decodedToken['role'] as RoleType,
          geofences: this.decodedToken['geofences'] as GeofenceDto[],
        })
      : null;
  }

  getExpiryTime(): number | null {
    this.decodeToken();
    return this.decodedToken ? this.decodedToken.exp : null;
  }

  isTokenExpired() {
    const expiryTime = this.getExpiryTime();
    return expiryTime ? 1000 * expiryTime - Date.now() < 5000 : false;
  }
}
