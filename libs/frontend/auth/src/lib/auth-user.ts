import { UserEventDto } from '@uh4d/dto/interfaces';
import { Location2D } from '@uh4d/dto/interfaces/custom/spatial';
import { BasicAuthUser } from '@uh4d/utils';
import { JwtUserData } from './jwt.types';

/**
 * Frontend auth user.
 */
export class AuthUser extends BasicAuthUser implements JwtUserData {
  /**
   * Check if user can edit item.
   */
  canEditItem(location: Location2D, userEvent?: UserEventDto | null) {
    return (
      this.isModerator() ||
      userEvent?.userId === this.id ||
      (this.isCreator() && this.isWithinGeofence(location))
    );
  }
}
