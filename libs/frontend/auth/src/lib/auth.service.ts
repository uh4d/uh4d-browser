import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import {
  BehaviorSubject,
  distinctUntilChanged,
  map,
  Observable,
  of,
  tap,
} from 'rxjs';
import { NgxRolesService } from 'ngx-permissions';
import { Uh4dRoles } from '@uh4d/config';
import { JwtTokenService } from './jwt-token.service';
import { TokensDto } from './jwt.types';
import { AuthUser } from './auth-user';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private readonly baseUrl = '/api/auth';

  private userId?: string;
  private refreshTokenTimeout?: number;

  private userSubject = new BehaviorSubject<AuthUser | null>(null);
  public user$ = this.userSubject.asObservable();

  constructor(
    private readonly jwtTokenService: JwtTokenService,
    private readonly router: Router,
    private readonly http: HttpClient,
    private readonly rolesServices: NgxRolesService,
  ) {
    this.user$
      .pipe(
        distinctUntilChanged(
          (prev, curr) => prev?.id === curr?.id && prev?.role === curr?.role,
        ),
      )
      .subscribe((user) => {
        this.setRolesAndPermissions(user?.role);
      });
  }

  isLoggedIn(): boolean {
    return !!this.userSubject.value;
  }

  register(data: {
    name: string;
    password: string;
    email: string;
    phone: string;
  }): Observable<void> {
    return this.http
      .post<{ userId: string }>(this.baseUrl + '/register', data)
      .pipe(
        map(({ userId }) => {
          this.userId = userId;
        }),
      );
  }

  requestCode(
    usernameOrEmail: string,
  ): Observable<{ userId: string; maskedPhone: string }> {
    return this.http
      .post<{ userId: string; maskedPhone: string }>(this.baseUrl + '/totp', {
        username: usernameOrEmail,
      })
      .pipe(
        tap(({ userId }) => {
          this.userId = userId;
        }),
      );
  }

  verify(code: string): Observable<void> {
    if (!this.userId) {
      throw new Error('userId not available');
    }

    return this.http
      .post<TokensDto>(this.baseUrl + '/verify', {
        userId: this.userId,
        code,
      })
      .pipe(
        map((tokens) => {
          this.jwtTokenService.setTokens(tokens);
          this.userSubject.next(this.jwtTokenService.getUser());
          this.startRefreshTokenTimer();
        }),
      );
  }

  login(usernameOrEmail: string, password: string): Observable<void> {
    return this.http
      .post<TokensDto>(this.baseUrl + '/login', {
        username: usernameOrEmail,
        password: password,
      })
      .pipe(
        map((tokens) => {
          this.jwtTokenService.setTokens(tokens);
          this.userSubject.next(this.jwtTokenService.getUser());
          this.startRefreshTokenTimer();
        }),
      );
  }

  logout() {
    this.http.get(this.baseUrl + '/logout').subscribe();
    this.stopRefreshTokenTimer();
    this.jwtTokenService.setTokens(null);
    this.userSubject.next(null);

    // leave non-public pages after logout
    if (/^\/(?:dashboard|profile)/.test(this.router.url)) {
      this.router.navigate(['/']);
    }
  }

  updatePassword(password: string): Observable<void> {
    return this.http.patch<void>(this.baseUrl + '/password', { password });
  }

  refreshToken(): Observable<void> {
    if (!this.jwtTokenService.getRefreshToken()) {
      // no refresh token available
      return of();
    }
    return this.http.get<TokensDto>(this.baseUrl + '/refresh').pipe(
      map((tokens) => {
        this.jwtTokenService.setTokens(tokens);
        this.userSubject.next(this.jwtTokenService.getUser());
        this.startRefreshTokenTimer();
      }),
    );
  }

  private startRefreshTokenTimer() {
    const expires = this.jwtTokenService.getExpiryTime();
    if (!expires) return;

    // set timeout to refresh the token a minute before it expires
    const timeout = expires * 1000 - Date.now() - 60 * 1000;
    this.refreshTokenTimeout = window.setTimeout(
      () => this.refreshToken().subscribe(),
      timeout,
    );
  }

  private stopRefreshTokenTimer() {
    clearTimeout(this.refreshTokenTimeout);
  }

  private setRolesAndPermissions(role?: string) {
    this.rolesServices.flushRolesAndPermissions();
    this.rolesServices.addRolesWithPermissions(
      Uh4dRoles.getRoles(role || (this.isLoggedIn() ? 'user' : null)),
    );
  }
}
