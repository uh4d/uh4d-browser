import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { JwtTokenService } from '../jwt-token.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private readonly jwtTokenService: JwtTokenService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler,
  ): Observable<HttpEvent<unknown>> {
    // use refreshToken on refresh route, otherwise use access token
    const token = request.url.endsWith('auth/refresh')
      ? this.jwtTokenService.getRefreshToken()
      : this.jwtTokenService.getAccessToken();

    if (token) {
      request = request.clone({
        headers: request.headers.set('Authorization', `Bearer ${token}`),
      });
    }

    return next.handle(request);
  }
}
