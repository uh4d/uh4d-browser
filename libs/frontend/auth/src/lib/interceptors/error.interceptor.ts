import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
import { catchError, Observable, throwError } from 'rxjs';
import { NotificationsService } from 'angular2-notifications';
import { AuthService } from '../auth.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private readonly authService: AuthService,
    private readonly notify: NotificationsService,
  ) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler,
  ): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError((err: HttpErrorResponse) => {
        if (err.status === 401 && this.authService.isLoggedIn()) {
          // auto logout if 401 response returned from api
          this.authService.logout();
        } else if (err.status === 403) {
          this.notify.error(
            'Forbidden',
            'You do not have the necessary permissions!',
          );
        }

        console.error(err);
        return throwError(() => err);
      }),
    );
  }
}
