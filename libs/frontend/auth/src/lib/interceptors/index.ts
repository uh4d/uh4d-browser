import { Provider } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NotificationsService } from 'angular2-notifications';
import { JwtTokenService } from '../jwt-token.service';
import { AuthService } from '../auth.service';
import { AuthInterceptor } from './auth.interceptor';
import { ErrorInterceptor } from './error.interceptor';

/**
 * Http interceptors in outside-in order.
 * See {@link https://v17.angular.io/guide/http-intercept-requests-and-responses#interceptor-order}
 */
export const httpInterceptorProviders: Provider[] = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true,
    deps: [JwtTokenService],
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: ErrorInterceptor,
    multi: true,
    deps: [AuthService, NotificationsService],
  },
];
