import { CanActivateFn, Router } from '@angular/router';
import { inject } from '@angular/core';
import { AuthService } from './auth.service';

export const authGuard: CanActivateFn = (route, state) => {
  const loggedIn = inject(AuthService).isLoggedIn();
  if (loggedIn) {
    return true;
  } else {
    inject(Router).navigate(['/login'], {
      queryParamsHandling: 'merge',
      queryParams: { returnUrl: state.url.replace(/\?.+/, '') },
    });
    return false;
  }
};
