import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { UserEventDto } from '@uh4d/dto/interfaces';
import { AuthService } from '../auth.service';
import { AuthUser } from '../auth-user';

@Directive({
  selector: '[uh4dHasEditPermission]',
})
export class HasEditPermissionDirective {
  private user: AuthUser | null = null;

  private hasView = false;

  constructor(
    private readonly templateRef: TemplateRef<any>,
    private readonly viewContainer: ViewContainerRef,
    authService: AuthService,
  ) {
    authService.user$.subscribe((user) => {
      this.user = user;
    });
  }

  @Input() set uh4dHasEditPermission(item: {
    uploadedBy?: UserEventDto | null;
  }) {
    const canEdit =
      this.user &&
      (this.user.isModerator() || this.user.id === item.uploadedBy?.userId);

    if (canEdit && !this.hasView) {
      this.viewContainer.createEmbeddedView(this.templateRef);
      this.hasView = true;
    } else if (!canEdit && this.hasView) {
      this.viewContainer.clear();
      this.hasView = false;
    }
  }
}
