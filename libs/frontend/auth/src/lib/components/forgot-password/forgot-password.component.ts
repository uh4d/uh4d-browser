import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { finalize } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NotificationsService } from 'angular2-notifications';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'uh4d-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.sass'],
})
export class ForgotPasswordComponent {
  form = new FormGroup({
    username: new FormControl<string>('', [Validators.required]),
  });

  isLoading = false;
  isVerifying = false;

  maskedPhone = '';

  step: 'username' | 'verify' = 'username';

  constructor(
    private readonly authService: AuthService,
    private readonly notify: NotificationsService,
    private readonly router: Router,
    private readonly modalRef?: BsModalRef,
  ) {}

  requestCode() {
    if (this.form.invalid) return;

    this.isLoading = true;

    this.authService
      .requestCode(this.form.value.username!.trim())
      .pipe(
        finalize(() => {
          this.isLoading = false;
        }),
      )
      .subscribe({
        next: ({ maskedPhone }) => {
          this.step = 'verify';
          this.maskedPhone = maskedPhone;
        },
        error: (err: HttpErrorResponse) => {
          let message = err.message;

          if (err.error?.message === 'User does not exist') {
            message = 'User does not exist';
          }

          this.notify.error(message);
        },
      });
  }

  verifyCode(code: string) {
    this.isVerifying = true;
    this.authService
      .verify(code)
      .pipe(
        finalize(() => {
          this.isVerifying = false;
        }),
      )
      .subscribe({
        next: () => {
          this.router.navigate(['/profile/password'], {
            queryParamsHandling: 'preserve',
          });
          this.close();
        },
        error: (err: HttpErrorResponse) => {
          const invalidToken = err.error?.message === 'Invalid token';
          this.notify.error(invalidToken ? 'Invalid code' : err.message);
        },
      });
  }

  backToForm() {
    this.step = 'username';
  }

  close() {
    this.modalRef?.hide();
  }
}
