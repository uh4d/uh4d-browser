import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

type LoginMode = 'login' | 'register' | 'forgot-password';

@Component({
  selector: 'uh4d-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.sass'],
})
export class LoginModalComponent {
  mode: LoginMode = 'login';

  constructor(public readonly modalRef: BsModalRef) {}

  setMode(event: Event, mode: LoginMode) {
    event.preventDefault();
    this.mode = mode;
  }
}
