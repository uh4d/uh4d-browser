import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { finalize } from 'rxjs';
import { NotificationsService } from 'angular2-notifications';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'uh4d-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass'],
})
export class RegisterComponent {
  form = new FormGroup({
    name: new FormControl<string>('', [Validators.required]),
    password: new FormControl<string>('', [
      Validators.required,
      Validators.minLength(6),
      Validators.pattern(/^\S+$/),
    ]),
    email: new FormControl<string>('', [Validators.required, Validators.email]),
    phone: new FormControl<string>('', [Validators.required]),
    gdprConsent: new FormControl<boolean>(false, [Validators.requiredTrue]),
  });

  passwordVisible = false;
  isLoading = false;
  isVerifying = false;

  registerStep: 'register' | 'verify' = 'register';

  constructor(
    private readonly authService: AuthService,
    private readonly notify: NotificationsService,
    private readonly modalRef?: BsModalRef,
  ) {}

  register() {
    if (this.form.invalid) return;

    this.isLoading = true;

    this.authService
      .register({
        name: this.form.value.name!.trim(),
        password: this.form.value.password!.trim(),
        email: this.form.value.email!.trim(),
        phone: this.form.value.phone!.trim(),
      })
      .pipe(
        finalize(() => {
          this.isLoading = false;
        }),
      )
      .subscribe({
        next: () => {
          this.registerStep = 'verify';
        },
        error: (err: HttpErrorResponse) => {
          let message = err.message;

          if (err.error?.message === 'User already exists') {
            message = 'User with this e-mail already exists';
          } else if (err.error?.message === 'Username already exists') {
            message = 'Username already exists';
          } else if (
            err.error?.message?.includes('phone must be a valid phone number')
          ) {
            message = 'Invalid phone number';
          }

          this.notify.error(message);
        },
      });
  }

  verifyCode(code: string) {
    this.isVerifying = true;
    this.authService
      .verify(code)
      .pipe(
        finalize(() => {
          this.isVerifying = false;
        }),
      )
      .subscribe({
        next: () => {
          this.notify.success('Registration successful');
          this.close();
        },
        error: (err: HttpErrorResponse) => {
          const invalidToken = err.error?.message === 'Invalid token';
          this.notify.error(invalidToken ? 'Invalid code' : err.message);
        },
      });
  }

  backToForm() {
    this.registerStep = 'register';
  }

  openPrivacyPolicy(event: Event) {
    event.preventDefault();
    window.open('/legalnotice', '_blank');
  }

  close() {
    this.modalRef?.hide();
  }
}
