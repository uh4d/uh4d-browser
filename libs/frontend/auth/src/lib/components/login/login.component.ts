import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { finalize } from 'rxjs';
import { NotificationsService } from 'angular2-notifications';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'uh4d-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass'],
})
export class LoginComponent {
  form = new FormGroup({
    username: new FormControl<string>('', [Validators.required]),
    password: new FormControl<string>('', [Validators.required]),
  });

  passwordVisible = false;
  isLoading = false;

  constructor(
    private readonly authService: AuthService,
    private readonly notify: NotificationsService,
    private readonly modalRef?: BsModalRef,
  ) {}

  login() {
    if (this.form.invalid) return;

    this.isLoading = true;

    this.authService
      .login(this.form.value.username!.trim(), this.form.value.password!.trim())
      .pipe(
        finalize(() => {
          this.isLoading = false;
        }),
      )
      .subscribe({
        next: () => {
          this.notify.success('Login successful');
          this.close();
        },
        error: (err: HttpErrorResponse) => {
          this.notify.error(
            err.status === 400 ? 'Invalid login data' : err.message,
          );
        },
      });
  }

  close() {
    this.modalRef?.hide();
  }
}
