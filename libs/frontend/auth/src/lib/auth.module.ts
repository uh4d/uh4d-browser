import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterLink } from '@angular/router';
import { CodeInputModule } from 'angular-code-input';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import { LoginComponent } from './components/login/login.component';
import { LoginModalComponent } from './components/login-modal/login-modal.component';
import { RegisterComponent } from './components/register/register.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { HasEditPermissionDirective } from './directives/has-edit-permission.directive';

@NgModule({
  declarations: [
    LoginComponent,
    LoginModalComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    HasEditPermissionDirective,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    CodeInputModule,
    RouterLink,
    Uh4dIconsModule,
  ],
  exports: [HasEditPermissionDirective],
})
export class AuthModule {}
