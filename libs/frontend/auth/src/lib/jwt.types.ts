import { RoleType } from '@uh4d/config';
import { GeofenceDto } from '@uh4d/dto/interfaces';

export interface JwtUserData {
  id: string;
  name: string;
  role?: RoleType;
  geofences?: GeofenceDto[];
}

export interface TokensDto {
  accessToken: string;
  refreshToken: string;
}

export interface TokenPayload {
  sub: string;
  username: string;
  exp: number;
  [key: string]: any;
}
