import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgxPermissionsModule } from 'ngx-permissions';
import { ButtonsModule } from 'ngx-bootstrap/buttons';

import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import {
  QuickHelpComponent,
  RecordUserMetadataComponent,
  SliderControlComponent,
} from '@uh4d/frontend/ui-components';
import { ImageSvgViewerComponent } from '@uh4d/frontend/image';
import { FormatImageDatePipe } from '@uh4d/frontend/pipes';
import { ExhibitionModeDirective } from '@uh4d/frontend/directives';
import { Viewport3dComponent } from './viewport3d.component';
import { Canvas3dComponent } from './components/canvas3d/canvas3d.component';
import { CompassComponent } from './components/compass/compass.component';
import { LoadProgressComponent } from './components/load-progress/load-progress.component';
import { ViewportControlsComponent } from './components/viewport-controls/viewport-controls.component';
import { ViewportTooltipComponent } from './components/viewport-tooltip/viewport-tooltip.component';
import { ViewportContextMenuComponent } from './components/viewport-context-menu/viewport-context-menu.component';
import { IsolationControlsComponent } from './components/isolation-controls/isolation-controls.component';
import { SpatializeManualComponent } from './components/spatialize-manual/spatialize-manual.component';
import { ObjectUploadComponent } from './components/object-upload/object-upload.component';
import { TransformControlsComponent } from './components/transform-controls/transform-controls.component';
import { TourControlsComponent } from './components/tour-controls/tour-controls.component';
import { TourFormModalComponent } from './components/tour-form-modal/tour-form-modal.component';
import { TourManagerComponent } from './components/tour-manager/tour-manager.component';
import { SimplePreviewComponent } from './components/simple-preview/simple-preview.component';
import { ProjectionControlsComponent } from './components/projection-controls/projection-controls.component';

@NgModule({
  declarations: [
    Viewport3dComponent,
    Canvas3dComponent,
    CompassComponent,
    LoadProgressComponent,
    ViewportControlsComponent,
    ViewportTooltipComponent,
    ViewportContextMenuComponent,
    IsolationControlsComponent,
    SpatializeManualComponent,
    ObjectUploadComponent,
    TransformControlsComponent,
    TourControlsComponent,
    TourFormModalComponent,
    TourManagerComponent,
    SimplePreviewComponent,
    ProjectionControlsComponent,
  ],
  exports: [Viewport3dComponent],
  imports: [
    CommonModule,
    Uh4dIconsModule,
    FileUploadModule,
    DragDropModule,
    ImageSvgViewerComponent,
    ReactiveFormsModule,
    FormsModule,
    RecordUserMetadataComponent,
    SliderControlComponent,
    QuickHelpComponent,
    FormatImageDatePipe,
    ExhibitionModeDirective,
    NgxPermissionsModule,
    ButtonsModule,
  ],
})
export class Viewport3dModule {}
