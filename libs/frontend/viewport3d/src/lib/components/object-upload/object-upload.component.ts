import { Component, OnInit } from '@angular/core';
import { FileItem, FileUploader } from 'ng2-file-upload';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NotificationsService } from 'angular2-notifications';
import { JwtTokenService } from '@uh4d/frontend/auth';
import { ViewportEventsService } from '@uh4d/frontend/services';
import { getSupportedFileEndings, isFileEndingSupported } from '@uh4d/config';

/**
 * Modal to select and upload an OBJ file (or zipped OBJ file with textures).
 * The file gets sent to the server, where it gets processed and converted
 * to a temporary stored .gltf file.
 *
 * ![Screenshot](../assets/viewport-object-upload-modal.png)
 */
@Component({
  selector: 'uh4d-object-upload',
  templateUrl: './object-upload.component.html',
  styleUrls: ['./object-upload.component.sass'],
})
export class ObjectUploadComponent implements OnInit {
  // config uploader
  // max 1 file of type .obj or .zip
  uploader: FileUploader = new FileUploader({
    url: `api/objects/tmp`,
    itemAlias: 'uploadModel',
    queueLimit: 1,
    filters: [
      {
        name: 'customFileType',
        fn: (file) => {
          const ext = file.name?.split('.').pop();
          return isFileEndingSupported(ext || '', 'model');
        },
      },
    ],
    authToken: 'Bearer ' + this.jwtTokenService.getAccessToken(),
  });

  fileItem?: FileItem;
  hasFileOverDropZone = false;

  supportedFileEndings = getSupportedFileEndings('model');

  constructor(
    private readonly modalRef: BsModalRef,
    private readonly notify: NotificationsService,
    private readonly events: ViewportEventsService,
    private readonly jwtTokenService: JwtTokenService,
  ) {}

  ngOnInit(): void {
    // listen to uploader events

    this.uploader.onWhenAddingFileFailed = (fileItem, filter) => {
      switch (filter.name) {
        case 'queueLimit':
          this.uploader.clearQueue();
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          this.uploader.addToQueue([fileItem]);
          break;
        case 'customFileType':
          this.notify.warn('File format not supported!');
          break;
        default:
          this.notify.error('Unknown error!', 'See console.');
      }

      console.warn('onWhenAddingFileFailed', fileItem, filter);
    };

    this.uploader.onAfterAddingFile = (fileItem) => {
      this.fileItem = fileItem;
    };

    this.uploader.onSuccessItem = (fileItem, response) => {
      this.notify.success('Upload successful!');
      this.events.callAction$.next({
        key: 'object-temp',
        file: JSON.parse(response),
      });
      this.close();
    };

    this.uploader.onErrorItem = (fileItem, response, status) => {
      this.notify.error('Something went wrong!');
      console.error('onErrorItem', fileItem, response, status);
    };
  }

  /**
   * Start upload process.
   */
  upload(): void {
    this.uploader.uploadAll();
  }

  /**
   * Close modal.
   */
  close(): void {
    this.modalRef.hide();
  }
}
