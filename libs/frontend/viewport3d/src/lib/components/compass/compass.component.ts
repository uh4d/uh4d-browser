import {
  Component,
  ElementRef,
  NgZone,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { Vector2, Vector3 } from 'three';
import { ViewportEventsService } from '@uh4d/frontend/services';

/**
 * Compass rose that is set with respect to the viewport's camera.
 * It supports the orientation of the user.
 * The only control is a `Align North` button to align the viewport's camera to north (negative z direction).
 *
 * ![Screenshot](../assets/viewport-compass.png)
 */
@Component({
  selector: 'uh4d-compass',
  templateUrl: './compass.component.html',
  styleUrls: ['./compass.component.sass'],
})
export class CompassComponent implements OnInit, OnDestroy {
  /**
   * @ignore
   */
  private unsubscribe$ = new Subject<void>();

  /**
   * Reference to `img` element.
   */
  @ViewChild('img', { static: true }) img!: ElementRef<HTMLImageElement>;

  constructor(
    private ngRenderer: Renderer2,
    private ngZone: NgZone,
    private events: ViewportEventsService,
  ) {}

  /**
   * Invoked after the component is instantiated.
   */
  ngOnInit(): void {
    this.ngRenderer.setStyle(
      this.img.nativeElement,
      'transform',
      `translate(-50%,-50%)`,
    );

    this.ngZone.runOutsideAngular(() => {
      // listen to camera updates
      this.events.cameraUpdate$
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((camera) => {
          // rotate image with respect to camera orientation
          const direction = camera.getWorldDirection(new Vector3());
          const rotation = -(
            new Vector2(direction.x, direction.z).angle() +
            Math.PI / 2
          );
          this.ngRenderer.setStyle(
            this.img.nativeElement,
            'transform',
            `translate(-50%,-50%)rotate(${rotation}rad)`,
          );
        });
    });
  }

  /**
   * Trigger `align-north` event to align camera to north (look into negative z direction).
   */
  alignNorth(): void {
    this.events.callAction$.next({ key: 'align-north' });
  }

  callHomeView(): void {
    this.events.callAction$.next({ key: 'home-view' });
  }

  /**
   * Invoked before the component gets destroyed.
   */
  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
