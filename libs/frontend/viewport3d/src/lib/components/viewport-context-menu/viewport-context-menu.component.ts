import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  NgZone,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest, Subject, takeUntil } from 'rxjs';
import { Vector2 } from 'three';
import { AuthService } from '@uh4d/frontend/auth';
import { ImageItem, ObjectItem, PoiItem } from '@uh4d/three';
import {
  SearchManagerService,
  ViewportEventsService,
} from '@uh4d/frontend/services';

/**
 * Component that is displayed when the user right-clicks an image, object, or poi item in 3D.
 * Multiple actions are shown according to the item type.
 *
 * ![Screenshot](../assets/viewport-contextmenu-image.png)&emsp;
 * ![Screenshot](../assets/viewport-contextmenu-object.png)
 */
@Component({
  selector: 'uh4d-viewport-context-menu',
  templateUrl: './viewport-context-menu.component.html',
  styleUrls: ['./viewport-context-menu.component.sass'],
})
export class ViewportContextMenuComponent implements OnInit, OnDestroy {
  /**
   * @ignore
   */
  private unsubscribe$ = new Subject<void>();
  /**
   * @ignore
   */
  private active = false;

  /**
   * Position of the context menu in screen (canvas) coordinates.
   */
  position = new Vector2(-999, -999);

  /**
   * Reference to image item.
   */
  image: ImageItem | null = null;
  /**
   * Reference to object item.
   */
  object: ObjectItem | null = null;
  /**
   * If `true`, the object item is part of the search params.
   */
  objectFiltered = false;
  /**
   * Reference to point of interest item.
   */
  poi: PoiItem | null = null;

  canEdit = false;

  @ViewChild('dialogElement', { static: true })
  dialogElement!: ElementRef<HTMLDivElement>;

  constructor(
    private events: ViewportEventsService,
    private element: ElementRef,
    private route: ActivatedRoute,
    private router: Router,
    private ngZone: NgZone,
    private changeDetector: ChangeDetectorRef,
    private searchManager: SearchManagerService,
    private authService: AuthService,
  ) {}

  /**
   * Invoked after component is instantiated.
   * Listen to context menu events.
   */
  ngOnInit(): void {
    combineLatest({
      value: this.events.contextmenu$,
      user: this.authService.user$,
    })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(({ value, user }) => {
        if (!value && !this.active) return;

        // reset
        this.reset();

        if (!value) {
          this.changeDetector.detectChanges();
          return;
        }

        this.active = true;
        this.canEdit = false;

        // set new tooltip
        if (value.item instanceof ImageItem) {
          this.image = value.item;
          this.canEdit =
            !!user &&
            user.canEditItem(
              value.item.source.camera,
              value.item.source.uploadedBy,
            );
        } else if (value.item instanceof ObjectItem) {
          this.object = value.item;
          this.objectFiltered = this.searchManager.params$
            .getValue()
            .q.includes('obj:' + this.object.object.userData['id']);
        } else if (value.item instanceof PoiItem) {
          this.poi = value.item;
        }

        this.changeDetector.detectChanges();

        // use timeout to wait for changes by Angular
        setTimeout(() => {
          // set position with respect to container size
          this.position.set(
            Math.min(
              value.position.x,
              this.element.nativeElement.offsetWidth -
                this.dialogElement.nativeElement.offsetWidth,
            ) - 20,
            Math.min(
              value.position.y,
              this.element.nativeElement.offsetHeight -
                this.dialogElement.nativeElement.offsetHeight,
            ) - 20,
          );

          this.changeDetector.detectChanges();
        });
      });
  }

  /**
   * Open image metadata sheet ({@link ImageSheetComponent}).
   */
  openImage(): void {
    this.ngZone.run(() => {
      this.router.navigate(['./image', this.image!.source.id], {
        relativeTo: this.route,
        queryParamsHandling: 'preserve',
      });
      this.reset();
    });
  }

  /**
   * Jump to location of image item (take perspective).
   */
  zoomImage(): void {
    this.image!.focus();
    this.reset();
    this.changeDetector.detectChanges();
  }

  setProjectionDistance(): void {
    this.events.callAction$.next({
      key: 'projective-setup',
      item: this.image!,
    });
    this.reset();
    this.changeDetector.detectChanges();
  }

  /**
   * Open object metadata sheet ({@link ObjectSheetComponent}).
   */
  openObject(): void {
    this.ngZone.run(() => {
      this.router.navigate(['./object', this.object!.object.userData['id']], {
        relativeTo: this.route,
        queryParamsHandling: 'preserve',
      });
      this.reset();
    });
  }

  /**
   * Jump to object that it fits the viewport.
   */
  zoomObject(): void {
    this.object!.focus();
    this.reset();
    this.changeDetector.detectChanges();
  }

  /**
   * Add/remove object to/from search parameters ({@link SearchManagerService}).
   * @param mode - Mode that should be set according to if the object is already part of the search params or not.
   */
  filterObject(mode: 'add' | 'remove'): void {
    // set boolean flags
    const add = mode === 'add';
    const remove = mode === 'remove';

    // update search params
    this.ngZone.run(() => {
      this.searchManager.setSearchParam(
        ['obj:' + this.object!.object.userData['id']],
        add,
        remove,
      );
      this.reset();
    });
  }

  /**
   * Open point of interest ({@link PoiFormComponent}).
   */
  openPoi(): void {
    this.events.callAction$.next({
      key: 'poi-open',
      poi: this.poi!.object.userData['resource'],
    });
    this.reset();
    this.changeDetector.detectChanges();
  }

  /**
   * Jump to point of interest.
   */
  zoomPoi(): void {
    this.poi!.focus();
    this.reset();
    this.changeDetector.detectChanges();
  }

  /**
   * Jump to camera (take perspective) associated with this point of interest (if available).
   */
  setCamera(): void {
    this.events.callAction$.next({
      key: 'poi-camera',
      poi: this.poi!,
    });
    this.reset();
    this.changeDetector.detectChanges();
  }

  /**
   * Hide and reset context menu.
   * @private
   */
  private reset(): void {
    // remove references
    this.image = null;
    this.object = null;
    this.poi = null;
    this.position.set(-999, -999);
    this.active = false;
  }

  /**
   * Invoked before the component gets destroyed.
   */
  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
