import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  Renderer2,
} from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { Cache } from '@uh4d/frontend/viewport3d-cache';

@Component({
  selector: 'uh4d-load-progress',
  templateUrl: './load-progress.component.html',
  styleUrls: ['./load-progress.component.sass'],
})
export class LoadProgressComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();

  private visible = false;

  item = '';
  loaded = 0;
  total = 0;
  progress = 0;

  constructor(
    private readonly renderer: Renderer2,
    private readonly element: ElementRef,
  ) {}

  ngOnInit(): void {
    // this.renderer.setStyle(this.element.nativeElement, 'display', 'flex');

    Cache.loadingManager.onProgress$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((value) => {
        if (/^data:application/.test(value.url)) {
          return;
        }

        if (!this.visible) {
          this.renderer.setStyle(this.element.nativeElement, 'display', 'flex');
          this.visible = true;
        }

        this.item = value.url;
        this.loaded = value.loaded;
        this.total = value.total;
        this.progress = Math.round((this.loaded / this.total) * 100);
      });

    Cache.loadingManager.onLoad$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        this.renderer.setStyle(this.element.nativeElement, 'display', 'none');
        this.visible = false;
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
