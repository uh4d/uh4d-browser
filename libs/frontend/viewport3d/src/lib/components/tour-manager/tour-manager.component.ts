import { Component, OnDestroy, OnInit } from '@angular/core';
import { combineLatest, Subject, takeUntil } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ToursApiService } from '@uh4d/frontend/api-service';
import { ConfirmModalService } from '@uh4d/frontend/confirm-modal';
import { TourDto } from '@uh4d/dto/interfaces/custom/poi';
import { coordsConverter } from '@uh4d/frontend/viewport3d-cache';
import { AuthService } from '@uh4d/frontend/auth';

@Component({
  selector: 'uh4d-tour-manager',
  templateUrl: './tour-manager.component.html',
  styleUrls: ['./tour-manager.component.sass'],
})
export class TourManagerComponent implements OnInit, OnDestroy {
  private readonly unsubscribe$ = new Subject<void>();
  tours: (TourDto & { canEdit: boolean })[] = [];

  constructor(
    private readonly modalRef: BsModalRef,
    private readonly toursApiService: ToursApiService,
    private readonly confirmService: ConfirmModalService,
    private readonly authService: AuthService,
  ) {}

  ngOnInit(): void {
    const origin = coordsConverter.getOrigin();
    combineLatest({
      tours: this.toursApiService.query({
        lat: origin.latitude,
        lon: origin.longitude,
        r: 5000,
      }),
      user: this.authService.user$,
    })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(({ tours, user }) => {
        this.tours = tours.map((tour) => ({
          ...tour,
          canEdit:
            !!user &&
            tour.pois.some((poi) =>
              user.canEditItem(poi.location, tour.createdBy),
            ),
        }));
        console.log(this.tours);
      });
  }

  async remove(tour: TourDto): Promise<void> {
    const confirmed = await this.confirmService.confirm({
      message: 'Do you really want to delete this tour?',
    });

    if (!confirmed) {
      return;
    }

    this.toursApiService.remove(tour.id).subscribe(() => {
      // remove from array
      const index = this.tours.findIndex((t) => t.id === tour.id);
      if (index !== -1) {
        this.tours.splice(index, 1);
      }
    });
  }

  close(): void {
    this.modalRef.hide();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
