import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostListener,
  NgZone,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import {
  asapScheduler,
  concatMap,
  debounceTime,
  fromEvent,
  merge,
  Subject,
  take,
  takeUntil,
  throttleTime,
} from 'rxjs';
import TWEEN from '@tweenjs/tween.js';
import { BsModalService } from 'ngx-bootstrap/modal';
import {
  Box3,
  Clock,
  CylinderGeometry,
  DirectionalLight,
  Euler,
  Group,
  Intersection,
  MathUtils,
  Matrix4,
  Mesh,
  MeshStandardMaterial,
  Object3D,
  PerspectiveCamera,
  Quaternion,
  Raycaster,
  Scene,
  Sphere,
  Vector2,
  Vector3,
  WebGLRenderer,
} from 'three';
import { MapControls } from 'three-addons/controls/MapControls';
import { FlyControls } from 'three/examples/jsm/controls/FlyControls';
import { TransformControls } from 'three/examples/jsm/controls/TransformControls';
import { Octree } from '@brakebein/threeoctree';
import { debounce, throttle } from '@uh4d/utils';
import {
  ClusterChart,
  HeatMap,
  RadialFan,
  VectorField,
  WindMap,
  VisualizationGradientConfig,
  VisualizationInstance,
} from '@uh4d/frontend/visualization';
import { VisualizationType } from '@uh4d/config';
import {
  CameraDto,
  ImageDto,
  ObjectDto,
  TerrainDto,
  Uh4dPoiDto,
} from '@uh4d/dto/interfaces';
import { TempObjectDto } from '@uh4d/dto/interfaces/custom/object';
import {
  GlobalEventsService,
  SearchManagerService,
  SpecialModeService,
  ViewportEventsService,
} from '@uh4d/frontend/services';
import {
  ClusterObject,
  ClusterTree,
  Collection,
  GenericItem,
  ImageItem,
  ImagePane,
  ObjectItem,
  PoiItem,
  PointOfInterest,
  ProjectiveMaterial,
  VirtualProjectorCamera,
} from '@uh4d/three';
import { OsmBuildingManager } from '@uh4d/frontend/osm';
import { PoisApiService } from '@uh4d/frontend/api-service';
import {
  Cache,
  coordsConverter,
  Settings,
  ShadingKey,
} from '@uh4d/frontend/viewport3d-cache';
import { PoiModalComponent } from '@uh4d/frontend/poi';
import { AuthService, AuthUser } from '@uh4d/frontend/auth';

enum MouseState {
  NONE = -1,
  LEFT = 0,
  MIDDLE = 1,
  RIGHT = 2,
}

@Component({
  selector: 'uh4d-canvas3d',
  templateUrl: './canvas3d.component.html',
  styleUrls: ['./canvas3d.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class Canvas3dComponent implements AfterViewInit, OnDestroy {
  constructor(
    private ngZone: NgZone,
    private cdr: ChangeDetectorRef,
    private events: ViewportEventsService,
    private element: ElementRef,
    private searchManager: SearchManagerService,
    private globalEvents: GlobalEventsService,
    private modalService: BsModalService,
    private specialMode: SpecialModeService,
    private poisApiService: PoisApiService,
    private authService: AuthService,
  ) {}

  private unsubscribe$ = new Subject<void>();

  private user: AuthUser | null = null;

  @ViewChild('canvasElement', { static: true })
  private canvasElement!: ElementRef<HTMLCanvasElement>;

  @ViewChild('previewCanvasElement')
  private previewCanvasElement?: ElementRef<HTMLCanvasElement>;

  private SCREEN_WIDTH = 0;
  private SCREEN_HEIGHT = 0;

  private scene!: Scene;
  private camera!: PerspectiveCamera;
  private renderer!: WebGLRenderer;
  private raycaster = new Raycaster();
  private clock?: Clock;
  private directionalLight!: DirectionalLight;
  private controls!: MapControls;
  private flyControls: FlyControls | null = null;
  private transformControls: TransformControls | null = null;
  private octree!: Octree;
  private clusterTree!: ClusterTree;
  private osmManager!: OsmBuildingManager;

  private selected: (ObjectItem | ImageItem | ClusterObject | PoiItem)[] = [];
  private highlighted: ObjectItem | ImageItem | ClusterObject | PoiItem | null =
    null;

  private isAnimating = false;
  private inIsolation = false;
  private inIsolatedView = false;
  private inProjectiveSetup = false;

  private mouseDownPos = new Vector2();
  private mouseState = MouseState.NONE;
  private isControlsMoving = false;
  private skipMouseLeaveEvent = false;

  private poiCursor: PointOfInterest | null = null;
  private visualization: VisualizationInstance | null = null;

  showPreviewCanvas = false;
  private poiCamera: {
    renderer: WebGLRenderer;
    camera: PerspectiveCamera;
    object: Group;
    lookAt: Vector3;
    poi: PoiItem;
  } | null = null;

  private objects!: Collection<ObjectItem>;
  private images!: Collection<ImageItem>;
  private pois!: Collection<PoiItem>;

  /**
   * Call animate() with debounce. Useful, when iterating over an array,
   * so animate() isn't called a hundred times to update the changes in the viewport.
   */
  private animateAsync = debounce(this.animateOnce, 50);
  private animateThrottle20 = throttle(this.animateOnce, 20);
  private animateThrottle500 = throttle(this.animateOnce, 500);
  private updateOctreeAsync = debounce(this.updateOctree, 100);
  private updateVisualizationAsync = debounce(this.updateVisualization, 500);

  private hoverDebounce = debounce<
    [GenericItem | ClusterObject, Vector2],
    void
  >(
    (item, position) => {
      if (item instanceof ClusterObject) {
        item.explode();
        this.animateAsync();
      } else {
        this.events.tooltip$.next({ item, position });
      }
    },
    500,
    false,
  );

  ngAfterViewInit() {
    this.osmManager = new OsmBuildingManager(coordsConverter, Cache.gltfLoader);

    if (this.specialMode.inExhibitionMode) {
      Settings.images.clusterEnabled = false;
      Settings.objects.showOsmBuildings = false;
      Settings.defaults.viewpoint.cameraPosition.set(0, 1000, 10);
      Cache.viewpoint.cameraPosition.set(0, 1000, 10);
    }

    this.authService.user$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((user) => {
        this.user = user;
      });

    // for performance reasons, all three.js code should run outside of Angular
    setTimeout(() => {
      this.ngZone.runOutsideAngular(() => {
        this.init();
      });
    });
  }

  init() {
    // viewport width and height
    this.SCREEN_WIDTH = this.element.nativeElement.offsetWidth;
    this.SCREEN_HEIGHT = this.element.nativeElement.offsetHeight;

    // camera
    this.camera = new PerspectiveCamera(
      Settings.defaults.FOV,
      this.SCREEN_WIDTH / this.SCREEN_HEIGHT,
      Settings.defaults.NEAR,
      Settings.defaults.FAR,
    );
    this.camera.position.copy(Cache.viewpoint.cameraPosition);
    this.camera.lookAt(Cache.viewpoint.controlsTarget);

    // scene, octree, cluster tree
    this.scene = Cache.scene;
    this.octree = Cache.octree;
    this.clusterTree = Cache.clusterTree;

    // renderer
    this.renderer = new WebGLRenderer({
      antialias: true,
      alpha: false,
      preserveDrawingBuffer: true,
      canvas: this.canvasElement.nativeElement,
    });
    this.renderer.setClearColor(Settings.defaults.backgroundColor, 1);
    this.renderer.setSize(this.SCREEN_WIDTH, this.SCREEN_HEIGHT, false);

    // light
    this.directionalLight = Cache.directionalLight;

    if (Settings.objects.showOsmBuildings) {
      this.scene.add(this.osmManager.group);
    }

    // controls
    this.controls = new MapControls(this.camera, this.renderer.domElement);
    if (this.specialMode.inExhibitionMode) {
      // restrict to camera tilt to not go "underground"
      this.controls.maxPolarAngle = Math.PI / 2;
    }
    this.controls.target.copy(Cache.viewpoint.controlsTarget);
    this.controls.update();
    fromEvent(this.controls, 'change')
      .pipe(
        throttleTime(15),
        takeUntil(this.unsubscribe$),
        // filter(() => !this.isAnimating)
      )
      .subscribe(() => {
        this.onControlsChange();
      });
    fromEvent(this.controls, 'end')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        this.updateVisualizationAsync();
      });

    fromEvent<MouseEvent>(this.element.nativeElement, 'pointerdown')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(this.onMouseDown.bind(this));
    fromEvent<MouseEvent>(this.element.nativeElement, 'pointermove')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(this.onMouseMove.bind(this));
    fromEvent<MouseEvent>(this.element.nativeElement, 'pointerup')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(this.onMouseUp.bind(this));
    fromEvent(this.element.nativeElement, 'dblclick')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(this.onDblClick.bind(this));
    fromEvent<MouseEvent>(this.element.nativeElement, 'pointerleave')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(this.onMouseLeave.bind(this));
    fromEvent<WheelEvent>(this.element.nativeElement, 'wheel')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(this.onMouseWheel.bind(this));
    fromEvent<MouseEvent>(this.element.nativeElement, 'contextmenu')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(this.onContextMenu.bind(this));

    this.objects = Cache.objects;
    this.images = Cache.images;
    this.pois = Cache.pois;

    // listen to updates
    merge(Cache.loadingManager.onProgress$, Cache.terrainManager.onUpdate$)
      .pipe(
        takeUntil(this.unsubscribe$),
        throttleTime(500, asapScheduler, { leading: true, trailing: true }),
      )
      .subscribe(() => {
        this.animateOnce();
      });

    merge(
      this.objects.update$,
      this.images.update$,
      this.pois.update$,
      this.clusterTree.update$,
    )
      .pipe(
        takeUntil(this.unsubscribe$),
        throttleTime(50, asapScheduler, { leading: true, trailing: true }),
      )
      .subscribe(() => {
        this.animateOnce();
      });

    this.objects.toggle$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((event) => {
        if (event.visible) {
          // add
          if (this.scene.getObjectById(event.target.object.id)) {
            return; // already part of scene
          }
          this.scene.add(event.target.object);
        } else {
          // remove
          this.scene.remove(event.target.object);
          this.setSelected(event.target, false, true);
        }
        this.animateAsync();
      });
    this.objects.focus$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((objItem) => {
        this.focusObjects([objItem.object]);
      });

    this.images.toggle$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((event) => {
        const item = event.target;
        if (event.visible) {
          // add
          if (this.scene.getObjectById(item.object.id)) {
            return; // already part of scene
          }
          this.scene.add(item.object);
          this.octree.add(item.object.collisionObject);
        } else {
          // remove
          this.scene.remove(item.object);
          this.octree.remove(item.object.collisionObject);
          this.setSelected(item, false, true);
        }
        this.updateOctreeAsync();
        this.animateAsync();
      });
    this.images.focus$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((imgItem) => {
        if (imgItem.hasOrientation) {
          this.setSpatialImageView(imgItem);
        } else {
          this.focusPoints([imgItem.object.position], 8);
        }
      });

    this.pois.toggle$.pipe(takeUntil(this.unsubscribe$)).subscribe((event) => {
      const item = event.target;
      if (event.visible) {
        // add
        if (this.scene.getObjectById(item.object.id)) {
          return; // already part of scene
        }
        this.scene.add(item.object);
      } else {
        // remove
        this.scene.remove(item.object);
        this.setSelected(item, false, true);
      }
      this.animateAsync();
    });
    this.pois.focus$.pipe(takeUntil(this.unsubscribe$)).subscribe((poiItem) => {
      if (poiItem.object.userData['resource'].camera) {
        this.startCameraTween(
          new Vector3().fromArray(poiItem.object.userData['resource'].camera),
          poiItem.object.position,
        );
      }
    });

    // listen to actions from outside
    this.events.callAction$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((action) => {
        switch (action.key) {
          case 'animate':
            this.animateThrottle20();
            break;
          case 'align-north':
            this.alignNorth();
            break;
          case 'home-view':
            this.setHomeView();
            break;
          case 'cluster-enable':
            // if clustering gets disabled, but there is an active ClusterChart visualization
            // then disable visualization
            if (
              !action.enabled &&
              this.visualization &&
              this.visualization instanceof ClusterChart
            ) {
              this.globalEvents.callAction$.next({
                key: 'visualization',
                type: 'none',
              });
            }
            this.searchManager.images$.pipe(take(1)).subscribe((images) => {
              this.ngZone.runOutsideAngular(() => {
                this.loadImages(images);
              });
            });
            break;
          case 'cluster-update':
            this.clusterTree.setDistanceMultiplier(
              Settings.images.clusterDistance,
            );
            this.clusterTree.setScale(Settings.images.scale);
            this.clusterTree.setOpacity(Settings.images.opacity);
            this.animateAsync();
            break;
          case 'shading-update':
            this.objects.forEach((item) => {
              item.applyShading();
            });
            this.animateAsync();
            break;
          case 'osm-enable':
            if (action.enabled) {
              this.scene.add(this.osmManager.group);
              this.updateOsmBuildings();
            } else {
              this.scene.remove(this.osmManager.group);
            }
            this.animateAsync();
            break;
          case 'isolation-exit':
            this.exitIsolation(true);
            break;
          case 'controls-set':
            if (action.type === 'fly') {
              this.controls.enabled = false;
              if (!this.flyControls) {
                this.flyControls = new FlyControls(
                  this.camera,
                  this.renderer.domElement,
                );
                this.flyControls.dragToLook = true;
              }
              this.startAnimation();
            } else {
              this.controls.enabled = true;
              if (this.flyControls) {
                this.flyControls.dispose();
                this.flyControls = null;
              }
              this.stopAnimation();
            }
            break;
          case 'controls-config':
            if (this.flyControls) {
              for (const [key, value] of Object.entries(action.config)) {
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                this.flyControls[key] = value;
              }
            }
            break;
          case 'spatialize-end':
            this.exitSpatializeManual();
            this.searchManager.queryImages();
            break;
          case 'spatialize-abort':
            this.exitSpatializeManual();
            this.searchManager.images$.pipe(take(1)).subscribe((images) => {
              this.loadImages(images);
            });
            break;
          case 'poi-start':
            if (!this.poiCursor) {
              this.poiCursor = new PointOfInterest();
              this.poiCursor.select();
              this.scene.add(this.poiCursor);
            }
            break;
          case 'poi-form':
            this.ngZone.run(() => {
              this.modalService.show(PoiModalComponent, {
                class: 'modal-lg',
                initialState: {
                  object: action.object || undefined,
                  position: action.position,
                },
              });
            });
            break;
          case 'poi-end':
            if (this.poiCursor) {
              this.scene.remove(this.poiCursor);
              this.poiCursor.dispose();
              this.poiCursor = null;
              this.setSelected();
              this.animateAsync();
            }
            break;
          case 'poi-open':
            this.ngZone.run(() => {
              this.modalService.show(PoiModalComponent, {
                class: 'modal-lg',
                initialState: { poi: action.poi },
              });
            });
            break;
          case 'poi-camera':
            this.startPoiCameraPreview(action.poi);
            break;
          case 'screenshot': {
            // get image from canvas and open in new browser tab
            const data = this.renderer.domElement.toDataURL();
            const document = window.open('', '_blank')!.document;
            document.head.innerHTML = `<title>Screenshot</title><link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" crossorigin="anonymous">`;
            document.body.innerHTML = `<div><a class="m-2 btn btn-sm btn-outline-primary" href="${data}" download>Download PNG file</a></div><img class="m-2 p-2 border border-secondary" src="${data}"/>`;
            break;
          }
          case 'object-temp':
            this.loadTempObject(action.file);
            break;
          case 'transform-start':
            this.setTransformControls(action.object);
            this.focusObjects([action.object]);
            break;
          case 'transform-end':
            if (this.transformControls?.object) {
              const group = this.transformControls.object;
              this.setTransformControls(null);
              const obj = group.children[0];
              const item = obj.userData['item'];
              const matrix = obj.matrixWorld.clone();
              group.remove(obj);
              this.scene.remove(group);
              if (action.keepObject) {
                obj.matrixAutoUpdate = false;
                obj.matrix = new Matrix4();
                obj.applyMatrix4(matrix);
                item.toggle(true);
              } else {
                this.objects.remove(item);
                item.dispose();
              }
              this.animateAsync();
            }
            break;
          case 'transform-update': {
            const item = this.objects.getByName(action.object.id);
            if (item) {
              const group = this.encapsulateObject(item.object);
              this.events.callAction$.next({
                key: 'transform-start',
                object: group,
                mode: 'update',
              });
            }
            break;
          }
          case 'projective-setup':
            this.enterProjectiveSetup(action.item);
            break;
          case 'projective-exit':
            this.exitProjectiveSetup(action.camera);
        }
      });

    // listen to global events
    this.globalEvents.callAction$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((action) => {
        switch (action.key) {
          case 'spatialize':
            this.enterSpatializeManual(action.image);
            break;
          case 'visualization':
            this.setVisualization(action.type);
            break;
          case 'set-camera-view':
            this.setCameraView(action.camera);
            break;
          case 'load-temp-image':
            this.loadSpatialImage(action.image);
        }
      });

    // listen to search results
    this.searchManager.images$
      .pipe(
        takeUntil(this.unsubscribe$),
        concatMap((images) =>
          Cache.terrainManager.getAsync().then(() => images),
        ),
        debounceTime(500),
      )
      .subscribe((images) => {
        this.loadImages(images);
      });
    this.searchManager.models$
      .pipe(takeUntil(this.unsubscribe$), debounceTime(500))
      .subscribe((models) => {
        this.loadObjects(models);
      });
    this.searchManager.pois$
      .pipe(takeUntil(this.unsubscribe$), debounceTime(500))
      .subscribe((pois) => {
        this.loadPois(pois);
      });
    this.searchManager.terrain$
      .pipe(takeUntil(this.unsubscribe$), debounceTime(500))
      .subscribe((terrain) => {
        this.loadTerrain(terrain);
      });
    this.searchManager.map$
      .pipe(takeUntil(this.unsubscribe$), debounceTime(500))
      .subscribe((mapData) => {
        if (mapData) {
          Cache.terrainManager.setMap('data/' + mapData.file.path);
        } else {
          Cache.terrainManager.setMap(null);
        }
      });

    this.animateOnce();
  }

  private onControlsChange() {
    if (!this.isAnimating) {
      this.clampControls();
      this.exitIsolation(true);
      this.updateDynamicSceneContent();
      // this.animateThrottle20();
      this.animate();
    }
    // this.animateOnce();
    this.events.cameraUpdate$.next(this.camera);
  }

  private clampControls() {
    const v = this.controls.target.clone();
    const { min, max } = Cache.terrainManager.getBoundingBox();
    this.controls.target.clamp(
      new Vector3(min.x, -Infinity, min.z),
      new Vector3(max.x, Infinity, max.z),
    );
    v.sub(this.controls.target);
    this.controls.object.position.sub(v);
  }

  private updateOctree() {
    this.render();
    this.octree.update();
  }

  /////
  ///// ANIMATION LOOP / RENDER
  /////

  private startAnimation() {
    if (!this.isAnimating) {
      this.isAnimating = true;
      this.clock = new Clock();
      this.animate();
    }
  }

  private stopAnimation() {
    if (this.isAnimating) {
      this.events.cameraUpdate$.next(this.camera);
      this.isAnimating = false;
      this.clock?.stop();
      this.updateVisualizationAsync();
      this.animateAsync();
    }
  }

  private animateOnce() {
    this.updateDynamicSceneContent();
    if (!this.isAnimating) {
      this.animate();
    }
  }

  /**
   * Animation loop
   */
  private animate() {
    if (this.isAnimating) {
      TWEEN.update();

      if (!this.flyControls) {
        this.controls.update();
      }

      let doAnimate = false;

      // look for active Tweens
      if (TWEEN.getAll().length) {
        doAnimate = true;
      }

      // update fly controls
      if (!this.controls.enabled && this.flyControls) {
        this.flyControls.movementSpeed =
          Math.max(
            10,
            Math.abs(this.controls.target.y - this.camera.position.y),
          ) * (this.camera.userData['movementFactor'] || 1);
        this.flyControls.update(this.clock!.getDelta());
        doAnimate = true;
      }

      if (this.visualization instanceof WindMap) {
        this.visualization.draw();
        doAnimate = true;
      }

      if (doAnimate) {
        requestAnimationFrame(() => {
          this.animate();
        });
      } else {
        // nothing to animate -> stop animation loop
        this.stopAnimation();
      }
    }

    // position light depending on camera
    if (this.directionalLight) {
      this.directionalLight.position.set(4, 4, 4);
      const lightMatrix = new Matrix4().makeRotationFromQuaternion(
        this.camera.quaternion,
      );
      this.directionalLight.position.applyMatrix4(lightMatrix);
    }

    this.render();
  }

  /**
   * Render calls
   */
  private render() {
    if (this.renderer) {
      this.renderer.render(this.scene, this.camera);
    }
    if (this.poiCamera?.renderer) {
      this.poiCamera.renderer.render(this.scene, this.poiCamera.camera);
    }
  }

  /**
   * Update scale and orientation of dynamic scene content.
   */
  private updateDynamicSceneContent() {
    if (!this.inIsolation) {
      // objects for ground height test
      const groundTestObjects = this.getRaycastTestObjects('object');
      groundTestObjects.push(Cache.terrainManager.get());

      this.clusterTree.update(this.camera, (clusterPosition) => {
        // determine the ground height in order to render cluster above ground
        this.raycaster.set(
          new Vector3(clusterPosition.x, 1000, clusterPosition.z),
          new Vector3(0, -1, 0),
        );
        const intersection = this.raycast(groundTestObjects, true);
        return intersection?.point.y;
      });
    }

    this.images.forEach((img) => {
      img.updateTexture(this.camera.position, 30);
      img.updateOrientation(this.camera.position);
    }, true);

    this.pois.forEach((poi) => {
      poi.object.scaleByDistance(this.camera.position);
    }, true);

    if (this.poiCursor) {
      this.poiCursor.scaleByDistance(this.camera.position);
    }
  }

  /////
  ///// MOUSE EVENTS
  /////

  private onMouseDown(event: MouseEvent) {
    this.events.contextmenu$.next();
    this.events.tooltip$.next();
    this.mouseDownPos = this.getViewportPosition(event);
    this.mouseState = event.button;
  }

  private onMouseMove(event: MouseEvent) {
    const mouse = this.getViewportPosition(event);

    this.events.tooltip$.next();

    if (this.mouseState !== MouseState.NONE) {
      if (
        !this.isControlsMoving &&
        this.mouseDownPos.distanceTo(mouse) > 0.000001
      ) {
        this.isControlsMoving = true;
      }
    } else if (this.poiCursor) {
      this.prepareRaycaster(mouse);
      const intersection = this.raycast(
        this.getRaycastTestObjects('object', 'terrain'),
        true,
      );

      if (intersection && intersection.face) {
        this.poiCursor.visible = true;
        this.poiCursor.position.copy(
          intersection.point.add(intersection.face.normal),
        );
        // const lookPos = intersection.point.add(intersection.face.normal);
        // this.poiCursor.lookAt(lookPos);
      } else {
        this.poiCursor.visible = false;
      }

      this.animateOnce();
    } else {
      if (this.transformControls?.object) {
        return;
      }

      // just hovering
      this.prepareRaycaster(mouse);
      const testObjects = this.inIsolation
        ? this.getRaycastTestObjects('object')
        : this.getRaycastTestObjects('object', 'image', 'poi');
      const intersection = this.raycast(testObjects, true);

      if (intersection) {
        let obj: Object3D | null = intersection.object;
        while (obj) {
          const item = obj.userData['item'] || obj.userData['cluster'];
          if (item) {
            this.setHighlighted(item);
            this.hoverDebounce(item, new Vector2(event.offsetX, event.offsetY));
            break;
          }
          obj = obj.parent;
        }
      } else {
        this.setHighlighted();
        this.hoverDebounce.cancel();
      }
    }
  }

  private onMouseUp(event: MouseEvent) {
    if (this.mouseState === MouseState.NONE) {
      return;
    }

    const mouse = this.getViewportPosition(event);

    if (this.isControlsMoving) {
      this.isControlsMoving = false;
    } else {
      // click
      if (this.mouseState === MouseState.LEFT) {
        // one click selection
        this.hoverDebounce.cancel();
        this.selectRay(mouse, event.ctrlKey);

        // open poi create form if valid object is hit
        if (this.poiCursor) {
          let obj = null;
          if (this.selected[0] && this.selected[0] instanceof ObjectItem) {
            obj = this.selected[0].object.userData['source'];
          }

          // if it's not an object, check if it's terrain
          const intersection =
            !obj && this.raycast(this.getRaycastTestObjects('terrain'), true);

          // open poi form modal
          if (obj || intersection) {
            this.events.callAction$.next({
              key: 'poi-form',
              object: obj,
              position: this.poiCursor.position.clone(),
            });
          }
        }
      } else if (this.mouseState === MouseState.RIGHT) {
        // open context menu
        this.selectRay(mouse);
        if (this.selected[0]) {
          this.hoverDebounce.cancel();
          this.events.tooltip$.next();
          if (
            this.selected[0] instanceof ImageItem ||
            this.selected[0] instanceof ObjectItem ||
            this.selected[0] instanceof PoiItem
          ) {
            this.skipMouseLeaveEvent = true;
            this.events.contextmenu$.next({
              item: this.selected[0],
              position: new Vector2(event.offsetX, event.offsetY),
            });
          }
        }
      }
    }

    this.mouseState = MouseState.NONE;
  }

  private onDblClick() {
    const item = this.selected[0];
    if (item) {
      if (item instanceof ClusterObject) {
        this.focusCluster(item);
      } else if (item instanceof PoiItem) {
        this.events.callAction$.next({
          key: 'poi-open',
          poi: item.object.userData['resource'],
        });
      } else {
        item.focus();
      }
    }
  }

  private onMouseLeave(event: MouseEvent) {
    if (this.skipMouseLeaveEvent) {
      this.skipMouseLeaveEvent = false;
      return;
    }

    this.mouseState = MouseState.NONE;
    this.events.contextmenu$.next();
    this.events.tooltip$.next();
    this.setHighlighted();
    this.hoverDebounce.cancel();

    if (this.isControlsMoving) {
      this.isControlsMoving = false;
    }
  }

  private onMouseWheel(event: WheelEvent) {
    this.events.tooltip$.next();

    if (this.inIsolatedView) {
      if (event.deltaY < 0) {
        this.events.callAction$.next({ key: 'isolation-zoom' });
      } else {
        this.exitIsolation(true);
      }
    }
  }

  private onContextMenu(event: MouseEvent) {
    event.preventDefault();
  }

  @HostListener('document:keyup', ['$event'])
  private onKeyUp(event: KeyboardEvent) {
    if (
      ['INPUT', 'TEXTAREA', 'SPAN'].includes(
        (event.target as HTMLElement).tagName,
      )
    ) {
      return;
    }

    if (this.transformControls) {
      switch (event.key) {
        case 'q':
          this.transformControls.setSpace(
            this.transformControls.space === 'local' ? 'world' : 'local',
          );
          break;
        case 'w':
          this.transformControls.setMode('translate');
          break;
        case 'e':
          this.transformControls.setMode('rotate');
          break;
        case 'r':
          this.transformControls.setMode('scale');
          break;
        case 's':
          if (this.transformControls.translationSnap === null) {
            this.transformControls.setTranslationSnap(10);
            this.transformControls.setRotationSnap(MathUtils.degToRad(15));
            this.transformControls.setScaleSnap(0.25);
          } else {
            this.transformControls.setTranslationSnap(null);
            this.transformControls.setRotationSnap(null);
            this.transformControls.setScaleSnap(null);
          }
      }
      return;
    }

    switch (event.key) {
      case 'l':
        console.log(
          'Selected:',
          this.selected.length === 1 ? this.selected[0] : this.selected,
        );
    }
  }

  /**
   * Transform mouse/screen coordinates into viewport coordinates.
   */
  private getViewportPosition(event: MouseEvent): Vector2 {
    return new Vector2(
      (event.offsetX / this.SCREEN_WIDTH) * 2 - 1,
      -(event.offsetY / this.SCREEN_HEIGHT) * 2 + 1,
    );
  }

  /**
   * Transform viewport coordinates into screen coordinates.
   */
  private getScreenPosition(pos: Vector2): Vector2 {
    return new Vector2(
      (this.SCREEN_WIDTH * (pos.x + 1)) / 2,
      (this.SCREEN_HEIGHT * (-pos.y + 1)) / 2,
    );
  }

  /////
  ///// SELECTION / RAYCASTING
  /////

  /**
   * Set raycaster to be prepared for raycasting.
   * @param mouse - Mouse viewport coordinates.
   */
  private prepareRaycaster(mouse: Vector2) {
    this.raycaster.setFromCamera(mouse, this.camera);
  }

  /**
   * Execute raycasting and return first object/intersection.
   * @param testObjects - Array of objects to be tested.
   * @param recursive - If true, also check descendants.
   */
  private raycast(
    testObjects: Object3D[],
    recursive = false,
  ): Intersection | undefined {
    const intersects = this.raycaster.intersectObjects(testObjects, recursive);
    return intersects[0];
  }

  /**
   * Collect test objects.
   */
  private getRaycastTestObjects(
    ...types: ('object' | 'image' | 'poi' | 'terrain')[]
  ): Object3D[] {
    const testObjects: Object3D[] = [];

    if (types.includes('object')) {
      this.objects.forEach((item) => {
        testObjects.push(item.object);
      }, true);
    }

    if (types.includes('image')) {
      this.octree
        .search(
          this.raycaster.ray.origin,
          0,
          true,
          this.raycaster.ray.direction,
        )
        .forEach((item) => {
          testObjects.push(item.object);
        });
    }

    if (types.includes('poi')) {
      this.pois.forEach((item) => {
        testObjects.push(item.object.children[item.object.children.length - 1]);
      }, true);
    }

    if (types.includes('terrain')) {
      testObjects.push(Cache.terrainManager.get());
    }

    return testObjects;
  }

  /**
   * Selection by a simple click.
   * @param mouse - Mouse position in viewport coordinates.
   * @param ctrlKey - ctrlKey pressed
   */
  private selectRay(mouse: Vector2, ctrlKey = false) {
    this.prepareRaycaster(mouse);
    const testObjects = this.getRaycastTestObjects('object', 'image', 'poi');
    const intersection = this.raycast(testObjects, true);

    if (intersection) {
      let obj: Object3D | null = intersection.object;
      while (obj) {
        const item = obj.userData['item'] || obj.userData['cluster'];
        if (item) {
          this.setSelected(item, ctrlKey);
          return;
        }
        obj = obj.parent;
      }

      console.warn('Raycast hit unknown object', intersection);
    } else {
      this.setSelected(null, ctrlKey);
    }
  }

  /**
   * Deselect any selected item and assign original material,
   * then select item and assign selection material.
   */
  private setSelected(
    item: ObjectItem | ImageItem | ClusterObject | PoiItem | null = null,
    onlySelect = false,
    onlyDeselect = false,
  ) {
    let selectionChanged = false;

    // deselect all
    if (this.selected.length && !onlySelect) {
      this.selected.forEach((oldItem) => {
        this.selectItem(oldItem, false);
        selectionChanged = true;
      });
      this.selected = [];
    }

    if (item && !onlyDeselect && !this.selected.includes(item)) {
      // select item
      this.selectItem(item);
      this.selected.push(item);
      selectionChanged = true;
    } else if (item && !onlyDeselect && this.selected.includes(item)) {
      // deselect single item
      this.selectItem(item, false);
      this.selected.splice(this.selected.indexOf(item), 1);
      selectionChanged = true;
    }

    if (selectionChanged) {
      this.animateAsync();
    }

    if (
      this.specialMode.inExhibitionMode &&
      this.selected[0] instanceof ImageItem
    ) {
      this.events.callAction$.next({
        key: 'preview-start',
        item: this.selected[0],
      });
    }
  }

  private selectItem(
    item: ObjectItem | ImageItem | ClusterObject | PoiItem,
    select = true,
  ) {
    if (item instanceof ClusterObject) {
      if (select) {
        item.explode();
      } else {
        item.implode();
      }
    }
    item.select(select);
  }

  /**
   * Highlight/unhighlight item.
   */
  private setHighlighted(
    item?: ObjectItem | ImageItem | ClusterObject | PoiItem,
  ) {
    let highlightChanged = false;

    if (this.highlighted && this.highlighted !== item) {
      this.highlighted.highlight(false);
      if (this.highlighted instanceof ClusterObject) {
        if (
          !(this.visualization && this.visualization instanceof ClusterChart)
        ) {
          this.highlighted.implode();
        }
      }
      this.highlighted = null;
      highlightChanged = true;
    }

    if (item && this.highlighted === null) {
      item.highlight(true);
      this.highlighted = item;
      highlightChanged = true;
    }

    if (highlightChanged) {
      this.animateAsync();
    }
  }

  /**
   * Set object to be transformed by TransformControls.
   */
  private setTransformControls(obj: Object3D | null) {
    if (!obj && !this.transformControls) {
      return;
    }

    if (!this.transformControls) {
      // init transform controls, listen to events
      this.transformControls = new TransformControls(
        this.camera,
        this.renderer.domElement,
      );

      fromEvent(this.transformControls, 'change')
        .pipe(takeUntil(this.unsubscribe$), throttleTime(15))
        .subscribe(() => {
          this.updatePoiCameraPreview();
          this.onControlsChange();
        });

      fromEvent(this.transformControls, 'dragging-changed')
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((event) => {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          this.controls.enabled = !event['value'];
        });
    }

    // show all axes (reset to default)
    this.transformControls.showX = true;
    this.transformControls.showY = true;
    this.transformControls.showZ = true;

    if (obj) {
      this.transformControls.attach(obj);
      this.scene.add(this.transformControls);
    } else {
      this.transformControls.detach();
      this.scene.remove(this.transformControls);
    }
  }

  /////
  ///// TERRAIN
  /////

  /**
   * Load terrain model from file provided by Elevation API or custom DEM model.
   */
  private async loadTerrain(terrain: string | TerrainDto): Promise<void> {
    if (!terrain) {
      return;
    }
    if (typeof terrain === 'string') {
      await Cache.terrainManager.loadElevationApiTerrain(terrain);
    } else {
      await Cache.terrainManager.loadCustomTerrain(terrain);
    }
    this.animateAsync();

    this.updateControlsTargetLevel();

    // load OSM buildings
    const osmMeta = await this.searchManager.queryOSMFile(terrain);
    await this.osmManager.load(osmMeta);

    this.updateOsmBuildings();
  }

  /**
   * Set controls target Y to vertical minimum of terrain geometry.
   */
  private updateControlsTargetLevel() {
    const { min } = Cache.terrainManager.getBoundingBox();
    this.controls.target.y = min.y;
  }

  /**
   * Update visibility and position of OSM buildings.
   */
  private updateOsmBuildings(): void {
    this.osmManager.update(this.objects.get().map((o) => o.object));
    this.animateAsync();
  }

  /////
  ///// OBJECTS / MODELS
  /////

  private async loadObjects(objects: ObjectDto[]) {
    this.setSelected();

    const toBeCreated: ObjectDto[] = [];
    const toBeUpdated: { item: ObjectItem; resource: ObjectDto }[] = [];
    const toBeRemoved: ObjectItem[] = [];

    // look if search results are already part of collection or not
    objects.forEach((obj) => {
      const item = this.objects.getByName(obj.id);
      if (item) {
        toBeUpdated.push({
          item,
          resource: obj,
        });
      } else {
        toBeCreated.push(obj);
      }
    });

    // get all items that are not part of search results anymore
    this.objects.forEach((item) => {
      if (!objects.find((obj) => item.name === obj.id)) {
        toBeRemoved.push(item);
      }
    });

    // remove "missing" objects
    toBeRemoved.forEach((item) => {
      this.scene.remove(item.object);
      this.objects.remove(item);
      item.dispose();
    });

    // update still existing objects
    toBeUpdated.forEach((value) => {
      value.item.label = value.resource.name;
      value.item.object.name = value.resource.id;
      value.item.object.userData['id'] = value.resource.id;
      value.item.object.userData['name'] = value.resource.name;
    });

    this.animateAsync();

    // load new objects
    await Promise.all(toBeCreated.map((obj) => this.loadObject(obj)));

    this.updateOsmBuildings();
  }

  private loadObject(item: ObjectDto) {
    return new Promise<void>((resolve) => {
      Cache.gltfLoader.load(
        'data/' +
          item.file.path +
          (item.vrcity_noEdges && item.file.noEdges
            ? item.file.noEdges
            : item.file.file),
        (gltf) => {
          const obj = gltf.scene.children[0];

          obj.name = item.id;
          obj.userData['id'] = item.id;
          obj.userData['name'] = item.name;
          obj.userData['source'] = item;

          obj.position.copy(
            coordsConverter.fromLatLon(
              item.origin.latitude,
              item.origin.longitude,
              item.origin.altitude,
            ),
          );
          obj.rotation.set(
            item.origin.omega,
            item.origin.phi,
            item.origin.kappa,
            'YXZ',
          );
          if (item.origin.scale) {
            if (Array.isArray(item.origin.scale)) {
              obj.scale.fromArray(item.origin.scale);
            } else {
              obj.scale.set(
                item.origin.scale,
                item.origin.scale,
                item.origin.scale,
              );
            }
          }

          const objItem = new ObjectItem(obj, item.name);
          this.objects.add(objItem);

          objItem.toggle(true);

          resolve();
        },
        undefined,
        (xhr) => {
          console.error("Couldn't load object", xhr);
          resolve();
        },
      );
    });
  }

  private loadTempObject(file: TempObjectDto) {
    Cache.gltfLoader.load('data/' + file.path + file.file, (gltf) => {
      const obj = gltf.scene.children[0];
      obj.userData['temp'] = file;

      const objItem = new ObjectItem(obj, file.name);
      this.objects.add(objItem);

      const group = this.encapsulateObject(obj);

      // place uploaded object on terrain model
      this.raycaster.set(
        new Vector3(group.position.x, 1000, group.position.z),
        new Vector3(0, -1, 0),
      );
      const intersection = this.raycast(
        this.getRaycastTestObjects('terrain'),
        true,
      );
      if (intersection) group.position.setY(intersection.point.y);

      this.events.callAction$.next({
        key: 'transform-start',
        object: group,
        mode: 'new',
      });
    });
  }

  private encapsulateObject(obj: Object3D): Group {
    // compute center of geometry and lowest point
    const bbox = new Box3();
    bbox.expandByObject(obj);
    const center = bbox.getCenter(new Vector3());
    const offset = new Vector3(center.x, bbox.min.y, center.z);
    obj.position.sub(offset);
    obj.matrixAutoUpdate = true;

    // wrap in group for transform controls
    const group = new Group();
    group.position.add(offset);
    group.add(obj);

    // add to scene
    this.scene.add(group);

    return group;
  }

  /////
  ///// SPATIAL IMAGES
  /////

  private loadImages(images: ImageDto[]) {
    this.setSelected();

    const toBeCreated: ImageDto[] = [];
    const toBeUpdated: { item: ImageItem; resource: ImageDto }[] = [];
    const toBeRemoved: ImageItem[] = [];

    // look if search results are already part of collection or not
    images.forEach((img) => {
      // only spatialized images
      if (img.spatialStatus < 3) {
        return;
      }
      const item = this.images.getByName(img.id);
      if (item) {
        toBeUpdated.push({
          item,
          resource: img,
        });
      } else {
        toBeCreated.push(img);
      }
    });

    // get all items that are not part of search results anymore
    this.images.forEach((item) => {
      if (!images.find((img) => item.name === img.id)) {
        toBeRemoved.push(item);
      }
    });

    this.clusterTree.clean();
    this.images.get().forEach((item) => {
      this.octree.remove(item.object.collisionObject);
      this.scene.remove(item.object);
    });

    // remove "missing" images
    toBeRemoved.forEach((item) => {
      this.images.remove(item);
      item.dispose();
    });

    // update still existing images
    toBeUpdated.forEach((value) => {
      value.item.source = value.resource;
      value.item.object.userData['source'] = value.resource;
    });

    // load spatial images for new results
    toBeCreated.forEach((img) => {
      this.loadSpatialImage(img);
    });

    // add to scene
    if (Settings.images.clusterEnabled) {
      this.clusterTree.bulkInsert(this.images.get().map((item) => item.object));
    } else {
      this.images.get().forEach((item) => {
        // this.octree.add(item.object.collisionObject);
        // this.scene.add(item.object);
        item.toggle(true);
      });
      this.updateOctreeAsync();
    }

    this.animateAsync();
    this.updateVisualizationAsync();
  }

  private loadSpatialImage(img: ImageDto) {
    const restricted =
      !this.user?.canEditItem(img.camera, img.uploadedBy) &&
      img.restrictedAccess;
    const imagePane = new ImagePane({
      texture:
        'data/' +
        img.file.path +
        (restricted ? img.file.thumb : img.file.preview),
      preview: 'data/' + img.file.path + img.file.tiny,
      width: img.file.width,
      height: img.file.height,
      ck: img.camera.ck || undefined,
      offset: img.camera.offset,
      showMarker: img.annotationsAvailable,
    });

    imagePane.position.copy(
      coordsConverter.fromLatLon(
        img.camera.latitude,
        img.camera.longitude,
        img.camera.altitude,
      ),
    );

    if (img.camera.ck) {
      imagePane.rotation.set(
        img.camera.omega!,
        img.camera.phi!,
        img.camera.kappa!,
        'YXZ',
      );
    } else {
      // compute altitude
      Cache.terrainManager.getAsync().then((terrain) => {
        this.raycaster.set(
          coordsConverter
            .fromLatLon(
              img.camera.latitude,
              img.camera.longitude,
              img.camera.altitude,
            )
            .setY(10000),
          new Vector3(0, -1, 0),
        );
        const intersection = this.raycast([terrain], true);
        if (intersection) {
          imagePane.position.copy(
            coordsConverter.fromLatLon(
              img.camera.latitude,
              img.camera.longitude,
              intersection.point.y + 2,
            ),
          );
          this.animateAsync();
        }
      });
    }

    imagePane.name = img.id;
    imagePane.userData['source'] = img;

    const item = new ImageItem(imagePane, img.title);
    if (Settings.images.opacity !== 1.0) {
      item.setOpacity(Settings.images.opacity);
    }
    item.setScale(Settings.images.scale);

    this.images.add(item);
  }

  private setSpatialImageView(item: ImageItem) {
    this.exitIsolation();
    this.clusterTree.update(this.camera);
    this.enterImageIsolation(item);

    // reset controls restriction
    this.controls.maxPolarAngle = Math.PI;

    const obj = item.object;

    // new controls/rotation anchor
    const end = new Vector3(0, 0, -100)
      .applyQuaternion(obj.quaternion)
      .add(obj.position);

    // update near clipping plane
    const imageDistance = Math.abs(obj.image.position.z * obj.scale.z);
    if (this.camera.near > imageDistance * 0.8) {
      this.camera.near = imageDistance * 0.8;
      this.camera.updateProjectionMatrix();
    }

    this.startCameraTween(
      obj.position,
      end,
      obj.fov,
      500,
      TWEEN.Easing.Quadratic.InOut,
    );
  }

  private enterImageIsolation(item: ImageItem) {
    if (Settings.images.clusterEnabled) {
      this.clusterTree.hide();
    } else {
      this.images.forEach((img) => {
        img.toggle(false);
      }, true);
    }
    this.inIsolation = true;
    this.inIsolatedView = true;
    this.controls.enabled = false;
    item.toggle(true);
    this.events.callAction$.next({ key: 'isolation-start', item });
  }

  private exitIsolation(restoreView = false) {
    if (!this.inIsolatedView) {
      return;
    }
    this.inIsolation = false;
    this.inIsolatedView = false;
    this.controls.enabled = true;
    this.events.callAction$.next({ key: 'isolation-exit' });

    if (!Settings.images.clusterEnabled) {
      this.images.get().forEach((img) => {
        img.toggle(true);
      });
    }

    // return to better navigation when leaving image zoom
    if (restoreView) {
      this.restoreView();
    }
  }

  private async enterProjectiveSetup(item: ImageItem) {
    if (Settings.images.clusterEnabled) {
      this.clusterTree.hide();
    } else {
      this.images.forEach((img) => {
        img.toggle(false);
      }, true);
    }
    this.inIsolation = true;
    this.inProjectiveSetup = true;
    item.toggle(true);

    // prepare scene with projective material
    const camera = await item.object.createProjectorCamera();
    camera.far = item.source.vrcity_projectionDistance || 150;
    this.scene.add(camera.createHelper());

    const projectiveMat = Cache.materials.get(
      'projectiveMat',
    ) as ProjectiveMaterial;
    projectiveMat.updateCameras([camera]);

    this.events.callAction$.next({ key: 'projective-start', item, camera });

    Settings.objects.shading = ShadingKey.Projective;
    this.objects.forEach((objItem) => {
      objItem.applyShading();
    });
    this.osmManager.applyShading(projectiveMat);

    this.animateAsync();
  }

  private exitProjectiveSetup(virtualCam: VirtualProjectorCamera) {
    if (!this.inProjectiveSetup) {
      return;
    }
    this.inIsolation = false;
    this.inProjectiveSetup = false;

    // cleanup scene
    if (virtualCam.helper) this.scene.remove(virtualCam.helper);
    virtualCam.dispose(false);
    const mat = Cache.materials.get('projectiveMat') as ProjectiveMaterial;
    mat.updateCameras([]);

    if (!Settings.images.clusterEnabled) {
      this.images.get().forEach((img) => {
        img.toggle(true);
      });
    }
    Settings.objects.shading = ShadingKey.Color;
    this.objects.forEach((objItem) => {
      objItem.applyShading();
    });
    this.osmManager.applyShading();

    this.animateAsync();
  }

  /////
  ///// POINTS OF INTEREST
  /////

  private loadPois(pois: Uh4dPoiDto[]) {
    this.setSelected();

    const toBeCreated: Uh4dPoiDto[] = [];
    const toBeUpdated: { item: PoiItem; resource: Uh4dPoiDto }[] = [];
    const toBeRemoved: PoiItem[] = [];

    // look if search results are already part of collection or not
    pois.forEach((poi) => {
      const item = this.pois.getByName(poi.id);
      if (item) {
        toBeUpdated.push({
          item,
          resource: poi,
        });
      } else {
        toBeCreated.push(poi);
      }
    });

    // get all items that are not part of search results anymore
    this.pois.forEach((item) => {
      if (!pois.find((poi) => item.name === poi.id)) {
        toBeRemoved.push(item);
      }
    });

    // remove "missing" objects
    toBeRemoved.forEach((item) => {
      this.scene.remove(item.object);
      this.pois.remove(item);
      item.dispose();
    });

    // update still existing objects
    toBeUpdated.forEach((value) => {
      value.item.label = value.resource.title;
      value.item.object.name = value.resource.id;
      value.item.object.userData['id'] = value.resource.id;
      value.item.object.userData['resource'] = value.resource;
    });

    // load new objects
    toBeCreated.forEach((poi) => {
      this.loadPointOfInterest(poi);
    });

    this.animateAsync();
  }

  private loadPointOfInterest(data: Uh4dPoiDto) {
    const obj = new PointOfInterest(data.timestamp ? 0x0c5363 : undefined);
    obj.name = data.id;
    obj.userData['id'] = data.id;
    obj.userData['resource'] = data;

    obj.position.copy(
      coordsConverter.fromLatLon(
        data.location.latitude,
        data.location.longitude,
        data.location.altitude,
      ),
    );

    const item = new PoiItem(obj, data.title);
    this.pois.add(item);

    item.toggle(true);
  }

  /**
   * Init poi preview camera and renderer, create dummy object and attach to transform controls.
   */
  private startPoiCameraPreview(poi: PoiItem) {
    this.stopPoiCameraPreview();

    // toggle canvas element
    this.showPreviewCanvas = true;
    this.cdr.detectChanges();

    // init preview renderer and camera
    const renderer = new WebGLRenderer({
      antialias: true,
      alpha: false,
      canvas: this.previewCanvasElement!.nativeElement,
    });
    const camera = new PerspectiveCamera(
      65,
      1,
      Settings.defaults.NEAR,
      Settings.defaults.FAR,
    );
    camera.position.setY(2);

    // cylindrical dummy object
    const dummy = new Mesh(
      new CylinderGeometry(0.2, 1, 2),
      new MeshStandardMaterial({ color: 0xdddd00 }),
    );
    dummy.position.setY(1);

    // group with dummy and camera
    const group = new Group();
    group.add(dummy, camera);
    this.scene.add(group);

    // set object and camera 30 meters next to poi
    group.position.copy(poi.object.position).add(new Vector3(0, 0, 30));

    this.poiCamera = {
      renderer,
      camera,
      poi,
      object: group,
      lookAt: poi.object.position,
    };

    // set transform controls
    this.setTransformControls(group);
    this.transformControls!.showY = false;

    this.animateOnce();

    this.focusPoints([poi.object.position, group.position]);
  }

  /**
   * Update poi preview camera object: 1. position on terrain surface (if any), 2. look at poi.
   */
  private updatePoiCameraPreview() {
    if (!this.poiCamera) {
      return;
    }

    this.raycaster.set(
      new Vector3(
        this.poiCamera.object.position.x,
        1000,
        this.poiCamera.object.position.z,
      ),
      new Vector3(0, -1, 0),
    );
    const intersection = this.raycast(
      this.getRaycastTestObjects('terrain'),
      true,
    );

    this.poiCamera.object.position.setY(
      intersection ? intersection.point.y : 0,
    );
    this.poiCamera.camera.lookAt(this.poiCamera.lookAt);
  }

  /**
   * Stop poi preview camera, dispose related objects.
   */
  stopPoiCameraPreview() {
    if (!this.poiCamera) {
      return;
    }

    // unset transform controls
    this.setTransformControls(null);

    // dispose geometry and renderer
    this.scene.remove(this.poiCamera.object);
    this.poiCamera.object.traverse((child) => {
      if (child instanceof Mesh) {
        child.geometry.dispose();
        (child.material as MeshStandardMaterial).dispose();
      }
    });

    this.poiCamera.renderer.forceContextLoss();
    this.poiCamera.renderer.dispose();

    this.poiCamera = null;
    this.ngZone.run(() => {
      this.showPreviewCanvas = false;
    });
  }

  /**
   * Save poi camera (update poi properties) and stop poi preview camera.
   */
  savePoiCamera() {
    const cameraPosition = new Vector3().setFromMatrixPosition(
      this.poiCamera!.camera.matrixWorld,
    );
    const poi: Uh4dPoiDto = this.poiCamera!.poi.object.userData['resource'];

    this.poisApiService
      .patch(poi.id, { camera: cameraPosition.toArray() })
      .subscribe((updatedPoi) => {
        poi.camera = updatedPoi.camera;
        this.stopPoiCameraPreview();
      });
  }

  /////
  ///// VISUALIZATIONS
  /////

  private setVisualization(type: VisualizationType) {
    // destroy old viz
    if (this.visualization) {
      if (this.visualization instanceof WindMap) {
        this.stopAnimation();
      }
      this.scene.remove(this.visualization);
      this.visualization.dispose();
      this.visualization = null;
    }

    switch (type) {
      case 'none':
        this.animateAsync();
        return;
      case 'heatmap':
        this.visualization = new HeatMap();
        break;
      case 'vectorfield':
        this.visualization = new VectorField();
        break;
      case 'windmap':
        this.visualization = new WindMap();
        this.visualization.setNumParticles(20000);
        break;
      case 'radialfan':
        // enable clustering if not enabled
        if (!Settings.images.clusterEnabled) {
          this.events.callAction$.next({
            key: 'cluster-enable',
            enabled: true,
          });
        }
        this.visualization = new RadialFan();
    }

    this.scene.add(this.visualization);

    this.updateVisualization();
  }

  private updateVisualization() {
    if (!this.visualization) return;

    let gradientConfig: VisualizationGradientConfig;

    if (this.visualization instanceof HeatMap) {
      gradientConfig = this.visualization.update(
        this.camera,
        Cache.terrainManager.get(),
        this.images.get().map((img) => img.object),
      );
    } else if (this.visualization instanceof VectorField) {
      gradientConfig = this.visualization.update(
        this.camera,
        Cache.terrainManager.get(),
        this.octree,
      );
    } else if (this.visualization instanceof WindMap) {
      gradientConfig = this.visualization.update(
        this.camera,
        Cache.terrainManager.get(),
        this.octree,
      );
      this.startAnimation();
    } else if (this.visualization instanceof RadialFan) {
      gradientConfig = this.visualization.update(
        this.camera,
        this.clusterTree.getActiveClusters(),
      );
    }

    this.ngZone.run(() => {
      this.globalEvents.callAction$.next({
        key: 'gradient-update',
        config: gradientConfig,
      });
    });

    this.animateAsync();
  }

  /////
  ///// FOCUS OBJECTS
  /////

  private focusSelection(selection: GenericItem[]) {
    if (selection.length < 1) {
      return;
    }

    const objs: Object3D[] = [];

    selection.forEach((item) => {
      objs.push(item.object);
    });

    this.focusObjects(objs);
  }

  /**
   * Focus all objects.
   */
  private focusAll() {
    const objs: Object3D[] = [];

    this.objects.forEach((item) => {
      objs.push(item.object);
    }, true);

    if (objs.length) {
      this.focusObjects(objs);
    }
  }

  /**
   * Focus given objects.
   */
  private focusObjects(objs: Object3D[]) {
    // determine maximum bounding box/sphere
    const boundingBox = new Box3();
    objs.forEach((obj) => {
      boundingBox.expandByObject(obj);
    });

    this.tweenCameraToFitSphere(boundingBox.getBoundingSphere(new Sphere()));
  }

  /**
   * Focus set of points.
   */
  private focusPoints(points: Vector3[], minRadius = 10) {
    // determine maximum bounding box/sphere
    const boundingBox = new Box3();
    points.forEach((point) => {
      boundingBox.expandByPoint(point);
    });

    const boundingSphere = boundingBox.getBoundingSphere(new Sphere());
    // enlarge radius
    boundingSphere.radius *= 2;
    boundingSphere.radius = Math.max(boundingSphere.radius, minRadius);

    this.tweenCameraToFitSphere(boundingSphere);
  }

  /**
   * Zoom to cluster.
   */
  private focusCluster(cluster: ClusterObject) {
    // compute bounding sphere
    const sphere = new Sphere().setFromPoints(
      cluster.getLeaves().map((leaf) => leaf.position),
    );

    this.tweenCameraToFitSphere(sphere);
    this.setSelected();
  }

  /**
   * Tween camera and controls target, such that sphere fits into viewport.
   */
  private tweenCameraToFitSphere(sphere: Sphere) {
    // calculate new camera.position and controls.target
    const s = new Vector3().subVectors(
      this.camera.position,
      this.controls.target,
    );
    const h =
      sphere.radius / Math.tan((this.camera.fov / 2) * MathUtils.DEG2RAD);
    const pos = new Vector3().addVectors(sphere.center, s.setLength(h));

    // adjust camera frustum (near, far)
    this.camera.near = Math.max(sphere.radius / 100, Settings.defaults.NEAR);
    this.camera.far = Math.max(sphere.radius * 100, Settings.defaults.FAR);
    this.camera.updateProjectionMatrix();

    // animate camera.position and controls.target
    this.startCameraTween(pos, sphere.center);

    // TODO: focus in orthographic view
  }

  /**
   * Tween camera facing north
   */
  private alignNorth() {
    const offset = new Vector3(
      this.camera.position.x,
      this.controls.target.y,
      this.camera.position.z,
    ).sub(this.controls.target);
    const radius = offset.length();

    // angle from z-axis around y-axis
    const theta = Math.atan2(offset.x, offset.z);

    // tween theta to `0` and apply new angle to camera
    new TWEEN.Tween({ theta })
      .to({ theta: 0 }, 1000)
      .easing(TWEEN.Easing.Cubic.InOut)
      .onUpdate((tweenObj) => {
        offset.set(
          radius * Math.sin(tweenObj.theta),
          this.camera.position.y,
          radius * Math.cos(tweenObj.theta),
        );

        this.camera.position
          .copy(new Vector3(this.controls.target.x, 0, this.controls.target.z))
          .add(offset);
      })
      .onComplete(() => {
        this.updateDynamicSceneContent();
        this.updateVisualizationAsync();
      })
      .start();

    this.startAnimation();
  }

  /**
   * Tween camera to home/default view
   */
  private setHomeView() {
    this.camera.near = Settings.defaults.NEAR;
    this.camera.far = Settings.defaults.FAR;
    this.camera.updateProjectionMatrix();
    this.startCameraTween(
      Settings.defaults.viewpoint.cameraPosition,
      Settings.defaults.viewpoint.controlsTarget,
      Settings.defaults.FOV,
      1000,
    );
  }

  private restoreView() {
    if (this.specialMode.inExhibitionMode) {
      // restrict to camera tilt to not go "underground"
      this.controls.maxPolarAngle = Math.PI / 2;
    }

    this.updateControlsTargetLevel();
    const cameraPos = this.camera.position.clone();
    const targetPos = this.controls.target.clone();
    const offset = new Vector3().subVectors(
      cameraPos,
      new Vector3(targetPos.x, cameraPos.y, targetPos.z),
    );
    offset.setLength(100);
    offset.y = targetPos.y + 100;
    cameraPos.add(offset);
    this.startCameraTween(cameraPos, targetPos, Settings.defaults.FOV);
  }

  private setCameraView(spatial: CameraDto) {
    const cameraPos = coordsConverter.fromLatLon(spatial);

    if ((spatial.omega ?? spatial.phi ?? spatial.kappa) === null) {
      this.focusPoints([cameraPos], 8);
      return;
    }

    const euler = new Euler(
      spatial.omega!,
      spatial.phi!,
      spatial.kappa!,
      'YXZ',
    );
    const targetPos = new Vector3(0, 0, -100)
      .applyQuaternion(new Quaternion().setFromEuler(euler))
      .add(cameraPos);

    const fov = spatial.ck
      ? 2 * Math.atan(1 / (2 * spatial.ck)) * MathUtils.RAD2DEG
      : null;

    this.startCameraTween(cameraPos, targetPos, fov);
  }

  /**
   * Animate `camera.position`, `controls.target` and `camera.fov`.
   */
  private startCameraTween(
    position: Vector3,
    target: Vector3,
    fov: number | null = null,
    duration = 500,
    easing = TWEEN.Easing.Cubic.InOut,
  ) {
    // camera.position
    new TWEEN.Tween(this.camera.position.clone())
      .to(position, duration)
      .easing(easing)
      .onUpdate((tweenObj) => {
        this.camera.position.copy(tweenObj);
      })
      .onComplete(() => {
        this.updateDynamicSceneContent();
        this.updateVisualizationAsync();
        this.globalEvents.callAction$.next({ key: 'camera-tween-complete' });
      })
      .start();

    // controls.target
    new TWEEN.Tween(this.controls.target.clone())
      .to(target, duration)
      .easing(easing)
      .onUpdate((tweenObj) => {
        this.controls.target.copy(tweenObj);
      })
      .start();

    // camera.fov
    if (fov && this.camera.fov !== fov) {
      new TWEEN.Tween({ fov: this.camera.fov })
        .to({ fov }, duration)
        .easing(easing)
        .onUpdate((tweenObj) => {
          this.camera.fov = tweenObj.fov;
          this.camera.updateProjectionMatrix();
        })
        .start();
    }

    this.startAnimation();
  }

  @HostListener('window:resize')
  private resizeViewport() {
    this.SCREEN_WIDTH = this.element.nativeElement.offsetWidth;
    this.SCREEN_HEIGHT = this.element.nativeElement.offsetHeight;

    this.camera.aspect = this.SCREEN_WIDTH / this.SCREEN_HEIGHT;
    this.camera.updateProjectionMatrix();

    this.renderer.setSize(this.SCREEN_WIDTH, this.SCREEN_HEIGHT, false);

    this.animateOnce();
  }

  private enterSpatializeManual(source: ImageDto) {
    // remove images
    this.setSelected();
    this.clusterTree.clean();
    const images = this.images.get().slice();
    images.forEach((item) => {
      this.octree.remove(item.object.collisionObject);
      this.scene.remove(item.object);
      this.images.remove(item);
      item.dispose();
    });

    this.exitIsolation();

    // call event
    this.events.callAction$.next({
      key: 'spatialize-start',
      image: source,
      camera: this.camera,
    });
  }

  private exitSpatializeManual() {
    // set new controls target/anchor
    const end = new Vector3(0, 0, -100);
    end.applyQuaternion(this.camera.quaternion);
    end.add(this.camera.position);
    this.controls.target.copy(end);

    this.events.callAction$.next({
      key: 'controls-set',
      type: 'default',
    });

    this.restoreView();
  }

  ngOnDestroy() {
    // unsubscribe
    this.unsubscribe$.next();
    this.unsubscribe$.complete();

    this.setSelected(null, false, true);
    this.setHighlighted();
    this.exitIsolation();
    this.setVisualization('none');
    this.stopPoiCameraPreview();

    // this.clusterTree.clean();

    // save camera and controls position
    Cache.viewpoint.cameraPosition.copy(this.camera.position);
    Cache.viewpoint.controlsTarget.copy(this.controls.target);

    this.scene.remove(this.osmManager.group);
    this.osmManager.dispose();

    // dispose controls
    this.controls.dispose();
    if (this.flyControls) {
      this.flyControls.dispose();
    }
    if (this.transformControls) {
      this.scene.remove(this.transformControls);
      this.transformControls.dispose();
    }

    // dispose renderer
    this.renderer.forceContextLoss();
    this.renderer.dispose();
  }
}
