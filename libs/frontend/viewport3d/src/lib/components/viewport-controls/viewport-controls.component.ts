import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ViewportEventsService } from '@uh4d/frontend/services';
import { Cache, Settings, Shadings } from '@uh4d/frontend/viewport3d-cache';
import { ObjectUploadComponent } from '../object-upload/object-upload.component';
import { TourManagerComponent } from '../tour-manager/tour-manager.component';

@Component({
  selector: 'uh4d-viewport-controls',
  templateUrl: './viewport-controls.component.html',
  styleUrls: ['./viewport-controls.component.sass'],
})
export class ViewportControlsComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();

  activeGroup = 'navigation';

  imageOpacity = Settings.images.opacity * 100;
  imageScale = Settings.images.scale;
  clusterEnabled = Settings.images.clusterEnabled;
  clusterDistance = Settings.images.clusterDistance;

  shadings = Shadings;
  activeShading = Settings.objects.shading;
  showOsmBuildings = Settings.objects.showOsmBuildings;

  terrainOpacity = Settings.terrain.opacity * 100;

  states = {
    poiCreating: false,
    projectiveSetup: false,
  };

  constructor(
    private events: ViewportEventsService,
    private modalService: BsModalService,
  ) {}

  ngOnInit(): void {
    this.events.callAction$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((action) => {
        switch (action.key) {
          case 'cluster-enable':
            this.clusterEnabled = Settings.images.clusterEnabled =
              action.enabled;
            break;

          case 'poi-start':
            this.states.poiCreating = true;
            break;

          case 'poi-end':
            this.states.poiCreating = false;
            break;

          case 'projective-start':
            this.states.projectiveSetup = true;
            break;

          case 'projective-exit':
            this.states.projectiveSetup = false;
        }
      });
  }

  toggleGroup(key: string) {
    this.activeGroup = key;
  }

  callAction(key: any) {
    this.events.callAction$.next({ key });
  }

  setImageOpacity() {
    const opacity = (Settings.images.opacity = this.imageOpacity / 100);
    Cache.images.setOpacity(opacity);
    this.events.callAction$.next({ key: 'cluster-update' });
  }

  setImageScale() {
    const scale = (Settings.images.scale = this.imageScale);
    Cache.images.forEach((item) => {
      item.setScale(scale);
    });
    this.events.callAction$.next({ key: 'cluster-update' });
  }

  toggleClustering() {
    this.events.callAction$.next({
      key: 'cluster-enable',
      enabled: this.clusterEnabled,
    });
  }

  setClusterDistance() {
    Settings.images.clusterDistance = this.clusterDistance;
    this.events.callAction$.next({ key: 'cluster-update' });
  }

  setShading() {
    Settings.objects.shading = this.activeShading;
    this.events.callAction$.next({ key: 'shading-update' });
  }

  toggleOsmBuildings() {
    Settings.objects.showOsmBuildings = this.showOsmBuildings;
    this.events.callAction$.next({
      key: 'osm-enable',
      enabled: this.showOsmBuildings,
    });
  }

  openUploadModal() {
    this.modalService.show(ObjectUploadComponent);
  }

  setTerrainOpacity() {
    const opacity = (Settings.terrain.opacity = this.terrainOpacity / 100);
    Cache.terrainManager.setOpacity(opacity);
  }

  openTourManager() {
    this.modalService.show(TourManagerComponent, {
      class: 'modal-lg',
    });
  }

  toggleTourCreation() {
    this.events.tourCollect$.next([]);
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
