import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { NotificationsService } from 'angular2-notifications';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Uh4dPoiDto } from '@uh4d/dto/interfaces';
import { ViewportEventsService } from '@uh4d/frontend/services';
import { TourFormModalComponent } from '../tour-form-modal/tour-form-modal.component';

@Component({
  selector: 'uh4d-tour-controls',
  templateUrl: './tour-controls.component.html',
  styleUrls: ['./tour-controls.component.sass'],
})
export class TourControlsComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();

  active = false;
  pois: Uh4dPoiDto[] = [];

  constructor(
    private events: ViewportEventsService,
    private notify: NotificationsService,
    private modalService: BsModalService,
  ) {}

  ngOnInit(): void {
    this.events.tourCollect$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((pois) => {
        if (pois) {
          this.active = true;
          this.pois = pois;
        } else {
          this.active = false;
          this.pois = [];
        }
      });
  }

  remove(poi: Uh4dPoiDto) {
    const index = this.pois.findIndex((p) => p.id === poi.id);
    if (index !== -1) {
      this.pois.splice(index, 1);
    }
  }

  save() {
    if (this.pois.length === 0) {
      this.notify.warn('Collect POIs in order to create a tour!');
      return;
    }

    this.modalService.show(TourFormModalComponent, {
      ignoreBackdropClick: true,
    });
  }

  abort() {
    this.events.tourCollect$.next(null);
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
