import {
  ChangeDetectorRef,
  Component,
  NgZone,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { Frustum, Matrix4, Plane, Quaternion, Vector3 } from 'three';
import { ImageItem } from '@uh4d/three';
import { Cache } from '@uh4d/frontend/viewport3d-cache';
import { ViewportEventsService } from '@uh4d/frontend/services';

/**
 * When the user clicks the button `Jump to location` or double-clicks an image item in 3D,
 * the isolation mode is entered.
 * All other images are hidden and additional controls are displayed.
 * The {@link ViewportControlsComponent viewport controls} are still usable to adjust image transparency, etc.
 *
 * Actions include:
 * - &#x2776; button `Exit image`: leave isolation mode
 * - &#x2777; button `Open image...`: open image sheet with metadata ({@link ImageSheetComponent})
 * - &#x2778; thin buttons at the edge of the component: jump to the nearest image in front|in back|left|right
 * - scroll down: leave isolation mode
 * - scroll up: open {@link ImageViewerComponent} to zoom into the image
 *
 * ![Screenshot](../assets/viewport-isolation-controls1.png)
 *
 * If the zoomable {@link ImageViewerComponent} is activated,
 * the scroll down behavior won't leave the isolation mode.
 * With the &#x2779; button `Close viewer`, the image viewer will be hidden again.
 *
 * ![Screenshot](../assets/viewport-isolation-controls2.png)
 */
@Component({
  selector: 'uh4d-isolation-controls',
  templateUrl: './isolation-controls.component.html',
  styleUrls: ['./isolation-controls.component.sass'],
})
export class IsolationControlsComponent implements OnInit, OnDestroy {
  /**
   * @ignore
   */
  private unsubscribe$ = new Subject<void>();

  /**
   * The image item that is isolated.
   * If set, the controls are displayed. If unset, the controls are hidden.
   */
  item: ImageItem | null = null;

  /**
   * If `true`, the {@link ImageViewerComponent} is displayed.
   */
  useViewer = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private ngZone: NgZone,
    private changeDetector: ChangeDetectorRef,
    private events: ViewportEventsService,
  ) {}

  /**
   * Invoked after the component is instantiated.
   * Listen to action events.
   */
  ngOnInit(): void {
    this.events.callAction$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((action) => {
        switch (action.key) {
          case 'isolation-start':
            // set item and unhide controls
            this.item = action.item;
            this.changeDetector.detectChanges();
            break;

          case 'isolation-zoom':
            // user scrolled up in isolation mode -> show image viewer
            if (this.item) {
              this.useViewer = true;
              this.changeDetector.detectChanges();
            }
            break;

          case 'isolation-exit':
            // hide isolation controls
            if (this.item) {
              this.item.toggle(false);
              this.item = null;
              this.useViewer = false;
              this.changeDetector.detectChanges();
            }
        }
      });
  }

  /**
   * Open image metadata sheet ({@link ImageSheetComponent}).
   */
  openImage(): void {
    this.ngZone.run(() => {
      this.router.navigate(['./image', this.item!.source.id], {
        relativeTo: this.route,
        queryParamsHandling: 'preserve',
      });
    });
  }

  /**
   * Exit isolation mode by triggering `isolation-exit` event.
   */
  exit(): void {
    this.events.callAction$.next({ key: 'isolation-exit' });
  }

  /**
   * Hide {@link ImageViewerComponent}.
   */
  exitViewer(): void {
    this.useViewer = false;
    this.changeDetector.detectChanges();
  }

  /**
   * Determine the nearest image item within the specified direction
   * and trigger a focus/'jump to location' (camera tween).
   * @param direction - Direction relative to the image orientation
   */
  zoomNext(direction: 'forward' | 'backward' | 'left' | 'right'): void {
    const position = new Vector3();
    const quaternion = new Quaternion();
    this.item!.object.matrixWorld.decompose(
      position,
      quaternion,
      new Vector3(),
    );
    const objMatrix = new Matrix4().compose(
      position,
      quaternion,
      new Vector3(1, 1, 1),
    );

    // create transformation matrix
    const matrix = new Matrix4();

    switch (direction) {
      case 'forward':
        matrix.makeRotationX(-Math.PI / 2);
        break;
      case 'backward':
        matrix.makeRotationX(Math.PI / 2);
        break;
      case 'left':
        matrix.makeRotationZ(Math.PI / 2);
        break;
      case 'right':
        matrix.makeRotationZ(-Math.PI / 2);
        break;
      default:
        return;
    }

    matrix.premultiply(objMatrix);

    // create frustum
    const n0 = new Vector3(0, 1, 0);
    const n1 = new Vector3(-1, 1, 0).normalize();
    const n2 = new Vector3(1, 1, 0).normalize();
    const n3 = new Vector3(0, 1, -1).normalize();
    const n4 = new Vector3(0, 1, 1).normalize();
    const n5 = new Vector3(0, -1, 0);

    const p0 = new Plane(n0, -1).applyMatrix4(matrix);
    const p1 = new Plane(n1, 0).applyMatrix4(matrix);
    const p2 = new Plane(n2, 0).applyMatrix4(matrix);
    const p3 = new Plane(n3, 0).applyMatrix4(matrix);
    const p4 = new Plane(n4, 0).applyMatrix4(matrix);
    const p5 = new Plane(n5, 200).applyMatrix4(matrix);

    const frustum = new Frustum(p0, p1, p2, p3, p4, p5);
    const normal = new Vector3(0, 0, -1).applyQuaternion(quaternion);
    const shortlist: ImageItem[] = [];

    // get images within frustum
    Cache.images.forEach((item) => {
      if (frustum.containsPoint(item.object.position)) {
        const nObj = new Vector3(0, 0, -1).applyQuaternion(
          item.object.quaternion,
        );
        if (nObj.dot(normal) > 0.5) {
          shortlist.push(item);
        }
      }
    });

    if (!shortlist.length) {
      return;
    }

    // sort by distance
    shortlist.sort((a, b) => {
      return (
        position.distanceTo(a.object.position) -
        position.distanceTo(b.object.position)
      );
    });

    // focus nearest image
    shortlist[0].focus();
  }

  /**
   * Invoked before the component gets destroyed.
   */
  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
