import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
import { delay, Subject, takeUntil } from 'rxjs';
import { ImageItem } from '@uh4d/three';
import { ViewportEventsService } from '@uh4d/frontend/services';

@Component({
  selector: 'uh4d-simple-preview',
  templateUrl: './simple-preview.component.html',
  styleUrls: ['./simple-preview.component.sass'],
  animations: [
    trigger('fadeImage', [
      transition('void => *', [
        style({ opacity: 0 }),
        animate(200, style({ opacity: 1 })),
      ]),
      transition('* => void', [animate(500, style({ opacity: 0 }))]),
    ]),
  ],
})
export class SimplePreviewComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();

  item: ImageItem | null = null;

  constructor(
    private readonly events: ViewportEventsService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit(): void {
    this.events.callAction$
      .pipe(takeUntil(this.unsubscribe$), delay(100))
      .subscribe((action) => {
        switch (action.key) {
          case 'preview-start':
            this.item = action.item;
            this.cdr.detectChanges();
            break;

          case 'preview-exit':
            if (this.item) {
              this.item = null;
              this.cdr.detectChanges();
            }
        }
      });
  }

  exit(): void {
    this.events.callAction$.next({ key: 'preview-exit' });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
