import { Component, OnDestroy, OnInit } from '@angular/core';
import { finalize, Subject, takeUntil } from 'rxjs';
import { ImageItem, VirtualProjectorCamera } from '@uh4d/three';
import { ImagesApiService } from '@uh4d/frontend/api-service';
import { ViewportEventsService } from '@uh4d/frontend/services';

@Component({
  selector: 'uh4d-projection-controls',
  templateUrl: './projection-controls.component.html',
  styleUrls: ['./projection-controls.component.sass'],
})
export class ProjectionControlsComponent implements OnInit, OnDestroy {
  private readonly unsubscribe$ = new Subject<void>();

  item: ImageItem | null = null;
  camera: VirtualProjectorCamera | null = null;
  distance = 150;

  isSaving = false;

  constructor(
    private readonly events: ViewportEventsService,
    private readonly imagesApiService: ImagesApiService,
  ) {}

  ngOnInit() {
    this.events.callAction$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((action) => {
        switch (action.key) {
          case 'projective-start':
            this.item = action.item;
            this.camera = action.camera;
            this.distance = this.camera.far;
            break;
          case 'projective-exit':
            this.item = null;
            this.camera = null;
        }
      });
  }

  changeDistance() {
    this.camera!.far = this.distance;
    this.camera!.updateProjectionMatrix();
    this.camera!.helper?.update();
    this.events.callAction$.next({ key: 'animate' });
  }

  blur(event: MouseEvent) {
    (event.target as HTMLElement).blur();
  }

  save() {
    this.isSaving = true;
    const resource = this.item!.source;
    this.imagesApiService
      .patch(resource.id, { vrcity_projectionDistance: this.distance })
      .pipe(finalize(() => (this.isSaving = false)))
      .subscribe(() => {
        resource.vrcity_projectionDistance = this.distance;
        this.exit();
      });
  }

  exit() {
    this.events.callAction$.next({
      key: 'projective-exit',
      camera: this.camera!,
    });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
