import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { fromEvent, Subject, take, takeUntil } from 'rxjs';
import { Vector2 } from 'three';
import { ImageItem, ObjectItem } from '@uh4d/three';
import { ViewportEventsService } from '@uh4d/frontend/services';

/**
 * Component that is displayed when the user hovers an image or object item in 3D.
 *
 * - If it's an image item, a thumbnail of the image gets displayed.
 * - If it's an object item, the object name gets displayed.
 *
 * ![Screenshot](../assets/viewport-tooltip-image.png)&emsp;
 * ![Screenshot](../assets/viewport-tooltip-object.png)
 */
@Component({
  selector: 'uh4d-viewport-tooltip',
  templateUrl: './viewport-tooltip.component.html',
  styleUrls: ['./viewport-tooltip.component.sass'],
})
export class ViewportTooltipComponent implements OnInit, OnDestroy {
  /**
   * @ignore
   */
  private unsubscribe$ = new Subject<void>();

  /**
   * If `false`, the tooltip is hidden.
   */
  active = false;

  /**
   * Position of the tooltip in screen (canvas) coordinates.
   */
  position = new Vector2(-999, -999);

  /**
   * Reference to image item. If set, thumbnail will be displayed.
   */
  image: ImageItem | null = null;
  /**
   * Reference to object item. If set, object label will be displayed.
   */
  object: ObjectItem | null = null;

  /**
   * Computed tooltip style with respect to screen position and label size.
   */
  tooltipStyle = {
    image: { left: '0', top: '0' },
    label: { top: '0', transform: '' },
    line: { top: '0' },
  };

  /**
   * Reference to main `div` element.
   */
  @ViewChild('dialogElement', { static: true })
  dialogElement!: ElementRef<HTMLDivElement>;

  constructor(
    private events: ViewportEventsService,
    private element: ElementRef,
    private renderer: Renderer2,
    private changeDetector: ChangeDetectorRef,
  ) {}

  /**
   * Invoked after component is instantiated.
   * Listen to tooltip events.
   */
  ngOnInit(): void {
    this.events.tooltip$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((value) => {
        if (!value && !this.active) return;

        // reset tooltip
        this.image = null;
        this.object = null;
        this.position.set(-999, -999);
        this.active = false;
        this.tooltipStyle = {
          image: { top: '0', left: '0' },
          label: { top: '-20px', transform: 'translate(-10px,-100%)' },
          line: { top: '-20px' },
        };

        // no value -> no tooltip to set
        if (!value) {
          this.changeDetector.detectChanges();
          return;
        }

        // set new tooltip
        if (value.item instanceof ImageItem) {
          this.image = value.item;
        } else if (value.item instanceof ObjectItem) {
          this.object = value.item;
        }
        this.position.copy(value.position);

        this.changeDetector.detectChanges();

        // use timeout to wait for changes by Angular
        setTimeout(async () => {
          const child = this.dialogElement.nativeElement.children[0];

          if (this.image) {
            // wait for image to be loaded
            await new Promise<void>((resolve) => {
              if ((child as HTMLImageElement).complete) {
                resolve();
              } else {
                fromEvent(child, 'load')
                  .pipe(take(1))
                  .subscribe(() => {
                    resolve();
                  });
              }
            });

            // compute image position
            this.tooltipStyle.image = {
              left:
                Math.min(
                  0,
                  this.element.nativeElement.offsetWidth -
                    value.position.x -
                    this.dialogElement.nativeElement.offsetWidth,
                ) + 'px',
              top:
                Math.min(
                  0,
                  this.element.nativeElement.offsetHeight -
                    value.position.y -
                    this.dialogElement.nativeElement.offsetHeight,
                ) + 'px',
            };
          } else if (this.object) {
            // determine label size and compute position
            const elementPos =
              this.element.nativeElement.getBoundingClientRect();
            const labelPos = child.children[0].getBoundingClientRect();
            const isAbove =
              value.position.y + labelPos.top - elementPos.top > 0;
            const labelOffset =
              elementPos.width - value.position.x - labelPos.width;

            this.tooltipStyle.label = {
              transform: `translate(${Math.min(
                labelOffset,
                Math.max(-10, -value.position.x),
              )}px,${isAbove ? '-100%' : '0'})`,
              top: isAbove ? '-20px' : '20px',
            };
            this.tooltipStyle.line = {
              top: isAbove ? '-20px' : '0',
            };
          }

          // make tooltip visible
          this.active = true;

          this.changeDetector.detectChanges();
        });
      });
  }

  /**
   * Invoked before the component gets destroyed.
   */
  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
