import { Component, OnDestroy, OnInit } from '@angular/core';
import { finalize, Subject, takeUntil } from 'rxjs';
import { Object3D, Quaternion, Vector3 } from 'three';
import { ObjectsApiService } from '@uh4d/frontend/api-service';
import {
  SearchManagerService,
  ViewportEventsService,
} from '@uh4d/frontend/services';
import { ObjectDto, UpdateObjectDto } from '@uh4d/dto/interfaces';
import { coordsConverter } from '@uh4d/frontend/viewport3d-cache';

/**
 * 3D viewport controls to translate, rotate, and scale a 3D object.
 *
 * ![Screenshot](../assets/viewport-transform-controls.png)
 */
@Component({
  selector: 'uh4d-transform-controls',
  templateUrl: './transform-controls.component.html',
  styleUrls: ['./transform-controls.component.sass'],
})
export class TransformControlsComponent implements OnDestroy, OnInit {
  private unsubscribe$ = new Subject<void>();

  private mode: 'new' | 'update' = 'new';

  transformGroup: Object3D | null = null;

  isSaving = false;

  constructor(
    private events: ViewportEventsService,
    private objectsApiService: ObjectsApiService,
    private searchManager: SearchManagerService,
  ) {}

  ngOnInit(): void {
    this.events.callAction$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((action) => {
        switch (action.key) {
          case 'transform-start':
            this.transformGroup = action.object;
            // backup original position, orientation, scale
            this.transformGroup.userData['position0'] =
              this.transformGroup.position.clone();
            this.transformGroup.userData['quaternion0'] =
              this.transformGroup.quaternion.clone();
            this.transformGroup.userData['scale0'] =
              this.transformGroup.scale.clone();
            this.mode = action.mode;
            break;
          case 'transform-end':
            this.transformGroup = null;
        }
      });
  }

  /**
   * Restore position, quaternion, or scale to its original values
   * (that were backed when transformation started).
   */
  reset(type: 'position' | 'quaternion' | 'scale'): void {
    if (this.transformGroup) {
      this.transformGroup[type].copy(this.transformGroup.userData[type + '0']);
    }

    this.events.callAction$.next({ key: 'animate' });
  }

  /**
   * Save transformation.
   * A temporary uploaded object will be stored constantly in the database.
   */
  save(): void {
    const obj = this.transformGroup!.children[0];
    console.log(obj.userData);
    console.log(obj.matrixWorld);
    const location = coordsConverter.toLatLon(obj.matrixWorld);
    const scale = new Vector3();
    obj.matrixWorld.decompose(new Vector3(), new Quaternion(), scale);

    this.isSaving = true;

    switch (this.mode) {
      case 'new':
        // save temporary upload as new object
        this.objectsApiService
          .save(
            Object.assign(obj.userData['temp'], location, {
              scale: scale.toArray(),
            }),
          )
          .pipe(
            finalize(() => {
              this.isSaving = false;
            }),
          )
          .subscribe((result) => {
            console.log(result);
            this.events.callAction$.next({
              key: 'transform-end',
              keepObject: false,
            });
            this.searchManager.queryModels();
          });
        break;

      case 'update': {
        // update position of existing object
        const resource: ObjectDto = obj.userData['source'];
        if (resource) {
          const body: UpdateObjectDto = {
            origin: {
              ...location,
              scale: scale.toArray(),
            },
          };
          this.objectsApiService
            .patch(resource.id, body)
            .pipe(
              finalize(() => {
                this.isSaving = false;
              }),
            )
            .subscribe((updatedObject) => {
              console.log(updatedObject);
              obj.userData['source'] = updatedObject;
              this.events.callAction$.next({
                key: 'transform-end',
                keepObject: true,
              });
            });
        }
      }
    }
  }

  /**
   * Cancel transformation.
   * Remove temporary uploaded 3D model or restore transformation of existing object.
   */
  abort(): void {
    switch (this.mode) {
      case 'new':
        // remove temporary upload
        if (this.transformGroup) {
          this.objectsApiService.removeUpload(
            this.transformGroup.children[0].userData['temp'].id,
          );
        }
        this.events.callAction$.next({
          key: 'transform-end',
          keepObject: false,
        });
        break;

      case 'update':
        // reset transformation
        this.reset('position');
        this.reset('quaternion');
        this.reset('scale');
        this.events.callAction$.next({
          key: 'transform-end',
          keepObject: true,
        });
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
