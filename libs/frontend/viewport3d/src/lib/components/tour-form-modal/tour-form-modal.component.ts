import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NotificationsService } from 'angular2-notifications';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ToursApiService } from '@uh4d/frontend/api-service';
import { Uh4dPoiDto } from '@uh4d/dto/interfaces/uh4d-poi';
import { ViewportEventsService } from '@uh4d/frontend/services';

@Component({
  selector: 'uh4d-tour-form-modal',
  templateUrl: './tour-form-modal.component.html',
  styleUrls: ['./tour-form-modal.component.sass'],
})
export class TourFormModalComponent implements OnInit {
  tourForm = new FormGroup({
    title: new FormControl('', Validators.required),
  });
  submitted = false;
  isSaving = false;

  pois: Uh4dPoiDto[] = [];

  constructor(
    private events: ViewportEventsService,
    private notify: NotificationsService,
    private modalRef: BsModalRef,
    private toursApiService: ToursApiService,
  ) {}

  ngOnInit(): void {
    this.pois = this.events.tourCollect$.getValue()!;
  }

  drop(event: CdkDragDrop<Uh4dPoiDto[]>) {
    moveItemInArray(this.pois, event.previousIndex, event.currentIndex);
  }

  removePoi(poi: Uh4dPoiDto) {
    const index = this.pois.findIndex((p) => p.id === poi.id);
    if (index !== -1) {
      this.pois.splice(index, 1);
    }
  }

  save() {
    this.submitted = true;

    if (this.tourForm.invalid) {
      this.notify.warn('Form must not be empty!');
      return;
    }

    this.isSaving = true;

    this.toursApiService
      .create(
        this.tourForm.value.title!,
        this.pois.map((p) => p.id),
      )
      .subscribe({
        next: (result) => {
          console.log(result);
          this.notify.success('Tour successfully saved!');
          this.events.tourCollect$.next(null);
          this.close();
        },
        error: (err) => {
          console.error(err);
          this.notify.error('Error while saving!', 'See console for details.');
          this.isSaving = false;
        },
      });
  }

  close() {
    this.modalRef.hide();
  }
}
