import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { MathUtils, PerspectiveCamera } from 'three';
import { NotificationsService } from 'angular2-notifications';
import { formatISO } from 'date-fns';
import { ImagesApiService } from '@uh4d/frontend/api-service';
import { ImageDto, UpdateImageDto } from '@uh4d/dto/interfaces';
import { coordsConverter } from '@uh4d/frontend/viewport3d-cache';
import { ViewportEventsService } from '@uh4d/frontend/services';

@Component({
  selector: 'uh4d-spatialize-manual',
  templateUrl: './spatialize-manual.component.html',
  styleUrls: ['./spatialize-manual.component.sass'],
})
export class SpatializeManualComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();
  private startTime?: string;

  image: ImageDto | null = null;
  camera: PerspectiveCamera | null = null;
  opacity = 50;
  precision = 1;
  controlMode = 'fly';

  constructor(
    private readonly events: ViewportEventsService,
    private readonly notify: NotificationsService,
    private readonly imagesApiService: ImagesApiService,
  ) {}

  ngOnInit(): void {
    this.events.callAction$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((action) => {
        switch (action.key) {
          case 'spatialize-start':
            this.image = action.image;
            this.camera = action.camera;
            this.controlMode = 'fly';
            this.opacity = 50;
            this.precision = 1;
            this.startTime = formatISO(new Date());
            this.setControls();
            break;
          case 'spatialize-end':
          case 'spatialize-abort':
            this.image = null;
            this.camera = null;
        }
      });
  }

  changeFOV() {
    this.camera!.updateProjectionMatrix();
  }

  changePrecision() {
    this.camera!.userData['movementFactor'] = this.precision;
    this.events.callAction$.next({
      key: 'controls-config',
      config: { rollSpeed: this.precision * 0.1 },
    });
  }

  setControls() {
    this.events.callAction$.next({
      key: 'controls-set',
      type: this.controlMode,
    });
    if (this.controlMode === 'fly') {
      this.changePrecision();
    }
  }

  save() {
    if (!this.image || !this.camera) {
      return;
    }

    const location = coordsConverter.toLatLon(this.camera.matrixWorld);

    const body: UpdateImageDto = {
      camera: {
        ...location,
        offset: [0, 0] as [number, number],
        ck: (1 / Math.tan((this.camera.fov / 2) * MathUtils.DEG2RAD)) * 0.5,
      },
    };

    this.imagesApiService
      .patch(this.image.id, body)
      .subscribe((updatedImage) => {
        Object.assign(this.image!, updatedImage);
        this.notify.success('Update successful!');
        this.events.callAction$.next({ key: 'spatialize-end' });
      });
  }

  abort() {
    this.events.callAction$.next({ key: 'spatialize-abort' });
  }

  blur(event: MouseEvent) {
    (event.target as HTMLElement).blur();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
