import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Subject, takeUntil } from 'rxjs';
import { Vector3 } from 'three';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import {
  SearchManagerService,
  ViewportEventsService,
} from '@uh4d/frontend/services';
import { ObjectDto, Uh4dPoiDto } from '@uh4d/dto/interfaces';
import { PoiSheetComponent } from '../poi-sheet/poi-sheet.component';
import { PoiDataService } from '../poi-data.service';
import { PoiFormComponent } from '../poi-form/poi-form.component';

@Component({
  selector: 'uh4d-poi-modal',
  standalone: true,
  imports: [CommonModule, Uh4dIconsModule, PoiSheetComponent, PoiFormComponent],
  templateUrl: './poi-modal.component.html',
  styleUrls: ['./poi-modal.component.sass'],
})
export class PoiModalComponent implements OnInit, OnDestroy {
  private unsubscribe$: Subject<void> = new Subject<void>();

  poi?: Uh4dPoiDto;

  object?: ObjectDto;

  position?: Vector3;

  constructor(
    private readonly modalRef: BsModalRef,
    private readonly searchManager: SearchManagerService,
    private readonly poiDataService: PoiDataService,
    private readonly events: ViewportEventsService,
  ) {}

  ngOnInit() {
    this.poiDataService.update$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        this.searchManager.queryPois();
      });
  }

  onCreate(poi: Uh4dPoiDto): void {
    this.poi = poi;
    this.events.callAction$.next({ key: 'poi-end' });
  }

  /**
   * Close modal.
   */
  close(): void {
    this.modalRef.hide();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
