import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Params, Router } from '@angular/router';
import {
  combineLatest,
  finalize,
  ReplaySubject,
  Subject,
  takeUntil,
} from 'rxjs';
import { NgxPermissionsModule } from 'ngx-permissions';
import { Uh4dPoiDto } from '@uh4d/dto/interfaces';
import {
  MapInteractiveComponent,
  RecordUserMetadataComponent,
  RecordValidationBoxComponent,
} from '@uh4d/frontend/ui-components';
import { AuthService } from '@uh4d/frontend/auth';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import { ConfirmModalService } from '@uh4d/frontend/confirm-modal';
import { PoisApiService } from '@uh4d/frontend/api-service';
import { ViewportEventsService } from '@uh4d/frontend/services';
import { Cache } from '@uh4d/frontend/viewport3d-cache';
import { PoiContentComponent } from '../poi-content/poi-content.component';
import { PoiDataService } from '../poi-data.service';
import { PoiFormComponent } from '../poi-form/poi-form.component';

@Component({
  selector: 'uh4d-poi-sheet',
  standalone: true,
  imports: [
    CommonModule,
    Uh4dIconsModule,
    RecordUserMetadataComponent,
    PoiContentComponent,
    PoiFormComponent,
    NgxPermissionsModule,
    RecordValidationBoxComponent,
    MapInteractiveComponent,
  ],
  templateUrl: './poi-sheet.component.html',
  styleUrls: ['./poi-sheet.component.sass'],
})
export class PoiSheetComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();

  private poi$ = new ReplaySubject<Uh4dPoiDto>(1);

  @Input({ required: true }) set poi(value: Uh4dPoiDto) {
    this.poi$.next(value);
  }

  @Input() showMap = false;

  @Output() closed = new EventEmitter<void>();

  poiValue!: Uh4dPoiDto;

  canEditPoi = false;
  canValidate = false;

  isEditing = false;

  isUpdating = false;

  collectedPois: Uh4dPoiDto[] | null = null;

  constructor(
    private readonly authService: AuthService,
    private readonly confirmService: ConfirmModalService,
    private readonly poisApiService: PoisApiService,
    private readonly poiDataService: PoiDataService,
    private readonly events: ViewportEventsService,
    private readonly router: Router,
  ) {}

  ngOnInit() {
    combineLatest({
      poi: this.poi$,
      user: this.authService.user$,
    })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(({ poi, user }) => {
        this.poiValue = poi;
        this.canEditPoi =
          !!user && user.canEditItem(poi.location, poi.createdBy);
        this.canValidate = !!user && user?.canEditItem(poi.location);
      });

    // collected POIs of new tour
    this.collectedPois = this.events.tourCollect$.getValue();
    // if this POI is already part of collection, do nothing
    if (
      this.collectedPois &&
      !!this.collectedPois.find((cp) => cp.id === this.poiValue.id)
    ) {
      this.collectedPois = null;
    }
  }

  /**
   * Add this POI to a new tour.
   */
  addToTour() {
    if (!this.collectedPois) return;

    this.collectedPois.push(this.poiValue);
    this.collectedPois = null;
  }

  /**
   * Jump to preferred camera position.
   */
  zoomPoi() {
    const item = Cache.pois.getByName(this.poiValue.id);
    if (item) item.focus();
    this.closed.emit();
  }

  onSave(savedPoi: Uh4dPoiDto) {
    this.isEditing = false;
    if (this.poiValue) {
      Object.assign(this.poiValue, savedPoi);
    }
  }

  openScene() {
    const queryParams: Params = {};
    if (this.poiValue.pending) {
      queryParams['q'] = 'pending';
    } else if (this.poiValue.declined) {
      queryParams['q'] = 'declined';
    }
    if (this.poiValue.date?.from) {
      queryParams['from'] = this.poiValue.date.from;
    }
    if (this.poiValue.date?.to) {
      queryParams['to'] = this.poiValue.date.to;
    }

    const url = this.router.serializeUrl(
      this.router.createUrlTree(
        [
          '/explore',
          `${this.poiValue.location.latitude},${this.poiValue.location.longitude}`,
        ],
        { queryParams },
      ),
    );

    window.open(url, '_blank');
  }

  validatePoi(accept: boolean) {
    this.isUpdating = true;
    this.poisApiService
      .updateValidationStatus(this.poiValue.id, false, !accept)
      .pipe(
        finalize(() => {
          this.isUpdating = false;
        }),
      )
      .subscribe((result) => {
        this.poiValue.pending = result.pending;
        this.poiValue.declined = result.declined;
        this.poiDataService.updated(this.poiValue);
      });
  }

  /**
   * Display confirm modal and call delete routine.
   */
  async remove(): Promise<void> {
    const confirmed = await this.confirmService.confirm({
      message: 'Do you really want to delete this Point of Interest?',
    });

    if (!confirmed) return;

    this.poisApiService.remove(this.poiValue.id).subscribe(() => {
      this.poiDataService.removed(this.poiValue);
      this.closed.emit();
    });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
