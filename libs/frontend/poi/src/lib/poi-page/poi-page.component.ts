import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Uh4dPoiDto } from '@uh4d/dto/interfaces';
import { PoiSheetComponent } from '../poi-sheet/poi-sheet.component';

@Component({
  selector: 'uh4d-poi-page',
  standalone: true,
  imports: [CommonModule, PoiSheetComponent],
  templateUrl: './poi-page.component.html',
  styleUrls: ['./poi-page.component.sass'],
})
export class PoiPageComponent implements OnInit {
  poi!: Uh4dPoiDto;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
  ) {}

  ngOnInit() {
    this.route.data.subscribe((data) => {
      this.poi = data['poi'];
    });
  }

  close() {
    this.router.navigate(['../..'], {
      relativeTo: this.route,
      queryParamsHandling: 'preserve',
      preserveFragment: true,
    });
  }
}
