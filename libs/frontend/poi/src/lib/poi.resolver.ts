import { inject } from '@angular/core';
import { ResolveFn, Router } from '@angular/router';
import { catchError, EMPTY } from 'rxjs';
import { Uh4dPoiDto } from '@uh4d/dto/interfaces';
import { PoisApiService } from '@uh4d/frontend/api-service';

/**
 * When entering the route, query database for poi entry with given `poiId` parameter.
 * If no entry found, navigate back to parent page.
 */
export const poiResolver: ResolveFn<Uh4dPoiDto> = (route) => {
  const router = inject(Router);
  const poiId = route.paramMap.get('poiId');

  if (!poiId) {
    return EMPTY;
  }

  return inject(PoisApiService)
    .get(poiId)
    .pipe(
      catchError(() => {
        let url = '';
        let parent = route.parent;
        while (parent) {
          if (parent.url[0]) {
            url = parent.url[0].path + '/' + url;
          }
          parent = parent.parent;
        }
        router.navigate([url], {
          queryParamsHandling: 'preserve',
        });
        return EMPTY;
      }),
    );
};
