import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormArray,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { finalize } from 'rxjs';
import { NotificationsService } from 'angular2-notifications';
import { Vector3 } from 'three';
import { damageFactors } from '@uh4d/config';
import { ObjectDto, Uh4dPoiDto } from '@uh4d/dto/interfaces';
import { ObjectsApiService, PoisApiService } from '@uh4d/frontend/api-service';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import { coordsConverter } from '@uh4d/frontend/viewport3d-cache';
import { PoiContentComponent } from '../poi-content/poi-content.component';
import { PoiDataService } from '../poi-data.service';

@Component({
  selector: 'uh4d-poi-form',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    Uh4dIconsModule,
    PoiContentComponent,
  ],
  templateUrl: './poi-form.component.html',
  styleUrls: ['./poi-form.component.sass'],
})
export class PoiFormComponent implements OnInit {
  @Output() saved = new EventEmitter<Uh4dPoiDto>();

  @Output() canceled = new EventEmitter<void>();

  @Input() poi?: Uh4dPoiDto;

  @Input() object?: ObjectDto;

  @Input() position?: Vector3;

  poiForm = new FormGroup({
    title: new FormControl('', Validators.required),
    content: new FormControl('', Validators.required),
    useObjectDate: new FormControl(false),
    from: new FormControl(''),
    to: new FormControl(''),
    factors: new FormArray(damageFactors.map(() => new FormControl(false))),
  });

  damageFactors = damageFactors;

  showPreview = false;

  submitted = false;
  isSaving = false;

  constructor(
    private readonly notify: NotificationsService,
    private readonly poisApiService: PoisApiService,
    private readonly objectsApiService: ObjectsApiService,
    private readonly poiDataService: PoiDataService,
  ) {}

  ngOnInit() {
    if (!this.poi && !this.position) {
      throw new Error('Either `poi` or `position` must be provided');
    }

    // set form if object is available
    if (this.object) {
      this.poiForm.patchValue({
        title: '',
        content: '',
        useObjectDate: true,
        from: this.object.date.from,
        to: this.object.date.to,
      });
    }

    if (this.poi) {
      if (this.poi.objectId && !this.object) {
        this.objectsApiService.get(this.poi.objectId).subscribe((obj) => {
          this.object = obj;
        });
      }
      this.poiForm.setValue({
        title: this.poi.title,
        content: this.poi.content,
        useObjectDate: this.poi.date.objectBound,
        from: this.poi.date.from,
        to: this.poi.date.to,
        factors: this.damageFactors.map(
          (value) => this.poi?.damageFactors?.includes(value.key) || false,
        ),
      });
    }

    // reset dates if checkbox is toggled
    this.poiForm.controls.useObjectDate.valueChanges.subscribe((changes) => {
      if (this.object && changes === true) {
        this.poiForm.controls.from.setValue(this.object.date.from);
        this.poiForm.controls.to.setValue(this.object.date.to);
      } else if (this.poi) {
        this.poiForm.controls.from.setValue(this.poi.date.from || '');
        this.poiForm.controls.to.setValue(this.poi.date.to || '');
      }
    });
  }

  /**
   * Submit form. Check if form is valid and either patch changes or save new POI.
   */
  save(): void {
    this.submitted = true;

    if (this.poiForm.invalid) {
      this.notify.warn('Form must not be empty!');
      return;
    }

    this.isSaving = true;

    const factors: string[] = this.poiForm.value
      .factors!.map((value, i) => value && this.damageFactors[i].key)
      .filter((value): value is string => !!value);

    const observable = this.poi
      ? // update existing poi
        this.poisApiService.patch(this.poi.id, {
          title: this.poiForm.value.title as string,
          content: this.poiForm.value.content as string,
          date: {
            from: this.poiForm.value.from || null,
            to: this.poiForm.value.to || null,
            objectBound: this.poiForm.value.useObjectDate as boolean,
          },
          damageFactors: factors,
        })
      : // save new poi
        this.poisApiService.create({
          title: this.poiForm.value.title as string,
          content: this.poiForm.value.content as string,
          location: coordsConverter.toLatLon(this.position!),
          date: {
            from: this.poiForm.value.from || undefined,
            to: this.poiForm.value.to || undefined,
            objectBound: this.poiForm.value.useObjectDate as boolean,
          },
          objectId: this.object?.id,
          camera: [],
          damageFactors: factors,
        });

    observable
      .pipe(
        finalize(() => {
          this.isSaving = false;
        }),
      )
      .subscribe({
        next: (poi) => {
          this.poiDataService.updated(poi);
          this.saved.emit(poi);
        },
        error: (err) => {
          console.error(err);
          this.notify.error('Error while saving!', 'See console for details.');
        },
      });
  }
}
