import { Injectable } from '@angular/core';
import { Uh4dPoiDto } from '@uh4d/dto/interfaces';
import { DataEventService } from '@uh4d/utils';

@Injectable({
  providedIn: 'root',
})
export class PoiDataService extends DataEventService<Uh4dPoiDto> {}
