import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { MarkdownModule } from 'ngx-markdown';

@Component({
  selector: 'uh4d-poi-content',
  standalone: true,
  imports: [CommonModule, MarkdownModule],
  templateUrl: './poi-content.component.html',
  styleUrls: ['./poi-content.component.sass'],
})
export class PoiContentComponent {
  @Input({ required: true }) set content(value: string | null | undefined) {
    this.setContent(value);
  }

  value = '';
  iframeUrl: SafeResourceUrl | null = null;

  constructor(private readonly sanitizer: DomSanitizer) {}

  /**
   * Set POI content. If it's a URL, then use sanitizer and prepare for iframe.
   */
  private setContent(value?: string | null): void {
    this.value = '';
    this.iframeUrl = null;

    if (!value) return;

    // set url for iframe
    if (/^https?:\/\/\S+$/.test(value)) {
      if (/^https:\/\/\S+$/.test(value)) {
        // bypass secure link
        this.iframeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(value);
      } else {
        // redirect insecure link
        this.iframeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
          'api/redirect?url=' + encodeURIComponent(value),
        );
      }
    } else {
      this.value = value;
    }
  }
}
