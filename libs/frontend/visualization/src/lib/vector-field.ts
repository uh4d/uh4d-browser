import {
  Camera,
  Color,
  CylinderGeometry,
  InstancedMesh,
  Matrix4,
  MeshBasicMaterial,
  Object3D,
  Quaternion,
  Vector2,
  Vector3,
} from 'three';
import { Octree } from '@brakebein/threeoctree';
import { VisualizationGradientConfig } from './visualization.types';
import {
  computeExtents,
  defaultGradientConfig,
  defaultPalette,
} from './helpers';

export class VectorField extends Object3D {
  private palette = defaultPalette;
  private readonly geometry: CylinderGeometry;
  private readonly material: MeshBasicMaterial;
  private mesh?: InstancedMesh<CylinderGeometry, MeshBasicMaterial>;

  radius: number;

  constructor() {
    super();

    this.geometry = new CylinderGeometry(0, 0.5, 1, 5, 1);
    this.material = new MeshBasicMaterial({
      depthWrite: false,
      depthTest: false,
    });

    this.radius = 30;
  }

  setRadius(radius: number): void {
    this.radius = radius;
  }

  update(
    camera: Camera,
    ground: Object3D,
    octree: Octree,
  ): VisualizationGradientConfig {
    const startTime = Date.now();

    // dispose old mesh
    if (this.mesh) {
      this.remove(this.mesh);
      this.mesh.dispose();
    }

    const {
      boundingBox: bbox,
      distance,
      height,
    } = computeExtents(camera, ground);
    const resolution = 65 / distance;
    const dimension = bbox.getSize(new Vector2());

    const vectors: {
      position: Vector3;
      direction: Vector3;
      count: number;
      dirWeight: number;
      disWeight: number;
    }[] = [];
    let maxValue = 0;

    // iterate over grid
    const step = 1 / resolution;
    for (let iy = 0, ly = dimension.y; iy < ly; iy += step) {
      for (let ix = 0, lx = dimension.x; ix < lx; ix += step) {
        // grid position with slight random offset
        const position = new Vector3(
          bbox.min.x + ix + ((Math.random() * step) / 2 - step / 4),
          height,
          bbox.min.y + iy + ((Math.random() * step) / 2 - step / 4),
        );

        // look for relevant objects in octree
        const results = octree.search(position, this.radius);
        const items: Object3D[] = [];

        // iterate over results and collect objects within radius
        results.forEach((result) => {
          if (!result.object.parent) return;
          if (result.object.parent.userData['cluster']) {
            const cluster = result.object.parent.userData['cluster'];
            const d = position.distanceTo(
              new Vector3(cluster.position.x, height, cluster.position.z),
            );
            if (d < this.radius + cluster.distance) {
              items.push(...cluster.getLeaves());
            }
          } else {
            items.push(result.object.parent);
          }
        });

        const direction = new Vector3();
        let count = 0;
        let disTmp = 0;
        let disMin = this.radius;

        // accumulate directions
        items.forEach((item) => {
          const d = position.distanceTo(
            new Vector3(item.position.x, height, item.position.z),
          );
          if (d < this.radius) {
            direction.add(
              new Vector3(0, 0, -1).applyQuaternion(item.quaternion),
            );
            count++;
            disTmp += d;
            disMin = Math.min(disMin, d);
          }
        });

        if (direction.x === 0 && direction.z === 0) continue;

        maxValue = Math.max(maxValue, count);

        vectors.push({
          position,
          direction: new Vector3(direction.x, 0, direction.z).normalize(),
          count,
          dirWeight: 1 - disTmp / this.radius / count,
          disWeight: 1 - disMin / this.radius,
        });
      }
    }

    // init new mesh
    const mesh = new InstancedMesh(
      this.geometry,
      this.material,
      vectors.length,
    );
    const matrix = new Matrix4();

    // set matrix and color for each instance
    vectors.forEach((vec, i) => {
      // set color
      const offset = Math.round((vec.count / maxValue) * 255) * 4;
      const color = new Color(
        `rgb(${this.palette[offset]},${this.palette[offset + 1]},${
          this.palette[offset + 2]
        })`,
      );

      mesh.setColorAt(i, color);

      // construct matrix
      const quaternion = new Quaternion();
      if (vec.direction.y > 0.9999) {
        quaternion.set(0, 0, 0, 1);
      } else if (vec.direction.y < -0.9999) {
        quaternion.set(1, 0, 0, 0);
      } else {
        quaternion.setFromAxisAngle(
          new Vector3(vec.direction.z, 0, -vec.direction.x).normalize(),
          Math.acos(vec.direction.y),
        );
      }

      const s = (1 / resolution) * vec.disWeight;
      const scale = new Vector3(s * 0.3, s * 0.7, s * 0.3);

      matrix.compose(vec.position, quaternion, scale);

      mesh.setMatrixAt(i, matrix);
    });

    this.mesh = mesh;
    this.add(this.mesh);

    console.log('VectorField - Elapsed time', Date.now() - startTime, 'ms');

    return {
      gradient: defaultGradientConfig,
      domain: [0, maxValue],
    };
  }

  dispose(): void {
    if (this.mesh) {
      this.remove(this.mesh);
      this.mesh.dispose();
    }
    this.geometry.dispose();
    this.material.dispose();
  }
}
