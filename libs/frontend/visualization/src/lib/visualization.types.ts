import { HeatMap } from './heat-map';
import { VectorField } from './vector-field';
import { WindMap } from './wind-map';
import { RadialFan } from './radial-fan';

export type VisualizationInstance = HeatMap | VectorField | WindMap | RadialFan;

export interface VisualizationGradientConfig {
  gradient: { [key: string]: string };
  domain: [number, number];
}
