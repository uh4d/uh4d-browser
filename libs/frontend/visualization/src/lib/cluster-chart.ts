import {
  Mesh,
  MeshBasicMaterial,
  Object3D,
  PlaneGeometry,
  Vector3,
} from 'three';
import { ClusterObject } from '@uh4d/three';
import { defaultGradientConfig, getColorPalette } from './helpers';

/**
 * Base class for visualizations based on {@link ClusterTree}/{@link ClusterObject}.
 */
export class ClusterChart extends Object3D {
  protected angleResolution = 16;

  protected palette: Uint8ClampedArray;

  protected charts: {
    cluster: ClusterObject;
    position: Vector3;
    normals: Vector3[];
    counted: number[];
    object?: Mesh<PlaneGeometry, MeshBasicMaterial>;
  }[] = [];

  constructor() {
    super();

    this.palette = getColorPalette(defaultGradientConfig);
  }

  /**
   * Set number of sample vectors. E.g., each of 16 sample vectors spans an angle of 22 degree.
   * @param resolution - Resolution (default: 16)
   */
  setAngleResolution(resolution = 16): void {
    this.angleResolution = resolution;
  }

  /**
   * Remove all charts and dispose their geometries and materials.
   */
  dispose(): void {
    for (const c of this.charts) {
      c.cluster.implode();

      const obj = c.object;
      if (obj) {
        this.remove(obj);
        obj.geometry.dispose();
        obj.material.map?.dispose();
        obj.material.dispose();
      }
    }

    this.charts.length = 0;
  }
}
