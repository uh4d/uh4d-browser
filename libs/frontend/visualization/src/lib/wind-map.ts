import {
  Camera,
  CanvasTexture,
  ClampToEdgeWrapping,
  LinearFilter,
  Mesh,
  MeshBasicMaterial,
  Object3D,
  PlaneGeometry,
  UVMapping,
  Vector2,
  Vector3,
} from 'three';
import { Octree } from '@brakebein/threeoctree';
import WindGL, { WindData } from '@brakebein/webgl-wind';
import { VisualizationGradientConfig } from './visualization.types';
import { computeExtents, defaultGradientConfig } from './helpers';

export class WindMap extends Mesh<PlaneGeometry, MeshBasicMaterial> {
  private canvas: HTMLCanvasElement;
  private wind: WindGL;

  radius: number;
  useWeight: 'countWeight';

  constructor() {
    const canvas = document.createElement('canvas');
    canvas.width = 500;
    canvas.height = 500;

    const geometry = new PlaneGeometry(1, 1);
    geometry.rotateX(-Math.PI / 2);

    const material = new MeshBasicMaterial({
      transparent: true,
      depthTest: false,
      depthWrite: false,
    });

    super(geometry, material);

    this.canvas = canvas;

    // init wind-gl
    const gl = canvas.getContext('webgl', { antialias: true, depth: false });
    if (!gl) throw new Error('Cannot get canvas webgl context');
    this.wind = new WindGL(gl);
    this.wind.numParticles = 65536;
    // this.wind.setColorRamp(defaultGradientConfig);

    this.radius = 30;
    this.useWeight = 'countWeight';
  }

  setRadius(radius: number): void {
    this.radius = radius;
  }

  setNumParticles(numParticles: number): void {
    this.wind.numParticles = numParticles;
  }

  setSpeedFactor(value: number): void {
    this.wind.speedFactor = value;
  }

  update(
    camera: Camera,
    ground: Object3D,
    octree: Octree,
  ): VisualizationGradientConfig {
    const startTime = Date.now();

    const {
      boundingBox: bbox,
      distance,
      height: altitude,
    } = computeExtents(camera, ground);
    const resolution = 200 / distance;
    const dimension = bbox.getSize(new Vector2());
    const { x: width, y: height } = dimension
      .clone()
      .multiplyScalar(resolution)
      .ceil();

    // resize canvas
    this.canvas.width = Math.min(
      2048,
      this.canvas.width === width * 5 ? width * 5 + 1 : width * 5,
    );
    this.canvas.height = Math.min(
      2048,
      this.canvas.height === height * 5 ? height * 5 + 1 : height * 5,
    );
    this.wind.resize();

    // set object position/scale
    this.scale.set(dimension.x, 1, dimension.y);
    this.position.set(
      (bbox.min.x + bbox.max.x) / 2,
      altitude,
      (bbox.min.y + bbox.max.y) / 2,
    );

    const vectors: {
      position: Vector3;
      direction: Vector3;
      count: number;
      dirWeight: number;
      disWeight: number;
      countWeight: number;
    }[] = [];
    let maxValue = 0;

    // iterate over pixel grid
    for (let iy = 0, ly = height; iy < ly; iy++) {
      for (let ix = 0, lx = width; ix < lx; ix++) {
        // grid world position
        const position = new Vector3(
          ix / resolution + bbox.min.x,
          altitude,
          iy / resolution + bbox.min.y,
        );

        // look for relevant objects in octree
        const results = octree.search(position, this.radius);
        const items: Object3D[] = [];

        // iterate over results and collect objects within radius
        results.forEach((result) => {
          if (!result.object.parent) return;
          if (result.object.parent.userData['cluster']) {
            const cluster = result.object.parent.userData['cluster'];
            const d = position.distanceTo(
              new Vector3(cluster.position.x, altitude, cluster.position.z),
            );
            if (d < this.radius + cluster.distance) {
              items.push(...cluster.getLeaves());
            }
          } else {
            items.push(result.object.parent);
          }
        });

        const direction = new Vector3();
        let count = 0;
        let disTmp = 0;
        let disMin = this.radius;

        // accumulate directions
        items.forEach((item) => {
          const d = position.distanceTo(
            new Vector3(item.position.x, altitude, item.position.z),
          );
          if (d < this.radius) {
            direction.add(
              new Vector3(0, 0, -1).applyQuaternion(item.quaternion),
            );
            count++;
            disTmp += d;
            disMin = Math.min(disMin, d);
          }
        });

        maxValue = Math.max(maxValue, count);

        vectors.push({
          position,
          direction: new Vector3(direction.x, 0, direction.z).normalize(),
          count,
          dirWeight: 1 - disTmp / this.radius / count,
          disWeight: 1 - disMin / this.radius,
          countWeight: 1,
        });
      }
    }

    // compute weight with respect to maxValue
    vectors.forEach((vec) => {
      vec.countWeight = vec.count / maxValue;
    });

    // write wind image - encode vectors as colors
    const vCanvas = document.createElement('canvas');
    vCanvas.width = width;
    vCanvas.height = height;
    const ctx = vCanvas.getContext('2d');
    if (!ctx) throw new Error('Cannot get canvas 2d context');
    const imageData = ctx.createImageData(width, height);
    const data = imageData.data;

    for (let i = 0, l = width * height; i < l; i++) {
      const i4 = i * 4;
      const vec = vectors[i];

      // tslint:disable:no-bitwise
      data[i4] = (vec.direction.x * vec[this.useWeight] * 127 + 128) >> 0;
      data[i4 + 1] = (-vec.direction.z * vec[this.useWeight] * 127 + 128) >> 0;
      data[i4 + 2] = 0;
      data[i4 + 3] = 255;
    }

    ctx.putImageData(imageData, 0, 0);

    // set wind data
    const windImage = new Image();
    windImage.src = vCanvas.toDataURL('image/png');

    const windData: WindData = {
      width,
      height,
      uMin: -10,
      uMax: 10,
      vMin: -10,
      vMax: 10,
      image: windImage,
    };

    windImage.onload = () => {
      this.wind.setWind(windData);
    };

    // update/replace texture
    this.material.map?.dispose();
    this.material.map = new CanvasTexture(
      this.canvas,
      UVMapping,
      ClampToEdgeWrapping,
      ClampToEdgeWrapping,
      LinearFilter,
      LinearFilter,
    );

    console.log('WindMap - Elapsed time', Date.now() - startTime, 'ms');

    return {
      gradient: defaultGradientConfig,
      domain: [0, maxValue],
    };
  }

  draw(): void {
    if (this.wind.windData) {
      this.wind.draw();
    }

    if (this.material.map) this.material.map.needsUpdate = true;
  }

  dispose(): void {
    this.geometry.dispose();
    this.material.map?.dispose();
    this.material.dispose();
  }
}
