import {
  Camera,
  CanvasTexture,
  ClampToEdgeWrapping,
  Frustum,
  LinearFilter,
  Matrix4,
  Mesh,
  MeshBasicMaterial,
  PlaneGeometry,
  RGBAFormat,
  UnsignedByteType,
  UVMapping,
  Vector2,
  Vector3,
} from 'three';
import { ClusterObject } from '@uh4d/three';
import { VisualizationGradientConfig } from './visualization.types';
import { ClusterChart } from './cluster-chart';
import { defaultGradientConfig } from './helpers';

export class RadialFan extends ClusterChart {
  protected angleOffset = 0;

  setAngleOffset(offset: number): void {
    this.angleOffset = offset;
  }

  /**
   * Generate charts for each cluster.
   */
  update(
    camera: Camera,
    clusters: ClusterObject[],
  ): VisualizationGradientConfig {
    const startTime = Date.now();

    this.dispose();

    // camera frustum
    const frustum = new Frustum().setFromProjectionMatrix(
      new Matrix4().multiplyMatrices(
        camera.projectionMatrix,
        camera.matrixWorldInverse,
      ),
    );

    // get data for each cluster
    let maxCount = 0;
    let minDistance = 1000;
    let maxScalarGlobal = 0;

    for (const c of clusters) {
      if (!frustum.containsPoint(c.position)) continue;

      const normals: Vector3[] = [];

      // collect normals of all image planes that are part of this cluster
      for (const leaf of c.getLeaves()) {
        normals.push(new Vector3(0, 0, -1).applyQuaternion(leaf.quaternion));
      }

      maxCount = Math.max(maxCount, normals.length);
      if (c.parent) {
        minDistance = Math.min(minDistance, c.parent.distance);
      }

      this.charts.push({
        cluster: c,
        position: c.position,
        normals,
        counted: [],
      });

      c.explode();
    }

    // parameters
    const canvasWidth = 200;
    const angleSteps = this.angleResolution;
    const piAngle = (2 * Math.PI) / angleSteps;

    const maxArea = (Math.pow(canvasWidth / 2 - 16, 2) * piAngle) / Math.PI;

    // initialize chart basic normals
    const normVectors: Vector2[] = [];
    for (let i = 0; i < angleSteps; i++) {
      normVectors.push(
        new Vector2(0, 1).rotateAround(
          new Vector2(0, 0),
          -i * piAngle + this.angleOffset,
        ),
      );
    }

    // compute each chart
    // iterate once to determine number of images for each chunk
    // and get the global maximum scalar value first
    for (const c of this.charts) {
      // init chunk accumulators
      const acc = normVectors.map(() => 0);

      // for each normal, determine into which chunks it falls
      for (const vec3 of c.normals) {
        // get angle
        const vec2 = new Vector2(vec3.x, vec3.z).normalize();
        let angle = Math.acos(vec2.y);
        if (vec2.x < 0) {
          angle = -angle + 2 * Math.PI;
        }

        // each image counts as 1 and will split into, e.g., 0.6 and 0.4, according to angle
        // respective (neighboring) chunks will be incremented by 0.6 and 0.4
        const temp = (angle + this.angleOffset) / piAngle;
        const index = Math.floor(temp);
        const t = temp - index;

        acc[index % angleSteps] += 1.0 - t;
        acc[(index + 1) % angleSteps] += t;
      }

      let maxScalar = 0;

      // slightly blur accumulated values
      // gaussian blur [1 4 6 4 1]
      c.counted = acc.map((value, index, array) => {
        const blur =
          (acc[(index + 2) % array.length] +
            acc[(index + 1) % array.length] * 4 +
            value * 6 +
            acc[(index - 1 + array.length) % array.length] * 4 +
            acc[(index - 2 + array.length) % array.length]) /
          16;

        maxScalar = Math.max(blur, maxScalar);

        return blur;
      });

      maxScalarGlobal = Math.max(maxScalarGlobal, maxScalar);
    }

    // finally, create each chart from counted images
    for (const c of this.charts) {
      // create canvas material and geometry
      const planeWidth = Math.round(minDistance * 1.2);
      const geometry = new PlaneGeometry(planeWidth, planeWidth);
      geometry.rotateX(-Math.PI / 2);

      const canvas = document.createElement('canvas');
      canvas.setAttribute('width', canvasWidth.toString());
      canvas.setAttribute('height', canvasWidth.toString());

      const material = new MeshBasicMaterial({
        map: new CanvasTexture(
          canvas,
          UVMapping,
          ClampToEdgeWrapping,
          ClampToEdgeWrapping,
          LinearFilter,
          LinearFilter,
          RGBAFormat,
          UnsignedByteType,
          4,
        ),
        transparent: true,
        depthTest: false,
        depthWrite: false,
      });

      const mesh = new Mesh(geometry, material);
      mesh.position.copy(c.position);
      mesh.renderOrder = 50;
      this.add(mesh);
      c.object = mesh;

      // draw chart
      const origin = new Vector2(canvasWidth / 2, canvasWidth / 2);
      const sampleVectors = c.counted.map((value, index) =>
        normVectors[index]
          .clone()
          .setLength(
            Math.sqrt(
              ((value / maxScalarGlobal) * maxArea * Math.PI) / piAngle,
            ),
          ),
      );

      const ctx = canvas.getContext('2d');
      if (!ctx) throw new Error('Cannot get canvas 2d context');
      ctx.translate(origin.x, origin.y);

      for (const [index, vec] of sampleVectors.entries()) {
        const angle = vec.angle();
        // const radius = Math.sqrt(vec.length());
        const radius = vec.length();
        const offset =
          Math.round((c.counted[index] / maxScalarGlobal) * 255) * 4;
        const color = `rgba(${this.palette[offset]}, ${
          this.palette[offset + 1]
        }, ${this.palette[offset + 2]}, 0.7)`;

        // draw fan blade
        ctx.beginPath();
        ctx.lineWidth = 1.0;
        ctx.fillStyle = color;

        const p1 = new Vector2(radius, 0).rotateAround(
          new Vector2(),
          angle - piAngle / 2,
        );
        const p2 = new Vector2(radius, 0).rotateAround(
          new Vector2(),
          angle + piAngle / 2,
        );
        const tip = new Vector2(radius + p1.distanceTo(p2) / 2, 0).rotateAround(
          new Vector2(),
          angle,
        );

        ctx.moveTo(0, 0);
        ctx.lineTo(p1.x, p1.y);
        ctx.lineTo(tip.x, tip.y);
        ctx.lineTo(p2.x, p2.y);
        ctx.closePath();
        ctx.stroke();
        ctx.fill();
      }
    }

    console.log('RadialFan - Elapsed time', Date.now() - startTime, 'ms');

    return {
      gradient: defaultGradientConfig,
      domain: [0, maxCount],
    };
  }
}
