import {
  Box2,
  BufferGeometry,
  Camera,
  CanvasTexture,
  ClampToEdgeWrapping,
  LinearFilter,
  Mesh,
  MeshBasicMaterial,
  Object3D,
  PlaneGeometry,
  UVMapping,
  Vector2,
} from 'three';
import { create, DataPoint, Heatmap } from '@brakebein/heatmap.js';
import { VisualizationGradientConfig } from './visualization.types';
import { computeExtents, defaultGradientConfig } from './helpers';

export class HeatMap extends Mesh<BufferGeometry, MeshBasicMaterial> {
  private heatmap: Heatmap<'value', 'x', 'y'>;

  radius: number;

  constructor(container0?: HTMLElement, canvas0?: HTMLCanvasElement) {
    const container = container0 || document.createElement('div');
    const canvas = canvas0 || document.createElement('canvas');
    canvas.width = 100;
    canvas.height = 100;

    // initial geometry
    const geometry = new PlaneGeometry(1, 1);
    geometry.rotateX(-Math.PI / 2);

    // material with CanvasTexture (texture gets created in `update()`)
    const material = new MeshBasicMaterial({
      transparent: true,
      depthTest: false,
      depthWrite: false,
    });

    super(geometry, material);

    this.radius = 30;
    this.renderOrder = 500;

    // init heat map
    this.heatmap = create({
      container,
      canvas,
      blur: 0.95,
      radius: 1.5,
      gradient: defaultGradientConfig,
    });
  }

  setRadius(radius: number): void {
    this.radius = radius;
  }

  update(
    camera: Camera,
    ground: Object3D,
    items: Object3D[],
  ): VisualizationGradientConfig {
    const startTime = Date.now();

    const {
      boundingBox: bbox,
      distance,
      height,
    } = computeExtents(camera, ground);
    const resolution = 100 / distance;

    // set heat map dimensions
    const dimensionWorld = bbox.getSize(new Vector2());
    const dimensionPixel = dimensionWorld
      .clone()
      .multiplyScalar(resolution)
      .ceil();
    this.heatmap._renderer.setDimensions(dimensionPixel.x, dimensionPixel.y);

    // set object position/scale
    this.scale.set(dimensionWorld.x, 1, dimensionWorld.y);
    this.position.set(
      (bbox.min.x + bbox.max.x) / 2,
      height,
      (bbox.min.y + bbox.max.y) / 2,
    );

    // initiate matrix with zero values
    const matrix: number[][] = Array(dimensionPixel.y)
      .fill(Array(dimensionPixel.x).fill(0))
      .map((a) => a.slice());

    const radius = Math.round(this.radius * resolution);
    const indexBbox = new Box2(
      new Vector2(),
      new Vector2(dimensionPixel.x - 1, dimensionPixel.y - 1),
    );

    // iterate over items and increment values within radius
    items.forEach((item) => {
      // skip if item is outside of view
      if (!bbox.containsPoint(new Vector2(item.position.x, item.position.z)))
        return;

      const offsetX = Math.round((item.position.x - bbox.min.x) * resolution);
      const offsetY = Math.round((item.position.z - bbox.min.y) * resolution);

      for (let y = -radius; y < radius; y++) {
        const width = Math.round(Math.sqrt(radius * radius - y * y));
        for (let x = -width; x < width; x++) {
          const ix = offsetX + x;
          const iy = offsetY + y;
          if (indexBbox.containsPoint(new Vector2(ix, iy))) {
            matrix[iy][ix]++;
          }
        }
      }
    });

    // pass values to h337 conform structure
    const points: DataPoint[] = [];
    let maxValue = 1;

    for (let iy = 0, ly = matrix.length; iy < ly; iy++) {
      for (let ix = 0, lx = matrix[iy].length; ix < lx; ix++) {
        const value = matrix[iy][ix];
        if (value > 0) {
          points.push({
            x: ix,
            y: iy,
            value,
          });
          maxValue = Math.max(maxValue, value);
        }
      }
    }

    // update heat map
    this.heatmap.setData({
      min: 1,
      max: maxValue,
      data: points,
    });

    // update/replace texture
    this.material.map?.dispose();
    this.material.map = new CanvasTexture(
      this.heatmap._config.canvas!,
      UVMapping,
      ClampToEdgeWrapping,
      ClampToEdgeWrapping,
      LinearFilter,
      LinearFilter,
    );

    console.log('HeatMap - Elapsed time', Date.now() - startTime, 'ms');

    return {
      gradient: this.heatmap._config.gradient!,
      domain: [0, maxValue],
    };
  }

  dispose(): void {
    this.geometry.dispose();
    this.material.map?.dispose();
    this.material.dispose();
  }
}
