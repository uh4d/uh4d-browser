import { Injectable } from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ConfirmModalComponent } from './confirm-modal.component';

@Injectable({
  providedIn: 'root',
})
export class ConfirmModalService {
  constructor(private modalService: BsModalService) {}

  confirm(options?: {
    message?: string;
    confirmLabel?: string;
    declineLabel?: string;
  }): Promise<boolean> {
    const modalRef = this.modalService.show(ConfirmModalComponent, {
      id: 9999,
      class: 'modal-sm modal-dialog-centered',
      initialState: options,
    });

    return lastValueFrom(modalRef.content!.result);
  }
}
