import { Component, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';

@Component({
  standalone: true,
  imports: [Uh4dIconsModule],
  selector: 'uh4d-confirm-modal',
  templateUrl: './confirm-modal.component.html',
})
export class ConfirmModalComponent implements OnDestroy {
  message = 'Continue?';
  confirmLabel = 'Yes';
  declineLabel = 'No';

  result = new Subject<boolean>();

  constructor(private readonly modalRef: BsModalRef) {}

  ngOnDestroy() {
    if (!this.result.closed) {
      this.result.next(false);
      this.result.complete();
    }
  }

  confirm(): void {
    this.result.next(true);
    this.result.complete();
    this.modalRef.hide();
  }

  decline(): void {
    this.result.next(false);
    this.result.complete();
    this.modalRef.hide();
  }
}
