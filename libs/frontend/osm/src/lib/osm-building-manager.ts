import { Group, LineSegments, Material, Mesh, Object3D } from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { OsmGeneratedDto } from '@uh4d/dto/interfaces/custom/osm';
import { Cache } from '@uh4d/frontend/viewport3d-cache';
import { CoordinatesConverter } from '@uh4d/utils';

export class OsmBuildingManager {
  group = new Group();
  buildings: Mesh[] = [];

  private baseUrl = '/api/osm/';

  constructor(
    private readonly converter: CoordinatesConverter,
    private readonly gltfLoader: GLTFLoader,
  ) {}

  async load(data: OsmGeneratedDto): Promise<void> {
    this.dispose();

    const gltf = await this.gltfLoader.loadAsync(
      this.baseUrl + data.cachedFile,
    );
    const model = gltf.scene.children[0];
    this.group.position.copy(this.converter.fromLatLon(data.location));

    model.traverse((child) => {
      if (child instanceof Mesh && child.name) {
        child.material = Cache.materials.get('defaultMat');
        child.userData['osmId'] = parseInt(child.name);
        this.buildings.push(child);
      } else if (child instanceof LineSegments) {
        child.material = Cache.materials.get('edgesMat');
      }
    });
  }

  update(objects: Object3D[]): void {
    this.group.remove(...this.buildings);

    for (const building of this.buildings) {
      // make osm buildings only visible if there is no other 3D object
      if (
        objects.find((obj) =>
          obj.userData['source'].osm_overlap.includes(
            building.userData['osmId'],
          ),
        )
      ) {
        continue;
      }

      this.group.add(building);
    }
  }

  applyShading(material?: Material) {
    const mat = Cache.materials.get('defaultMat')!;
    this.buildings.forEach((building) => {
      building.material = material || mat;
    });
  }

  dispose(): void {
    for (const obj of this.buildings) {
      this.group.remove(obj);
      obj.traverse((child) => {
        if (child instanceof Mesh || child instanceof LineSegments) {
          child.geometry.dispose();
        }
      });
    }

    this.buildings = [];
  }
}
