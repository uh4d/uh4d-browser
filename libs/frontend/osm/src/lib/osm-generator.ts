import {
  Box2,
  BufferGeometry,
  EdgesGeometry,
  ExtrudeGeometry,
  Group,
  LineSegments,
  Mesh,
  Object3D,
  Path,
  Raycaster,
  Shape,
  Vector2,
  Vector3,
} from 'three';
import * as BufferGeometryUtils from 'three-addons/utils/BufferGeometryUtils.js';
import { Cache } from '@uh4d/frontend/viewport3d-cache';
import { CoordinatesConverter } from '@uh4d/utils';
import { getOSMBuildingHeight } from './osm-building-heights';

interface INode {
  type: 'node';
  id: number;
  lat: number;
  lon: number;
}

type ITags = { [key: string]: string };

interface IMember {
  type: string;
  ref: number;
  role: 'outer' | 'inner' | 'part';
}

interface IWay {
  type: 'way';
  id: number;
  nodes: number[];
  tags?: ITags;
}

interface IRelation {
  type: 'relation';
  id: number;
  members: IMember[];
  tags: ITags;
}

interface IOsmData {
  elements: (INode | IWay | IRelation)[];
}

/**
 * Class to generate and organize 3D objects retrieved from OpenStreetMap (via Overpass API).
 */
export class OSMGenerator {
  private coordsConverter: CoordinatesConverter;

  private osmData!: IOsmData;
  private nodes: INode[] = [];
  private ways: IWay[] = [];
  private relations: IRelation[] = [];

  group = new Group();
  buildings: Object3D[] = [];

  constructor(converterService: CoordinatesConverter) {
    this.coordsConverter = converterService;
  }

  /**
   * Generate buildings from data from Overpass API.
   */
  async generate(data: IOsmData): Promise<void> {
    if (this.osmData === data) {
      return;
    }

    this.dispose();

    // split different types of elements
    this.nodes = data.elements.filter((e): e is INode => e.type === 'node');
    this.ways = data.elements.filter((e): e is IWay => e.type === 'way');
    this.relations = data.elements.filter(
      (e): e is IRelation => e.type === 'relation',
    );

    const buildings: Object3D[] = [];
    const processedIds: number[] = [];

    // process relations
    for (const rel of this.relations) {
      const existingBuilding = this.buildings.find(
        (obj) => obj.userData['osmId'] === rel.id,
      );
      if (existingBuilding) {
        buildings.push(existingBuilding);
        continue;
      }

      rel.members.sort((a, b) => {
        if (a.role === 'outer') {
          return -1;
        }
        if (a.role === 'inner') {
          return 1;
        }
        return 0;
      });

      const parts: { shape: Shape; tags?: ITags }[] = [];
      for (const m of rel.members) {
        const way = this.ways.find((w) => w.id === m.ref);
        if (way) {
          switch (m.role) {
            case 'outer':
            case 'part':
              parts.push({
                shape: this.getShape(way),
                tags: way.tags || rel.tags,
              });
              break;
            case 'inner':
              parts[parts.length - 1].shape.holes.push(this.getPath(way));
          }
          processedIds.push(way.id);
        }
      }

      const group = this.generateBuilding(parts);
      group.userData['osmId'] = rel.id;
      buildings.push(group);
    }

    // process ways
    for (const way of this.ways) {
      if (processedIds.includes(way.id) || !way.tags) {
        continue;
      }

      const existingBuilding = this.buildings.find(
        (obj) => obj.userData['osmId'] === way.id,
      );
      if (existingBuilding) {
        buildings.push(existingBuilding);
        continue;
      }

      // const shape = this.processWay(way, 'part');
      const shape = this.getShape(way);

      const group = this.generateBuilding([{ shape, tags: way.tags }]);
      group.userData['osmId'] = way.id;
      buildings.push(group);
    }

    // dispose obsolete buildings
    const obsoleteBuildings = this.buildings.filter(
      (obj) => !buildings.includes(obj),
    );
    for (const obj of obsoleteBuildings) {
      this.group.remove(obj);
      obj.traverse((child) => {
        if (child instanceof Mesh || child instanceof LineSegments) {
          child.geometry.dispose();
        }
      });
    }

    this.buildings = buildings;
  }

  /**
   * Build shape from way element's nodes.
   * @private
   */
  private processWay(way: IWay, role: 'outer' | 'part'): Shape;
  private processWay(way: IWay, role: 'inner'): Path;
  private processWay(
    way: IWay,
    role: 'inner' | 'outer' | 'part',
  ): Path | Shape | undefined;
  private processWay(
    way: IWay,
    role: 'inner' | 'outer' | 'part',
  ): Path | Shape | undefined {
    const points: Vector2[] = [];

    for (const nodeId of way.nodes) {
      const node = this.nodes.find((n) => n.id === nodeId);
      if (node) {
        const p = this.coordsConverter.fromLatLon(node.lat, node.lon, 0);
        points.push(new Vector2(p.x, -p.z));
      }
    }

    switch (role) {
      case 'outer':
        return new Shape(points);
      case 'inner':
        return new Path(points);
      case 'part': {
        return new Shape(points);
      }
    }
  }

  private getShape(way: IWay) {
    return new Shape(this.getVertices(way.nodes));
  }

  private getPath(way: IWay) {
    const vertices = this.getVertices(way.nodes);
    if (this.polygonArea(vertices) < 0) vertices.reverse();
    return new Path(vertices);
  }

  private polygonArea(vertices: Vector2[]) {
    let area = 0;
    for (let i = 0; i < vertices.length; i++) {
      const j = (i + 1) % vertices.length;
      area += vertices[i].x * vertices[j].y;
      area -= vertices[j].x * vertices[i].y;
    }
    return area / 2;
  }

  private getVertices(nodes: number[]): Vector2[] {
    const points: Vector2[] = [];

    for (const nodeId of nodes) {
      const node = this.nodes.find((n) => n.id === nodeId);
      if (node) {
        const p = this.coordsConverter.fromLatLon(node.lat, node.lon, 0);
        points.push(new Vector2(p.x, -p.z));
      }
    }

    return points;
  }

  /**
   * Extrude 3D geometry from shape and generate edges.
   * @private
   */
  private generateBuilding(parts: { shape: Shape; tags?: ITags }[]): Object3D {
    // const shapes = Array.isArray(shape) ? shape : [shape];
    const bbox = new Box2();
    const extrudeGeos: BufferGeometry[] = [];
    const edgesGeos: BufferGeometry[] = [];

    parts.forEach(({ shape, tags }) => {
      shape.getPoints().forEach((p) => {
        bbox.expandByPoint(p);
      });

      const geo = new ExtrudeGeometry(shape, {
        depth: getOSMBuildingHeight(bbox, tags),
        bevelEnabled: false,
      });
      geo.rotateX(-Math.PI / 2);
      extrudeGeos.push(geo);
      edgesGeos.push(new EdgesGeometry(geo, 24));
    });

    const mesh = new Mesh(
      BufferGeometryUtils.mergeGeometries(extrudeGeos),
      Cache.materials.get('defaultMat'),
    );
    const edges = new LineSegments(
      BufferGeometryUtils.mergeGeometries(edgesGeos),
      Cache.materials.get('edgesMat'),
    );

    const group = new Group();
    group.add(mesh, edges);
    group.userData['points'] = parts.map(({ shape }) => shape.getPoints());
    group.userData['boundingBox'] = bbox;
    group.userData['tags'] = parts
      .map(({ tags }) => tags)
      .filter((tags) => !!tags);

    return group;
  }

  /**
   * Update visibility and position according to terrain and existing objects.
   */
  update(
    terrain: Object3D,
    objects: Object3D[],
    altitudes?: { [osmId: string]: number },
  ): void {
    this.group.remove(...this.buildings);

    const raycaster = new Raycaster();
    const dir = new Vector3(0, -1, 0);

    const startTime = Date.now();

    for (const building of this.buildings) {
      // make osm buildings only visible if there is no other 3D object
      if (
        objects.find((obj) =>
          obj.userData['source'].osm_overlap.includes(
            building.userData['osmId'],
          ),
        )
      ) {
        continue;
      }

      let height = Number.MAX_VALUE;

      if (altitudes && altitudes[building.userData['osmId']]) {
        height = altitudes[building.userData['osmId']];
      } else {
        const bbox: Box2 = building.userData['boundingBox'];
        const points = [
          bbox.min,
          new Vector2(bbox.min.x, bbox.max.y),
          bbox.max,
          new Vector2(bbox.max.x, bbox.min.y),
        ];

        for (const point of points) {
          raycaster.set(new Vector3(point.x, 10000, -point.y), dir);
          const intersections = raycaster.intersectObject(terrain, true);

          if (intersections[0]) {
            height = Math.min(height, intersections[0].point.y);
          }
        }
      }

      building.position.setY(height === Number.MAX_VALUE ? 0 : height);

      this.group.add(building);
    }

    console.log(
      'OSM Generator - Elapsed time: ',
      (Date.now() - startTime) / 1000,
    );
  }

  /**
   * Dispose all generated objects.
   */
  dispose(): void {
    for (const obj of this.buildings) {
      this.group.remove(obj);
      obj.traverse((child) => {
        if (child instanceof Mesh || child instanceof LineSegments) {
          child.geometry.dispose();
        }
      });
    }
  }
}
