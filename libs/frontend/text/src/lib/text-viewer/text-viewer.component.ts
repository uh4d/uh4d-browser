import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { CommonModule, Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { FormsModule } from '@angular/forms';
import {
  iif,
  map,
  ReplaySubject,
  Subject,
  takeUntil,
  withLatestFrom,
} from 'rxjs';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { PdfJsViewerComponent, PdfJsViewerModule } from 'ng2-pdfjs-viewer';
import { TextFileDto } from '@uh4d/dto/interfaces/text-file';
import { TextAnnotationDto } from '@uh4d/dto/interfaces/text-annotation';
import { SimilarAnnotationsRecord } from '@uh4d/dto/interfaces/custom/annotation';
import { TextAnnotationService } from '../text-annotation.service';
import {
  AnnotationData,
  TextViewerAnnotationComponent,
} from '../text-viewer-annotation/text-viewer-annotation.component';

@Component({
  selector: 'uh4d-text-viewer',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ButtonsModule,
    PdfJsViewerModule,
    TextViewerAnnotationComponent,
  ],
  templateUrl: './text-viewer.component.html',
  styleUrls: ['./text-viewer.component.sass'],
})
export class TextViewerComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();
  private textFile$ = new ReplaySubject<TextFileDto>(1);
  private annotations$ = new ReplaySubject<TextAnnotationDto[]>(1);

  @Input({ required: true }) set file(file: TextFileDto) {
    this.textFile$.next(file);
  }

  @Input() set annotations(value: TextAnnotationDto[]) {
    this.annotations$.next(value);
  }

  @Input() showAnnotations = false;

  @Output() annotationQueried = new EventEmitter<TextAnnotationDto>();

  @ViewChild('pdfViewer', { static: false }) pdfViewer?: PdfJsViewerComponent;

  textFileValue?: TextFileDto;
  annotatedText: AnnotationData[] = [];

  activeViewer: 'plain' | 'pdf' = 'plain';

  plainTextAvailable = false;
  pdfAvailable = false;
  annotationsAvailable = false;

  constructor(
    private readonly textAnnotationService: TextAnnotationService,
    private readonly route: ActivatedRoute,
    private readonly location: Location,
  ) {}

  ngOnInit(): void {
    iif(
      () => this.showAnnotations,
      this.annotations$.pipe(withLatestFrom(this.textFile$)),
      this.textFile$.pipe(
        map<TextFileDto, [TextAnnotationDto[], TextFileDto]>((value) => [
          [],
          value,
        ]),
      ),
    )
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(([annotations, file]) => {
        this.textFileValue = file;
        this.annotatedText = [];
        this.pdfAvailable =
          this.plainTextAvailable =
          this.annotationsAvailable =
            false;

        if (file.pdf) {
          this.pdfAvailable = true;
        }
        if (file.txt) {
          this.plainTextAvailable = true;
          this.annotationsAvailable = !!annotations.length;
          this.renderText(file, annotations);
        }
        if (this.activeViewer === 'pdf') {
          if (this.pdfAvailable)
            setTimeout(() => {
              this.pdfViewer?.refresh();
            });
          else this.activeViewer = 'plain';
        }
        if (!this.plainTextAvailable && this.pdfAvailable) {
          this.activeViewer = 'pdf';
        }
      });
  }

  /**
   * Load and prepare plain text with annotations.
   */
  private async renderText(
    file: TextFileDto,
    annotations: TextAnnotationDto[],
  ) {
    // load text
    const response = await fetch('/data/' + file.path + file.txt);
    const text = await response.text();

    // get weighted annotations from navigation history
    type StateAnnotationRecords = {
      annotations?: Record<string, SimilarAnnotationsRecord['annotations']>;
    };
    const weightedAnnotations = (
      this.location.getState() as StateAnnotationRecords
    ).annotations;

    // compose annotation data
    this.annotatedText = [];
    let textIndex = 0;

    annotations.forEach((ann) => {
      if (textIndex < ann.start) {
        this.annotatedText.push({
          text: text.slice(textIndex, ann.start),
          start: textIndex,
          end: ann.start,
        });
      }

      const weight = weightedAnnotations
        ? Object.values(weightedAnnotations).reduce((acc, curr) => {
            const wa = curr.find((a) => a.id === ann.id);
            return Math.max(acc, wa ? wa.similarity : 0);
          }, 0)
        : undefined;

      this.annotatedText.push({
        text: text.slice(ann.start, ann.end),
        start: ann.start,
        end: ann.end,
        data: ann,
        weight,
      });
      textIndex = ann.end;
    });

    if (textIndex < text.length) {
      this.annotatedText.push({
        text: text.slice(textIndex),
        start: textIndex,
        end: text.length,
      });
    }

    await new Promise<void>((resolve) => setTimeout(resolve, 500));

    // highlight annotation from fragment
    const fragment = this.route.snapshot.fragment;
    if (fragment) {
      const ann = annotations.find((d) => d.id === fragment);
      if (ann) {
        this.textAnnotationService.selectedAnnotation$.next(ann);
        document
          .getElementById(ann.id)
          ?.scrollIntoView({ behavior: 'smooth', block: 'center' });
      }
    }
  }

  backgroundClick(): void {
    this.textAnnotationService.selectedAnnotation$.next(null);
  }

  ngOnDestroy(): void {
    this.textFile$.complete();
    this.annotations$.complete();
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
