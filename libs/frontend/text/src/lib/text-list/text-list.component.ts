import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { lastValueFrom, Subject, takeUntil } from 'rxjs';
import { GlobalEventsService } from '@uh4d/frontend/services';
import { TextsApiService } from '@uh4d/frontend/api-service';
import { TextDto } from '@uh4d/dto/interfaces';
import { RecordMediaItemComponent } from '@uh4d/frontend/ui-components';

@Component({
  selector: 'uh4d-text-list',
  standalone: true,
  imports: [CommonModule, RecordMediaItemComponent],
  templateUrl: './text-list.component.html',
  styleUrls: ['./text-list.component.sass'],
})
export class TextListComponent implements OnInit, OnDestroy {
  /**
   * @ignore
   */
  private unsubscribe$ = new Subject<void>();

  @Input() objectId?: string;

  texts: TextDto[] = [];

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly textsApiService: TextsApiService,
    private readonly events: GlobalEventsService,
  ) {}

  ngOnInit(): void {
    this.loadAssociatedTexts();

    this.events.textUpdate$.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
      this.loadAssociatedTexts();
    });
  }

  /**
   * Load texts associated filtered by object ID.
   */
  private async loadAssociatedTexts(): Promise<void> {
    this.texts = await lastValueFrom(
      this.textsApiService.query(
        this.objectId ? { q: [`obj:${this.objectId}`] } : {},
      ),
    );
  }

  /**
   * Open text details sheet.
   */
  openText(textId: string): void {
    this.router.navigate(
      ['/explore', this.route.snapshot.paramMap.get('scene'), 'text', textId],
      {
        queryParamsHandling: 'preserve',
      },
    );
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
