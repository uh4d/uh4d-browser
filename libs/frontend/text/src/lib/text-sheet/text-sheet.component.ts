import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { combineLatest, finalize, Subject, takeUntil, tap } from 'rxjs';
import { CdkAccordionModule } from '@angular/cdk/accordion';
import { NgxPermissionsModule } from 'ngx-permissions';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import {
  AnnotationCloudComponent,
  RecordCardItemComponent,
  RecordValidationBoxComponent,
  Uh4dAccordionModule,
} from '@uh4d/frontend/ui-components';
import { TextAnnotationDto, TextDto } from '@uh4d/dto/interfaces';
import {
  SimilarAnnotationsDto,
  SimilarRecord,
} from '@uh4d/dto/interfaces/custom/annotation';
import { TextsApiService } from '@uh4d/frontend/api-service';
import { AuthService } from '@uh4d/frontend/auth';
import { ConfirmModalService } from '@uh4d/frontend/confirm-modal';
import { SearchManagerService } from '@uh4d/frontend/services';
import { TextMetadataComponent } from '../text-metadata/text-metadata.component';
import { TextViewerComponent } from '../text-viewer/text-viewer.component';
import { TextDataService } from '../text-data.service';

@Component({
  selector: 'uh4d-text-sheet',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    NgxPermissionsModule,
    AnnotationCloudComponent,
    CdkAccordionModule,
    Uh4dIconsModule,
    RecordCardItemComponent,
    RouterLink,
    Uh4dAccordionModule,
    TextViewerComponent,
    TextMetadataComponent,
    RecordValidationBoxComponent,
  ],
  templateUrl: './text-sheet.component.html',
  styleUrls: ['./text-sheet.component.sass'],
})
export class TextSheetComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();

  text!: TextDto;

  annotations: TextAnnotationDto[] = [];

  canEditText = false;
  canValidate = false;

  expertMode = false;

  isUpdating = false;

  similarRecords: SimilarAnnotationsDto | null = null;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly authService: AuthService,
    private readonly textsApiService: TextsApiService,
    private readonly searchManager: SearchManagerService,
    private readonly confirmService: ConfirmModalService,
    private readonly textDataService: TextDataService,
  ) {}

  ngOnInit(): void {
    combineLatest({
      // get text data from resolver
      data: this.route.data.pipe(
        tap((data) => {
          this.text = data['text'];
          console.log(this.text);
          this.similarRecords = null;
          this.loadAnnotations();
        }),
      ),
      user: this.authService.user$,
    })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(({ user }) => {
        this.canEditText =
          !!user &&
          user.canEditItem(this.text.objects[0].location, this.text.uploadedBy);
        this.canValidate =
          !!user && user?.canEditItem(this.text.objects[0].location);
      });
  }

  private loadAnnotations() {
    this.textsApiService
      .queryAnnotations(this.text.id)
      .subscribe((annotations) => {
        this.annotations = annotations;
      });
  }

  searchAnnotation(annotation: TextAnnotationDto) {
    this.searchManager.setSearchParam(['ann:' + annotation.id], true, false);
  }

  validateText(accept: boolean) {
    this.isUpdating = true;
    this.textsApiService
      .updateValidationStatus(this.text.id, false, !accept)
      .pipe(
        finalize(() => {
          this.isUpdating = false;
        }),
      )
      .subscribe((result) => {
        this.text.pending = result.pending;
        this.text.declined = result.declined;
        this.textDataService.updated(this.text);
      });
  }

  /**
   * Remove text from database.
   * The user has to confirm this operation.
   * After successful removal, return to 3D viewport.
   */
  async removeText(): Promise<void> {
    const confirmed = await this.confirmService.confirm({
      message:
        'This operation will remove the text from database and delete all corresponding files. Do you want to continue?',
    });

    if (!confirmed) return;

    this.textsApiService.remove(this.text.id).subscribe(() => {
      this.searchManager.queryTexts();
      this.router.navigate(['../..'], {
        relativeTo: this.route,
        queryParamsHandling: 'preserve',
      });
    });
  }

  openRecord(
    type: 'image' | 'text' | 'object',
    record: { id: string } | SimilarRecord,
  ) {
    this.router.navigate(
      ['/explore', this.route.snapshot.paramMap.get('scene'), type, record.id],
      {
        queryParamsHandling: 'preserve',
        fragment: (record as SimilarRecord).annotations?.[0].id,
      },
    );
  }

  /**
   * Toggle {@link #expertMode expertMode} when user presses <kbd>Shift</kbd> + <kbd>Alt</kbd> + <kbd>D</kbd>.
   */
  @HostListener('document:keyup.shift.alt.d')
  private toggleExpertMode(): void {
    this.expertMode = !this.expertMode;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
