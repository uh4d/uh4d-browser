import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NotificationsService } from 'angular2-notifications';
import { FileItem, FileUploader, FileUploadModule } from 'ng2-file-upload';
import {
  GlobalEventsService,
  SearchManagerService,
} from '@uh4d/frontend/services';
import { JwtTokenService } from '@uh4d/frontend/auth';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';

@Component({
  selector: 'uh4d-text-upload-modal',
  standalone: true,
  imports: [CommonModule, Uh4dIconsModule, FileUploadModule],
  templateUrl: './text-upload-modal.component.html',
  styleUrls: ['./text-upload-modal.component.sass'],
})
export class TextUploadModalComponent implements OnInit {
  // config uploader
  uploader: FileUploader = new FileUploader({
    url: 'api/texts',
    itemAlias: 'uploadText',
    queueLimit: 1,
    filters: [
      {
        name: 'customFileType',
        fn: (file) => {
          const ext = file.name?.split('.').pop();
          return ext ? ['pdf', 'zip'].includes(ext) : false;
        },
      },
    ],
    authToken: 'Bearer ' + this.jwtTokenService.getAccessToken(),
  });

  fileItem?: FileItem;

  hasFileOverDropZone = false;

  objectIds: string[] = [];

  constructor(
    private readonly modalRef: BsModalRef,
    private readonly notify: NotificationsService,
    private readonly events: GlobalEventsService,
    private readonly searchManager: SearchManagerService,
    private readonly jwtTokenService: JwtTokenService,
  ) {}

  ngOnInit(): void {
    // listen to uploader events

    this.uploader.onWhenAddingFileFailed = (fileItem, filter) => {
      switch (filter.name) {
        case 'queueLimit':
          this.uploader.clearQueue();
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          this.uploader.addToQueue([fileItem]);
          break;
        case 'fileType':
          this.notify.warn(
            `File format ${fileItem.type} not supported!`,
            'Only common images are supported.',
          );
          break;
        default:
          this.notify.error('Unknown error!', 'See console.');
      }

      console.warn('onWhenAddingFileFailed', fileItem, filter);
    };

    this.uploader.onAfterAddingFile = (fileItem) => {
      this.fileItem = fileItem;
    };

    this.uploader.onBuildItemForm = (fileItem, form: FormData) => {
      form.append('objects[0]', this.objectIds[0]);
    };

    this.uploader.onSuccessItem = (fileItem, response) => {
      this.notify.success('Upload successful!');
      this.events.textUpdate$.next(JSON.parse(response));
      this.close();
    };

    this.uploader.onErrorItem = (fileItem, response, status) => {
      this.notify.error('Error while uploading!');
      console.error('onErrorItem', fileItem, response, status);
    };

    this.uploader.onCompleteAll = () => {
      this.searchManager.queryTexts();
    };
  }

  /**
   * Start upload process.
   */
  upload(): void {
    this.uploader.uploadAll();
  }

  /**
   * Close modal.
   */
  close(): void {
    this.modalRef.hide();
  }
}
