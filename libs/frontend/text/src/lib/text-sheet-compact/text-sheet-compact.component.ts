import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Params, Router, RouterLink } from '@angular/router';
import { combineLatest, finalize, Subject, takeUntil, tap } from 'rxjs';
import { NgxPermissionsModule } from 'ngx-permissions';
import {
  MapInteractiveComponent,
  RecordValidationBoxComponent,
} from '@uh4d/frontend/ui-components';
import { AuthService } from '@uh4d/frontend/auth';
import { LocationDto, TextDto } from '@uh4d/dto/interfaces';
import { TextsApiService } from '@uh4d/frontend/api-service';
import { TextDataService } from '../text-data.service';
import { TextMetadataComponent } from '../text-metadata/text-metadata.component';
import { TextViewerComponent } from '../text-viewer/text-viewer.component';

@Component({
  selector: 'uh4d-text-sheet-compact',
  standalone: true,
  imports: [
    CommonModule,
    RouterLink,
    TextMetadataComponent,
    MapInteractiveComponent,
    NgxPermissionsModule,
    RecordValidationBoxComponent,
    TextViewerComponent,
  ],
  templateUrl: './text-sheet-compact.component.html',
  styleUrls: ['./text-sheet-compact.component.sass'],
})
export class TextSheetCompactComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();

  text!: TextDto;

  locations: LocationDto[] = [];

  canEditText = false;
  canValidate = false;

  isUpdating = false;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly authService: AuthService,
    private readonly textsApiService: TextsApiService,
    private readonly textDataService: TextDataService,
  ) {}

  ngOnInit() {
    combineLatest({
      // get text data from resolver
      data: this.route.data.pipe(
        tap((data) => {
          this.text = data['text'];
          this.locations = this.text.objects
            .map((obj) => obj.location)
            .filter((location): location is LocationDto => !!location);
          console.log(this.text);
        }),
      ),
      user: this.authService.user$,
    })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(({ user }) => {
        this.canEditText =
          !!user &&
          user.canEditItem(this.text.objects[0].location, this.text.uploadedBy);
        this.canValidate =
          !!user && user?.canEditItem(this.text.objects[0].location);
      });
  }

  validateText(accept: boolean) {
    this.isUpdating = true;
    this.textsApiService
      .updateValidationStatus(this.text.id, false, !accept)
      .pipe(
        finalize(() => {
          this.isUpdating = false;
        }),
      )
      .subscribe((result) => {
        this.text.pending = result.pending;
        this.text.declined = result.declined;
        this.textDataService.updated(this.text);
      });
  }

  openScene() {
    const queryParams: Params = {};
    if (this.text.pending) {
      queryParams['q'] = 'pending';
    } else if (this.text.declined) {
      queryParams['q'] = 'declined';
    }
    if (this.text.date?.from) {
      queryParams['from'] = this.text.date.from;
    }
    if (this.text.date?.to) {
      queryParams['to'] = this.text.date.to;
    }

    const location = this.locations[0];

    const url = this.router.serializeUrl(
      this.router.createUrlTree(
        ['/explore', `${location.latitude},${location.longitude}`],
        { queryParams },
      ),
    );

    window.open(url, '_blank');
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
