import { inject } from '@angular/core';
import { ResolveFn, Router } from '@angular/router';
import { catchError, EMPTY } from 'rxjs';
import { TextDto } from '@uh4d/dto/interfaces';
import { TextsApiService } from '@uh4d/frontend/api-service';

/**
 * When entering the route, query database for text entry with given `textId` parameter.
 * If no entry found, navigate back to parent page.
 */
export const textResolver: ResolveFn<TextDto> = (route) => {
  const router = inject(Router);
  const textId = route.paramMap.get('textId');

  if (!textId) {
    return EMPTY;
  }

  return inject(TextsApiService)
    .get(textId)
    .pipe(
      catchError(() => {
        let url = '';
        let parent = route.parent;
        while (parent) {
          if (parent.url[0]) {
            url = parent.url[0].path + '/' + url;
          }
          parent = parent.parent;
        }
        router.navigate([url], {
          queryParamsHandling: 'preserve',
        });
        return EMPTY;
      }),
    );
};
