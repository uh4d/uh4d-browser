import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { TextAnnotationDto } from '@uh4d/dto/interfaces';

@Injectable({
  providedIn: 'root',
})
export class TextAnnotationService {
  selectedAnnotation$ = new BehaviorSubject<TextAnnotationDto | null>(null);
  queryAnnotation$ = new Subject<TextAnnotationDto>();
}
