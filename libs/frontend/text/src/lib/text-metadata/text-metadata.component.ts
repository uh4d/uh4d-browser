import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { combineLatest, ReplaySubject, Subject, takeUntil } from 'rxjs';
import { TextDto } from '@uh4d/dto/interfaces';
import { AuthService } from '@uh4d/frontend/auth';
import {
  CitationEditComponent,
  RecordUserMetadataComponent,
} from '@uh4d/frontend/ui-components';
import { TextsApiService } from '@uh4d/frontend/api-service';
import { TextDataService } from '../text-data.service';

@Component({
  selector: 'uh4d-text-metadata',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    CitationEditComponent,
    RecordUserMetadataComponent,
  ],
  templateUrl: './text-metadata.component.html',
  styleUrls: ['./text-metadata.component.sass'],
})
export class TextMetadataComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();
  private text$ = new ReplaySubject<TextDto>(1);

  @Input({ required: true }) set text(value: TextDto) {
    this.text$.next(value);
  }

  record!: TextDto;

  canEditText = false;

  constructor(
    private readonly authService: AuthService,
    private readonly textsApiService: TextsApiService,
    private readonly textDataService: TextDataService,
  ) {}

  ngOnInit() {
    combineLatest({
      text: this.text$,
      user: this.authService.user$,
    })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(({ text, user }) => {
        this.record = text;
        this.canEditText =
          !!user && user.canEditItem(text.objects[0].location, text.uploadedBy);
      });
  }

  /**
   * Update text reference information.
   */
  updateText(): void {
    this.textsApiService
      .patch(this.text.id, { bibtex: this.text.bibtex })
      .subscribe((updatedText) => {
        Object.assign(this.text, updatedText);
        // signal image update to application
        this.textDataService.updated(this.record);
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
