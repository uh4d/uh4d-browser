import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { delay, fromEvent, of, Subject, takeUntil } from 'rxjs';
import { TooltipDirective, TooltipModule } from 'ngx-bootstrap/tooltip';
import { NgxPermissionsService } from 'ngx-permissions';
import * as d3 from 'd3';
import { NormDataNodeDto, TextAnnotationDto } from '@uh4d/dto/interfaces';
import { stringToColor } from '@uh4d/utils';
import { ContextMenuService } from '@uh4d/frontend/context-menu';
import { TextAnnotationService } from '../text-annotation.service';

export interface AnnotationData {
  text: string;
  start: number;
  end: number;
  data?: TextAnnotationDto;
  weight?: number;
}

interface DebugNeighborLine {
  lx: number;
  ly: number;
  tx: number;
  ty: number;
  distance: number;
  weight: number;
}

@Component({
  selector: 'uh4d-text-viewer-annotation',
  standalone: true,
  imports: [CommonModule, TooltipModule],
  templateUrl: './text-viewer-annotation.component.html',
  styleUrls: ['./text-viewer-annotation.component.sass'],
})
export class TextViewerAnnotationComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();

  @Input({ required: true }) data!: AnnotationData;

  @Input() disabled = false;

  @Output() annotationQueried = new EventEmitter<TextAnnotationDto>();

  color = '';
  activeTooltip = false;

  wikidata: NormDataNodeDto[] = [];
  aat: NormDataNodeDto[] = [];

  @ViewChild('tooltip') tooltip!: TooltipDirective;

  @ViewChild('wrapper', { static: true }) wrapper!: ElementRef<HTMLSpanElement>;
  @ViewChild('svgElement', { static: true })
  svgElement!: ElementRef<SVGElement>;

  lines: DebugNeighborLine[] = [];

  constructor(
    private readonly elRef: ElementRef<HTMLElement>,
    private readonly textAnnotationService: TextAnnotationService,
    private readonly permissionsService: NgxPermissionsService,
    private readonly contextMenuService: ContextMenuService,
  ) {}

  ngOnInit(): void {
    if (this.data.data) {
      // prepare data
      const data = this.data.data;
      data.wikidata.sort((a, b) => (b.weight || 0) - (a.weight || 0));
      data.aat.sort((a, b) => (b.weight || 0) - (a.weight || 0));

      if (this.data.weight !== undefined) {
        const colorInterpolator = d3.interpolateRgb(
          'rgb(255, 255, 204)',
          'rgb(225, 30, 32)',
        );
        this.color = colorInterpolator(this.data.weight);
      } else {
        const identifierCode = data.wikidata[0]
          ? data.wikidata[0].identifier + data.wikidata[0].label
          : data.aat[0]
          ? data.aat[0].identifier + data.aat[0].label
          : '';
        this.color = stringToColor(identifierCode, true);
      }

      this.wikidata = data.wikidata;
      this.aat = data.aat;

      // register events
      if (this.permissionsService.getPermission('showDebug')) {
        const blur$ = fromEvent(this.wrapper.nativeElement, 'mouseleave').pipe(
          takeUntil(this.unsubscribe$),
        );
        blur$.subscribe(() => {
          this.onMouseLeave();
        });
        fromEvent(this.wrapper.nativeElement, 'mouseenter')
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(() => {
            of(1)
              .pipe(delay(500), takeUntil(blur$))
              .subscribe(() => {
                this.onMouseEnter();
              });
          });
      }

      fromEvent(this.wrapper.nativeElement, 'click').subscribe((event) => {
        event.stopPropagation();
        this.onClick();
      });
      fromEvent(this.wrapper.nativeElement, 'contextmenu').subscribe(
        (event) => {
          this.onRightClick(event as MouseEvent);
        },
      );

      this.textAnnotationService.selectedAnnotation$
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((selectedAnnotation) => {
          if (this.activeTooltip && this.data.data !== selectedAnnotation) {
            this.activeTooltip = false;
            this.tooltip.hide();
          } else if (
            !this.activeTooltip &&
            this.data.data === selectedAnnotation
          ) {
            this.activeTooltip = true;
            this.tooltip.show();
          }
        });
    }
  }

  private onClick() {
    // this.activeTooltip = !this.activeTooltip;
    // if (!this.activeTooltip && this.tooltip.isOpen) this.tooltip.hide();
    // if (this.activeTooltip && !this.tooltip.isOpen) this.tooltip.show();
    if (this.activeTooltip)
      this.textAnnotationService.selectedAnnotation$.next(null);
    else
      this.textAnnotationService.selectedAnnotation$.next(
        this.data.data || null,
      );
  }

  onRightClick(event: MouseEvent) {
    this.contextMenuService.open(event, {
      title: this.data.text,
      actions: [
        {
          icon: 'filter-custom',
          label: 'Search by annotation',
          click: () => {
            this.annotationQueried.emit(this.data.data);
          },
        },
      ],
    });
  }

  private onMouseEnter() {
    // if (!this.activeTooltip && !this.tooltip.isOpen) this.tooltip.show();
    this.showDebugLines();
  }

  private onMouseLeave() {
    // if (!this.activeTooltip && this.tooltip.isOpen) this.tooltip.hide();
    this.lines = [];
  }

  private showDebugLines() {
    const offset = getOffset(this.elRef.nativeElement);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    this.data.data?.neighbors.forEach((n) => {
      const el = document.getElementById(n.id);
      if (!el) return;
      const o = getOffset(el);
      this.lines.push({
        lx: o.left - offset.left,
        ly: o.top - offset.top,
        tx: (o.left - offset.left) * 0.9,
        ty: (o.top - offset.top) * 0.9,
        distance: n.distance,
        weight: n.weight,
      });
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}

function getOffset(el: HTMLElement) {
  let _x = 0;
  let _y = 0;
  let _el: any = el;
  while (_el && !isNaN(_el.offsetLeft) && !isNaN(_el.offsetTop)) {
    _x += _el.offsetLeft - _el.scrollLeft;
    _y += _el.offsetTop - _el.scrollTop;
    _el = _el.offsetParent;
  }
  return { top: _y, left: _x };
}
