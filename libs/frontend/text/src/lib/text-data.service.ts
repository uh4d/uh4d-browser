import { Injectable } from '@angular/core';
import { TextDto } from '@uh4d/dto/interfaces';
import { DataEventService } from '@uh4d/utils';

@Injectable({
  providedIn: 'root',
})
export class TextDataService extends DataEventService<TextDto> {}
