import {
  BufferGeometry,
  DoubleSide,
  Float32BufferAttribute,
  LinearFilter,
  LineBasicMaterial,
  LineLoop,
  LineSegments,
  MathUtils,
  Mesh,
  MeshBasicMaterial,
  Object3D,
  PlaneGeometry,
  SRGBColorSpace,
  Texture,
  TextureLoader,
  Vector2,
  Vector3,
} from 'three';
import { ViewportDefaults } from '@uh4d/config';
import {
  createBox2Lines,
  createCircleLines,
} from './geometry-generator/line-geometries';
import { VirtualProjectorCamera } from './projective-texturing/virtual-projector-camera';
import { DefaultTextureBatchLoader } from './texture-batch-loader';

/**
 * Configuration including intrinsic camera parameters to pass to {@link ImagePane} constructor.
 */
export interface IImagePaneConfig {
  /**
   * Path to image texture.
   */
  texture: string;
  /**
   * Path to thumbnail texture.
   */
  preview?: string;
  /**
   * Camera constant (relates to an image height of `1`).
   */
  ck?: number;
  /**
   * Principal point (offset from midpoint).
   */
  offset?: number[];
  /**
   * Image width (is only used to determine the aspect ratio).
   */
  width: number;
  /**
   * Image height (is only used to determine the aspect ratio).
   */
  height: number;
  /**
   * Show marker in the top-right corner of the frame, e.g., if image has annotations.
   */
  showMarker?: boolean;
}

/**
 * Class for visualizing a spatialized image. It consists of a plane with the image as texture,
 * and lines arranged as a pyramid representing the camera orientation and field of view.
 *
 * ![Screenshot](../assets/image-pane.png)
 *
 * For the interior orientation of the camera, the camera constant `ck` is used to compute the field of view.
 * The camera constant `ck` is unitless and relates to an image height of `1`,
 * while the image width is the aspect ratio.
 * The field of view (in degrees) is calculated trigonometrically.
 *
 * ![Screenshot](../assets/image-pane-fig1.png)
 */
export class ImagePane extends Object3D {
  config: IImagePaneConfig;

  width: number;
  height: number;
  fov = 0;

  image: Mesh<BufferGeometry, MeshBasicMaterial>;
  collisionObject: Mesh<BufferGeometry, MeshBasicMaterial>;
  pyramid?: LineSegments<BufferGeometry, LineBasicMaterial>;

  texture?: Texture;
  previewTexture?: Texture;
  private previewPromise: Promise<Texture> | null = null;

  vertices: {
    [key in
      | 'origin'
      | 'top-left'
      | 'top-right'
      | 'bottom-left'
      | 'bottom-right']: Vector3;
  };

  isLoading = false;

  /**
   * Construct basic geometries from intrinsic camera parameters.
   * This includes the texture plane and a click dummy.
   */
  constructor(config: IImagePaneConfig) {
    super();

    this.config = config;

    // height = 1, width = aspect ratio
    this.width = config.width / config.height;
    this.height = 1;

    let distance = 0;

    // field of view
    if (config.ck) {
      this.fov =
        2 * Math.atan(this.height / (2 * config.ck)) * MathUtils.RAD2DEG;
      distance = -config.ck;
    }

    // offset from center
    const offset = new Vector2();
    if (config.offset) {
      offset.fromArray(config.offset);
      offset.negate();
    }

    // plane with texture
    const paneGeometry = new PlaneGeometry(this.width, this.height);
    const paneMaterial = new MeshBasicMaterial({ side: DoubleSide });
    this.image = new Mesh(paneGeometry, paneMaterial);
    this.image.position.set(offset.x, offset.y, distance);

    this.vertices = {
      origin: new Vector3(),
      'top-left': new Vector3(
        -this.width / 2 + offset.x,
        this.height / 2 + offset.y,
        distance,
      ),
      'top-right': new Vector3(
        this.width / 2 + offset.x,
        this.height / 2 + offset.y,
        distance,
      ),
      'bottom-left': new Vector3(
        -this.width / 2 + offset.x,
        -this.height / 2 + offset.y,
        distance,
      ),
      'bottom-right': new Vector3(
        this.width / 2 + offset.x,
        -this.height / 2 + offset.y,
        distance,
      ),
    };

    // invisible click dummy
    const dummyPositions = [
      ...this.vertices['top-left'].toArray(),
      ...this.vertices['top-right'].toArray(),
      ...this.vertices['bottom-left'].toArray(),
      ...this.vertices['bottom-right'].toArray(),
    ];
    const dummyIndices = [0, 2, 1, 0, 1, 2, 1, 2, 3, 1, 3, 2];
    const dummyGeometry = new BufferGeometry();
    dummyGeometry.setAttribute(
      'position',
      new Float32BufferAttribute(dummyPositions, 3),
    );
    dummyGeometry.setIndex(dummyIndices);
    const dummyMaterial = new MeshBasicMaterial({ visible: false });
    this.collisionObject = new Mesh(dummyGeometry, dummyMaterial);

    // add objects to parent
    this.add(this.image, this.collisionObject);
  }

  /**
   * Load preview texture and return as promise.
   */
  loadPreview(): Promise<Texture> {
    // return texture if already loaded
    if (this.previewTexture) {
      return Promise.resolve(this.previewTexture);
    }

    // if not already loading, create loading promise
    if (!this.previewPromise) {
      this.previewPromise = new Promise((resolve, reject) => {
        DefaultTextureBatchLoader.load(
          this.config.preview!,
          (texture) => {
            texture.anisotropy = 8;
            texture.minFilter = LinearFilter;
            texture.colorSpace = SRGBColorSpace;
            this.previewTexture = texture;
            resolve(texture);
            this.previewPromise = null;
          },
          undefined,
          (xhr) => {
            console.error("Couldn't load texture", xhr);
            reject(xhr);
          },
        );
      });
    }

    // return promise
    return this.previewPromise;
  }

  private async loadTexture(): Promise<Texture> {
    if (this.texture) return this.texture;

    if (this.previewTexture) {
      this.texture = this.previewTexture;
    }

    const texture = await new TextureLoader().loadAsync(this.config.texture);
    texture.anisotropy = 8;
    texture.minFilter = LinearFilter;
    texture.colorSpace = SRGBColorSpace;
    this.texture = texture;

    return texture;
  }

  /**
   * Check if vector is within a certain distance and replace texture according as vector is within or outside threshold.
   * @param vector - Vector position, e.g. camera position.
   * @param threshold - Threshold distance.
   * @param force - Set `true`, update regardless of threshold.
   * @return True, if texture has been updated.
   */
  async updateTexture(
    vector: Vector3,
    threshold: number,
    force = false,
  ): Promise<boolean> {
    if (this.isLoading) {
      return false;
    }

    const distance = new Vector3().subVectors(vector, this.position).length();
    const mat = this.image.material;

    if (this.config.preview && (distance > threshold || force)) {
      // load preview texture
      if (!this.previewTexture) {
        this.isLoading = true;
        return this.loadPreview().then((texture) => {
          mat.map = texture;
          mat.needsUpdate = true;
          this.isLoading = false;
          return true;
        });
      } else if (mat.map === this.previewTexture) {
        return false;
      } else {
        mat.map = this.previewTexture;
        return true;
      }
    } else {
      // load normal texture
      if (!this.texture) {
        this.isLoading = true;
        return this.loadTexture().then((texture) => {
          mat.map = texture;
          mat.needsUpdate = true;
          this.isLoading = false;
          return true;
        });
      } else if (mat.map === this.texture) {
        return false;
      } else {
        mat.map = this.texture;
        return true;
      }
    }
  }

  /**
   * Create pyramid representing camera frustum.
   */
  private createPyramid() {
    if (this.pyramid) {
      return;
    }

    const v = this.vertices;
    const positions = [
      ...v.origin.toArray(),
      ...v['top-left'].toArray(),
      ...v.origin.toArray(),
      ...v['top-right'].toArray(),
      ...v.origin.toArray(),
      ...v['bottom-left'].toArray(),
      ...v.origin.toArray(),
      ...v['bottom-right'].toArray(),
      ...v['top-left'].toArray(),
      ...v['bottom-left'].toArray(),
      ...v['bottom-left'].toArray(),
      ...v['bottom-right'].toArray(),
      ...v['bottom-right'].toArray(),
      ...v['top-right'].toArray(),
      ...v['top-right'].toArray(),
      ...v['top-left'].toArray(),
    ];
    const lineGeometry = new BufferGeometry();
    lineGeometry.setAttribute(
      'position',
      new Float32BufferAttribute(positions, 3),
    );

    const lineMaterial = new LineBasicMaterial({
      color: ViewportDefaults.highlightColor,
      transparent: true,
      opacity: 0.5,
    });
    // if (Settings.images.opacity < 1) {
    //   lineMaterial.transparent = true;
    //   lineMaterial.opacity = Settings.images.opacity;
    // }

    this.pyramid = new LineSegments(lineGeometry, lineMaterial);

    if (this.config.showMarker) {
      const boxGeo = createBox2Lines(0.08, 0.08);
      boxGeo.translate(
        v['top-right'].x - 0.08,
        v['top-right'].y - 0.08,
        v['top-right'].z + 0.001,
      );
      const circleGeo = createCircleLines(0.03, 8);
      circleGeo.translate(
        v['top-right'].x - 0.08,
        v['top-right'].y - 0.08,
        v['top-right'].z + 0.001,
      );

      this.pyramid.add(
        new LineLoop(boxGeo, lineMaterial),
        new LineLoop(circleGeo, lineMaterial),
      );
    }
  }

  /**
   * Set scale of the object.
   */
  setScale(value: number): void {
    this.scale.set(value, value, value);
  }

  /**
   * Set opacity of the object.
   */
  setOpacity(value: number): void {
    const imageMat = this.image.material;

    if (value < 1) {
      imageMat.needsUpdate = imageMat.transparent === false;
      imageMat.transparent = true;
      imageMat.opacity = value;
    } else {
      imageMat.needsUpdate = imageMat.transparent === true;
      imageMat.transparent = false;
      imageMat.opacity = 1;
    }

    // if (this.pyramid) {
    //   const pyramidMat = this.pyramid.material as LineBasicMaterial;
    //   if (value < 1) {
    //     pyramidMat.transparent = true;
    //     pyramidMat.opacity = value;
    //   } else {
    //     pyramidMat.transparent = false;
    //     pyramidMat.opacity = 1;
    //   }
    // }
  }

  /**
   * Toggle pyramid and apply selection color.
   */
  select(): void {
    if (!this.pyramid) {
      this.createPyramid();
    }
    if (this.pyramid!.parent !== this) {
      this.add(this.pyramid!);
    }

    this.pyramid!.material.color.setHex(ViewportDefaults.selectionColor);
  }

  deselect(): void {
    if (this.pyramid) this.remove(this.pyramid);
  }

  /**
   * Toggle pyramid and apply highlight color.
   */
  highlight(customColor?: number): void {
    if (!this.pyramid) {
      this.createPyramid();
    }
    if (this.pyramid!.parent !== this) {
      this.add(this.pyramid!);
    }

    if (customColor !== undefined) {
      this.pyramid!.material.color.setHex(customColor);
    } else {
      this.pyramid!.material.color.set(ViewportDefaults.highlightColor);
    }
  }

  /**
   * Hide pyramid.
   */
  dehighlight(): void {
    if (this.pyramid) this.remove(this.pyramid);
  }

  /**
   * Dispose geometries, materials, and textures.
   */
  dispose(): void {
    if (this.image) {
      this.image.geometry.dispose();
      this.image.material.dispose();
    }

    if (this.texture) {
      this.texture.dispose();
    }
    if (this.previewTexture) {
      this.previewTexture.dispose();
    }

    if (this.collisionObject) {
      this.collisionObject.material.dispose();
      this.collisionObject.geometry.dispose();
    }
    if (this.pyramid) {
      this.pyramid.material.dispose();
      this.pyramid.geometry.dispose();
    }
  }

  async createProjectorCamera() {
    const texture = await this.loadTexture();
    const cam = new VirtualProjectorCamera(
      texture,
      this.fov,
      this.width / this.height,
    );
    cam.position.copy(this.position);
    cam.rotation.copy(this.rotation);
    cam.updateMatrixWorld();

    return cam;
  }
}
