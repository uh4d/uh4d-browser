import {
  Color,
  Group,
  Mesh,
  MeshBasicMaterial,
  SphereGeometry,
  Vector3,
} from 'three';
import { ViewportDefaults } from '@uh4d/config';

/**
 * Object representing a Point of Interest composed of multiple spheres.
 */
export class PointOfInterest extends Group {
  /**
   * Multiplier used to scale object with respect to distance ({@link #scaleByDistance}).
   * @private
   */
  private distanceMultiplier = 0.005;

  /**
   * Color that is used if the object is neither highlighted nor selected.
   * @private
   */
  private readonly baseColor: string | number | Color;

  constructor(baseColor: string | number | Color = 0x0a1c37) {
    super();

    this.baseColor = baseColor;

    // add spheres
    this.add(
      // opaque sphere
      new Mesh(
        new SphereGeometry(1),
        new MeshBasicMaterial({ color: this.baseColor }),
      ),
      // semi-transparent sphere
      new Mesh(
        new SphereGeometry(2),
        new MeshBasicMaterial({
          color: this.baseColor,
          transparent: true,
          opacity: 0.5,
        }),
      ),
      // bigger, invisible sphere used as click dummy
      new Mesh(
        new SphereGeometry(4),
        new MeshBasicMaterial({ visible: false }),
      ),
    );
  }

  /**
   * Set {@link #distanceMultiplier}.
   * @param value
   */
  setDistanceMultiplier(value: number): void {
    this.distanceMultiplier = value;
  }

  /**
   * Scale object with respect to position and {@link #distanceMultiplier}.
   * @param position - Position, usually viewport's camera position.
   */
  scaleByDistance(position: Vector3): void {
    const scale = this.position.distanceTo(position) * this.distanceMultiplier;
    this.scale.set(scale, scale, scale);
  }

  /**
   * Color object with highlight color.
   * @param on - If `false`, reset to {@link #baseColor}.
   */
  highlight(on = true) {
    this.setColor(on ? ViewportDefaults.highlightColor : this.baseColor);
  }

  /**
   * Color object with selection color (`0xdd0000`).
   * @param on - If `false`, reset to {@link #baseColor}.
   */
  select(on = true) {
    this.setColor(on ? 0xdd0000 : this.baseColor);
  }

  /**
   * Set color of all spheres.
   * @private
   */
  private setColor(color: string | number | Color): void {
    this.children.forEach((child) => {
      const mesh = child as Mesh<SphereGeometry, MeshBasicMaterial>;
      mesh.material.color.set(color);
    });
  }

  /**
   * Dispose all geometries and materials.
   */
  dispose(): void {
    this.children.forEach((child) => {
      const mesh = child as Mesh<SphereGeometry, MeshBasicMaterial>;
      mesh.geometry.dispose();
      mesh.material.dispose();
    });
  }
}
