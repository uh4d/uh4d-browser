import {
  Box3,
  BoxGeometry,
  BufferAttribute,
  BufferGeometry,
  Color,
  Group,
  LineBasicMaterial,
  LineSegments,
  Mesh,
  MeshBasicMaterial,
  PlaneGeometry,
  Scene,
  SphereGeometry,
  Vector3,
} from 'three';
import { TextGeometry } from 'three/examples/jsm/geometries/TextGeometry';
import { ViewportDefaults } from '@uh4d/config';
import { ImagePane } from '../image-pane';
import { GlobalFontManager } from '../manager/font-manager';
import { ClusterTree } from './cluster-tree';

export class ClusterObject<T extends ImagePane = ImagePane> {
  active = false;
  exploded = false;

  tree: ClusterTree;
  parent: ClusterObject<T> | null = null;
  children: (ClusterObject<T> | T)[] = [];

  count: number;
  depth: number;
  distance: number;

  position: Vector3;

  groundHeight = Number.MIN_VALUE;
  groundHeightNeedsUpdate = true;

  objectMap: {
    images?: Mesh<BufferGeometry, MeshBasicMaterial>[];
    text?: Mesh<BufferGeometry, MeshBasicMaterial>;
    line?: LineSegments<BufferGeometry, LineBasicMaterial>;
    collisionObject?: Mesh<BufferGeometry, MeshBasicMaterial>;
    bullet?: Mesh<BufferGeometry, MeshBasicMaterial>;
    explodeLines?: LineSegments<BufferGeometry, LineBasicMaterial>;
  } = {};
  object: Group;
  collisionObject?: Mesh;

  constructor(
    tree: ClusterTree,
    children: (ClusterObject<T> | T)[],
    depth = 0,
    distance?: number,
  ) {
    this.tree = tree;

    this.children = children;

    this.count = children.reduce((sum, obj) => {
      let count = 1;
      if (obj instanceof ClusterObject) {
        count = obj.count;
        obj.parent = this;
      }
      return sum + count;
    }, 0);

    this.depth = depth;
    this.distance =
      distance || children[0].position.distanceTo(children[1].position);

    this.position = children.reduce((vector, obj) => {
      const scalar = obj instanceof ClusterObject ? obj.count : 1;
      return vector.add(
        obj.position.clone().multiplyScalar(scalar / this.count),
      );
    }, new Vector3());

    this.object = new Group();
    this.object.position.copy(this.position);
  }

  /**
   * Toggle cluster object. Generates visible objects.
   */
  toggle(visible: boolean) {
    if (!this.objectMap.images) {
      const lineVertices: number[] = [];
      const bbox = new Box3();
      const images = this.getLeaves();
      const length = Math.min(images.length, 3);
      const planes: Mesh[] = [];

      for (let i = 0; i < length; i++) {
        const img = images[i];

        const offset = new Vector3(
          0.5 * i - (0.5 * (length - 1)) / 2,
          Math.random() * 0.3 - 0.15,
          0.2 * i - (0.2 * (length - 1)) / 2,
        );

        const planeMat = new MeshBasicMaterial({
          map: img.previewTexture || img.texture || null,
        });
        if (!planeMat.map) {
          img.loadPreview().then((texture) => {
            planeMat.map = texture;
            planeMat.needsUpdate = true;
            // TODO: better update routine
            this.tree.update$.next();
          });
        }

        const plane = new Mesh(
          new PlaneGeometry(img.width, img.height),
          planeMat,
        );
        plane.position.copy(offset);
        plane.name = 'image' + i;

        this.object.add(plane);
        planes.push(plane);

        bbox.expandByObject(plane);

        const tr = img.vertices['top-right'];
        lineVertices.push(
          tr.x + offset.x,
          tr.y + offset.y,
          offset.z,
          -tr.x + offset.x,
          tr.y + offset.y,
          offset.z,
          -tr.x + offset.x,
          tr.y + offset.y,
          offset.z,
          -tr.x + offset.x,
          -tr.y + offset.y,
          offset.z,
          -tr.x + offset.x,
          -tr.y + offset.y,
          offset.z,
          tr.x + offset.x,
          -tr.y + offset.y,
          offset.z,
          tr.x + offset.x,
          -tr.y + offset.y,
          offset.z,
          tr.x + offset.x,
          tr.y + offset.y,
          offset.z,
        );
      }

      const lineGeo = new BufferGeometry();
      lineGeo.setAttribute(
        'position',
        new BufferAttribute(new Float32Array(lineVertices), 3),
      );
      const line = new LineSegments(
        lineGeo,
        new LineBasicMaterial({
          color: ViewportDefaults.selectionColor,
        }),
      );
      line.name = 'lines';

      const bbSize = bbox.getSize(new Vector3());
      const collisionObj = new Mesh(
        new BoxGeometry(bbSize.x, bbSize.y, bbSize.z),
        new MeshBasicMaterial({ visible: false }),
      );
      collisionObj.name = 'collisionObject';

      const font = GlobalFontManager.get('HelvetikerFont');
      if (!font) throw new Error('Cannot get font');
      const text = new Mesh(
        new TextGeometry(this.count.toString(), {
          font,
          size: 0.5,
          height: 0.02,
          curveSegments: 6,
        }),
        new MeshBasicMaterial({
          color: 0xffdd00,
          depthWrite: true,
          depthTest: true,
        }),
      );
      text.geometry.center();
      text.position.z = 0.25;
      text.name = 'text';

      this.object.add(line);
      this.object.add(collisionObj);
      this.object.add(text);

      Object.assign(this.objectMap, {
        images: planes,
        line,
        text,
        collisionObject: collisionObj,
      });
      this.collisionObject = collisionObj;

      this.object.scale.set(5, 5, 5);

      this.object.userData['cluster'] = this;
    }

    if (visible && !this.active) {
      this.setOpacity(this.tree.opacity);
      this.tree.scene.add(this.object);
      this.tree.octree.add(this.collisionObject!);
      this.active = true;
    } else {
      this.tree.scene.remove(this.object);
      this.tree.octree.remove(this.collisionObject!);
      this.active = false;
    }
  }

  lookAt(position: Vector3) {
    this.object.lookAt(position);
    const scale =
      (this.object.position.distanceTo(position) * this.tree.scale) / 200;
    this.object.position.y = Math.max(
      this.position.y,
      this.groundHeight + scale / 2,
    );
    this.object.scale.set(scale, scale, scale);
  }

  setGroundHeight(value?: number) {
    this.groundHeight = !Number.isNaN(value)
      ? (value as number)
      : Number.MIN_VALUE;
    this.groundHeightNeedsUpdate = false;
  }

  setOpacity(value: number) {
    const isTransparent = value < 1.0;
    const needsUpdate =
      this.objectMap.text?.material.transparent !== isTransparent;

    this.objectMap.images?.forEach((img) => {
      img.material.transparent = isTransparent;
      img.material.opacity = value;
      img.material.needsUpdate = needsUpdate;
    });
    if (this.objectMap.line) {
      this.objectMap.line.material.transparent = isTransparent;
      this.objectMap.line.material.opacity = value;
      this.objectMap.line.material.needsUpdate = needsUpdate;
    }
    if (this.objectMap.text) {
      this.objectMap.text.material.transparent = isTransparent;
      this.objectMap.text.material.opacity = value;
      this.objectMap.text.material.needsUpdate = needsUpdate;
    }
  }

  /**
   * Traverse this node and its children.
   * @param callback - Callback for node. If `false` gets returned, the traversal of current node gets aborted.
   * @param includeLeaves - Call callback also for leaf nodes (i.e. that are not cluster objects).
   */
  traverse(
    callback: (node: ClusterObject<T> | T) => boolean | void,
    includeLeaves?: boolean,
  ): void;
  traverse(callback: (node: ClusterObject<T> | T) => boolean | void): void;
  traverse(
    callback: (node: ClusterObject<T> | T) => boolean | void,
    includeLeaves?: boolean,
  ): void {
    if (callback(this) !== false) {
      this.children.forEach((child) => {
        if (child instanceof ClusterObject) {
          child.traverse(callback, includeLeaves);
        } else if (includeLeaves === true) {
          callback(child);
        }
      });
    }
  }

  /**
   * Get all leaves of this cluster object sorted by depth.
   */
  getLeaves(): T[] {
    const tmp: { leaf: T; parent: ClusterObject<T> }[] = [];

    this.traverse((cluster) => {
      cluster.children.forEach((child) => {
        if (!(child instanceof ClusterObject)) {
          tmp.push({
            leaf: child as T,
            parent: cluster as ClusterObject<T>,
          });
        }
      });
    });

    tmp.sort((a, b) => b.parent.depth - a.parent.depth);

    return tmp.map((value) => value.leaf);
  }

  explode() {
    if (this.exploded) return;

    const obj = this.object;
    const showLines = obj.parent instanceof Scene;
    let lineVertices: number[] = [];

    this.getLeaves().forEach((leaf) => {
      if (!(leaf.parent instanceof Scene)) {
        this.tree.scene.add(leaf);
        leaf.userData['item'].visible = true;
        leaf.highlight(0x000000);
      }
      if (showLines) {
        lineVertices = lineVertices
          .concat(this.object.position.toArray())
          .concat(leaf.position.toArray());
      }
    });

    if (showLines && !this.objectMap.explodeLines) {
      const lineGeo = new BufferGeometry();
      lineGeo.setAttribute(
        'position',
        new BufferAttribute(new Float32Array(lineVertices), 3),
      );
      const line = new LineSegments(
        lineGeo,
        new LineBasicMaterial({
          color: 0x000000,
          transparent: true,
          opacity: 0.5,
          depthTest: true,
          depthWrite: true,
        }),
      );
      line.renderOrder = -2;
      line.name = 'explodeLines';
      this.objectMap.explodeLines = line;
    }

    if (showLines && !this.objectMap.bullet) {
      const bullet = new Mesh(
        new SphereGeometry(0.1, 12, 6),
        new MeshBasicMaterial({
          color: ViewportDefaults.selectionColor,
          depthTest: true,
        }),
      );
      bullet.name = 'bullet';
      this.objectMap.bullet = bullet;
    }

    if (this.objectMap.line) {
      obj.remove(this.objectMap.line);
    }
    if (this.objectMap.text) {
      obj.remove(this.objectMap.text);
    }
    if (this.objectMap.images) {
      this.objectMap.images.forEach((img) => {
        obj.remove(img);
      });
    }

    if (showLines && this.objectMap.bullet) {
      obj.add(this.objectMap.bullet);
    }
    if (showLines && this.objectMap.explodeLines) {
      this.tree.scene.add(this.objectMap.explodeLines);
    }

    this.exploded = true;
  }

  implode() {
    if (!this.exploded) return;

    this.getLeaves().forEach((leaf) => {
      this.tree.scene.remove(leaf);
      leaf.userData['item'].visible = false;
      leaf.dehighlight();
    });

    const obj = this.object;

    if (this.objectMap.bullet) obj.remove(this.objectMap.bullet);
    if (this.objectMap.explodeLines)
      this.tree.scene.remove(this.objectMap.explodeLines);

    if (this.objectMap.line) obj.add(this.objectMap.line);
    if (this.objectMap.text) obj.add(this.objectMap.text);
    this.objectMap.images?.forEach((img) => {
      obj.add(img);
    });

    this.exploded = false;
  }

  select(active: boolean) {
    if (!this.objectMap.images) return;
    this.objectMap.images.forEach((img) => {
      if (active) {
        img.material.color.lerp(
          new Color(ViewportDefaults.selectionColor),
          0.3,
        );
      } else {
        img.material.color.setHex(0xffffff);
      }
    });
  }

  highlight(highlighted: boolean) {
    if (!this.objectMap.images) return;
    this.objectMap.images.forEach((img) => {
      if (highlighted) {
        img.material.color.lerp(
          new Color(ViewportDefaults.highlightColor),
          0.3,
        );
      } else {
        img.material.color.setHex(0xffffff);
      }
    });
  }

  /**
   * Dispose display objects.
   */
  dispose() {
    for (const obj of Object.values(this.objectMap)) {
      if (Array.isArray(obj)) {
        obj.forEach((o) => {
          o.geometry.dispose();
          o.material.dispose();
        });
      } else {
        obj.geometry.dispose();
        obj.material.dispose();
      }
    }
  }
}
