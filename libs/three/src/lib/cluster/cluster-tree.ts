import { Subject } from 'rxjs';
import { Camera, MathUtils, Mesh, Scene, Vector3 } from 'three';
import { Octree } from '@brakebein/threeoctree';
import { ClusterObject } from './cluster-object';
import { ImagePane } from '../image-pane';

export interface IClusterTreeConfig {
  scene: Scene;
  octree: Octree;
  linkage?: 'average' | 'single' | 'complete';
  distanceMultiplier?: number;
  scale?: number;
  opacity?: number;
}

type ClusterHelper<T> = {
  key: number;
  size: number;
  dist?: number;
  value?: T;
  index: number;
  left?: ClusterHelper<T>;
  right?: ClusterHelper<T>;
};

export class ClusterTree<T extends ImagePane = ImagePane> {
  scene: Scene;
  octree: Octree;

  private readonly linkage: 'average' | 'single' | 'complete';

  private activeClusters: (ClusterObject<T> | T)[] = [];
  private distanceMultiplier = 10;
  scale = 3;
  opacity = 1.0;

  root: ClusterObject<T> | T | null = null;

  update$ = new Subject<void>();

  constructor(config: IClusterTreeConfig) {
    this.scene = config.scene;
    this.octree = config.octree;

    this.linkage = config.linkage || 'average';

    if (config.distanceMultiplier) {
      this.distanceMultiplier = config.distanceMultiplier;
    }
    if (config.scale) {
      this.scale = config.scale;
    }
    if (config.opacity) {
      this.opacity = config.opacity;
    }
  }

  // TODO: cleanup code

  // TODO: insert single element
  // insert() {
  //
  // }

  /**
   * Empty cluster tree and build up with new objects.
   */
  bulkInsert(objects: T[]) {
    this.clean();

    const startTime = Date.now();

    // adopted code from https://github.com/harthur/clustering

    const clusters: ClusterHelper<T>[] = [];
    const dists: number[][] = []; // distances between each pair of clusters
    const mins: number[] = []; // closest cluster for each cluster
    const index: ClusterHelper<T>[] = []; // keep a hash of all clusters by key

    for (let i = 0, l = objects.length; i < l; i++) {
      const cluster: ClusterHelper<T> = {
        value: objects[i],
        key: i,
        index: i,
        size: 1,
      };
      clusters[i] = cluster;
      index[i] = cluster;
      dists[i] = [];
      mins[i] = 0;
    }

    for (let i = 0, l = clusters.length; i < l; i++) {
      for (let j = 0; j <= i; j++) {
        const dist =
          i === j
            ? Infinity
            : clusters[i].value!.position.distanceTo(
                clusters[j].value!.position,
              );
        dists[i][j] = dist;
        dists[j][i] = dist;
        if (dist < dists[i][mins[i]]) {
          mins[i] = j;
        }
      }
    }

    const mergeClosest = () => {
      // find two closest clusters from cached mins
      let minKey = 0;
      let minDist = Infinity;
      for (let i = 0, l = clusters.length; i < l; i++) {
        const key = clusters[i].key;
        const dist = dists[key][mins[key]];
        if (dist < minDist) {
          minKey = key;
          minDist = dist;
        }
      }

      if (minDist >= Infinity) {
        return false;
      }

      const c1 = index[minKey];
      const c2 = index[mins[minKey]];

      // merge two clusters
      const merged: ClusterHelper<T> = {
        left: c1,
        right: c2,
        key: c1.key,
        index: c1.index,
        size: c1.size + c2.size,
        dist: dists[c1.key][c2.key],
      };

      clusters[c1.index] = merged;
      clusters.splice(c2.index, 1);
      index[c1.key] = merged;

      // update distances with new merged cluster
      for (let i = 0, l = clusters.length; i < l; i++) {
        const ci = clusters[i];
        let dist = 0;

        if (c1.key === ci.key) {
          dist = Infinity;
        } else if (this.linkage === 'average') {
          dist =
            (dists[c1.key][ci.key] * c1.size +
              dists[c2.key][ci.key] * c2.size) /
            (c1.size + c2.size);
        } else if (this.linkage === 'single') {
          dist = dists[c1.key][ci.key];
          if (dists[c1.key][ci.key] > dists[c2.key][ci.key]) {
            dist = dists[c2.key][ci.key];
          }
        } else if (this.linkage === 'complete') {
          dist = dists[c1.key][ci.key];
          if (dists[c1.key][ci.key] < dists[c2.key][ci.key]) {
            dist = dists[c2.key][ci.key];
          }
        }

        dists[c1.key][ci.key] = dists[ci.key][c1.key] = dist;
      }

      // update cached mins
      for (let i = 0, l = clusters.length; i < l; i++) {
        const key1 = clusters[i].key;
        if (mins[key1] === c1.key || mins[key1] === c2.key) {
          let min = key1;
          for (let j = 0; j < l; j++) {
            const key2 = clusters[j].key;
            if (dists[key1][key2] < dists[key1][min]) {
              min = key2;
            }
          }
          mins[key1] = min;
        }
        clusters[i].index = i;
      }

      // clean uo metadata used for clustering
      // delete c1.key;
      // delete c2.key;
      // delete c1.index;
      // delete c2.index;

      return true;
    };

    while (mergeClosest()) {
      // wait iterative process
    }

    // clusters.forEach((c) => {
    //   delete c.key;
    //   delete c.index;
    // });

    const depthFirst = (cluster: ClusterHelper<T>): T | ClusterObject<T> => {
      if (!cluster.left || !cluster.right) {
        return cluster.value!;
      }
      let a;
      let b;
      if (cluster.left.left) {
        a = depthFirst(cluster.left);
      } else if (cluster.left.value) {
        a = cluster.left.value;
      }
      if (cluster.right.right) {
        b = depthFirst(cluster.right);
      } else if (cluster.right.value) {
        b = cluster.right.value;
      }
      return new ClusterObject(this, [a!, b!]);
    };

    if (clusters[0]) {
      this.root = depthFirst(clusters[0]);
    }

    console.log(
      'ClusterTree - Build Elapsed time',
      Date.now() - startTime,
      'ms',
    );
    console.log('ClusterTree - root', this.root);
  }

  /**
   * Empty cluster tree.
   */
  clean() {
    this.activeClusters.forEach((c) => {
      if (c instanceof ClusterObject) {
        c.toggle(false);
      } else {
        this.scene.remove(c);
        this.octree.remove(c.collisionObject);
        c.userData['item'].visible = false;
      }
    });

    this.activeClusters = [];

    if (!this.root) return;

    if (this.root instanceof ClusterObject) {
      this.root.traverse((node) => {
        node.dispose();
      });
    }

    this.root = null;
  }

  getActiveClusters(): ClusterObject<T>[] {
    return this.activeClusters.filter<ClusterObject<T>>(
      ((value) => value instanceof ClusterObject) as (
        v: ClusterObject<T> | T,
      ) => v is ClusterObject<T>,
    );
  }

  getActiveLeaves(): T[] {
    return this.activeClusters.filter<T>(
      ((value) => !(value instanceof ClusterObject)) as (
        v: ClusterObject<T> | T,
      ) => v is T,
    );
  }

  setDistanceMultiplier(value: number) {
    this.distanceMultiplier = value;
  }

  setScale(value: number) {
    this.scale = value;
  }

  setOpacity(value: number) {
    this.opacity = MathUtils.clamp(value, 0, 1);
  }

  getObjectsByThreshold(threshold: number) {
    // this.root
  }

  getObjectsByDistance(position: Vector3): (ClusterObject<T> | T)[] {
    const tmp: (ClusterObject<T> | T)[] = [];

    if (this.root instanceof ClusterObject) {
      // traverse tree
      this.root.traverse((node) => {
        if (
          !(node instanceof ClusterObject) ||
          node.position.distanceTo(position) >
            node.distance * this.distanceMultiplier
        ) {
          tmp.push(node);
          return false;
        }
        return true;
      }, true);
    } else {
      // there is only one single object in tree
      tmp.push(this.root!);
    }

    return tmp;
  }

  getCollisionObjects(): Mesh[] {
    return this.activeClusters.map((c) => c.collisionObject!);
  }

  hide() {
    this.activeClusters.forEach((c) => {
      if (c instanceof ClusterObject) {
        c.toggle(false);
      } else {
        this.scene.remove(c);
        this.octree.remove(c.collisionObject);
        c.userData['item'].visible = false;
      }
    });

    this.octree.update();
  }

  update(camera: Camera, getGroundHeight?: (pos: Vector3) => number | undefined) {
    if (!this.root) return;

    // handle outdated objects
    this.activeClusters.forEach((c) => {
      if (c instanceof ClusterObject) {
        c.toggle(false);
      } else {
        this.scene.remove(c);
        this.octree.remove(c.collisionObject);
        c.userData['item'].visible = false;
      }
    });

    this.activeClusters = this.getObjectsByDistance(camera.position);

    // activate new objects
    this.activeClusters.forEach((c) => {
      if (c instanceof ClusterObject) {
        c.toggle(true);
        if (getGroundHeight && c.groundHeightNeedsUpdate) {
          c.setGroundHeight(getGroundHeight(c.position));
        }
        c.lookAt(camera.position);
      } else {
        this.scene.add(c);
        this.octree.add(c.collisionObject);
        c.userData['item'].visible = true;
      }
    });

    this.octree.update();
  }

  // drawDebugGraph() {
  //
  //   const geometry = new Geometry();
  //   const colorStep = 0.75 / (this.root.depth - 1);
  //
  //   this.root.traverse(node => {
  //
  //   });
  //
  // }
}
