import { Loader, LoadingManager } from 'three';
import { Subject } from 'rxjs';

export class LoadingManagerRx implements LoadingManager {
  private loading = false;
  private itemsLoaded = 0;
  private itemsTotal = 0;

  private urlModifier?: (url: string) => string;
  private handlers: (RegExp | Loader)[] = [];

  public onStart?: (url: string, loaded: number, total: number) => void;
  public onLoad: () => void;
  public onProgress: (url: string, loaded: number, total: number) => void;
  public onError: (url: string) => void;

  public onStart$ = new Subject<{
    url: string;
    loaded: number;
    total: number;
  }>();
  public onLoad$ = new Subject<void>();
  public onProgress$ = new Subject<{
    url: string;
    loaded: number;
    total: number;
  }>();
  public onError$ = new Subject<string>();

  constructor(
    onLoad?: () => void,
    onProgress?: (url: string, loaded: number, total: number) => void,
    onError?: (url: string) => void,
  ) {
    this.onLoad = onLoad!;
    this.onProgress = onProgress!;
    this.onError = onError!;
  }

  isLoading(): boolean {
    return this.loading;
  }

  itemStart(url: string): any {
    this.itemsTotal++;

    if (this.loading === false) {
      if (this.onStart !== undefined) {
        this.onStart(url, this.itemsLoaded, this.itemsTotal);
      }
      this.onStart$.next({
        url,
        loaded: this.itemsLoaded,
        total: this.itemsTotal,
      });
    }

    this.loading = true;
  }

  itemEnd(url: string): void {
    this.itemsLoaded++;

    if (this.onProgress !== undefined) {
      this.onProgress(url, this.itemsLoaded, this.itemsTotal);
    }
    this.onProgress$.next({
      url,
      loaded: this.itemsLoaded,
      total: this.itemsTotal,
    });

    if (this.itemsLoaded === this.itemsTotal) {
      this.loading = false;
      if (this.onLoad !== undefined) {
        this.onLoad();
      }
      this.onLoad$.next();
    }
  }

  itemError(url: string) {
    if (this.onError !== undefined) {
      this.onError(url);
    }
    this.onError$.next(url);
  }

  reset() {
    this.itemsLoaded = 0;
    this.itemsTotal = 0;
  }

  setURLModifier(callback?: (url: string) => string): this {
    this.urlModifier = callback;
    return this;
  }

  resolveURL(url: string): string {
    if (this.urlModifier) {
      return this.urlModifier(url);
    }
    return url;
  }

  getHandler(file: string): Loader | null {
    for (let i = 0, l = this.handlers.length; i < l; i += 2) {
      const regex = this.handlers[i] as RegExp;
      const loader = this.handlers[i + 1];

      if (regex.global) {
        regex.lastIndex = 0;
      }

      if (regex.test(file)) {
        return loader as Loader;
      }
    }

    return null;
  }

  addHandler(regex: RegExp, loader: Loader): this {
    this.handlers.push(regex, loader);

    return this;
  }

  removeHandler(regex: RegExp): this {
    const index = this.handlers.indexOf(regex);

    if (index !== -1) {
      this.handlers.splice(index, 2);
    }

    return this;
  }
}
