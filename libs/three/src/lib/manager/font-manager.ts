import { Font, FontLoader } from 'three/examples/jsm/loaders/FontLoader';

export class FontManager {
  private fonts: Map<string, Font>;

  constructor() {
    this.fonts = new Map();

    const fontLoader = new FontLoader();
    fontLoader.load('assets/fonts/helvetiker_bold.typeface.json', (font) => {
      this.fonts.set('HelvetikerFont', font);
    });
  }

  get(key: string): Font | undefined {
    return this.fonts.get(key);
  }
}

export const GlobalFontManager = new FontManager();
