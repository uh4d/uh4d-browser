import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import {
  Box3,
  BufferAttribute,
  Color,
  FileLoader,
  Group,
  MathUtils,
  Mesh,
  MeshStandardMaterial,
  Object3D,
  SRGBColorSpace,
  TextureLoader,
} from 'three';
import { BehaviorSubject, filter, firstValueFrom, Subject } from 'rxjs';
import { TerrainDto } from '@uh4d/dto/interfaces';
import { CoordinatesConverter } from '@uh4d/utils';

export class TerrainManager {
  private coordsConverter!: CoordinatesConverter;
  private gltfLoader: GLTFLoader;

  private baseUrl = '';
  private currentTerrain: string | null = null;
  private currentMap: string | null = null;

  private promise?: Promise<Object3D>;
  private terrain$ = new BehaviorSubject<Object3D | undefined>(undefined);

  private root: Object3D = new Group();
  private type?: 'plane' | 'dgm' | 'elevation-api';

  private boundingBox = new Box3();

  public onUpdate$ = new Subject<void>();

  constructor(gltfLoader: GLTFLoader) {
    this.gltfLoader = gltfLoader;
  }

  get() {
    return this.root;
  }

  getAsync(): Promise<Object3D> {
    return firstValueFrom(
      this.terrain$.pipe(filter((value): value is Object3D => !!value)),
    );
  }

  getFilePath() {
    return this.currentTerrain;
  }

  setBaseUrl(url: string) {
    this.baseUrl = url;
  }

  setCoordsConverter(converter: CoordinatesConverter) {
    this.coordsConverter = converter;
  }

  getBoundingBox() {
    return this.boundingBox;
  }

  /**
   * Load terrain tiles from GLTF file.
   * @param data - Terrain data
   */
  loadCustomTerrain(data: TerrainDto): Promise<Object3D> {
    const path = 'data/' + data.file.path + data.file.file;

    if (path === this.currentTerrain) {
      return Promise.resolve(this.root);
    }

    this.dispose();

    this.promise = new Promise<Object3D>((resolve, reject) => {
      this.gltfLoader.load(
        this.baseUrl + path,
        (gltf) => {
          const model = gltf.scene.children[0];
          model.position.copy(this.coordsConverter.fromLatLon(data.location));
          model.updateMatrixWorld(true);

          this.root.add(model);
          this.type = 'dgm';
          this.currentTerrain = path;

          // this.root.traverse(object => {
          //   if (object instanceof Mesh) {
          //     // object.renderOrder = -95;
          //     const mat = object.material as MeshStandardMaterial;
          //     // mat.depthTest = false;
          //   }
          // });

          this.boundingBox.expandByObject(this.root);

          this.onUpdate$.next();

          this.terrain$.next(this.root);
          resolve(this.root);
        },
        undefined,
        (err) => {
          console.error('Error while loading GLTF file!', err);
          reject(err);
        },
      );
    });

    return this.promise;
  }

  loadElevationApiTerrain(path: string) {
    if (path === this.currentTerrain) {
      return Promise.resolve(this.root);
    }

    this.dispose();

    this.promise = new Promise<Object3D>((resolve, reject) => {
      this.gltfLoader.load(
        this.baseUrl + path,
        (gltf) => {
          const model = gltf.scene.getObjectByName('TerrainNode') as Mesh;

          // transform geometry
          const attr = model.geometry.getAttribute(
            'position',
          ) as BufferAttribute;
          for (let i = 0, l = attr.count; i < l; i++) {
            const vec = this.coordsConverter.fromWebMercator(
              attr.getX(i),
              attr.getZ(i),
              attr.getY(i),
            );
            attr.setXYZ(i, vec.x, vec.y, vec.z);
          }
          model.geometry.computeBoundingBox();
          model.geometry.computeBoundingSphere();

          const material = model.material as MeshStandardMaterial;
          if (material.map) {
            material.map.colorSpace = SRGBColorSpace;
          }

          this.root.add(model);
          this.type = 'elevation-api';
          this.currentTerrain = path;

          this.boundingBox.expandByObject(this.root);

          this.onUpdate$.next();

          this.terrain$.next(this.root);
          resolve(this.root);
        },
        undefined,
        (err) => {
          console.error('Error while loading GLTF file!', err);
          reject(err);
        },
      );
    });

    return this.promise;
  }

  /**
   * Apply map textures to terrain tiles.
   * @param path - Path to folder with map tiles
   */
  async setMap(path: string | null) {
    if (path === this.currentMap || this.type === 'elevation-api') {
      return;
    }

    if (!this.promise) {
      console.warn('No terrain model generated or loaded!');
      return;
    }

    await this.promise;

    if (path) {
      path = path.replace(/\/$/, '');

      // load and apply textures
      const fileLoader = new FileLoader();
      const textureLoader = new TextureLoader();

      // get map metadata
      const meta = JSON.parse(
        (await fileLoader.loadAsync(
          this.baseUrl + path + '/meta.json',
        )) as string,
      );

      // load textures for each tile
      const regexp = /tile_(-?\d+)_(-?\d+)/;

      this.root.traverse((object) => {
        if (object instanceof Mesh) {
          // extract indexes
          const matches = object.name.match(regexp);
          if (!matches) return; // ToDo: make more robust
          const indexX = parseInt(matches[1], 10);
          const indexZ = parseInt(matches[2], 10);

          const mat = object.material as MeshStandardMaterial;

          if (
            meta.extents.min.x <= indexX &&
            indexX <= meta.extents.max.x &&
            meta.extents.min.y <= indexZ &&
            indexZ <= meta.extents.max.y
          ) {
            // texture available for tile
            textureLoader.load(
              `${this.baseUrl + path}/${object.name}.jpg`,
              (texture) => {
                texture.anisotropy = 8;
                texture.colorSpace = SRGBColorSpace;
                mat.map = texture;
                mat.color.set(0xeeeeee);
                mat.needsUpdate = true;
                this.onUpdate$.next();
              },
            );
          } else {
            // no texture for tile, set color
            mat.color.set(meta.baseColor).multiply(new Color(0xeeeeee));
            if (mat.map) {
              mat.map.dispose();
              mat.map = null;
              mat.needsUpdate = true;
            }
          }
        }
      });
    } else {
      // no texture, simple color
      this.root.traverse((object) => {
        if (object instanceof Mesh) {
          const mat = object.material as MeshStandardMaterial;
          if (mat.map) {
            mat.map.dispose();
            mat.map = null;
          }
          mat.color.set(0x808080);
          mat.needsUpdate = true;
        }
      });
    }

    this.currentMap = path;

    this.onUpdate$.next();
  }

  setOpacity(value: number) {
    value = MathUtils.clamp(value, 0, 1);

    this.root.traverse((object) => {
      if (object instanceof Mesh) {
        const mat = object.material as MeshStandardMaterial;
        mat.transparent = value !== 1;
        mat.opacity = value;
        mat.needsUpdate = true;
      }
    });

    this.onUpdate$.next();
  }

  dispose() {
    this.root.traverse((object) => {
      if (object instanceof Mesh) {
        object.geometry.dispose();
        const mat = object.material as MeshStandardMaterial;
        if (mat.map) {
          mat.map.dispose();
        }
        mat.dispose();
      }
    });

    this.root.remove(...this.root.children);

    this.currentTerrain = null;
    this.currentMap = null;
  }
}
