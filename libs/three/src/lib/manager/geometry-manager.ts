import { BufferGeometry } from 'three';

export class GeometryManager {
  private geometries: Map<string, BufferGeometry>;

  constructor() {
    this.geometries = new Map();

    // set standard geometries
    const initGeo = new BufferGeometry();
    initGeo.name = 'initGeo';
    initGeo.userData = { mPersistent: true };
    this.add(initGeo);
  }

  /**
   * Get geometry by id/name.
   */
  get(key: string): BufferGeometry | undefined {
    return this.geometries.get(key);
  }

  /**
   * Check if geometry instance is already added.
   */
  contains(geometry: BufferGeometry): boolean {
    return [...this.geometries.values()].includes(geometry);
  }

  /**
   * Add to manager (if not already done, set mCount=0 in userData).
   */
  private add(
    geometry: BufferGeometry,
    prefix = '',
  ): BufferGeometry | undefined {
    if (!(geometry instanceof BufferGeometry)) {
      console.warn('Geometry type not supported! Use THREE.BufferGeometry.');
      return undefined;
    }

    if (!geometry.name && !prefix) {
      console.warn('Geometry must have a name!');
      return undefined;
    }

    // check if geometry already part of manager
    const existingGeo = this.get(
      (prefix ? prefix + '_' : '') + geometry.userData['mName'],
    );

    if (existingGeo) {
      if (!this.contains(geometry)) {
        geometry.dispose();
      }
      return existingGeo;
    }

    // prepare metadata
    Object.assign(geometry.userData, {
      mCount: 0,
      mName: geometry.name,
    });

    if (prefix) {
      geometry.name = prefix + '_' + geometry.name;
    }

    this.geometries.set(geometry.name, geometry);

    return geometry;
  }

  /**
   * Add to manager if not already done and increment mCount
   */
  assign(key: string): BufferGeometry | undefined;
  assign(geometry: BufferGeometry, prefix?: string): BufferGeometry | undefined;
  assign(
    keyOrGeo: string | BufferGeometry,
    prefix = '',
  ): BufferGeometry | undefined {
    let geo: BufferGeometry | undefined;

    if (keyOrGeo instanceof BufferGeometry) {
      geo = this.add(keyOrGeo, prefix);
    } else {
      geo = this.get(keyOrGeo);
    }

    if (!geo) {
      console.warn('Geometry not found!');
      return undefined;
    }

    geo.userData['mCount']++;

    return geo;
  }

  /**
   * Remove geometry and dispose or decrement mCount
   */
  remove(geometry: BufferGeometry) {
    const geo = this.get(geometry.name);

    if (geo) {
      if (geo.userData['mCount'] > 1) {
        geo.userData['mCount']--;
      } else if (!geo.userData['mPersistent']) {
        this.geometries.delete(geo.name);
        geo.dispose();
      }
    }

    // TODO: set timeout, so geometries are not instantly deleted
  }
}
