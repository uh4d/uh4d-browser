import { RGBAFormat, Texture, TextureLoader } from 'three';
import { buffer, debounceTime, Subject } from 'rxjs';

export class TextureBatchLoader extends TextureLoader {
  request$ = new Subject<{
    url: string;
    texture: Texture;
    onLoad: (texture: Texture) => void;
    onProgress?: (event: ProgressEvent) => void;
    onError?: (event: ErrorEvent) => void;
  }>();

  constructor() {
    super();

    this.request$
      .pipe(buffer(this.request$.pipe(debounceTime(100))))
      .subscribe(async (requests) => {
        // collect files
        const files = requests.map((value) => value.url);

        // batch request
        const response = await fetch('data/batch', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ files }),
        });

        const results = await response.json();

        for (let i = 0, l = requests.length; i < l; i++) {
          const req = requests[i];
          const res = results[i];

          if (res.status !== 200) {
            req.onError?.(res);
            continue;
          }

          // set texture image source
          const image = new Image();
          req.texture.image = image;
          // const isJPEG = req.url.search( /\.jpe?g($|\?)/i ) > 0 || req.url.search( /^data:image\/jpeg/ ) === 0;
          // req.texture.format = isJPEG ? RGBFormat : RGBAFormat;
          req.texture.format = RGBAFormat;

          image.onload = () => {
            req.texture.needsUpdate = true;
            req.onLoad(req.texture);
          };

          image.src = 'data:' + res.mimeType + ';base64,' + res.file;
        }
      });
  }

  override load(
    url: string,
    onLoad: (texture: Texture) => void,
    onProgress?: (event: ProgressEvent) => void,
    onError?: (event: ErrorEvent) => void,
  ): Texture {
    const texture = new Texture();

    this.request$.next({ url, texture, onLoad, onProgress, onError });

    return texture;
  }
}

export const DefaultTextureBatchLoader = new TextureBatchLoader();
