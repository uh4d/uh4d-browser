import { ImageLoader, RGBAFormat, Texture, TextureLoader } from 'three';
import { mergeMap, Subject } from 'rxjs';

export class TextureLoaderRx extends TextureLoader {
  request$ = new Subject<{
    url: string;
    texture: Texture;
    onLoad: (texture: Texture) => void;
    onProgress?: (event: ProgressEvent) => void;
    onError?: (event: ErrorEvent) => void;
  }>();

  constructor() {
    super();

    this.request$
      .pipe(
        mergeMap(
          (value) =>
            new Promise((resolve) => {
              new ImageLoader().load(
                value.url,
                (image) => {
                  value.texture.image = image;
                  // const isJPEG =
                  //   value.url.search(/\.jpe?g($|\?)/i) > 0 ||
                  //   value.url.search(/^data:image\/jpeg/) === 0;
                  // value.texture.format = isJPEG ? RGBFormat : RGBAFormat;
                  value.texture.format = RGBAFormat;
                  value.texture.needsUpdate = true;
                  value.onLoad(value.texture);
                  resolve(value.url);
                },
                value.onProgress,
                (err: any) => {
                  value.onError?.(err);
                  resolve(err);
                },
              );
            }),
          50,
        ),
      )
      .subscribe(() => {
        // subscription
      });
  }

  override load(
    url: string,
    onLoad: (texture: Texture) => void,
    onProgress?: (event: ProgressEvent) => void,
    onError?: (event: ErrorEvent) => void,
  ): Texture {
    const texture = new Texture();

    this.request$.next({ url, texture, onLoad, onProgress, onError });

    return texture;
  }
}

export const DefaultTextureLoaderRx = new TextureLoaderRx();
