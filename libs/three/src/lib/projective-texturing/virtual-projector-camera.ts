import {
  CameraHelper,
  Frustum,
  LineBasicMaterial,
  Matrix4,
  Object3D,
  PerspectiveCamera,
  Texture,
} from 'three';

/**
 * Virtual camera that is used to project a texture onto an objects surface.
 * Internally, the assigned texture as well as the camera's `matrixWorldInverse` and `projectionMatrix`
 * will be passed to the projective texturing shader in {@link ProjectiveMaterial}.
 */
export class VirtualProjectorCamera extends PerspectiveCamera {
  private enabled = true;
  private readonly texture: Texture;
  private readonly frustum: Frustum;
  helper?: CameraHelper;

  constructor(
    texture: Texture,
    fov: number,
    aspect: number,
    near = 1,
    far = 150,
  ) {
    super(fov, aspect, near, far);
    this.texture = texture;
    this.frustum = new Frustum();
  }

  setEnabled(enable: boolean): void {
    this.enabled = enable;
  }

  isEnabled(): boolean {
    return this.enabled;
  }

  override updateProjectionMatrix() {
    super.updateProjectionMatrix();
    this.setFrustum();
  }

  override updateMatrixWorld(force?: boolean) {
    super.updateMatrixWorld(force);
    this.setFrustum();
  }

  private setFrustum(): void {
    if (!this.frustum) return;
    const matrix = new Matrix4().multiplyMatrices(
      this.projectionMatrix,
      this.matrixWorldInverse,
    );
    this.frustum.setFromProjectionMatrix(matrix);
  }

  /**
   * Check if object is inside camera frustum.
   */
  insideFrustum(object: Object3D): boolean {
    return this.frustum.intersectsObject(object);
  }

  getTexture(): Texture {
    return this.texture;
  }

  updateAspect(aspect: number): void {
    this.aspect = aspect;
    this.updateProjectionMatrix();
  }

  updateFOV(fov: number): void {
    this.fov = fov;
    this.updateProjectionMatrix();
  }

  /**
   * Create camera helper for debugging reasons.
   */
  createHelper(): CameraHelper {
    if (!this.helper) {
      this.helper = new CameraHelper(this);
    }

    return this.helper;
  }

  /**
   * Dispose texture and camera helper.
   */
  dispose(disposeTexture = true) {
    if (disposeTexture) {
      this.texture.dispose();
    }
    if (this.helper) {
      this.helper.geometry.dispose();
      (this.helper.material as LineBasicMaterial).dispose();
    }
  }
}
