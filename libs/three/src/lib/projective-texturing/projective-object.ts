import {
  Box3,
  DoubleSide,
  LineSegments,
  Material,
  Mesh,
  Object3D,
  Vector3,
} from 'three';
import { VirtualProjectorCamera } from './virtual-projector-camera';
import { ProjectiveMaterial } from './projective-material';

/**
 * Class that wraps an object/mesh and applies a {@link ProjectiveMaterial}
 * (unless `forceTextures` is `true`).
 */
export class ProjectiveObject extends Object3D {
  projectiveMaterial?: ProjectiveMaterial;
  boundingBox?: Box3;

  /**
   * Transform object/mesh into ProjectiveObject.
   */
  constructor(object: Object3D, forceTextures = false) {
    super();

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    this.copy(object, true);

    // apply projective material, unless the object's own material is supposed to be used
    if (!forceTextures) {
      this.projectiveMaterial = new ProjectiveMaterial([]);
    }

    this.traverse((child) => {
      if (child instanceof Mesh) {
        if (!forceTextures) {
          // set projective material
          child.material = this.projectiveMaterial;
        }
        // double-sided material is required for opacity checks of POIs
        (Array.isArray(child.material)
          ? child.material
          : [child.material]
        ).forEach((mat: Material) => (mat.side = DoubleSide));
      } else if (child instanceof LineSegments) {
        // set color of edges
        child.material.color.set(0x101010);
      }
    });
  }

  /**
   * Compute object's bounding box in local space.
   */
  computeBoundingBox(): void {
    this.boundingBox = new Box3();
    this.boundingBox.expandByObject(this);
    this.boundingBox.applyMatrix4(this.matrixWorld.clone().invert());
  }

  /**
   * Update object's material with new virtual projector cameras and their respective textures.
   * The cameras get filtered with respect to precomputed weights.
   * Only applies, if projective material has been set (see `forceTextures` flag).
   * @param cameras - Set of virtual projector cameras with textures.
   */
  update(cameras: VirtualProjectorCamera[]): void {
    if (!this.projectiveMaterial) return;

    if (!this.boundingBox) {
      this.computeBoundingBox();
    }

    // get center of mesh
    const center = this.boundingBox!.getCenter(new Vector3());
    center.applyMatrix4(this.matrixWorld);

    const meshes: Mesh[] = [];
    this.traverse((child) => {
      if (child instanceof Mesh) {
        meshes.push(child);
      }
    });

    // filter cameras that look into the object's direction
    const camerasFiltered = cameras.filter(
      (cam) =>
        cam.isEnabled() && meshes.some((mesh) => cam.insideFrustum(mesh)),
    );

    // sort cameras by distance to object center
    camerasFiltered.sort(
      (cam1, cam2) =>
        cam1.position.distanceTo(center) - cam2.position.distanceTo(center),
    );

    // // get cameras that see the mesh (has computed weight)
    // const camerasFiltered = cameras.filter(cam => cam.isEnabled() &&
    //   cam.userData.resource.objects.hasOwnProperty(this.userData.resource.id) &&
    //   cam.userData.resource.objects[this.userData.resource.id] > 0.005
    //   // meshes.some(mesh => cam.insideFrustum(mesh))
    // );

    // // sort cameras by weights, highest weight first
    // camerasFiltered.sort((cam1, cam2) => cam2.userData.resource.objects[this.userData.resource.id] - cam1.userData.resource.objects[this.userData.resource.id]);

    this.projectiveMaterial.updateCameras(camerasFiltered.slice(0, 6));
  }

  /**
   * Dispose all child elements.
   */
  dispose(): void {
    this.traverse((child) => {
      if (child instanceof Mesh || child instanceof LineSegments) {
        child.geometry.dispose();

        (Array.isArray(child.material)
          ? child.material
          : [child.material]
        ).forEach((mat) => {
          if (mat.map) {
            mat.map.dispose();
          }
          mat.dispose();
        });
      }
    });
  }
}
