import { Matrix4, RawShaderMaterial, Texture, Vector4 } from 'three';
import { VirtualProjectorCamera } from './virtual-projector-camera';
import * as ProjectiveShader from './projective-shader.glsl';
import { ProjectiveLightParameters } from './projective-light-parameters';

const maxSize = 6;

/**
 * Material that uses the projective texturing shader.
 * It takes the textures as well as `matrixWorldInverse` and `projectionMatrix`
 * of the assigned virtual projector cameras and passes them to the shader.
 *
 * For better compatibility especially with older devices, we keep the number of fragment uniforms
 * to a maximum of 64, and so the number of textures and camera matrices to a maximum of 6.
 *
 * Determining the number of used fragment uniforms:
 * - a 4x4 matrix claims 4 uniforms (we pass 2 matrices for each camera: 4 x 2 x 6 = 48 uniforms)
 * - each texture claims 1 uniform (1 x 6 = 6 uniforms)
 * - array of boolean enabled states (6 uniforms)
 * - other parameters including light intensity, light color, light position, and options (4 uniforms)
 * - results in 64 uniforms.
 */
export class ProjectiveMaterial extends RawShaderMaterial {
  cameras: VirtualProjectorCamera[] = [];

  constructor(cameras: VirtualProjectorCamera[] = []) {
    const cameraMatrix: Matrix4[] = [];
    const projMatrix: Matrix4[] = [];
    const texture: (Texture | null)[] = [];
    const enabled: boolean[] = [];

    for (let i = 0; i < maxSize; i++) {
      cameraMatrix.push(new Matrix4());
      projMatrix.push(new Matrix4());
      texture.push(null);
      enabled.push(false);
    }

    const uniforms = {
      LightPosition: { value: ProjectiveLightParameters.position },
      LightColor: { value: ProjectiveLightParameters.color },
      LightIntensity: { value: ProjectiveLightParameters.intensity },

      options: { value: new Vector4(1, 0, 0, 0) },

      cameraMatrix: { value: cameraMatrix },
      projMatrix: { value: projMatrix },
      texture: { type: 'tv', value: texture },
      enabled: { value: enabled },
    };

    super({
      uniforms,
      vertexShader: ProjectiveShader.vertex,
      fragmentShader: ProjectiveShader.fragment,
    });

    this.updateCameras(cameras);
  }

  /**
   * Update material according to cameras' `enabled` state.
   */
  updateMaterial(): void {
    for (let i = 0; i < maxSize; i++) {
      this.uniforms['enabled'].value[i] =
        this.cameras[i] !== undefined && this.cameras[i].isEnabled();
    }
  }

  /**
   * Update material and shader uniforms with new list of virtual projector cameras.
   */
  updateCameras(cameras: VirtualProjectorCamera[]): void {
    this.cameras = cameras;

    // const length = Math.min(cameras.length, WebGLCapabilities.limitedFragmentUniforms() ? 7 : 8);
    const length = Math.min(cameras.length, maxSize);

    for (let i = 0; i < length; i++) {
      this.uniforms['cameraMatrix'].value[i] = cameras[i].matrixWorldInverse;
      this.uniforms['projMatrix'].value[i] = cameras[i].projectionMatrix;
      this.uniforms['texture'].value[i] = cameras[i].getTexture();
    }

    this.updateMaterial();
  }
}
