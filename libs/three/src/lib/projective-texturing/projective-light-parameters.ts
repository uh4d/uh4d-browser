import { Color, Vector3 } from 'three';

export const ProjectiveLightParameters = {
  position: new Vector3(),
  color: new Color(),
  intensity: new Vector3(),
};
