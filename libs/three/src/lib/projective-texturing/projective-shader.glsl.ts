// language=GLSL
export const vertex = `
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 modelViewMatrix;
uniform mat3 normalMatrix;

uniform vec3 LightPosition;

attribute vec3 position;
attribute vec3 normal;

varying vec4 vPosition;
varying vec3 vNormal;
varying vec3 lightNormal;

void main()
{

  // transform vertex into world space
  vPosition = modelMatrix * vec4(position, 1.0);

  gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);

  // transform the normal's orientation into world space
  vNormal = vec3(modelMatrix * vec4(normal, 0.0));

  // normal of directional light in world space
  lightNormal = normalize(LightPosition.xyz);

}
`;

// language=GLSL
export const fragment = `
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif

//#ifdef LimitedNumUniforms
const vec3 Ambient = vec3(0.5, 0.5, 0.5);
const vec3 Diffuse = vec3(0.3, 0.3, 0.3);
//#else
//uniform vec3 Ambient;
//uniform vec3 Diffuse;
//#endif

uniform vec3 LightPosition;
uniform vec3 LightColor;
uniform vec3 LightIntensity;
// x : useDiffuseCompensation;
// y : blend Mode --> 0 = first sample, 1: add, 2: multiply, 3: divide
uniform vec4 options;

uniform sampler2D texture[6];
uniform mat4 cameraMatrix[6];
uniform mat4 projMatrix[6];
uniform bool enabled[6];

varying vec4 vPosition;
varying vec3 vNormal;
varying vec3 lightNormal;

struct camMeta {
  int index;
  vec2 uv;
  bool visible;
  float weight;
};

camMeta meta[6];

bool isInsideProjFrustum(vec4 p) {
  return abs(p.x) < p.w && abs(p.y) < p.w && abs(p.z) < p.w;
}

vec2 convertTexCoord(vec4 srcTexCoord) {
  vec3 ndc = srcTexCoord.xyz / srcTexCoord.w;
  return ndc.xy * .5 + .5;
  //  return srcTexCoord.xy / srcTexCoord.w / 2.0 + 0.5;
}

float SRGBToLinear(float c) {
    if (c < 0.04045) return c * 0.0773993808;
    else return pow(c * 0.9478672986 + 0.0521327014, 2.4);
}
float LinearToSRGB(float c) {
    if (c < 0.0031308) return c * 12.92;
    else return 1.055 * (pow(c, 0.41666)) - 0.055;
}

vec4 computeColor(vec4 srcColor, vec2 uv, sampler2D texture, float divSamples) {

  bool isEmpty = divSamples - 1.1 < 0.0;

  if (isEmpty)
  {
    srcColor = texture2D(texture, uv);
  }
  else
  {
    if (options.y < 1.1 && options.y > 0.9) // additive
    {
      srcColor += texture2D(texture, uv);
      srcColor = vec4(srcColor.x / divSamples, srcColor.y / divSamples, srcColor.z / divSamples, srcColor.w);
    }
    else if (options.y > 1.1)
    {
      vec4 colValue = texture2D(texture, uv);

      if (options.y < 2.1) // multiply
      {
        srcColor *= colValue;
      }
      else if (options.y < 3.1) // divide
      {
        srcColor *= 1.0/colValue;
      }
    }
  }

  return srcColor;

}

//bool doBlendTextures(vec2 uv, float camEnabled, float numSamples)
//{
//  if (options.y < 1.0 && numSamples > 0.0) {
//    return false;
//  }
//
//  return isInsideProjFrustum(uv) && camEnabled > 0.0;
//}

camMeta computeWeight(int index, bool enabled, mat4 projectionMat, mat4 cameraMat) {

  camMeta cmeta;

  cmeta.index = index;
  cmeta.weight = 0.0;
  cmeta.visible = enabled;

  //  meta.visible = options.y < 1.0 && enabled < 1.0;
  if (!cmeta.visible) {
    return cmeta;
  }

  // compute weights
  vec4 forward = vec4(0.0, 0.0, -1.0, 0.0);
  vec3 positionView = (cameraMat * vPosition).xyz;
  vec3 nPositionView = normalize(positionView);
  vec3 nNormalView = normalize((cameraMat * vec4(vNormal, 0.0)).xyz);

  // determine angle between camera position & vertex --> no projection "behind" camera
  float cosAnglePos = dot(nPositionView, forward.xyz);
  // determine angle between camera position & normal --> only projecting on normals looking towards camera
  float cosAngleNormal = -1.0 * dot(nPositionView, nNormalView);

  cmeta.visible = cosAnglePos > 0.0 && cosAngleNormal >= 0.15;
  if (!cmeta.visible) {
    return cmeta;
  }

  // compute uv
  vec4 projCoord = projectionMat * cameraMat * vPosition;
  cmeta.uv = convertTexCoord(projCoord);
  cmeta.visible = isInsideProjFrustum(projCoord);

  if (!cmeta.visible) {
    return cmeta;
  }

  // compute distance factor
  float distance = 1.0 - min(250.0, length(positionView)) / 250.0;

  cmeta.weight = cosAngleNormal + distance;

  return cmeta;

}

void main() {

  // compute meta and weights
  for (int i = 0; i < 6; i++) {
    meta[i] = computeWeight(i, enabled[i], projMatrix[i], cameraMatrix[i]);
  }

  // bubble sort by weight
  camMeta tmp;
  for (int i = 0; i < 6; i++) {
    for (int j = 0; j < 5; j++) {
      if (meta[j].weight < meta[j+1].weight) {
        tmp = meta[j+1];
        meta[j+1] = meta[j];
        meta[j] = tmp;
      }
    }
  }

  vec4 color = vec4(0,0,0,0);
  float divSamples = 0.0;

  for (int i = 0; i < 6; ++i) {
    // if (meta[i].visible && divSamples < 1.0) {
    if (meta[i].visible) {
      divSamples++;
      // cannot use dynamic index: Index expression must be constant
      if (meta[i].index == 0) {
        color = computeColor(color, meta[i].uv, texture[0], divSamples);
      } else if (meta[i].index == 1) {
        color = computeColor(color, meta[i].uv, texture[1], divSamples);
      } else if (meta[i].index == 2) {
        color = computeColor(color, meta[i].uv, texture[2], divSamples);
      } else if (meta[i].index == 3) {
        color = computeColor(color, meta[i].uv, texture[3], divSamples);
      } else if (meta[i].index == 4) {
        color = computeColor(color, meta[i].uv, texture[4], divSamples);
      } else if (meta[i].index == 5) {
        color = computeColor(color, meta[i].uv, texture[5], divSamples);
      }
      break;
    }
  }

  //  bool doBlend = doBlendTextures(uv1, cameraEnabled14.x, divSamples);
  //
  //  if (doBlend) {
  //    divSamples++;
  //    color = computeColor(color, uv1, texture1, divSamples);
  //  }


  // Lambertian lighting model
  vec3 ambientIntensity = vec3(LightIntensity.x);
  vec3 diffuseIntensity = vec3(LightIntensity.y);

  vec3 ambient = (color.w > 0.0) ? color.xyz : Ambient;
  ambient = ambient * ambientIntensity;

  float diffFactor = max(dot(lightNormal, vNormal), 0.0);
  vec3 diffuse = (options.x > 0.0 && color.w > 0.0) ? color.xyz : Diffuse;
  diffuse = diffuse * diffuseIntensity * diffFactor * LightColor;

  vec3 lighting = min(ambient + diffuse, vec3(1.0));

  gl_FragColor = vec4(LinearToSRGB(lighting.x), LinearToSRGB(lighting.y), LinearToSRGB(lighting.z), 1.0);

}
`;
