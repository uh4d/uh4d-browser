import {
  Color,
  LineSegments,
  Material,
  Mesh,
  MeshStandardMaterial,
  Object3D,
  SRGBColorSpace,
} from 'three';
import { Cache, Settings, ShadingKey } from '@uh4d/frontend/viewport3d-cache';
import { ViewportDefaults } from '@uh4d/config';
import { StandardMaterial } from '../manager/material-manager';
import { GenericItem } from './generic-item';

/**
 * Extended GenericItem class for 3D objects.
 */
export class ObjectItem extends GenericItem {
  edges: LineSegments[] = [];

  constructor(object: Object3D, label: string) {
    super(object, label);

    let count = 0;
    object.traverse((child) => {
      if (child instanceof Mesh) {
        child.geometry.name = `geo_${count++}`;
        Cache.geometries.assign(child.geometry, object.name);

        const handleMaterial = (mat: MeshStandardMaterial): Material => {
          mat.color.convertSRGBToLinear();
          if (mat.map) {
            mat.map.colorSpace = SRGBColorSpace;
          }
          return Cache.materials.assign(mat, object.name);
        };

        if (Array.isArray(child.material)) {
          child.material = child.material.map(handleMaterial);
        } else {
          child.material = handleMaterial(child.material);
        }

        child.userData['originalMat'] = child.material;
      } else if (child instanceof LineSegments) {
        child.geometry.name = `geo_${count++}`;
        Cache.geometries.assign(child.geometry, object.name);

        child.material.dispose();
        child.material = Cache.materials.get('edgesMat');
        this.edges.push(child);
      }
    });
  }

  /**
   * Assign corresponding edges object.
   */
  setEdges(edges: LineSegments) {
    // this.edges = edges;
  }

  /**
   * Activate/select the item.
   * @param active - If not set, `active` property will be inverted.
   */
  override select(active?: boolean) {
    if (this.active === active) return;
    super.select(active);

    if (Settings.objects.shading === ShadingKey.XRay) {
      const mat = Cache.materials.get(
        this.highlighted ? 'xraySelectionMat' : 'xrayMat',
      );
      this.object.traverse((child) => {
        if (child instanceof Mesh) {
          child.material = mat;
        }
      });
    }

    this.edges.forEach((edge) => {
      edge.material = Cache.materials.get(
        this.active ? 'edgesSelectionMat' : 'edgesMat',
      )!;
    });
  }

  /**
   * Highlight item.
   * @param highlighted - If not set, `highlighted` property will be inverted.
   */
  override highlight(highlighted?: boolean) {
    if (this.highlighted === highlighted) return;
    super.highlight(highlighted);

    if (this.active && Settings.objects.shading === ShadingKey.XRay) {
      // no highlighting when active and xray shader
      return;
    } else if (Settings.objects.shading === ShadingKey.Color) {
      // copy original material and lerp color
      const hColor = new Color(ViewportDefaults.highlightColor);
      const handleMaterial = (mat: Material): Material => {
        const hMat = mat.clone() as StandardMaterial;
        hMat.color.lerp(hColor, 0.3);
        return hMat;
      };

      this.object.traverse((child) => {
        if (child instanceof Mesh) {
          if (this.highlighted) {
            if (Array.isArray(child.material)) {
              child.material = child.material.map(handleMaterial);
            } else {
              child.material = handleMaterial(child.material);
            }
          } else {
            if (child.material !== this.object.userData['originalMat']) {
              (Array.isArray(child.material)
                ? child.material
                : [child.material]
              ).forEach((mat) => {
                mat.dispose();
              });
            }
            child.material = child.userData['originalMat'];
          }
        }
      });
    } else {
      // apply predefined materials
      let mat: Material;

      switch (Settings.objects.shading) {
        case ShadingKey.Transparent:
          mat = Cache.materials.get(
            this.highlighted ? 'transparentHighlightMat' : 'transparentMat',
          )!;
          break;
        case ShadingKey.XRay:
          mat = Cache.materials.get(
            this.highlighted ? 'xrayHighlightMat' : 'xrayMat',
          )!;
          break;
        case ShadingKey.Projective:
          mat = Cache.materials.get('projectiveMat')!;
          break;
        default:
          mat = Cache.materials.get(
            this.highlighted ? 'defaultHighlightMat' : 'defaultMat',
          )!;
      }

      this.object.traverse((child) => {
        if (child instanceof Mesh) {
          child.material = mat;
        }
      });
    }
  }

  /**
   * Apply shading/materials according to current viewport settings.
   */
  applyShading() {
    const hideEdges =
      !Settings.objects.showEdges ||
      Settings.objects.shading === ShadingKey.XRay;

    this.edges.forEach((edge) => {
      if (hideEdges) edge.layers.disable(0);
      else edge.layers.enable(0);
    });

    if (Settings.objects.shading === ShadingKey.Color) {
      this.object.traverse((child) => {
        if (child instanceof Mesh) {
          child.material = child.userData['originalMat'];
        }
      });
    } else {
      let mat: Material;

      switch (Settings.objects.shading) {
        case ShadingKey.Transparent:
          mat = Cache.materials.get('transparentMat')!;
          break;
        case ShadingKey.XRay:
          mat = Cache.materials.get(
            this.active ? 'xraySelectionMat' : 'xrayMat',
          )!;
          break;
        case ShadingKey.Projective:
          mat = Cache.materials.get('projectiveMat')!;
          break;
        default:
          mat = Cache.materials.get('defaultMat')!;
      }

      this.object.traverse((obj) => {
        if (obj instanceof Mesh) {
          obj.material = mat;
        }
      });
    }
  }

  /**
   * Remove any references to meshes and other items, so this item is ready for GC.
   * @param disposeObject - If true (default), object will get disposed too.
   */
  override dispose(disposeObject = true) {
    if (disposeObject) {
      this.object.traverse((child) => {
        if (child instanceof Mesh || child instanceof LineSegments) {
          Cache.geometries.remove(child.geometry);
          Cache.materials.remove(child.material);
        }
      });
    }

    super.dispose();
    this.edges.length = 0;
  }
}
