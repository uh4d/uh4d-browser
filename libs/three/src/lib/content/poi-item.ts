import { GenericItem } from './generic-item';
import { PointOfInterest } from '../point-of-interest';

export class PoiItem extends GenericItem {
  override object: PointOfInterest;

  constructor(obj: PointOfInterest, label?: string) {
    super(obj, label);
  }

  override highlight(highlighted?: boolean) {
    if (this.active) return;

    super.highlight(highlighted);
    this.object.highlight(this.highlighted);
  }

  override select(active?: boolean) {
    super.select(active);
    this.object.select(this.active);
  }

  override dispose() {
    this.object.dispose();
    super.dispose();
  }
}
