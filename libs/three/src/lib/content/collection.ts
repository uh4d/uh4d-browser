import { Event, MathUtils } from 'three';
import { Subject } from 'rxjs';
import { GenericItem } from './generic-item';

/**
 * This class is a container for objects, plans, or images. There are properties that are useful for global settings.
 */
export class Collection<T extends GenericItem> {
  /**
   * Array containing all entries
   */
  private list: T[] = [];

  /**
   * Amount of objects in this collection.
   */
  private count = 0;

  private visible = true;

  /**
   * Global opacity value.
   */
  private opacity = 1.0;

  public toggle$ = new Subject<{ target: T; visible: boolean }>();
  public focus$ = new Subject<T>();
  public update$ = new Subject<T>();

  /**
   * Get an entry by the given id. If no id is specified, the whole list will be returned.
   */
  get(): T[];
  get(id: string | number): T;
  get(id?: string | number) {
    if (id) {
      return this.getByProperty('id', id);
    } else {
      return [...this.list];
    }
  }

  /**
   * Get item by name.
   */
  getByName(value: string) {
    return this.getByProperty('name', value);
  }

  /**
   * Get item by property.
   */
  getByProperty(prop: string, value: any) {
    return this.list.find((item) => item[prop] === value);
  }

  /**
   * Add item to the collection (uses `item.id` as id).
   */
  add(item: T) {
    if (this.get(item.id)) {
      return;
    }

    this.list.push(item);

    item.addEventListener('toggle', this.toggleHandler.bind(this));
    item.addEventListener('focus', this.focusHandler.bind(this));
    item.addEventListener('change', this.updateHandler.bind(this));

    this.count++;
  }

  /**
   * Remove item from the collection.
   */
  remove(item: T) {
    const index = this.list.indexOf(item);
    if (index !== -1) {
      this.list.splice(index, 1);

      item.removeEventListener('toggle', this.toggleHandler);
      item.removeEventListener('focus', this.focusHandler);
      item.removeEventListener('change', this.updateHandler);

      this.count--;
    }
  }

  /**
   * Iterate over the list and execute the callback function for each item.
   */
  forEach(callback: (item: T) => void, onlyVisible = false) {
    this.list.forEach((item) => {
      if (!onlyVisible || (onlyVisible && item.visible)) {
        callback(item);
      }
    });
  }

  /**
   * Set opacity of all items.
   */
  setOpacity(value: number) {
    this.opacity = MathUtils.clamp(value, 0, 1);
    this.list.forEach((item) => {
      item.setOpacity(this.opacity);
    });
  }

  private toggleHandler(event: Event) {
    if (event['visible']) {
      this.visible = true;
    }
    this.toggle$.next({ target: event.target as T, visible: event['visible'] });
  }

  private focusHandler(event: Event) {
    this.focus$.next(event.target as T);
  }

  private updateHandler(event: Event) {
    this.update$.next(event.target as T);
  }
}
