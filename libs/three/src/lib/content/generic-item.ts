/* eslint-disable @typescript-eslint/ban-types */
import { EventDispatcher, MathUtils, Object3D } from 'three';

export interface GenericItemEventMap {
  change: {};
  focus: {};
  toggle: { visible: boolean };
}

/**
 * Base class for items to be added to a Collection
 */
export class GenericItem extends EventDispatcher<GenericItemEventMap> {
  id: string | number;
  name: string;
  label: string;
  object: Object3D;
  visible = false;
  active = false;
  highlighted = false;
  opacity = 1.0;

  constructor(object: Object3D, label?: string) {
    super();
    this.id = object.id;
    this.name = object.name;
    this.label = label || object.name;
    this.object = object;
    this.object.userData['item'] = this;
  }

  /**
   * Toggle object in scene.
   * @param visible - Object will be toggled depending on this value. If not set, the visibility will be inverted.
   */
  toggle(visible?: boolean) {
    if (typeof visible === 'boolean') {
      this.visible = visible;
    } else {
      this.visible = !this.visible;
    }

    this.dispatchEvent({ type: 'toggle', visible: this.visible });
  }

  /**
   * Set opacity of item.
   */
  setOpacity(value: number) {
    this.opacity = MathUtils.clamp(value, 0, 1);
  }

  /**
   * Activate/select the item and dispatch `select` event.
   * @param active - If not set, `active` property will be inverted.
   */
  select(active?: boolean) {
    if (typeof active === 'boolean') {
      this.active = active;
    } else {
      this.active = !this.active;
    }
  }

  /**
   * Highlight item.
   * @param highlighted - If not set, `highlighted` property will be inverted.
   */
  highlight(highlighted?: boolean) {
    if (typeof highlighted === 'boolean') {
      this.highlighted = highlighted;
    } else {
      this.highlighted = !this.highlighted;
    }
  }

  /**
   * Set camera to fit the object. `focus` event is being dispatched.
   */
  focus() {
    this.dispatchEvent({ type: 'focus' });
  }

  update() {
    this.dispatchEvent({ type: 'change' });
  }

  /**
   * Remove any references to meshes and other items, so this item is ready for GC.
   */
  dispose() {
    delete this.object.userData['item'];
    delete this.object;
  }
}
