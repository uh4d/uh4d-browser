import { BufferGeometry, Float32BufferAttribute } from 'three';

/**
 * Create 2D box-shaped lines geometry for LineLoop object.
 */
export function createBox2Lines(width: number, height: number) {
  const positions = [
    -width / 2,
    height / 2,
    0,
    width / 2,
    height / 2,
    0,
    width / 2,
    -height / 2,
    0,
    -width / 2,
    -height / 2,
    0,
  ];

  const lineGeometry = new BufferGeometry();
  lineGeometry.setAttribute(
    'position',
    new Float32BufferAttribute(positions, 3),
  );

  return lineGeometry;
}

/**
 * Create 2D circle-shaped line geometry for LineLoop object.
 */
export function createCircleLines(radius: number, segments = 8) {
  const positions: number[] = [];
  for (let s = 0; s < segments; s++) {
    const segment = (s / segments) * Math.PI * 2;
    positions.push(radius * Math.cos(segment), radius * Math.sin(segment), 0);
  }

  const lineGeometry = new BufferGeometry();
  lineGeometry.setAttribute(
    'position',
    new Float32BufferAttribute(positions, 3),
  );

  return lineGeometry;
}
