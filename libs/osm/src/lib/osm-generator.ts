import {
  Box2,
  BufferGeometry,
  EdgesGeometry,
  ExtrudeGeometry,
  Group,
  LineBasicMaterial,
  LineSegments,
  Mesh,
  MeshBasicMaterial,
  Object3D,
  Path,
  Raycaster,
  Shape,
  Vector2,
  Vector3,
} from 'three';
import * as BufferGeometryUtils from 'three-addons/utils/BufferGeometryUtils.js';
import {
  OsmNode,
  OsmRelation,
  OsmTags,
  OsmWay,
} from '@uh4d/dto/interfaces/custom/osm';
import { Coordinates } from '@uh4d/utils';
import { getOSMBuildingHeight } from './osm-building-heights';

type BasicConsole = Pick<typeof console, 'log' | 'warn' | 'error' | 'debug'>;

interface OSMGeneratorOptions {
  includeEdges?: boolean;
  logger?: BasicConsole;
}

/**
 * Class to generate and organize 3D objects retrieved from OpenStreetMap (via Overpass API).
 */
export class OSMGenerator {
  private readonly logger: BasicConsole = console;

  private nodes: OsmNode[] = [];
  private ways: OsmWay[] = [];
  private relations: OsmRelation[] = [];

  private readonly origin: Coordinates;
  private readonly includeEdges: boolean = false;

  group = new Group();
  buildings: Object3D[] = [];

  constructor(origin: Coordinates, options: OSMGeneratorOptions = {}) {
    this.origin = origin;
    this.includeEdges = options.includeEdges || false;
    if (options.logger) this.logger = options.logger;
  }

  /**
   * Generate buildings from data from Overpass API.
   */
  async generate(data: {
    elements: (OsmNode | OsmWay | OsmRelation)[];
  }): Promise<void> {
    this.dispose();

    // split different types of elements
    this.nodes = data.elements.filter((e): e is OsmNode => e.type === 'node');
    this.ways = data.elements.filter((e): e is OsmWay => e.type === 'way');
    this.relations = data.elements.filter(
      (e): e is OsmRelation => e.type === 'relation',
    );

    const buildings: Object3D[] = [];
    const processedIds: number[] = [];

    // process relations
    for (const rel of this.relations) {
      const existingBuilding = this.buildings.find(
        (obj) => obj.userData['osmId'] === rel.id,
      );
      if (existingBuilding) {
        buildings.push(existingBuilding);
        continue;
      }

      rel.members.sort((a, b) => {
        if (a.role === 'outer') return -1;
        if (a.role === 'inner') return 1;
        return 0;
      });

      const parts: { shape: Shape; tags?: OsmTags }[] = [];
      for (const m of rel.members) {
        const way = this.ways.find((w) => w.id === m.ref);
        if (way) {
          switch (m.role) {
            case 'outer':
            case 'part':
            case '':
              parts.push({
                shape: this.getShape(way),
                tags: way.tags || rel.tags,
              });
              break;
            case 'inner':
              parts[parts.length - 1].shape.holes.push(this.getPath(way));
          }
          processedIds.push(way.id);
        }
      }

      const group = this.generateBuilding(parts);
      group.userData['osmId'] = rel.id;
      group.children[0].name = rel.id.toString();
      buildings.push(group);
    }

    // process ways
    for (const way of this.ways) {
      if (processedIds.includes(way.id) || !way.tags) continue;

      const existingBuilding = this.buildings.find(
        (obj) => obj.userData['osmId'] === way.id,
      );
      if (existingBuilding) {
        buildings.push(existingBuilding);
        continue;
      }

      const shape = this.getShape(way);

      const group = this.generateBuilding([{ shape, tags: way.tags }]);
      group.userData['osmId'] = way.id;
      group.children[0].name = way.id.toString();
      buildings.push(group);
    }

    // dispose obsolete buildings
    const obsoleteBuildings = this.buildings.filter(
      (obj) => !buildings.includes(obj),
    );
    for (const obj of obsoleteBuildings) {
      this.group.remove(obj);
      obj.traverse((child) => {
        if (child instanceof Mesh || child instanceof LineSegments) {
          child.geometry.dispose();
        }
      });
    }

    this.buildings = buildings;
  }

  private getShape(way: OsmWay) {
    return new Shape(this.getVertices(way.nodes));
  }

  private getPath(way: OsmWay) {
    const vertices = this.getVertices(way.nodes);
    if (this.computePolygonArea(vertices) < 0) vertices.reverse();
    return new Path(vertices);
  }

  private getVertices(nodes: number[]): Vector2[] {
    const points: Vector2[] = [];

    for (const nodeId of nodes) {
      const node = this.nodes.find((n) => n.id === nodeId);
      if (node) {
        const p = new Coordinates(node.lat, node.lon, 0).toCartesian(
          this.origin,
        );
        points.push(new Vector2(p.x, -p.z));
      }
    }

    return points;
  }

  private computePolygonArea(vertices: Vector2[]) {
    let area = 0;
    for (let i = 0; i < vertices.length; i++) {
      const j = (i + 1) % vertices.length;
      area += vertices[i].x * vertices[j].y;
      area -= vertices[j].x * vertices[i].y;
    }
    return area / 2;
  }

  /**
   * Extrude 3D geometry from shape and generate edges.
   * @private
   */
  private generateBuilding(
    parts: { shape: Shape; tags?: OsmTags }[],
  ): Object3D {
    const bbox = new Box2();
    const extrudeGeos: BufferGeometry[] = [];
    const edgesGeos: BufferGeometry[] = [];

    parts.map(({ shape, tags }) => {
      shape.getPoints().forEach((p) => {
        bbox.expandByPoint(p);
      });

      const geo = new ExtrudeGeometry(shape, {
        depth: getOSMBuildingHeight(bbox, tags),
        bevelEnabled: false,
      });
      geo.rotateX(-Math.PI / 2);
      extrudeGeos.push(geo);

      if (this.includeEdges) {
        edgesGeos.push(new EdgesGeometry(geo, 24));
      }
    });

    const mesh = new Mesh(
      BufferGeometryUtils.mergeGeometries(extrudeGeos),
      new MeshBasicMaterial(),
    );

    const group = new Group();
    group.add(mesh);

    if (this.includeEdges) {
      const edges = new LineSegments(
        BufferGeometryUtils.mergeGeometries(edgesGeos),
        new LineBasicMaterial(),
      );
      group.add(edges);
    }

    group.userData['points'] = parts.map(({ shape }) => shape.getPoints());
    group.userData['boundingBox'] = bbox;

    return group;
  }

  /**
   * Update visibility and position according to terrain and existing objects.
   */
  update(terrain: Object3D): void {
    this.group.remove(...this.buildings);

    const raycaster = new Raycaster();
    const dir = new Vector3(0, -1, 0);

    const startTime = Date.now();

    for (const building of this.buildings) {
      const bbox: Box2 = building.userData['boundingBox'];
      const points = [
        bbox.min,
        new Vector2(bbox.min.x, bbox.max.y),
        bbox.max,
        new Vector2(bbox.max.x, bbox.min.y),
      ];

      let height = Number.MAX_VALUE;

      for (const point of points) {
        raycaster.set(new Vector3(point.x, 10000, -point.y), dir);
        const intersections = raycaster.intersectObject(terrain, true);

        if (intersections[0]) {
          height = Math.min(height, intersections[0].point.y);
        }
      }

      building.position.setY(height === Number.MAX_VALUE ? 0 : height);

      this.group.add(building);
    }

    this.logger.log(
      `OSM Generator - Elapsed time: ${(Date.now() - startTime) / 1000}`,
    );
  }

  /**
   * Dispose all generated objects.
   */
  dispose(): void {
    for (const obj of this.buildings) {
      this.group.remove(obj);
      obj.traverse((child) => {
        if (child instanceof Mesh || child instanceof LineSegments) {
          child.geometry.dispose();
          const mats = Array.isArray(child.material)
            ? child.material
            : [child.material];
          mats.forEach((mat) => {
            if (mat.map) mat.map.dispose();
            mat.dispose();
          });
        }
      });
    }
  }
}
