Documentation of graph database schema using CIDOC CRM as base ontology.

[draw.io](https://www.drawio.com/) is used for viewing and editing. If exported to SVG, the schemas can be embedded into the API documentation.
