# TTS (text-to-speech) service

Service to convert text to speech using open-source model.

- https://huggingface.co/coqui/XTTS-v2
- https://huggingface.co/spaces/coqui/xtts
- https://coqui-tts.readthedocs.io/en/latest/models/xtts.html#inference
