import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { TextToSpeechService } from './text-to-speech.service';
import { TextToSpeechPayload } from './dto/text-to-speech.payload';

@Controller('text-to-speech')
export class TextToSpeechController {
  constructor(private readonly ttsService: TextToSpeechService) {}

  @MessagePattern('tts-mp3')
  async textToSpeech(@Payload() data: TextToSpeechPayload) {
    return this.ttsService.addToQueue(data.text, data.targetFile);
  }
}
