import { IsNotEmpty, IsString } from 'class-validator';

export class TextToSpeechPayload {
  @IsNotEmpty()
  @IsString()
  text: string;

  @IsNotEmpty()
  @IsString()
  targetFile: string;
}
