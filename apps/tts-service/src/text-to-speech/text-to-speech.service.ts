import { Injectable, Logger } from '@nestjs/common';
import { join } from 'path';
import * as tmp from 'tmp-promise';
import markdownToTxt from 'markdown-to-txt';
import * as franc from 'franc-min';
import { move } from 'fs-extra';
import * as langCodes from 'langs';
import { concatMap, Subject } from 'rxjs';
import { spawnAndLog } from '@uh4d/backend/utils';
import { TtsConfig } from '../tts-config/tts.config';

interface TtsQueueItem {
  text: string;
  targetFile: string;
  resolve: (value: string) => void;
  reject: (err: any) => void;
}

@Injectable()
export class TextToSpeechService {
  private readonly logger = new Logger(TextToSpeechService.name);

  private queue = new Subject<TtsQueueItem>();

  constructor(private readonly config: TtsConfig) {
    // queue to process only one request at a time
    this.queue
      .pipe(
        concatMap(async ({ text, targetFile, resolve, reject }) =>
          this.generateAudioFromText(text, targetFile)
            .then((result) => {
              resolve(result);
              return result;
            })
            .catch((err) => {
              reject(err);
              throw err;
            }),
        ),
      )
      .subscribe({
        next: (result) => {
          this.logger.log(`Generated ${result}`);
        },
        error: (err) => {
          this.logger.error(err);
        },
      });
  }

  addToQueue(text: string, targetFile: string): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      this.queue.next({ text, targetFile, resolve, reject });
    });
  }

  private async generateAudioFromText(text: string, targetFile: string) {
    // convert markdown to text
    const plainText = markdownToTxt(text);

    // detect language
    const detectedLang = langCodes.where(2, franc(plainText));
    this.logger.debug('Detected language: ' + detectedLang['1']);

    const tmpDir = await tmp.dir({ unsafeCleanup: true });
    const outputWav = join(tmpDir.path, 'output.wav');
    const outputMp3 = join(tmpDir.path, 'output.mp3');

    try {
      // TTS
      await this.convertTextToWav(plainText, detectedLang['1'], outputWav);
      await this.convertWavToMp3(outputWav, outputMp3);

      // copy final mp3 file
      await move(outputMp3, targetFile, { overwrite: true });

      return targetFile;
    } finally {
      await tmpDir.cleanup();
    }
  }

  /**
   * Generate speech/audio from text using TTS.
   * @param text Input text
   * @param lang Language code (2 characters, ISO 639-1)
   * @param outputPath Path to output WAV file
   */
  private async convertTextToWav(
    text: string,
    lang: string,
    outputPath: string,
  ): Promise<void> {
    await spawnAndLog(
      this.config.binTTS,
      [
        '--model_path',
        this.config.ttsModelPath,
        '--config_path',
        join(this.config.ttsModelPath, 'config.json'),
        '--speaker_wav',
        join(__dirname, 'assets/female.wav'),
        '--language_idx',
        lang,
        '--text',
        text,
        '--out_path',
        outputPath,
      ],
      this.logger,
    );
  }

  /**
   * Convert WAV file to MP3 file.
   * @param wavPath Path to source WAV file
   * @param mp3Path Path to target MP3 file
   */
  private async convertWavToMp3(
    wavPath: string,
    mp3Path: string,
  ): Promise<void> {
    await spawnAndLog(
      this.config.binFFMPEG,
      [
        '-i',
        wavPath,
        '-vn',
        '-acodec',
        'libmp3lame',
        '-ar',
        '44100',
        '-ac',
        '2',
        '-b:a',
        '128k',
        mp3Path,
        '-loglevel',
        'error',
      ],
      this.logger,
    );
  }
}
