import { Module } from '@nestjs/common';
import { TtsConfigModule } from '../tts-config/tts-config.module';
import { TextToSpeechController } from './text-to-speech.controller';
import { TextToSpeechService } from './text-to-speech.service';

@Module({
  imports: [TtsConfigModule],
  controllers: [TextToSpeechController],
  providers: [TextToSpeechService],
})
export class TextToSpeechModule {}
