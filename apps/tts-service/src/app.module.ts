import { Module } from '@nestjs/common';
import { TextToSpeechModule } from './text-to-speech/text-to-speech.module';
import { TtsConfigModule } from './tts-config/tts-config.module';

@Module({
  imports: [TtsConfigModule, TextToSpeechModule],
})
export class AppModule {}
