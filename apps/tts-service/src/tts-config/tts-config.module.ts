import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TtsConfig } from './tts.config';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      expandVariables: true,
    }),
  ],
  providers: [TtsConfig],
  exports: [ConfigModule, TtsConfig],
})
export class TtsConfigModule {}
