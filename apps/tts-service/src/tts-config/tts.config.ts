import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class TtsConfig {
  constructor(private readonly config: ConfigService) {}

  /**
   * Path to data directory.
   */
  get dirData() {
    return this.config.get<string>('DIR_DATA');
  }

  /**
   * Base folder of TTS model.
   */
  get ttsModelPath() {
    return this.config.get<string>('TTS_MODEL_PATH');
  }

  /**
   * `tts` executable.
   */
  get binTTS() {
    return this.config.get<string>('BIN_TTS');
  }

  /**
   * `ffmpeg` executable.
   */
  get binFFMPEG() {
    return this.config.get<string>('BIN_FFMPEG');
  }
}
