import argparse
import json
import os
import cv2
import numpy as np

ap = argparse.ArgumentParser(
    description='Find contours for each object in color-coded render image'
)
ap.add_argument('-i', '--image', type=str, required=True,
                help='Color-coded render image')
ap.add_argument('-j', '--json', type=str, required=True,
                help='JSON file with metadata')
ap.add_argument('-e', '--epsilon', type=float, default=3,
                help='Contour approximation accuracy')
ap.add_argument('-a', '--max-area', type=float, default=200,
                help='If the contour area is smaller than this threshold, the contour gets get skipped.')
ap.add_argument('-d', '--debug', action='store_true',
                help='Save mask with contours as image for each object next to the JSON file for debugging.')
args = vars(ap.parse_args())

# read json file
with open(args['json'], 'r') as infile:
    json_object = json.load(infile)

# read render image
image = cv2.imread(args['image'])

# iterate over objects
for item in json_object:
    # mask object by color
    color = tuple(int(item['color'][i:i+2], 16) for i in (0, 2, 4))
    lower = np.flip(np.array(color))
    upper = np.flip(np.array(color))
    mask = cv2.inRange(image, lower, upper)

    # find contours in mask
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    mask_img = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
    approxList = []
    for c in contours:
        # store approximated contours for object
        area = cv2.contourArea(c)
        approx = cv2.approxPolyDP(c, args['epsilon'], True)

        if area < args['max_area']:
            continue

        if args['debug']:
            cv2.drawContours(mask_img, [approx], -1, (0, 255, 0), 1)
        approxList.append(approx.reshape((-1, 2)).tolist())

    if args['debug']:
        mask_img_path = os.path.join(os.path.dirname(args['json']), item['id'] + '.png')
        cv2.imwrite(mask_img_path, mask_img)
    item['contours'] = approxList

# save json
with open(args['json'], 'w') as outfile:
    outfile.write(json.dumps(json_object, indent=2))
