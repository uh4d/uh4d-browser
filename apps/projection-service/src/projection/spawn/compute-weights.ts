import * as process from 'process';
import { program } from 'commander';
import 'reflect-metadata';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { Logger } from '@nestjs/common';
import { DoubleSide, Mesh, MeshDepthMaterial } from 'three';
import { ObjectWeights } from '@uh4d/dto/interfaces/custom/linked-objects';
import { ProjectionPayloadDto } from '../dto/projection-payload.dto';
import { ColorCodedScene } from './color-coded-scene';

program
  .name('compute-weights')
  .description('Load scene and compute weights.')
  .option('-o, --output-color <string>', 'save color render image to file')
  .option('-d, --output-depth <string>', 'save depth render image to file');
program.parse();

const opts = program.opts();
const logger = new Logger('ChildProcess compute-weights');

process.on('message', async (data) => {
  // start process with incoming message
  try {
    await computeWeights(data);
    process.exit();
  } catch (reason) {
    logger.error(reason);
    process.exit(1);
  }
});

async function computeWeights(payload: unknown) {
  // validate payload
  const data = plainToInstance(ProjectionPayloadDto, payload);
  const validationErrors = await validate(data);
  if (validationErrors.length) {
    throw new Error('Payload has wrong format');
  }

  const scene = new ColorCodedScene<{ coverage: number; distance: number }>();
  scene.setInitialValues({ coverage: 0, distance: 0 });
  scene.setCamera(data.image);

  // load terrain
  if (data.terrain) {
    await scene.loadObject(data.terrain, true);
  }

  // load objects
  await Promise.all(
    data.objects.map((objData) => scene.loadObject(objData, false, true)),
  );

  // render color image
  scene.colorMap.forEach(({ object, material }) => {
    object.traverse((child) => {
      if (child instanceof Mesh) {
        child.material = material;
      }
    });
  });

  const colorImage = scene.render(data.image.width, data.image.height);

  if (opts.outputColor) {
    await colorImage.toFile(opts.outputColor);
  }
  const { data: colorData, info } = await colorImage
    .raw()
    .toBuffer({ resolveWithObject: true });

  // render depth image
  const depthMat = new MeshDepthMaterial({ side: DoubleSide });
  scene.traverse((child) => {
    if (child instanceof Mesh) {
      child.material = depthMat;
    }
  });

  const depthImage = scene.render(data.image.width, data.image.height);

  if (opts.outputDepth) {
    await depthImage.toFile(opts.outputDepth);
  }
  const { data: depthData } = await depthImage
    .raw()
    .toBuffer({ resolveWithObject: true });

  // compute weights
  for (
    let i = 0, l = info.width * info.height * info.channels;
    i < l;
    i += info.channels
  ) {
    const r = colorData[i];
    const g = colorData[i + 1];
    const b = colorData[i + 2];

    if (r === 0 && g === 0 && b === 0) continue;

    const key = [r, g, b].reduce(
      (acc, curr) => acc + ('00' + curr.toString(16)).slice(-2),
      '',
    );
    const entry = scene.colorMap.get(key);
    if (entry) {
      entry.coverage += 1;
      entry.distance += depthData[i] / 255;
    } else {
      logger.error(`Unknown color key ${key}`);
    }
  }

  const objectWeights: Record<string, ObjectWeights> = {};
  for (const [key, value] of scene.colorMap) {
    if (key !== '000000') {
      const coverageWeight = value.coverage / (info.width * info.height);
      const distanceWeight = value.distance / value.coverage || 0;
      objectWeights[value.objectId] = {
        coverageWeight,
        distanceWeight,
        weight: coverageWeight * distanceWeight,
      };
    }
  }

  // send to parent process
  process.send(objectWeights);
}
