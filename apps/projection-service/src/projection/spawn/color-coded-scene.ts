import {
  Color,
  DoubleSide,
  LineSegments,
  MathUtils,
  Matrix4,
  MeshBasicMaterial,
  Object3D,
} from 'three';
import { loadGltf } from 'node-three-gltf';
import { random } from '@uh4d/backend/utils';
import { ServerSideScene } from './server-side-scene';
import {
  ProjectionSourceImage,
  ProjectionSourceObject,
} from '../dto/projection-payload.dto';

interface ColorMapBase {
  color: Color;
  objectId: string;
  object: Object3D;
  depth: number;
  material: MeshBasicMaterial;
}

/**
 * Traverse object while also consuming the depth within the object hierarchy.
 */
const traverseObject = (
  child: Object3D,
  depth: number,
  cb: (child: Object3D, depth: number) => void,
): void => {
  cb(child, depth);
  child.children.forEach((c) => {
    traverseObject(c, depth + 1, cb);
  });
};

export class ColorCodedScene<
  T = Record<string, never>,
> extends ServerSideScene {
  public readonly colorMap = new Map<string, ColorMapBase & T>();
  public readonly objects: Object3D[] = [];
  private initialValues: T | null = null;

  /**
   * Set values for additional colorMap properties each instance will get initialized with.
   */
  setInitialValues(values: T): void {
    this.initialValues = values;
  }

  /**
   * Set camera from image data.
   */
  setCamera(data: ProjectionSourceImage): void {
    this.camera.fov = 2 * Math.atan(1 / (2 * data.ck)) * MathUtils.RAD2DEG;
    this.camera.updateProjectionMatrix();
    const matrix = new Matrix4().fromArray(data.matrix);
    this.camera.applyMatrix4(matrix);
    this.camera.matrixAutoUpdate = false;
  }

  async loadObject(
    data: ProjectionSourceObject,
    hiddenObject = false,
    subObjectWise = false,
  ): Promise<Object3D> {
    const gltf = await loadGltf(data.file);
    const obj = gltf.scene.children[0];

    const matrix = new Matrix4().fromArray(data.matrix);
    obj.applyMatrix4(matrix);
    obj.matrixAutoUpdate = false;

    // remove line segments
    const toBeRemoved: LineSegments[] = [];
    obj.traverse((child) => {
      if (child instanceof LineSegments) {
        toBeRemoved.push(child);
      }
    });
    toBeRemoved.forEach((child) => {
      if (child.parent) child.parent.remove(child);
    });

    // collect object/sub-objects
    const objList: { object: Object3D; depth: number }[] = [];
    if (subObjectWise && !hiddenObject) {
      traverseObject(obj, 0, (child, depth) => {
        objList.push({ object: child, depth });
      });
    } else {
      objList.push({ object: obj, depth: 0 });
    }

    objList.forEach((value) => {
      // set unique color
      let color = new Color(0x000000);
      if (!hiddenObject) {
        do {
          color = new Color(random.integer(1, 0xffffff));
        } while (this.colorMap.has(color.getHexString()));
      }

      this.colorMap.set(color.getHexString(), {
        color,
        objectId: data.id,
        object: value.object,
        depth: value.depth,
        material: new MeshBasicMaterial({ color, side: DoubleSide }),
        ...this.initialValues,
      });
      obj.userData['colorMapKey'] = color.getHexString();
    });

    this.objects.push(obj);
    this.scene.add(obj);

    return obj;
  }

  getObjectsByDepth() {
    const map = new Map<number, (ColorMapBase & T)[]>();

    this.colorMap.forEach((value) => {
      const entry = map.get(value.depth);
      if (entry) {
        entry.push(value);
      } else {
        map.set(value.depth, [value]);
      }
    });

    return map;
  }
}
