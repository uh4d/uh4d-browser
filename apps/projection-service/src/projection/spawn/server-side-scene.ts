import { JSDOM } from 'jsdom';
import { createCanvas } from 'canvas';
import * as gl from 'gl';
import * as sharp from 'sharp';
import {
  ColorManagement,
  Light,
  LinearSRGBColorSpace,
  Mesh,
  Object3D,
  PerspectiveCamera,
  Scene,
  WebGLRenderer,
  WebGLRenderTarget,
} from 'three';
import { EffectComposer } from 'three-addons/postprocessing/EffectComposer';
import { ShaderPass } from 'three-addons/postprocessing/ShaderPass';
import { CopyShader } from 'three-addons/shaders/CopyShader';
import { RenderPass } from 'three-addons/postprocessing/RenderPass';
import { OutputPass } from 'three-addons/postprocessing/OutputPass';

// polyfill
const jsdom = new JSDOM();
jsdom.window.cancelAnimationFrame = () => {
  /* noop */
};
global['self'] = jsdom.window as any;

ColorManagement.enabled = false;

export class ServerSideScene {
  public readonly camera: PerspectiveCamera;
  public readonly scene = new Scene();
  private readonly renderer: WebGLRenderer;
  private readonly renderTarget: WebGLRenderTarget;
  private readonly composer: EffectComposer;

  constructor() {
    this.camera = new PerspectiveCamera(50, 1, 1, 1000);

    // fake canvas element
    const canvas = createCanvas(400, 400) as any;
    canvas.style = {};
    canvas.addEventListener = () => {
      /* noop */
    };
    canvas.removeEventListener = () => {
      /* noop */
    };

    this.renderer = new WebGLRenderer({
      canvas: canvas as HTMLCanvasElement,
      context: gl(400, 400),
      antialias: false,
      stencil: false,
      depth: false,
      logarithmicDepthBuffer: true,
    });
    this.renderTarget = new WebGLRenderTarget(400, 400);
    this.renderer.setRenderTarget(this.renderTarget);
    this.renderer.setClearColor(0, 1);
    this.renderer.outputColorSpace = LinearSRGBColorSpace;

    // post-processing
    this.composer = new EffectComposer(this.renderer, this.renderTarget);
    this.composer.addPass(new RenderPass(this.scene, this.camera));
    this.composer.addPass(new ShaderPass(CopyShader));
    this.composer.addPass(new OutputPass());
  }

  add(...objects: Object3D[]): void {
    this.scene.add(...objects);
  }

  traverse(callback: (object: Object3D) => void): void {
    this.scene.traverse(callback);
  }

  render(width: number, height: number): sharp.Sharp {
    if (isNaN(width) || isNaN(height))
      throw new Error('Width and height not set for rendering!');

    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();

    // update renderer
    this.renderer.setSize(width, height);
    this.renderTarget.setSize(width, height);
    this.composer.setSize(width, height);

    // render
    this.composer.render();

    const buffer = new Uint8Array(width * height * 4);
    this.renderer.readRenderTargetPixels(
      this.renderTarget,
      0,
      0,
      width,
      height,
      buffer,
    );

    return sharp(buffer, {
      raw: { width, height, channels: 4 },
    }).flip();
  }

  dispose(): void {
    this.scene.traverse((child) => {
      if (child instanceof Mesh) {
        child.geometry.dispose();
        const materials = Array.isArray(child.material)
          ? child.material
          : [child.material];
        materials.forEach((mat) => mat.dispose());
      } else if (child instanceof Light) {
        child.dispose();
      }
    });

    this.renderer.dispose();
    this.renderTarget.dispose();
  }
}
