import * as process from 'process';
import { program } from 'commander';
import { join } from 'path';
import 'reflect-metadata';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { Logger } from '@nestjs/common';
import { Mesh } from 'three';
import { ProjectionPayloadDto } from '../dto/projection-payload.dto';
import { ProjectedAnnotations } from '../dto/projected-annotations';
import { ColorCodedScene } from './color-coded-scene';

program
  .name('render-object-annotations')
  .description('Load objects and render annotations.')
  .requiredOption('-o, --output <string>', 'Output path for rendered images');
program.parse();

const opts = program.opts();
const logger = new Logger('ChildProcess render-object-annotations');

process.on('message', async (data) => {
  // start process with incoming message
  try {
    await renderAnnotations(data);
    process.exit();
  } catch (reason) {
    logger.error(reason);
    process.exit(1);
  }
});

async function renderAnnotations(payload: unknown) {
  // validate payload
  const data = plainToInstance(ProjectionPayloadDto, payload);
  const validationErrors = await validate(data);
  if (validationErrors.length) {
    throw new Error('Payload has wrong format');
  }

  const scene = new ColorCodedScene();
  scene.setCamera(data.image);

  // load terrain
  if (data.terrain) {
    await scene.loadObject(data.terrain, true);
  }

  // load objects
  await Promise.all(
    data.objects.map((objData) => scene.loadObject(objData, false, true)),
  );

  const response: ProjectedAnnotations[] = [];

  // render color image for each hierarchy level
  for (const [depth, records] of scene.getObjectsByDepth()) {
    const objectColors: { id: string; color: string }[] = [];

    // apply material
    records.forEach((value) => {
      value.object.traverse((child) => {
        if (child instanceof Mesh) {
          child.material = value.material;
        }
      });
      if (value.color.getHexString() !== '000000') {
        objectColors.push({
          id: value.object.name || value.objectId,
          color: value.color.getHexString(),
        });
      }
    });

    // render color image
    const colorImage = scene.render(data.image.width, data.image.height);

    const filePath = join(opts.output, `render${depth}.png`);
    await colorImage.toFile(filePath);

    response.push({ file: filePath, annotations: objectColors });
  }

  // send to parent process
  process.send(response);
}
