export interface ProjectedAnnotations {
  file: string;
  annotations: {
    id: string;
    color: string;
    contours?: number[][][];
  }[];
}
