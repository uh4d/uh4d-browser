import {
  ArrayMaxSize,
  ArrayMinSize,
  IsArray,
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import {
  ProjectionPayload,
  ProjectionSourceImage as SourceImage,
  ProjectionSourceObject as SourceObject,
} from '@uh4d/dto/interfaces/custom/projection';

export class ProjectionSourceImage implements SourceImage {
  @IsNotEmpty()
  @IsString()
  id: string;
  @IsNotEmpty()
  @IsArray()
  @ArrayMinSize(16)
  @ArrayMaxSize(16)
  @IsNumber({}, { each: true })
  matrix: number[];
  @IsNotEmpty()
  @IsNumber()
  ck: number;
  @IsNotEmpty()
  @IsNumber()
  @IsInt()
  width: number;
  @IsNotEmpty()
  @IsNumber()
  @IsInt()
  height: number;
}

export class ProjectionSourceObject implements SourceObject {
  @IsNotEmpty()
  @IsString()
  id: string;
  @IsNotEmpty()
  @IsString()
  file: string;
  @IsNotEmpty()
  @IsArray()
  @ArrayMinSize(16)
  @ArrayMaxSize(16)
  @IsNumber({}, { each: true })
  matrix: number[];
}

export class ProjectionPayloadDto implements ProjectionPayload {
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => ProjectionSourceImage)
  image: ProjectionSourceImage;
  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => ProjectionSourceObject)
  objects: ProjectionSourceObject[];
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => ProjectionSourceObject)
  terrain?: ProjectionSourceObject;
}
