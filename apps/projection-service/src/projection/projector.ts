import {
  Color,
  LineBasicMaterial,
  LineSegments,
  MathUtils,
  Mesh,
  Object3D,
  PerspectiveCamera,
  Scene,
  WebGLRenderer,
  WebGLRenderTarget,
} from 'three';
import { ObjectDto } from '@uh4d/dto/interfaces/object';
import { loadGltf } from 'node-three-gltf';
import { join } from 'path';
import { Coordinates } from '@uh4d/utils';
import { random } from '@uh4d/backend/utils';
import { ImageDto } from '@uh4d/dto/interfaces';
import * as sharp from 'sharp';
import { ContourNode } from './contour-node';

export class Projector {
  camera: PerspectiveCamera;
  renderer: WebGLRenderer;
  scene: Scene;
  objects: Object3D[] = [];
  dataPath = '';
  tempPath = '';
  origin = new Coordinates();
  colorMap = new Map<string, ContourNode>();
  rootContour: ContourNode;

  constructor() {
    this.renderer = new WebGLRenderer();
    this.camera = new PerspectiveCamera(50, 1, 1, 1000);
    this.scene = new Scene();

    this.rootContour = new ContourNode(this.scene);
  }

  setDataPath(dataPath: string) {
    this.dataPath = dataPath;
  }

  setTempPath(tempPath: string) {
    this.tempPath = tempPath;
  }

  setOrigin(origin: Coordinates) {
    this.origin = origin;
  }

  async setObjects(items: ObjectDto[]): Promise<void> {
    const toBeCreated: ObjectDto[] = [];
    const toBeRemoved: Object3D[] = [];

    items.forEach((item) => {
      if (!this.objects.find((obj) => obj.userData.resource?.id === item.id)) {
        toBeCreated.push(item);
      }
    });

    this.objects.forEach((obj) => {
      if (!obj.userData.resource) return;
      if (!items.find((item) => obj.userData.resource.id === item.id)) {
        toBeRemoved.push(obj);
      }
    });

    toBeRemoved.forEach((obj) => {
      this.disposeObject(obj);
    });

    for (const data of toBeCreated) {
      const obj = await this.loadObject(data);
      this.objects.push(obj);
      this.scene.add(obj);
    }
  }

  private async loadObject(data: ObjectDto): Promise<Object3D> {
    const filePath = join(this.dataPath, data.file.path, data.file.file);
    const gltf = await loadGltf(filePath);
    const obj = gltf.scene.children[0];
    obj.userData.resource = data;

    obj.position.copy(new Coordinates(data.origin).toCartesian(this.origin));
    obj.rotation.set(
      data.origin.omega,
      data.origin.phi,
      data.origin.kappa,
      'YXZ',
    );
    if (data.origin.scale) {
      if (Array.isArray(data.origin.scale)) {
        obj.scale.fromArray(data.origin.scale);
      } else {
        obj.scale.set(data.origin.scale, data.origin.scale, data.origin.scale);
      }
    }
    obj.updateMatrixWorld(true);

    // remove line segments
    const toBeRemoved: LineSegments[] = [];
    obj.traverse((child) => {
      if (child instanceof LineSegments) {
        toBeRemoved.push(child);
      }
    });
    toBeRemoved.forEach((child) => {
      if (child.parent) child.parent.remove(child);
      child.geometry.dispose();
      (child.material as LineBasicMaterial).dispose();
    });

    // traverse children
    const traverse = (child: Object3D, parent: ContourNode) => {
      // set unique color
      let color: Color;
      do {
        color = new Color(random.integer(1, 0xffffff));
      } while (this.colorMap.has(color.getHexString()));

      const contourNode = new ContourNode(child, color);
      parent.add(contourNode);

      this.colorMap.set(color.getHexString(), contourNode);
      child.userData.colorMapKey = color.getHexString();

      for (const c of child.children) {
        traverse(c, contourNode);
      }
    };
    traverse(obj, this.rootContour);

    return obj;
  }

  private disposeObject(obj: Object3D) {
    const index = this.objects.indexOf(obj);
    if (index !== -1) {
      this.objects.splice(index, 1);
    }
    this.scene.remove(obj);

    obj.traverse((child) => {
      if (child instanceof Mesh) {
        child.geometry.dispose();
      }
    });

    if (this.colorMap.has(obj.userData.colorMapKey)) {
      this.colorMap.get(obj.userData.colorMapKey)?.material.dispose();
      this.colorMap.delete(obj.userData.colorMapKey);
    }
  }

  async processImage(img: ImageDto) {
    if (
      !img.camera?.ck ||
      !img.camera.omega ||
      !img.camera.phi ||
      !img.camera.kappa
    ) {
      throw new Error('Camera ist not yet spatialized!');
    }

    const width = img.file.width;
    const height = img.file.height;

    this.camera.fov =
      2 * Math.atan(1 / (2 * img.camera.ck)) * MathUtils.RAD2DEG;
    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();

    this.camera.position.copy(
      new Coordinates(img.camera).toCartesian(this.origin),
    );
    this.camera.rotation.set(
      img.camera.omega,
      img.camera.phi,
      img.camera.kappa,
      'YXZ',
    );
    this.camera.updateMatrixWorld(true);

    // update renderer
    this.renderer.setSize(width, height);
    const renderTarget = new WebGLRenderTarget(width, height);
    this.renderer.setRenderTarget(renderTarget);

    // render
    this.renderer.render(this.scene, this.camera);

    let buffer = new Uint8Array(width * height * 4);
    this.renderer.readRenderTargetPixels(
      renderTarget,
      0,
      0,
      width,
      height,
      buffer,
    );

    const image = sharp(buffer, {
      raw: {
        width: 2,
        height: 1,
        channels: 4,
      },
    });
  }

  dispose() {
    this.scene.traverse((child) => {
      if (child instanceof Mesh) {
        child.geometry.dispose();
      }
    });
    this.renderer.forceContextLoss();
    this.renderer.dispose();
  }
}
