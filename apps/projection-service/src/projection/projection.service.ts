import { Injectable, Logger } from '@nestjs/common';
import * as process from 'process';
import { join } from 'path';
import { ForkOptions } from 'child_process';
import { spawn } from 'promisify-child-process';
import { readJson, writeJson } from 'fs-extra';
import * as tmp from 'tmp-promise';
import { Box2, ShapeUtils, Vector2 } from 'three';
import { forkProcess } from '@uh4d/backend/utils';
import { ObjectWeights } from '@uh4d/dto/interfaces/custom/linked-objects';
import { ImageAnnotationDto } from '@uh4d/dto/interfaces';
import { ProjectionPayloadDto } from './dto/projection-payload.dto';
import { ProjectedAnnotations } from './dto/projected-annotations';

@Injectable()
export class ProjectionService {
  private readonly logger = new Logger(ProjectionService.name);

  private forkRenderProcess<T = unknown>(
    modulePath: string,
    args: string[],
    payload: ProjectionPayloadDto,
  ): Promise<T> {
    // fork render process
    const options: ForkOptions = {};
    if (process.env['USE_XVFB'] === '1') {
      options.execPath = 'xvfb-run';
      options.execArgv = [
        '-a',
        '-s',
        `-ac -screen 0 ${payload.image.width}x${payload.image.height}x24`,
        'node',
      ];
    }

    return forkProcess<T>(modulePath, args, payload, options);
  }

  async computeObjectWeights(
    payload: ProjectionPayloadDto,
  ): Promise<Record<string, ObjectWeights>> {
    // create tmp directory
    const tmpDir = await tmp.dir({ unsafeCleanup: true });

    // debug render files
    const colorRenderFile = join(tmpDir.path, 'render-color.png');
    const depthRenderFile = join(tmpDir.path, 'render-depth.png');

    const args = [
      '--output-color',
      colorRenderFile,
      '--output-depth',
      depthRenderFile,
    ];

    try {
      return await this.forkRenderProcess<Record<string, ObjectWeights>>(
        join(__dirname, 'compute-weights.js'),
        args,
        payload,
      );
    } catch (e) {
      this.logger.error(e.stderr || e);
      throw e;
    } finally {
      await tmpDir.cleanup();
    }
  }

  async projectAnnotationsFromObjects(
    payload: ProjectionPayloadDto,
  ): Promise<Pick<ImageAnnotationDto, 'id' | 'polylist' | 'bbox' | 'area'>[]> {
    // create tmp directory
    const tmpDir = await tmp.dir({ unsafeCleanup: true });

    try {
      // render object annotations to files
      const args = ['--output', tmpDir.path];

      const projected = await this.forkRenderProcess<ProjectedAnnotations[]>(
        join(__dirname, 'render-object-annotations.js'),
        args,
        payload,
      );

      // find contours for each annotation in rendered image
      for (const value of projected) {
        const jsonFile = value.file + '.json';
        await writeJson(jsonFile, value.annotations);

        await spawn(
          'python',
          [
            join(__dirname, 'assets/find-contours.py'),
            '--image',
            value.file,
            '--json',
            jsonFile,
          ],
          { encoding: 'utf8' },
        );

        value.annotations = await readJson(jsonFile);
      }

      return projected
        .map((value) =>
          value.annotations
            .filter(({ contours }) => contours.length > 0)
            .map(({ id, contours }) => {
              // set bbox and area
              const bbox = new Box2();
              let area = 0;

              for (const c of contours) {
                const list = c.map((v) => {
                  const vec2 = new Vector2().fromArray(v);
                  bbox.expandByPoint(vec2);
                  return vec2;
                });
                area += Math.abs(ShapeUtils.area(list));
              }

              const size = bbox.getSize(new Vector2());

              return {
                id,
                polylist: contours,
                bbox: [bbox.min.x, bbox.min.y, size.x, size.y],
                area,
              };
            }),
        )
        .flat();
    } catch (e) {
      this.logger.error(e.stderr || e);
      throw e;
    } finally {
      await tmpDir.cleanup();
    }
  }
}
