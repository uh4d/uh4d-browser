import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { ObjectWeights } from '@uh4d/dto/interfaces/custom/linked-objects';
import { ProjectionService } from './projection.service';
import { ProjectionPayloadDto } from './dto/projection-payload.dto';

@Controller('projection')
export class ProjectionController {
  constructor(private readonly projectionService: ProjectionService) {}

  @MessagePattern({ context: 'image', action: 'computeObjectWeights' })
  computeWeights(
    @Payload() payload: ProjectionPayloadDto,
  ): Promise<Record<string, ObjectWeights>> {
    return this.projectionService.computeObjectWeights(payload);
  }

  @MessagePattern({ context: 'image', action: 'projectAnnotationsFromObject' })
  projectAnnotationsFromObjects(@Payload() payload: ProjectionPayloadDto) {
    return this.projectionService.projectAnnotationsFromObjects(payload);
  }
}
