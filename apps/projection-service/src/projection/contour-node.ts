import { Color, DoubleSide, Mesh, MeshBasicMaterial, Object3D } from 'three';

export class ContourNode {
  isMesh = false;
  color?: Color;
  object: Object3D;
  material: MeshBasicMaterial;
  children: ContourNode[] = [];
  parent: ContourNode | null = null;
  contours: [number, number][][] = [];

  constructor(object: Object3D, color?: Color) {
    this.object = object;
    this.isMesh = object instanceof Mesh;
    this.material = new MeshBasicMaterial({ side: DoubleSide });
    if (color) {
      this.setColor(color);
    }
  }

  add(node: ContourNode) {
    this.children.push(node);
    node.parent = this;
  }

  getHex(): string {
    return this.color?.getHexString() || '';
  }

  setColor(color: Color) {
    this.color = color;
    this.material.color.set(color);
  }

  async traverse(
    callback: (node: ContourNode, depth: number) => Promise<boolean>,
    depth = 0
  ): Promise<void> {
    const proceed = await callback(this, depth);
    if (proceed) {
      for (const child of this.children) {
        await child.traverse(callback, depth + 1);
      }
    }
  }
}
