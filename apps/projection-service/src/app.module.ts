import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ProjectionModule } from './projection/projection.module';

@Module({
  imports: [ConfigModule.forRoot(), ProjectionModule],
})
export class AppModule {}
