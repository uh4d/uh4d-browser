import packageJson from '../../../../package.json';

export const environment = {
  production: true,
  apiBaseUrl: '/api',
  version: packageJson.version,
};
