import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { NgxCutModule } from 'ngx-cut';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import {
  QuickHelpComponent,
  RecordsPagesComponent,
} from '@uh4d/frontend/ui-components';
import { ExhibitionModeDirective } from '@uh4d/frontend/directives';
import { Viewport3dModule } from '@uh4d/frontend/viewport3d';
import { ExploreRoutingModule } from './explore-routing.module';
import { SharedModule } from '@frontend/shared/shared.module';

import { ExploreComponent } from './explore.component';
import { ExploreHeaderComponent } from './components/explore-header/explore-header.component';
import { LegendGradientComponent } from './components/legend-gradient/legend-gradient.component';
import { MetasearchComponent } from './components/metasearch/metasearch.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { TimeSliderComponent } from './components/time-slider/time-slider.component';
import { VisualizationPanelComponent } from './components/visualization-panel/visualization-panel.component';
import { VisualizationPanelPreviewComponent } from './components/visualization-panel-preview/visualization-panel-preview.component';
import { MapSearchComponent } from './components/map-search/map-search.component';
import { ResultsCollapseContainerComponent } from './components/results-collapse-container/results-collapse-container.component';
import { ExploreFooterComponent } from './components/explore-footer/explore-footer.component';
import { AvatarDropdownComponent } from '@frontend/user-profile/components/avatar-dropdown/avatar-dropdown.component';

@NgModule({
  declarations: [
    ExploreComponent,
    ExploreHeaderComponent,
    LegendGradientComponent,
    MetasearchComponent,
    SearchBarComponent,
    TimeSliderComponent,
    VisualizationPanelComponent,
    VisualizationPanelPreviewComponent,
    MapSearchComponent,
    ResultsCollapseContainerComponent,
    ExploreFooterComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    SharedModule,
    Viewport3dModule,
    ExploreRoutingModule,
    LeafletModule,
    NgxCutModule,
    BsDropdownModule,
    AvatarDropdownComponent,
    RecordsPagesComponent,
    QuickHelpComponent,
    ExhibitionModeDirective,
  ],
})
export class ExploreModule {}
