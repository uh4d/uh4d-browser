import {
  AfterViewInit,
  Component,
  ElementRef,
  HostListener,
  Input,
  OnChanges,
  ViewChild,
} from '@angular/core';
import * as d3 from 'd3';
import { nanoid } from 'nanoid';

@Component({
  selector: 'uh4d-legend-gradient',
  template: '<svg #svgElement></svg>',
  styleUrls: ['./legend-gradient.component.sass'],
})
export class LegendGradientComponent implements AfterViewInit, OnChanges {
  @ViewChild('svgElement', { static: true })
  svgElement!: ElementRef<SVGElement>;

  @Input() orientation: 'top' | 'bottom' | 'left' | 'right' = 'bottom';
  @Input() gradient: { [key: string]: string } = { 0: '#000', 1: '#fff' };
  @Input() domain: [number, number] = [0, 100];

  ngAfterViewInit(): void {
    this.draw();
  }

  ngOnChanges() {
    this.draw();
  }

  private draw(): void {
    const id = nanoid(9);

    // layout dependent css
    if (this.orientation === 'left' || this.orientation === 'right') {
      this.svgElement.nativeElement.style.width = '50px';
      this.svgElement.nativeElement.style.height = '100%';
    } else {
      this.svgElement.nativeElement.style.width = '100%';
      this.svgElement.nativeElement.style.height = '40px';
    }

    const { width, height } =
      this.svgElement.nativeElement.getBoundingClientRect();
    const offset = 10;
    const tickValues = [
      this.domain[0],
      Math.round((this.domain[0] + this.domain[1]) / 2),
      this.domain[1],
    ];
    const tmpValues: { stop: number; color: string }[] = [];

    // linear gradient
    for (const [key, value] of Object.entries(this.gradient)) {
      tmpValues.push({ stop: parseFloat(key) * 100, color: value });
    }
    tmpValues.sort((a, b) => a.stop - b.stop);

    const svg = d3.select(this.svgElement.nativeElement);

    svg.selectAll('*').remove();

    const defGradient = svg
      .append('defs')
      .append('linearGradient')
      .attr('id', 'svgGradient_' + id)
      .attr('x1', '0%')
      .attr('y2', '0%');
    tmpValues.forEach((value) => {
      defGradient
        .append('stop')
        .attr('offset', value.stop + '%')
        .attr('stop-color', value.color);
    });

    const scale = d3.scaleLinear().domain(this.domain);

    const rect = svg.append('rect');
    const group = svg.append('g');
    let axis: d3.Axis<number>;

    switch (this.orientation) {
      case 'left':
        defGradient.attr('x2', '0%').attr('y1', '100%');
        rect
          .attr('x', 30)
          .attr('y', offset)
          .attr('width', 20)
          .attr('height', Math.max(height - offset * 2 + 1, 0));
        scale.range([height - offset * 2, 0]);
        axis = d3.axisLeft<number>(scale).tickValues(tickValues);
        group.attr('transform', `translate(30,${offset})`);
        break;
      case 'right':
        defGradient.attr('x2', '0%').attr('y1', '100%');
        rect
          .attr('y', offset)
          .attr('width', 20)
          .attr('height', Math.max(height - offset * 2 + 1, 0));
        scale.range([height - offset * 2, 0]);
        axis = d3.axisRight<number>(scale).tickValues(tickValues);
        group.attr('transform', `translate(20,${offset})`);
        break;
      case 'top':
        defGradient.attr('x2', '100%').attr('y1', '0%');
        rect
          .attr('x', offset)
          .attr('y', 20)
          .attr('width', Math.max(width - offset * 2 + 1, 0))
          .attr('height', 20);
        scale.range([0, width - offset * 2]);
        axis = d3.axisTop<number>(scale).tickValues(tickValues);
        group.attr('transform', `translate(${offset},20)`);
        break;
      default:
        defGradient.attr('x2', '100%').attr('y1', '0%');
        rect
          .attr('x', offset)
          .attr('width', Math.max(width - offset * 2 + 1, 0))
          .attr('height', 20);
        scale.range([0, width - offset * 2]);
        axis = d3.axisBottom<number>(scale).tickValues(tickValues);
        group.attr('transform', `translate(${offset},20)`);
    }

    rect.attr('fill', `url(#svgGradient_${id})`);

    group.call(axis);
  }

  @HostListener('window:resize')
  private resize() {
    this.draw();
  }
}
