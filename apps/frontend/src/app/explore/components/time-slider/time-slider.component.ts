import {
  AfterViewInit,
  Component,
  ElementRef,
  HostBinding,
  HostListener,
  Input,
  NgZone,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { combineLatest, filter, first, switchMap } from 'rxjs';
import * as d3 from 'd3';
import { addYears, formatISO, subYears } from 'date-fns';
import { SearchManagerService } from '@uh4d/frontend/services';

const getDateYear = d3.timeFormat('%Y');

interface IDateHandle {
  date: Date;
  x?: number;
  element?: d3.Selection<SVGGElement, IDateHandle, SVGGElement, null>;
  year?: string;
}

@Component({
  selector: 'uh4d-time-slider',
  templateUrl: './time-slider.component.html',
  styleUrls: ['./time-slider.component.sass'],
})
export class TimeSliderComponent implements AfterViewInit {
  @Input() id = 'main';
  @HostBinding('class.preview') @Input() preview = false;

  @ViewChild('svgElement', { static: true })
  private svgElement!: ElementRef<SVGElement>;

  private WIDTH = 0;
  private HEIGHT = 0;

  private svg: d3.Selection<SVGElement, undefined, any, undefined>;
  private container: d3.Selection<
    SVGGElement,
    undefined,
    SVGGElement,
    undefined
  >;
  private zoom: d3.ZoomBehavior<SVGGElement, unknown>;
  private timeScale: d3.ScaleTime<number, number>;
  private timeScaleInit: d3.ScaleTime<number, number>;
  private tickContainer: d3.Selection<
    SVGGElement,
    undefined,
    SVGGElement,
    undefined
  >;
  private imageSpan: d3.Selection<
    SVGRectElement,
    undefined,
    SVGGElement,
    undefined
  >;
  private yearDiffX: number;

  private start: IDateHandle;
  private end: IDateHandle;
  private from: IDateHandle;
  private to: IDateHandle;
  private model: IDateHandle;
  private maps: IDateHandle[] = [];
  private activeMap: IDateHandle;

  optionsForm = new FormGroup({
    includeUndated: new FormControl<boolean>(true),
    useAsTexture: new FormControl<boolean>(false),
  });

  constructor(
    private route: ActivatedRoute,
    private searchManager: SearchManagerService,
    private ngZone: NgZone,
  ) {}

  async ngAfterViewInit() {
    this.svg = d3.select(this.svgElement.nativeElement);

    this.from = { date: null, x: 0, element: null };
    this.to = { date: null, x: 0, element: null };
    this.model = { date: new Date('1930-01-01'), x: 0, element: null };

    const requiredData$ = combineLatest([
      this.searchManager.dateExtent$.pipe(filter((v) => !!v)),
      this.searchManager.maps$.pipe(filter((v) => !!v)),
    ]).pipe(
      switchMap(([dateExtent, maps]) => {
        // set available maps
        this.maps.length = 0;
        this.maps.push(
          ...maps.map((value) => ({
            date: new Date(value.date),
            x: 0,
            element: null,
            year: value.date,
          })),
        );

        // set min and max date
        if (!this.container) {
          // determine the earliest date
          let earliestDate = this.maps[0]
            ? new Date(dateExtent.min) < this.maps[0].date
              ? dateExtent.min
              : this.maps[0].date
            : dateExtent.min;
          // minimum date should be 1810 or before
          earliestDate =
            earliestDate < new Date('1820-01-01')
              ? earliestDate
              : new Date('1820-01-01');

          this.start = { date: subYears(earliestDate, 10) };
          this.end = { date: new Date() };

          this.from.date = this.from.date
            ? this.from.date
            : addYears(this.start.date, 10);
          this.to.date = this.to.date
            ? this.to.date
            : subYears(this.end.date, 10);
        }

        if (this.container) this.draw();
        return [dateExtent, maps];
      }),
    );

    // watch query parameters
    this.route.queryParamMap
      .pipe((source) =>
        requiredData$.pipe(
          first(),
          switchMap(() => source),
        ),
      )
      .subscribe((params) => {
        if (params.get('from')) {
          this.from.date = new Date(params.get('from'));
        }
        if (params.get('to')) {
          this.to.date = new Date(params.get('to'));
        }
        if (params.get('model')) {
          this.model.date = new Date(params.get('model'));
        }
        if (params.get('undated')) {
          this.optionsForm.patchValue({
            includeUndated: params.get('undated') === '1',
          });
        }
        if (params.get('useAsTexture')) {
          this.optionsForm.patchValue({
            useAsTexture: params.get('useAsTexture') === '1',
          });
        }
        if (params.get('map')) {
          const mapDate = params.get('map');
          const mapHandle = this.maps.find((m) => m.year === mapDate);
          if (this.container) {
            this.setMap(mapHandle);
          } else {
            this.activeMap = mapHandle;
          }
        } else {
          // get map that is dated nearest to 1930
          const refDate = new Date('1930-01-01').valueOf();
          this.activeMap = this.maps.length
            ? this.maps.reduce((nearest, current) => {
                const diffNearest = Math.abs(nearest.date.valueOf() - refDate);
                const diffCurrent = Math.abs(current.date.valueOf() - refDate);
                return diffCurrent < diffNearest ? current : nearest;
              })
            : null;
          if (!this.preview) {
            this.updateMapSearch(this.activeMap);
          }
        }

        this.ngZone.runOutsideAngular(async () => {
          if (this.container) {
            this.draw();
          } else {
            this.initSvg();

            if (!this.preview) {
              await this.updateImageSearch();
              await this.updateModelSearch();
            }
          }
        });
      });

    if (!this.preview) {
      // watch options form
      this.optionsForm.valueChanges.subscribe(() => {
        this.updateImageSearch();
      });
    }
  }

  private initSvg() {
    const bbox = this.svg.node().getBoundingClientRect();
    this.WIDTH = bbox.width;
    this.HEIGHT = bbox.height;

    this.svg
      .select(`#${this.id}-alphaMask > rect`)
      .attr('width', this.WIDTH)
      .attr('height', this.HEIGHT);

    this.timeScale = this.timeScaleInit = d3
      .scaleTime()
      .domain([this.start.date, this.end.date])
      .range([0, this.WIDTH])
      .clamp(false);

    this.zoom = d3
      .zoom<SVGGElement, unknown>()
      .scaleExtent([1, 10])
      .translateExtent([
        [0, 0],
        [this.WIDTH, 0],
      ])
      .on('zoom', (event: d3.D3ZoomEvent<SVGGElement, unknown>) => {
        this.timeScale = event.transform.rescaleX(this.timeScaleInit);
        this.draw();
      });

    this.container = this.svg.append('g').call(this.zoom);

    this.container
      .append('rect')
      .attr('class', 'background')
      .attr('width', '100%')
      .attr('height', '100%');

    this.imageSpan = this.container
      .append('rect')
      .attr('class', 'image-span')
      .attr('y', 15)
      .attr('height', 32);

    if (!this.preview) {
      this.imageSpan.call(
        d3
          .drag()
          .on('drag', (event: d3.D3DragEvent<SVGGElement, unknown, any>) => {
            const width = parseFloat(this.imageSpan.attr('width'));
            this.from.x = Math.min(
              Math.max(this.from.x + event.dx, this.start.x),
              this.end.x - width,
            );
            this.to.x = this.from.x + width;
            this.from.date = this.timeScale.invert(this.from.x);
            this.to.date = this.timeScale.invert(this.to.x);
            this.updateTimeSpan(false);
          })
          .on('end', () => {
            this.updateImageSearch();
          }),
      );
    }

    this.container
      .append('rect')
      .attr('class', 'track')
      .attr('x', 0)
      .attr('y', this.HEIGHT / 2 - 2)
      .attr('width', '100%')
      .attr('height', 3)
      .attr('mask', `url(#${this.id}-alphaMask`);

    this.from.element = this.container.append('g').attr('class', 'from-handle');
    this.from.element.append('path').attr('d', 'M 5 16 H 0 V 46 H 5');
    this.from.element
      .append('text')
      .attr('text-anchor', 'middle')
      .attr('x', 0)
      .attr('y', 12);
    this.from.element
      .append('rect')
      .attr('x', -10)
      .attr('width', 20)
      .attr('height', '100%');

    if (!this.preview) {
      this.from.element.call(
        d3
          .drag()
          .container(this.container.node())
          .on('drag', (event: d3.D3DragEvent<SVGGElement, unknown, any>) => {
            this.from.x = Math.min(
              Math.max(event.x, 0),
              this.to.x - this.yearDiffX,
              this.WIDTH,
            );
            this.from.date = this.timeScale.invert(this.from.x);
            this.updateTimeSpan(false);
          })
          .on('end', () => {
            this.updateImageSearch();
          }),
      );
    }

    this.to.element = this.container.append('g').attr('class', 'to-handle');
    this.to.element.append('path').attr('d', 'M -5 16 H 0 V 46 H -5');
    this.to.element
      .append('text')
      .attr('text-anchor', 'middle')
      .attr('x', 0)
      .attr('y', 12);
    this.to.element
      .append('rect')
      .attr('x', -10)
      .attr('width', 20)
      .attr('height', '100%');

    if (!this.preview) {
      this.to.element.call(
        d3
          .drag()
          .container(this.container.node())
          .on('drag', (event: d3.D3DragEvent<SVGGElement, unknown, any>) => {
            this.to.x = Math.max(
              Math.min(event.x, this.WIDTH),
              this.from.x + this.yearDiffX,
              0,
            );
            this.to.date = this.timeScale.invert(this.to.x);
            this.updateTimeSpan(false);
          })
          .on('end', () => {
            this.updateImageSearch();
          }),
      );
    }

    this.model.element = this.container
      .append('g')
      .attr('class', 'model-handle');
    this.model.element
      .append('path')
      .attr('d', 'M 0 16 V 46 M -4 46 H 4 M -4 16 H 4');
    this.model.element.append('line').attr('y1', 15).attr('y2', 47);
    this.model.element.append('circle').attr('cy', 25).attr('r', 5);
    this.model.element
      .append('text')
      .attr('text-anchor', 'middle')
      .attr('x', 0)
      .attr('y', 12);
    this.model.element
      .append('rect')
      .attr('x', -10)
      .attr('width', 20)
      .attr('height', '100%');

    if (!this.preview) {
      this.model.element.call(
        d3
          .drag()
          .container(this.container.node())
          .on('drag', (event: d3.D3DragEvent<SVGGElement, unknown, any>) => {
            this.model.x = Math.max(Math.min(event.x, this.WIDTH), 0);
            this.model.date = this.timeScale.invert(this.model.x);
            this.updateModelHandle(false);
          })
          .on('end', () => {
            this.updateModelSearch();
          }),
      );
    }

    this.tickContainer = this.container
      .append('g')
      .attr('class', 'tick-container')
      .attr('transform', 'translate(0,25)')
      .attr('mask', `url(#${this.id}-alphaMask`);

    this.draw();
  }

  private draw() {
    // this.timeScale.domain([this.start.date, this.end.date]);

    this.yearDiffX = this.timeScale(
      new Date(+this.timeScale.domain()[0]).setFullYear(
        this.timeScale.domain()[0].getFullYear() + 1,
      ),
    );

    // draw maps
    const maps = this.container.selectAll('g.map-handle').data(this.maps);

    const mapEnter = maps
      .enter()
      .append('g')
      .attr('class', 'map-handle')
      .attr('transform', (d) => `translate(${this.timeScale(d.date)},0)`)
      .each((d, i, group) => {
        d.element = d3.select(group[i]);
        if (d === this.activeMap) {
          d.element.classed('active', true);
        }
      });
    mapEnter.append('path').attr('d', 'M -2 16 V 46 H 4 V 16 Z');
    mapEnter
      .append('text')
      .attr('text-anchor', 'middle')
      .attr('x', 0)
      .attr('y', 12)
      .text((d) => getDateYear(d.date));
    mapEnter
      .append('rect')
      .attr('x', -10)
      .attr('width', 20)
      .attr('height', '100%');
    if (!this.preview) {
      mapEnter.on('click', (event, d) => {
        this.updateMapSearch(d !== this.activeMap ? d : null);
      });
    }

    maps
      .transition()
      .duration(0)
      .attr('transform', (d) => `translate(${this.timeScale(d.date)},0)`);

    maps.exit().remove();

    // draw ticks
    const ticks = this.tickContainer
      .selectAll('g.tick')
      .data(this.timeScale.ticks(Math.floor(this.WIDTH / 50)));

    const tickEnter = ticks
      .enter()
      .append('g')
      .attr('class', 'tick')
      .attr('transform', (d) => `translate(${this.timeScale(d)},0)`);
    tickEnter.append('line').attr('y1', 0).attr('y2', 6);
    tickEnter
      .append('text')
      .attr('y', 18)
      .attr('text-anchor', 'middle')
      .text(getDateYear);

    ticks
      .transition()
      .duration(0)
      .attr('transform', (d) => `translate(${this.timeScale(d)},0)`)
      .select('text')
      .text(getDateYear);

    ticks.exit().remove();

    this.updateTimeSpan();
    this.updateModelHandle();
  }

  private updateTimeSpan(computePosition = true) {
    if (computePosition) {
      this.from.x = this.timeScale(this.from.date);
      this.to.x = this.timeScale(this.to.date);
      this.start.x = this.timeScale(this.start.date);
      this.end.x = this.timeScale(this.end.date);
    }

    const diff = this.to.x - this.from.x;

    this.imageSpan.attr('x', this.from.x).attr('width', diff);

    this.from.element.attr('transform', `translate(${this.from.x},0)`);
    this.from.element
      .select('text')
      .attr(
        'x',
        Math.min(
          this.WIDTH - this.from.x - 50,
          Math.max(-this.from.x + 17, Math.min(diff / 2, 17) - 17),
        ),
      )
      .text(getDateYear(this.from.date));

    this.to.element.attr('transform', `translate(${this.to.x},0)`);
    this.to.element
      .select('text')
      .attr(
        'x',
        Math.max(
          -this.to.x + 50,
          Math.min(this.WIDTH - this.to.x - 17, Math.max(-diff / 2, -17) + 17),
        ),
      )
      .text(getDateYear(this.to.date));
  }

  private updateModelHandle(computePosition = true) {
    if (computePosition) {
      this.model.x = this.timeScale(this.model.date);
    }

    this.model.element.attr('transform', `translate(${this.model.x},0)`);
    this.model.element.select('text').text(getDateYear(this.model.date));
  }

  private setMap(map: IDateHandle, toggle = false) {
    if (this.activeMap) {
      this.activeMap.element.classed('active', false);
    }

    if (toggle) {
      if (map === this.activeMap) {
        this.activeMap = null;
      } else {
        map.element.classed('active', true);
        this.activeMap = map;
      }
    } else {
      this.activeMap = map;
      if (this.activeMap) {
        this.activeMap.element.classed('active', true);
      }
    }
  }

  private async updateImageSearch() {
    return this.ngZone.run(() => {
      return this.searchManager.setImageDates(
        formatISO(this.from.date, { representation: 'date' }),
        formatISO(this.to.date, { representation: 'date' }),
        this.optionsForm.value.includeUndated,
        this.optionsForm.value.useAsTexture,
      );
    });
  }

  private async updateModelSearch() {
    return this.ngZone.run(() => {
      return this.searchManager.setModelDate(
        formatISO(this.model.date, { representation: 'date' }),
      );
    });
  }

  private async updateMapSearch(map?: IDateHandle) {
    return this.ngZone.run(() => {
      return this.searchManager.setMapDate(map ? map.year : 'none');
    });
  }

  @HostListener('window:resize')
  private resize() {
    const bbox = this.svg.node().getBoundingClientRect();
    this.WIDTH = bbox.width;
    this.HEIGHT = bbox.height;

    this.svg
      .select(`#${this.id}-alphaMask > rect`)
      .attr('width', this.WIDTH)
      .attr('height', this.HEIGHT);

    this.zoom.translateExtent([
      [0, 0],
      [this.WIDTH, 0],
    ]);
    this.timeScale.range([0, this.WIDTH]);
    this.timeScaleInit.range([0, this.WIDTH]);

    this.draw();
  }
}
