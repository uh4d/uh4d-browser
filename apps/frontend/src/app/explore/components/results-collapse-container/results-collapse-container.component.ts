import {
  AfterViewInit,
  Component,
  ElementRef,
  NgZone,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { BsModalService } from 'ngx-bootstrap/modal';
import { SearchManagerService } from '@uh4d/frontend/services';
import { Cache } from '@uh4d/frontend/viewport3d-cache';
import { ImageUploadModalComponent } from '@uh4d/frontend/image';
import { ImageDto } from '@uh4d/dto/interfaces/image';
import { TextDto } from '@uh4d/dto/interfaces/text';
import { ImageExtendedDto } from '@uh4d/dto/interfaces/custom/image';
import { TextExtendedDto } from '@uh4d/dto/interfaces/custom/text';

/**
 * Container component that enables users to interactively resize the component's width.
 * The container can also be collapsed, so that only a small bar remains.
 * Additionally, the number of search results is displayed at the top.
 * The component wraps other content, such as {@link ResultsPagesComponent}.
 *
 * ![In resize mode](../assets/image-collapse-container.png)&emsp;
 * ![In collapsed mode](../assets/image-collapse-container-bar.png)
 */
@Component({
  selector: 'uh4d-results-collapse-container',
  templateUrl: './results-collapse-container.component.html',
  styleUrls: ['./results-collapse-container.component.sass'],
})
export class ResultsCollapseContainerComponent
  implements OnInit, AfterViewInit, OnDestroy
{
  /**
   * @ignore
   */
  private unsubscribe$ = new Subject<void>();
  /**
   * @ignore
   */
  private unlisten: (() => void)[] = [];
  /**
   * @ignore
   */
  private unlistenMouseDown: () => void;

  /**
   * Reference to the element that serves as interactive resize handle.
   */
  @ViewChild('resizeHandle', { static: false })
  resizeHandle: ElementRef<HTMLDivElement>;

  /**
   * Initial size in pixels as set by the parent component.
   * @private
   */
  private initialSize = 0;

  /**
   * Offset from {@link #initialSize} in pixels.
   * @private
   */
  private resizeOffset = 0;

  /**
   * Current size in pixels in non-collapsed mode.
   * @private
   */
  private width = 600;

  /**
   * Flag indicating if the container is collapsed.
   */
  collapsed = false;

  activeTab: 'images' | 'texts' = 'images';

  /**
   * Number of image search results displayed in the tab.
   */
  imagesLength = 0;

  /**
   * Number of text search results displayed in the tab.
   */
  textsLength = 0;

  constructor(
    public searchManager: SearchManagerService,
    private renderer: Renderer2,
    private element: ElementRef<HTMLElement>,
    private ngZone: NgZone,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: BsModalService,
  ) {}

  /**
   * Invoked after the component is instantiated.
   */
  ngOnInit(): void {
    this.renderer.setStyle(
      this.element.nativeElement,
      'width',
      this.width + 'px',
    );
    this.imagesLength = this.searchManager.images$.getValue().length;
    this.textsLength = this.searchManager.texts$.getValue().length;
  }

  /**
   * Invoked after the component's view is initialized.
   * Listen to image query updates and set initial width.
   */
  ngAfterViewInit(): void {
    // listen to images
    this.searchManager.images$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((images) => {
        this.imagesLength = images.length;
      });

    // listen to texts
    this.searchManager.texts$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((texts) => {
        this.textsLength = texts.length;
      });

    // set initial width
    this.toggle(this.collapsed, false);
  }

  /**
   * Set active results tab. If in collapsed mode, expand results container.
   */
  setActiveTab(tab: 'images' | 'texts'): void {
    this.activeTab = tab;
    if (this.collapsed) {
      this.toggle(false);
    }
  }

  /**
   * Toggle collapse mode. Set width of container
   * and bind `mousedown` event to {@link #resizeHandle resizeHandle}.
   * @param collapse Set `true` to enter collapse mode, and vice versa
   * @param triggerEvent Set to `false` to prevent global `resize` event
   */
  toggle(collapse: boolean, triggerEvent = true): void {
    this.collapsed = collapse;
    this.renderer.setStyle(
      this.element.nativeElement,
      'width',
      (this.collapsed ? 30 : this.width) + 'px',
    );

    if (collapse) {
      if (this.unlistenMouseDown) {
        this.unlistenMouseDown();
        this.unlistenMouseDown = null;
      }
    } else {
      setTimeout(() => {
        this.ngZone.runOutsideAngular(() => {
          this.unlistenMouseDown = this.renderer.listen(
            this.resizeHandle.nativeElement,
            'mousedown',
            this.onMouseDown.bind(this),
          );
        });
      });
    }

    if (triggerEvent) {
      setTimeout(() => {
        window.dispatchEvent(new Event('resize'));
      });
    }
  }

  /**
   * `mousedown` event listener bound to {@link #resizeHandle resizeHandle}.
   * Bind `mousemove` and `mouseup` events.
   * @private
   */
  private onMouseDown(event: MouseEvent): void {
    event.preventDefault();

    if (event.button !== 0) return;

    this.resizeOffset = event.pageX;
    this.initialSize = this.element.nativeElement.offsetWidth;

    this.unlisten.push(
      this.renderer.listen(document, 'mousemove', this.onMouseMove.bind(this)),
      this.renderer.listen(document, 'mouseup', this.onMouseUp.bind(this)),
    );
  }

  /**
   * `mousemove` event listener, update container width.
   * @private
   */
  private onMouseMove(event: MouseEvent): void {
    this.width = this.initialSize + this.resizeOffset - event.pageX;
    this.renderer.setStyle(
      this.element.nativeElement,
      'width',
      Math.max(this.width, 400) + 'px',
    );
  }

  /**
   * `mouseup` event listener, unbind `mousemove` and `mouseup` events.
   * Trigger global `resize` event if container width has changed.
   * @private
   */
  private onMouseUp(event: MouseEvent): void {
    this.unlisten.forEach((fn) => fn());

    if (this.resizeOffset !== event.pageX) {
      // trigger resize
      window.dispatchEvent(new Event('resize'));
    }
  }

  /**
   * Open image details sheet.
   */
  openImage(image: ImageDto): void {
    this.router.navigate(['./image', image.id], {
      relativeTo: this.route,
      queryParamsHandling: 'preserve',
    });
  }

  openImageWithAnnotations(event: MouseEvent, image: ImageExtendedDto): void {
    event.stopPropagation();
    this.router.navigate(['./image', image.id], {
      relativeTo: this.route,
      queryParamsHandling: 'preserve',
      fragment: 'annotations',
      state: { annotations: image.annotations },
    });
  }

  /**
   * Return to /explore path and jump to image location.
   */
  zoomImage(event: MouseEvent, image: ImageDto): void {
    event.stopPropagation();
    if (!image.camera) return;

    const item = Cache.images.getByName(image.id);

    if (item) {
      this.router.navigate(['.'], {
        relativeTo: this.route,
        queryParamsHandling: 'preserve',
      });
      item.focus();
    }
  }

  /**
   * Open modal to upload images to the scene.
   */
  openUploadModal(): void {
    this.modalService.show(ImageUploadModalComponent, {
      class: 'modal-lg',
      ignoreBackdropClick: true,
    });
  }

  /**
   * Open text details sheet.
   */
  openText(text: TextDto): void {
    this.router.navigate(['./text', text.id], {
      relativeTo: this.route,
      queryParamsHandling: 'preserve',
    });
  }

  openTextWithAnnotations(event: MouseEvent, text: TextExtendedDto): void {
    event.stopPropagation();
    this.router.navigate(['./text', text.id], {
      relativeTo: this.route,
      queryParamsHandling: 'preserve',
      state: { annotations: text.annotations },
    });
  }

  /**
   * Invoked before the component gets destroyed.
   */
  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
