import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsCollapseContainerComponent } from './results-collapse-container.component';

describe('ResultsCollapseContainerComponent', () => {
  let component: ResultsCollapseContainerComponent;
  let fixture: ComponentFixture<ResultsCollapseContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ResultsCollapseContainerComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ResultsCollapseContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
