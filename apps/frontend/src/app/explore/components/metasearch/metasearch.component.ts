import { Component, OnDestroy, OnInit } from '@angular/core';
import { lastValueFrom, Subject, takeUntil } from 'rxjs';
import { SearchManagerService } from '@uh4d/frontend/services';
import {
  LegalBodiesApiService,
  PersonsApiService,
} from '@uh4d/frontend/api-service';
import { ImageDto, ObjectDto, PersonDto } from '@uh4d/dto/interfaces';
import { LegalBodyDto } from '@uh4d/dto/interfaces/custom/legal-body';

@Component({
  selector: 'uh4d-metasearch',
  templateUrl: './metasearch.component.html',
  styleUrls: ['./metasearch.component.sass'],
})
export class MetasearchComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();

  authors: PersonDto[] = [];
  authorsFiltered: (PersonDto & { active: boolean })[] = [];
  owners: LegalBodyDto[] = [];
  ownersFiltered: (LegalBodyDto & { active: boolean })[] = [];
  objects: (ObjectDto & { active: boolean })[] = [];

  constructor(
    private searchManager: SearchManagerService,
    private personsApiService: PersonsApiService,
    private legalBodiesApiService: LegalBodiesApiService,
  ) {}

  ngOnInit(): void {
    this.searchManager.images$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((images) => {
        this.updateFacettedMeta(images);
      });

    this.searchManager.models$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((models) => {
        this.updateFacettedObjects(models);
      });
  }

  private async updateFacettedMeta(images: ImageDto[]) {
    if (this.authors.length === 0) {
      this.authors = await lastValueFrom(this.personsApiService.query());
    }
    if (this.owners.length === 0) {
      this.owners = await lastValueFrom(this.legalBodiesApiService.query());
    }

    // clear
    this.authorsFiltered = [];
    this.ownersFiltered = [];

    const qAuthor = this.searchManager.params$
      .getValue()
      .q.filter((q) => /^author:/.test(q));
    const qOwner = this.searchManager.params$
      .getValue()
      .q.filter((q) => /^owner:/.test(q));

    images.forEach((img) => {
      if (img.author) {
        const index = this.authors.findIndex((a) => a.value === img.author);
        if (index !== -1) {
          const author = this.authors[index];
          if (this.authorsFiltered.find((a) => a.id === author.id)) {
            return;
          }
          this.authorsFiltered.push(
            Object.assign(
              {
                active: qAuthor.some((q) => q === 'author:' + author.id),
              },
              author,
            ),
          );
        }
      }

      if (img.owner) {
        const index = this.owners.findIndex((o) => o.value === img.owner);
        if (index !== -1) {
          const owner = this.owners[index];
          if (this.ownersFiltered.find((a) => a.id === owner.id)) {
            return;
          }
          this.ownersFiltered.push(
            Object.assign(
              {
                active: qOwner.some((q) => q === 'owner:' + owner.id),
              },
              owner,
            ),
          );
        }
      }
    });

    // sort
    this.authorsFiltered.sort((a, b) => a.value.localeCompare(b.value));
    this.ownersFiltered.sort((a, b) => a.value.localeCompare(b.value));
  }

  private updateFacettedObjects(objects: ObjectDto[]) {
    const qObj = this.searchManager.params$
      .getValue()
      .q.filter((q) => /^obj:/.test(q));

    this.objects = objects.map((obj) =>
      Object.assign({ active: qObj.some((q) => q === 'obj:' + obj.id) }, obj),
    );

    // sort
    this.objects.sort((a, b) => a.name.localeCompare(b.name));
  }

  filterAuthor(author: PersonDto & { active: boolean }) {
    this.searchManager.setSearchParam(
      ['author:' + author.id],
      !author.active,
      author.active,
    );
  }

  filterOwner(owner: LegalBodyDto & { active: boolean }) {
    this.searchManager.setSearchParam(
      ['owner:' + owner.id],
      !owner.active,
      owner.active,
    );
  }

  async filterObject(object: ObjectDto & { active: boolean }) {
    await this.searchManager.setSearchParam(
      ['obj:' + object.id],
      !object.active,
      object.active,
    );
    object.active = !object.active;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
