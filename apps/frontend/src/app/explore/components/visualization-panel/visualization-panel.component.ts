import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { GlobalEventsService } from '@uh4d/frontend/services';
import { IVizMeta, VisualizationType, vizMeta } from '@uh4d/config';
import { VisualizationGradientConfig } from '@uh4d/frontend/visualization';

@Component({
  selector: 'uh4d-visualization-panel',
  templateUrl: './visualization-panel.component.html',
  styleUrls: ['./visualization-panel.component.sass'],
})
export class VisualizationPanelComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();

  activeType: VisualizationType = 'none';
  activeViz: IVizMeta;
  availableViz: [VisualizationType, IVizMeta][] = [];
  gradientConfig: VisualizationGradientConfig;

  constructor(private events: GlobalEventsService) {}

  ngOnInit(): void {
    this.availableViz = Array.from(vizMeta.entries());

    this.events.callAction$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((action) => {
        switch (action.key) {
          case 'visualization':
            // set visualization
            this.activeType = action.type;
            this.activeViz = vizMeta.get(action.type);
            break;

          case 'gradient-update':
            this.gradientConfig = action.config;
        }
      });
  }

  select(type: VisualizationType): void {
    this.events.callAction$.next({
      key: 'visualization',
      type,
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
