import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UntypedFormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import {
  delay,
  from,
  lastValueFrom,
  Observable,
  Observer,
  of,
  Subject,
  switchMap,
  takeUntil,
} from 'rxjs';
import {
  latLng,
  LatLng,
  Map,
  MapOptions,
  rectangle,
  tileLayer,
} from 'leaflet';
import { OpenStreetMapProvider } from 'leaflet-geosearch';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { Coordinates } from '@uh4d/utils';

interface SearchResult {
  x: number;
  y: number;
  label: string;
  bounds: [[number, number], [number, number]] | null;
}

@Component({
  selector: 'uh4d-map-search',
  templateUrl: './map-search.component.html',
  styleUrls: ['./map-search.component.sass'],
})
export class MapSearchComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();
  private debounceInterrupt$ = new Subject<void>();

  private position$ = new Subject<LatLng>();
  private positionData: LatLng = latLng(51.049329, 13.738144);

  get currentPosition(): LatLng {
    return this.positionData;
  }

  private map: Map | null = null;

  leafletOptions: MapOptions = {
    layers: [
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
      }),
    ],
    center: this.positionData,
    zoom: 14,
  };

  rectMarker = rectangle(
    [
      [0, 0],
      [0, 0],
    ],
    { color: '#df691a', weight: 3 },
  );

  inputControl = new UntypedFormControl('');

  placeName = 'Dresden, Sachsen, Deutschland';

  searchResults$!: Observable<SearchResult[]>;

  constructor(
    private readonly http: HttpClient,
    private readonly router: Router,
  ) {}

  ngOnInit(): void {
    // observable for typeahead
    this.searchResults$ = new Observable((observer: Observer<string>) => {
      observer.next(this.inputControl.value);
    }).pipe(
      switchMap((query) => {
        if (query) {
          return from(this.queryGeoSearch(query));
        }
        return of([]);
      }),
    );

    // lookup place name if map has been moved
    this.position$
      .pipe(
        takeUntil(this.unsubscribe$),
        switchMap((value) =>
          of(value).pipe(delay(2000), takeUntil(this.debounceInterrupt$)),
        ),
      )
      .subscribe(async (position) => {
        this.placeName = await this.reverseGeoLookup(position);
      });
  }

  onMapReady(map: Map): void {
    this.map = map;
    setTimeout(() => {
      this.map?.invalidateSize(false);
      this.setRectBounds(this.positionData);
    });
  }

  onMapMove(): void {
    if (this.map) this.setRectBounds(this.map.getCenter());
  }

  onMapMoveEnd(): void {
    if (this.map) {
      this.positionData = this.map.getCenter();
      this.position$.next(this.positionData);
    }
  }

  /**
   * Set new position of map center and marker.
   */
  private setMapPosition(value: LatLng = this.currentPosition): void {
    // set map center
    if (this.map) {
      this.map.setView(value);
    } else {
      this.leafletOptions.center = value;
    }

    // set marker position
    this.setRectBounds(value);
  }

  private setRectBounds(position: LatLng): void {
    const bounds = new Coordinates(
      position.lat,
      position.lng,
    ).computeBoundingBox(2500);
    this.rectMarker.setBounds([
      [bounds.south, bounds.west],
      [bounds.north, bounds.east],
    ]);
  }

  /**
   * Set new position data when autocomplete option has been selected.
   */
  onSelect(event: TypeaheadMatch): void {
    this.positionData = latLng(event.item.y, event.item.x);
    this.placeName = event.item.label;
    this.setMapPosition();
    this.debounceInterrupt$.next();
  }

  /**
   * Search OpenStreetMap to obtain geo-coordinates.
   */
  private async queryGeoSearch(value: string): Promise<SearchResult[]> {
    const provider = new OpenStreetMapProvider();
    return provider.search({ query: value });
  }

  /**
   * Lookup place name from geo-coordinates.
   */
  private async reverseGeoLookup(position: LatLng): Promise<string> {
    const result = await lastValueFrom(
      this.http.get<any>('https://nominatim.openstreetmap.org/reverse', {
        params: {
          format: 'jsonv2',
          lat: position.lat,
          lon: position.lng,
          zoom: 17,
        },
      }),
    );

    return [
      'hamlet',
      'village',
      'suburb',
      'town',
      'city',
      'county',
      'state',
      'country',
    ]
      .reduce(
        (list, prop) =>
          result.address[prop] && !list.includes(result.address[prop])
            ? list.concat([result.address[prop]])
            : list,
        [] as string[],
      )
      .join(', ');
  }

  /**
   * Open scene with selected coordinates.
   */
  openScene(): void {
    this.router.navigate([
      '/explore',
      `${this.currentPosition.lat},${this.currentPosition.lng}`,
    ]);
  }

  ngOnDestroy(): void {
    this.debounceInterrupt$.complete();
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
