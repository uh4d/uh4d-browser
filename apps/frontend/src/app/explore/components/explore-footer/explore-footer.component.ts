import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { GlobalEventsService } from '@uh4d/frontend/services';
import { IVizMeta, VisualizationType, vizMeta } from '@uh4d/config';

@Component({
  selector: 'uh4d-explore-footer',
  templateUrl: './explore-footer.component.html',
  styleUrls: ['./explore-footer.component.sass'],
})
export class ExploreFooterComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();

  activeType: VisualizationType = 'none';
  availableViz: [VisualizationType, IVizMeta][] = [];

  constructor(private events: GlobalEventsService) {}

  ngOnInit(): void {
    this.availableViz.push(
      ['heatmap', vizMeta.get('heatmap')],
      ['vectorfield', vizMeta.get('vectorfield')],
    );

    this.events.callAction$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((action) => {
        switch (action.key) {
          case 'visualization':
            // set visualization
            this.activeType = action.type;
            break;
        }
      });
  }

  selectViz(type: VisualizationType): void {
    this.events.callAction$.next({
      key: 'visualization',
      type,
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
