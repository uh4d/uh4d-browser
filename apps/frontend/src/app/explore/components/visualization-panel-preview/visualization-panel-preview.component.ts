import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { GlobalEventsService } from '@uh4d/frontend/services';
import { IVizMeta, vizMeta } from '@uh4d/config';
import { VisualizationGradientConfig } from '@uh4d/frontend/visualization';

@Component({
  selector: 'uh4d-visualization-panel-preview',
  templateUrl: './visualization-panel-preview.component.html',
  styleUrls: ['./visualization-panel-preview.component.sass'],
})
export class VisualizationPanelPreviewComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();

  activeViz: IVizMeta;
  gradientConfig: VisualizationGradientConfig;

  constructor(private events: GlobalEventsService) {}

  ngOnInit(): void {
    this.events.callAction$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((action) => {
        switch (action.key) {
          case 'visualization':
            this.activeViz = vizMeta.get(action.type);
            break;
          case 'gradient-update':
            this.gradientConfig = action.config;
        }
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
