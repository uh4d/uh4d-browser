import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { imageResolver, ImageSheetComponent } from '@uh4d/frontend/image';
import { objectResolver, ObjectSheetComponent } from '@uh4d/frontend/object';
import { textResolver, TextSheetComponent } from '@uh4d/frontend/text';
import {
  normDataResolver,
  NormDataSheetComponent,
} from '@uh4d/frontend/norm-data';

import { ExploreComponent } from './explore.component';
import { SceneResolverService } from './scene-resolver.service';
import { MapSearchComponent } from './components/map-search/map-search.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: MapSearchComponent,
      },
      {
        path: ':scene',
        component: ExploreComponent,
        resolve: {
          scene: SceneResolverService,
        },
        children: [
          {
            path: 'image/:imageId',
            component: ImageSheetComponent,
            resolve: {
              image: imageResolver,
            },
          },
          {
            path: 'object/:objectId',
            component: ObjectSheetComponent,
            resolve: {
              object: objectResolver,
            },
          },
          {
            path: 'text/:textId',
            component: TextSheetComponent,
            resolve: {
              text: textResolver,
            },
          },
          {
            path: 'normdata/:identifier',
            component: NormDataSheetComponent,
            resolve: {
              normData: normDataResolver,
            },
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExploreRoutingModule {}
