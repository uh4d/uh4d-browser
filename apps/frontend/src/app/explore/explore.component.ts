import { Component, OnInit } from '@angular/core';
import { Cache, coordsConverter } from '@uh4d/frontend/viewport3d-cache';

@Component({
  selector: 'uh4d-explore',
  templateUrl: './explore.component.html',
  styleUrls: ['./explore.component.sass'],
})
export class ExploreComponent implements OnInit {
  ngOnInit(): void {
    // set coordinates converter
    Cache.terrainManager.setCoordsConverter(coordsConverter);
  }
}
