import { Component, OnInit } from '@angular/core';
import { fromEvent } from 'rxjs';
import { NgxPermissionsService } from 'ngx-permissions';

const editPermissions = [
  'editImages',
  'manageImages',
  'spatializeImages',
  'linkImages',
  'editObjects',
  'manageObjects',
  'managePOIs',
  'editTexts',
  'manageTexts',
];
const debugPermissions = ['showDebug'];

@Component({
  selector: 'uh4d-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
})
export class AppComponent implements OnInit {
  constructor(private permissionsService: NgxPermissionsService) {}

  ngOnInit(): void {
    // restore edit mode
    // if (localStorage.getItem('editMode')) {
    //   this.permissionsService.addPermission(editPermissions);
    // }
    // // restore debug mode
    // if (localStorage.getItem('debugMode')) {
    //   this.permissionsService.addPermission(debugPermissions);
    // }

    fromEvent(document, 'keyup').subscribe((event: KeyboardEvent) => {
      if (event.shiftKey && event.altKey) {
        switch (event.code) {
          case 'KeyE':
            this.toggleEditorMode();
            break;
          case 'KeyQ':
            this.toggleDebugMode();
            break;
        }
      }
    });
  }

  private toggleEditorMode() {
    const editMode = localStorage.getItem('editMode');

    // if (editMode) {
    //   // leave edit mode, flush permissions
    //   this.removePermissions(editPermissions);
    //   localStorage.removeItem('editMode');
    // } else {
    //   // enter edit mode, add permissions
    //   this.permissionsService.addPermission(editPermissions);
    //   localStorage.setItem('editMode', '1');
    // }
  }

  private toggleDebugMode() {
    const debugMode = localStorage.getItem('debugMode');

    if (debugMode) {
      // leave debug mode
      this.removePermissions(debugPermissions);
      localStorage.removeItem('debugMode');
    } else {
      // enter debug mode
      this.permissionsService.addPermission(debugPermissions);
      localStorage.setItem('debugMode', '1');
    }
  }

  private removePermissions(permissions: string[]) {
    permissions.forEach((p) => {
      this.permissionsService.removePermission(p);
    });
  }
}
