import { Tagify, TagData } from 'ngx-tagify';

const availableFilter = ['obj', 'author', 'owner', 'ann'];

export const regexpFilter = new RegExp(
  '^(' + availableFilter.map((f) => `(?:${f})`).join('|') + '):(.+)',
);

export function tagTemplate(this: Tagify, tagData: TagData): string {
  const type = tagData['type'] || 'text';
  const strippedValue = (tagData['label'] || tagData.value).replace(
    /&quot;/g,
    '',
  );

  let img = '';
  switch (type) {
    case 'obj':
      img = `<img src="assets/icons/building.svg"/> `;
      break;
    case 'author':
      img = `<img src="assets/icons/user-solid.svg"/> `;
      break;
    case 'owner':
      img = `<img src="assets/icons/university-solid.svg"/> `;
      break;
    case 'ann':
      img = `<img src="assets/icons/annotation.svg"/> `;
  }

  return `
    <tag title="${
      tagData['title'] || strippedValue
    }" contenteditable="false" spellcheck="false" tabIndex="-1" class="tagify__tag ${
    tagData['class'] ? tagData['class'] : ''
  }" ${this.getAttributes(tagData)}>
        <x class="tagify__tag__removeBtn" role="button" aria-label="remove-tag"></x>
        <div><span class="tagify__tag-text">${img}${strippedValue}</span></div>
    </tag>`;
}
