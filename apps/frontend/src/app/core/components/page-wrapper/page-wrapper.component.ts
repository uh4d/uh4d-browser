import { Component } from '@angular/core';

@Component({
  selector: 'uh4d-page-wrapper',
  templateUrl: './page-wrapper.component.html',
  styleUrls: ['./page-wrapper.component.sass'],
})
export class PageWrapperComponent {}
