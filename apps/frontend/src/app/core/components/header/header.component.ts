import { Component } from '@angular/core';

@Component({
  selector: 'uh4d-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass'],
})
export class HeaderComponent {}
