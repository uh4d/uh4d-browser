import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@frontend/shared/shared.module';
import { AvatarDropdownComponent } from '@frontend/user-profile/components/avatar-dropdown/avatar-dropdown.component';

import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './pages/home/home.component';
import { LegalnoticeComponent } from './pages/legalnotice/legalnotice.component';
import { HelpComponent } from './pages/help/help.component';
import { PageWrapperComponent } from './components/page-wrapper/page-wrapper.component';

@NgModule({
  declarations: [
    HeaderComponent,
    PageWrapperComponent,
    HomeComponent,
    LegalnoticeComponent,
    HelpComponent,
  ],
  exports: [HeaderComponent],
  imports: [CommonModule, SharedModule, AvatarDropdownComponent],
})
export class CoreModule {}
