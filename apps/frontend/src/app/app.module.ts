import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NgxPermissionsModule } from 'ngx-permissions';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from '@frontend/shared/shared.module';
import { ContextMenuModule } from '@uh4d/frontend/context-menu';
import {
  authInitializer,
  AuthModule,
  httpInterceptorProviders,
} from '@uh4d/frontend/auth';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SimpleNotificationsModule.forRoot({ timeOut: 5000 }),
    CoreModule,
    AuthModule,
    SharedModule,
    ContextMenuModule,
    NgxPermissionsModule.forRoot(),
    AppRoutingModule,
  ],
  providers: [authInitializer, httpInterceptorProviders],
  bootstrap: [AppComponent],
})
export class AppModule {}
