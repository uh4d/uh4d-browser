import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ngxPermissionsGuard } from 'ngx-permissions';

import {
  imageResolver,
  ImageSheetCompactComponent,
} from '@uh4d/frontend/image';
import {
  objectResolver,
  ObjectSheetCompactComponent,
} from '@uh4d/frontend/object';
import { textResolver, TextSheetCompactComponent } from '@uh4d/frontend/text';
import { PoiPageComponent, poiResolver } from '@uh4d/frontend/poi';

import { DashboardComponent } from './dashboard.component';
import { UserUploadsComponent } from './pages/user-uploads/user-uploads.component';
import { UserRephotosComponent } from './pages/user-rephotos/user-rephotos.component';
import { ManageUserAccountsComponent } from './pages/manage-user-accounts/manage-user-accounts.component';
import { ContentValidationComponent } from './pages/content-validation/content-validation.component';

const detailRoutes: Routes = [
  {
    path: 'image/:imageId',
    component: ImageSheetCompactComponent,
    resolve: {
      image: imageResolver,
    },
  },
  {
    path: 'object/:objectId',
    component: ObjectSheetCompactComponent,
    resolve: {
      object: objectResolver,
    },
  },
  {
    path: 'text/:textId',
    component: TextSheetCompactComponent,
    resolve: {
      text: textResolver,
    },
  },
  {
    path: 'poi/:poiId',
    component: PoiPageComponent,
    resolve: {
      poi: poiResolver,
    },
  },
];

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: '',
        redirectTo: '/dashboard/uploads',
        pathMatch: 'full',
      },
      {
        path: 'uploads',
        component: UserUploadsComponent,
        children: detailRoutes,
      },
      {
        path: 'validation',
        component: ContentValidationComponent,
        children: detailRoutes,
      },
      {
        path: 'rephotos',
        component: UserRephotosComponent,
      },
      {
        path: 'admin',
        canActivate: [ngxPermissionsGuard],
        data: {
          permissions: {
            only: 'ADMIN',
          },
        },
        children: [
          {
            path: 'users',
            component: ManageUserAccountsComponent,
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
