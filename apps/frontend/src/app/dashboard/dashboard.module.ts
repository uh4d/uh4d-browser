import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgxPermissionsModule } from 'ngx-permissions';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import {
  MapEditableGeojsonComponent,
  RecordsFilterComponent,
  RecordsPagesComponent,
} from '@uh4d/frontend/ui-components';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { UserRephotosComponent } from './pages/user-rephotos/user-rephotos.component';
import { UserUploadsComponent } from './pages/user-uploads/user-uploads.component';
import { ManageUserAccountsComponent } from './pages/manage-user-accounts/manage-user-accounts.component';
import { ContentValidationComponent } from './pages/content-validation/content-validation.component';
import { UserGeofenceModalComponent } from './components/user-geofence-modal/user-geofence-modal.component';

@NgModule({
  declarations: [
    DashboardComponent,
    UserRephotosComponent,
    UserUploadsComponent,
    ManageUserAccountsComponent,
    ContentValidationComponent,
    UserGeofenceModalComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    NgxPermissionsModule,
    RecordsFilterComponent,
    RecordsPagesComponent,
    ButtonsModule,
    FormsModule,
    Uh4dIconsModule,
    MapEditableGeojsonComponent,
  ],
})
export class DashboardModule {}
