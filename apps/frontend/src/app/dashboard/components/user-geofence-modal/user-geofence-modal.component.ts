import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { GeofenceDto, UserDto } from '@uh4d/dto/interfaces';
import {
  convertGeoFeatureToLocation,
  convertLocationToGeoFeature,
  PointFeature,
} from '@uh4d/utils';
import { UsersApiService } from '@uh4d/frontend/api-service';

@Component({
  selector: 'uh4d-user-geofence-modal',
  templateUrl: './user-geofence-modal.component.html',
  styleUrls: ['./user-geofence-modal.component.sass'],
})
export class UserGeofenceModalComponent implements OnInit {
  user!: UserDto;

  geoJson: PointFeature[] = [];

  private fences: GeofenceDto[] = [];

  isSaving = false;

  constructor(
    private readonly modalRef: BsModalRef,
    private readonly usersApiService: UsersApiService,
  ) {}

  ngOnInit() {
    this.fences = [...this.user.geofences];
    this.geoJson = this.user.geofences.map((fence) =>
      convertLocationToGeoFeature(fence),
    );
  }

  onGeoJsonUpdate(features: PointFeature[]) {
    this.fences = features.map((value) => convertGeoFeatureToLocation(value));
  }

  save() {
    if (this.fences.length === 0 && this.user.geofences.length === 0) {
      // nothing changed, skip save
      this.close();
      return;
    }

    this.isSaving = true;
    this.usersApiService
      .updateUserGeofence(this.user.id, this.fences)
      .pipe(
        finalize(() => {
          this.isSaving = false;
        }),
      )
      .subscribe(() => {
        // update user
        this.user.geofences = this.fences;
        this.close();
      });
  }

  close() {
    this.modalRef.hide();
  }
}
