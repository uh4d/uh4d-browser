import { Component, OnDestroy, OnInit } from '@angular/core';
import { finalize, map, mergeMap, of, Subject, take, takeUntil } from 'rxjs';
import { UsersApiService } from '@uh4d/frontend/api-service';
import { AuthService } from '@uh4d/frontend/auth';
import {
  RecordItem,
  RecordType,
  ValidationStatus,
} from '@uh4d/frontend/ui-components';
import { ActivatedRoute, Router } from '@angular/router';
import { ImageDataService } from '@uh4d/frontend/image';
import { TextDataService } from '@uh4d/frontend/text';
import { ObjectDataService } from '@uh4d/frontend/object';

@Component({
  selector: 'uh4d-user-uploads',
  templateUrl: './user-uploads.component.html',
  styleUrls: ['./user-uploads.component.sass'],
})
export class UserUploadsComponent implements OnInit, OnDestroy {
  private unsubscribe$: Subject<void> = new Subject<void>();

  recordType: RecordType = 'image';
  validationStatus: ValidationStatus = 'accepted';

  records: RecordItem[] = [];

  isLoading = false;

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly usersApiService: UsersApiService,
    private readonly imageDataService: ImageDataService,
    private readonly textDataService: TextDataService,
    private readonly objectDataService: ObjectDataService,
  ) {}

  ngOnInit() {
    this.route.fragment
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((fragment) => {
        if (fragment) {
          const [type, status] = fragment.split(':');
          if (type && status) {
            this.recordType = type as RecordType;
            this.validationStatus = status as ValidationStatus;
          }
        }
        this.loadRecords();
      });

    this.imageDataService.update$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        if (this.recordType === 'image') {
          this.loadRecords();
        }
      });
    this.textDataService.update$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        if (this.recordType === 'text') {
          this.loadRecords();
        }
      });
    this.objectDataService.update$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        if (this.recordType === 'object') {
          this.loadRecords();
        }
      });
  }

  setFragment() {
    this.router.navigate(['.'], {
      relativeTo: this.route,
      fragment: `${this.recordType}:${this.validationStatus}`,
    });
  }

  loadRecords() {
    const query: string[] = [];
    if (this.validationStatus === 'pending') {
      query.push('pending');
    } else if (this.validationStatus === 'declined') {
      query.push('declined');
    }

    this.isLoading = true;
    this.authService.user$
      .pipe(
        take(1),
        mergeMap((user) => {
          switch (this.recordType) {
            case 'image':
              return this.usersApiService.getUserImages(user.id, { q: query });
            case 'text':
              return this.usersApiService.getUserTexts(user.id, { q: query });
            case 'object':
              return this.usersApiService
                .getUserObjects(user.id, { q: query })
                .pipe(
                  map((value) =>
                    value.map((v) => ({
                      ...v,
                      title: v.name,
                      date: undefined,
                      file: undefined,
                      placeholder: 'assets/placeholder-3d.jpg',
                    })),
                  ),
                );
            case 'poi':
              return this.usersApiService
                .getUserPois(user.id, { q: query })
                .pipe(
                  map((value) =>
                    value.map((v) => ({
                      ...v,
                      date: undefined,
                      placeholder: 'assets/placeholder-poi.jpg',
                    })),
                  ),
                );
            default:
              return of([] as RecordItem[]);
          }
        }),
        finalize(() => {
          this.isLoading = false;
        }),
      )
      .subscribe((records: RecordItem[]) => {
        records.sort((a, b) =>
          (b.uploadedBy || b.createdBy).timestamp.localeCompare(
            (a.uploadedBy || a.createdBy).timestamp,
          ),
        );
        this.records = records;
      });
  }

  openRecord(recordType: string, record: RecordItem) {
    this.router.navigate(['.', recordType, record.id], {
      relativeTo: this.route,
      preserveFragment: true,
    });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
