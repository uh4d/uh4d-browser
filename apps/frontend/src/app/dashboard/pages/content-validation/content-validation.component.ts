import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  filter,
  finalize,
  map,
  merge,
  mergeMap,
  of,
  Subject,
  take,
  takeUntil,
} from 'rxjs';
import {
  RecordItem,
  RecordType,
  ValidationStatus,
} from '@uh4d/frontend/ui-components';
import {
  ImagesApiService,
  ObjectsApiService,
  PoisApiService,
  TextsApiService,
} from '@uh4d/frontend/api-service';
import { ImageDataService } from '@uh4d/frontend/image';
import { TextDataService } from '@uh4d/frontend/text';
import { ObjectDataService } from '@uh4d/frontend/object';
import { PoiDataService } from '@uh4d/frontend/poi';
import { AuthService } from '@uh4d/frontend/auth';

@Component({
  selector: 'uh4d-content-validation',
  templateUrl: './content-validation.component.html',
  styleUrls: ['./content-validation.component.sass'],
})
export class ContentValidationComponent implements OnInit, OnDestroy {
  private unsubscribe$: Subject<void> = new Subject();

  recordType: RecordType = 'image';
  validationStatus: ValidationStatus = 'pending';

  records: RecordItem[] = [];

  isLoading = false;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly authService: AuthService,
    private readonly imagesApiService: ImagesApiService,
    private readonly textsApiService: TextsApiService,
    private readonly objectsApiService: ObjectsApiService,
    private readonly poisApiService: PoisApiService,
    private readonly imageDataService: ImageDataService,
    private readonly textDataService: TextDataService,
    private readonly objectDataService: ObjectDataService,
    private readonly poiDataService: PoiDataService,
  ) {}

  ngOnInit() {
    this.route.fragment
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((fragment) => {
        if (fragment) {
          const [type, status] = fragment.split(':');
          if (type && status) {
            this.recordType = type as RecordType;
            this.validationStatus = status as ValidationStatus;
          }
        }
        this.loadRecords();
      });

    merge(
      this.imageDataService.update$.pipe(
        filter(() => this.recordType === 'image'),
      ),
      this.textDataService.update$.pipe(
        filter(() => this.recordType === 'text'),
      ),
      this.objectDataService.update$.pipe(
        filter(() => this.recordType === 'object'),
      ),
      this.poiDataService.update$.pipe(filter(() => this.recordType === 'poi')),
    )
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        this.loadRecords();
      });
  }

  setFragment() {
    this.router.navigate(['.'], {
      relativeTo: this.route,
      fragment: `${this.recordType}:${this.validationStatus}`,
    });
  }

  loadRecords() {
    const query: string[] = [];
    if (this.validationStatus === 'pending') {
      query.push('pending');
    } else if (this.validationStatus === 'declined') {
      query.push('declined');
    }

    this.isLoading = true;
    this.authService.user$
      .pipe(
        take(1),
        mergeMap((user) => {
          switch (this.recordType) {
            case 'image':
              return this.imagesApiService
                .query({ q: query })
                .pipe(
                  map((images) =>
                    images.filter((img) => user.canEditItem(img.camera)),
                  ),
                );
            case 'text':
              return this.textsApiService
                .query({ q: query })
                .pipe(
                  map((texts) =>
                    texts.filter((text) =>
                      user.canEditItem(text.objects[0].location),
                    ),
                  ),
                );
            case 'object':
              return this.objectsApiService.query({ q: query }).pipe(
                map((value) =>
                  value
                    .filter((obj) => user.canEditItem(obj.location))
                    .map((v) => ({
                      ...v,
                      title: v.name,
                      date: undefined,
                      file: undefined,
                      placeholder: 'assets/placeholder-3d.jpg',
                    })),
                ),
              );
            case 'poi':
              return this.poisApiService.query({ q: query }).pipe(
                map((value) =>
                  value
                    .filter((poi) => user.canEditItem(poi.location))
                    .map((v) => ({
                      ...v,
                      date: undefined,
                      placeholder: 'assets/placeholder-poi.jpg',
                    })),
                ),
              );
            default:
              return of([] as RecordItem[]);
          }
        }),
        finalize(() => {
          this.isLoading = false;
        }),
      )
      .subscribe((records) => {
        records.sort((a, b) =>
          (b.uploadedBy || b.createdBy).timestamp.localeCompare(
            (a.uploadedBy || a.createdBy).timestamp,
          ),
        );
        this.records = records;
      });
  }

  openRecord(recordType: string, record: RecordItem) {
    this.router.navigate(['.', recordType, record.id], {
      relativeTo: this.route,
      preserveFragment: true,
    });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
