import { Component, OnInit } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import { take } from 'rxjs';
import { BsModalService } from 'ngx-bootstrap/modal';
import { UserDto } from '@uh4d/dto/interfaces';
import { UsersApiService } from '@uh4d/frontend/api-service';
import { AuthService, AuthUser } from '@uh4d/frontend/auth';
import { UserGeofenceModalComponent } from '../../components/user-geofence-modal/user-geofence-modal.component';

@Component({
  selector: 'uh4d-manage-user-accounts',
  templateUrl: './manage-user-accounts.component.html',
  styleUrls: ['./manage-user-accounts.component.sass'],
})
export class ManageUserAccountsComponent implements OnInit {
  authUser!: AuthUser;

  users: UserDto[] = [];

  constructor(
    private readonly authService: AuthService,
    private readonly usersApiService: UsersApiService,
    private readonly notify: NotificationsService,
    private readonly modalService: BsModalService,
  ) {}

  ngOnInit() {
    this.authService.user$.pipe(take(1)).subscribe((user) => {
      this.authUser = user;
    });

    this.loadUsers();
  }

  private loadUsers() {
    this.usersApiService.query().subscribe((users) => {
      users.sort((a, b) => a.name.localeCompare(b.name));
      this.users = users;
    });
  }

  updateUserRole(user: UserDto) {
    this.usersApiService.updateUserRole(user.id, user.role).subscribe(() => {
      this.notify.success(
        'User updated',
        `${user.name} is now ${user.role || 'normal user'}.`,
      );
    });
  }

  openGeofenceModal(user: UserDto) {
    this.modalService.show(UserGeofenceModalComponent, {
      class: 'modal-lg',
      initialState: {
        user,
      },
    });
  }
}
