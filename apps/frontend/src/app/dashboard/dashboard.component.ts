import { Component } from '@angular/core';

@Component({
  selector: 'uh4d-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass'],
})
export class DashboardComponent {}
