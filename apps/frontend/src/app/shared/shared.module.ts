import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  FaIconLibrary,
  FontAwesomeModule,
} from '@fortawesome/angular-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { faMarkdown } from '@fortawesome/free-brands-svg-icons';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TagifyModule } from 'ngx-tagify';
import { MarkdownModule, MarkedOptions } from 'ngx-markdown';
import { NgxPermissionsModule } from 'ngx-permissions';
import { MomentModule } from 'ngx-moment';
import { NgxCutModule } from 'ngx-cut';

import { markedOptionsFactory } from '@frontend/shared/marked-options-factory';
import { FooterComponent } from '@frontend/shared/components/footer/footer.component';
import { ImageViewerComponent } from './components/image-viewer/image-viewer.component';

import {
  faBuildingCustom,
  faFilterCustom,
  faFilterCustomSlash,
  faMapMarkerAltSlash,
  faMapMarkerAltSlashDashed,
  faMapMarkerLight,
  faScreenshot,
  faTourFlag,
  faTourFlagPlus,
  faCamSpatialSolid,
  faCamSpatialEmpty,
  faCamSpatialSlash,
  faCamSpatialFog,
  faImgAnnotation,
} from '@frontend/shared/icons';

@NgModule({
  declarations: [
    FooterComponent,
    ImageViewerComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    ButtonsModule.forRoot(),
    PaginationModule.forRoot(),
    PopoverModule.forRoot(),
    ProgressbarModule.forRoot(),
    TypeaheadModule.forRoot(),
    ModalModule.forRoot(),
    TagifyModule,
    MarkdownModule.forRoot({
      markedOptions: {
        provide: MarkedOptions,
        useFactory: markedOptionsFactory,
      },
    }),
    NgxPermissionsModule.forChild(),
    MomentModule,
    NgxCutModule,
  ],
  exports: [
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    FooterComponent,
    ButtonsModule,
    PaginationModule,
    ProgressbarModule,
    TypeaheadModule,
    TagifyModule,
    MarkdownModule,
    NgxPermissionsModule,
    MomentModule,
    ImageViewerComponent,
  ],
})
export class SharedModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas, far);
    library.addIcons(
      faMarkdown,
      faMapMarkerLight,
      faMapMarkerAltSlash,
      faMapMarkerAltSlashDashed,
      faFilterCustom,
      faFilterCustomSlash,
      faBuildingCustom,
      faScreenshot,
      faTourFlag,
      faTourFlagPlus,
      faCamSpatialSolid,
      faCamSpatialEmpty,
      faCamSpatialSlash,
      faCamSpatialFog,
      faImgAnnotation,
    );
  }
}
