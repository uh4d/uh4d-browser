import { MarkedOptions, MarkedRenderer } from 'ngx-markdown';

export function markedOptionsFactory(): MarkedOptions {
  const renderer = new MarkedRenderer();

  renderer.link = (href, title, text) => {
    return `<a href="${href}" target="_blank">${text}</a>`;
  };

  return {
    renderer,
  };
}
