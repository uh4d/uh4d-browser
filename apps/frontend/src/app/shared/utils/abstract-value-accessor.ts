import { ControlValueAccessor } from '@angular/forms';

export class AbstractValueAccessor<T> implements ControlValueAccessor {
  private valueData: T;
  private onChange: any = Function.prototype;
  private onTouched: any = Function.prototype;

  get value(): T {
    return this.valueData;
  }

  set value(v: T) {
    if (v !== this.valueData) {
      this.valueData = v;
      this.onChange(v);
    }
  }

  writeValue(v: T) {
    this.valueData = v;
  }

  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }
}
