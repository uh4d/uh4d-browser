import {
  Component,
  ElementRef,
  HostListener,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { BehaviorSubject, Subject, takeUntil } from 'rxjs';
import {
  Mesh,
  MeshBasicMaterial,
  PerspectiveCamera,
  PlaneGeometry,
  Scene,
  TextureLoader,
  WebGLRenderer,
} from 'three';

@Component({
  selector: 'uh4d-image-viewer',
  template: ` <canvas
    #canvasElement
    (contextmenu)="$event.preventDefault()"
  ></canvas>`,
  styleUrls: ['./image-viewer.component.sass'],
})
export class ImageViewerComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();
  private unlisten: (() => void)[] = [];
  private src$ = new BehaviorSubject<string>('');

  @ViewChild('canvasElement', { static: true }) canvasElement: ElementRef;

  @Input() options: { width: number; height: number };
  @Input()
  set src(value: string) {
    this.src$.next(value);
  }

  private isPanning = false;
  private panSpeed = 0.0015;
  private zoomSpeed = 0.1;
  private maxZoomIn = -0.05;
  private maxZoomOut = -1;

  private SCREEN_WIDTH: number;
  private SCREEN_HEIGHT: number;
  private imageAspect: number;
  private canvasAspect: number;
  private angleX: number;
  private angleY: number;

  private loader: TextureLoader;
  private scene: Scene;
  private renderer: WebGLRenderer;
  private camera: PerspectiveCamera;
  private plane: Mesh;

  constructor(
    private element: ElementRef,
    private ngZone: NgZone,
    private ngRenderer: Renderer2,
  ) {}

  ngOnInit(): void {
    this.resizeViewer();

    this.loader = new TextureLoader();

    this.scene = new Scene();

    this.renderer = new WebGLRenderer({
      antialias: true,
      alpha: true,
      canvas: this.canvasElement.nativeElement,
    });
    this.renderer.setClearColor(0xffffff, 0.0);
    this.renderer.setSize(this.SCREEN_WIDTH, this.SCREEN_HEIGHT);

    this.camera = new PerspectiveCamera(
      (2 * this.angleY * 180) / Math.PI,
      this.canvasAspect,
      0.01,
      500,
    );
    this.camera.lookAt(0, 0, -1);

    this.plane = new Mesh(
      new PlaneGeometry(1, 1, 1, 1),
      new MeshBasicMaterial(),
    );
    this.plane.position.set(0, 0, -1);

    this.ngZone.runOutsideAngular(() => {
      this.unlisten.push(
        this.ngRenderer.listen(
          this.element.nativeElement,
          'mousedown',
          this.onMouseDown.bind(this),
        ),
        this.ngRenderer.listen(
          this.element.nativeElement,
          'mousemove',
          this.onMouseMove.bind(this),
        ),
        this.ngRenderer.listen(
          window.document,
          'mouseup',
          this.onMouseUp.bind(this),
        ),
        this.ngRenderer.listen(
          this.element.nativeElement,
          'wheel',
          this.onWheel.bind(this),
        ),
      );
    });

    this.src$.pipe(takeUntil(this.unsubscribe$)).subscribe((src) => {
      this.loadTexture(src);
    });
  }

  private loadTexture(path: string) {
    if (!['jpg', 'png'].includes(path.split('.').pop())) {
      return;
    }

    this.loader.load(
      path,
      (texture) => {
        if (this.options && this.options.width && this.options.height) {
          this.imageAspect = this.options.width / this.options.height;
        } else {
          this.imageAspect = texture.image.width / texture.image.height;
        }

        this.plane.scale.set(this.imageAspect, 1, 1);

        const mat = this.plane.material as MeshBasicMaterial;
        if (mat.map) {
          mat.map.dispose();
        }
        mat.map = texture;
        mat.needsUpdate = true;

        this.scene.add(this.plane);

        this.resizeViewer();
        this.resetView();
      },
      null,
      () => {
        console.error("ImageViewer: Couldn't load texture - " + path);
      },
    );
  }

  private render() {
    this.renderer.render(this.scene, this.camera);
  }

  /**
   * Mouse down event handler
   */
  private onMouseDown(event: MouseEvent) {
    event.preventDefault();
    if (event.button === 0 || event.button === 1) {
      this.isPanning = true;
    }
  }

  /**
   * Mouse move event handler
   */
  private onMouseMove(event: MouseEvent) {
    event.preventDefault();
    if (this.isPanning) {
      this.plane.translateX(
        event.movementX * this.panSpeed * -this.plane.position.z,
      );
      this.plane.translateY(
        event.movementY * this.panSpeed * this.plane.position.z,
      );
      this.fitToBorders();
      this.render();
    }
  }

  /**
   * Mouse up event handler
   */
  private onMouseUp(event: MouseEvent) {
    if (this.isPanning && (event.button === 0 || event.button === 1)) {
      this.isPanning = false;
    }
  }

  /**
   * Mouse wheel event handler
   */
  private onWheel(event: WheelEvent) {
    if (event.deltaY < 0) {
      // scroll up -> zoom in
      const delta = -this.zoomSpeed * this.plane.position.z;
      if (this.plane.position.z + delta > this.maxZoomIn) return;
      this.plane.translateZ(delta);
      this.fitToBorders();
      this.render();
    } else if (event.deltaY > 0) {
      // scroll down -> zoom out
      let delta = this.zoomSpeed * this.plane.position.z;
      if (this.plane.position.z + delta <= this.maxZoomOut) {
        delta = this.maxZoomOut - this.plane.position.z;
      }
      this.plane.translateZ(delta);
      this.fitToBorders();
      this.render();
    }
  }

  /**
   * Keep image plane within the camera view.
   */
  private fitToBorders() {
    const xa = Math.abs(this.plane.position.z) * Math.tan(this.angleX);
    const ya = Math.abs(this.plane.position.z) * Math.tan(this.angleY);
    const cWidth = 2 * xa;
    const cHeight = 2 * ya;
    const iWidth =
      Math.abs(this.plane.position.x - this.imageAspect / 2) +
      Math.abs(this.plane.position.x + this.imageAspect / 2);
    const iHeight =
      Math.abs(this.plane.position.y - 0.5) +
      Math.abs(this.plane.position.y + 0.5);

    if (this.imageAspect < this.canvasAspect && cWidth > iWidth) {
      if (xa < this.plane.position.x + this.imageAspect / 2) {
        this.plane.translateX(
          xa - (this.plane.position.x + this.imageAspect / 2),
        );
      }
      if (-xa > this.plane.position.x - this.imageAspect / 2) {
        this.plane.translateX(
          -xa - (this.plane.position.x - this.imageAspect / 2),
        );
      }
    } else {
      if (xa > this.plane.position.x + this.imageAspect / 2) {
        this.plane.translateX(
          xa - (this.plane.position.x + this.imageAspect / 2),
        );
      }
      if (-xa < this.plane.position.x - this.imageAspect / 2) {
        this.plane.translateX(
          -xa - (this.plane.position.x - this.imageAspect / 2),
        );
      }
    }

    if (this.imageAspect > this.canvasAspect && cHeight > iHeight) {
      if (ya < this.plane.position.y + 0.5) {
        this.plane.translateY(ya - (this.plane.position.y + 0.5));
      }
      if (-ya > this.plane.position.y - 0.5) {
        this.plane.translateY(-ya - (this.plane.position.y - 0.5));
      }
    } else {
      if (ya > this.plane.position.y + 0.5) {
        this.plane.translateY(ya - (this.plane.position.y + 0.5));
      }
      if (-ya < this.plane.position.y - 0.5) {
        this.plane.translateY(-ya - (this.plane.position.y - 0.5));
      }
    }
  }

  /**
   * Set initial view/position of image plane and set maxZoomOut
   */
  private resetView() {
    if (this.imageAspect < this.canvasAspect) {
      this.plane.position.set(0, 0, -1);
      this.maxZoomOut = -1;
    } else {
      const z = this.imageAspect / 2 / Math.tan(this.angleX);
      this.plane.position.set(0, 0, -z);
      this.maxZoomOut = -z;
    }

    if (this.renderer) {
      this.render();
    }
  }

  @HostListener('window:resize')
  private resizeViewer() {
    this.SCREEN_WIDTH = this.element.nativeElement.offsetWidth;
    this.SCREEN_HEIGHT = this.element.nativeElement.offsetHeight;

    this.canvasAspect = this.SCREEN_WIDTH / this.SCREEN_HEIGHT;
    this.angleX = Math.atan(0.5 * this.canvasAspect);
    this.angleY = Math.atan(0.5);

    const doResetView = this.plane && this.plane.position.z === this.maxZoomOut;

    if (this.imageAspect && this.imageAspect > this.canvasAspect) {
      this.maxZoomOut = -(this.imageAspect / 2) / Math.tan(this.angleX);
    } else {
      this.maxZoomOut = -1;
    }

    if (this.camera) {
      this.camera.aspect = this.canvasAspect;
      this.camera.updateProjectionMatrix();
    }

    if (this.renderer) {
      this.renderer.setSize(this.SCREEN_WIDTH, this.SCREEN_HEIGHT);
      if (doResetView) {
        this.resetView();
      } else {
        this.fitToBorders();
        this.render();
      }
    }
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();

    this.unlisten.forEach((fn) => fn());

    // dispose geometry and materials
    if (this.plane) {
      this.plane.geometry.dispose();
      const mat = this.plane.material as MeshBasicMaterial;
      if (mat.map) mat.map.dispose();
      mat.dispose();
    }

    // dispose renderer
    if (this.renderer) {
      this.renderer.forceContextLoss();
      this.renderer.dispose();
    }
  }
}
