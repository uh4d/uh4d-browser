import { Component } from '@angular/core';
import { environment } from '@frontend/environment';

@Component({
  selector: 'uh4d-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass'],
})
export class FooterComponent {
  appVersion = environment.version;
}
