import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import { UserProfileComponent } from './user-profile.component';
import { UserProfileRoutingModule } from './user-profile-routing.module';
import { ProfileSheetComponent } from './pages/profile-sheet/profile-sheet.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';

@NgModule({
  declarations: [
    UserProfileComponent,
    ProfileSheetComponent,
    ChangePasswordComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    UserProfileRoutingModule,
    Uh4dIconsModule,
  ],
})
export class UserProfileModule {}
