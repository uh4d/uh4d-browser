import { Component } from '@angular/core';

@Component({
  selector: 'uh4d-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.sass'],
})
export class UserProfileComponent {}
