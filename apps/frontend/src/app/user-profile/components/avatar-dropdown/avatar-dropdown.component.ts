import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterLink } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { NgxPermissionsModule } from 'ngx-permissions';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import {
  AuthService,
  JwtUserData,
  LoginModalComponent,
} from '@uh4d/frontend/auth';
import { UserAvatarComponent } from '../user-avatar/user-avatar.component';

@Component({
  selector: 'uh4d-avatar-dropdown',
  standalone: true,
  imports: [
    CommonModule,
    RouterLink,
    Uh4dIconsModule,
    BsDropdownModule,
    NgxPermissionsModule,
    UserAvatarComponent,
  ],
  templateUrl: './avatar-dropdown.component.html',
  styleUrls: ['./avatar-dropdown.component.sass'],
})
export class AvatarDropdownComponent implements OnInit {
  user: JwtUserData | null = null;

  constructor(
    private readonly authService: AuthService,
    private readonly modal: BsModalService,
  ) {}

  ngOnInit() {
    this.authService.user$.subscribe((user) => {
      this.user = user;
    });
  }

  openLogin() {
    this.modal.show(LoginModalComponent, {
      ignoreBackdropClick: true,
    });
  }

  logout() {
    this.authService.logout();
  }
}
