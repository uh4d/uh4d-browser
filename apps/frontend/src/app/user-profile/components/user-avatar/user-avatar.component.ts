import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Uh4dIconsModule } from '@uh4d/frontend/uh4d-icons';
import { JwtUserData } from '@uh4d/frontend/auth';

@Component({
  selector: 'uh4d-user-avatar',
  standalone: true,
  imports: [CommonModule, Uh4dIconsModule],
  templateUrl: './user-avatar.component.html',
  styleUrls: ['./user-avatar.component.sass'],
})
export class UserAvatarComponent {
  @Input() set user(value: JwtUserData | null) {
    if (value) {
      this.initials = this.generateInitials(value.name);
      this.colorIndex = this.generateUserColorIndex(value.id);
    } else {
      this.initials = '';
      this.colorIndex = 0;
    }
  }

  initials = '';
  colorIndex = 0;

  private generateInitials(name: string): string {
    return (
      name
        .match(/(\b\w)?/g)
        ?.join('')
        .match(/(^\S|\S$)?/g)
        ?.join('')
        .toUpperCase() ?? ''
    );
  }

  private generateUserColorIndex(userId: string): number {
    const alphaNumericUserId = userId.replace(/[^a-z0-9]/gi, '');
    let userValue = 0;
    for (let i = 0; i < alphaNumericUserId.length; i++) {
      userValue += alphaNumericUserId.charCodeAt(i);
    }
    return (userValue % 10) + 1;
  }
}
