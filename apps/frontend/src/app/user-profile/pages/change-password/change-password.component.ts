import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {
  AbstractControl,
  FormControl,
  FormGroup, ReactiveFormsModule,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { finalize } from 'rxjs';
import { NotificationsService } from 'angular2-notifications';
import { AuthService } from '@uh4d/frontend/auth';

@Component({
  selector: 'uh4d-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.sass'],
})
export class ChangePasswordComponent {
  form = new FormGroup(
    {
      password: new FormControl<string>('', [
        Validators.required,
        Validators.minLength(6),
        Validators.pattern(/^\S+$/),
      ]),
      passwordRepeat: new FormControl<string>('', [Validators.required]),
    },
    (control: AbstractControl): ValidationErrors | null => {
      const pw1 = control.get('password')?.value;
      const pw2 = control.get('passwordRepeat')?.value;
      return pw1 && pw1 !== pw2
        ? { passwordMatch: 'passwords do not match' }
        : null;
    },
  );

  isLoading = false;
  passwordVisible = false;
  passwordRepeatVisible = false;

  constructor(
    private readonly authService: AuthService,
    private readonly notify: NotificationsService,
    private readonly router: Router,
  ) {}

  save() {
    if (this.form.invalid) return;

    this.isLoading = true;

    this.authService
      .updatePassword(this.form.value.password!)
      .pipe(
        finalize(() => {
          this.isLoading = false;
        }),
      )
      .subscribe({
        next: () => {
          this.notify.success('Password changed');
          this.close();
        },
      });
  }

  close() {
    this.router.navigate(['/profile']);
  }
}
