import { Component, OnInit } from '@angular/core';
import { UsersApiService } from '@uh4d/frontend/api-service';
import { UserDto } from '@uh4d/dto/interfaces';

@Component({
  selector: 'uh4d-profile-sheet',
  templateUrl: './profile-sheet.component.html',
  styleUrls: ['./profile-sheet.component.sass'],
})
export class ProfileSheetComponent implements OnInit {
  user: UserDto;

  constructor(private readonly usersApiService: UsersApiService) {}

  ngOnInit() {
    this.usersApiService.getProfileData().subscribe((user) => {
      this.user = user;
    });
  }
}
