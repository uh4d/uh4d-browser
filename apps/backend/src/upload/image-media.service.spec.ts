import { Test, TestingModule } from '@nestjs/testing';
import { ImageMediaService } from './image-media.service';

describe('ImageMediaService', () => {
  let service: ImageMediaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ImageMediaService],
    }).compile();

    service = module.get<ImageMediaService>(ImageMediaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
