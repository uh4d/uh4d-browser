import { Global, Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { diskStorage } from 'multer';
import { nanoid } from 'nanoid';
import { join } from 'node:path';
import { MicroservicesModule } from '@backend/microservices/microservices.module';
import { ImageMediaService } from './image-media.service';
import { ObjectMediaService } from './object-media.service';
import { TextMediaService } from './text-media.service';

@Global()
@Module({
  imports: [
    MulterModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        storage: diskStorage({
          destination: (req, file, callback) => {
            callback(null, join(configService.get<string>('DIR_DATA'), 'tmp'));
          },
          filename(req, file, callback) {
            callback(null, file.fieldname + '_' + nanoid(9));
          },
        }),
      }),
      inject: [ConfigService],
    }),
    MicroservicesModule,
  ],
  providers: [ImageMediaService, ObjectMediaService, TextMediaService],
  exports: [
    MulterModule,
    ImageMediaService,
    ObjectMediaService,
    TextMediaService,
  ],
})
export class UploadModule {}
