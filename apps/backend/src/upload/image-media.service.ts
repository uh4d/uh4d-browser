import { Injectable, Logger } from '@nestjs/common';
import { v4 as uuid } from 'uuid';
import { join, parse } from 'node:path';
import * as sharp from 'sharp';
import * as fs from 'fs-extra';
import { MathUtils } from 'three';
import { ResizeImageConfig } from '@uh4d/config';
import { CreateImageFileDto } from '@backend/dto';

@Injectable()
export class ImageMediaService {
  private readonly logger = new Logger(ImageMediaService.name);

  /**
   * Process image: copy to data folder, create thumbnails, determine dimensions, etc.
   */
  async generateImageMedia(file: string): Promise<CreateImageFileDto> {
    const parsedPath = parse(file);
    const shortPath = `images/${uuid()}/`;
    const path = join(parsedPath.dir, shortPath);
    const filename = parsedPath.base;
    const filenameThumb = ResizeImageConfig.createFilename(filename, 'thumb');
    const filenamePreview = ResizeImageConfig.createFilename(
      filename,
      'preview',
    );
    const filenameTiny = ResizeImageConfig.createFilename(filename, 'tiny');
    const filenameTexture = ResizeImageConfig.createFilename(
      filename,
      'texture',
    );

    this.logger.log(`Directory: ${shortPath} File: ${filename}`);

    try {
      const image = sharp(file);

      // get image size
      const metadata = await image.metadata();

      // create directory
      await fs.ensureDir(path);

      // copy image into directory
      await fs.copy(file, join(path, filename));

      await this.generateThumbnails(image, {
        thumb: join(path, filenameThumb),
        preview: join(path, filenamePreview),
        tiny: join(path, filenameTiny),
        texture: join(path, filenameTexture),
      });

      return {
        path: shortPath,
        original: filename,
        type: parsedPath.ext.slice(1),
        preview: filenamePreview,
        thumb: filenameThumb,
        tiny: filenameTiny,
        texture: filenameTexture,
        ...this.getNormalSize(metadata),
      };
    } catch (e) {
      this.logger.warn('Unlink ' + path);
      await fs.remove(path);

      throw e;
    }
  }

  /**
   * Generate thumbnails and preview images from image file.
   * @param imageFile Full path or sharp instance
   * @param output Map with full output paths
   */
  async generateThumbnails(
    imageFile: string | sharp.Sharp,
    output: {
      thumb?: string;
      preview?: string;
      tiny?: string;
      texture?: string;
    },
  ): Promise<void> {
    const image = typeof imageFile === 'string' ? sharp(imageFile) : imageFile;

    if (output.thumb) {
      // create thumbnail
      await image
        .clone()
        .rotate()
        .resize({
          width: ResizeImageConfig.sizes.thumb.width,
          height: ResizeImageConfig.sizes.thumb.height,
          fit: ResizeImageConfig.sizes.thumb.fit,
          withoutEnlargement: true,
        })
        .toFormat(ResizeImageConfig.sizes.thumb.format, {
          quality: ResizeImageConfig.sizes.thumb.quality,
        })
        .toFile(output.thumb);
    }

    if (output.preview) {
      // down-sample preview image
      await image
        .clone()
        .rotate()
        .resize({
          width: ResizeImageConfig.sizes.preview.width,
          height: ResizeImageConfig.sizes.preview.height,
          fit: ResizeImageConfig.sizes.preview.fit,
          withoutEnlargement: true,
        })
        .toFormat(ResizeImageConfig.sizes.preview.format, {
          quality: ResizeImageConfig.sizes.preview.quality,
        })
        .toFile(output.preview);
    }

    if (output.tiny) {
      // create tiny thumbnail (for 3D preview)
      await image
        .clone()
        .rotate()
        .resize({
          width: ResizeImageConfig.sizes.tiny.width,
          height: ResizeImageConfig.sizes.tiny.height,
          fit: ResizeImageConfig.sizes.tiny.fit,
        })
        .toFormat(ResizeImageConfig.sizes.tiny.format, {
          quality: ResizeImageConfig.sizes.tiny.quality,
        })
        .toFile(output.tiny);
    }

    if (output.texture) {
      // create texture with dimensions of power of 2
      const size = this.getNormalSize(await image.metadata());
      await image
        .clone()
        .rotate()
        .resize({
          width: MathUtils.floorPowerOfTwo(
            Math.min(size.width, ResizeImageConfig.sizes.texture.width),
          ),
          height: MathUtils.floorPowerOfTwo(
            Math.min(size.height, ResizeImageConfig.sizes.texture.height),
          ),
          fit: ResizeImageConfig.sizes.texture.fit,
        })
        .toFormat(ResizeImageConfig.sizes.texture.format, {
          quality: ResizeImageConfig.sizes.texture.quality,
        })
        .toFile(output.texture);
    }
  }

  getNormalSize(metadata: sharp.Metadata): { width: number; height: number } {
    return (metadata.orientation || 0) >= 5
      ? { width: metadata.height, height: metadata.width }
      : { width: metadata.width, height: metadata.height };
  }
}
