import { Test, TestingModule } from '@nestjs/testing';
import { TextMediaService } from './text-media.service';

describe('TextMediaService', () => {
  let service: TextMediaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TextMediaService],
    }).compile();

    service = module.get<TextMediaService>(TextMediaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
