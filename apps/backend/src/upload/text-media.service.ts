import { Injectable, UnsupportedMediaTypeException } from '@nestjs/common';
import { v4 as uuid } from 'uuid';
import { join, parse } from 'node:path';
import * as fs from 'fs-extra';
import { PdfConvert } from '@brakebein/pdf2png';
import * as textToImage from 'text-to-image';
import * as sharp from 'sharp';
import { extractFileFromZip, openZipFile } from '@backend/utils/jszip';
import { CreateTextFileDto } from '@backend/dto/text-file';
import { parseBibTex } from '@backend/utils/bibtex';

@Injectable()
export class TextMediaService {
  async generateTextMediaFromPdf(file: string): Promise<CreateTextFileDto> {
    const parsedPath = parse(file);
    const shortPath = `texts/${uuid()}/`;
    const path = join(parsedPath.dir, shortPath);
    const filename = parsedPath.base;
    const filenameThumb = parsedPath.name + '_thumb.jpg';
    const filenamePreview = parsedPath.name + '_preview.jpg';

    // create directory
    await fs.ensureDir(path);

    // copy pdf file into directory
    await fs.copy(file, join(path, filename));

    await this.generateThumbnails(
      join(path, filename),
      join(path, filenamePreview),
      join(path, filenameThumb),
    );

    return {
      path: shortPath,
      original: filename,
      file: filename,
      type: parsedPath.ext.slice(1),
      preview: filenamePreview,
      thumb: filenameThumb,
      pdf: filename,
    };
  }

  async generateTextMediaFromZip(zipFile: string): Promise<CreateTextFileDto> {
    const parsedPath = parse(zipFile);
    const shortPath = `texts/${uuid()}/`;
    const path = join(parsedPath.dir, shortPath);
    const filename = parsedPath.base;
    const filenameThumb = parsedPath.name + '_thumb.jpg';
    const filenamePreview = parsedPath.name + '_preview.jpg';
    const fileTxt = parsedPath.base + '.txt';
    let pdfFile: string;
    let annotationsFile: string;
    let bibFile: string;

    // create directory
    await fs.ensureDir(path);

    // read zip file
    const zip = await openZipFile(zipFile);

    // extract txt file
    const txtResults = zip.file(/.+\.txt$/i);
    if (txtResults[0]) {
      await extractFileFromZip(txtResults[0], join(path, fileTxt));
    } else {
      throw new UnsupportedMediaTypeException(
        'Zip file does not contain one of the following formats: txt',
      );
    }

    // extract additional pdf file
    const pdfResults = zip.file(/.+\.pdf$/i);
    if (pdfResults[0]) {
      pdfFile = parsedPath.name + '.pdf';
      await extractFileFromZip(pdfResults[0], join(path, pdfFile));
    }

    // extract annotation file
    const jsonResults = zip.file(/.+\.json$/i);
    if (jsonResults[0]) {
      annotationsFile = parsedPath.name + '.json';
      await extractFileFromZip(jsonResults[0], join(path, annotationsFile));
      // check if this is valid json
      try {
        const json = await fs.readFile(join(path, annotationsFile), 'utf8');
        JSON.parse(json);
      } catch (e) {
        throw new UnsupportedMediaTypeException(
          'Invalid json extracted from zip file',
        );
      }
    }

    // extract bibtex file
    const bibResults = zip.file(/.+\.bib$/i);
    if (bibResults[0]) {
      bibFile = parsedPath.name + '.bib';
      await extractFileFromZip(bibResults[0], join(path, bibFile));
      // check if this is valid bibtex
      try {
        const bibtex = await fs.readFile(join(path, bibFile), 'utf8');
        parseBibTex(bibtex);
      } catch (e) {
        throw new UnsupportedMediaTypeException('Wrong bibtex format!', {
          cause: e,
        });
      }
    }

    await this.generateThumbnails(
      join(path, pdfFile || fileTxt),
      join(path, filenamePreview),
      join(path, filenameThumb),
    );

    return {
      path: shortPath,
      original: filename,
      file: filename,
      type: 'txt',
      preview: filenamePreview,
      thumb: filenameThumb,
      txt: fileTxt,
      pdf: pdfFile,
      json: annotationsFile,
      bib: bibFile,
    };
  }

  private async generateThumbnails(
    baseFile: string,
    previewFile: string,
    thumbFile: string,
  ): Promise<void> {
    let image: sharp.Sharp;

    if (/.+\.pdf$/i.test(baseFile)) {
      // get first page as image
      const converter = new PdfConvert(baseFile);
      const buffer = await converter.convertPageToImage(1);
      await converter.dispose();
      image = sharp(buffer);
    } else if (/.+\.txt$/i.test(baseFile)) {
      // create image from text
      const textValue = await fs.readFile(baseFile, 'utf8');
      // limit size of text to 5000 characters to avoid too big image size
      const dataUri = await textToImage.generate(textValue.slice(0, 5000), {
        fontFamily: 'Open Sans, Helvetica, Sans',
        fontSize: 20,
        lineHeight: 25,
        maxWidth: 1024,
      });
      const base64 = dataUri.substring(dataUri.indexOf('base64') + 7);
      const buffer = Buffer.from(base64, 'base64');

      image = sharp(buffer);
      const imageMeta = await image.metadata();

      if (imageMeta.height > 1448) {
        const tmpBuffer = await image
          .resize({
            width: 1024,
            height: 1448,
            fit: 'cover',
            position: 'top',
          })
          .toBuffer();
        image = sharp(tmpBuffer);
      }
    } else {
      throw new Error(
        'Cannot generate thumbnail from unsupported file: ' + baseFile,
      );
    }

    if (!image) {
      throw new Error('No image instance created');
    }

    // create thumbnail
    await image
      .clone()
      .resize({
        width: 200,
        height: 200,
        fit: 'outside',
        withoutEnlargement: true,
      })
      .jpeg({
        quality: 80,
        mozjpeg: true,
      })
      .toFile(thumbFile);

    // preview image
    await image
      .clone()
      .resize({
        width: 2048,
        height: 2048,
        fit: 'outside',
        withoutEnlargement: true,
      })
      .jpeg({
        quality: 80,
        mozjpeg: true,
      })
      .toFile(previewFile);
  }
}
