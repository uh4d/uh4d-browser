import { Injectable, UnsupportedMediaTypeException } from '@nestjs/common';
import { basename, extname } from 'path';
import { isFileEndingSupported } from '@uh4d/config';
import { ConverterClientService } from '@backend/microservices/converter-client/converter-client.service';
import { ConvertedFilesDto } from '@uh4d/dto/interfaces/custom/object';

@Injectable()
export class ObjectMediaService {
  constructor(private readonly converterClient: ConverterClientService) {}

  async generateGltfFromFile(file: { path: string; name: string }): Promise<
    {
      name: string;
      path: string;
      original: string;
    } & ConvertedFilesDto
  > {
    const fileEnding = extname(file.name);
    if (!isFileEndingSupported(fileEnding, 'model')) {
      throw new UnsupportedMediaTypeException();
    }

    const converted = await this.converterClient.sendConverterTask(
      file.path,
      file.name,
    );

    return {
      name: basename(file.name),
      path: file.path,
      original: file.name,
      ...converted,
    };
  }
}
