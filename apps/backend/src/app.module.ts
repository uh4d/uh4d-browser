import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { AppConfigModule } from '@backend/config/app-config.module';
import { RoutesModule } from '@backend/routes/routes.module';
import { UploadModule } from '@backend/upload/upload.module';
import { MicroservicesModule } from '@backend/microservices/microservices.module';
import { AuthModule } from '@backend/auth/auth.module';

@Module({
  imports: [
    AppConfigModule,
    AuthModule,
    RoutesModule,
    UploadModule,
    MicroservicesModule,
    ScheduleModule.forRoot(),
  ],
})
export class AppModule {}
