import { LocationQueryParams } from '@backend/routes/dto/location-query-params';

/**
 * Return spatial query params only if all three values (latitude, longitude, radius)
 * are set. Otherwise, return `null`.
 */
export function getSpatialParams(
  query: LocationQueryParams,
): { latitude: number; longitude: number; radius: number } | null {
  const spatialParams = {
    latitude: query.lat,
    longitude: query.lon,
    radius: query.r,
  };
  return !isNaN(spatialParams.latitude) &&
    !isNaN(spatialParams.longitude) &&
    !isNaN(spatialParams.radius)
    ? spatialParams
    : null;
}
