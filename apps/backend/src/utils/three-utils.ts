import { Box3, LineSegments, Matrix4, Mesh, Vector3 } from 'three';
import { loadGltf } from 'node-three-gltf';

/**
 * Compute center of 3D model in local Cartesian coordinates.
 * @param path Absolute path to gltf file
 * @param matrix
 */
export async function computeModelCenter(
  path: string,
  matrix = new Matrix4(),
): Promise<Vector3> {
  // load 3D model
  const gltf = await loadGltf(path);
  const obj = gltf.scene.children[0];

  const mat4 =
    matrix instanceof Matrix4 ? matrix : new Matrix4().fromArray(matrix);
  obj.applyMatrix4(mat4);
  obj.matrixAutoUpdate = false;

  // compute center
  const bbox = new Box3();
  bbox.expandByObject(obj);
  const center = bbox.getCenter(new Vector3());
  center.y = bbox.min.y;

  // dispose object
  gltf.scene.traverse((child) => {
    if (child instanceof Mesh || child instanceof LineSegments) {
      child.geometry.dispose();
      const materials = Array.isArray(child.material)
        ? child.material
        : [child.material];
      materials.forEach((mat) => mat.dispose());
    }
  });

  const origin = new Vector3().setFromMatrixPosition(mat4);
  return center.sub(origin);
}
