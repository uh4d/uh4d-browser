import {
  add,
  Duration,
  endOfMonth,
  formatISO,
  isValid,
  startOfMonth,
  sub,
} from 'date-fns';
import { ImageDateDto } from '@backend/generated/dto';

export interface PrefixConfig {
  token: string;
  minus?: Duration;
  plus?: Duration;
}

export const DATE_PARSE_PREFIXES: PrefixConfig[] = [
  {
    token: 'um',
    minus: { years: 5 },
    plus: { years: 5 },
  },
  {
    token: 'wohl',
    minus: { years: 5 },
    plus: { years: 5 },
  },
  {
    token: 'around',
    minus: { years: 5 },
    plus: { years: 5 },
  },
  {
    token: 'vor',
    minus: { years: 10 },
  },
  {
    token: 'before',
    minus: { years: 10 },
  },
  {
    token: 'nach',
    plus: { years: 10 },
  },
  {
    token: 'after',
    plus: { years: 10 },
  },
];

/**
 * Parse date and determine time-span.
 * Supported formats are:
 * - YYYY/YYYY - year span
 * - <prefix> <date>
 *
 * where <date> can be:
 * - YYYY
 * - YYYY-MM or YYYY.MM
 * - YYYY-MM-DD or YYYY.MM.DD
 * - DD-MM-YYYY or DD.MM.YYYY
 *
 * and <prefix> can be (case-insensitive):
 * - um, wohl, around
 * - vor, before
 * - nach, after
 *
 * If no date or time-span could be determined, `undefined` is returned.
 */
export function parseDate(
  value: string,
): { value: string; from: string; to: string; display: string } | undefined {
  if (!value) return undefined;

  const tokens = DATE_PARSE_PREFIXES.map((p) => p.token);
  const regex = new RegExp(
    `^(?:(${tokens.join(
      '|',
    )})\\s)?(\\d{2,4})(?:[.-](\\d{2})(?:[.-](\\d{2,4}))?|\\/(\\d{4}))?$`,
    'i',
  );
  const matches = regex.exec(value);

  if (!matches || !matches[2]) return undefined;

  let prefix = '';
  let year = '';
  let month = '';
  let day = '';
  let endYear = '';
  let display = '';

  if (
    matches[2].length === 2 &&
    matches[3].length === 2 &&
    matches[4].length === 4
  ) {
    // reverse date format DD-MM-YYYY
    [, prefix = '', day, month, year] = matches;
    display = 'YYYY.MM.DD';

    return {
      value,
      from: `${year}-${month}-${day}`,
      to: `${year}-${month}-${day}`,
      display,
    };
  }

  if (matches[2].length === 4) {
    // date format YYYY-MM-DD
    [, prefix = '', year, month = '', day = '', endYear = ''] = matches;
  }
  prefix = prefix.toLowerCase();

  if (!year) return undefined;

  let from: Date | null = null;
  let to: Date | null = null;

  if (endYear) {
    // YYYY/YYYY
    from = new Date(`${year}-01-01`);
    to = new Date(`${endYear}-12-31`);
    display = 'YYYY/YYYY';
  } else if (!month && !day) {
    // YYYY
    from = new Date(`${year}-01-01`);
    to = new Date(`${year}-12-31`);
    display = 'YYYY';
  } else if (month && !day) {
    // YYYY-MM
    from = startOfMonth(new Date(`${year}-${month}`));
    to = endOfMonth(new Date(`${year}-${month}`));
    display = 'YYYY.MM';
  } else if (month && day) {
    // YYYY-MM-DD
    from = new Date(`${year}-${month}-${day}`);
    to = new Date(`${year}-${month}-${day}`);
    display = 'YYYY.MM.DD';
  }

  if (!from || !to) return undefined;

  if (!isValid(from) || !isValid(to)) return undefined;

  // transform time-span according to prefix
  if (prefix && tokens.includes(prefix)) {
    const prefixConfig = DATE_PARSE_PREFIXES.find((p) => p.token === prefix);
    if (prefixConfig) {
      if (prefixConfig.minus) {
        from = sub(from, prefixConfig.minus);
      }
      if (prefixConfig.plus) {
        to = add(to, prefixConfig.plus);
      }
      switch (prefixConfig.token) {
        case 'um':
        case 'wohl':
        case 'around':
          display = 'around YYYY';
          break;
        case 'vor':
        case 'before':
          display = 'before YYYY';
          break;
        case 'nach':
        case 'after':
          display = 'after YYYY';
          break;
      }
    }
  }

  return {
    value,
    from: formatISO(from, { representation: 'date' }),
    to: formatISO(to, { representation: 'date' }),
    display,
  };
}

/**
 * Get the medium date of a timespan.
 */
export function getMediumDate(date: ImageDateDto) {
  if (date.from === date.to) {
    return new Date(date.from);
  }

  return new Date(
    (new Date(date.from).valueOf() + new Date(date.to).valueOf()) / 2,
  );
}
