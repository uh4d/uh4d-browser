import { Cite } from '@citation-js/core';
import '@citation-js/plugin-bibtex';
import { tidy } from 'bibtex-tidy';
import { parseDate } from '@backend/utils/date-utils';

/**
 * Parse BibTex item.
 */
export function parseBibTex(bibItem: string) {
  const ref = new Cite(bibItem).get()[0];

  return {
    bibtex: tidy(bibItem, { curly: true, space: 2, escape: false }).bibtex,
    title: ref.title,
    authors:
      ref.author?.map((author) => `${author.family}, ${author.given}`) || [],
    doi: ref.doi,
    url: ref.url,
    date: parseDate(ref.issued?.['date-parts']?.[0]?.[0] || null),
  };
}
