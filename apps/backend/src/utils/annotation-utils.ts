import { Vector2 } from 'three';

export class Line2D {
  constructor(public start = new Vector2(), public end = new Vector2()) {}

  delta(target: Vector2) {
    return target.subVectors(this.end, this.start);
  }

  distance() {
    return this.start.distanceTo(this.end);
  }

  intersectsLine(line: Line2D): boolean {
    // https://stackoverflow.com/questions/9043805/test-if-two-lines-intersect-javascript-function
    return (
      CCW(this.start, line.start, line.end) !==
        CCW(this.end, line.start, line.end) &&
      CCW(this.start, this.end, line.start) !==
        CCW(this.start, this.end, line.end)
    );
  }

  closestDistanceToPoint(point: Vector2): number {
    // https://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
    const A = point.x - this.start.x;
    const B = point.y - this.start.y;
    const C = this.end.x - this.start.x;
    const D = this.end.y - this.start.y;

    const dot = A * C + B * D;
    const len_sq = C * C + D * D;
    let param = -1;
    if (len_sq != 0)
      //in case of 0 length line
      param = dot / len_sq;

    let xx: number, yy: number;

    if (param < 0) {
      xx = this.start.x;
      yy = this.start.y;
    } else if (param > 1) {
      xx = this.end.x;
      yy = this.end.y;
    } else {
      xx = this.start.x + param * C;
      yy = this.start.y + param * D;
    }

    const dx = point.x - xx;
    const dy = point.y - yy;
    return Math.sqrt(dx * dx + dy * dy);
  }
}

function CCW(p1: Vector2, p2: Vector2, p3: Vector2) {
  return (p3.y - p1.y) * (p2.x - p1.x) > (p2.y - p1.y) * (p3.x - p1.x);
}

/**
 * Check if point is inside polygon/polylist.
 */
export function isPointInsidePolygon(
  point: Vector2,
  vertices: Vector2[],
): boolean {
  const x = point.x;
  const y = point.y;

  let inside = false;
  for (let i = 0, j = vertices.length - 1; i < vertices.length; j = i++) {
    const xi = vertices[i].x,
      yi = vertices[i].y;
    const xj = vertices[j].x,
      yj = vertices[j].y;

    const intersect =
      yi > y != yj > y && x < ((xj - xi) * (y - yi)) / (yj - yi) + xi;
    if (intersect) inside = !inside;
  }

  return inside;
}
