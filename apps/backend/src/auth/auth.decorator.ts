import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiForbiddenResponse,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { PermissionType } from '@uh4d/config';
import { AccessTokenGuard } from './jwt/access-token.guard';
import { PermissionGuard } from './permission/permission.guard';
import { UserGuard } from './user/user.guard';

/**
 * Decorator that adds `AccessTokenGuard` and auth-related Swagger decorators.
 */
export function Auth(...permissions: PermissionType[]) {
  return applyDecorators(
    SetMetadata('permissions', permissions),
    UseGuards(AccessTokenGuard, PermissionGuard, UserGuard),
    ApiBearerAuth(),
    ApiUnauthorizedResponse(),
    ApiForbiddenResponse(),
  );
}
