import { Inject, Injectable } from '@nestjs/common';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache, Milliseconds } from 'cache-manager';
import { RoleType } from '@uh4d/config';
import { GeofenceDto } from '@backend/dto';

export type UserCacheType = {
  role: RoleType;
  geofences: GeofenceDto[];
};

@Injectable()
export class UserRoleCache {
  private readonly cacheTtl: Milliseconds = 600 * 1000;

  constructor(@Inject(CACHE_MANAGER) private readonly cacheManager: Cache) {}

  async getCachedRole(userId: string): Promise<UserCacheType | null> {
    const role = await this.cacheManager.get<UserCacheType>(
      this.cacheKey(userId),
    );
    return role || null;
  }

  async setCachedRole(
    userId: string,
    role: RoleType,
    geofences: GeofenceDto[] = [],
  ): Promise<void> {
    return this.cacheManager.set(
      this.cacheKey(userId),
      { role, geofences },
      this.cacheTtl,
    );
  }

  async invalidateCachedRole(userId: string): Promise<void> {
    return this.cacheManager.del(this.cacheKey(userId));
  }

  private cacheKey(userId: string): string {
    return `user_role_${userId}`;
  }
}
