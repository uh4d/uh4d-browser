import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';
import { PermissionType, Uh4dRoles } from '@uh4d/config';
import { AuthUser } from '@backend/auth/user';
import { PermissionService } from './permission.service';

@Injectable()
export class PermissionGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly permissionService: PermissionService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const permissions = this.reflector.get<PermissionType[]>(
      'permissions',
      context.getHandler(),
    );
    const request = context.switchToHttp().getRequest();

    return this.hasUserPermission(request, permissions);
  }

  /**
   * Check if the user's role has any of the requested permissions.
   * @return Resolves `true` if at least one permission is granted.
   */
  private async hasUserPermission(
    request: Request,
    requestPermissions: PermissionType[],
  ): Promise<boolean> {
    const user = request.user as AuthUser;

    const rolePermissions = Uh4dRoles.getPermissions(user.role);
    const hasPermission = requestPermissions.some((permission) =>
      rolePermissions.includes(permission),
    );

    // get itemId if the route contains respective parameter
    const item = this.retrieveItemId(request.params);

    // when it is no item route
    if (!item) {
      // pass or reject depending on permission granted
      return hasPermission || requestPermissions.length === 0;
    }

    // grant if there are no other permission, but user owns item
    if (!hasPermission && requestPermissions.length === 0) {
      const isOwner = await this.permissionService.hasUserCreatedItem(
        user,
        item.itemId,
      );
      if (isOwner) return true;
    }

    const isModerator =
      user.isModerator() ||
      (user.isCreator() &&
        (await this.permissionService.isItemInUserGeofence(
          user,
          item.itemId,
          item.type,
        )));

    // grant if at least moderator, otherwise reject
    return hasPermission || isModerator;
  }

  private retrieveItemId(
    params?: Record<string, string>,
  ): { itemId: string; type: string } | undefined {
    if (!params) return;

    const itemKeys = ['imageId', 'objectId', 'textId', 'poiId', 'tourId'];
    const itemKey = Object.keys(params).find((key) => itemKeys.includes(key));

    if (!itemKey) return;

    return {
      itemId: params[itemKey],
      type: itemKey.replace(/Id$/, ''),
    };
  }
}
