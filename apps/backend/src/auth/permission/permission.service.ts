import { ForbiddenException, Injectable } from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { RoleType } from '@uh4d/config';
import { GeofenceDto } from '@backend/dto';
import { Location2D } from '@uh4d/dto/interfaces/custom/spatial';
import { AuthUser } from '@backend/auth/user';
import { UserCacheType, UserRoleCache } from './user-role.cache';

@Injectable()
export class PermissionService {
  constructor(
    private readonly neo4j: Neo4jService,
    private readonly userRoleCache: UserRoleCache,
  ) {}

  /**
   * Get role of user.
   */
  async getUserRole(userId: string): Promise<UserCacheType | null> {
    const cachedRole = await this.userRoleCache.getCachedRole(userId);
    if (cachedRole) return cachedRole;

    // query user from database
    // language=Cypher
    const q = `
      MATCH (user:User:UH4D { id: $userId })
      OPTIONAL MATCH (user)-[:has_geofence]->(fence:E94)
      RETURN user.id AS id, user.role AS role, collect(fence) AS geofences`;
    const user = (
      await this.neo4j.read<{
        id: string;
        role?: RoleType;
        geofences: GeofenceDto[];
      }>(q, { userId })
    )[0];

    const role = user ? user.role || 'user' : null;
    if (!role) return null;

    await this.userRoleCache.setCachedRole(userId, role, user.geofences);

    return { role, geofences: user.geofences };
  }

  /**
   * Check whether the user has at least moderator role, or the user is the uploader/owner of the item.
   */
  async canUserEditItem(user: AuthUser, itemId: string): Promise<boolean> {
    return user.isModerator() || (await this.hasUserCreatedItem(user, itemId));
  }

  /**
   * Check whether the user has at least moderator role, or the user is the uploader/owner of the item.
   * If `false`, throw Forbidden exception.
   */
  async canUserEditItemOrThrow(user: AuthUser, itemId: string): Promise<void> {
    if (!(await this.canUserEditItem(user, itemId))) {
      throw new ForbiddenException();
    }
  }

  /**
   * Check if user has uploaded/created the item/content.
   */
  async hasUserCreatedItem(user: AuthUser, itemId: string): Promise<boolean> {
    // language=Cypher
    const q = `
      MATCH (user:User:UH4D { id: $userId })<-[:uploaded_by|created_by]-(item:UH4D { id: $itemId })
      RETURN user, item
    `;

    const results = await this.neo4j.read(q, { userId: user.id, itemId });
    return !!results[0];
  }

  /**
   * Check if item is within one of the user's geofences.
   */
  async isItemInUserGeofence(
    user: AuthUser,
    itemId: string,
    type: string,
  ): Promise<boolean> {
    if (!user.geofences || user.geofences.length < 1) return false;

    const itemLocations = await this.getItemLocation(itemId, type);

    return itemLocations.some((location) => user.isWithinGeofence(location));
  }

  private async getItemLocation(
    itemId: string,
    type: string,
  ): Promise<Location2D[]> {
    const results = await this.neo4j.read<{ location: Location2D }>(
      this.getItemLocationQuery(type),
      { itemId },
    );
    return results.map((r) => r.location);
  }

  private getItemLocationQuery(type: string): string {
    switch (type) {
      case 'image':
        // language=Cypher
        return `MATCH (image:E38:UH4D {id: $itemId})<-[:P94]-(e65:E65)-[:P7]->(:E53)-[:P168]->(geo:E94)
                RETURN geo as location`;
      case 'object':
        // language=Cypher
        return `MATCH (obj:E22:UH4D {id: $itemId})<-[:P157]-(:E53)-[:P168]->(geo:E94)
                RETURN geo as location`;
      case 'text':
        // language=Cypher
        return `MATCH (text:E33:UH4D {id: $itemId})-[:P67]->(:E22)<-[:P157]-(:E53)-[:P168]->(geo:E94)
                RETURN geo as location`;
      case 'poi':
        // language=Cypher
        return `MATCH (poi:E73:UH4D {id: $itemId})-[:P67]->(:E92)-[:P161]->(:E53)-[:P168]->(geo:E94)
                RETURN geo as location`;
      case 'tour':
        // language=Cypher
        return `MATCH (tour:Tour:UH4D {id: $itemId})-[r:CONTAINS]->(:E73)-[:P67]->(:E92)-[:P161]->(:E53)-[:P168]->(geo:E94)
                RETURN geo as location`;
      default:
        throw new Error('Unsupported itemIdKey');
    }
  }
}
