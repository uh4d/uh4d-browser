import { forwardRef, Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { HttpModule } from '@nestjs/axios';
import { CacheModule } from '@nestjs/cache-manager';
import { UsersModule } from '@backend/routes/users/users.module';
import { AuthService } from './auth.service';
import { AccessTokenStrategy } from './jwt/access-token.strategy';
import { RefreshTokenStrategy } from './jwt/refresh-token.strategy';
import { AuthController } from './auth.controller';
import { SmsGatewayService } from './sms-gateway.service';
import { UserRoleCache } from './permission/user-role.cache';
import { PermissionService } from './permission/permission.service';

@Module({
  imports: [
    PassportModule,
    JwtModule.register({}),
    HttpModule,
    CacheModule.register(),
    forwardRef(() => UsersModule),
  ],
  providers: [
    AuthService,
    AccessTokenStrategy,
    RefreshTokenStrategy,
    SmsGatewayService,
    UserRoleCache,
    PermissionService,
  ],
  controllers: [AuthController],
  exports: [UserRoleCache, PermissionService],
})
export class AuthModule {}
