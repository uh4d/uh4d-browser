import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { firstValueFrom } from 'rxjs';
import { BackendConfig } from '@backend/config';

@Injectable()
export class SmsGatewayService {
  constructor(
    private readonly http: HttpService,
    private readonly config: BackendConfig,
  ) {}

  /**
   * Check if url, user, password has been set for SMS gateway.
   */
  isAvailable(): boolean {
    return !!(
      this.config.smsGatewayUrl &&
      this.config.smsGatewayUser &&
      this.config.smsGatewayPassword
    );
  }

  /**
   * Send message to user's phone via SMS.
   * @param phone Destination mobile number including country code (E.164 format)
   * @param message
   */
  sendSmsMessage(phone: string, message: string) {
    // remove '+' from phone number
    const destination = phone.replace('+', '');

    // send request
    return firstValueFrom(
      this.http.post(this.config.smsGatewayUrl, message, {
        responseType: 'text',
        params: {
          authid: this.config.smsGatewayUser,
          authcode: this.config.smsGatewayPassword,
          destination,
        },
      }),
    );
  }
}
