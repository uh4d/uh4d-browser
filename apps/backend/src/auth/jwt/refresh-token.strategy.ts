import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { Request } from 'express';
import { BackendConfig } from '@backend/config';
import { AuthUser } from '@backend/auth/user';
import { JwtPayload } from './jwt.payload';

@Injectable()
export class RefreshTokenStrategy extends PassportStrategy(
  Strategy,
  'jwt-refresh',
) {
  constructor(readonly config: BackendConfig) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: config.jwtRefreshSecret,
      passReqToCallback: true,
    });
  }

  validate(req: Request, payload: JwtPayload): AuthUser {
    const refreshToken = req.get('Authorization').replace('Bearer', '').trim();
    return new AuthUser({
      id: payload.sub,
      name: payload.username,
      refreshToken,
    });
  }
}
