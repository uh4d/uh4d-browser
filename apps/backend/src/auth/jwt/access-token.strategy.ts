import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { BackendConfig } from '@backend/config';
import { AuthUser } from '@backend/auth/user';
import { JwtPayload } from './jwt.payload';
import { PermissionService } from '../permission/permission.service';

@Injectable()
export class AccessTokenStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(
    readonly config: BackendConfig,
    private readonly permissionService: PermissionService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: config.jwtAccessSecret,
    });
  }

  async validate(payload: JwtPayload): Promise<AuthUser> {
    const userCache = await this.permissionService.getUserRole(payload.sub);
    return new AuthUser({
      id: payload.sub,
      name: payload.username,
      role: userCache.role,
      geofences: userCache.geofences,
    });
  }
}
