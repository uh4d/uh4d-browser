import { GeofenceDto } from '@backend/dto';

export type JwtPayload = {
  sub: string;
  username: string;
  role?: string;
  geofences?: GeofenceDto[];
};
