import {
  Body,
  Controller,
  Get,
  HttpCode,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiNoContentResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { CreateUserDto, UserDto } from '@backend/dto';
import { Auth } from './auth.decorator';
import { User } from './user';
import { AuthUser } from './user/auth-user';
import { AuthService } from './auth.service';
import { RefreshTokenGuard } from './jwt/refresh-token.guard';
import { TotpVerificationDto } from './dto/totp-verification.dto';
import { TokensDto } from './dto/tokens.dto';
import { RegisterResponseDto } from './dto/register-response.dto';
import { RegisterQueryParams } from './dto/register-query-params';
import { AuthDto } from './dto/auth.dto';
import { UpdatePasswordDto } from './dto/update-password.dto';
import { RequestTotpDto } from './dto/request-totp.dto';
import { RequestTotpResponseDto } from './dto/request-totp-response.dto';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOperation({
    summary: 'Register',
    description: 'Register user. A code will be sent for verification.',
  })
  @ApiOkResponse({
    type: RegisterResponseDto,
  })
  @ApiBadRequestResponse()
  @Post('register')
  async register(
    @Body() body: CreateUserDto,
    @Query() params: RegisterQueryParams,
  ): Promise<RegisterResponseDto> {
    return this.authService.register(body, params.lang);
  }

  @ApiOperation({
    summary: 'Verify code',
    description:
      'Verify TOTP code that has been sent to user to complete registration and return access tokens.',
  })
  @ApiOkResponse({
    type: TokensDto,
  })
  @ApiBadRequestResponse()
  @Post('verify')
  async verifyCode(@Body() body: TotpVerificationDto): Promise<TokensDto> {
    return this.authService.verify(body.userId, body.code);
  }

  @ApiOperation({
    summary: 'Login',
    description: 'Login with username or email and password.',
  })
  @ApiOkResponse({
    type: TokensDto,
  })
  @ApiBadRequestResponse()
  @Post('login')
  async login(@Body() body: AuthDto): Promise<TokensDto> {
    return this.authService.login(body);
  }

  @ApiOperation({
    summary: 'Logout',
  })
  @ApiNoContentResponse()
  @Auth()
  @HttpCode(204)
  @Get('logout')
  async logout(@User() user: UserDto): Promise<void> {
    return this.authService.logout(user.id);
  }

  @ApiOperation({
    summary: 'Refresh tokens',
    description: 'Refresh access and refresh token.',
  })
  @ApiBearerAuth()
  @ApiOkResponse({
    type: TokensDto,
  })
  @ApiUnauthorizedResponse()
  @UseGuards(RefreshTokenGuard)
  @Get('refresh')
  async refresh(@User() user: AuthUser): Promise<TokensDto> {
    return this.authService.refreshTokens(user.id, user.refreshToken);
  }

  @ApiOperation({
    summary: 'Request TOTP',
    description:
      'Request a new TOTP to login, if, for example, user forgot password. However, user needs to be already registered.',
  })
  @ApiOkResponse({
    type: RequestTotpResponseDto,
  })
  @ApiBadRequestResponse()
  @Post('totp')
  async requestTotp(
    @Body() body: RequestTotpDto,
    @Query() params: RegisterQueryParams,
  ): Promise<RegisterResponseDto> {
    return this.authService.requestTotp(body, params.lang);
  }

  @ApiOperation({
    summary: 'Change password',
    description: 'Update user password.',
  })
  @ApiNoContentResponse()
  @Auth()
  @HttpCode(204)
  @Patch('password')
  async changePassword(
    @User() user: AuthUser,
    @Body() body: UpdatePasswordDto,
  ): Promise<void> {
    return this.authService.updatePassword(user.id, body.password);
  }
}
