import { ApiProperty } from '@nestjs/swagger';

export class RegisterResponseDto {
  @ApiProperty({
    description: 'User ID',
    required: false,
  })
  userId: string;
  code?: string;
}
