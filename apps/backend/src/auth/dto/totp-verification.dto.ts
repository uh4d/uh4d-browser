import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class TotpVerificationDto {
  @ApiProperty({
    description: 'User ID',
  })
  @IsNotEmpty()
  @IsString()
  userId: string;

  @ApiProperty({
    description: 'TOTP verification token',
  })
  @IsNotEmpty()
  @IsString()
  code: string;
}
