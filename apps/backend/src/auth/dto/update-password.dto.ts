import { PickType } from '@nestjs/swagger';
import { CreateUserDto } from '@backend/dto';

export class UpdatePasswordDto extends PickType(CreateUserDto, ['password']) {}
