import { RegisterResponseDto } from './register-response.dto';
import { ApiProperty } from '@nestjs/swagger';

export class RequestTotpResponseDto extends RegisterResponseDto {
  @ApiProperty({
    description: 'Phone number with only last 4 digits visible',
    required: false,
  })
  maskedPhone: string;
}
