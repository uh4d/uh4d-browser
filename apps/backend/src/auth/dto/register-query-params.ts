import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class RegisterQueryParams {
  @ApiProperty({
    description:
      'Preferred language of the verification code message. Default and fallback: `en`.',
    enum: ['de', 'en'],
    required: false,
  })
  @IsOptional()
  @IsString()
  lang?: string;
}
