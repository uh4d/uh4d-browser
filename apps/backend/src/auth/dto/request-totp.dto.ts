import { PickType } from '@nestjs/swagger';
import { AuthDto } from './auth.dto';

export class RequestTotpDto extends PickType(AuthDto, ['username']) {}
