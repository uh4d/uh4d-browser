import {
  BadRequestException,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { TOTP } from 'otpauth';
import * as argon2 from 'argon2';
import { BackendConfig } from '@backend/config';
import { UsersService } from '@backend/routes/users/users.service';
import { CreateUserDto, UserDto } from '@backend/dto';
import { JwtPayload } from './jwt/jwt.payload';
import { TokensDto } from './dto/tokens.dto';
import { SmsGatewayService } from './sms-gateway.service';
import { AuthDto } from './dto/auth.dto';
import { RegisterResponseDto } from './dto/register-response.dto';
import { RequestTotpDto } from './dto/request-totp.dto';
import { RequestTotpResponseDto } from './dto/request-totp-response.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    private readonly config: BackendConfig,
    private readonly smsGatewayService: SmsGatewayService,
  ) {}

  /**
   * Add (merge) user to database and issue a TOTP token for verification.
   */
  async register(
    data: CreateUserDto,
    lang?: string,
  ): Promise<RegisterResponseDto> {
    const email = data.email.trim();
    const name = data.name.trim();
    const password = data.password.trim();

    // check if user exists
    const emailExists = await this.usersService.findByNameOrEmail(email);
    if (emailExists && emailExists.pending !== true) {
      throw new BadRequestException('User already exists');
    }
    const usernameExists = await this.usersService.findByNameOrEmail(name);
    if (usernameExists && usernameExists.pending !== true) {
      throw new BadRequestException('Username already exists');
    }

    // hash password
    const hash = await argon2.hash(password);
    const user = await this.usersService.create({
      ...data,
      email,
      name,
      password: hash,
    });

    const code = this.generateTotpToken(user);

    if (this.smsGatewayService.isAvailable()) {
      // send code via SMS gateway
      const message = this.getVerificationCodeMessage(code, lang);
      await this.smsGatewayService.sendSmsMessage(user.phone, message);

      return {
        userId: user.id,
      };
    } else {
      // otherwise return code via response body
      return {
        userId: user.id,
        code,
      };
    }
  }

  /**
   * Verify TOTP token to complete registration and issue access and refresh tokens.
   */
  async verify(userId: string, token: string): Promise<TokensDto> {
    const user = await this.usersService.getById(userId);
    if (!user) {
      throw new BadRequestException('User does not exist');
    }

    if (!this.isTotpValid(user, token)) {
      throw new BadRequestException('Invalid token');
    }

    await this.usersService.completeRegistration(userId);

    const tokens = await this.generateJwtTokens(user);
    await this.updateRefreshToken(userId, tokens.refreshToken);
    return tokens;
  }

  /**
   * Login user with name/email and password.
   */
  async login(data: AuthDto): Promise<TokensDto> {
    const username = data.username.trim();
    const password = data.password.trim();

    // check if user exists
    const user = await this.usersService.findByNameOrEmail(username);
    if (!user || user.pending === true) {
      throw new BadRequestException('Invalid credentials');
    }

    const passwordMatches = await argon2.verify(user.password, password);
    if (!passwordMatches) {
      throw new BadRequestException('Invalid credentials');
    }

    const tokens = await this.generateJwtTokens(user);
    await this.updateRefreshToken(user.id, tokens.refreshToken);
    return tokens;
  }

  /**
   * Log out user by clearing refresh token.
   */
  async logout(userId: string): Promise<void> {
    await this.usersService.updateRefreshToken(userId, null);
  }

  /**
   * Verify user and refresh token and issue new tokens.
   */
  async refreshTokens(
    userId: string,
    refreshToken: string,
  ): Promise<TokensDto> {
    const user = await this.usersService.getById(userId);
    if (!user || !user.refreshToken) {
      throw new ForbiddenException('Access Denied');
    }

    const refreshTokenMatches = await argon2.verify(
      user.refreshToken,
      refreshToken,
    );
    if (!refreshTokenMatches) {
      throw new ForbiddenException('Access Denied');
    }

    const tokens = await this.generateJwtTokens(user);
    await this.updateRefreshToken(userId, tokens.refreshToken);
    return tokens;
  }

  /**
   * Issue a new TOTP token for already registered user to login without password
   */
  async requestTotp(
    data: RequestTotpDto,
    lang?: string,
  ): Promise<RequestTotpResponseDto> {
    // check if user exists
    const user = await this.usersService.findByNameOrEmail(
      data.username.trim(),
    );
    if (!user || user.pending === true) {
      throw new BadRequestException('User does not exist');
    }

    const maskedPhone =
      user.phone.slice(0, -4).replace(/./g, '*') + user.phone.slice(-4);
    const code = this.generateTotpToken(user);

    if (this.smsGatewayService.isAvailable()) {
      // send code via SMS gateway
      const message = this.getVerificationCodeMessage(code, lang);
      await this.smsGatewayService.sendSmsMessage(user.phone, message);

      return {
        userId: user.id,
        maskedPhone,
      };
    } else {
      // otherwise return code via response body
      return {
        userId: user.id,
        maskedPhone,
        code,
      };
    }
  }

  /**
   * Update user password.
   */
  async updatePassword(userId: string, password: string): Promise<void> {
    // hash password
    const hash = await argon2.hash(password.trim());
    return this.usersService.updatePassword(userId, hash);
  }

  /**
   * Return TOTP instance with specific secret.
   */
  private getTotpInstance(secret: string): TOTP {
    return new TOTP({
      issuer: 'UH4D',
      secret,
      period: 300, // expires in 5 minutes
    });
  }

  /**
   * Generate a TOTP token with user-specific secret and that is valid for 5 minutes.
   */
  private generateTotpToken(user: UserDto): string {
    const totp = this.getTotpInstance(user.authSecret);
    return totp.generate();
  }

  /**
   * Check if TOTP for user is valid.
   */
  private isTotpValid(user: UserDto, token: string): boolean {
    const totp = this.getTotpInstance(user.authSecret);
    const delta = totp.validate({ token, window: 1 });
    return delta !== null;
  }

  /**
   * Get message for verification code.
   * @param code Verification code
   * @param lang Preferred language. Falls back to `en` (English).
   */
  private getVerificationCodeMessage(code: string, lang?: string): string {
    switch (lang) {
      case 'de':
        return `Dein Bestätigungscode (gültig für 5 Minuten): ${code}`;
      default:
        return `Your verification code (valid for 5 minutes): ${code}`;
    }
  }

  /**
   * Generate new access and refresh token with user id and name as payload.
   */
  private async generateJwtTokens(user: UserDto): Promise<TokensDto> {
    const payload: JwtPayload = {
      username: user.name,
      sub: user.id,
      role: user.role,
      geofences: user.geofences,
    };
    return {
      accessToken: this.jwtService.sign(payload, {
        secret: this.config.jwtAccessSecret,
        expiresIn: '5m',
      }),
      refreshToken: this.jwtService.sign(payload, {
        secret: this.config.jwtRefreshSecret,
        expiresIn: '14d',
      }),
    };
  }

  /**
   * Hash new refresh token and save in database.
   */
  private async updateRefreshToken(
    userId: string,
    refreshToken: string,
  ): Promise<void> {
    const hashedRefreshToken = await argon2.hash(refreshToken);
    await this.usersService.updateRefreshToken(userId, hashedRefreshToken);
  }
}
