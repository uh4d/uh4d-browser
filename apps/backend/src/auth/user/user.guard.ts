import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AuthUser } from './auth-user';

/**
 * Any route with `:userId` parameter should only be accessible by the respective user,
 * or any user with at least moderator role.
 */
@Injectable()
export class UserGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();

    const userId = request.params?.userId as string;
    if (!userId) return true;

    const authUser = request.user as AuthUser;
    return userId === authUser.id || authUser.isModerator();
  }
}
