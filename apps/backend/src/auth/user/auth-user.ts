import { RoleType } from '@uh4d/config';
import { GeofenceDto } from '@backend/dto';
import { BasicAuthUser } from '@uh4d/utils';

/**
 * Backend auth user.
 */
export class AuthUser extends BasicAuthUser {
  refreshToken?: string;

  constructor(data: {
    id: string;
    name: string;
    role?: RoleType;
    geofences?: GeofenceDto[];
    refreshToken?: string;
  }) {
    super(data);
    this.refreshToken = data.refreshToken;
  }

  needsValidation(): boolean {
    return !this.isModerator();
  }
}
