### Loading 3D models into scene

All 3D models have been converted to [glTF](https://www.khronos.org/gltf/) using Draco compression to keep the file size as small as possible and to enable a fast and efficient loading of the objects.
Use `GLTFLoader` and `DRACOLoader` to load the 3D model into scene.
Alternatively, the 3D model can also be loaded from the original asset file (e.g. OBJ).

Also refer to the [GLTFLoader docs](https://threejs.org/docs/index.html#examples/en/loaders/GLTFLoader).

<small>_All code snippets are using three.js classes._</small>

```javascript
// initialize loaders
const gltfLoader = new GLTFLoader();
const dracoLoader = new DRACOLoader();
dracoLoader.setDecoderPath('assets/gltf/');
gltfLoader.setDRACOLoader(dracoLoader);

// load object
gltfLoader.load('data/' + file.path + file.file, (gltf) => {
  
  const obj = gltf.scene.children[0];
  
  scene.add(obj);
  
});
```

The 3D model has its local coordinate system and needs to be positioned in the scene.
The `object` property of the database entry contains information regarding the position and orientation of the 3D model in the world similar to the `spatial` property of an image.
Refer to a [Exterior Orientation](#exterior-orientation) on how to build a transformation matrix.
Be aware that `object.matrix` is the transformation matrix that applies to the UH4D Browser context.

The glTF file does not only contain the meshes of the 3D model.
During the conversion to glTF, a separate edges geometry has been computed for all meshes and stored alongside within the glTF file.
This is to emphasize the edges of an object, since otherwise they are sometimes hard to see with simple color shading.

<object data="/api/assets/docs/3d-geometry.svg" type="image/svg+xml" width="60%"></object>

You can distinguish between a normal mesh and the edges object by traversing the loaded obj and looking for the `LineSegments` object:

```javascript
const obj = gltf.scene.children[0];

obj.traverse(child => {
  
  if (child instanceof Mesh) {
    
    // -> mesh object
    
  } else if (child instanceof LineSegments) {
    
    // -> edges object, no faces
    
  }
  
});
```

### Conceptual Reference Model

<object data="/api/assets/docs/objects-crm.svg" type="image/svg+xml"></object>
