In order to link an image to objects (for, e.g., filtering or projective texturing), it needs to be determined, which objects are visible from the image's camera perspective and to what extent they contribute to the image.
The latter depends on the coverage of an object in relation to the whole image and on the distance between image and object.
To this end, the objects in the vicinity, which fall in the time of the capturing date, are loaded.
Then, the scene is rendered from the camera's perspective a) with unique colors for each object and b) as a depth map.
A weight value is computed from the number of pixels that an object covers in relation to the total number of pixels in the image.
The distance is obtained from the gray value of the depth map and is stored as an additional weight value.
An object close to the camera position is ranked higher tha an object further away.
Eventually, three weight values are stored:
`coverageWeight`, `distanceWeight`, and `weight` (combination of the first two).
Each may suit a different use case when querying an image and its corresponding objects or, resp., an object and its corresponding images.

<img src="/api/assets/docs/compute-object-weights.png" style="max-width: 400px"/>

_Figure: Computing weights between (a) historical photograph and (b) 3D building object by rendering (c) color-coded objects and (d) depth map._
