Get the digital terrain model as requested by the `/elevation` route.
Use the `assetInfo.modelFile` property to build the final path.

```javascript
const url = `api/elevation${response.assetInfo.modelFile}`;
// url: api/elevation/gltf/20210722/8000352c-0002-f500-b63f-84710c7967bb.glb
```

__Note:__ The returned model is set in WebMercator coordinates.
If you want to use it in another coordinate system, you need to transform each vertex of the geometry ([formula](https://alastaira.wordpress.com/2011/01/23/the-google-maps-bing-maps-spherical-mercator-projection/)).

```javascript
import * as utm from 'utm';
import { Vector3 } from 'three';

function fromWebMercatorToUTM(x, y, altitude = 0) {

  const longitude = (x / 20037508.34) * 180;
  const latitude = 180 / Math.PI * (2 * Math.atan(Math.exp((-y / 20037508.34) * Math.PI)) - Math.PI / 2);

  const { easting, northing } = utm.fromLatLon(latitude, longitude);

  const utmPosition = new Vector3(easting, altitude, northing);

  // may be different according to your needs
  return utmPosition.multiply(new Vector3(1, 1, -1));

}

// load glTF file
const url = `https://4dbrowser.urbanhistory4d.org/api/elevation${response.assetInfo.modelFile}`;
const gltf = await loadGltf(url);
const terrain = gltf.scene.getObjectByName('TerrainNode');

// transform geometry
const attr = terrain.geometry.getAttribute('position');

for (let i = 0, l = attr.count; i < l; i++) {
  const vec = fromWebMercatorToUTM(attr.getX(i), attr.getZ(i), attr.getY(i));
  attr.setXYZ(i, vec.x, vec.y, vec.z);
}

terrain.geometry.computeBoundingBox();
terrain.geometry.computeBoundingSphere();
```
