### Conceptual Reference Model

An point of interest (POI) is considered as a _E73 Information Object_ with _E55 Type_ = `poi`.
Title and content are stored with _E35 Title_ and _E62 String_ nodes.
A reference to a _E22 Man-Made Object_ is optional.
Spatial and temporal information is connected via _E92 Spacetime Volume_.
While position is set via _E53 Place_ and _E94 Space Primitive_, temporal information is optional with _E52 Time-Span_ and _E61 Time Primitive_.

A _Tour_ can contain several POIs.
The relationship has an _order_ property to store the correct sequence of POIs.
_Tour_ entities are out of the scope of the CIDOC CRM.

<object data="/api/assets/docs/poi-crm.svg" type="image/svg+xml"></object>
