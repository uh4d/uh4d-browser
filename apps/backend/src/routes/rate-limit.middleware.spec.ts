import { rateLimitMiddleware } from './rate-limit.middleware';

describe('RateLimitMiddleware', () => {
  it('should be defined', () => {
    expect(rateLimitMiddleware).toBeDefined();
  });
});
