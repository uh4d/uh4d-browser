import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty, IsOptional } from 'class-validator';

export class UpdateValidationStatusDto {
  @ApiProperty({
    description:
      'Indicates if the entity uploaded/created by a user is in pending state and needs to be validated by moderator.',
  })
  @IsNotEmpty()
  @IsBoolean()
  pending: boolean;

  @ApiProperty({
    description: 'Indicates if the entity has been declined by moderator.',
  })
  @IsOptional()
  @IsBoolean()
  declined?: boolean;
}
