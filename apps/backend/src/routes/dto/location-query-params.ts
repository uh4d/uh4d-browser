import { ApiProperty } from '@nestjs/swagger';
import {
  IsLatitude,
  IsLongitude,
  IsNumber,
  IsOptional,
  Min,
} from 'class-validator';
import { Type } from 'class-transformer';

export class LocationQueryParams {
  @ApiProperty({
    description: 'Latitude in decimal degree',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  @IsLatitude()
  @Type(() => Number)
  lat?: number;

  @ApiProperty({
    description: 'Longitude in decimal degree',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  @IsLongitude()
  @Type(() => Number)
  lon?: number;

  @ApiProperty({
    description: 'Radius in meters',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  @Min(0)
  @Type(() => Number)
  r?: number;
}
