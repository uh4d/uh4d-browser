import {
  Injectable,
  InternalServerErrorException,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { SparqlService } from '@uh4d/backend/sparql';
import {
  GettyAATNodeDto,
  NormDataNodeWithoutWeight,
  WikiDataNodeDto,
} from './dto/norm-data-node.dto';
import { NormDataMetaDto } from './dto/norm-data-meta.dto';
import { NormDataDescriptionDto } from './dto/norm-data-description.dto';
import { UpdateNormDataDto } from './dto/update-norm-data.dto';

type UpdateReturnType = { logs: string[]; updated: Set<string> };

@Injectable()
export class NormDataService {
  private readonly logger = new Logger(NormDataService.name);

  constructor(
    private readonly neo4j: Neo4jService,
    private readonly sparqlService: SparqlService,
  ) {}

  /**
   * Query all Wikidata nodes in the database.
   */
  queryWikidataNodes(): Promise<WikiDataNodeDto[]> {
    // language=Cypher
    const q = `
      MATCH (wiki:Wikidata:UH4D)
      OPTIONAL MATCH (wiki)-[:refers_to_aat]-(aat:AAT)
      RETURN wiki.id AS id,
             wiki.identifier AS identifier,
             wiki.label AS label,
             aat
    `;

    return this.neo4j.read(q);
  }

  /**
   * Query all AAT nodes in the database.
   */
  queryAATNodes(): Promise<GettyAATNodeDto[]> {
    // language=Cypher
    const q = `
      MATCH (aat:AAT:UH4D)
      OPTIONAL MATCH (aat)-[:refers_to_aat]-(wiki:Wikidata)
      RETURN aat.id AS id,
             aat.identifier AS identifier,
             aat.label AS label,
             wiki AS wikidata
    `;

    return this.neo4j.read(q);
  }

  /**
   * For norm data node, get other corresponding norm data nodes as well as images and texts connected via annotations.
   */
  async getNormDataMeta(nodeId: string): Promise<NormDataMetaDto> {
    // language=Cypher
    const q = `
      CALL apoc.when(
        $wikiId IS NOT NULL,
        'MATCH (wiki:Wikidata:UH4D {id: $wikiId}) OPTIONAL MATCH (wiki)-[:refers_to_aat]-(aat:AAT) RETURN wiki, aat',
        'MATCH (aat:AAT:UH4D {id: $aatId}) OPTIONAL MATCH (aat)-[:refers_to_aat]-(wiki:Wikidata) RETURN wiki, aat',
        { wikiId: $wikiId, aatId: $aatId }
      ) YIELD value
      WITH value.wiki AS wiki, value.aat AS aat

      CALL {
        WITH wiki, aat
        OPTIONAL MATCH (img:E38)-[:P106]->(ann:Annotation)
        OPTIONAL MATCH (ann)-[rw:P2]->(wiki)
          WHERE rw.confidence > 0 OR rw.weight > 0.8
        OPTIONAL MATCH (ann)-[ra:P2]->(aat)
          WHERE ra.confidence > 0 OR ra.weight > 0.8
        WITH DISTINCT img, count(ann) AS numberOfAnnotations
        OPTIONAL MATCH (img)<-[:P94]->(e65:E65), (img)<-[:P67]-(file:D9), (img)-[:P102]->(title:E35)
        OPTIONAL MATCH (e65)-[:P14]->(:E21)-[:P131]->(author:E82)
        OPTIONAL MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
        RETURN collect(img{.id, title: title.value, file: apoc.map.removeKey(file, 'id'), author: author.value, date: CASE WHEN date IS NULL THEN NULL ELSE apoc.map.removeKey(date, 'id') END, numberOfAnnotations}) AS images
      }
      CALL {
        WITH wiki, aat
        OPTIONAL MATCH (text:E33)-[:P106]->(ann:Annotation)
        OPTIONAL MATCH (ann)-[rw:P2]->(wiki)
          WHERE rw.confidence > 0 OR rw.weight > 0.8
        OPTIONAL MATCH (ann)-[ra:P2]->(aat)
          WHERE ra.confidence > 0 OR ra.weight > 0.8
        WITH DISTINCT text, count(ann) AS numberOfAnnotations
        OPTIONAL MATCH (text)<-[:P94]-(e65:E65), (text)<-[:P67]-(file:D9), (text)-[:P102]->(title:E35)
        OPTIONAL MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
        OPTIONAL MATCH (e65)-[:P14]->(:E21)-[:P131]->(person:E82)
        WITH text, title, file, date, collect(person.value) AS authors, numberOfAnnotations
        RETURN collect(text{.id, title: title.value, file: apoc.map.removeKey(file, 'id'), authors, date: CASE WHEN date IS NULL THEN NULL ELSE apoc.map.removeKey(date, 'id') END, numberOfAnnotations}) AS texts
      }

      return wiki AS wikidata, aat, images, texts
    `;

    const params = {
      wikiId: nodeId.startsWith('wiki:') ? nodeId : null,
      aatId: nodeId.startsWith('aat:') ? nodeId : null,
    };

    const results = await this.neo4j.read(q, params);
    if (results[0]) {
      return results[0];
    } else {
      throw new NotFoundException();
    }
  }

  /**
   * Query description (and image) for norm data node from either Wikidata or Getty AAT SPARQL endpoint.
   */
  async getNormDataDescription(
    nodeId: string,
  ): Promise<NormDataDescriptionDto> {
    // language=Cypher
    const q = `
      MATCH (n:UH4D {id: $nodeId})
      RETURN n.id AS id,
             n.identifier AS identifier,
             n.label AS label,
             labels(n) AS labels
    `;

    const node = (await this.neo4j.read(q, { nodeId }))[0];
    if (!node) {
      throw new NotFoundException();
    }

    if (node.labels.includes('Wikidata')) {
      // language=SPARQL
      const qWiki = `
        SELECT ?desc ?image
        WHERE
        {
          BIND (wd:${node.identifier} as ?item)
          OPTIONAL {
            ?item schema:description ?desc.
            FILTER ( lang(?desc) = "en" )
          }
          OPTIONAL {
            ?item schema:description ?desc.
            FILTER ( lang(?desc) = "de" )
          }
          OPTIONAL {
              ?item wdt:P18 ?image
          }
        }
      `;
      const result = await this.sparqlService.queryWikidata<{
        desc: string;
        image: string;
      }>(qWiki);
      if (!result[0]) throw new InternalServerErrorException();
      return {
        description: result[0].desc.value,
        imageUrl: result[0].image?.value,
      };
    } else if (node.labels.includes('AAT')) {
      // language=SPARQL
      const qAAT = `
        SELECT ?desc
        WHERE
        {
          BIND (aat:${node.identifier} as ?item)
          ?item skos:scopeNote|xl:prefLabel|xl:altLabel ?descNode.
          ?descNode rdf:value ?desc.
          FILTER ( lang(?desc) = "en" )
        }
      `;
      const result = await this.sparqlService.queryGettyAAT<{
        desc: string;
      }>(qAAT);
      if (!result[0]) throw new InternalServerErrorException();
      return {
        description: result[0].desc.value,
        imageUrl: null,
      };
    }
  }

  private async updateWikidataNodes(
    forceUpdate = false,
  ): Promise<UpdateReturnType> {
    const logs: string[] = [];
    const updatedIds = new Set<string>();

    // get all wikidata nodes without label
    // language=Cypher
    const qGet = `
      MATCH (wiki:Wikidata)
      ${forceUpdate ? '' : 'WHERE wiki.label IS NULL'}
      RETURN wiki.id AS id,
             wiki.identifier AS identifier,
             wiki.label AS label
    `;

    const nodes = await this.neo4j.read<WikiDataNodeDto>(qGet);

    for (const node of nodes) {
      // query wikidata
      // language=Sparql
      const sparql = `
        SELECT ?item ?itemLabel ?aat
        WHERE
        {
          BIND (wd:${node.identifier} as ?item)
          OPTIONAL {?item wdt:P1014 ?aat. }
          SERVICE wikibase:label { bd:serviceParam wikibase:language "en,de". }
        }
      `;

      const result = (
        await this.sparqlService.queryWikidata<{
          item: string;
          itemLabel?: string;
          aat?: string;
        }>(sparql)
      )[0];

      if (!result) {
        logs.push(`${node.id} - no result`);
        continue;
      }

      // update wikidata nodes
      const label = result.itemLabel?.value;
      const aat = result.aat?.value;

      if (!label) {
        logs.push(`${node.id} - no label`);
        continue;
      }

      // language=Cypher
      const qUpdate = `
        MATCH (wiki:Wikidata:UH4D {id: $id})
        OPTIONAL MATCH (aat:AAT:UH4D {identifier: $aat})
        SET wiki.label = $label

        WITH wiki, aat
        CALL apoc.do.when(aat IS NOT NULL, 'MERGE (wiki)-[:refers_to_aat]->(aat) RETURN 1', 'RETURN 0', {wiki: wiki, aat: aat}) YIELD value

        RETURN wiki, aat
      `;
      const params = {
        id: node.id,
        label,
        aat: aat || null,
      };

      const rUpdate = (await this.neo4j.write(qUpdate, params))[0];
      if (rUpdate?.wiki) {
        updatedIds.add(node.id);
        logs.push(
          `${node.id} - label: ${rUpdate.wiki.label}${
            rUpdate.aat ? ' | connected to: ' + rUpdate.aat.id : ''
          }`,
        );
      } else {
        logs.push(`${node.id} - update failed`);
      }

      // continue after 200ms
      await new Promise((resolve) => setTimeout(resolve, 200));
    }

    this.logger.debug(`Updated Wikidata labels: ${updatedIds.size}`);

    return { logs, updated: updatedIds };
  }

  private async buildWikidataHierarchy(
    forceUpdate = false,
  ): Promise<UpdateReturnType> {
    const logs: string[] = [];
    const updatedIds = new Set<string>();

    const allNodes = await this.queryWikidataNodes();

    // query all node that are not part yet of the hierarchy
    // language=Cypher
    const qGet = `
        MATCH (wiki:Wikidata:UH4D)
          ${forceUpdate ? '' : 'WHERE NOT (wiki)-[:P127]->()'}
        RETURN wiki.id AS id,
               wiki.identifier AS identifier,
               wiki.label AS label
      `;

    const nodeQueue = await this.neo4j.read<WikiDataNodeDto>(qGet);

    for (let i = 0; i < nodeQueue.length; i++) {
      const node = nodeQueue[i];

      // query wikidata
      // language=Sparql
      const sparql = `
          SELECT ?parent ?parentLabel ?aat
          WHERE
          {
              BIND (wd:${node.identifier} as ?item)
              ?item wdt:P279 ?parent.
              OPTIONAL {?parent wdt:P1014 ?aat.}
              SERVICE wikibase:label { bd:serviceParam wikibase:language "en,de". }
          }
      `;

      const results = await this.sparqlService.queryWikidata<{
        parent: string;
        parentLabel: string;
        aat?: string;
      }>(sparql);

      if (!results.length) {
        logs.push(`${node.id} - no parents`);
        continue;
      }

      updatedIds.add(node.id);

      // merge parent in database
      const pairs = results.map((r) => {
        const identifier = r.parent.value.split('/').pop();
        return {
          childId: node.id,
          id: 'wiki:' + identifier,
          identifier,
          label: r.parentLabel.value,
          aat: r.aat?.value || null,
        };
      });

      // add new parent nodes to queue
      pairs.forEach((pair) => {
        // only add to queue if it's not already part of the database or the queue
        if (allNodes.find((n) => n.id === pair.id)) return;
        if (nodeQueue.find((n) => n.id === pair.id)) return;
        nodeQueue.push({
          id: pair.id,
          identifier: pair.identifier,
          label: pair.label,
        });
      });

      // language=Cypher
      const qMerge = `
        MATCH (child:Wikidata:UH4D {id: $childId})
        OPTIONAL MATCH (aat:AAT:UH4D {identifier: $aat})
        MERGE (parent:E55:UH4D:Wikidata {id: $id})
          ON CREATE SET parent.identifier = $identifier, parent.label = $label
        MERGE (child)-[:P127]->(parent)

        WITH child, parent, aat
        CALL apoc.do.when(aat IS NOT NULL, 'MERGE (wiki)-[:refers_to_aat]->(aat) RETURN 1', 'RETURN 0', {wiki: parent, aat: aat}) YIELD value

        RETURN child, parent
      `;

      const statements = pairs.map((pair) => ({
        statement: qMerge,
        parameters: pair,
      }));

      await this.neo4j.multipleStatements(statements);

      logs.push(
        pairs.map((p) => node.identifier + ' -> ' + p.identifier).join(', '),
      );

      // continue after 200ms
      await new Promise((resolve) => setTimeout(resolve, 200));
    }

    this.logger.debug(`Updated Wikidata hierarchy: ${updatedIds.size}`);

    return { logs, updated: updatedIds };
  }

  private async updateAATNodes(forceUpdate = false): Promise<UpdateReturnType> {
    const logs: string[] = [];
    const updatedIds = new Set<string>();

    // get all aat nodes without label
    // language=Cypher
    const qGet = `
      MATCH (aat:AAT)
      ${forceUpdate ? '' : 'WHERE aat.label IS NULL'}
      RETURN aat.id AS id,
             aat.identifier AS identifier,
             aat.label AS label
    `;

    const nodes = await this.neo4j.read<GettyAATNodeDto>(qGet);

    for (const node of nodes) {
      // query Getty AAT
      // language=Sparql
      const sparql = `
        SELECT ?item ?itemLabel
        WHERE {
          BIND (aat:${node.identifier} as ?item)
          ?item xl:prefLabel ?labelNode.
          ?labelNode xl:literalForm ?itemLabel.
          FILTER ( lang(?itemLabel) = "en" || lang(?itemLabel) = "en-us" )
        }
      `;

      const result = (
        await this.sparqlService.queryGettyAAT<{
          item: string;
          itemLabel?: string;
        }>(sparql)
      )[0];

      if (!result) {
        logs.push(`${node.id} - no result`);
        continue;
      }

      // update wikidata nodes
      const label = result.itemLabel?.value;

      if (!label) {
        logs.push(`${node.id} - no label`);
        continue;
      }

      // language=Cypher
      const qUpdate = `
        MATCH (aat:AAT:UH4D {id: $id})
        SET aat.label = $label
        RETURN aat
      `;
      const params = {
        id: node.id,
        label,
      };

      const rUpdate = (await this.neo4j.write(qUpdate, params))[0];
      if (rUpdate?.aat) {
        updatedIds.add(node.id);
        logs.push(`${node.id} - label: ${rUpdate.aat.label}`);
      } else {
        logs.push(`${node.id} - update failed`);
      }

      // continue after 200ms
      await new Promise((resolve) => setTimeout(resolve, 200));
    }

    this.logger.debug(`Updated AAT labels: ${updatedIds.size}`);

    return { logs, updated: updatedIds };
  }

  private async buildAATHierarchy(
    forceUpdate = false,
  ): Promise<UpdateReturnType> {
    const logs: string[] = [];
    const updatedIds = new Set<string>();

    // query all nodes that are not part yet of the hierarchy
    // language=Cypher
    const qGet = `
      MATCH (aat:AAT:UH4D)
      WHERE ${
        forceUpdate ? '' : 'NOT (aat)-[:P127]->() AND'
      } NOT aat.identifier = '300000000'
      RETURN aat.id AS id,
             aat.identifier AS identifier,
             aat.label AS label
    `;

    const nodes = await this.neo4j.read<GettyAATNodeDto>(qGet);

    for (const node of nodes) {
      // query all ancestors from Getty AAT
      // language=Sparql
      const sparql = `
          SELECT ?parent ?parentLabel ?type
          WHERE {
              BIND (aat:${node.identifier} as ?item)
              ?item gvp:broaderPreferred+ ?parent.
              ?parent xl:prefLabel ?labelNode.
              ?labelNode xl:literalForm ?parentLabel.
              ?parent rdf:type ?typeNode.
              ?typeNode rdfs:label ?type.
              FILTER ( lang(?parentLabel) = "en" || lang(?parentLabel) = "en-us" )
              FILTER (?type IN ("GuideTerm", "Concept", "Hierarchy", "Facet"))
          }
      `;

      const results = await this.sparqlService.queryGettyAAT<{
        parent: string;
        parentLabel: string;
        type: string;
      }>(sparql);

      if (!results.length) {
        logs.push(`${node.id} - no ancestors`);
        continue;
      }

      // merge ancestors in database
      const ancestors = results
        .filter((r) => ['Concept', 'Facet'].includes(r.type.value))
        .map((r) => ({
          identifier: r.parent.value.split('/').pop(),
          label: r.parentLabel.value,
          type: r.type.value,
        }));

      const pairs = [];

      for (let i = 0; i < ancestors.length; i++) {
        pairs.push({
          childId: i === 0 ? node.id : 'aat:' + ancestors[i - 1].identifier,
          parentId: 'aat:' + ancestors[i].identifier,
          identifier: ancestors[i].identifier,
          label: ancestors[i].label,
        });
      }
      pairs.push({
        childId: pairs[pairs.length - 1].parentId,
        parentId: 'aat:root',
        identifier: '300000000',
        label: 'Top of the AAT hierarchies',
      });

      // language=Cypher
      const qMerge = `
        MATCH (child:AAT:UH4D {id: $childId})
        MERGE (parent:E55:UH4D:AAT {id: $parentId})
          ON CREATE SET parent.identifier = $identifier, parent.label = $label
        MERGE (child)-[r:P127]->(parent)
          ON CREATE SET r.update = 1
        WITH child, parent, r, r.update AS update
        REMOVE r.update
        RETURN child, parent, update
      `;

      const statements = pairs.map((pair) => ({
        statement: qMerge,
        parameters: pair,
      }));

      const rMerge = await this.neo4j.multipleStatements(statements);

      rMerge.forEach((r) => {
        if (r[0]) {
          if (r[0].update === 1) {
            updatedIds.add(r[0].child.id);
          }
        }
      });
      logs.push(
        [node.identifier, ...pairs.map((p) => p.identifier)].join(' -> '),
      );

      // continue after 200ms
      await new Promise((resolve) => setTimeout(resolve, 200));
    }

    this.logger.debug(`Updated AAT hierarchy: ${updatedIds.size}`);

    return { logs, updated: updatedIds };
  }

  /**
   * Query semantically close identifiers from norm data hierarchies and
   * store them as relationship between identifier nodes with similarity weight.
   */
  private async computeNeighborWeights(
    updatedNodeIds: Set<string>,
    forceUpdate = false,
  ) {
    // compute weights in wikidata hierarchy
    const wikiNodes = await this.queryWikidataNodes();
    const filteredWikiNodes = forceUpdate
      ? wikiNodes
      : wikiNodes.filter((n) => updatedNodeIds.has(n.id));
    const wikiWeights = await this.computeWeightsInHierarchy(
      filteredWikiNodes,
      'wiki:Q35120',
      2,
    );
    this.logger.debug(
      `Computed Wikidata neighbor relationships: ${wikiWeights.size}`,
    );

    // compute weights in AAT hierarchy
    const aatNodes = await this.queryAATNodes();
    const filteredAatNodes = forceUpdate
      ? aatNodes
      : aatNodes.filter((n) => updatedNodeIds.has(n.id));
    const aatWeights = await this.computeWeightsInHierarchy(
      filteredAatNodes,
      'aat:root',
      2,
    );
    this.logger.debug(
      `Computed AAT neighbor relationships: ${aatWeights.size}`,
    );

    // delete old weighted relationships
    // language=Cypher
    const qDeleteOld = `
      MATCH (n)
      WHERE (n:Wikidata OR n:AAT) ${forceUpdate ? '' : 'AND n.id IN $updateIds'}
      MATCH (n)-[r:is_similar]-()
      DELETE r
    `;

    await this.neo4j.write(qDeleteOld, {
      updateIds: updatedNodeIds,
    });

    // save new relationships with weights
    // language=Cypher
    const qMergeWeight = `
      UNWIND $weights AS w
      MATCH (source:UH4D {id: w.sourceId}), (target:UH4D {id: w.targetId})
      MERGE (source)-[rel:is_similar]->(target)
      SET rel.weight = w.weight
      RETURN source.id AS sourceId, target.id AS targetId, rel.weight AS weight
    `;

    const params = {
      weights: [...wikiWeights.values(), ...aatWeights.values()].filter(
        (value) => value.weight > 0,
      ),
    };

    return this.neo4j.write(qMergeWeight, params);
  }

  private async computeWeightsInHierarchy(
    nodes: NormDataNodeWithoutWeight[],
    rootId: string,
    maxDistance: number,
  ) {
    const weightMap = new Map<
      string,
      { sourceId: string; targetId: string; weight: number }
    >();

    // iterate over nodes
    for (const node of nodes) {
      // get neighbors of node in hierarchy
      // language=Cypher
      const qNeighbors = `
        MATCH (n:UH4D {id: $nodeId})
        CALL apoc.path.expand(n, 'P127>', null, 1, $maxDistance) YIELD path
        UNWIND nodes(path) AS node
        RETURN DISTINCT node.id AS id
        UNION
        MATCH (n:UH4D {id: $nodeId})
        CALL apoc.path.expand(n, '<P127', null, 1, $maxDistance) YIELD path
        UNWIND nodes(path) AS node
        RETURN DISTINCT node.id AS id
      `;

      const rNeighbors = await this.neo4j.read<{ id: string }>(qNeighbors, {
        nodeId: node.id,
        maxDistance: maxDistance,
      });

      // iterate over neighbors of node
      for (const neighbor of rNeighbors) {
        if (neighbor.id === node.id) continue;

        const pairId1 = `${node.id}${neighbor.id}`;
        const pairId2 = `${neighbor.id}${node.id}`;

        // skip if already computed between a pair of concepts
        if (weightMap.has(pairId1) || weightMap.has(pairId2)) continue;

        // determine semantic similarity from depths in hierarchy between node and neighbor
        const similarity = await this.determineSimilarityFromDepth(
          node.id,
          neighbor.id,
          rootId,
        );

        weightMap.set(pairId1, {
          sourceId: node.id,
          targetId: neighbor.id,
          weight: similarity,
        });
      }
    }

    return weightMap;
  }

  /**
   * Compute similarity between two concepts by determining depths from the least common ancestor.
   */
  private async determineSimilarityFromDepth(
    sourceId: string,
    targetId: string,
    rootId: string,
  ): Promise<number> {
    // language=Cypher
    const qDepth = `
          MATCH (c1:UH4D {id: $sourceId})-[:P127*0..]->(p1),
                (c2:UH4D {id: $targetId})-[:P127*0..]->(p2)
            WHERE p1.id = p2.id
          MATCH path = (c1)-[:P127*0..]->(p1)
          WITH c1, c2, p1, p2, length(path) AS distance
            ORDER BY distance
            LIMIT 1
          MATCH (root:UH4D {id: $rootId})
          CALL apoc.when(
            root = p1,
            'RETURN 0 AS depth',
            'MATCH path = shortestPath((p1)-[:P127*]->(root)) RETURN length(path) AS depth',
            {root: root, p1: p1}) YIELD value
          WITH c2, p2, value.depth AS depthLCA, value.depth + distance AS depth1
          MATCH path = (c2)-[:P127*0..]->(p2)
          RETURN depthLCA, depth1, length(path) + depthLCA AS depth2
        `;

    const result = (
      await this.neo4j.read<{
        depthLCA: number;
        depth1: number;
        depth2: number;
      }>(qDepth, { sourceId, targetId, rootId })
    )[0];

    if (!result) {
      this.logger.warn(`No results for depths: ${sourceId} ${targetId}`);
      return 0;
    }

    return (2 * result.depthLCA) / (result.depth1 + result.depth2);
  }

  async updateAll(flags: UpdateNormDataDto = {}): Promise<string[]> {
    const logs: string[] = [];

    this.logger.debug('Update AAT nodes');
    const aatNodes = await this.updateAATNodes(flags.forceAATUpdate);
    logs.push(...aatNodes.logs);

    this.logger.debug('Update Wikidata nodes');
    const wikiNodes = await this.updateWikidataNodes(flags.forceWikidataUpdate);
    logs.push(...wikiNodes.logs);

    this.logger.debug('Update AAT hierarchy');
    const aatHierarchy = await this.buildAATHierarchy(flags.forceAATUpdate);
    logs.push(...aatHierarchy.logs);

    this.logger.debug('Update Wikidata hierarchy');
    const wikiHierarchy = await this.buildWikidataHierarchy(
      flags.forceWikidataUpdate,
    );
    logs.push(...wikiHierarchy.logs);

    this.logger.debug('Compute semantic neighbor weights');
    const mergedUpdatedIds = new Set([
      ...aatNodes.updated,
      ...wikiNodes.updated,
      ...aatHierarchy.updated,
      ...wikiHierarchy.updated,
    ]);
    await this.computeNeighborWeights(
      mergedUpdatedIds,
      flags.forceSemanticWeightsUpdate,
    );

    this.logger.debug('Norm data update done');

    return logs;
  }

  /**
   * Parse Wikidata and Getty AAT identifiers from string.
   * The identifiers must be prefixed by `wd:` or `aat:`.
   * Valid strings are, e.g.,
   * `architrave_wd:Q183448_aat:300001780`,
   * `arch_wdQ12277_aat300000994_006`.
   */
  parseIdentifier(str: string): {
    wikidata: string | null;
    aat: string | null;
  } {
    const identifiers = { wikidata: null, aat: null };
    if (!str) return identifiers;

    str.split('_').forEach((value) => {
      const wikiMatches = /^wd:?(Q\d+)$/.exec(value);
      const aatMatches = /^aat:?(\d+)$/.exec(value);
      if (wikiMatches?.[1]) identifiers.wikidata = wikiMatches[1];
      if (aatMatches?.[1]) identifiers.aat = aatMatches[1];
    });

    return identifiers;
  }
}
