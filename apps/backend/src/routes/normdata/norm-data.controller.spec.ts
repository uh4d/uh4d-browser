import { Test, TestingModule } from '@nestjs/testing';
import { NormDataController } from './norm-data.controller';

describe('NormDataController', () => {
  let controller: NormDataController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NormDataController],
    }).compile();

    controller = module.get<NormDataController>(NormDataController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
