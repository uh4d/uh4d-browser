import { Test, TestingModule } from '@nestjs/testing';
import { NormDataService } from './norm-data.service';

describe('NormDataService', () => {
  let service: NormDataService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NormDataService],
    }).compile();

    service = module.get<NormDataService>(NormDataService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
