import { Module } from '@nestjs/common';
import { SparqlModule } from '@uh4d/backend/sparql';
import { NormDataController } from './norm-data.controller';
import { NormDataService } from './norm-data.service';

@Module({
  imports: [SparqlModule],
  controllers: [NormDataController],
  providers: [NormDataService],
  exports: [NormDataService],
})
export class NormDataModule {}
