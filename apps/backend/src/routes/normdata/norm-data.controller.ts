import { Body, Controller, Get, Param, Post, Req } from '@nestjs/common';
import {
  ApiBody,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { Request } from 'express';
import { GettyAATNodeDto, WikiDataNodeDto } from './dto/norm-data-node.dto';
import { NormDataDescriptionDto } from './dto/norm-data-description.dto';
import { NormDataMetaDto } from './dto/norm-data-meta.dto';
import { UpdateNormDataDto } from './dto/update-norm-data.dto';
import { NormDataService } from './norm-data.service';

@ApiTags('Norm Data')
@Controller('normdata')
export class NormDataController {
  constructor(private normDataService: NormDataService) {}

  @ApiOperation({
    summary: 'Query Wikidata nodes',
  })
  @ApiOkResponse({
    type: WikiDataNodeDto,
    isArray: true,
  })
  @Get('wikidata')
  queryWikidataNodes() {
    return this.normDataService.queryWikidataNodes();
  }

  @ApiOperation({
    summary: 'Query Getty AAT nodes',
  })
  @ApiOkResponse({
    type: GettyAATNodeDto,
    isArray: true,
  })
  @Get('aat')
  queryAATNodes() {
    return this.normDataService.queryAATNodes();
  }

  @ApiOperation({
    summary: 'Get norm data meta info',
    description:
      'Get this and corresponding norm data nodes as well as images and texts connected via annotations.',
  })
  @ApiOkResponse({
    type: NormDataMetaDto,
  })
  @ApiNotFoundResponse()
  @Get(':identifier')
  getNormDataMeta(@Param('identifier') nodeId: string) {
    return this.normDataService.getNormDataMeta(nodeId);
  }

  @ApiOperation({
    summary: 'Get norm data description',
    description:
      'Query description (and image) for norm data node from either Wikidata or Getty AAT SPARQL endpoint.',
  })
  @ApiOkResponse({
    type: NormDataDescriptionDto,
  })
  @ApiNotFoundResponse()
  @Get(':identifier/description')
  getNormDataDesc(@Param('identifier') nodeId: string) {
    return this.normDataService.getNormDataDescription(nodeId);
  }

  @ApiOperation({
    summary: 'Update norm data',
    // language=Markdown
    description: `
Update Wikidata and AAT nodes and build hierarchies. The following tasks are performed:

1. __Update AAT nodes:__ Query Getty AAT SPARQL endpoint. Set label for all AAT nodes that don't have a label yet and update them.
2. __Update Wikidata nodes:__ Query Wikidata SPARQL endpoint. Set label for all Wikidata nodes that don't have a label yet and update them. Also create relationships to corresponding AAT node.
3. __Build AAT hierarchy:__ Query Getty AAT SPARQL endpoint. Get ancestors of all AAT nodes and merge them in database. Considers only AAT nodes that are of type \`Concept\` or \`Facet\`. Types \`GuideTerm\` and \`Hierarchy\` are omitted, since they are not supposed for indexing or cataloguing.
4. __Build Wikidata hierarchy:__ Query Wikidata SPARQL endpoint. Get ancestors of all Wikidata nodes and merge them in database.

Finally, semantic similarity scores are computed between each concept and its neighbors (distance = 2) and stored as weighted relationships.

These tasks will be called automatically when uploading/processing annotation data of images, texts, or objects.
`,
  })
  @ApiBody({
    type: UpdateNormDataDto,
    required: false,
  })
  @ApiOkResponse({
    description: 'List of updated nodes and hierarchy chains.',
    type: String,
    isArray: true,
  })
  @Post('update')
  updateAll(@Req() req: Request, @Body() body: UpdateNormDataDto) {
    req.setTimeout(1800000);
    return this.normDataService.updateAll(body);
  }
}
