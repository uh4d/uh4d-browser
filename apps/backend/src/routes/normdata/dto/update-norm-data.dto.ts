import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsOptional } from 'class-validator';

export class UpdateNormDataDto {
  @ApiProperty({
    description:
      'Update all Wikidata nodes, even if label and parents are already set',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsBoolean()
  forceWikidataUpdate?: boolean;
  @ApiProperty({
    description:
      'Update all AAT nodes, even if label and parents are already set',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsBoolean()
  forceAATUpdate?: boolean;
  @ApiProperty({
    description: 'Recompute semantic neighbor weights for all nodes',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsBoolean()
  forceSemanticWeightsUpdate?: boolean;
}
