import { ApiProperty, OmitType } from '@nestjs/swagger';
import { NormDataNodeDto } from '@backend/generated/dto';

export class NormDataNodeWithoutWeight extends OmitType(NormDataNodeDto, ['weight']) {}

export class WikiDataNodeDto extends NormDataNodeWithoutWeight {
  @ApiProperty({
    required: false,
    nullable: true,
  })
  aat?: NormDataNodeWithoutWeight;
}

export class GettyAATNodeDto extends NormDataNodeWithoutWeight {
  @ApiProperty({
    required: false,
    nullable: true,
  })
  wikidata?: NormDataNodeWithoutWeight;
}
