import { ApiProperty } from '@nestjs/swagger';

export class NormDataDescriptionDto {
  @ApiProperty({
    description: 'Description text retrieved from Wikidata or AAT',
    required: false,
  })
  description: string;
  @ApiProperty({
    description: 'Url to example image (only available for Wikidata nodes)',
    required: false,
    nullable: true,
  })
  imageUrl: string | null;
}
