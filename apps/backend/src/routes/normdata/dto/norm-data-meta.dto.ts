import { ApiProperty, PickType } from '@nestjs/swagger';
import { ImageDto } from '@backend/dto/image';
import { TextDto } from '@backend/dto/text';
import { NormDataNodeWithoutWeight } from './norm-data-node.dto';

class ConnectedImageDto extends PickType(ImageDto, [
  'id',
  'title',
  'file',
  'date',
  'author',
]) {
  @ApiProperty({
    description: 'Number of connected annotations',
    required: false,
  })
  numberOfAnnotations: number;
}

class ConnectedTextDto extends PickType(TextDto, [
  'id',
  'title',
  'file',
  'date',
  'authors',
]) {
  @ApiProperty({
    description: 'Number of connected annotations',
    required: false,
  })
  numberOfAnnotations: number;
}

export class NormDataMetaDto {
  @ApiProperty({
    description: 'Wikidata node',
    required: false,
    nullable: true,
  })
  wikidata?: NormDataNodeWithoutWeight;
  @ApiProperty({
    description: 'Getty AAT node',
    required: false,
    nullable: true,
  })
  aat?: NormDataNodeWithoutWeight;
  @ApiProperty({
    description: 'Connected images',
    required: false,
    type: ConnectedImageDto,
    isArray: true,
  })
  images: ConnectedImageDto[];
  @ApiProperty({
    description: 'Connected texts',
    required: false,
    type: ConnectedTextDto,
    isArray: true,
  })
  texts: ConnectedTextDto[];
}
