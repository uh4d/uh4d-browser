import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import { LocationQueryParams } from '@backend/routes/dto/location-query-params';

export class OsmQueryParams extends LocationQueryParams {
  @ApiProperty({
    description:
      'Terrain ID, if custom terrain model, otherwise path to gltf file (cached model from Elevation API with Web Mercator coordinates)',
  })
  @IsNotEmpty()
  @IsString()
  terrain: string;
}
