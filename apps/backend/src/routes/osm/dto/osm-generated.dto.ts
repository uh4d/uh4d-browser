import { ApiProperty } from '@nestjs/swagger';
import { LocationDto } from '@backend/dto';

export class OsmGeneratedDto {
  @ApiProperty({
    description: 'Geographic position of the center of the geometries.',
    type: () => LocationDto,
    required: false,
  })
  location: LocationDto;

  @ApiProperty({
    description: 'Filename of cached glb file',
    required: false,
  })
  cachedFile: string;
}
