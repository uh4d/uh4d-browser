import {
  Controller,
  Get,
  NotFoundException,
  Param,
  Query,
  Res,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';
import { join } from 'node:path';
import { createReadStream, pathExists } from 'fs-extra';
import { Response } from 'express';
import { roundDecimalDegree } from '@backend/utils/round-decimal-degree';
import { OsmQueryParams } from './dto/osm-query-params';
import { OsmService } from './osm.service';
import { OsmGeneratedDto } from './dto/osm-generated.dto';

@ApiTags('OSM')
@Controller('osm')
export class OsmController {
  constructor(
    private readonly config: ConfigService,
    private readonly osmService: OsmService,
  ) {}

  @ApiOperation({
    summary: 'Get OSM glb file information',
    description:
      'Look for cached glb file. If not existent, generate new file from OSM data retrieved via Overpass API',
  })
  @ApiOkResponse({
    type: OsmGeneratedDto,
  })
  @ApiBadRequestResponse()
  @Get()
  async getOsmGeneratedData(
    @Query() queryParams: OsmQueryParams,
  ): Promise<OsmGeneratedDto> {
    // round position
    const r = Math.round(queryParams.r);
    const lat = roundDecimalDegree(queryParams.lat, r / 10);
    const lon = roundDecimalDegree(queryParams.lon, r / 10);

    const cachedBody = await this.osmService.getOsmGeneratedDto({
      ...queryParams,
      lat,
      lon,
      r,
    });

    // send response body for cached file
    if (cachedBody) return cachedBody;

    // generate osm buildings
    return this.osmService.generateOsmBuildingsFile({
      ...queryParams,
      lat,
      lon,
      r,
    });
  }

  @ApiOperation({
    summary: 'Get OSM glb file',
    description: 'Get cached glb file of generated OSM buildings',
  })
  @ApiOkResponse({
    schema: {
      type: 'string',
      format: 'binary',
    },
  })
  @ApiNotFoundResponse()
  @Get(':file')
  async getOsmFile(
    @Param('file') file: string,
    @Res() res: Response,
  ): Promise<void> {
    const cacheFile = join(this.config.get<string>('DIR_CACHE'), 'osm', file);

    if (!(await pathExists(cacheFile))) {
      throw new NotFoundException();
    }

    // send cached file
    res.writeHead(200, {
      'Content-Type': 'application/octet-stream',
    });
    const readStream = createReadStream(cacheFile);
    readStream.pipe(res);
  }
}
