import { BadRequestException, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { join } from 'node:path';
import { ensureDir, move, pathExists, remove } from 'fs-extra';
import { nanoid } from 'nanoid';
import { OverpassService } from '@backend/routes/overpass/overpass.service';
import { TerrainService } from '@backend/routes/terrain/terrain.service';
import { ConverterClientService } from '@backend/microservices/converter-client/converter-client.service';
import { OsmGenerationPayload } from '@uh4d/dto/interfaces/custom/osm';
import { OsmGeneratedDto } from './dto/osm-generated.dto';
import { OsmQueryParams } from './dto/osm-query-params';

@Injectable()
export class OsmService {
  constructor(
    private readonly config: ConfigService,
    private readonly overpassService: OverpassService,
    private readonly terrainService: TerrainService,
    private readonly converterClient: ConverterClientService,
  ) {}

  private getCacheFilename(query: OsmQueryParams): string {
    return `${query.terrain.split('/').pop()}-${query.lat}-${query.lon}-${
      query.r
    }.glb`;
  }

  /**
   * Check if cache file exists and return response body.
   * @param query Query params already rounded
   */
  async getOsmGeneratedDto(
    query: OsmQueryParams,
  ): Promise<OsmGeneratedDto | undefined> {
    const filename = this.getCacheFilename(query);
    const cacheFile = join(
      this.config.get<string>('DIR_CACHE'),
      'osm',
      filename,
    );

    if (!(await pathExists(cacheFile))) return;

    return {
      location: {
        latitude: query.lat,
        longitude: query.lon,
        altitude: 0,
      },
      cachedFile: filename,
    };
  }

  /**
   * Query Overpass API and generate glb file from OSM data.
   * @param query Query params already rounded
   */
  async generateOsmBuildingsFile(
    query: OsmQueryParams,
  ): Promise<OsmGeneratedDto> {
    // generate OSM buildings glb
    const osmData = await this.overpassService.getOsmData(query);

    const threadData: OsmGenerationPayload = {
      lat: query.lat,
      lon: query.lon,
      folder: join(this.config.get<string>('DIR_DATA'), 'tmp', nanoid(9)),
      osmData,
      terrainPath: '',
    };

    // if `terrain` is just an ID and not a path with slashes
    if (query.terrain.split('/').length === 1) {
      // custom terrain
      const terrainData = await this.terrainService.getById(query.terrain);
      if (!terrainData) {
        throw new BadRequestException(
          `Terrain with ID ${query.terrain} not found!`,
        );
      }

      threadData.terrainData = terrainData;
      threadData.terrainPath = join(
        this.config.get<string>('DIR_DATA'),
        terrainData.file.path,
        terrainData.file.file,
      );
      threadData.isCustomTerrain = true;
    } else {
      // elevation api terrain
      const modelPath = join(
        this.config.get<string>('DIR_CACHE'),
        'elevation',
        query.terrain.replace(/.+\/gltf\//, ''),
      );
      if (!(await pathExists(modelPath)))
        throw new BadRequestException("Path to model file couldn't be found!");

      threadData.terrainPath = modelPath;
    }

    try {
      // create temporary directory
      await ensureDir(threadData.folder);

      // convert osm data to glb file
      const converted = await this.converterClient.sendOsmConverterTask(
        threadData,
      );

      const filename = this.getCacheFilename(query);
      const cacheFile = join(
        this.config.get<string>('DIR_CACHE'),
        'osm',
        filename,
      );
      await move(join(threadData.folder, converted.file), cacheFile);

      return await this.getOsmGeneratedDto(query);
    } finally {
      await remove(threadData.folder);
    }
  }
}
