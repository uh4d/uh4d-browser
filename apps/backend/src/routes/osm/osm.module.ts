import { Module } from '@nestjs/common';
import { MicroservicesModule } from '@backend/microservices/microservices.module';
import { OverpassModule } from '@backend/routes/overpass/overpass.module';
import { TerrainModule } from '@backend/routes/terrain/terrain.module';
import { OsmController } from './osm.controller';
import { OsmService } from './osm.service';

@Module({
  imports: [MicroservicesModule, OverpassModule, TerrainModule],
  controllers: [OsmController],
  providers: [OsmService],
})
export class OsmModule {}
