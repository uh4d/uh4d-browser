import { NextFunction, Request, Response } from 'express';
import { Subject } from 'rxjs';
import { rateLimit } from '@uh4d/utils';

/**
 * Middleware to rate limit requests.
 * @param limit Number of requests
 * @param window Rolling window duration (in milliseconds)
 */
export function rateLimitMiddleware(limit: number, window: number) {
  const subject = new Subject<NextFunction>();

  subject.pipe(rateLimit(limit, window)).subscribe((next) => {
    next();
  });

  return (req: Request, res: Response, next: NextFunction) => {
    subject.next(next);
  };
}
