import { Injectable } from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { nanoid } from 'nanoid';
import { getSpatialParams } from '@backend/utils/spatial-params';
import { AuthUser } from '@backend/auth/user';
import { GetToursQueryParams } from './dto/get-tours-query-params';
import { TourWithPoisDto } from './dto/tour.dto';
import { CreateTourDto } from './dto/create-tour.dto';

@Injectable()
export class ToursService {
  constructor(private readonly neo4j: Neo4jService) {}

  async query(query: GetToursQueryParams): Promise<TourWithPoisDto[]> {
    // parse query parameter
    const spatialParams = getSpatialParams(query);

    // language=Cypher
    let q = `
    MATCH (tour:Tour)-[r:CONTAINS]->(poi:E73)-[:P2]->(:E55 {id: "poi"}),
          (poi)-[:P67]->(:E92)-[:P161]->(place:E53)-[:P168]->(geo:E94),
          (poi)-[:P102]->(title:E35)
    ${query.scene ? 'WHERE (place)-[:P89]->(:E53:UH4D {id: $scene})' : ''}
    WITH tour, poi, geo, title
    ORDER BY r.order
    WITH tour, collect({id: poi.id, title: title.value, geo: geo}) AS pois`;

    if (spatialParams) {
      q += `, point({latitude: $geo.latitude, longitude: $geo.longitude}) AS userPoint
        WHERE any(p IN pois WHERE distance(p.geo.point, userPoint) < $geo.radius)`;
    }

    // language=Cypher
    q += `
      OPTIONAL MATCH (tour)-[userEventU:uploaded_by]->(userU:User)-[:P131]->(usernameU:E82)

      RETURN tour.id AS id,
             tour.title AS title,
             [p IN pois | {id: p.id, title: p.title, location: apoc.map.removeKeys(p.geo, ['id', 'point'])}] as pois,
             userU{userId: userU.id, username: usernameU.value, timestamp: userEventU.timestamp} AS createdBy
    `;

    const params = {
      scene: query.scene,
      geo: spatialParams,
    };

    return this.neo4j.read(q, params);
  }

  async create(body: CreateTourDto, user: AuthUser): Promise<TourWithPoisDto> {
    // language=Cypher
    const q = `
      MATCH (user:User:UH4D { id: $userId })-[:P131]->(username:E82)
      CREATE (tour:Tour:UH4D $tourMap)-[userEvent:created_by { timestamp: datetime() }]->(user)
      WITH tour, range(0, size($pois) - 1) as indices,
           user{userId: user.id, username: username.value, timestamp: userEvent.timestamp} AS createdBy
      UNWIND indices as i
      MATCH (poi:E73 {id: $pois[i]})-[:P2]->(:E55 {id: "poi"}),
            (poi)-[:P102]->(pTitle:E35),
            (poi)-[:P67]->(:E92)-[:P161]->(place:E53)-[:P168]->(geo:E94)
      CREATE (tour)-[:CONTAINS {order: i}]->(poi)
      WITH tour, createdBy, collect({id: poi.id, title: pTitle.value, geo: geo}) as pois
      RETURN tour.id AS id,
             tour.title AS title,
             [p IN pois | {id: p.id, title: p.title, location: apoc.map.removeKeys(p.geo, ['id', 'point'])}] as pois,
             createdBy
    `;

    const params = {
      userId: user.id,
      tourMap: {
        id: 'tour_' + nanoid(9),
        title: body.title,
      },
      pois: body.pois,
    };

    return (await this.neo4j.write(q, params))[0];
  }

  /**
   * Remove tour.
   * @return Resolves with `true` if operation was successful.
   */
  async remove(tourId: string): Promise<boolean> {
    // language=Cypher
    const q = `
      MATCH (poi:E73)<-[:CONTAINS]-(tour:Tour {id: $id})
      DETACH DELETE tour
      RETURN true AS check
    `;

    const params = {
      id: tourId,
    };

    return !!(await this.neo4j.write(q, params))[0];
  }
}
