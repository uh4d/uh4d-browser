import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNotEmpty, IsString } from 'class-validator';

export class CreateTourDto {
  @ApiProperty({
    description: 'Title of tour',
  })
  @IsNotEmpty()
  @IsString()
  title: string;
  @ApiProperty({
    description: 'List of IDs of points of interest',
  })
  @IsNotEmpty()
  @IsArray()
  @IsString({ each: true })
  pois: string[];
}
