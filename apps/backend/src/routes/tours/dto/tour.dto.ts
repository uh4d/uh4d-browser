import { ApiProperty } from '@nestjs/swagger';
import { LocationDto, TourDto } from '@backend/dto';

export class TourPoi {
  @ApiProperty({
    description: 'Point of interest ID',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Title of point of interest',
    required: false,
  })
  title: string;
  @ApiProperty({
    description: 'Spatial information',
    required: false,
  })
  location: LocationDto;
}

export class TourWithPoisDto extends TourDto {
  @ApiProperty({
    description: 'List of points of interest, already in order',
    required: false,
    type: [TourPoi],
  })
  pois: TourPoi[];
}
