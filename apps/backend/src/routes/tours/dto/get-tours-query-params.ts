import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
import { LocationQueryParams } from '@backend/routes/dto/location-query-params';

export class GetToursQueryParams extends LocationQueryParams {
  @ApiProperty({
    description: 'Filter by specific scene',
    required: false,
  })
  @IsOptional()
  @IsString()
  scene?: string;
}
