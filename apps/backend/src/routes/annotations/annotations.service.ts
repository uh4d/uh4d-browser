import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { ParsedImageAnnotationDto } from '@backend/routes/images/image-annotations/dto/parsed-image-annotation.dto';
import { ObjectAnnotationDto, TextAnnotationDto } from '@backend/dto';
import {
  CombinedSimilarAnnotationsDto,
  SimilarAnnotationsRecord,
} from '@uh4d/dto/interfaces/custom/annotation';
import { SimilarAnnotationsDto } from './dto/similar-annotations.dto';

@Injectable()
export class AnnotationsService {
  private readonly logger = new Logger(AnnotationsService.name);

  constructor(private readonly neo4j: Neo4jService) {}

  async getAnnotationById(
    annotationId: string,
  ): Promise<
    ParsedImageAnnotationDto | TextAnnotationDto | ObjectAnnotationDto
  > {
    // language=Cypher
    const q = `
      MATCH (ann:Annotation {id: $id})
      OPTIONAL MATCH (ann)-[aatRel:P2]->(aat:AAT)
        WHERE aatRel.confidence IS NOT NULL
      WITH ann, aatRel, aat ORDER BY aatRel.confidence DESC
      WITH ann, collect(aat{.id, .identifier, .label, weight: aatRel.weight}) AS aat
      OPTIONAL MATCH (ann)-[wikiRel:P2]->(wd:Wikidata)
        WHERE wikiRel.confidence IS NOT NULL
      WITH ann, aat, wikiRel, wd ORDER BY wikiRel.confidence DESC
      WITH ann, aat, collect(wd{.id, .identifier, .label, weight: wikiRel.weight}) AS wikidata
      RETURN apoc.map.removeKeys(ann, ['embedding', 'embeddingAat', 'embeddingWiki', 'embeddingSpatial']) AS annotation,
             ann.polylist AS polylist,
             ann.bbox AS bbox,
             ann.area AS area,
             aat,
             wikidata
    `;
    const params = {
      id: annotationId,
    };

    const result = (await this.neo4j.read(q, params))[0];

    if (!result) throw new NotFoundException();

    return {
      ...result.annotation,
      aat: result.aat,
      wikidata: result.wikidata,
    };
  }

  private async querySimilarDataByAnnotations(
    cypher: string,
    annotationIds: string[],
  ): Promise<CombinedSimilarAnnotationsDto[]> {
    // run query for each annotation
    const promises = annotationIds.map((annotationId) =>
      this.neo4j
        .read<SimilarAnnotationsRecord>(cypher, { annotationId })
        .then((records) => ({ annotationId, items: records })),
    );
    const results = await Promise.all(promises);
    const length = results.length;

    // compose objects
    const map = new Map<string, CombinedSimilarAnnotationsDto>();
    results.forEach(({ annotationId, items }) => {
      items.forEach((item) => {
        if (!map.has(item.id)) {
          map.set(item.id, {
            id: item.id,
            combinedSimilarity: 0,
            annotationRecords: {},
          });
        }
        const record = map.get(item.id);
        record.combinedSimilarity += item.maxSimilarity / length;
        record.annotationRecords[annotationId] = {
          annotations: item.annotations,
          maxSimilarity: item.maxSimilarity,
        };
      });
    });

    return [...map.values()];
  }

  async querySimilarImagesByAnnotations(annotationIds: string[]) {
    // language=Cypher
    const q = `
      MATCH (ann:Annotation:UH4D {id: $annotationId})<-[:P106|P46]-(item)
      MATCH (annImg:Annotation:E36)<-[:P106]-(img:E38)
        WHERE img <> item AND annImg.embedding IS NOT NULL
      WITH img, annImg, gds.similarity.cosine(ann.embedding, annImg.embedding) AS similarity
        WHERE toString(similarity) <> 'NaN'
      WITH DISTINCT img, collect(annImg{.id, similarity: similarity}) AS annotations, max(similarity) AS maxSimilarity
        ORDER BY maxSimilarity DESC
      RETURN img.id AS id, annotations, maxSimilarity
    `;

    return this.querySimilarDataByAnnotations(q, annotationIds);
  }

  async querySimilarTextsByAnnotations(annotationIds: string[]) {
    // language=Cypher
    const q = `
      MATCH (ann:Annotation:UH4D {id: $annotationId})<-[:P106|P46]-(item)
      MATCH (annText:Annotation:E33)<-[:P106]-(text:E33)
        WHERE text <> item AND annText.embedding IS NOT NULL
      WITH text, annText, gds.similarity.cosine(ann.embedding, annText.embedding) AS similarity
        WHERE toString(similarity) <> 'NaN'
      WITH DISTINCT text, collect(annText{.id, similarity: similarity}) AS annotations, max(similarity) AS maxSimilarity
        ORDER BY maxSimilarity DESC
      RETURN text.id AS id, annotations, maxSimilarity
    `;

    return this.querySimilarDataByAnnotations(q, annotationIds);
  }

  async querySimilarAnnotations(
    annotationId: string,
  ): Promise<SimilarAnnotationsDto> {
    // language=Cypher
    const qImage = `
      MATCH (ann:Annotation:UH4D {id: $annotationId})<-[:P106|P46]-(item)
      MATCH (annImg:Annotation:E36)<-[:P106]-(img:E38)
        WHERE img <> item AND annImg.embedding IS NOT NULL
      WITH img, annImg, gds.similarity.cosine(ann.embedding, annImg.embedding) AS similarity
        WHERE toString(similarity) <> 'NaN'
      WITH DISTINCT img, collect(annImg{.id, similarity: similarity}) AS annotations, max(similarity) AS maxSimilarity
        ORDER BY maxSimilarity DESC
      MATCH (img)-[:P102]->(title:E35), (img)<-[:P67]-(file:D9)
      RETURN img.id AS id, title.value AS title, apoc.map.removeKey(file, 'id') AS file, annotations, maxSimilarity
    `;

    // language=Cypher
    const qText = `
      MATCH (ann:Annotation:UH4D {id: $annotationId})<-[:P106|P46]-(item)
      MATCH (annText:Annotation:E33)<-[:P106]-(text:E33)
        WHERE text <> item AND annText.embedding IS NOT NULL
      WITH text, annText, gds.similarity.cosine(ann.embedding, annText.embedding) AS similarity
        WHERE toString(similarity) <> 'NaN'
      WITH DISTINCT text, collect(annText{.id, similarity: similarity}) AS annotations, max(similarity) AS maxSimilarity
        ORDER BY maxSimilarity DESC
      MATCH (text)-[:P102]->(title:E35), (text)<-[:P67]-(file:D9)
      RETURN text.id AS id, title.value AS title, apoc.map.removeKey(file, 'id') AS file, annotations, maxSimilarity
    `;

    // language=Cypher
    const qObject = `
      MATCH (ann:Annotation:UH4D {id: $annotationId})<-[:P106|P46]-(item)
      MATCH (annObj:Annotation:E22)<-[:P46]-(obj:E22)
        WHERE obj <> item AND annObj.embedding IS NOT NULL
      WITH obj, annObj, gds.similarity.cosine(ann.embedding, annObj.embedding) AS similarity
        WHERE toString(similarity) <> 'NaN'
      WITH DISTINCT obj, collect(annObj{.id, similarity: similarity}) AS annotations, max(similarity) AS maxSimilarity
        ORDER BY maxSimilarity DESC
      MATCH (obj)-[:P1]->(name:E41), (obj)<-[:P67]-(file:D9)
      RETURN obj.id AS id, name.value AS title, apoc.map.removeKey(file, 'id') AS file, annotations, maxSimilarity
    `;

    const params = {
      annotationId,
    };

    const resultsImages = await this.neo4j.read(qImage, params);
    const resultsTexts = await this.neo4j.read(qText, params);
    const resultsObjects = await this.neo4j.read(qObject, params);

    return {
      images: resultsImages,
      texts: resultsTexts,
      objects: resultsObjects,
    };
  }

  /**
   * Query semantically close identifiers from norm data hierarchies and
   * store relationship between annotation and identifier nodes with similarity weight.
   */
  async computeSemanticNeighborWeights(annotationId: string) {
    // delete old relationships
    // language=Cypher
    const qDeleteOld = `
      MATCH (ann:Annotation:UH4D {id: $annotationId})
      OPTIONAL MATCH (ann)-[rel:P2]->(:E55)
        WHERE rel.confidence IS NULL
      DELETE rel
    `;

    await this.neo4j.write(qDeleteOld, { annotationId });

    // query hierarchy weights
    // language=Cypher
    const qQuery = `
      MATCH (ann:Annotation:UH4D {id: $annotationId})-[rType:P2]->(norm:E55)
      OPTIONAL MATCH (norm)-[rSim:is_similar]-(nSim:E55)
      RETURN norm.id AS typeId, rType.confidence AS confidence, collect(nSim{.id, weight: rSim.weight}) AS neighbors
    `;

    const results = await this.neo4j.read<{
      typeId: string;
      confidence: number;
      neighbors: { id: string; weight: number }[];
    }>(qQuery, { annotationId });

    // merge weights
    const weightedTypes: { typeId: string; weight: number }[] = [];
    results.forEach((value) => {
      weightedTypes.push({ typeId: value.typeId, weight: value.confidence });
    });
    results.forEach((value) => {
      value.neighbors.forEach((n) => {
        const existentWeight = weightedTypes.find((w) => w.typeId === n.id);
        if (existentWeight) {
          existentWeight.weight = Math.max(
            n.weight * value.confidence,
            existentWeight.weight,
          );
        } else {
          weightedTypes.push({
            typeId: n.id,
            weight: n.weight * value.confidence,
          });
        }
      });
    });

    // save new weights
    // language=Cypher
    const qUpdate = `
      MATCH (ann:Annotation:UH4D {id: $annotationId})
      UNWIND $weightedTypes AS wt
      MATCH (tNode:UH4D {id: wt.typeId})
      MERGE (ann)-[rel:P2]->(tNode)
      SET rel.weight = wt.weight

      RETURN tNode.id AS id, rel.weight AS weight
    `;

    const params = {
      annotationId,
      weightedTypes: weightedTypes.filter((value) => value.weight > 0),
    };

    return this.neo4j.write(qUpdate, params);
  }

  /**
   * Compute embeddings via Fast Random Projection (FastPR)
   */
  async computeEmbeddings() {
    const logs: { operation: string; result: any }[] = [];

    this.logger.debug('Compute embeddings started...');

    // language=Cypher
    const qProject = `
      CALL gds.graph.project(
        'annotationContext',
        ['Annotation', 'Wikidata', 'AAT'],
        {
          P2: {
            orientation: 'NATURAL',
            properties: ['weight']
          },
          is_close_to: {
            orientation: 'UNDIRECTED',
            properties: ['weight']
          }
        }
      )
    `;
    const logProject = await this.neo4j.read(qProject);
    logs.push({
      operation: 'gds.graph.project',
      result: logProject[0],
    });
    this.logger.debug(
      `(1) gds.graph.project - ${this.sumMilliseconds(logProject[0])} ms`,
    );

    // language=Cypher
    const qAATEmbeddings = `
      CALL gds.fastRP.mutate('annotationContext', {
        embeddingDimension: 64,
        randomSeed: 42,
        relationshipWeightProperty: 'weight',
        nodeLabels: ['Annotation', 'AAT'],
        relationshipTypes: ['P2'],
        iterationWeights: [1.0],
        mutateProperty: 'embeddingAatTmp'
      })
    `;
    const logAAT = await this.neo4j.read(qAATEmbeddings);
    logs.push({
      operation: 'gds.fastRP.mutate embeddingAatTmp',
      result: logAAT[0],
    });
    this.logger.debug(
      `(2) gds.fastRP.mutate (AAT) - ${this.sumMilliseconds(logAAT[0])} ms`,
    );

    // language=Cypher
    const qAATEmbeddingsWrite = `
      CALL gds.fastRP.write('annotationContext', {
        embeddingDimension: 64,
        randomSeed: 42,
        relationshipWeightProperty: 'weight',
        nodeLabels: ['Annotation', 'AAT'],
        relationshipTypes: ['P2'],
        iterationWeights: [1.0],
        writeProperty: 'embeddingAat'
      })
    `;
    const logAATWrite = await this.neo4j.write(qAATEmbeddingsWrite);
    logs.push({
      operation: 'gds.fastRP.write embeddingAat',
      result: logAATWrite[0],
    });
    this.logger.debug(
      `(3) gds.fastRP.write (AAT) - ${this.sumMilliseconds(logAATWrite[0])} ms`,
    );

    // language=Cypher
    const qWikiEmbeddings = `
      CALL gds.fastRP.mutate('annotationContext', {
        embeddingDimension: 64,
        randomSeed: 42,
        relationshipWeightProperty: 'weight',
        nodeLabels: ['Annotation', 'Wikidata'],
        relationshipTypes: ['P2'],
        iterationWeights: [1.0],
        mutateProperty: 'embeddingWikiTmp'
      })
    `;
    const logWiki = await this.neo4j.read(qWikiEmbeddings);
    logs.push({
      operation: 'gds.fastRP.mutate embeddingWikiTmp',
      result: logWiki[0],
    });
    this.logger.debug(
      `(4) gds.fastRP.mutate (Wiki) - ${this.sumMilliseconds(logWiki[0])} ms`,
    );

    // language=Cypher
    const qWikiEmbeddingsWrite = `
      CALL gds.fastRP.write('annotationContext', {
        embeddingDimension: 64,
        randomSeed: 42,
        relationshipWeightProperty: 'weight',
        nodeLabels: ['Annotation', 'Wikidata'],
        relationshipTypes: ['P2'],
        iterationWeights: [1.0],
        writeProperty: 'embeddingWiki'
      })
    `;
    const logWikiWrite = await this.neo4j.write(qWikiEmbeddingsWrite);
    logs.push({
      operation: 'gds.fastRP.write embeddingWiki',
      result: logWikiWrite[0],
    });
    this.logger.debug(
      `(5) gds.fastRP.write (Wiki) - ${this.sumMilliseconds(
        logWikiWrite[0],
      )} ms`,
    );

    // language=Cypher
    const qSpatialEmbeddings = `
      CALL gds.fastRP.write('annotationContext', {
        embeddingDimension: 128,
        randomSeed: 42,
        relationshipWeightProperty: 'weight',
        nodeLabels: ['Annotation'],
        relationshipTypes: ['is_close_to'],
        iterationWeights: [1.0],
        featureProperties: ['embeddingAatTmp', 'embeddingWikiTmp'],
        propertyRatio: 1.0,
        writeProperty: 'embeddingSpatial'
      })
    `;

    const logSpatial = await this.neo4j.write(qSpatialEmbeddings);
    logs.push({
      operation: 'gds.fastRP.write embeddingSpatial',
      result: logSpatial[0],
    });
    this.logger.debug(
      `(6) gds.fastRP.write (spatial) - ${this.sumMilliseconds(
        logSpatial[0],
      )} ms`,
    );

    // language=Cypher
    const qDrop = `
      CALL gds.graph.drop('annotationContext')
    `;
    const logDrop = await this.neo4j.read(qDrop);
    logs.push({
      operation: 'gds.graph.drop',
      result: logDrop[0],
    });
    this.logger.debug(
      `(7) gds.graph.drop - ${this.sumMilliseconds(logDrop[0])} ms`,
    );

    // language=Cypher
    const qCombine = `
      MATCH (n:Annotation)
      OPTIONAL MATCH (n)-[:P2]->(type:E55)
      SET n.embedding = n.embeddingSpatial + n.embeddingWiki + n.embeddingAat
      REMOVE type.embeddingWiki, type.embeddingAat
    `;
    const rCombine = await this.neo4j.writeRaw(qCombine);
    logs.push({
      operation: 'combine embeddings, cleanup',
      result: rCombine.summary.updateStatistics.updates(),
    });

    this.logger.debug('Compute embeddings finished');

    return logs;
  }

  private sumMilliseconds(result: Record<string, any>) {
    return Object.entries(result).reduce(
      (sum, [key, value]) => sum + (/Millis$/.test(key) ? value : 0),
      0,
    );
  }
}
