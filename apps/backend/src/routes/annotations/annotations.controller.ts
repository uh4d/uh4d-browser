import { Controller, Get, Param, Post } from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  getSchemaPath,
} from '@nestjs/swagger';
import { ObjectAnnotationDto, TextAnnotationDto } from '@backend/dto';
import { ParsedImageAnnotationDto } from '@backend/routes/images/image-annotations/dto/parsed-image-annotation.dto';
import { AnnotationsService } from './annotations.service';
import { SimilarAnnotationsDto } from './dto/similar-annotations.dto';

@ApiTags('Annotations')
@Controller('annotations')
export class AnnotationsController {
  constructor(private readonly annotationsService: AnnotationsService) {}

  @ApiOperation({
    summary: 'Get annotation',
    description:
      'Get annotation by ID. Includes related AAT and Wikidata information.',
  })
  @ApiOkResponse({
    schema: {
      oneOf: [
        { $ref: getSchemaPath(ParsedImageAnnotationDto) },
        { $ref: getSchemaPath(TextAnnotationDto) },
        { $ref: getSchemaPath(ObjectAnnotationDto) },
      ],
    },
  })
  @ApiNotFoundResponse()
  @Get(':annotationId')
  getAnnotation(
    @Param('annotationId') annotationId: string,
  ): Promise<
    ParsedImageAnnotationDto | TextAnnotationDto | ObjectAnnotationDto
  > {
    return this.annotationsService.getAnnotationById(annotationId);
  }

  @ApiOperation({
    summary: 'Get similar annotations',
    description:
      'Get similar annotations with respect to the query annotation. The annotations are grouped by the record (image, text, or object).',
  })
  @ApiOkResponse({
    type: SimilarAnnotationsDto,
  })
  @Get(':annotationId/similar')
  querySimilarAnnotations(@Param('annotationId') annotationId: string) {
    return this.annotationsService.querySimilarAnnotations(annotationId);
  }

  @ApiOperation({
    summary: 'Compute embeddings',
    description:
      'Compute embeddings for annotations via Fast Random Projection (FastPR). This task will be called automatically when uploading/processing annotation data of images, texts, or objects.',
  })
  @ApiCreatedResponse({
    description: 'Logs and results from the Neo4j operations.',
  })
  @Post('embeddings')
  updateEmbeddings() {
    return this.annotationsService.computeEmbeddings();
  }
}
