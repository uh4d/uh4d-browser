import { ApiProperty } from '@nestjs/swagger';
import {
  SimilarAnnotationsDto as ISimilarAnnotations,
  SimilarRecord as ISimilarRecord,
} from '@uh4d/dto/interfaces/custom/annotation';
import { ImageFileDto, ObjectFileDto, TextFileDto } from '@backend/dto';

class RecordAnnotation {
  @ApiProperty({
    description: 'Annotation ID',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Similarity score with respect to the query annotation',
    required: false,
  })
  similarity: number;
}

class SimilarRecord implements Omit<ISimilarRecord<never>, 'file'> {
  @ApiProperty({
    description: 'Record ID',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Title/name',
    required: false,
  })
  title: string;
  @ApiProperty({
    description: 'Maximum similarity score among the annotations',
    required: false,
  })
  maxSimilarity: number;
  @ApiProperty({
    description: 'List of annotations',
    required: false,
    type: RecordAnnotation,
    isArray: true,
  })
  annotations: RecordAnnotation[];
}

class SimilarImage extends SimilarRecord {
  @ApiProperty({
    description: 'File reference',
    required: false,
    type: ImageFileDto,
  })
  file: ImageFileDto;
}

class SimilarText extends SimilarRecord {
  @ApiProperty({
    description: 'File reference',
    required: false,
    type: TextFileDto,
  })
  file: TextFileDto;
}

class SimilarObject extends SimilarRecord {
  @ApiProperty({
    description: 'File reference',
    required: false,
    type: ObjectFileDto,
  })
  file: ObjectFileDto;
}

export class SimilarAnnotationsDto implements ISimilarAnnotations {
  @ApiProperty({
    description: 'List of similar images',
    required: false,
    type: SimilarImage,
    isArray: true,
  })
  images: SimilarImage[];
  @ApiProperty({
    description: 'List of similar texts',
    required: false,
    type: SimilarText,
    isArray: true,
  })
  texts: SimilarText[];
  @ApiProperty({
    description: 'List of similar objects',
    required: false,
    type: SimilarObject,
    isArray: true,
  })
  objects: SimilarObject[];
}
