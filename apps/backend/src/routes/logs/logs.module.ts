import { Module } from '@nestjs/common';
import { LogsController } from './logs.controller';
import { LogsGateway } from './logs.gateway';
import { LogsCronjobService } from './logs-cronjob/logs-cronjob.service';

@Module({
  controllers: [LogsController],
  providers: [LogsGateway, LogsCronjobService],
})
export class LogsModule {}
