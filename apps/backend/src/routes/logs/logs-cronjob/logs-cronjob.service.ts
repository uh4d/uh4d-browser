import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { ConfigService } from '@nestjs/config';
import { ensureDir, pathExists, readdir, readJson, writeJson } from 'fs-extra';
import {
  addDays,
  differenceInMilliseconds,
  eachDayOfInterval,
  endOfDay,
  format,
  formatISO,
  isBefore,
  parse,
  startOfToday,
  subDays,
} from 'date-fns';
import { join } from 'node:path';
import { AnalyzedLogDto } from './analyzed-log.dto';

@Injectable()
export class LogsCronjobService {
  private readonly logger = new Logger(LogsCronjobService.name);

  private readonly summaryDirPath = join(
    this.config.get<string>('DIR_LOGS'),
    'summary',
  );

  constructor(private readonly config: ConfigService) {}

  // run each day at 2am in the morning
  @Cron('0 2 * * *')
  async handleCron() {
    this.logger.log('Compute log summaries');

    const today = startOfToday();

    let last = await this.getLastSummeryDate();
    if (last) {
      last = endOfDay(last);
    } else {
      last = endOfDay(subDays(today, 60));
    }

    if (!isBefore(last, today)) {
      this.logger.log('Already up-to-date');
      return;
    }

    // collect all logs from last summary until yesterday
    const daysToSummarize = new Map<string, string[]>(
      eachDayOfInterval({
        start: addDays(last, 1),
        end: subDays(today, 1),
      }).map((day) => [format(day, 'yyyyMMdd'), []]),
    );

    const logList = await this.getLogList();
    for (const log of logList) {
      const dayList = daysToSummarize.get(log.slice(0, 8));
      if (dayList) {
        dayList.push(log);
      }
    }

    await ensureDir(this.summaryDirPath);

    // iterate over days, analyze and write summary json
    for (const [key, logList] of daysToSummarize.entries()) {
      const isoString = formatISO(parse(key, 'yyyyMMdd', new Date()), {
        representation: 'date',
      });
      const data = await this.analyzeLogs(logList);
      if (data.length) {
        await writeJson(join(this.summaryDirPath, isoString + '.json'), data, {
          spaces: 2,
        });
        this.logger.log(`analyzed logs for ${isoString}`);
      }
    }
  }

  private async getLogList(): Promise<string[]> {
    const logs = await readdir(this.config.get<string>('DIR_LOGS'));
    return logs.filter((file) => /\.log$/.test(file));
  }

  private async getLastSummeryDate(): Promise<Date | undefined> {
    if (!(await pathExists(this.summaryDirPath))) return;

    const files = await readdir(this.summaryDirPath);
    const filename = files.pop();
    if (filename) {
      return new Date(filename.replace('.json', ''));
    }
  }

  private async analyzeLogs(logList: string[]) {
    const analyzed: AnalyzedLogDto[] = [];

    for (const file of logList) {
      const data = await readJson(
        join(this.config.get<string>('DIR_LOGS'), file + '.json'),
      );

      const startLog = data.logs.log.find(
        (log) => log.message === 'viewport init',
      );
      const endLog = data.logs.log.find(
        (log) => log.message === 'Event: action scene-ready',
      );
      const routerLog = data.logs.log.find((log) =>
        /Router:\s.*scene=[^\s&]+/.test(log.message),
      );
      const firstErrorLog = data.logs.errors[0];

      if (!startLog) continue;

      const time = endLog
        ? differenceInMilliseconds(endLog.timestamp, startLog.timestamp)
        : firstErrorLog
        ? differenceInMilliseconds(firstErrorLog.timestamp, data.startTime)
        : differenceInMilliseconds(data.endTime, data.startTime);

      const tags: string[] = [];
      if (firstErrorLog) tags.push('error');
      const scene = /Router:\s.*scene=([^\s&]+)/.exec(routerLog.message)?.[1];
      if (scene) tags.push(scene);
      const testFile = /Router:\s.*test=([^\s&]+)/.exec(routerLog.message)?.[1];
      if (testFile) tags.push('test', testFile);
      if (!endLog) tags.push('interrupted');
      if (data.userAgent.device.type) tags.push(data.userAgent.device.type);
      tags.push(data.userAgent.ua);
      tags.push(data.origin);

      analyzed.push({
        id: file,
        type: data.userAgent.device.type,
        size: data.size / 1024 / 1024,
        time: time / 1000,
        interrupted: !endLog,
        isTestScene: !!testFile,
        hasErrors: !!firstErrorLog,
        tags,
      });
    }

    return analyzed;
  }
}
