export interface AnalyzedLogDto {
  id: string;
  type: string;
  size: number;
  time: number;
  interrupted: boolean;
  isTestScene: boolean;
  hasErrors: boolean;
  tags: string[];
}
