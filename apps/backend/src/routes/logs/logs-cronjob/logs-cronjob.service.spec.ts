import { Test, TestingModule } from '@nestjs/testing';
import { LogsCronjobService } from './logs-cronjob.service';

describe('LogsCronjobService', () => {
  let service: LogsCronjobService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LogsCronjobService],
    }).compile();

    service = module.get<LogsCronjobService>(LogsCronjobService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
