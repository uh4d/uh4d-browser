import { OnApplicationShutdown, UsePipes } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WsException,
} from '@nestjs/websockets';
import { WebSocket } from 'ws';
import { format, formatDistanceStrict } from 'date-fns';
import { writeFile } from 'fs-extra';
import { join } from 'node:path';
import { nanoid } from 'nanoid';
import { LogMessageDto } from './dto/log-message.dto';
import { WsValidationPipe } from './ws-validation.pipe';

interface SimpleLog {
  timestamp: string;
  message: string;
}

interface RequestLog {
  timestamp: string;
  method: string;
  url: string;
  status: number;
  size: number;
  elapsedTime: number;
}

interface LogSession {
  id: string;
  origin: string;
  timestamp: string;
  userAgent?: { [key: string]: any };
  logs: {
    log: SimpleLog[];
    requests: RequestLog[];
    files: RequestLog[];
    warnings: SimpleLog[];
    errors: SimpleLog[];
    [others: string]: (SimpleLog | RequestLog)[];
  };
}

const clients = new Map<WebSocket, LogSession>();

@WebSocketGateway({
  path: '/logs',
})
export class LogsGateway
  implements OnApplicationShutdown, OnGatewayConnection, OnGatewayDisconnect
{
  constructor(private readonly config: ConfigService) {}

  handleConnection(client: WebSocket, incoming: any): void {
    const id = nanoid(9);

    clients.set(client, {
      id,
      origin: incoming.headers.origin || 'unknown',
      timestamp: new Date().toISOString(),
      logs: {
        log: [],
        requests: [],
        files: [],
        warnings: [],
        errors: [],
      },
    });

    client.send(`Connected. Session ID: ${id}`);
  }

  @UsePipes(new WsValidationPipe())
  @SubscribeMessage('message')
  handleMessage(
    @ConnectedSocket() client: WebSocket,
    @MessageBody() payload: LogMessageDto,
  ): void {
    if (['timestamp', 'exitCode'].includes(payload.type)) {
      throw new WsException(`Restricted message type: ${payload.type}`);
    }

    const data = clients.get(client);
    const timestamp = format(new Date(), "yyyy-MM-dd'T'HH:mm:ss.SSS");

    switch (payload.type) {
      case 'userAgent':
        data.userAgent = JSON.parse((payload.value as SimpleLog).message);
        break;

      default:
        if (!data.logs[payload.type]) {
          data.logs[payload.type] = [];
        }
        data.logs[payload.type].push({ timestamp, ...payload.value });
    }
  }

  async handleDisconnect(client: WebSocket): Promise<void> {
    const data = clients.get(client);
    const timestamp = new Date();

    const plainOutput = this.composePlainOutput(data, timestamp);
    const jsonOutput = this.composeJsonOutput(data, timestamp);
    const userAgent = data.userAgent;

    // compose filename
    const filename = [format(new Date(data.timestamp), 'yyyyMMdd-HHmmss-S')];

    if (userAgent) {
      if (userAgent.os.name) {
        filename.push(
          userAgent.os.name +
            (userAgent.os.version ? '-' + userAgent.os.version : ''),
        );
      }
      if (userAgent.browser.name) {
        filename.push(
          userAgent.browser.name +
            (userAgent.browser.major ? '-' + userAgent.browser.major : ''),
        );
      }
    }

    // write log file
    const filePath = join(
      this.config.get<string>('DIR_LOGS'),
      filename.join('-') + '.log',
    );
    await writeFile(filePath, plainOutput, { encoding: 'utf8' });
    await writeFile(filePath + '.json', jsonOutput, { encoding: 'utf8' });

    clients.delete(client);
  }

  private composePlainOutput(data: LogSession, timestamp: Date): string {
    let output = `SessionID: ${data.id}\n`;
    output += `Origin: ${data.origin}\n`;
    output += `Timestamp: ${data.timestamp}\n`;
    output += `Exited at ${timestamp.toISOString()} after ${formatDistanceStrict(
      timestamp,
      new Date(data.timestamp),
    )}\n`;
    output += `Size: ${parseFloat(
      (this.getDownloadSize(data) / 1024 / 1024).toFixed(3),
    )} MB\n`;

    const userAgent = data.userAgent;
    if (userAgent) {
      output += '\nUserAgent:\n----------\n';
      output += JSON.stringify(userAgent, null, 2);
    }

    for (const [key, value] of Object.entries(data.logs)) {
      const heading =
        key.replace(/^(.)/, (match, p1) => p1.toUpperCase()) + ':';
      output += `\n\n${heading}\n${heading.replace(/./g, '-')}\n`;

      value.forEach((log) => {
        if (typeof (log as SimpleLog).message === 'string') {
          const l = log as SimpleLog;
          output += `[${l.timestamp}] ${l.message}\n`;
        } else {
          const l = log as RequestLog;
          output += `[${l.timestamp}] ${l.method} ${l.url} ${l.status} - ${l.size} Bytes - ${l.elapsedTime} ms\n`;
        }
      });
    }

    return output;
  }

  private composeJsonOutput(data: LogSession, timestamp: Date): string {
    const json = {
      sessionId: data.id,
      origin: data.origin,
      startTime: data.timestamp,
      endTime: timestamp.toISOString(),
      size: this.getDownloadSize(data),
      userAgent: data.userAgent,
      logs: data.logs,
    };
    return JSON.stringify(json, null, 2);
  }

  private getDownloadSize(data: LogSession): number {
    let size = 0;
    data.logs.requests.concat(data.logs.files).forEach((log) => {
      if (log.size > 0) size += log.size;
    });
    return size;
  }

  async onApplicationShutdown(): Promise<void> {
    await Promise.all(
      [...clients.keys()].map((client) => this.handleDisconnect(client)),
    );
  }
}
