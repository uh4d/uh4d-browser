import { Injectable, ValidationPipe } from '@nestjs/common';
import { WsException } from '@nestjs/websockets';
import { ValidationError } from 'class-validator';

@Injectable()
export class WsValidationPipe extends ValidationPipe {
  createExceptionFactory(): (
    validationErrors?: ValidationError[],
  ) => WsException {
    return (validationErrors?: ValidationError[]): WsException => {
      if (this.isDetailedOutputDisabled) {
        return new WsException('Bad request');
      }
      const errors = this.flattenValidationErrors(validationErrors);
      return new WsException(errors);
    };
  }
}
