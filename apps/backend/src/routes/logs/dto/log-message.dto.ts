import { IsInt, IsNotEmpty, IsString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';

export class LogMessageDto {
  @IsNotEmpty()
  @IsString()
  type: string;
  @IsNotEmpty()
  @ValidateNested()
  @Type((data) => {
    switch (data.object.type) {
      case 'files':
      case 'requests':
        return RequestLogDto;
      default:
        return SimpleLogDto;
    }
  })
  value: SimpleLogDto | RequestLogDto;
}

class SimpleLogDto {
  @IsNotEmpty()
  @IsString()
  message: string;
}

class RequestLogDto {
  @IsNotEmpty()
  @IsString()
  method: string;
  @IsNotEmpty()
  @IsString()
  url: string;
  @IsNotEmpty()
  @IsInt()
  status: number;
  @IsNotEmpty()
  @IsInt()
  size: number;
  @IsNotEmpty()
  @IsInt()
  elapsedTime: number;
}
