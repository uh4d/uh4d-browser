import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsOptional } from 'class-validator';

export class GetLogsQueryParams {
  @ApiProperty({
    description: 'Query format',
    enum: ['html'],
    required: false,
  })
  @IsOptional()
  format?: string;

  @ApiProperty({
    description: 'Query by date (to)',
    required: false,
  })
  @IsOptional()
  @IsDateString()
  from?: string;

  @ApiProperty({
    description: 'Query by date (from)',
    required: false,
  })
  @IsOptional()
  @IsDateString()
  to?: string;
}
