import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class GetSingleLogQueryParams {
  @ApiProperty({
    description: 'Query format',
    enum: ['plain', 'json'],
    required: false,
  })
  @IsOptional()
  format?: string;
}
