import {
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  NotFoundException,
  Param,
  Query,
  Res,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiProduces,
  ApiTags,
} from '@nestjs/swagger';
import {
  createReadStream,
  pathExists,
  readdir,
  readJson,
  remove,
} from 'fs-extra';
import { join } from 'node:path';
import { Response } from 'express';
import { isWithinInterval, parse } from 'date-fns';
import { GetLogsQueryParams } from './dto/get-logs-query-params';
import { GetSingleLogQueryParams } from './dto/get-single-log-query-params';
import { AnalyzedLogDto } from './logs-cronjob/analyzed-log.dto';

@ApiTags('Logs')
@Controller('logs')
export class LogsController {
  constructor(private readonly config: ConfigService) {}

  @ApiOperation({
    summary: 'Query logs',
    description:
      'Query log files. A list of log files is returned.\n\nIf query parameter `format=html` or `format=chart` is set, it returns an HTML page, on which the log files can be viewed interactively.\n\nNarrow down results by querying by date using `from` and `to` parameters.',
  })
  @ApiOkResponse({
    content: {
      'application/json': {
        schema: { type: 'array', items: { type: 'string' } },
      },
      'text/html': {
        schema: { type: 'string' },
      },
    },
  })
  @Get()
  async query(
    @Query() queryParams: GetLogsQueryParams,
    @Res() res: Response,
  ): Promise<void> {
    // serve html files
    switch (queryParams.format) {
      case 'html': {
        const readStream = createReadStream(
          join(__dirname, 'assets/log-viewer.html'),
        );
        res.setHeader('Content-Type', 'text/html');
        readStream.pipe(res);
        return;
      }

      case 'chart': {
        const readStream = createReadStream(
          join(__dirname, 'assets/log-chart.html'),
        );
        res.setHeader('Content-Type', 'text/html');
        readStream.pipe(res);
        return;
      }
    }

    // query files and return list
    let files = await readdir(this.config.get<string>('DIR_LOGS'));

    // filter list
    if (queryParams.from && queryParams.to) {
      files = files.filter(
        (file) =>
          /\.log$/.test(file) &&
          isWithinInterval(
            parse(file.slice(0, 15), 'yyyyMMdd-HHmmss', new Date()),
            {
              start: new Date(queryParams.from),
              end: new Date(queryParams.to),
            },
          ),
      );
    } else {
      files = files.filter((file) => /\.log$/.test(file));
    }

    res.send(files);
  }

  @ApiOperation({
    summary: 'Get log summaries',
    description:
      'Query logs that have already been analyzed and condensed to essential information.',
  })
  @ApiOkResponse()
  @Get('summary')
  async querySummary(@Query() queryParams: GetLogsQueryParams) {
    // query files and return list
    const summaryPathDir = join(this.config.get<string>('DIR_LOGS'), 'summary');
    if (!(await pathExists(summaryPathDir))) return [];
    const files = await readdir(summaryPathDir);

    // merge lists
    const list: AnalyzedLogDto[] = [];
    if (queryParams.from && queryParams.to) {
      for (const file of files) {
        if (
          isWithinInterval(new Date(file.replace('.json', '')), {
            start: new Date(queryParams.from),
            end: new Date(queryParams.to),
          })
        ) {
          const data = await readJson(join(summaryPathDir, file));
          list.push(...data);
        }
      }
    }

    return list;
  }

  @ApiOperation({
    summary: 'Get log file',
    description:
      'Get plain log file. Append `.json` or `?format=json` to get the log as JSON',
  })
  @ApiProduces('text/plain')
  @ApiOkResponse({
    schema: {
      type: 'string',
    },
  })
  @ApiNotFoundResponse()
  @Get(':logFile')
  async getById(
    @Param('logFile') file: string,
    @Query() queryParams: GetSingleLogQueryParams,
    @Res() res: Response,
  ): Promise<void> {
    let filePath = join(this.config.get<string>('DIR_LOGS'), file);

    if (queryParams.format === 'json') {
      filePath += '.json';
    }

    // check if file exists
    const exists = await pathExists(filePath);
    if (!exists) {
      throw new NotFoundException();
    }

    const readStream = createReadStream(filePath);
    readStream.pipe(res);
  }

  @ApiOperation({
    summary: 'Delete log file',
  })
  @ApiNoContentResponse()
  @ApiNotFoundResponse()
  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete(':logFile')
  async remove(
    @Param('logFile') file: string,
    @Res() res: Response,
  ): Promise<void> {
    const filePath = join(this.config.get<string>('DIR_LOGS'), file);

    // check if file exists
    const exists = await pathExists(filePath);
    if (!exists) {
      throw new NotFoundException();
    }

    await remove(filePath);

    const jsonPath = filePath + '.json';
    if (await pathExists(jsonPath)) {
      await remove(jsonPath);
    }

    res.send();
  }
}
