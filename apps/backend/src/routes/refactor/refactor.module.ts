import { Module } from '@nestjs/common';
import { TtsModule } from '@backend/routes/tts/tts.module';
import { RefactorController } from './refactor.controller';
import { RefactorService } from './refactor.service';

@Module({
  imports: [TtsModule],
  controllers: [RefactorController],
  providers: [RefactorService],
})
export class RefactorModule {}
