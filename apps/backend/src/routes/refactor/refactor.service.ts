import { Injectable, Logger } from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { nanoid } from 'nanoid';
import { join } from 'node:path';
import * as fs from 'fs-extra';
import { v4 as uuid } from 'uuid';
import { isURL } from 'class-validator';
import { removeIllegalFileChars } from '@uh4d/utils';
import { ObjectFileDto } from '@backend/dto';
import { ObjectMediaService } from '@backend/upload/object-media.service';
import { TtsService } from '@backend/routes/tts/tts.service';
import { BackendConfig } from '@backend/config';

@Injectable()
export class RefactorService {
  private readonly logger = new Logger(RefactorService.name);

  constructor(
    private readonly neo4j: Neo4jService,
    private readonly config: BackendConfig,
    private readonly objectMediaService: ObjectMediaService,
    private readonly ttsService: TtsService,
  ) {}

  async refactorGltfToGlb() {
    // get all 3d model media with gltf file
    // language=Cypher
    const q = `
      MATCH (obj:E22:UH4D)<-[:P67]-(file:UH4D:D9),
            (obj)-[:P1]->(name:E41)
      WHERE NOT exists(file.noEdges)
      RETURN collect({ id: obj.id, name: name.value }) AS objects,
             file
    `;
    const results = await this.neo4j.read<{
      objects: { id: string; name: string }[];
      file: ObjectFileDto;
    }>(q);

    for (const item of results) {
      // skip residenz due to missing zip file (cannot extract textures anymore)
      if (item.objects[0].id === 'e22_Z2VeJrd0w_residenz2') {
        this.logger.warn(`Skip ${item.objects[0].id} due to missing zip file`);
        continue;
      }

      this.logger.log(
        'Refactor ' +
          item.objects.map((obj) => `${obj.id} ${obj.name}`).join(' , '),
      );

      if (!/\.(gltf|glb)$/.test(item.file.file)) {
        this.logger.warn(
          `Queried media ${item.objects[0].id} has no valid gltf file path`,
        );
        continue;
      }

      const oldAbsPath = join(this.config.dirData, item.file.path);
      const newRelPath = `objects/${uuid()}/`;
      const newAbsPath = join(this.config.dirData, newRelPath);

      // create tmp folder
      const tmpId = nanoid(9);
      const tmpRelPath = `tmp/${tmpId}/`;
      const tmpAbsPath = join(this.config.dirData, tmpRelPath);
      const fileName = removeIllegalFileChars(item.file.original);
      await fs.ensureDir(tmpAbsPath);

      try {
        // copy original file to tmp folder
        await fs.copy(
          join(oldAbsPath, item.file.original),
          join(tmpAbsPath, fileName),
        );

        // convert to binary glb
        const { file, type, lowRes, noEdges, noEdgesLowRes } =
          await this.objectMediaService.generateGltfFromFile({
            path: tmpAbsPath,
            name: fileName,
          });

        // move new files to new directory
        await fs.move(tmpAbsPath, newAbsPath);

        // update database entry
        // language=Cypher
        const qUpdate = `
          MATCH (obj:E22:UH4D {id: $id})<-[:P67]-(file:D9:UH4D)
          SET file += $file
          RETURN file
        `;
        const params = {
          id: item.objects[0].id,
          file: {
            path: newRelPath,
            original: fileName,
            file,
            type,
            lowRes,
            noEdges,
            noEdgesLowRes,
          },
        };

        const session = this.neo4j.getWriteSession();
        const txc = session.beginTransaction();

        try {
          const rUpdate = await txc.run(qUpdate, params);
          const records = Neo4jService.extractRecords<{ file: ObjectFileDto }>(
            rUpdate.records,
          );
          if (records[0].file.path !== newRelPath) {
            throw new Error('No database update');
          }

          // remove old directory
          await fs.remove(oldAbsPath);

          await txc.commit();
        } catch (e) {
          await txc.rollback();
          throw e;
        } finally {
          await session.close();
        }
      } catch (e) {
        this.logger.error(e);
        // cleanup paths
        await fs.remove(tmpAbsPath);
        await fs.remove(newAbsPath);
      }
    }
  }

  async generateTTSAudioForPois() {
    // query all POIs
    // language=Cypher
    const q = `
      MATCH (poi:E73:UH4D)-[:P3]->(content:E62)
      RETURN poi.id AS id, content.value AS content
    `;
    const results = await this.neo4j.read<{ id: string; content: string }>(q);

    for (const poi of results) {
      if (isURL(poi.content)) continue;
      this.logger.log(`Generate TTS for POI ${poi.id}`);
      try {
        const audioPath = await this.ttsService.generateAudioFromText(
          poi.content,
        );
        this.logger.log(audioPath);
      } catch (e) {
        this.logger.error(e);
      }
    }
  }
}
