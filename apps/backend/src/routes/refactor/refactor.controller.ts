import { Controller, Get, Logger } from '@nestjs/common';
import { RefactorService } from './refactor.service';

@Controller('refactor')
export class RefactorController {
  private readonly logger = new Logger(RefactorController.name);

  constructor(private readonly refactorService: RefactorService) {}

  @Get('gltf')
  refactorGltf() {
    this.refactorService
      .refactorGltfToGlb()
      .then(() => {
        this.logger.log('Refactoring completed');
      })
      .catch((reason) => {
        this.logger.error(reason);
      });

    return 'Refactoring gltf to glb started...';
  }

  @Get('tts')
  generateTTS() {
    this.refactorService
      .generateTTSAudioForPois()
      .then(() => {
        this.logger.log('Refactoring completed');
      })
      .catch((reason) => {
        this.logger.error(reason);
      });

    return 'Generating TTS started...';
  }
}
