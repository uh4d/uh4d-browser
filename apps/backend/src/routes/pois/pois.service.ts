import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { nanoid } from 'nanoid';
import { SparqlService } from '@uh4d/backend/sparql';
import { extractUpdateProperties } from '@backend/utils/body-utils';
import { getSpatialParams } from '@backend/utils/spatial-params';
import { AuthUser } from '@backend/auth/user';
import { TtsService } from '@backend/routes/tts/tts.service';
import {
  CreateUh4dPoiDto,
  GeneralPoiDto,
  Uh4dPoiDto,
  UpdateUh4dPoiDto,
} from '@backend/dto';
import { GetPoisQueryParams } from './dto/get-pois-query-params';

@Injectable()
export class PoisService {
  private readonly logger = new Logger(PoisService.name);

  constructor(
    private readonly neo4j: Neo4jService,
    private readonly ttsService: TtsService,
    private readonly sparqlService: SparqlService,
  ) {}

  async query(
    query: GetPoisQueryParams,
    userId?: string,
  ): Promise<Uh4dPoiDto[]> {
    const spatialParams = getSpatialParams(query);

    let pending = false;
    let declined = false;

    // first triage
    (query.q ? (Array.isArray(query.q) ? query.q : [query.q]) : []).forEach(
      (value) => {
        if (value === 'pending') {
          pending = true;
        } else if (value === 'declined') {
          declined = true;
        }
      },
    );

    let q = '';

    if (spatialParams) {
      q += `WITH point({latitude: $geo.latitude, longitude: $geo.longitude}) AS userPoint `;
    }

    q += 'MATCH ' + (query.scene ? '(:E53:UH4D {id: $scene})<-[:P89]-' : '');
    q += `(place:E53)<-[:P161]-(e92:E92)<-[:P67]-(poi:E73)-[:P2]->(:E55 {id: "poi"}),
          (place)-[:P168]->(geo:E94) `;

    q += `
      WHERE coalesce(poi.pending, FALSE) = $pending
      AND coalesce(poi.declined, FALSE) = $declined
    `;
    if (spatialParams) {
      q += `AND distance(geo.point, userPoint) < $geo.radius`;
    }
    if (userId) {
      q += `AND (poi)-[:created_by]->(:User:UH4D {id: $userId}) `;
    }

    q += `
      OPTIONAL MATCH (e92)-[:P160]->(:E52)-[:P82]->(poiDate:E61)
      OPTIONAL MATCH (poi)-[:P67]->(e22:E22)
      OPTIONAL MATCH (e22)<-[:P108]-(:E12)-[:P4]->(:E52)-[:P82]->(objFrom:E61)
      OPTIONAL MATCH (e22)<-[:P13]-(:E6)-[:P4]->(:E52)-[:P82]->(objTo:E61)
      WITH poi, place, e22,
           CASE
             WHEN poiDate IS NOT NULL THEN { from: poiDate.from, to: poiDate.to, objectBound: false }
             ELSE { from: objFrom.value, to: objTo.value, objectBound: true }
             END AS date
  `;

    if (query.date) {
      q +=
        'WHERE (date.from IS NULL OR date.from < date($date)) AND (date.to IS NULL OR date.to > date($date))';
    }

    // language=Cypher
    q += `
      MATCH (place)-[:P168]->(geo:E94),
            (poi)-[:P102]->(title:E35),
            (poi)-[:P3]->(content:E62)
      OPTIONAL MATCH (poi)-[userEventU:created_by]->(userU:User)-[:P131]->(usernameU:E82)
      OPTIONAL MATCH (poi)-[userEventE:edited_by]->(userE:User)-[:P131]->(usernameE:E82)
      RETURN poi.id AS id,
             title.value AS title,
             content.value AS content,
             apoc.map.removeKeys(geo, ['id', 'point']) AS location,
             e22.id AS objectId,
             poi.camera AS camera,
             poi.timestamp As timestamp,
             date,
             poi.damageFactors AS damageFactors,
             'uh4d' AS type,
             poi.pending AS pending,
             poi.declined AS declined,
             userU{userId: userU.id, username: usernameU.value, timestamp: userEventU.timestamp} AS createdBy,
             userE{userId: userE.id, username: usernameE.value, timestamp: userEventE.timestamp} AS editedBy
    `;

    const params = {
      userId,
      scene: query.scene,
      geo: spatialParams,
      date: query.date,
      pending,
      declined,
    };

    return this.neo4j.read(q, params);
  }

  async getById(poiId: string): Promise<Uh4dPoiDto | undefined> {
    // language=Cypher
    const q = `
      MATCH (poi:E73:UH4D {id: $id})-[:P2]->(:E55 {id: "poi"}),
            (poi)-[:P102]->(title:E35),
            (poi)-[:P3]->(content:E62),
            (poi)-[:P67]->(e92:E92)-[:P161]->(place:E53)-[:P168]->(geo:E94)
      OPTIONAL MATCH (e92)-[:P160]->(:E52)-[:P82]->(poiDate:E61)
      OPTIONAL MATCH (poi)-[:P67]->(e22:E22)
      OPTIONAL MATCH (e22)<-[:P108]-(:E12)-[:P4]->(:E52)-[:P82]->(objFrom:E61)
      OPTIONAL MATCH (e22)<-[:P13]-(:E6)-[:P4]->(:E52)-[:P82]->(objTo:E61)
      OPTIONAL MATCH (poi)-[userEventU:created_by]->(userU:User)-[:P131]->(usernameU:E82)
      OPTIONAL MATCH (poi)-[userEventE:edited_by]->(userE:User)-[:P131]->(usernameE:E82)
      RETURN poi.id AS id,
             title.value AS title,
             content.value AS content,
             apoc.map.removeKeys(geo, ['id', 'point']) AS location,
             e22.id AS objectId,
             poi.camera AS camera,
             poi.timestamp As timestamp,
             CASE
               WHEN poiDate IS NOT NULL THEN { from: poiDate.from, to: poiDate.to, objectBound: false }
               ELSE { from: objFrom.value, to: objTo.value, objectBound: true }
             END AS date,
             poi.damageFactors AS damageFactors,
             'uh4d' AS type,
             poi.pending AS pending,
             poi.declined AS declined,
             userU{userId: userU.id, username: usernameU.value, timestamp: userEventU.timestamp} AS createdBy,
             userE{userId: userE.id, username: usernameE.value, timestamp: userEventE.timestamp} AS editedBy
    `;

    const params = {
      id: poiId,
    };

    return (await this.neo4j.read(q, params))[0];
  }

  async create(body: CreateUh4dPoiDto, user: AuthUser): Promise<Uh4dPoiDto> {
    // TODO: update camera property to lat,lon,altitude
    // language=Cypher
    const q = `
      MATCH (user:User:UH4D { id: $userId })-[:P131]->(username:E82), (tpoi:E55:UH4D {id: "poi"})
      CREATE (poi:E73:UH4D {id: $poiId})-[:P67]->(e92:E92:UH4D {id: $e92id})-[:P161]->(place:E53:UH4D {id: $placeId})-[:P168]->(geo:E94:UH4D $geo),
             (poi)-[:P2]->(tpoi),
             (poi)-[:P102]->(title:E35:UH4D $title),
             (poi)-[:P3]->(content:E62:UH4D $content),
             (poi)-[userEvent:created_by { timestamp: datetime() }]->(user)
      SET poi.damageFactors = $damageFactors,
          geo.point = point({latitude: $geo.latitude, longitude: $geo.longitude})

      WITH poi, e92, geo, title, content,
           user{userId: user.id, username: username.value, timestamp: userEvent.timestamp} AS createdBy
      CALL apoc.do.when($pending, 'SET poi.pending = TRUE RETURN poi', '', { poi: poi }) YIELD value

      WITH poi, e92, geo, title, content, createdBy
      CALL apoc.do.when($objectId IS NOT NULL,
        'MATCH (object:E22 {id: objectId}) CREATE (poi)-[:P67]->(object) RETURN object',
        'RETURN NULL AS object',
        { poi: poi, objectId: $objectId }) YIELD value

      WITH poi, e92, geo, title, content, createdBy, value.object AS object
      OPTIONAL MATCH (object)<-[:P108]-(:E12)-[:P4]->(:E52)-[:P82]->(objFrom:E61)
      OPTIONAL MATCH (object)<-[:P13]-(:E6)-[:P4]->(:E52)-[:P82]->(objTo:E61)

      CALL apoc.do.when($objectId IS NOT NULL AND $dateMap.objectBound,
        'RETURN { from: objFrom.value, to: objTo.value } AS date, true AS objectBound',
        'CREATE (e92)-[:P160]->(:E52:UH4D {id: e52id})-[:P82]->(date:E61:UH4D {id: dateMap.id, from: CASE WHEN dateMap.from IS NULL THEN NULL ELSE date(dateMap.from) END, to: CASE WHEN dateMap.to IS NULL THEN NULL ELSE date(dateMap.to) END}) RETURN date, false AS objectBound',
        { e92: e92, e52id: $e52id, dateMap: $dateMap, objFrom: objFrom, objTo: objTo }) YIELD value AS dateValue

      RETURN poi.id AS id,
             title.value AS title,
             content.value AS content,
             apoc.map.removeKey(geo, 'id') as location,
             object.id AS objectId,
             [] AS camera,
             poi.timestamp As timestamp,
             { from: dateValue.date.from, to: dateValue.date.to, objectBound: dateValue.objectBound } AS date,
             poi.damageFactors AS damageFactors,
             'uh4d' AS type,
             poi.pending AS pending,
             poi.declined AS declined,
             createdBy
    `;

    const id = nanoid(9);

    const params = {
      userId: user.id,
      pending: user.needsValidation(),
      poiId: 'e73_poi_' + id,
      placeId: 'e53_poi_' + id,
      e92id: 'e92_poi_' + id,
      geo: {
        id: 'e94_poi_' + id,
        latitude: body.location.latitude,
        longitude: body.location.longitude,
        altitude: body.location.altitude,
      },
      title: {
        id: 'e35_poi_' + id,
        value: body.title,
      },
      content: {
        id: 'e62_poi_' + id,
        value: body.content,
      },
      objectId: body.objectId || null,
      damageFactors: body.damageFactors,
      e52id: 'e52_poi_' + id,
      dateMap: {
        id: 'e61_poi_' + id,
        from: body.date.from,
        to: body.date.to,
        objectBound: !!body.date.objectBound,
      },
    };

    const created = (await this.neo4j.write(q, params))[0];

    // trigger TTS generation
    this.ttsService
      .generateAudioFromText(body.content)
      .then((file) => {
        this.logger.log(`TTS generated for POI: ${file}`);
      })
      .catch((err) => {
        this.logger.warn(err);
      });

    return created;
  }

  async update(
    poiId: string,
    body: UpdateUh4dPoiDto,
  ): Promise<Uh4dPoiDto | undefined> {
    // get current entry
    const poi = await this.getById(poiId);
    if (!poi) return undefined;

    // build cypher statements for properties that need to be updated
    const updatePoi = extractUpdateProperties(poi, body);
    console.log(updatePoi);
    const statements = Object.entries(updatePoi).map(([key, value]) =>
      this.buildUpdateStatement(poi, { [key]: value }),
    );
    await this.neo4j.multipleStatements(statements);

    // get complete updated entry
    const updated = this.getById(poiId);

    if (updatePoi.content) {
      // trigger TTS generation
      this.ttsService
        .generateAudioFromText(updatePoi.content)
        .then((file) => {
          this.logger.log(`TTS generated for POI: ${file}`);
        })
        .catch((err) => {
          this.logger.warn(err);
        });
    }

    return updated;
  }

  private buildUpdateStatement(
    currentEntry: Uh4dPoiDto,
    body: UpdateUh4dPoiDto,
  ): { statement: string; parameters: Record<string, any> } {
    let q = 'MATCH (poi:E73:UH4D {id: $id})-[:P2]->(:E55 {id: "poi"}) ';

    const params: Record<string, any> = {
      id: currentEntry.id,
    };

    const id = nanoid(9);

    switch (Object.keys(body)[0]) {
      case 'title':
        q += 'MATCH (poi)-[:P102]->(title:E35) SET title.value = $title';
        params.title = body.title;
        break;

      case 'content':
        q += 'MATCH (poi)-[:P3]->(content:E62) SET content.value = $content';
        params.content = body.content;
        break;

      case 'camera':
        q += 'SET poi.camera = $camera';
        params.camera = body.camera;
        break;

      case 'damageFactors':
        if (body.damageFactors?.length) {
          q += 'SET poi.damageFactors = $damageFactors';
          params.damageFactors = body.damageFactors;
        } else {
          q += 'REMOVE poi.damageFactors';
        }
        break;

      case 'date':
        // language=Cypher
        q += `
          MATCH (poi)-[:P67]->(e92:E92)
          OPTIONAL MATCH (poi)-[:P67]->(e22:E22)
          OPTIONAL MATCH (e22)<-[:P108]-(:E12)-[:P4]->(:E52)-[:P82]->(objFrom:E61)
          OPTIONAL MATCH (e22)<-[:P13]-(:E6)-[:P4]->(:E52)-[:P82]->(objTo:E61)

          CALL apoc.do.when(e22 IS NOT NULL AND $dateMap.objectBound,
            'OPTIONAL MATCH (e92)-[:P160]->(e52:E52)-[:P82]->(date:E61) DETACH DELETE e52, date RETURN { from: objFrom.value, to: objTo.value } AS date, true AS objectBound',
            'MERGE (e92)-[:P160]->(e52:E52:UH4D)-[:P82]->(date:E61:UH4D) ON CREATE SET e52.id = e52id, date.id = dateMap.id SET date.from = CASE WHEN dateMap.from IS NULL THEN NULL ELSE date(dateMap.from) END, date.to = CASE WHEN dateMap.to IS NULL THEN NULL ELSE date(dateMap.to) END RETURN date, false AS objectBound',
            { e92: e92, e52id: $e52id, dateMap: $dateMap, objFrom: objFrom, objTo: objTo }
          ) YIELD value AS dateValue
        `;
        params.e52id = 'e52_poi_' + id;
        params.dateMap = {
          id: 'e61_poi_' + id,
          from: body.date.from,
          to: body.date.to,
          objectBound: !!body.date.objectBound,
        };
    }

    q += ' RETURN poi';

    return { statement: q, parameters: params };
  }

  /**
   * Remove point of interest.
   * @return Resolves with `true` if operation was successful.
   */
  async remove(poiId: string): Promise<boolean> {
    // language=Cypher
    const q = `
      MATCH (poi:E73 {id: $id})-[:P2]->(:E55 {id: "poi"}),
            (poi)-[:P102]->(title:E35),
            (poi)-[:P3]->(content:E62),
            (poi)-[:P67]->(e92:E92)-[:P161]->(place:E53)-[:P168]->(geo:E94)
      OPTIONAL MATCH (e92)-[:P160]->(e52:E52)-[:P82]->(date:E61)
      DETACH DELETE poi, place, geo, title, content, e92, e52, date
      RETURN true AS check
    `;

    const params = {
      id: poiId,
    };

    return !!(await this.neo4j.write(q, params))[0];
  }

  async queryByType(
    type: string,
    queryParams: GetPoisQueryParams,
  ): Promise<(GeneralPoiDto | Uh4dPoiDto)[]> {
    switch (type) {
      case 'uh4d':
        return this.query(queryParams);
      case 'wikidata':
        return this.queryWikidata(queryParams);
      default:
        return [];
    }
  }

  async queryWikidata(query: GetPoisQueryParams): Promise<GeneralPoiDto[]> {
    const latitude = query.lat;
    const longitude = query.lon;
    const radius = query.r;

    if (isNaN(latitude) || isNaN(longitude) || isNaN(radius)) {
      throw new BadRequestException(
        'Wikidata query requires lat|lon|r query parameters',
      );
    }

    const preferredLang = query.lang === 'en' ? 'En' : 'De';
    const fallbackLang = query.lang === 'en' ? 'De' : 'En';

    const wdFacility = 'Q13226383';
    const wdPointOfInterest = 'Q960648';
    const wIds = [wdFacility, wdPointOfInterest].map((wd) => 'wd:' + wd);

    // language=Sparql
    const sparql = `
      SELECT ?item ?itemLabel ?lat ?lon ?sitelink WHERE {
        ?item wdt:P31/wdt:P279* ?wId .
        FILTER (?wId IN (${wIds.join(', ')}))
        ?item p:P625 ?statement .
        ?statement psv:P625 ?coordinate_node .
        ?coordinate_node wikibase:geoLatitude ?lat .
        ?coordinate_node wikibase:geoLongitude ?lon .
        OPTIONAL {
            ?sitelinkDe schema:about ?item .
            ?sitelinkDe schema:isPartOf <https://de.wikipedia.org/> .
            ?item rdfs:label ?labelDe FILTER (LANG(?labelDe) = "de")
        }
        OPTIONAL {
            ?sitelinkEn schema:about ?item .
            ?sitelinkEn schema:isPartOf <https://en.wikipedia.org/> .
            ?item rdfs:label ?labelEn FILTER (LANG(?labelEn) = "en")
        }
        BIND (COALESCE(?sitelink${preferredLang}, ?sitelink${fallbackLang}) AS ?sitelink)
        BIND (COALESCE(?label${preferredLang}, ?label${fallbackLang}) AS ?itemLabel)
        FILTER (BOUND(?itemLabel) && BOUND(?sitelink))
        SERVICE wikibase:around {
          ?item wdt:P625 ?location .
          bd:serviceParam wikibase:center "Point(${longitude} ${latitude})"^^geo:wktLiteral .
          bd:serviceParam wikibase:radius "${radius / 1000}".
        }
      }
      GROUP BY ?item ?itemLabel ?lat ?lon ?sitelink
    `;

    const results = await this.sparqlService.queryWikidata<{
      item: string;
      itemLabel: string;
      lat: string;
      lon: string;
      sitelink: string;
    }>(sparql);

    // map to POI format
    return results.map((r) => ({
      id: r.item.value,
      title: r.itemLabel.value,
      content: r.sitelink.value,
      location: {
        latitude: parseFloat(r.lat.value),
        longitude: parseFloat(r.lon.value),
        altitude: 0,
      },
      type: 'wikidata',
    }));
  }
}
