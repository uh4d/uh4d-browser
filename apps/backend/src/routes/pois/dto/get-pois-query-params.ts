import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsOptional, IsString } from 'class-validator';
import { LocationQueryParams } from '@backend/routes/dto/location-query-params';

export class GetPoisQueryParams extends LocationQueryParams {
  @ApiProperty({
    description: 'Filter by date',
    required: false,
  })
  @IsOptional()
  @IsDateString()
  date?: string;

  @ApiProperty({
    description: 'Filter by specific scene',
    required: false,
  })
  @IsOptional()
  scene?: string;

  @ApiProperty({
    description:
      'Type of sources points of interest should be queried from. Default: `uh4d`',
    enum: ['uh4d', 'wikidata'],
    required: false,
    type: String,
  })
  @IsOptional()
  type?: string | string[];

  @ApiProperty({
    description:
      'Preferred language of the point of interest with fallback to the other language. Only applies to `wikidata` POIs. Default: `de`.',
    enum: ['de', 'en'],
    required: false,
  })
  @IsOptional()
  @IsString()
  lang?: string;

  @ApiProperty({
    description:
      'Query string to filter results. Currently, only `pending` and `declined` are supported to filter by validation status.',
    required: false,
  })
  @IsOptional()
  q?: string;
}
