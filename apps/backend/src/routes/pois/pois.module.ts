import { forwardRef, Module } from '@nestjs/common';
import { SparqlModule } from '@uh4d/backend/sparql';
import { AuthModule } from '@backend/auth/auth.module';
import { CommonModule } from '@backend/common/common.module';
import { TtsModule } from '@backend/routes/tts/tts.module';
import { PoisController } from './pois.controller';
import { PoisService } from './pois.service';

@Module({
  imports: [
    forwardRef(() => AuthModule),
    CommonModule,
    TtsModule,
    SparqlModule,
  ],
  controllers: [PoisController],
  providers: [PoisService],
  exports: [PoisService],
})
export class PoisModule {}
