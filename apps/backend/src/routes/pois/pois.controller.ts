import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  NotFoundException,
  Param,
  Patch,
  Post,
  Query,
  Res,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiExtraModels,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  getSchemaPath,
} from '@nestjs/swagger';
import { Response } from 'express';
import { Auth } from '@backend/auth/auth.decorator';
import { AuthUser, User } from '@backend/auth/user';
import { CommonService } from '@backend/common/common.service';
import { UpdateValidationStatusDto } from '@backend/routes/dto/update-validation-status.dto';
import {
  CreateUh4dPoiDto,
  GeneralPoiDto,
  Uh4dPoiDto,
  UpdateUh4dPoiDto,
} from '@backend/dto';
import { PoisService } from './pois.service';
import { GetPoisQueryParams } from './dto/get-pois-query-params';

@ApiTags('Points of Interest')
@Controller('pois')
export class PoisController {
  constructor(
    private readonly poiService: PoisService,
    private readonly commonService: CommonService,
  ) {}

  @ApiOperation({
    summary: 'Query points of interest',
    description:
      'Query all points of interest. Query parameters may be set to filter the data.\n\n`lat`, `lon`, and `r` only work in combination and may be used to look for points of interest within a radius at a position.\n\n`type` specifies the source API/database the points of interests should be queried from (provided respective retrieval script). If omitted, only UH4D points of interest will be queried (equals `type=uh4d`). Multiple types are possible, e.g., `?type=uh4d&type=wikidata`.',
  })
  @ApiExtraModels(Uh4dPoiDto, GeneralPoiDto)
  @ApiOkResponse({
    schema: {
      type: 'array',
      items: {
        oneOf: [
          { $ref: getSchemaPath(Uh4dPoiDto) },
          { $ref: getSchemaPath(GeneralPoiDto) },
        ],
      },
    },
  })
  @Get()
  async query(
    @Query() queryParams: GetPoisQueryParams,
  ): Promise<(GeneralPoiDto | Uh4dPoiDto)[]> {
    // parse types
    const types = queryParams.type
      ? Array.isArray(queryParams.type)
        ? queryParams.type
        : [queryParams.type]
      : ['uh4d'];

    // iterate over types and execute query
    const promises = types.map((type) =>
      this.poiService.queryByType(type, queryParams),
    );

    // concatenate lists of POIs
    const results = await Promise.all(promises);
    return results.reduce((previousValue, currentValue) =>
      previousValue.concat(currentValue),
    );
  }

  @ApiOperation({
    summary: 'Get point of interest',
    description:
      "Get point of interest by ID (only possible for `type: 'uh4d'`).",
  })
  @ApiOkResponse({
    type: Uh4dPoiDto,
  })
  @ApiNotFoundResponse()
  @Get(':poiId')
  async getById(@Param('poiId') poiId: string): Promise<Uh4dPoiDto> {
    const poi = await this.poiService.getById(poiId);

    if (!poi) {
      throw new NotFoundException(`POI with ID ${poiId} not found!`);
    }

    return poi;
  }

  @ApiOperation({
    summary: 'Create point of interest',
    description: 'Create new point of interest at position.',
  })
  @ApiCreatedResponse({
    type: Uh4dPoiDto,
  })
  @ApiBadRequestResponse()
  @Auth('createPOIs')
  @Post()
  async create(
    @Body() body: CreateUh4dPoiDto,
    @User() user: AuthUser,
  ): Promise<Uh4dPoiDto> {
    const poi = await this.poiService.create(body, user);

    if (!poi) {
      throw new BadRequestException(`No POI created!`);
    }

    return poi;
  }

  @ApiOperation({
    summary: 'Update point of interest',
    description:
      "Update properties of point of interest (only possible for `type: 'uh4d'`). At the moment, only `title`, `content`, `date` properties, `camera`, and `damageFactors` are updated. Location/position updates are not yet supported.",
  })
  @ApiOkResponse({
    type: Uh4dPoiDto,
  })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse()
  @Auth()
  @Patch(':poiId')
  async update(
    @Param('poiId') poiId: string,
    @Body() body: UpdateUh4dPoiDto,
    @User() user: AuthUser,
  ): Promise<Uh4dPoiDto> {
    const poi = await this.poiService.update(poiId, body);

    if (!poi) {
      throw new NotFoundException(`POI with ID ${poiId} not found!`);
    }

    poi.editedBy = await this.commonService.updateEditedBy(user, poiId);

    return poi;
  }

  @ApiOperation({
    summary: 'Delete point of interest',
    description:
      "Delete point of interest by ID (only possible for `type: 'uh4d'`).",
  })
  @ApiNoContentResponse()
  @ApiNotFoundResponse()
  @HttpCode(HttpStatus.NO_CONTENT)
  @Auth()
  @Delete(':poiId')
  async remove(
    @Param('poiId') poiId: string,
    @Res() res: Response,
  ): Promise<void> {
    const removed = await this.poiService.remove(poiId);

    if (!removed) {
      throw new NotFoundException(`POI with ID ${poiId} not found!`);
    }

    res.send();
  }

  @ApiOperation({
    summary: 'Confirm/decline point of interest',
    description: 'Set `pending` and/or `declined` flag.',
  })
  @ApiOkResponse({
    type: Uh4dPoiDto,
  })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse()
  @Auth('validateContent')
  @Patch(':poiId/validation')
  async validatePoi(
    @Param('poiId') poiId: string,
    @Body() body: UpdateValidationStatusDto,
  ): Promise<Uh4dPoiDto> {
    const update = await this.commonService.updateValidationStatus(
      poiId,
      body.pending,
      body.declined,
    );

    if (!update) {
      throw new NotFoundException(`POI with ID ${poiId} not found!`);
    }

    return this.poiService.getById(poiId);
  }
}
