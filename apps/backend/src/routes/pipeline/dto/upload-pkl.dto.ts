import { ApiProperty, PickType } from '@nestjs/swagger';
import { ImageDto } from '@backend/dto/image';

export class UploadPklResponseDto extends PickType(ImageDto, ['id', 'file']) {}

export class UploadPklFileDto {
  @ApiProperty({
    type: 'string',
    format: 'binary',
  })
  file: any;
}
