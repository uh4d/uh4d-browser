import { ApiProperty, PickType } from '@nestjs/swagger';
import { CreatePipelineImageDto, PipelineImageDto } from '@backend/dto/pipeline-image';
import { CreateLocationDto, LocationDto } from '@backend/dto/location';
import { IsArray, IsNotEmpty, IsString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';

export class PipelineImagesDto {
  @ApiProperty({
    description:
      'Global WGS-84 coordinates to which the following Cartesian image coordinates are relative to',
    required: false,
  })
  origin: LocationDto;
  @ApiProperty({
    description: 'List of images',
    required: false,
    type: PipelineImageDto,
    isArray: true,
  })
  images: PipelineImageDto[];
}

export class SavePoseDto extends PickType(CreatePipelineImageDto, [
  'width',
  'height',
  'pose',
]) {
  @ApiProperty({
    description: 'Image ID',
  })
  @IsNotEmpty()
  @IsString()
  id: string;
}

export class SavePipelineImagesDto {
  @ApiProperty({
    description:
      'Global WGS-84 coordinates to which the following Cartesian image coordinates are relative to',
  })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreateLocationDto)
  origin: CreateLocationDto;
  @ApiProperty({
    description: 'List of images',
    type: SavePoseDto,
    isArray: true,
  })
  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => SavePoseDto)
  images: SavePoseDto[];
}
