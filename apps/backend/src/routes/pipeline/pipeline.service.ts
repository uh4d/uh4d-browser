import { Injectable, NotFoundException } from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { UploadPklResponseDto } from '@backend/routes/pipeline/dto/upload-pkl.dto';
import { CreateCameraDto } from '@backend/dto/camera';

@Injectable()
export class PipelineService {
  constructor(private readonly neo4j: Neo4jService) {}

  async savePoses(
    images: { id: string; camera: CreateCameraDto }[],
  ): Promise<void> {
    const statements: { statement: string; parameters: Record<string, any> }[] =
      images.map((img) => {
        // language=Cypher
        const q = `
            MATCH (img:E38:UH4D {id: $imageId})<-[:P94]-(e65:E65)-[:P7]->(place:E53)-[:P168]->(geo:E94:UH4D)
            OPTIONAL MATCH (image)-[rSnot:P2]->(:E55 {id: "image:spatial:not_possible"})
            OPTIONAL MATCH (image)-[rSyet:P2]->(:E55 {id: "image:spatial:not_yet_possible"})
            MERGE (e65)-[:P16]->(camera:E22:UH4D)
            SET camera = $camera
            SET geo += $geo
            SET geo.status = 5, geo.point = point({latitude: $geo.latitude, longitude: $geo.longitude})
            REMOVE geo.radius
            DELETE rSnot, rSyet
            RETURN img
          `;

        return {
          statement: q,
          parameters: {
            imageId: img.id,
            geo: {
              latitude: img.camera.latitude,
              longitude: img.camera.longitude,
              altitude: img.camera.altitude,
            },
            camera: {
              id: 'camera_' + img.id,
              ck: img.camera.ck,
              offset: img.camera.offset,
              k: img.camera.k,
              omega: img.camera.omega,
              phi: img.camera.phi,
              kappa: img.camera.kappa,
              method: 'pipeline',
            },
          },
        };
      });

    const session = this.neo4j.getWriteSession();

    try {
      const txc = session.beginTransaction();

      // check for each statement if it has been successful, otherwise rollback transaction
      for (const s of statements) {
        const result = await txc.run(s.statement, s.parameters);

        if (!result.records[0]) {
          await txc.rollback();
          await session.close();
          throw new NotFoundException(
            `Image not found with ID ${s.parameters.id}. No values updated (transaction rollback)!`,
          );
        }
      }

      await txc.commit();
    } finally {
      await session.close();
    }
  }

  async setPklFile(
    imageId: string,
    pklFile: string | null,
  ): Promise<UploadPklResponseDto> {
    // language=Cypher
    const q = `
      MATCH (image:E38:UH4D {id: $imageId})<-[:P67]-(file:D9)
      SET file.pkl = $pkl
      RETURN image.id AS id, apoc.map.removeKey(file, 'id') AS file
    `;

    const params = {
      imageId,
      pkl: pklFile,
    };

    return (await this.neo4j.write(q, params))[0];
  }
}
