import { Module } from '@nestjs/common';
import { PipelineController } from './pipeline.controller';
import { PipelineService } from './pipeline.service';
import { ImagesModule } from '@backend/routes/images/images.module';

@Module({
  imports: [ImagesModule],
  controllers: [PipelineController],
  providers: [PipelineService],
})
export class PipelineModule {}
