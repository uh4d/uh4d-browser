import {
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Post,
  Query,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiConsumes,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
import { Express } from 'express';
import { join, parse } from 'node:path';
import * as fs from 'fs-extra';
import { Euler, Quaternion, Vector2, Vector3 } from 'three';
import { Coordinates } from '@uh4d/utils';
import { ImagesService } from '@backend/routes/images/images.service';
import { BackendConfig } from '@backend/config';
import { GetImagesQueryParams } from '@backend/routes/images/dto/get-images-query-params';
import { GetImageQueryParams } from '@backend/routes/images/dto/get-image-query-params';
import { PipelineImageDto } from '@backend/dto/pipeline-image';
import { CreateCameraDto } from '@backend/dto/camera';
import { PipelineService } from './pipeline.service';
import { UploadPklFileDto, UploadPklResponseDto } from './dto/upload-pkl.dto';
import {
  PipelineImagesDto,
  SavePipelineImagesDto,
} from './dto/pipeline-images.dto';

@ApiTags('Pipeline')
@Controller('pipeline')
export class PipelineController {
  constructor(
    private readonly pipelineService: PipelineService,
    private readonly imagesService: ImagesService,
    private readonly config: BackendConfig,
  ) {}

  @ApiOperation({
    summary: 'Query images',
    description:
      "Query images with Cartesian coordinates relative to origin. The origin is either the average position of all queried images with spatial coordinates or, if `scene` and `forceSceneOrigin=1` are set, the scene's origin.\n\nQuery parameters may be set to filter the data. `lat`, `lon`, and `r` works only in combination and may be used to look for images within a radius at a position.",
  })
  @ApiOkResponse({
    type: PipelineImagesDto,
  })
  @ApiBadRequestResponse()
  @Get('images')
  async query(
    @Query() queryParams: GetImagesQueryParams,
  ): Promise<PipelineImagesDto> {
    const images = await this.imagesService.query(queryParams);

    // set origin by average position of all available positions
    const origin = images
      .filter((img) => img.camera)
      .map((img) => ({
        latitude: img.camera.latitude,
        longitude: img.camera.longitude,
      }))
      .reduce<Coordinates>((acc, curr, index, array) => {
        acc.latitude += curr.latitude / array.length;
        acc.longitude += curr.longitude / array.length;
        return acc;
      }, new Coordinates());

    // map image properties
    const entries = images.map((img) => {
      const map: PipelineImageDto = {
        id: img.id,
        image: this.config.host + '/data/' + img.file.path + img.file.original,
        pkl: img.file.pkl
          ? this.config.host + '/data/' + img.file.path + img.file.pkl
          : null,
        date: img.date,
        width: img.file.width,
        height: img.file.height,
        pose: null,
        spatialStatus: img.spatialStatus,
      };

      if (img.camera) {
        // set translation relative to origin of local coordinate system
        const t = new Coordinates(img.camera).toCartesian(origin);
        const euler = new Euler(
          img.camera.omega,
          img.camera.phi,
          img.camera.kappa,
          'YXZ',
        );
        const q = new Quaternion().setFromEuler(euler);

        const pp = new Vector2()
          .fromArray(img.camera.offset || [0, 0])
          .multiplyScalar(map.height)
          .add(new Vector2(map.width / 2, map.height / 2));
        const k = Array.isArray(img.camera.k) ? img.camera.k : [0];

        map.pose = {
          tx: t.x,
          ty: t.y,
          tz: t.z,
          qw: q.w,
          qx: q.x,
          qy: q.y,
          qz: q.z,
          params: [img.camera.ck * map.height, ...pp.toArray(), ...k],
        };
      }

      return map;
    });

    // send data
    return {
      origin: origin.toJSON(),
      images: entries,
    };
  }

  @ApiOperation({
    summary: 'Save poses',
    description: 'Save computed poses for a set of images.',
  })
  @ApiCreatedResponse()
  @ApiBadRequestResponse()
  @ApiNotFoundResponse()
  @Post('images')
  async savePoses(@Body() body: SavePipelineImagesDto): Promise<void> {
    const origin = new Coordinates(body.origin);

    const poseData = body.images.map<{ id: string; camera: CreateCameraDto }>(
      (img) => {
        // convert back to WGS-84 coordinates
        const position = origin
          .clone()
          .add(new Vector3(img.pose.tx, img.pose.ty, img.pose.tz))
          .toJSON();
        const quaternion = new Quaternion(
          img.pose.qx,
          img.pose.qy,
          img.pose.qz,
          img.pose.qw,
        );
        const euler = new Euler().setFromQuaternion(quaternion, 'YXZ');

        // convert intrinsic parameters
        const ck = img.pose.params[0] / img.height;
        const offset = new Vector2(img.width / 2, img.height / 2)
          .sub(new Vector2(img.pose.params[1], img.pose.params[2]))
          .divideScalar(img.height)
          .toArray();
        const k = img.pose.params.slice(3);

        return {
          id: img.id,
          camera: {
            ...position,
            ck,
            offset,
            k,
            omega: euler.x,
            phi: euler.y,
            kappa: euler.z,
          },
        };
      },
    );

    await this.pipelineService.savePoses(poseData);
  }

  @ApiOperation({
    summary: 'Save PKL file',
    description: 'Save `.pkl` feature file.',
  })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    type: UploadPklFileDto,
  })
  @ApiCreatedResponse({
    type: UploadPklResponseDto,
  })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse()
  @Post('images/:imageId/pkl')
  @UseInterceptors(FileInterceptor('file'))
  async uploadPkl(
    @UploadedFile() file: Express.Multer.File,
    @Param('imageId') imageId: string,
    @Query() queryParams: GetImageQueryParams,
  ): Promise<UploadPklResponseDto> {
    // get image by ID
    const image = await this.imagesService.getById(
      imageId,
      queryParams.regexp === '1',
    );
    if (!image) {
      throw new NotFoundException(`Image with ID ${imageId} not found!`);
    }

    // copy uploaded pkl file
    const filename = parse(image.file.original).name + '.pkl';
    const filePath = join(this.config.dirData, image.file.path, filename);

    await fs.rename(file.path, filePath);

    // update database entry
    try {
      return this.pipelineService.setPklFile(imageId, filename);
    } catch (e) {
      // delete file if database write failed
      await fs.remove(filePath);

      throw e;
    }
  }
}
