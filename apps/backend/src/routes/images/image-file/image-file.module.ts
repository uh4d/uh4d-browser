import { forwardRef, Module } from '@nestjs/common';
import { AuthModule } from '@backend/auth/auth.module';
import { CommonModule } from '@backend/common/common.module';
import { UploadModule } from '@backend/upload/upload.module';
import { ImageFileService } from './image-file.service';
import { ImageFileController } from './image-file.controller';
import { ImagesModule } from '../images.module';

@Module({
  imports: [
    AuthModule,
    CommonModule,
    forwardRef(() => ImagesModule),
    forwardRef(() => UploadModule),
  ],
  providers: [ImageFileService],
  controllers: [ImageFileController],
})
export class ImageFileModule {}
