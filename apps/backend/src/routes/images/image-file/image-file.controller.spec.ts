import { Test, TestingModule } from '@nestjs/testing';
import { ImageFileController } from './image-file.controller';

describe('ImageFileController', () => {
  let controller: ImageFileController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ImageFileController],
    }).compile();

    controller = module.get<ImageFileController>(ImageFileController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
