import { ApiProperty } from '@nestjs/swagger';
import { ProbeResult, Orientation } from 'probe-image-size';

export class FileUpdateFailDto {
  @ApiProperty({
    description: 'Resolves `false` for below reason',
    required: false,
  })
  updateAvailable: boolean;
  @ApiProperty({
    description: 'Reason why update is not available',
    required: false,
  })
  reason: string;
}

export class FileUpdateSuccessDto implements ProbeResult {
  @ApiProperty({
    description: 'Resolves `true` if update is available',
    required: false,
  })
  updateAvailable: false;
  @ApiProperty({
    required: false,
  })
  width: number;
  @ApiProperty({
    required: false,
  })
  height: number;
  @ApiProperty({
    required: false,
  })
  wUnits: string;
  @ApiProperty({
    required: false,
  })
  hUnits: string;
  @ApiProperty({
    required: false,
  })
  length: number;
  @ApiProperty({
    required: false,
  })
  type: string;
  @ApiProperty({
    required: false,
  })
  url: string;
  @ApiProperty({
    required: false,
  })
  mime: string;
  @ApiProperty({
    required: false,
    type: Number,
  })
  orientation: Orientation;
}
