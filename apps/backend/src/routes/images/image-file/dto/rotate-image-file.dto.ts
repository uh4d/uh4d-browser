import { ApiProperty } from '@nestjs/swagger';
import { IsIn, IsNotEmpty } from 'class-validator';

export class RotateImageFileDto {
  @ApiProperty({
    description: 'Rotation angle',
    enum: [-90, 90],
  })
  @IsNotEmpty()
  @IsIn([-90, 90])
  angle: number;
}
