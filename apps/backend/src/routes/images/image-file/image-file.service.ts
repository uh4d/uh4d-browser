import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import * as sharp from 'sharp';
import * as fs from 'fs-extra';
import { nanoid } from 'nanoid';
import { join, parse } from 'node:path';
import * as probe from 'probe-image-size';
import { removeIllegalFileChars } from '@uh4d/utils';
import { ImageFileDto } from '@backend/generated/dto';
import { FotothekParserService } from '@backend/routes/images/fotothek-parser/fotothek-parser.service';
import { ImageMediaService } from '@backend/upload/image-media.service';
import { ResizeImageConfig, resizeImageTypes } from '@uh4d/config';
import { BackendConfig } from '@backend/config';
import { FileUpdateFailDto, FileUpdateSuccessDto } from './dto/file-update.dto';

@Injectable()
export class ImageFileService {
  constructor(
    private readonly neo4j: Neo4jService,
    private readonly config: BackendConfig,
    private readonly fotothekParser: FotothekParserService,
    private readonly imageMediaService: ImageMediaService,
  ) {}

  /**
   * Get file and identifier information of image.
   */
  private async getFileMeta(
    imageId: string,
  ): Promise<ImageFileDto & { id: string; permalink?: string }> {
    // language=Cypher
    const qGet = `
      MATCH (image:E38:UH4D {id: $imageId})<-[:P67]-(file:D9),
            (image)-[:P48]->(identifier:E42)
      RETURN file, identifier.permalink AS permalink
    `;

    const results = await this.neo4j.read<{
      file: ImageFileDto & { id: string };
      permalink: string | null;
    }>(qGet, {
      imageId,
    });

    if (results[0]) {
      return {
        ...results[0].file,
        permalink: results[0].permalink,
      };
    } else {
      throw new NotFoundException(`Image with ID ${imageId} not found!`);
    }
  }

  async checkFileUpdate(
    imageId: string,
  ): Promise<FileUpdateSuccessDto | FileUpdateFailDto> {
    // get image metadata
    const fileMeta = await this.getFileMeta(imageId);

    if (!fileMeta.permalink) {
      return {
        updateAvailable: false,
        reason: 'No permalink available!',
      };
    }

    // parse image url
    const imageUrl = await this.fotothekParser.getImageUrl(fileMeta.permalink);
    if (!imageUrl) {
      return {
        updateAvailable: false,
        reason: `Couldn't retrieve image url from ${fileMeta.permalink}!`,
      };
    }

    // get image size of online image
    const imageSize = await probe(imageUrl);
    return {
      ...imageSize,
      updateAvailable:
        imageSize.width > fileMeta.width || imageSize.height > fileMeta.height,
    } as FileUpdateSuccessDto;
  }

  async updateFile(imageId: string): Promise<Omit<ImageFileDto, 'pkl'>> {
    // get image metadata
    const fileMeta = await this.getFileMeta(imageId);

    // parse image url
    const imageUrl = await this.fotothekParser.getImageUrl(fileMeta.permalink);
    if (!imageUrl) {
      throw new NotFoundException(
        `Couldn't retrieve image url from ${fileMeta.permalink}!`,
      );
    }

    // create temporary directory
    const tmpDir = join(this.config.dirData, 'tmp', nanoid(9));
    const fileName = removeIllegalFileChars(imageUrl.split('/').pop());
    const tmpFile = join(tmpDir, fileName);
    let finalPath: string;

    try {
      await fs.ensureDir(tmpDir);

      // download image to tmp file
      await this.fotothekParser.downloadImage(imageUrl, tmpFile);

      // generate new thumbnails
      const newFileMeta = await this.imageMediaService.generateImageMedia(
        tmpFile,
      );

      // copy into image folder
      await fs.copy(
        join(tmpDir, newFileMeta.path),
        join(this.config.dirData, newFileMeta.path),
      );
      finalPath = newFileMeta.path;

      // update database entry
      // language=Cypher
      const q = `
        MATCH (image:E38:UH4D {id: $imageId})<-[:P67]-(file:D9)
        SET file += $file
        RETURN file;
      `;
      const params = {
        imageId,
        file: newFileMeta,
      };
      const result = await this.neo4j.write(q, params);
      if (!result[0]) {
        throw new Error('No data written to database!');
      }

      // remove old files
      await fs.remove(join(this.config.dirData, fileMeta.path));

      return newFileMeta;
    } catch (e) {
      if (finalPath) {
        await fs.remove(join(this.config.dirData, finalPath));
      }
      throw new InternalServerErrorException(e);
    } finally {
      // remove temporary folder
      await fs.remove(tmpDir);
    }
  }

  /**
   * Rotate image by 90 degree.
   */
  async rotateImage(imageId: string, angle: number): Promise<ImageFileDto> {
    if (!(angle === 90 || angle === -90)) {
      throw new Error('Angle other than -90 or +90 is not yet supported');
    }

    // get image data
    const fileRef = await this.getFileMeta(imageId);

    // rotate images
    const absOriginal = join(
      this.config.dirData,
      fileRef.path,
      fileRef.original,
    );
    // apply rotation of EXIF data
    const bufferOriginal = await sharp(absOriginal).rotate().toBuffer();
    // make final rotation
    await sharp(bufferOriginal).rotate(angle).toFile(absOriginal);

    // regenerate thumbnails
    return this.regenerateThumbnails(fileRef);
  }

  async regenerateThumbnails(
    imageIdOrFileRef: string | (ImageFileDto & { id: string }),
  ): Promise<ImageFileDto> {
    const fileRef =
      typeof imageIdOrFileRef === 'string'
        ? await this.getFileMeta(imageIdOrFileRef)
        : imageIdOrFileRef;

    const absOriginal = join(
      this.config.dirData,
      fileRef.path,
      fileRef.original,
    );
    const parsedPath = parse(fileRef.original);
    const thumbFiles = {
      preview: ResizeImageConfig.createFilename(parsedPath.base, 'preview'),
      thumb: ResizeImageConfig.createFilename(parsedPath.base, 'thumb'),
      tiny: ResizeImageConfig.createFilename(parsedPath.base, 'tiny'),
      texture: ResizeImageConfig.createFilename(parsedPath.base, 'texture'),
    };

    const image = sharp(absOriginal);
    const metadata = await image.metadata();

    // regenerate thumbnails
    await this.imageMediaService.generateThumbnails(image, {
      preview: join(this.config.dirData, fileRef.path, thumbFiles.preview),
      thumb: join(this.config.dirData, fileRef.path, thumbFiles.thumb),
      tiny: join(this.config.dirData, fileRef.path, thumbFiles.tiny),
      texture: join(this.config.dirData, fileRef.path, thumbFiles.texture),
    });

    // update width, height
    // language=Cypher
    const qUpdate = `
      MATCH (file:D9:UH4D {id: $id})
      SET file += $fileUpdate
      RETURN apoc.map.removeKey(file, 'id') AS file
    `;
    const params = {
      id: fileRef.id,
      fileUpdate: {
        ...thumbFiles,
        ...this.imageMediaService.getNormalSize(metadata),
      },
    };

    try {
      const rResults = await this.neo4j.write<{ file: ImageFileDto }>(
        qUpdate,
        params,
      );

      return rResults[0].file;
    } finally {
      // remove old files
      for (const type of resizeImageTypes) {
        if (fileRef[type] && fileRef[type] !== thumbFiles[type])
          await fs.remove(
            join(this.config.dirData, fileRef.path, fileRef[type]),
          );
      }
    }
  }
}
