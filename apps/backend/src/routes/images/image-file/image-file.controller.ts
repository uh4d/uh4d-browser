import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import {
  ApiExtraModels,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  getSchemaPath,
} from '@nestjs/swagger';
import { Auth } from '@backend/auth/auth.decorator';
import { AuthUser, User } from '@backend/auth/user';
import { CommonService } from '@backend/common/common.service';
import { ImageFileDto } from '@backend/generated/dto';
import { FileUpdateFailDto, FileUpdateSuccessDto } from './dto/file-update.dto';
import { RotateImageFileDto } from './dto/rotate-image-file.dto';
import { ImageFileService } from './image-file.service';

@ApiTags('Images')
@Controller('images/:imageId/file')
export class ImageFileController {
  constructor(
    private readonly imageFileService: ImageFileService,
    private readonly commonService: CommonService,
  ) {}

  @ApiOperation({
    summary: 'Check file update',
    description:
      'Check if a new, bigger sized image file is available. Requires permalink to _Deutsche Fotothek_ site.',
  })
  @ApiExtraModels(FileUpdateSuccessDto, FileUpdateFailDto)
  @ApiOkResponse({
    schema: {
      oneOf: [
        { $ref: getSchemaPath(FileUpdateSuccessDto) },
        { $ref: getSchemaPath(FileUpdateFailDto) },
      ],
    },
  })
  @ApiNotFoundResponse()
  @Auth()
  @Get('check')
  async checkFileUpdate(@Param('imageId') imageId: string) {
    return this.imageFileService.checkFileUpdate(imageId);
  }

  @ApiOperation({
    summary: 'Update file',
    description:
      'Retrieve new image from permalink and replace old image files. (Currently only available for _Deutsche Fotothek_.)',
  })
  @ApiOkResponse({
    type: ImageFileDto,
  })
  @ApiNotFoundResponse()
  @Auth()
  @Get('update')
  async updateFile(@Param('imageId') imageId: string, @User() user: AuthUser) {
    const imageFile = await this.imageFileService.updateFile(imageId);
    await this.commonService.updateEditedBy(user, imageId);
    return imageFile;
  }

  @ApiOperation({
    summary: 'Rotate image',
    description:
      'Rotate image by -90 or +90 degrees. All preview and thumbnail files will be rotated as well.',
  })
  @ApiOkResponse({
    type: ImageFileDto,
  })
  @ApiNotFoundResponse()
  @Auth()
  @Post('rotate')
  async rotateImage(
    @Param('imageId') imageId: string,
    @Body() body: RotateImageFileDto,
    @User() user: AuthUser,
  ): Promise<ImageFileDto> {
    const imageFile = await this.imageFileService.rotateImage(
      imageId,
      body.angle,
    );
    await this.commonService.updateEditedBy(user, imageId);
    return imageFile;
  }

  @ApiOperation({
    summary: 'Update thumbnails',
    description:
      'Regenerate thumbnails and preview image and update width and height dimension in database entry.',
  })
  @ApiOkResponse({
    type: ImageFileDto,
  })
  @ApiNotFoundResponse()
  @Auth()
  @Post('thumbnails')
  async updateThumbnails(
    @Param('imageId') imageId: string,
  ): Promise<ImageFileDto> {
    return this.imageFileService.regenerateThumbnails(imageId);
  }
}
