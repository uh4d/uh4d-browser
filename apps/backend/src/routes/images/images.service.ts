import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { nanoid } from 'nanoid';
import * as fs from 'fs-extra';
import { join, parse } from 'node:path';
import { Express } from 'express';
import { isNumber } from 'class-validator';
import { escapeRegex, removeIllegalFileChars } from '@uh4d/utils';
import { parseDate } from '@backend/utils/date-utils';
import {
  checkRequiredProperties,
  extractUpdateProperties,
} from '@backend/utils/body-utils';
import { getSpatialParams } from '@backend/utils/spatial-params';
import { CreateImageFileDto, ImageDto } from '@backend/dto';
import { LocationQueryParams } from '@backend/routes/dto/location-query-params';
import { AnnotationsService } from '@backend/routes/annotations/annotations.service';
import { CombinedSimilarAnnotationsDto } from '@uh4d/dto/interfaces/custom/annotation';
import { UploadImageDto } from '@backend/routes/images/dto/upload-image.dto';
import { ImageMediaService } from '@backend/upload/image-media.service';
import { BackendConfig } from '@backend/config';
import { AuthUser } from '@backend/auth/user';
import { GetImagesQueryParams } from './dto/get-images-query-params';
import {
  CreateImageFullDto,
  ImageFullDto,
  UpdateImageFullDto,
} from './dto/image-full.dto';
import { ImageWithWeightsAndAnnotationsDto } from './dto/image-extended.dto';

@Injectable()
export class ImagesService {
  private readonly logger = new Logger(ImagesService.name);

  constructor(
    private readonly neo4j: Neo4jService,
    private readonly config: BackendConfig,
    private readonly imageMediaService: ImageMediaService,
    private readonly annotationsService: AnnotationsService,
  ) {}

  async query(
    query: GetImagesQueryParams,
    userId?: string,
  ): Promise<ImageWithWeightsAndAnnotationsDto[]> {
    const stringFilter = [];
    const objFilter = [];
    const annFilter = [];
    const regexPosList = [];
    const regexNegList = [];
    const regexAuthorList = [];
    const regexOwnerList = [];

    let pending = false;
    let declined = false;

    // first triage
    (query.q ? (Array.isArray(query.q) ? query.q : [query.q]) : []).forEach(
      (value) => {
        if (/^obj:/.test(value)) {
          objFilter.push(value.replace(/^obj:/, ''));
        } else if (/^ann:/.test(value)) {
          annFilter.push(value.replace(/^ann:/, ''));
        } else if (value === 'unvalidated') {
          pending = true;
        } else if (value === 'pending') {
          pending = true;
        } else if (value === 'declined') {
          declined = true;
        } else {
          stringFilter.push(value);
        }
      },
    );

    const spatialParams = getSpatialParams(query);

    let q = '';

    if (spatialParams) {
      q +=
        'WITH point({latitude: $geo.latitude, longitude: $geo.longitude}) AS userPoint ';
    }

    if (query.scene) {
      q += `MATCH (:E53:UH4D {id: $scene})<-[:P89]-`;
    } else {
      q += 'MATCH ';
    }

    q += `(place:E53)<-[:P7]-(e65:E65)-[:P94]->(image:E38)
      WHERE coalesce(image.pending, FALSE) = $pending
      AND coalesce(image.declined, FALSE) = $declined`;

    if (userId) {
      q += ` AND (image)-[:uploaded_by]->(:User:UH4D {id: $userId})`;
    }

    if (objFilter.length) {
      q += `
      MATCH (image)-[:P138]->(e22:E22)
      WHERE e22.id IN $objList
    `;
    }

    if (spatialParams) {
      q += `
      MATCH (place)-[:P168]->(geo:E94)
      WHERE distance(geo.point, userPoint) < $geo.radius + coalesce(geo.radius, 0)
    `;
    }

    if (
      query.from &&
      query.to &&
      query.from !== 'null' &&
      query.to !== 'null'
    ) {
      q += `
      OPTIONAL MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
      WITH image, place, e65, date
      `;
      if (query.useAsTexture) {
        q += `
        WHERE CASE WHEN image.vrcity_useAsTextureFrom IS NOT NULL OR image.vrcity_useAsTextureTo IS NOT NULL THEN
          coalesce(
            coalesce(image.vrcity_useAsTextureTo >= date($from), TRUE) AND
            coalesce(image.vrcity_useAsTextureFrom <= date($to), TRUE),
            date.to >= date($from) AND date.from <= date($to)
          )
          ELSE date.to >= date($from) AND date.from <= date($to) END
        `;
      } else {
        q += `
          WHERE date ${query.undated ? 'IS NULL OR' : 'IS NOT NULL AND'}
          date.to >= date($from) AND date.from <= date($to)
        `;
      }
    }

    q += `
      WITH image, place, e65
      MATCH (image)<-[:P67]-(file:D9),
            (image)-[:P102]->(title:E35),
            (image)-[:P48]->(identifier:E42)
      OPTIONAL MATCH (e65)-[:P14]->(authorId:E21)-[:P131]->(author:E82)
      OPTIONAL MATCH (image)-[:P105]->(ownerId:E40)-[:P131]->(owner:E82)
      OPTIONAL MATCH (image)-[:P104]->(right:E30)
      OPTIONAL MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
      OPTIONAL MATCH (e65)-[:P16]->(spatial:E22)
      OPTIONAL MATCH (place)-[:P168]->(geo:E94)
      OPTIONAL MATCH (image)-[:has_tag]->(tag:TAG)
      WITH image, file, title, identifier, authorId, author, date, ownerId, owner, right, spatial, geo, collect(tag.id) AS tags
      CALL {
        WITH image
        OPTIONAL MATCH (image)-[:P106]->(ann:Annotation)-[:P2]->(annType:Wikidata)
        RETURN annType
        UNION
        WITH image
        OPTIONAL MATCH (image)-[:P106]->(ann:Annotation)-[:P2]->(annType:AAT)
        RETURN annType
      }
      WITH image, file, title, identifier, authorId, author, date, ownerId, owner, right, spatial, geo, tags, collect(annType) as annTypes
    `;

    stringFilter.forEach((value, index) => {
      const qPrefix = index === 0 ? ' WHERE' : ' AND';

      if (value === 'spatial:set') {
        q += qPrefix + ' spatial IS NOT NULL';
      } else if (value === 'spatial:unset') {
        q += qPrefix + ' spatial IS NULL';
      } else if (/^author:/.test(value)) {
        if (regexAuthorList.length < 1) {
          q +=
            qPrefix +
            ` any(e IN $regexAuthorList
                WHERE authorId.id = e
                OR author.value =~ "(?i).*" + e + ".*"
              )`;
        }
        regexAuthorList.push(escapeRegex(value.replace(/^author:/, '')));
      } else if (/^owner:/.test(value)) {
        if (regexOwnerList.length < 1) {
          q +=
            qPrefix +
            ` any(e IN $regexOwnerList
                WHERE ownerId.id = e
                OR owner.value =~ "(?i).*" + e + ".*"
              )`;
        }
        regexOwnerList.push(escapeRegex(value.replace(/^owner:/, '')));
      } else if (/^-/.test(value)) {
        if (regexNegList.length < 1) {
          q +=
            qPrefix +
            ` none(e IN $regexNegList
                WHERE coalesce(title.value, "") =~ e
                OR coalesce(author.value, "") =~ e
                OR coalesce(owner.value, "") =~ e
                OR any(tag IN tags WHERE tag =~ e)
                OR any(ann IN annTypes WHERE ann.label =~ e)
                OR coalesce(identifier.slub_cap_no, "") =~ e
              )`;
        }
        regexNegList.push(
          `(?i).*${escapeRegex(value.replace(/^-/, '').replace(/"/g, ''))}.*`,
        );
      } else {
        if (regexPosList.length < 1) {
          q +=
            qPrefix +
            ` all(e IN $regexPosList
                WHERE title.value =~ e
                OR author.value =~ e
                OR owner.value =~ e
                OR any(tag IN tags WHERE tag =~ e)
                OR any(ann IN annTypes WHERE ann.label =~ e)
                OR identifier.slub_cap_no =~ e
              )`;
        }
        regexPosList.push(`(?i).*${escapeRegex(value.replace(/"/g, ''))}.*`);
      }
    });

    q += `
      WITH image, file, title, author, date, owner, right, spatial, geo, tags, annTypes
      OPTIONAL MATCH (image)-[userEventU:uploaded_by]->(userU:User)-[:P131]->(usernameU:E82)
      OPTIONAL MATCH (image)-[userEventE:edited_by]->(userE:User)-[:P131]->(usernameE:E82)
    `;

    if (query.weights) {
      q += `OPTIONAL MATCH (image)-[rw:P138]->(e22:E22)`;
    }

    // language=Cypher
    q += `
      RETURN image.id AS id,
        apoc.map.removeKey(file, 'id') AS file,
        title.value AS title,
        author.value AS author,
        owner.value AS owner,
        right.restricted AS restrictedAccess,
        date,
        apoc.map.removeKeys(apoc.map.merge(geo, spatial), ['id', 'method', 'point', 'status']) AS camera,
        geo.status AS spatialStatus,
        image.vrcity_useAsTextureFrom AS vrcity_useAsTextureFrom,
        image.vrcity_useAsTextureTo AS vrcity_useAsTextureTo,
        image.vrcity_projectionDistance AS vrcity_projectionDistance,
        size(annTypes) > 0 AS annotationsAvailable,
        image.pending AS pending,
        image.pending AS needsValidation,
        image.declined AS declined,
        userU{userId: userU.id, username: usernameU.value, timestamp: userEventU.timestamp} AS uploadedBy,
        userE{userId: userE.id, username: usernameE.value, timestamp: userEventE.timestamp} AS editedBy
    `;
    // ToDo: remove needsValidation

    if (query.weights) {
      q += `,
      CASE
        WHEN e22 IS NULL THEN []
        ELSE collect({id: e22.id, weight: rw.weight})
      END AS objects`;
    }

    const params = {
      userId,
      scene: query.scene,
      from: query.from,
      to: query.to,
      regexPosList,
      regexNegList,
      regexAuthorList,
      regexOwnerList,
      objList: objFilter,
      geo: spatialParams,
      pending,
      declined,
    };

    // run Neo4j query
    type ObjectWeightsList = { id: string; weight: number }[];
    let results = await this.neo4j.read<
      ImageDto & { objects?: ObjectWeightsList }
    >(q, params);

    // query annotations and filter results
    const annotations: CombinedSimilarAnnotationsDto[] = [];
    if (annFilter.length) {
      annotations.push(
        ...(await this.annotationsService.querySimilarImagesByAnnotations(
          annFilter,
        )),
      );
      results = results.filter((img) =>
        annotations.find((ann) => ann.id === img.id),
      );
    }

    // enhance data with annotation query results
    return results.map((img) => {
      const record = {
        ...img,
        objects: undefined,
      } as ImageWithWeightsAndAnnotationsDto;

      if (img.objects) {
        record.objects = img.objects.reduce((map, curr) => {
          map[curr.id] = curr.weight;
          return map;
        }, {});
      }

      if (annFilter.length) {
        const annotation = annotations.find((ann) => ann.id === img.id);
        record.annotations = Object.entries(
          annotation.annotationRecords,
        ).reduce((map, [annId, annRecord]) => {
          annRecord.annotations.sort((a, b) => b.similarity - a.similarity);
          map[annId] = annRecord.annotations;
          return map;
        }, {});
        record.relevance = annotation.combinedSimilarity;
      }

      return record;
    });
  }

  async getById(
    imageId: string,
    fuzzySearch = false,
  ): Promise<ImageFullDto | undefined> {
    let q = '';

    if (fuzzySearch) {
      q += `MATCH (image:E38:UH4D)
        WHERE image.id =~ ".*" + $id + ".*"
        MATCH `;
    } else {
      q += 'MATCH (image:E38:UH4D {id: $id}),';
    }

    // language=Cypher
    q += `  (image)<-[:P94]-(e65:E65),
            (image)<-[:P67]-(file:D9),
            (image)-[:P102]->(title:E35),
            (image)-[:P48]->(identifier:E42)
      OPTIONAL MATCH (e65)-[:P14]->(:E21)-[:P131]->(author:E82)
      OPTIONAL MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
      OPTIONAL MATCH (image)-[:P105]->(:E40)-[:P131]->(owner:E82)
      OPTIONAL MATCH (image)-[:P104]->(right:E30)
      OPTIONAL MATCH (image)-[:P3]->(desc:E62)-[:P3_1]->(:E55 {id: "image_description"})
      OPTIONAL MATCH (image)-[:P3]->(misc:E62)-[:P3_1]->(:E55 {id: "image_miscellaneous"})
      OPTIONAL MATCH (e65)-[:P16]->(spatial:E22)
      OPTIONAL MATCH (e65)-[:P7]->(:E53)-[:P168]->(geo:E94)
      OPTIONAL MATCH (image)-[userEventU:uploaded_by]->(userU:User)-[:P131]->(usernameU:E82)
      OPTIONAL MATCH (image)-[userEventE:edited_by]->(userE:User)-[:P131]->(usernameE:E82)
      OPTIONAL MATCH (image)-[:has_tag]->(tag:TAG)
      WITH image, file, title, identifier, author, date, owner, right, desc, misc, spatial, geo,
           userU{userId: userU.id, username: usernameU.value, timestamp: userEventU.timestamp} AS uploadedBy,
           userE{userId: userE.id, username: usernameE.value, timestamp: userEventE.timestamp} AS editedBy,
           collect(tag.id) AS tags
      CALL {
        WITH image
        OPTIONAL MATCH (image)-[:P106]->(ann:Annotation)-[:P2]->(annType:Wikidata)
        RETURN annType
        UNION
        WITH image
        OPTIONAL MATCH (image)-[:P106]->(ann:Annotation)-[:P2]->(annType:AAT)
        RETURN annType
      }

      RETURN image.id AS id,
        apoc.map.removeKey(file, 'id') AS file,
        title.value AS title,
        identifier.permalink AS permalink,
        identifier.slub_cap_no AS captureNumber,
        author.value AS author,
        date {.*, from: toString(date.from), to: toString(date.to)} AS date,
        owner.value AS owner,
        right.restricted AS restrictedAccess,
        desc.value AS description,
        misc.value AS misc,
        apoc.map.removeKeys(apoc.map.merge(geo, spatial), ['id', 'method', 'point', 'status']) AS camera,
        geo.status AS spatialStatus,
        tags,
        image.vrcity_useAsTextureFrom AS vrcity_useAsTextureFrom,
        image.vrcity_useAsTextureTo AS vrcity_useAsTextureTo,
        image.vrcity_projectionDistance AS vrcity_projectionDistance,
        count(annType) > 0 AS annotationsAvailable,
        image.pending AS pending,
        image.pending AS needsValidation,
        image.declined AS declined,
        uploadedBy,
        editedBy
    `;
    // ToDo: remove needsValidation

    const params = {
      id: imageId,
    };

    return (await this.neo4j.read(q, params))[0];
  }

  async upload(
    file: Express.Multer.File,
    body: UploadImageDto,
    user: AuthUser,
  ): Promise<ImageFullDto> {
    const tmpDir = join(this.config.dirTmp, 'tmp', nanoid(9));
    const fileName = removeIllegalFileChars(file.originalname);
    const tmpFile = join(tmpDir, fileName);
    let finalPath: string;

    try {
      // create tmp folder
      await fs.ensureDir(tmpDir);
      // copy uploaded file to tmp folder
      await fs.rename(file.path, tmpFile);

      const meta: CreateImageFullDto & { file: CreateImageFileDto } = {
        title: body.title || parse(file.originalname).name,
        camera: {
          latitude: body.latitude,
          longitude: body.longitude,
          altitude: 0,
          radius: body.radius,
          offset: [],
          k: [],
        },
        spatialStatus: body.radius > 0 ? 0 : 3,
        file: await this.imageMediaService.generateImageMedia(tmpFile),
        tags: [],
        description: body.description,
        author: body.author,
        date: body.date ? { value: body.date } : undefined,
      };

      // copy into image folder
      await fs.copy(
        join(tmpDir, meta.file.path),
        join(this.config.dirData, meta.file.path),
      );

      finalPath = meta.file.path;

      // write to database
      const result = await this.create(meta, user);

      if (!result) {
        // remove generated files
        await fs.remove(join(this.config.dirData, meta.file.path));
        throw new Error('No data written to database!');
      }

      // retrieve image meta data
      return this.getById(result.image.id);
    } catch (e) {
      this.logger.error(e);
      if (finalPath) {
        await fs.remove(join(this.config.dirData, finalPath));
      }
      throw new InternalServerErrorException(e);
    } finally {
      // remove temporary folder
      await fs.remove(tmpDir);
    }
  }

  async create(
    data: CreateImageFullDto & { file: CreateImageFileDto },
    user: AuthUser,
  ): Promise<{ image: { id: string } } | undefined> {
    const date = parseDate(data.date?.value);

    let q = `
      MATCH (tdesc:E55:UH4D {id: "image_description"}), (tmisc:E55:UH4D {id: "image_miscellaneous"})
      MATCH (user:User:UH4D { id: $userId })
      CREATE (image:E38:UH4D {id: $imageId}),
        (image)-[:P102]->(title:E35:UH4D $title),
        (image)<-[:P67]-(file:D9:UH4D $file),
        (image)-[:P48]->(identifier:E42:UH4D $identifier),
        (image)<-[:P94]-(e65:E65:UH4D {id: $e65id})-[:P7]->(place:E53:UH4D {id: $placeId})-[:P168]->(geo:E94:UH4D $geo)
      SET geo.point = point({latitude: $geo.latitude, longitude: $geo.longitude}) `;

    if (user.needsValidation()) q += `SET image.pending = true `;

    q += `CREATE (image)-[:uploaded_by { timestamp: datetime() }]->(user) `;

    if (data.author)
      q += `MERGE (author:E21:UH4D)-[:P131]->(authorName:E82:UH4D {value: $author.value})
        ON CREATE SET author.id = $authorId, authorName.id = $author.id
      CREATE (e65)-[:P14]->(author) `;

    if (date)
      q += `CREATE (e65)-[:P4]->(:E52:UH4D {id: $e52id})-[:P82]->(date:E61:UH4D {id: $date.id, value: $date.value, from: date($date.from), to: date($date.to), display: $date.display}) `;

    if (data.owner)
      q += `MERGE (owner:E40:UH4D)-[:P131]->(ownerName:E82:UH4D {value: $owner.value})
        ON CREATE SET owner.id = $ownerId, ownerName.id = $owner.id
      CREATE (image)-[:P105]->(owner) `;

    if (data.description)
      q += `CREATE (image)-[:P3]->(desc:E62:UH4D $desc)-[:P3_1]->(tdesc) `;

    if (data.misc && data.misc.length)
      q += `CREATE (image)-[:P3]->(:E62:UH4D $misc)-[:P3_1]->(tmisc) `;

    q += `FOREACH (tag IN $tags |
        MERGE (t:TAG:UH4D {id: tag})
        MERGE (image)-[:has_tag]->(t)
      )

		  RETURN image`;

    const id = nanoid(9) + '_' + data.file.original;
    const authorId =
      nanoid(9) + '_' + removeIllegalFileChars(data.author || '', true);
    const ownerId =
      nanoid(9) + '_' + removeIllegalFileChars(data.owner || '', true);

    const params = {
      userId: user.id,
      imageId: id,
      title: {
        id: 'e35_' + id,
        value: data.title,
      },
      identifier: {
        id: 'e42_' + id,
        permalink: data.permalink,
        // slub_id: data.id, TODO: save SLUB id and numbers
        // slub_cap_no: data.captureNumber,
      },
      file: { id: 'd9_' + id, ...data.file },
      placeId: 'e53_' + id,
      geo: {
        id: 'e94_' + id,
        latitude: data.camera.latitude,
        longitude: data.camera.longitude,
        altitude: data.camera.altitude,
        radius: data.camera.radius || 0,
        status: data.spatialStatus,
      },
      e65id: 'e65_' + id,
      e52id: 'e52_' + id,
      date: { id: 'e61_e52_' + id, ...date },
      author: {
        id: 'e82_' + authorId,
        value: data.author,
      },
      authorId: 'e21_' + authorId,
      owner: {
        id: 'e82_' + ownerId,
        value: data.owner,
      },
      ownerId: 'e40_' + ownerId,
      desc: {
        id: 'e62_desc_' + id,
        value: data.description,
      },
      misc: {
        id: 'e62_misc_' + id,
        value: Array.isArray(data.misc) ? data.misc.join(', ') : data.misc,
      },
      tags: data.tags || [],
    };

    return (await this.neo4j.write(q, params))[0];
  }

  async update(
    imageId: string,
    body: UpdateImageFullDto,
  ): Promise<ImageFullDto | undefined> {
    // get current entry
    const image = await this.getById(imageId);
    if (!image) return undefined;

    // build cypher statements for properties that need to be updated
    const updateImage = extractUpdateProperties(image, body);

    const statements = Object.entries(updateImage).map(([key, value]) =>
      this.buildUpdateStatement(image, { [key]: value }),
    );
    await this.neo4j.multipleStatements(statements);

    // get complete updated entry
    return this.getById(imageId);
  }

  private buildUpdateStatement(
    currentEntry: ImageFullDto,
    body: UpdateImageFullDto,
  ): { statement: string; parameters: Record<string, any> } {
    let q = `MATCH (image:E38:UH4D {id: $id})<-[:P94]-(e65:E65)-[:P7]->(place:E53)-[:P168]->(geo:E94) `;

    const params: Record<string, any> = {
      id: currentEntry.id,
    };

    const id = nanoid(9);

    switch (Object.keys(body)[0] as keyof UpdateImageFullDto) {
      case 'title':
        if (!body.title) {
          throw new BadRequestException('Title must not be empty!');
        }

        q += `MATCH (image)-[:P102]->(title:E35) SET title.value = $title`;
        params.title = body.title;
        break;

      case 'date': {
        const date = parseDate(body.date.value);
        if (!date) {
          throw new BadRequestException(
            '`date` must be in the format `{ value: string }` in order to be updated!',
          );
        }
        q += `
          MERGE (e65)-[:P4]->(e52:E52:UH4D)-[:P82]->(date:E61:UH4D)
            ON CREATE SET e52.id = $e52id, date.id = $e61id
          SET date.value = $date.value`;
        if (date) {
          q += `,
            date.from = date($date.from),
            date.to = date($date.to),
            date.display = $date.display`;
        }
        params.date = date;
        params.e52id = 'e52_' + id;
        params.e61id = 'e61_e52_' + id;
        break;
      }

      case 'vrcity_useAsTextureFrom':
        q += 'SET image.vrcity_useAsTextureFrom = date($useAsTextureFrom)';
        params.useAsTextureFrom = body.vrcity_useAsTextureFrom;
        break;
      case 'vrcity_useAsTextureTo':
        q += 'SET image.vrcity_useAsTextureTo = date($useAsTextureTo)';
        params.useAsTextureTo = body.vrcity_useAsTextureTo;
        break;

      case 'vrcity_projectionDistance':
        q += 'SET image.vrcity_projectionDistance = $projectionDistance';
        params.projectionDistance = body.vrcity_projectionDistance;
        break;

      case 'description':
        q += `MATCH (tdesc:E55:UH4D {id: "image_description"}) `;
        if (body.description) {
          // set desc
          q += `
            MERGE (image)-[:P3]->(desc:E62:UH4D)-[:P3_1]->(tdesc)
              ON CREATE SET desc.id = $desc.id
            SET desc.value = $desc.value`;
          params.desc = { id: 'e62_desc_' + id, value: body.description };
        } else {
          // remove desc
          q += `OPTIONAL MATCH (image)-[:P3]->(desc:E62)-[:P3_1]->(tdesc) DETACH DELETE desc`;
        }
        break;

      case 'misc':
        q += `MATCH (tmisc:E55:UH4D {id: "image_miscellaneous"}) `;
        if (body.misc) {
          // set misc
          q += `
            MERGE (image)-[:P3]->(misc:E62:UH4D)-[:P3_1]->(tmisc)
              ON CREATE SET misc.id = $misc.id
            SET misc.value = $misc.value`;
          params.misc = { id: 'e62_misc_' + id, value: body.misc };
        } else {
          // remove misc
          q += `OPTIONAL MATCH (image)-[:P3]->(misc:E62)-[:P3_1]->(tmisc) DETACH DELETE misc`;
        }
        break;

      case 'author':
        q += `OPTIONAL MATCH (e65)-[r14:P14]->(:E21)-[:P131]->(:E82)`;
        if (body.author) {
          q += `
            MERGE (e21:E21:UH4D)-[:P131]->(e82:E82:UH4D {value: $name})
              ON CREATE SET e21.id = $e21id, e82.id = $e82id
            CREATE (e65)-[:P14]->(e21)`;
          params.name = body.author;
          params.e21id = `e21_${id}_${removeIllegalFileChars(
            body.author,
            true,
          )}`;
          params.e82id = `e82_${id}_${removeIllegalFileChars(
            body.author,
            true,
          )}`;
        }
        q += ` DELETE r14`;
        break;

      case 'owner':
        q += `OPTIONAL MATCH (image)-[r105:P105]->(:E40)-[:P131]->(:E82)`;
        if (body.owner) {
          q += `
            MERGE (e40:E40:UH4D)-[:P131]->(e82:E82:UH4D {value: $owner})
              ON CREATE SET e40.id = $e40id, e82.id = $e82id
            CREATE (image)-[:P105]->(e40)`;
          params.owner = body.owner;
          params.e40id = `e40_${id}_${removeIllegalFileChars(
            body.owner,
            true,
          )}`;
          params.e82id = `e82_${id}_${removeIllegalFileChars(
            body.owner,
            true,
          )}`;
        }
        q += ' DELETE r105';
        break;

      case 'permalink':
        q += `MATCH (image)-[:P48]->(identifier:E42)`;
        if (body.permalink) {
          q += ' SET identifier.permalink = $permalink';
          params.permalink = body.permalink;
        } else {
          q += ' REMOVE identifier.permalink';
        }
        break;

      case 'restrictedAccess':
        if (body.restrictedAccess) {
          q += `
            MERGE (image)-[:P104]->(right:E30:UH4D)
              ON CREATE SET right.id = $right.id
            SET right.restricted = $right.restricted`;
          params.right = {
            id: 'e30_right_' + id,
            restricted: body.restrictedAccess,
          };
        } else {
          q +=
            ' OPTIONAL MATCH (image)-[:P104]->(right:E30) DETACH DELETE right';
        }
        break;

      case 'tags':
        q += `
          OPTIONAL MATCH (image)-[rtag:has_tag]->(:TAG)
          DELETE rtag
          WITH image
          FOREACH (tag in $tags |
            MERGE (t:TAG:UH4D {id: tag})
            MERGE (image)-[:has_tag]->(t)
          )`;
        params.tags = body.tags || [];
        break;

      case 'camera': {
        if (!body.camera) {
          throw new BadRequestException('camera cannot be null on update');
        }

        params.geo = {
          latitude: body.camera.latitude,
          longitude: body.camera.longitude,
          altitude: body.camera.altitude,
        };

        if (
          isNumber(body.camera.omega) ||
          isNumber(body.camera.phi) ||
          isNumber(body.camera.kappa) ||
          isNumber(body.camera.ck)
        ) {
          // full (manual) spatialization
          const requiredProps = ['omega', 'phi', 'kappa', 'ck'];
          if (!checkRequiredProperties(body.camera, requiredProps)) {
            throw new BadRequestException(
              `For full camera update, ${requiredProps.join(
                ', ',
              )} need to be set`,
            );
          }

          q += `
            MERGE (e65)-[:P16]->(camera:E22:UH4D)
            SET camera = $camera
            SET geo += $geo
            SET geo.status = $spatialStatus, geo.point = point({latitude: $geo.latitude, longitude: $geo.longitude})
            REMOVE geo.radius
          `;

          params.camera = {
            id: 'camera_' + currentEntry.id,
            ck: body.camera.ck,
            offset: body.camera.offset || [0, 0],
            k: body.camera.k || [],
            omega: body.camera.omega,
            phi: body.camera.phi,
            kappa: body.camera.kappa,
            method: 'manual',
          };
          params.spatialStatus = 4;
        } else {
          // vague spatialization
          q += `
            OPTIONAL MATCH (e65)-[:P16]->(camera:E22:UH4D)
            DETACH DELETE camera
            SET geo += $geo
            SET geo.status = $spatialStatus, geo.point = point({latitude: $geo.latitude, longitude: $geo.longitude})
          `;

          params.spatialStatus =
            body.camera.radius !== currentEntry.camera.radius
              ? body.camera.radius > 0
                ? 0
                : 3
              : currentEntry.spatialStatus;
          params.geo.radius = body.camera.radius;
        }

        break;
      }

      case 'spatialStatus':
        // status can only be changed, if not yet spatialized
        if (currentEntry.camera) {
          throw new BadRequestException(
            'Image already properly spatialized. Status cannot be updated!',
          );
        }

        // update status
        switch (body.spatialStatus) {
          case 0:
            q += ' SET geo.status = 0';
            break;
          case 1:
            q += ' SET geo.status = 1';
            break;
          case 2:
            q += ' SET geo.status = 2';
            break;
          case 3:
            throw new BadRequestException(
              'spatialStatus: 3 cannot be set manually! Use `camera` property instead.',
            );
          case 4:
            throw new BadRequestException(
              'spatialStatus: 4 cannot be set manually! Use `camera` property instead.',
            );
          case 5:
            throw new BadRequestException(
              'spatialStatus: 5 cannot be set manually! Use POST `pipeline/images` route instead.',
            );
          default:
            throw new BadRequestException('No valid spatialStatus!');
        }
    }

    q += ' RETURN image';

    return { statement: q, parameters: params };
  }

  /**
   * Delete image.
   * @return Resolves `true` if image has been successfully deleted.
   */
  async remove(imageId: string): Promise<boolean> {
    // language=Cypher
    const q = `MATCH (image:E38:UH4D {id: $id})<-[:P94]-(e65:E65),
            (image)<-[:P67]-(file:D9),
            (image)-[:P102]->(title:E35),
            (image)-[:P48]->(identifier:E42)
      OPTIONAL MATCH (e65)-[:P4]->(e52:E52)-[:P82]->(date:E61)
      OPTIONAL MATCH (image)-[:P104]->(right:E30)
      OPTIONAL MATCH (image)-[:P3]->(desc:E62)-[:P3_1]->(:E55 {id: "image_description"})
      OPTIONAL MATCH (image)-[:P3]->(misc:E62)-[:P3_1]->(:E55 {id: "image_miscellaneous"})
      OPTIONAL MATCH (e65)-[:P16]->(spatial:E22)
      OPTIONAL MATCH (e65)-[:P7]->(place:E53)-[:P168]->(geo:E94)
      OPTIONAL MATCH (image)-[:P106]->(ann:Annotation)

      WITH image, e65, file, title, identifier, e52, date, right, desc, misc, spatial, place, geo, ann, file.path AS path

      DETACH DELETE image, e65, file, title, identifier, e52, date, right, desc, misc, spatial, place, geo, ann

      RETURN path
    `;

    const params = {
      id: imageId,
    };

    // wrap in custom transaction
    const session = this.neo4j.getWriteSession();
    const txc = session.beginTransaction();

    try {
      // execute cypher query
      const result = await txc.run(q, params);
      const records = Neo4jService.extractRecords<{ path: string }>(
        result.records,
      );

      if (!records[0]) {
        await txc.rollback();
        return false;
      }

      // check for corrupted path to not remove wrong directory
      const relPath = records[0].path;
      if (!/^images\/\S+$/.test(relPath)) {
        throw new Error('Path to delete does not match pattern!');
      }

      // remove files
      this.logger.log('Remove: ' + relPath);
      await fs.remove(join(this.config.dirData, relPath));

      await txc.commit();
      return true;
    } catch (e) {
      await txc.rollback();
      throw e;
    } finally {
      await session.close();
    }
  }

  async getDateExtent(query: LocationQueryParams): Promise<{ d: string }[]> {
    const spatialParams = {
      latitude: query.lat,
      longitude: query.lon,
      radius: query.r,
    };
    const doSpatialQuery =
      !isNaN(spatialParams.latitude) &&
      !isNaN(spatialParams.longitude) &&
      !isNaN(spatialParams.radius);

    let q = '';

    if (doSpatialQuery) {
      q +=
        'WITH point({latitude: $geo.latitude, longitude: $geo.longitude}) AS userPoint ';
    }

    // language=Cypher
    q += `
      MATCH (image:E38:UH4D)<-[:P94]-(e65:E65)-[:P7]->(place:E53)`;

    if (doSpatialQuery) {
      // language=Cypher
      q += `
        MATCH (place)-[:P168]->(geo:E94)
        WHERE distance(geo.point, userPoint) < coalesce(geo.radius, $geo.radius, 0)`;
    }

    // language=Cypher
    q += `
      CALL {
        WITH e65
        MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
        WHERE date.from IS NOT NULL
        RETURN date.from AS d
        ORDER BY d
        LIMIT 1
        UNION
        WITH e65
        MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
        WHERE date.to IS NOT NULL
        RETURN date.to AS d
        ORDER BY d DESC
        LIMIT 1
      }
      RETURN d
    `;

    const params = {
      geo: spatialParams,
    };

    return this.neo4j.read(q, params);
  }
}
