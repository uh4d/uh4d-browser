import { Test, TestingModule } from '@nestjs/testing';
import { FotothekParserService } from './fotothek-parser.service';

describe('FotothekParserService', () => {
  let service: FotothekParserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FotothekParserService],
    }).compile();

    service = module.get<FotothekParserService>(FotothekParserService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
