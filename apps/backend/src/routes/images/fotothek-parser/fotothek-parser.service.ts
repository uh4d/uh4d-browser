import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { pipeline } from 'node:stream/promises';
import { createWriteStream } from 'fs-extra';

@Injectable()
export class FotothekParserService {
  constructor(private readonly http: HttpService) {}

  /**
   * Parse image url from _Deutsche Fotothek_ site.
   */
  async getImageUrl(permalink: string) {
    const response = await this.http.axiosRef.get(permalink);
    const pattern = /<a class="download"[^<>]*href="([^"]*)"/;
    const matches = pattern.exec(response.data);
    return matches?.[1];
  }

  /**
   * Download image to file.
   */
  async downloadImage(url: string, file: string) {
    const response = await this.http.axiosRef.get(url, {
      responseType: 'stream',
    });

    return pipeline(response.data, createWriteStream(file));
  }
}
