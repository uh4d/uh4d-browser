import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { AuthModule } from '@backend/auth/auth.module';
import { CommonModule } from '@backend/common/common.module';
import { AnnotationsModule } from '@backend/routes/annotations/annotations.module';
import { ImageAnnotationsModule } from './image-annotations/image-annotations.module';
import { ImagesService } from './images.service';
import { ImagesController } from './images.controller';
import { ImageFileModule } from './image-file/image-file.module';
import { FotothekParserService } from './fotothek-parser/fotothek-parser.service';
import { LinkObjectsModule } from './link-objects/link-objects.module';
import { ImageCaptionModule } from './image-caption/image-caption.module';

@Module({
  imports: [
    HttpModule,
    AuthModule,
    CommonModule,
    AnnotationsModule,
    ImageFileModule,
    ImageAnnotationsModule,
    LinkObjectsModule,
    ImageCaptionModule,
  ],
  providers: [ImagesService, FotothekParserService],
  controllers: [ImagesController],
  exports: [ImagesService, FotothekParserService],
})
export class ImagesModule {}
