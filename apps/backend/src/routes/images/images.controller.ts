import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  NotFoundException,
  Param,
  Patch,
  Post,
  Query,
  Res,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiConsumes,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
import { Express, Response } from 'express';
import * as fs from 'fs-extra';
import { Auth } from '@backend/auth/auth.decorator';
import { AuthUser, User } from '@backend/auth/user';
import { CommonService } from '@backend/common/common.service';
import { UpdateValidationStatusDto } from '@backend/routes/dto/update-validation-status.dto';
import { ImagesService } from './images.service';
import { GetImageQueryParams } from './dto/get-image-query-params';
import { GetImagesQueryParams } from './dto/get-images-query-params';
import { ImageFullDto, UpdateImageFullDto } from './dto/image-full.dto';
import { DateExtentDto } from './dto/date-extent.dto';
import { UploadImageDto } from './dto/upload-image.dto';
import { ImageWithWeightsAndAnnotationsDto } from './dto/image-extended.dto';
import { LocationQueryParams } from '../dto/location-query-params';

@ApiTags('Images')
@Controller('images')
export class ImagesController {
  constructor(
    private readonly imagesService: ImagesService,
    private readonly commonService: CommonService,
  ) {}

  @ApiOperation({
    summary: 'Query images',
    description:
      'Query all images. Query parameters may be set to filter the data. Only the most essential information gets returned. For more detailed information of an image, refer to `/images/{imageId}`.\n\n`lat`, `lon`, and `r` work only in combination and may be used to look for images within a radius at a position.',
  })
  @ApiOkResponse({
    type: ImageWithWeightsAndAnnotationsDto,
    isArray: true,
  })
  @Get()
  async query(
    @Query() queryParams: GetImagesQueryParams,
  ): Promise<ImageWithWeightsAndAnnotationsDto[]> {
    return this.imagesService.query(queryParams);
  }

  @ApiOperation({
    summary: 'Get user uploads',
    description: 'Query images, the user has uploaded.',
    deprecated: true,
  })
  @ApiOkResponse({
    type: ImageWithWeightsAndAnnotationsDto,
    isArray: true,
  })
  @Auth()
  @Get('user')
  async getUserUploads(
    @Query() queryParams: GetImagesQueryParams,
    @User() user: AuthUser,
  ): Promise<ImageWithWeightsAndAnnotationsDto[]> {
    return this.imagesService.query(queryParams, user.id);
  }

  @ApiOperation({
    summary: 'Get date extent',
    description: 'Get minimum and maximum date of all images.',
    deprecated: true,
  })
  @ApiOkResponse({
    type: DateExtentDto,
  })
  @Get('dateExtent')
  async getDateExtent(
    @Query() queryParams: LocationQueryParams,
  ): Promise<DateExtentDto> {
    const results = await this.imagesService.getDateExtent(queryParams);

    // if there are no images with dates yet, return standard dates
    if (!results[0]) {
      return {
        from: '1800-01-01',
        to: '2020-01-01',
      };
    }

    return {
      from: results[0].d,
      to: results[1].d,
    };
  }

  @ApiOperation({
    summary: 'Get image',
    description: 'Get image by ID.',
  })
  @ApiOkResponse({
    type: ImageFullDto,
  })
  @ApiNotFoundResponse({
    description: 'Image with ID not found!',
  })
  @Get(':imageId')
  async getById(
    @Param('imageId') imageId: string,
    @Query() queryParams: GetImageQueryParams,
  ): Promise<ImageFullDto> {
    const image = await this.imagesService.getById(
      imageId,
      queryParams.regexp === '1',
    );

    if (!image) {
      throw new NotFoundException(`Image with ID ${imageId} not found!`);
    }

    return image;
  }

  @ApiOperation({
    summary: 'Upload image',
    description: `
Upload an image to position defined by \`latitude\` and \`longitude\`. If you choose a \`radius\` greater than \`0\`, the position is considered as unknown, but rests in an area inside this radius.

Depending on the user's role, the image will be instantly visible to all, or needs validation by a moderator.
`,
  })
  @ApiConsumes('multipart/form-data')
  @ApiCreatedResponse({
    type: ImageFullDto,
  })
  @ApiBadRequestResponse()
  @Auth('uploadImages')
  @Post()
  @UseInterceptors(FileInterceptor('uploadImage'))
  async upload(
    @UploadedFile() file: Express.Multer.File,
    @Body() body: UploadImageDto,
    @User() user: AuthUser,
  ): Promise<ImageFullDto> {
    // check required data
    if (!body.latitude || !body.longitude) {
      await fs.remove(file.path);
      throw new BadRequestException('No latitude/longitude provided!');
    }
    return this.imagesService.upload(file, body, user);
  }

  @ApiOperation({
    summary: 'Upload image (unvalidated)',
    description:
      'Upload an image to position defined by `latitude` and `longitude`. If you choose a `radius` greater than `0`, The position is considered as unknown, but rests in an area inside this radius.\n\nSame as _Upload image_, but sets `needsValidation: true` and connects to the user who uploads the image.',
    deprecated: true,
  })
  @ApiConsumes('multipart/form-data')
  @ApiCreatedResponse({
    type: ImageFullDto,
  })
  @ApiBadRequestResponse()
  @Auth()
  @Post('user')
  @UseInterceptors(FileInterceptor('uploadImage'))
  async uploadUnvalidated(
    @UploadedFile() file: Express.Multer.File,
    @Body() body: UploadImageDto,
    @User() user: AuthUser,
  ): Promise<ImageFullDto> {
    // check required data
    if (!body.latitude || !body.longitude) {
      await fs.remove(file.path);
      throw new BadRequestException('No latitude/longitude provided!');
    }
    return this.imagesService.upload(file, body, user);
  }

  @ApiOperation({
    summary: 'Update image',
    // language=Markdown
    description: `
Update properties of an image. Currently, only following properties can be updated: \`title\`, \`date\`, \`description\`, \`misc\`, \`author\`, \`owner\`, \`permalink\`, \`restrictedAccess\`, \`tags\`, \`camera\`, \`spatialStatus\`, \`vrcity_useAsTextureFrom\`, \`vrcity_useAsTextureTo\`, \`vrcity_projectionDistance\`.

Currently, \`date\` needs to be in the special format \`{ value: string }\`, where \`value\` can be:

| value | Description |
| --- | --- |
| \`YYYY\` | specific year, e.g., \`1905\` |
| \`YYYY.MM\` | specific month, e.g., \`1904.08\` |
| \`YYYY.MM.DD\` | specific day, e.g., \`1934.05.01\` |
| \`YYYY/YYYY\` | time-span of several years, e.g., \`1928/1945\` |
| \`around YYYY\`, \`um YYYY\` | approximate date (time-span) (&plusmn;5 years), e.g., \`um 1910\` |
| \`before YYYY\`, \`vor YYYY\` | approximate date (time-span) (-10 years), e.g., \`vor 1945\` |
| \`after YYYY\`, \`nach YYYY\` | approximate date (time-span) (+10 years), e.g., \`nach 1945\` |

\`spatialStatus\` can only be set to \`0\`, \`1\`, and \`2\` and if \`camera\` is not yet set.

\`camera\` can be either set to update only the location, or both location and orientation (i.e., exterior and interior camera parameters).
If setting only the location, \`latitude\`, \`longitude\`, and \`altitude\` are mandatory.
Set \`altitude\` to \`0\` if it's unknown.
\`radius\` is optionally preserving the current \`spatialStatus\` if left empty,
 otherwise \`spatialStatus\` is set to \`3\` (if \`radius\` equals \`0\`) or \`0\` (if \`radius\` is positive).
If updating full orientation, \`omega\`, \`phi\`, \`kappa\`, and \`ck\` are also mandatory.
This sets spatialStatus to \`4\`. \`radius\` will be ignored.
      `,
  })
  @ApiOkResponse({
    type: ImageFullDto,
  })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse()
  @Auth()
  @Patch(':imageId')
  async update(
    @Param('imageId') imageId: string,
    @Body() body: UpdateImageFullDto,
    @User() user: AuthUser,
  ): Promise<ImageFullDto> {
    const image = await this.imagesService.update(imageId, body);

    if (!image) {
      throw new NotFoundException(`Image with ID ${imageId} not found!`);
    }

    image.editedBy = await this.commonService.updateEditedBy(user, imageId);

    return image;
  }

  @ApiOperation({
    summary: 'Delete image by user',
    description: 'Delete image and all associated files and annotations.',
    deprecated: true,
  })
  @ApiNoContentResponse()
  @ApiForbiddenResponse()
  @ApiNotFoundResponse()
  @Auth()
  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete('/user/:imageId')
  async removeUserImage(
    @Param('imageId') imageId: string,
    @Res() res: Response,
  ): Promise<void> {
    const removed = await this.imagesService.remove(imageId);

    if (!removed) {
      throw new NotFoundException(`Image with ID ${imageId} not found!`);
    }

    res.send();
  }

  @ApiOperation({
    summary: 'Delete image',
    description: 'Delete image and all associated files and annotations.',
  })
  @ApiNoContentResponse()
  @ApiNotFoundResponse()
  @HttpCode(HttpStatus.NO_CONTENT)
  @Auth()
  @Delete(':imageId')
  async remove(
    @Param('imageId') imageId: string,
    @Res() res: Response,
  ): Promise<void> {
    const removed = await this.imagesService.remove(imageId);

    if (!removed) {
      throw new NotFoundException(`Image with ID ${imageId} not found!`);
    }

    res.send();
  }

  @ApiOperation({
    summary: 'Confirm/decline image',
    description: 'Set `pending` and/or `declined` flag.',
  })
  @ApiOkResponse({
    type: ImageFullDto,
  })
  @ApiNotFoundResponse()
  @Auth('validateContent')
  @Patch(':imageId/validation')
  async validateImage(
    @Param('imageId') imageId: string,
    @Body() body: UpdateValidationStatusDto,
  ): Promise<ImageFullDto> {
    const update = await this.commonService.updateValidationStatus(
      imageId,
      body.pending,
      body.declined,
    );

    if (!update) {
      throw new NotFoundException(`Image with ID ${imageId} not found!`);
    }

    return this.imagesService.getById(imageId);
  }
}
