import { forwardRef, Module } from '@nestjs/common';
import { AuthModule } from '@backend/auth/auth.module';
import { MicroservicesModule } from '@backend/microservices/microservices.module';
import { TerrainModule } from '@backend/routes/terrain/terrain.module';
import { ObjectsModule } from '@backend/routes/objects/objects.module';
import { ImagesModule } from '../images.module';
import { LinkObjectsController } from './link-objects.controller';
import { LinkObjectsService } from './link-objects.service';

@Module({
  imports: [
    AuthModule,
    MicroservicesModule,
    TerrainModule,
    ObjectsModule,
    forwardRef(() => ImagesModule),
  ],
  controllers: [LinkObjectsController],
  providers: [LinkObjectsService],
})
export class LinkObjectsModule {}
