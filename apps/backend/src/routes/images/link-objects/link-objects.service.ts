import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { formatISO } from 'date-fns';
import { join } from 'path';
import { Euler, Matrix4, Quaternion, Vector3 } from 'three';
import { Coordinates } from '@uh4d/utils';
import { ImagesService } from '@backend/routes/images/images.service';
import { TerrainService } from '@backend/routes/terrain/terrain.service';
import { ObjectsService } from '@backend/routes/objects/objects.service';
import { getMediumDate } from '@backend/utils/date-utils';
import { ProjectionClientService } from '@backend/microservices/projection-client/projection-client.service';
import {
  LinkedObjectWeights,
  LinkToObjectsDto,
} from './dto/link-to-objects.dto';

@Injectable()
export class LinkObjectsService {
  constructor(
    private readonly neo4j: Neo4jService,
    private readonly config: ConfigService,
    private readonly imageService: ImagesService,
    private readonly terrainService: TerrainService,
    private readonly objectsService: ObjectsService,
    private readonly projectionClient: ProjectionClientService,
  ) {}

  async linkToObjects(
    imageId: string,
    weights: Record<string, LinkedObjectWeights>,
  ): Promise<LinkToObjectsDto> {
    // prepare data
    const weightsList: { id: string; weightMap: LinkedObjectWeights }[] = [];
    for (const [key, value] of Object.entries(weights)) {
      if (
        typeof value.weight !== 'number' ||
        typeof value.coverageWeight !== 'number' ||
        typeof value.distanceWeight !== 'number'
      )
        throw new BadRequestException(
          `Object weight is not a number: "${key}": ${value}`,
        );
      if (value.weight > 0) {
        weightsList.push({
          id: key,
          weightMap: value,
        });
      }
    }

    // language=Cypher
    let q = `
      MATCH (image:E38:UH4D {id: $imageId})
      OPTIONAL MATCH (image)-[r:P138]->(:E22)
      DELETE r`;

    if (weightsList.length) {
      // language=Cypher
      q += `
        WITH image
        UNWIND $weights AS value
        MATCH (e22:E22:UH4D {id: value.id})
        MERGE (image)-[r:P138]->(e22)
        SET r = value.weightMap
        RETURN image.id AS imageId, collect(r{.*, id: e22.id}) AS weights
  `;
    } else {
      q += `
        RETURN image.id AS imageId, [] AS weights
    `;
    }

    const params = {
      imageId,
      weights: weightsList,
    };

    type QueryResult = {
      imageId: string;
      weights: ({ id: string } & LinkedObjectWeights)[];
    };
    const result = (await this.neo4j.write<QueryResult>(q, params))[0];

    if (!result) {
      throw new NotFoundException(`Image with ID ${imageId} not found!`);
    }

    // compose response body
    return {
      imageId: result.imageId,
      weights: result.weights.reduce(
        (map: LinkToObjectsDto['weights'], curr) => {
          map[curr.id] = {
            coverageWeight: curr.coverageWeight,
            distanceWeight: curr.distanceWeight,
            weight: curr.weight,
          };
          return map;
        },
        {},
      ),
    };
  }

  async computeWeights(imageId: string): Promise<LinkToObjectsDto> {
    // get image
    const image = await this.imageService.getById(imageId);
    if (!image) {
      throw new NotFoundException(`Image with ID ${imageId} not found!`);
    }
    if (image.spatialStatus < 4) {
      throw new BadRequestException(
        'Image has not enough information on spatialization data!',
      );
    }
    if (!image.date) {
      throw new BadRequestException('Image must have some date!');
    }

    // query terrain
    const terrains = await this.terrainService.query({
      lat: image.camera.latitude,
      lon: image.camera.longitude,
    });

    const mediumDate = getMediumDate(image.date);

    // query objects
    const objects = await this.objectsService.query({
      lat: image.camera.latitude,
      lon: image.camera.longitude,
      r: 1000,
      date: formatISO(mediumDate, { representation: 'date' }),
    });

    // prepare data for microservice
    const origin = new Coordinates(image.camera);
    const imageEuler = new Euler(
      image.camera.omega,
      image.camera.phi,
      image.camera.kappa,
      'YXZ',
    );

    const threadData = {
      image: {
        id: image.id,
        camera: image.camera,
        ck: image.camera.ck,
        width: image.file.width,
        height: image.file.height,
        matrix: new Matrix4()
          .compose(
            new Vector3(),
            new Quaternion().setFromEuler(imageEuler),
            new Vector3(1, 1, 1),
          )
          .toArray(),
      },
      terrain: terrains[0]
        ? {
            id: terrains[0].id,
            location: terrains[0].location,
            file: join(
              this.config.get('DIR_DATA'),
              terrains[0].file.path,
              terrains[0].file.file,
            ),
            matrix: new Matrix4()
              .compose(
                new Coordinates(terrains[0].location).toCartesian(origin),
                new Quaternion(),
                new Vector3(1, 1, 1),
              )
              .toArray(),
          }
        : null,
      objects: objects.map((obj) => {
        const pos = new Coordinates(obj.origin).toCartesian(origin);
        const euler = new Euler(
          obj.origin.omega,
          obj.origin.phi,
          obj.origin.kappa,
          'YXZ',
        );
        const scale = obj.origin.scale
          ? new Vector3().fromArray(obj.origin.scale)
          : new Vector3(1, 1, 1);
        return {
          id: obj.id,
          file: join(this.config.get('DIR_DATA'), obj.file.path, obj.file.file),
          matrix: new Matrix4()
            .compose(pos, new Quaternion().setFromEuler(euler), scale)
            .toArray(),
        };
      }),
    };

    // run microservice task
    const computedWeights = await this.projectionClient.computeObjectWeights(
      threadData,
    );

    return {
      imageId,
      weights: computedWeights,
    };
  }
}
