import { Test, TestingModule } from '@nestjs/testing';
import { LinkObjectsService } from './link-objects.service';

describe('LinkObjectsService', () => {
  let service: LinkObjectsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LinkObjectsService],
    }).compile();

    service = module.get<LinkObjectsService>(LinkObjectsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
