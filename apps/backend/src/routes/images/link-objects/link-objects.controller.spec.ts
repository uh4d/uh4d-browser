import { Test, TestingModule } from '@nestjs/testing';
import { LinkObjectsController } from './link-objects.controller';

describe('LinkObjectsController', () => {
  let controller: LinkObjectsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LinkObjectsController],
    }).compile();

    controller = module.get<LinkObjectsController>(LinkObjectsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
