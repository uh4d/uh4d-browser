import { ApiProperty, getSchemaPath } from '@nestjs/swagger';
import { IsNotEmpty, IsObject } from 'class-validator';
import { ObjectWeights } from '@uh4d/dto/interfaces/custom/linked-objects';

export class LinkedObjectWeights implements ObjectWeights {
  @ApiProperty({
    description: 'Coverage of object in relation to whole image',
    required: false,
  })
  coverageWeight: number;
  @ApiProperty({
    description: 'Distance between image and object as weight value',
    required: false,
  })
  distanceWeight: number;
  @ApiProperty({
    description: 'Combination of `coverageWeight` and `distance Weight`',
    required: false,
  })
  weight: number;
}

export class LinkToObjectsDto {
  @ApiProperty({
    description: 'Image ID',
    required: false,
  })
  imageId: string;
  @ApiProperty({
    description:
      'Hash map, where the keys are object ids and the values an object of weights',
    type: 'object',
    additionalProperties: {
      $ref: getSchemaPath(LinkedObjectWeights),
    },
    example: {
      e22_3EdeqabzzU_zwinger: {
        coverageWeight: 0.30397428460943543,
        distanceWeight: 0.38870174515743133,
        weight: 0.11815533491066928,
      },
    },
    required: false,
  })
  weights: Record<string, LinkedObjectWeights>;
}

export class CreateLinkToObjectsDto {
  @ApiProperty({
    description:
      'Hash map, where the keys are object ids and the values an object of weights',
    type: 'object',
    additionalProperties: {
      $ref: getSchemaPath(LinkedObjectWeights),
    },
    example: {
      e22_3EdeqabzzU_zwinger: {
        coverageWeight: 0.30397428460943543,
        distanceWeight: 0.38870174515743133,
        weight: 0.11815533491066928,
      },
    },
  })
  @IsNotEmpty()
  @IsObject()
  weights: { [objectId: string]: LinkedObjectWeights };
}
