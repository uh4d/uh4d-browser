import { Body, Controller, Param, Post } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiExtraModels,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { embedDocFile } from '@backend/utils/ref-file';
import { Auth } from '@backend/auth/auth.decorator';
import { LinkObjectsService } from './link-objects.service';
import {
  CreateLinkToObjectsDto,
  LinkedObjectWeights,
  LinkToObjectsDto,
} from './dto/link-to-objects.dto';

@ApiTags('Images')
@ApiExtraModels(LinkedObjectWeights)
@Controller('images/:imageId')
export class LinkObjectsController {
  constructor(private readonly linkObjectsService: LinkObjectsService) {}

  @ApiOperation({
    summary: 'Link to objects',
    description:
      'Link image to objects with weights. Existing links will be overwritten or deleted. If an empty `weights` object is supplied, all links will be deleted. Will internally be called by `Compute object weights`.',
  })
  @ApiCreatedResponse({
    type: LinkToObjectsDto,
  })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse()
  @Auth()
  @Post('link')
  async linkToObjects(
    @Param('imageId') imageId: string,
    @Body() body: CreateLinkToObjectsDto,
  ): Promise<LinkToObjectsDto> {
    return this.linkObjectsService.linkToObjects(imageId, body.weights);
  }

  @ApiOperation({
    summary: 'Compute object weights',
    description: embedDocFile('compute-object-weights.md'),
  })
  @ApiCreatedResponse({
    type: LinkToObjectsDto,
  })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse()
  @Auth()
  @Post('objectWeights')
  async computeWeights(
    @Param('imageId') imageId: string,
  ): Promise<LinkToObjectsDto> {
    const { weights } = await this.linkObjectsService.computeWeights(imageId);
    return this.linkObjectsService.linkToObjects(imageId, weights);
  }
}
