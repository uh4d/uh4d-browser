import { Test, TestingModule } from '@nestjs/testing';
import { ImageCaptionController } from './image-caption.controller';

describe('ImageCaptionController', () => {
  let controller: ImageCaptionController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ImageCaptionController],
    }).compile();

    controller = module.get<ImageCaptionController>(ImageCaptionController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
