import { ApiProperty } from '@nestjs/swagger';

export class ImageCaptionObject {
  @ApiProperty({
    description: 'Object ID',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Object name',
    required: false,
  })
  name: string;
  @ApiProperty({
    description: 'Coverage ration within the image',
    required: false,
  })
  coverageWeight: number;
}

export class ImageCaptionIdentifier {
  @ApiProperty({
    description: 'Identifier ID',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Identifier (without internal prefix)',
    required: false,
  })
  identifier: string;
  @ApiProperty({
    description: 'Concept label',
    required: false,
  })
  label: string;
  @ApiProperty({
    description: 'Number of occurrences (multiplied by their weights)',
    required: false,
  })
  count: number;
}

export class ImageCaptionDto {
  @ApiProperty({
    description: 'Possible caption composed from data below',
    required: false,
  })
  caption: string;
  @ApiProperty({
    description: 'Compass direction of the image',
    required: false,
    nullable: true,
  })
  direction: string | null;
  @ApiProperty({
    description: 'Opposite compass direction',
    required: false,
    nullable: true,
  })
  oppositeDirection: string | null;
  @ApiProperty({
    description: 'Objects visible in the image',
    required: false,
    type: ImageCaptionObject,
    isArray: true,
  })
  objects: ImageCaptionObject[];
  @ApiProperty({
    description: 'Getty AAT concepts linked to annotations of the image',
    required: false,
    type: ImageCaptionIdentifier,
    isArray: true,
  })
  aat: ImageCaptionIdentifier[];
  @ApiProperty({
    description: 'Wikidata concepts linked to annotations of the image',
    required: false,
    type: ImageCaptionIdentifier,
    isArray: true,
  })
  wikidata: ImageCaptionIdentifier[];
}
