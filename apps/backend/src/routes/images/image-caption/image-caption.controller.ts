import { Controller, Get, Param } from '@nestjs/common';
import {
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { ImageCaptionService } from './image-caption.service';
import { ImageCaptionDto } from './dto/image-caption.dto';

@ApiTags('Images')
@Controller('images/:imageId/caption')
export class ImageCaptionController {
  constructor(private imageCaptionService: ImageCaptionService) {}

  @ApiOperation({
    summary: 'Get image caption',
    description:
      'Generate a possible image caption. The caption is composed with respect to the objects linked to the image (see _Compute object weights_), the Wikidata concepts assigned to any annotations, and the direction of the camera perspective.',
  })
  @ApiOkResponse({
    type: ImageCaptionDto,
  })
  @ApiNotFoundResponse()
  @Get()
  getCaption(@Param('imageId') imageId: string): Promise<ImageCaptionDto> {
    return this.imageCaptionService.getImageCaption(imageId);
  }
}
