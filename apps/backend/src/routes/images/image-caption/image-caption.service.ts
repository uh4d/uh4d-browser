import { Injectable, NotFoundException } from '@nestjs/common';
import { Euler, MathUtils, Quaternion, Vector2, Vector3 } from 'three';
import * as pluralize from 'pluralize';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { ImagesService } from '@backend/routes/images/images.service';
import { ImageAnnotationsService } from '@backend/routes/images/image-annotations/image-annotations.service';
import { OrientationDto } from '@uh4d/dto/interfaces/custom/spatial';
import {
  ImageCaptionDto,
  ImageCaptionIdentifier,
  ImageCaptionObject,
} from './dto/image-caption.dto';

const COMPASS_DIRECTIONS = [
  'east',
  'southeast',
  'south',
  'southwest',
  'west',
  'northwest',
  'north',
  'northeast',
];

@Injectable()
export class ImageCaptionService {
  constructor(
    private readonly imagesService: ImagesService,
    private readonly imageAnnotationsService: ImageAnnotationsService,
    private readonly neo4j: Neo4jService,
  ) {}

  async getImageCaption(imageId: string): Promise<ImageCaptionDto> {
    const image = await this.imagesService.getById(imageId);
    if (!image) {
      throw new NotFoundException(`Image with ID ${imageId} not found!`);
    }

    // compute direction
    const directions =
      image.spatialStatus >= 4 ? this.getCompassDirection(image.camera) : null;

    // language=Cypher
    const q = `
      MATCH (img:E38:UH4D {id: $imageId})-[r:P138]->(e22:E22)-[:P1]->(name:E41)
      RETURN e22.id AS id, name.value AS name, r.coverageWeight AS coverageWeight
    `;

    const objects = await this.neo4j.read<ImageCaptionObject>(q, { imageId });
    objects.sort((a, b) => b.coverageWeight - a.coverageWeight);

    // query annotations
    const annotations =
      await this.imageAnnotationsService.queryAnnotationsByImage(imageId);

    const identifierMap = new Map<string, ImageCaptionIdentifier>();
    annotations.forEach((ann) => {
      [...ann.wikidata, ...ann.aat].forEach((value) => {
        const idObj = identifierMap.get(value.id);
        if (idObj) {
          idObj.count += value.weight;
        } else {
          identifierMap.set(value.id, {
            id: value.id,
            identifier: value.identifier,
            label: value.label,
            count: value.weight,
          });
        }
      });
    });
    const identifiers = [...identifierMap.values()].sort(
      (a, b) => b.count - a.count,
    );

    const data: Omit<ImageCaptionDto, 'caption'> = {
      direction: directions ? directions.direction : null,
      oppositeDirection: directions ? directions.oppositeDirection : null,
      objects,
      aat: identifiers.filter((value) => value.id.startsWith('aat:')),
      wikidata: identifiers.filter((value) => value.id.startsWith('wiki:')),
    };

    return {
      caption: this.composeCaption(data),
      ...data,
    };
  }

  private getCompassDirection(orientation: OrientationDto): {
    direction: string;
    oppositeDirection: string;
  } {
    const euler = new Euler(
      orientation.omega,
      orientation.phi,
      orientation.kappa,
      'YXZ',
    );
    const vec3 = new Vector3(0, 0, -1).applyQuaternion(
      new Quaternion().setFromEuler(euler),
    );
    const angle =
      new Vector2(vec3.x, vec3.z).normalize().angle() * MathUtils.RAD2DEG;

    const l = COMPASS_DIRECTIONS.length;
    const index = Math.floor(((angle + 360 / (2 * l)) / (360 / l)) % l);

    return {
      direction: COMPASS_DIRECTIONS[index],
      oppositeDirection: COMPASS_DIRECTIONS[(index + l / 2) % l],
    };
  }

  private composeCaption(data: Omit<ImageCaptionDto, 'caption'>): string {
    const caption: string[] = [];

    const mainObjects = data.objects.filter((obj) => obj.coverageWeight > 0.1);
    const backgroundObjects = data.objects.filter(
      (obj) => obj.coverageWeight < 0.1 && obj.coverageWeight > 0.02,
    );
    const annotations = data.wikidata.slice(0, 3);

    if (mainObjects.length) {
      caption.push('View of');
      mainObjects.forEach((obj, index, array) => {
        if (index === 0) {
          caption.push('the');
        } else if (index === array.length - 1) {
          caption.push('and the');
        } else {
          caption.push(', the');
        }
        caption.push(obj.name[0].toUpperCase() + obj.name.slice(1));
      });
      caption.push(`from the ${data.oppositeDirection}.`);
    }

    if (annotations.length) {
      annotations.forEach((ann, index, array) => {
        const label = ann.count > 1 ? pluralize(ann.label) : 'a ' + ann.label;
        if (index === 0) {
          caption.push(label[0].toUpperCase() + label.slice(1));
        } else if (index === array.length - 1) {
          caption.push('and', label);
        } else {
          caption.push(',', label);
        }
      });
      caption.push('are clearly visible.');
    }

    if (backgroundObjects.length) {
      backgroundObjects.forEach((obj, index, array) => {
        if (index === 0) {
          caption.push('The');
        } else if (index === array.length - 1) {
          caption.push('and the');
        } else {
          caption.push(', the');
        }
        caption.push(obj.name[0].toUpperCase() + obj.name.slice(1));
      });
      caption.push('can be seen in the background.');
    }

    return caption.join(' ').replace(/\s+(,)/g, '$1');
  }
}
