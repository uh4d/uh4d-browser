import { Test, TestingModule } from '@nestjs/testing';
import { ImageCaptionService } from './image-caption.service';

describe('ImageCaptionService', () => {
  let service: ImageCaptionService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ImageCaptionService],
    }).compile();

    service = module.get<ImageCaptionService>(ImageCaptionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
