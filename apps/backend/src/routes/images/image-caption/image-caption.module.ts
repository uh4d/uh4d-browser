import { forwardRef, Module } from '@nestjs/common';
import { ImagesModule } from '../images.module';
import { ImageAnnotationsModule } from '../image-annotations/image-annotations.module';
import { ImageCaptionController } from './image-caption.controller';
import { ImageCaptionService } from './image-caption.service';

@Module({
  imports: [forwardRef(() => ImagesModule), ImageAnnotationsModule],
  controllers: [ImageCaptionController],
  providers: [ImageCaptionService],
})
export class ImageCaptionModule {}
