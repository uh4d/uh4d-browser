import { ArrayMinSize, IsArray, IsDefined, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ProjectFromObjectsDto {
  @ApiProperty({
    description: 'List of object IDs the annotations should be projected from',
  })
  @IsDefined()
  @IsArray()
  @ArrayMinSize(1)
  @IsString({ each: true })
  objects: string[];
}
