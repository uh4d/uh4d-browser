import { ApiProperty } from '@nestjs/swagger';

export class UploadImageAnnotationsDto {
  @ApiProperty({
    description: 'JSON file in COCO dataset segmentation format',
    type: 'string',
    format: 'binary',
  })
  data: any;
  @ApiProperty({
    description: 'Space-separated text file with identifier mappings',
    type: 'string',
    format: 'binary',
    required: false,
  })
  mappings?: any;
}
