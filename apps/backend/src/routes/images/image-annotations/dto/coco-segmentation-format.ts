import {
  ArrayMaxSize,
  ArrayMinSize,
  IsArray,
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

export class CocoInfo {
  year: number;
  version: string;
  description: string;
  contributor: string;
  url: string;
  date_created: string;
}

export class CocoImage {
  @IsNotEmpty()
  @IsInt()
  id: number;
  @IsNotEmpty()
  @IsInt()
  width: number;
  @IsNotEmpty()
  @IsInt()
  height: number;
  @IsNotEmpty()
  @IsString()
  file_name: string;
}

export class CocoCategory {
  @IsNotEmpty()
  @IsInt()
  id: number;
  @IsNotEmpty()
  @IsString()
  name: string;
}

export class CocoAnnotation {
  @IsNotEmpty()
  @IsInt()
  id: number;
  @IsNotEmpty()
  @IsInt()
  image_id: number;
  @IsNotEmpty()
  @IsInt()
  category_id: number;
  @IsNotEmpty()
  @IsArray()
  segmentation: number[][];
  @IsNotEmpty()
  @IsArray()
  @ArrayMinSize(4)
  @ArrayMaxSize(4)
  @IsNumber({}, { each: true })
  bbox: [number, number, number, number];
  ignore: 0 | 1;
  iscrowd: 0 | 1;
  @IsNotEmpty()
  @IsNumber()
  area: number;
}

/**
 * COCO segmentation format
 * https://cocodataset.org/#format-data
 */
export class CocoSegmentationFormat {
  info: CocoInfo;
  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CocoImage)
  images: CocoImage[];
  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CocoCategory)
  categories: CocoCategory[];
  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CocoAnnotation)
  annotations: CocoAnnotation[];
}
