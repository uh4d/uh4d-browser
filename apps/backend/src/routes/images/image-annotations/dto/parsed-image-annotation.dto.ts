import { ApiProperty } from '@nestjs/swagger';
import { ImageAnnotationDto } from '@backend/generated/dto';

export class ParsedImageAnnotationDto extends ImageAnnotationDto {
  @ApiProperty({
    description: 'List of polygons, each being a list of points `[x, y]`.',
    type: 'array',
    items: {
      type: 'array',
      items: {
        type: 'array',
        items: {
          type: 'number',
          format: 'float',
        },
        minItems: 2,
        maxItems: 2,
      },
    },
  })
  polylist: number[][][];
}
