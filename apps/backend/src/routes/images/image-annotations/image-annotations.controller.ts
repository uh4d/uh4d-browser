import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Param,
  Post,
  UploadedFiles,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiConsumes,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiProduces,
  ApiTags,
} from '@nestjs/swagger';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { readFile, readJson, remove } from 'fs-extra';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { ImageAnnotationsService } from './image-annotations.service';
import { UploadImageAnnotationsDto } from './dto/upload-image-annotations.dto';
import { ParsedImageAnnotationDto } from './dto/parsed-image-annotation.dto';
import { CocoSegmentationFormat } from './dto/coco-segmentation-format';
import { ProjectFromObjectsDto } from './dto/project-from-objects.dto';

@ApiTags('Annotations')
@Controller('images')
export class ImageAnnotationsController {
  constructor(
    private readonly imageAnnotationService: ImageAnnotationsService,
  ) {}

  @ApiOperation({
    summary: 'Get annotations of image',
    description:
      'Get all annotation of an image. All values in `polylist`, `bbox`, and `area` are normalized to an image width and height of 1.',
  })
  @ApiOkResponse({
    type: ParsedImageAnnotationDto,
    isArray: true,
  })
  @Get(':imageId/annotations')
  queryAnnotationsByImage(@Param('imageId') imageId: string) {
    return this.imageAnnotationService.queryAnnotationsByImage(imageId);
  }

  @ApiOperation({
    summary: 'Post image annotation data',
    description: `
Post annotation data for several images.
The data must be a JSON file in the [COCO dataset format](https://cocodataset.org/#format-data) for segmentations.
For each image, the segmentations will be processed and stored as nodes in the database.
Only those images will be processed, where the identifiers could be matched.
Already stored annotations will be overridden by the new annotations.

If identifiers could not be matched, you can provide a text file with mappings,
including the file name in LabelStudio and the UH4D image ID separated by whitespace.

\`\`\`text
images/74bae8a3-df_hauptkatalog_0054809.jpg SkFodcbMz_df_hauptkatalog_0054809.jpg
images/fbf0e5b7-download.jpg r1ojdcbGz_df_hauptkatalog_0054810.jpg
\`\`\`
    `,
  })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    type: UploadImageAnnotationsDto,
  })
  @ApiProduces('text/plain')
  @ApiCreatedResponse({
    type: String,
    isArray: true,
  })
  @ApiBadRequestResponse()
  @Post('annotations')
  @UseInterceptors(
    FileFieldsInterceptor([
      { name: 'data', maxCount: 1 },
      { name: 'mappings', maxCount: 1 },
    ]),
  )
  async postAnnotations(
    @UploadedFiles()
    files?: {
      data?: Express.Multer.File[];
      mappings?: Express.Multer.File[];
    },
  ) {
    // read segmentation data file
    const dataFile = files?.data?.[0]?.path;
    if (!dataFile) throw new BadRequestException('data file not provided');
    const jsonData = await readJson(dataFile);

    // validate data file
    const data = plainToInstance(CocoSegmentationFormat, jsonData);
    const validationErrors = await validate(data);
    if (validationErrors.length) {
      throw new BadRequestException('JSON segmentation file has wrong format.');
    }

    // read mappings file
    const mappings = new Map<string, string>();
    const mappingFile = files?.mappings?.[0]?.path;
    if (mappingFile) {
      const mappingData = await readFile(mappingFile, { encoding: 'utf8' });
      mappingData
        .trim()
        .split(/\r?\n/)
        .forEach((line) => {
          // consider only first two in line
          const list = line.trim().split(/\s+/);
          mappings.set(list[0], list[1]);
        });
    }

    try {
      return await this.imageAnnotationService.processAnnotationData(data, mappings);
    } finally {
      if (dataFile) await remove(dataFile);
      if (mappingFile) await remove(mappingFile);
    }
  }

  @ApiOperation({
    summary: 'Compute weights (image)',
    description:
      'Compute spatial weights between annotations of a single image and semantic weights between each annotation and identifier nodes.\n\nThese tasks will be called automatically when uploading/processing annotation data of an image.',
  })
  @ApiCreatedResponse()
  @Post(':imageId/annotations/weights')
  computeWeights(@Param('imageId') imageId: string) {
    return this.imageAnnotationService.triggerUpdateChain(imageId, false);
  }

  @ApiOperation({
    summary: 'Compute weights (all images)',
    description:
      'For all images with annotations, compute spatial weights between annotations of each single image and semantic weights between each annotation and identifier nodes.',
  })
  @ApiCreatedResponse()
  @Post('annotations/weights')
  async computeWeightsAll() {
    const images =
      await this.imageAnnotationService.getAllImagesWithAnnotations();

    await this.imageAnnotationService.triggerUpdateChain(
      images.map((img) => img.id),
      false,
    );

    return images;
  }

  @ApiOperation({
    summary: 'Project from objects',
    description:
      'Project annotations from objects onto image and store as image annotations.',
  })
  @ApiBody({
    type: ProjectFromObjectsDto,
  })
  @ApiCreatedResponse()
  @ApiBadRequestResponse()
  @ApiNotFoundResponse()
  @Post(':imageId/annotations/fromObjects')
  projectFromObjects(
    @Param('imageId') imageId: string,
    @Body() body: ProjectFromObjectsDto,
  ) {
    return this.imageAnnotationService.projectFromObjects(
      imageId,
      body.objects,
    );
  }
}
