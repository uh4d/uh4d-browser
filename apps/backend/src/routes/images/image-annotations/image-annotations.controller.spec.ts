import { Test, TestingModule } from '@nestjs/testing';
import { ImageAnnotationsController } from './image-annotations.controller';

describe('ImageAnnotationsController', () => {
  let controller: ImageAnnotationsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ImageAnnotationsController],
    }).compile();

    controller = module.get<ImageAnnotationsController>(ImageAnnotationsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
