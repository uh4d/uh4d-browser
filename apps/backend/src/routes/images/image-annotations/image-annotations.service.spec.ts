import { Test, TestingModule } from '@nestjs/testing';
import { ImageAnnotationsService } from './image-annotations.service';

describe('ImageAnnotationsService', () => {
  let service: ImageAnnotationsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ImageAnnotationsService],
    }).compile();

    service = module.get<ImageAnnotationsService>(ImageAnnotationsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
