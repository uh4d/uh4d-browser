import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { nanoid } from 'nanoid';
import { join } from 'path';
import { Euler, Matrix4, Quaternion, Vector2, Vector3 } from 'three';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { Coordinates } from '@uh4d/utils';
import { CreateImageAnnotationDto, ImageFileDto } from '@backend/generated/dto';
import { isPointInsidePolygon, Line2D } from '@backend/utils/annotation-utils';
import { NormDataService } from '@backend/routes/normdata/norm-data.service';
import { AnnotationsService } from '@backend/routes/annotations/annotations.service';
import { ImagesService } from '@backend/routes/images/images.service';
import { TerrainService } from '@backend/routes/terrain/terrain.service';
import { ObjectsService } from '@backend/routes/objects/objects.service';
import { ProjectionClientService } from '@backend/microservices/projection-client/projection-client.service';
import { ParsedImageAnnotationDto } from './dto/parsed-image-annotation.dto';
import {
  CocoAnnotation,
  CocoCategory,
  CocoImage,
  CocoSegmentationFormat,
} from './dto/coco-segmentation-format';

type LabelMap = Map<
  number,
  {
    name: string;
    wikidata: string | null;
    aat: string | null;
    full: string;
  }
>;

interface CreateImageAnnotationBase
  extends Pick<CreateImageAnnotationDto, 'polylist' | 'bbox' | 'area'> {
  id: string;
  aat: string | null;
  wikidata: string | null;
}

@Injectable()
export class ImageAnnotationsService {
  private readonly logger = new Logger(ImageAnnotationsService.name);

  constructor(
    private readonly config: ConfigService,
    private readonly neo4j: Neo4jService,
    private readonly normDataService: NormDataService,
    private readonly annotationsService: AnnotationsService,
    private readonly imagesService: ImagesService,
    private readonly terrainService: TerrainService,
    private readonly objectsService: ObjectsService,
    private readonly projectionClient: ProjectionClientService,
  ) {}

  /**
   * Get all annotations of an image, including Wikidata and AAT identifiers as well as neighbors.
   */
  async queryAnnotationsByImage(
    imageId: string,
  ): Promise<ParsedImageAnnotationDto[]> {
    // language=Cypher
    const q = `
      MATCH (image:E38:UH4D {id: $id})-[:P106]->(ann:Annotation)
      OPTIONAL MATCH (ann)-[aatRel:P2]->(aat:AAT)
        WHERE aatRel.confidence IS NOT NULL
      WITH ann, collect(aat{.id, .identifier, .label, weight: aatRel.weight}) AS aat
      OPTIONAL MATCH (ann)-[wikiRel:P2]->(wd:Wikidata)
        WHERE wikiRel.confidence IS NOT NULL
      WITH ann, aat, collect(wd{.id, .identifier, .label, weight: wikiRel.weight}) AS wikidata
      OPTIONAL MATCH (ann)-[nRel:is_close_to]-(neighbor:Annotation)
      RETURN ann.id AS id,
             ann.polylist AS polylist,
             ann.bbox AS bbox,
             ann.area AS area,
             aat,
             wikidata,
             collect(neighbor{.id, distance: nRel.distance, weight: nRel.weight}) AS neighbors
    `;
    const params = {
      id: imageId,
    };

    const results = await this.neo4j.read(q, params);
    return results.map((annotation) => ({
      ...annotation,
      polylist: JSON.parse(annotation.polylist),
    }));
  }

  /**
   * Query all images that have annotations.
   */
  getAllImagesWithAnnotations(): Promise<{ id: string }[]> {
    // language=Cypher
    const q = `
      MATCH (image:E38:UH4D)
      WHERE (image)-[:P106]->(:Annotation)
      RETURN image.id AS id
    `;

    return this.neo4j.read<{ id: string }>(q);
  }

  /**
   * Get file reference information of image.
   */
  private async getImageFileData(imageId: string): Promise<ImageFileDto> {
    // language=Cypher
    const q = `
      MATCH (image:E38:UH4D {id: $id})<-[:P67]-(file:D9)
      RETURN image.id AS id, file
    `;

    const results = await this.neo4j.read<{ id: string; file: ImageFileDto }>(
      q,
      { id: imageId },
    );

    if (results[0]) {
      return results[0].file;
    } else {
      throw new NotFoundException(`Image with ID ${imageId} not found!`);
    }
  }

  /**
   * Process COCO image segmentation JSON and store annotations (for multiple images).
   */
  async processAnnotationData(
    data: CocoSegmentationFormat,
    mappings = new Map<string, string>(),
  ): Promise<string[]> {
    // parse categories
    const labels = this.prepareAnnotationLabels(data.categories);

    const imageIds: string[] = [];
    const response: string[] = [];

    for (const image of data.images) {
      try {
        // find internal image id
        // strip first 16 characters from label studio id
        const searchId =
          mappings.get(image.file_name) || image.file_name.slice(16);
        const uh4dId = await this.findImage(searchId);
        if (!uh4dId) {
          response.push(`${image.file_name} not found`);
          continue;
        }

        // get only annotations for current image
        const annotations = data.annotations.filter(
          (a) => a.image_id === image.id,
        );

        // process and save annotations for image
        await this.saveCocoAnnotationDataForImage(
          uh4dId,
          image,
          annotations,
          labels,
        );

        imageIds.push(uh4dId);
        response.push(`${image.file_name} -> ${uh4dId} successful`);
      } catch (e) {
        response.push(`${image.file_name} failed`, e.toString());
      }
    }

    // update norm data and annotation weights asynchronously
    this.triggerUpdateChain(imageIds)
      .then(() => {
        this.logger.log('Norm data and image annotation update complete.');
      })
      .catch((reason) => {
        this.logger.error(reason);
      });

    return response;
  }

  /**
   * Extract Wikidata and AAT ID from COCO category string.
   */
  private prepareAnnotationLabels(categories: CocoCategory[]): LabelMap {
    const labels: LabelMap = new Map();

    categories.forEach((c) => {
      const [name, wd, aat] = c.name.split('_');
      labels.set(c.id, {
        name,
        wikidata: wd ? wd.split(':')[1] : null,
        aat: aat ? aat.split(':')[1] : null,
        full: c.name,
      });
    });

    return labels;
  }

  /**
   * Look if image exists via fuzzy ID search and return full UH4D ID.
   */
  private async findImage(searchId: string): Promise<string | undefined> {
    // language=Cypher
    const q = `
      MATCH (image:E38:UH4D)
      WHERE image.id =~ ".*" + $id + ".*"
      RETURN image;
    `;

    const params = {
      id: searchId,
    };

    const results = await this.neo4j.read(q, params);
    return results[0]?.image.id;
  }

  /**
   * Save COCO annotation data for a single image.
   */
  private async saveCocoAnnotationDataForImage(
    uh4dId: string,
    image: CocoImage,
    annotations: CocoAnnotation[],
    labels: LabelMap,
  ): Promise<void> {
    // prepare annotations
    const preparedAnnotations = annotations.map((a) => {
      // normalize values to 1
      const polylist: [number, number][][] = [];
      for (let i = 0; i < a.segmentation.length; i++) {
        const innerList: [number, number][] = [];
        for (let j = 0; j < a.segmentation[i].length; j += 2) {
          innerList.push([
            a.segmentation[i][j] / image.width,
            a.segmentation[i][j + 1] / image.height,
          ]);
        }
        polylist.push(innerList);
      }

      const bbox = [
        a.bbox[0] / image.width,
        a.bbox[1] / image.height,
        a.bbox[2] / image.width,
        a.bbox[3] / image.height,
      ];

      const label = labels.get(a.category_id);

      return {
        id: 'annotation_image_' + nanoid(9),
        polylist,
        bbox,
        area: a.area / (image.width * image.height),
        aat: label.aat,
        wikidata: label.wikidata,
      };
    });

    return this.saveAnnotationDataForImage(uh4dId, preparedAnnotations);
  }

  /**
   * Save annotation data for a single image.
   */
  private async saveAnnotationDataForImage(
    imageId: string,
    annotations: CreateImageAnnotationBase[],
  ): Promise<void> {
    // language=Cypher
    const q = `
      MATCH (image:E38:UH4D {id: $imageId})
      CALL {
        WITH image
        OPTIONAL MATCH (image)-[:P106]->(oldAnn:Annotation)
        DETACH DELETE oldAnn
      }

      UNWIND $annotations AS a
      MERGE (ann:E36:UH4D:Annotation {id: a.id, polylist: a.polylist, bbox: a.bbox, area: a.area})
      MERGE (image)-[:P106]->(ann)

      WITH image, ann, a
      CALL apoc.do.when(
        a.aat IS NOT NULL,
        'MERGE (aatType:E55:UH4D:AAT {id: "aat:" + aatId, identifier: aatId}) MERGE (ann)-[rel:P2]->(aatType) SET rel.confidence = 1 RETURN aatType',
        '',
        { ann: ann, aatId: a.aat }
      ) YIELD value

      WITH image, ann, a
      CALL apoc.do.when(
        a.wikidata IS NOT NULL,
        'MERGE (wikiType:E55:UH4D:Wikidata {id: "wiki:" + wikiId, identifier: wikiId}) MERGE (ann)-[rel:P2]->(wikiType) SET rel.confidence = 1 RETURN wikiType',
        '',
        { ann: ann, wikiId: a.wikidata }
      ) YIELD value

      RETURN DISTINCT image
    `;

    const params = {
      imageId,
      annotations: annotations.map((value) => ({
        ...value,
        polylist: JSON.stringify(value.polylist),
      })),
    };

    const results = await this.neo4j.write(q, params);
    if (!results[0]?.image) {
      throw new Error('Write query did not return any results');
    }
  }

  /**
   * Compute distance between annotations of an image and save weighted relationships.
   */
  async computeSpatialNeighborWeights(imageId: string) {
    const imageFileData = await this.getImageFileData(imageId);
    const annotations = await this.queryAnnotationsByImage(imageId);
    if (annotations.length === 0) return;

    // prepare annotation polylist
    const aspectRatio = imageFileData.width / imageFileData.height;
    const list = annotations.map((ann) => {
      const points = ann.polylist[0].map(
        (point) => new Vector2(point[0] * aspectRatio, point[1]),
      );
      const lines: Line2D[] = [];

      for (let i = 0, l = points.length; i < l; i++) {
        lines.push(new Line2D(points[i], points[(i + 1) % l]));
      }

      return {
        id: ann.id,
        area: ann.area,
        points,
        lines,
      };
    });

    const pairs: { sourceId: string; targetId: string; distance: number }[] =
      [];

    for (let i = 0, l = list.length; i < l; i++) {
      const annCurr = list[i];
      for (let j = i + 1; j < l; j++) {
        const annTest = list[j];

        // check if polys intersect
        const intersects = annCurr.lines.some((cLine) =>
          annTest.lines.some((tLine) => cLine.intersectsLine(tLine)),
        );
        if (intersects) {
          pairs.push({
            sourceId: annCurr.id,
            targetId: annTest.id,
            distance: 0,
          });
          continue;
        }

        // check if poly is inside the other
        const inside =
          annCurr.points.every((p) =>
            isPointInsidePolygon(p, annTest.points),
          ) ||
          annTest.points.every((p) => isPointInsidePolygon(p, annCurr.points));
        if (inside) {
          pairs.push({
            sourceId: annCurr.id,
            targetId: annTest.id,
            distance: 0,
          });
          continue;
        }

        // compute distance between every line and every point
        let minDistance = Number.MAX_VALUE;
        annCurr.lines.forEach((line) => {
          annTest.points.forEach((point) => {
            const d = line.closestDistanceToPoint(point);
            minDistance = Math.min(d, minDistance);
          });
        });
        annTest.lines.forEach((line) => {
          annCurr.points.forEach((point) => {
            const d = line.closestDistanceToPoint(point);
            minDistance = Math.min(d, minDistance);
          });
        });

        pairs.push({
          sourceId: annCurr.id,
          targetId: annTest.id,
          distance: minDistance,
        });
      }
    }

    // save weighted relationships
    const weightedRelations = pairs.map((p) => ({
      ...p,
      weight: 1 - p.distance,
    }));

    // language=Cypher
    const q = `
      MATCH (image:E38:UH4D {id: $imageId})
      CALL {
        WITH image
        MATCH (image)-[:P106]->(ann:Annotation)
        OPTIONAL MATCH (ann)-[oldRel:is_close_to]-(:Annotation)
        DELETE oldRel
      }

      UNWIND $weights AS w
      MATCH (image)-[:P106]->(source:Annotation:UH4D {id: w.sourceId}),
            (image)-[:P106]->(target:Annotation:UH4D {id: w.targetId})
      MERGE (source)-[rel:is_close_to]->(target)
      SET rel.distance = w.distance, rel.weight = w.weight

      RETURN source, target, rel.weight AS weight
    `;

    const params = {
      imageId,
      weights: weightedRelations,
    };

    const results = await this.neo4j.write(q, params);
    return results.map((r) => ({
      sourceId: r.source.id,
      targetId: r.target.id,
      weight: r.weight,
    }));
  }

  /**
   * Update identifier nodes and compute weighted relationships
   * between spatial and semantic neighbors.
   */
  async triggerUpdateChain(
    imageIds: string | string[],
    updateNormData = true,
  ): Promise<void> {
    if (updateNormData) {
      await this.normDataService.updateAll();
    }

    const ids = Array.isArray(imageIds) ? imageIds : [imageIds];
    for (const id of ids) {
      this.logger.debug(`Compute weights for image ${id}`);

      await this.computeSpatialNeighborWeights(id);

      const annotations = await this.queryAnnotationsByImage(id);
      for (const ann of annotations) {
        await this.annotationsService.computeSemanticNeighborWeights(ann.id);
      }
    }

    await this.annotationsService.computeEmbeddings();
  }

  async projectFromObjects(imageId: string, objectIds: string[]) {
    // get image
    const image = await this.imagesService.getById(imageId);
    if (!image) {
      throw new NotFoundException(`Image with ID ${imageId} not found!`);
    }

    // query terrain
    const terrains = await this.terrainService.query({
      lat: image.camera.latitude,
      lon: image.camera.longitude,
    });

    // query objects
    const objects = await Promise.all(
      objectIds.map((objId) => this.objectsService.getById(objId)),
    );

    // prepare data for microservice
    const origin = new Coordinates(image.camera);
    const imageEuler = new Euler(
      image.camera.omega,
      image.camera.phi,
      image.camera.kappa,
      'YXZ',
    );

    const threadData = {
      image: {
        id: image.id,
        camera: image.camera,
        ck: image.camera.ck,
        width: image.file.width,
        height: image.file.height,
        matrix: new Matrix4()
          .compose(
            new Vector3(),
            new Quaternion().setFromEuler(imageEuler),
            new Vector3(1, 1, 1),
          )
          .toArray(),
      },
      terrain: terrains[0]
        ? {
            id: terrains[0].id,
            location: terrains[0].location,
            file: join(
              this.config.get('DIR_DATA'),
              terrains[0].file.path,
              terrains[0].file.file,
            ),
            matrix: new Matrix4()
              .compose(
                new Coordinates(terrains[0].location).toCartesian(origin),
                new Quaternion(),
                new Vector3(1, 1, 1),
              )
              .toArray(),
          }
        : null,
      objects: objects.map((obj) => {
        const pos = new Coordinates(obj.origin).toCartesian(origin);
        const euler = new Euler(
          obj.origin.omega,
          obj.origin.phi,
          obj.origin.kappa,
          'YXZ',
        );
        const scale = obj.origin.scale
          ? new Vector3().fromArray(obj.origin.scale)
          : new Vector3(1, 1, 1);
        return {
          id: obj.id,
          file: join(this.config.get('DIR_DATA'), obj.file.path, obj.file.file),
          matrix: new Matrix4()
            .compose(pos, new Quaternion().setFromEuler(euler), scale)
            .toArray(),
        };
      }),
    };

    // run microservice task
    const annotations =
      await this.projectionClient.projectAnnotationsFromObject(threadData);

    // prepare annotations
    const preparedAnnotations = annotations.flatMap<CreateImageAnnotationBase>(
      (a) => {
        const { wikidata, aat } = this.normDataService.parseIdentifier(a.id);
        if (!(wikidata || aat)) return [];

        // normalize values to 1
        const polylist = a.polylist.map((p) =>
          p.map((vec) => [
            vec[0] / image.file.width,
            vec[1] / image.file.height,
          ]),
        );

        const bbox = [
          a.bbox[0] / image.file.width,
          a.bbox[1] / image.file.height,
          a.bbox[2] / image.file.width,
          a.bbox[3] / image.file.height,
        ];

        return [
          {
            id: 'annotation_image_' + nanoid(9),
            polylist,
            bbox,
            area: a.area / (image.file.width * image.file.height),
            aat: aat,
            wikidata: wikidata,
          },
        ];
      },
    );

    await this.saveAnnotationDataForImage(imageId, preparedAnnotations);

    // update norm data and annotation weights asynchronously
    this.triggerUpdateChain(imageId)
      .then(() => {
        this.logger.log('Norm data and image annotation update complete.');
      })
      .catch((reason) => {
        this.logger.error(reason);
      });
  }
}
