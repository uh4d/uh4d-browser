import { forwardRef, Module } from '@nestjs/common';
import { MicroservicesModule } from '@backend/microservices/microservices.module';
import { AnnotationsModule } from '@backend/routes/annotations/annotations.module';
import { NormDataModule } from '@backend/routes/normdata/norm-data.module';
import { TerrainModule } from '@backend/routes/terrain/terrain.module';
import { ObjectsModule } from '@backend/routes/objects/objects.module';
import { ImagesModule } from '@backend/routes/images/images.module';
import { ImageAnnotationsController } from './image-annotations.controller';
import { ImageAnnotationsService } from './image-annotations.service';

@Module({
  imports: [
    MicroservicesModule,
    AnnotationsModule,
    NormDataModule,
    TerrainModule,
    ObjectsModule,
    forwardRef(() => ImagesModule),
  ],
  controllers: [ImageAnnotationsController],
  providers: [ImageAnnotationsService],
  exports: [ImageAnnotationsService],
})
export class ImageAnnotationsModule {}
