import { IntersectionType } from '@nestjs/swagger';
import { CreateImageDto, ImageDto, UpdateImageDto } from '@backend/dto/image';
import {
  CreateImageMetaDto,
  ImageMetaDto,
  UpdateImageMetaDto,
} from '@backend/dto/image-meta';

export class ImageFullDto extends IntersectionType(ImageDto, ImageMetaDto) {}

export class CreateImageFullDto extends IntersectionType(
  CreateImageDto,
  CreateImageMetaDto,
) {}

export class UpdateImageFullDto extends IntersectionType(
  UpdateImageDto,
  UpdateImageMetaDto,
) {}
