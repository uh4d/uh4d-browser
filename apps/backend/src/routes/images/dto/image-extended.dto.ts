import { ImageDto } from '@backend/dto';
import { ApiProperty } from '@nestjs/swagger';
import { ImageExtendedDto } from '@uh4d/dto/interfaces/custom/image';

export class ImageWithWeightsAndAnnotationsDto
  extends ImageDto
  implements ImageExtendedDto
{
  @ApiProperty({
    description:
      'Map with object weights where the property name is the object ID.',
    type: 'object',
    additionalProperties: {
      description: 'Weight between the image and the object',
      type: 'number',
      format: 'float',
    },
    required: false,
    nullable: true,
  })
  objects: { [objId: string]: number } | null;

  @ApiProperty({
    description:
      'List of annotations for each of the query annotation, where the property name is the ID of the query annotation.',
    type: 'object',
    additionalProperties: {
      description: 'List of annotations with similarity score',
      type: 'array',
      items: {
        type: 'object',
        properties: {
          id: {
            description: 'ID of the annotation',
            type: 'string',
          },
          similarity: {
            description: 'Similarity score',
            type: 'number',
            format: 'float',
          },
        },
      },
    },
    required: false,
    nullable: true,
  })
  annotations: {
    [key: string]: {
      id: string;
      similarity: number;
    }[];
  } | null;

  @ApiProperty({
    description:
      'If the query is also based on some similarity measures (e.g., querying annotations), the computed ranking will be stored in this value (ranges from 0 to 1).',
    type: 'number',
    format: 'float',
    required: false,
    nullable: true,
  })
  relevance: number | null;
}
