import { IsIn, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class GetImageQueryParams {
  @ApiProperty({
    description:
      'If `regexp=1`, the first image will be returned where its ID (partially) contains the provided ID parameter.',
    required: false,
  })
  @IsOptional()
  @IsIn(['0', '1'])
  regexp?: string;
}
