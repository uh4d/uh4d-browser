import { ApiProperty } from '@nestjs/swagger';
import {
  IsLatitude,
  IsLongitude,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  Min,
} from 'class-validator';
import { Type } from 'class-transformer';

export class UploadImageDto {
  @ApiProperty({
    type: 'string',
    format: 'binary',
  })
  uploadImage: any;

  @ApiProperty({
    type: 'number',
    format: 'float',
  })
  @IsNotEmpty()
  @IsNumber()
  @IsLatitude()
  @Type(() => Number)
  latitude: number;

  @ApiProperty({
    type: 'number',
    format: 'float',
  })
  @IsNotEmpty()
  @IsNumber()
  @IsLongitude()
  @Type(() => Number)
  longitude: number;

  @ApiProperty({
    description:
      'Radius in which to spatially search for the image. It defaults to `0`, which assumes the position to be known. If radius is greater than `0`, the position is not known exactly, but can be somewhere in the area defined by latitude, longitude, and radius.',
    required: false,
    type: 'number',
    format: 'float',
    default: 0,
  })
  @IsOptional()
  @IsNumber()
  @Min(0)
  @Type(() => Number)
  radius?: number;

  @ApiProperty({
    description: 'Image title',
    required: false,
  })
  @IsOptional()
  @IsString()
  title?: string;

  @ApiProperty({
    description: 'Image description',
    required: false,
  })
  @IsOptional()
  @IsString()
  description?: string;

  @ApiProperty({
    description: 'Date that will get parsed',
    required: false,
  })
  @IsOptional()
  @IsString()
  date?: string;

  @ApiProperty({
    description: 'Name of the photographer',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  author?: string;
}
