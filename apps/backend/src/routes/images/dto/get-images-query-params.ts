import { IsDateString, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { ToBoolean } from '@uh4d/backend/utils';
import { LocationQueryParams } from '@backend/routes/dto/location-query-params';

export class GetImagesQueryParams extends LocationQueryParams {
  @ApiProperty({
    description:
      'Query string to filter results (looks for title, author, owner, tags). The query string can be prefixed to search more precisely. Currently supported prefixes are: `obj:`, `author:`, `owner:`. A simple dash `-` will exclude query from results. Special queries are `spatial:set` and `spatial:unset` to look for images that are already spatialized or not yet spatialized, respectively. The keyword `unvalidated` sets the `unvalidated` flag to `true`.',
    required: false,
  })
  @IsOptional()
  q?: string;

  @ApiProperty({
    description: 'Timespan minimum value',
    required: false,
  })
  @IsOptional()
  @IsDateString()
  from?: string;

  @ApiProperty({
    description: 'Timespan maximum value',
    required: false,
  })
  @IsOptional()
  @IsDateString()
  to?: string;

  @ApiProperty({
    description:
      "If `from` and `to` are set, `undated=1` will include also images that don't have a date yet.",
    required: false,
  })
  @IsOptional()
  @ToBoolean()
  undated?: boolean;

  @ApiProperty({
    description:
      'Include images for which timespan has been set in which the image can be used as texture and if this timespan overlaps with the `from`/`to` timespan. If `true`, it invalidates `undated` flag.',
    required: false,
  })
  @IsOptional()
  @ToBoolean()
  useAsTexture?: boolean;

  @ApiProperty({
    description: 'Filter by specific scene',
    required: false,
  })
  @IsOptional()
  scene?: string;

  @ApiProperty({
    description: 'Also retrieve object weights if `weights=1`',
    required: false,
  })
  @IsOptional()
  @ToBoolean()
  weights?: boolean;
}
