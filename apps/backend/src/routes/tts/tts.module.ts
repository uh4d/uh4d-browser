import { Module } from '@nestjs/common';
import { MicroservicesModule } from '@backend/microservices/microservices.module';
import { TtsController } from './tts.controller';
import { TtsService } from './tts.service';

@Module({
  imports: [MicroservicesModule],
  controllers: [TtsController],
  providers: [TtsService],
  exports: [TtsService],
})
export class TtsModule {}
