import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class TtsRequestDto {
  @ApiProperty({
    description: 'Input text (with a minimum length of 10 words)',
    type: String,
  })
  @IsNotEmpty()
  @IsString()
  text: string;
}
