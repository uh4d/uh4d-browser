import { ApiProperty } from '@nestjs/swagger';

export class TtsResponseDto {
  @ApiProperty({
    description: 'Path to audio file',
    required: false,
    type: String,
  })
  path: string;
}
