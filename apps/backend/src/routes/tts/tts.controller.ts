import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiOkResponse,
  ApiOperation,
} from '@nestjs/swagger';
import { TtsService } from './tts.service';
import { TtsRequestDto } from './dto/tts-request.dto';
import { TtsResponseDto } from './dto/tts-response.dto';

@Controller('tts')
export class TtsController {
  constructor(private readonly ttsService: TtsService) {}

  @ApiOperation({
    summary: 'Generate TTS',
    description:
      'Generate text-to-speech (TTS) audio file from text input. If the text has already been processed (audio file is already stored in the system), then return this one. Returns path to audio file.',
  })
  @ApiOkResponse({
    type: TtsResponseDto,
  })
  @ApiBadRequestResponse()
  @HttpCode(HttpStatus.OK)
  @Post()
  async generateAudioFromText(
    @Body() body: TtsRequestDto,
  ): Promise<TtsResponseDto> {
    return {
      path: await this.ttsService.generateAudioFromText(body.text),
    };
  }
}
