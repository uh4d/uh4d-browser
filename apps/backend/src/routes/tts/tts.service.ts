import { BadRequestException, Injectable } from '@nestjs/common';
import * as md5 from 'md5';
import { ensureDir, pathExists } from 'fs-extra';
import { join } from 'node:path';
import { TtsClientService } from '@backend/microservices/tts-client/tts-client.service';
import { BackendConfig } from '@backend/config';

@Injectable()
export class TtsService {
  constructor(
    private readonly ttsClient: TtsClientService,
    private readonly config: BackendConfig,
  ) {}

  /**
   * Generate text-to-speech (TTS) audio file from text input.
   * @param text Text input
   * @param force Don't use cached file, but regenerate new one
   * @return Path to audio file
   */
  async generateAudioFromText(text: string, force = false): Promise<string> {
    const hash = md5(text);

    const audioFilename = hash + '.mp3';
    const audioDir = join(this.config.dirData, 'audio');
    const absPath = join(audioDir, audioFilename);
    const relPath = join('audio', audioFilename);

    // return existing file
    if (!force && (await pathExists(absPath))) return relPath;

    // input text should be at least 10 words long (for proper language detection)
    if (text.split(/\s+/).length < 10)
      throw new BadRequestException('Text needs to be at least 10 words long');

    // generate file via TTS
    await ensureDir(audioDir);
    await this.ttsClient.sendTtsTask(text, absPath);

    return relPath;
  }
}
