import { Test, TestingModule } from '@nestjs/testing';
import { LegalBodiesService } from './legal-bodies.service';

describe('LegalBodiesService', () => {
  let service: LegalBodiesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LegalBodiesService],
    }).compile();

    service = module.get<LegalBodiesService>(LegalBodiesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
