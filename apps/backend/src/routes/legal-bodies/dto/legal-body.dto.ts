import { ApiProperty } from '@nestjs/swagger';
import { LegalBodyDto as LegalBodyInterface } from '@uh4d/dto/interfaces/custom/legal-body';

export class LegalBodyDto implements LegalBodyInterface {
  @ApiProperty({
    description: 'Legal body ID',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Legal body name',
    required: false,
  })
  value: string;
}
