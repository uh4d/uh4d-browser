import {
  Controller,
  DefaultValuePipe,
  Get,
  NotFoundException,
  Param,
  Query,
} from '@nestjs/common';
import {
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { LegalBodiesService } from './legal-bodies.service';
import { LegalBodyDto } from './dto/legal-body.dto';

@ApiTags('Metadata')
@Controller('legalbodies')
export class LegalBodiesController {
  constructor(private readonly legalBodyService: LegalBodiesService) {}

  @ApiOperation({
    summary: 'Query legal bodies',
    description:
      'Query all legal bodies. Use `search` query parameter to filter results.',
  })
  @ApiOkResponse({
    type: LegalBodyDto,
    isArray: true,
  })
  @Get()
  async query(
    @Query('search', new DefaultValuePipe('')) search: string,
  ): Promise<LegalBodyDto[]> {
    return this.legalBodyService.query(search);
  }

  @ApiOperation({
    summary: 'Get legal body',
    description: 'Get legal body by ID',
  })
  @ApiOkResponse({
    type: LegalBodyDto,
  })
  @ApiNotFoundResponse()
  @Get(':legalBodyId')
  async getById(
    @Param('legalBodyId') legalBodyId: string,
  ): Promise<LegalBodyDto> {
    const person = await this.legalBodyService.getById(legalBodyId);

    if (!person) {
      throw new NotFoundException(
        `Legal body with ID ${legalBodyId} not found!`,
      );
    }

    return person;
  }
}
