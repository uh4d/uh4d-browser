import { Controller, Get, Query, Res } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { Response } from 'express';
import {
  ApiBadRequestResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';

@ApiTags('Misc')
@Controller('proxy')
export class ProxyController {
  constructor(private readonly http: HttpService) {}

  @ApiOperation({
    summary: 'Proxy',
    description:
      "Proxy a request to resources and APIs that don't allow CORS (Cross-Origin Resource Sharing). The `Access-Control-Allow-Origin` header will be set to *.\n\nThe `url` query parameter should be encoded:\n\n```js\nconst response = await fetch('https:4dbrowser.urbanhistory4d.org/api/proxy?url=' + encodeURIComponent(url));\n```",
  })
  @ApiOkResponse()
  @ApiBadRequestResponse()
  @Get()
  async proxyRequest(
    @Query('url') url: string,
    @Res() res: Response,
  ): Promise<void> {
    const response = await this.http.axiosRef.get(url, {
      responseType: 'stream',
    });
    res.setHeader('access-control-allow-origin', '*');
    response.data.pipe(res);
  }
}
