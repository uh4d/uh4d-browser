import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { ProxyController } from './proxy.controller';
import { RedirectController } from './redirect.controller';
import { BatchController } from './batch.controller';

@Module({
  imports: [HttpModule],
  controllers: [ProxyController, RedirectController, BatchController],
})
export class MiscModule {}
