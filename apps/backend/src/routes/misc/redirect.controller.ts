import { Controller, Get, Query, Res } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { Response } from 'express';
import { URL } from 'url';

@ApiTags('Misc')
@Controller('redirect')
export class RedirectController {
  constructor(
    private readonly http: HttpService,
    private readonly config: ConfigService,
  ) {}

  @ApiOperation({
    summary: 'Redirect',
    description:
      "Redirect a non-secure URL to a secure context. All non-secure links to resoures within the requested URL will be replaced by this `redirect` link. This is particularly useful with `<iframe>` with non-secure resource embedded in a secure context.\n\nThe `url` query parameter should be encoded:\n\n```js\n'https:4dbrowser.urbanhistory4d.org/api/redirect?url=' + encodeURIComponent(url)\n```",
  })
  @ApiOkResponse()
  @ApiBadRequestResponse()
  @Get()
  async redirect(
    @Query('url') rawUrl: string,
    @Res() res: Response,
  ): Promise<void> {
    // remove last slash, if any
    const trimmedHost = this.config.get<string>('HOST').replace(/\/$/, '');
    const escapedHost = trimmedHost.replace(/\//g, '\\/');

    // query parameters are automatically decoded
    const url = new URL(rawUrl);

    // only replace contents if requested file is a text file, otherwise pass forward
    if (!/\/$|\/[^./]+$|\.(?:js|css|html|txt)$/.test(url.pathname)) {
      // stream file https://stackoverflow.com/questions/26982996/trying-to-proxy-an-image-in-node-js-express
      const streamResponse = await this.http.axiosRef.get(url.href, {
        responseType: 'stream',
      });
      streamResponse.data.pipe(res);
      return;
    }

    // fetch file
    const response = await this.http.axiosRef.get(rawUrl, {
      responseType: 'text',
    });

    // always replace &amp; and encode uri

    // replace all unsecure http links with redirect url
    let replaced = response.data.replace(
      /(http:\/\/[^"'\s]+)/g,
      (match, p1) =>
        trimmedHost +
        '/api/redirect?url=' +
        encodeURIComponent(p1.replace(/&amp;/g, '&')),
    );

    // unsecure links that are escaped
    replaced = replaced.replace(
      /(?<!\/\^?)(http:\\\/\\\/(?:(?!"|'|\s|\\").)+)/g,
      (match, p1) =>
        escapedHost +
        '\\/api\\/redirect?url=' +
        encodeURIComponent(p1.replace(/&amp;/g, '&')),
    );

    // prefix relative link
    replaced = replaced.replace(
      /((?:href|src)=["'])(?!(?:https?:\/\/|\/\/)|javascript:)\/?([^"']*)/g,
      (match, p1, p2) =>
        p1 +
        trimmedHost +
        '/api/redirect?url=' +
        encodeURIComponent(url.origin + '/' + p2.replace(/&amp;/g, '&')),
    );

    // absolute links without protocol
    replaced = replaced.replace(
      /((?:href|src)=["'])(\/\/[^"']*)/g,
      (match, p1, p2) =>
        p1 +
        trimmedHost +
        '/api/redirect?url=' +
        encodeURIComponent(url.protocol + p2.replace(/&amp;/g, '&')),
    );

    // if css file, replace relative url links
    if (/\/[^/]+\.css(?:\?|$)/.test(url.href)) {
      replaced = replaced.replace(
        /(url\('?"?)(?!#)([^:'")]+)('?"?\))/g,
        (match, p1, p2, p3) =>
          p1 +
          trimmedHost +
          '/api/redirect?url=' +
          encodeURIComponent(
            url.href.replace(/\/[^/]+\.css.*$/, '/') +
              p2.replace(/&amp;/g, '&'),
          ) +
          p3,
      );
    }

    res.setHeader('content-type', response.headers['content-type']);
    res.send(replaced);
  }
}
