import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
} from '@nestjs/common';
import { ApiOkResponse, ApiOperation } from '@nestjs/swagger';
import { Auth } from '@backend/auth/auth.decorator';
import { AuthUser, User } from '@backend/auth/user';
import { UsersService } from './users.service';
import { BasicUserDto } from './dto/basic-user.dto';
import { UpdateUserRoleDto } from './dto/update-user-role.dto';
import { UpdateUserGeofenceDto } from './dto/update-user-geofence.dto';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @ApiOperation({
    summary: 'Get user data',
    description: 'Get user profile data of current (logged-in) user.',
  })
  @ApiOkResponse({
    type: BasicUserDto,
  })
  @Auth()
  @Get('profile')
  async getById(@User() authUser: AuthUser): Promise<BasicUserDto> {
    const user = await this.usersService.getById(authUser.id);

    // do not return authSecret and refreshToken
    return {
      id: user.id,
      name: user.name,
      email: user.email,
      phone: user.phone,
      role: user.role || null,
      geofences: user.geofences,
      lastLogin: user.lastLogin,
    };
  }

  @ApiOperation({
    summary: 'Query all users',
  })
  @ApiOkResponse({
    type: BasicUserDto,
    isArray: true,
  })
  @Auth('manageAccounts')
  @Get()
  async query(): Promise<BasicUserDto[]> {
    const users = await this.usersService.query();

    return users.map((user) => ({
      id: user.id,
      name: user.name,
      email: user.email,
      phone: user.phone,
      role: user.role || null,
      geofences: user.geofences,
      lastLogin: user.lastLogin,
    }));
  }

  @ApiOperation({
    summary: 'Set user role',
  })
  @ApiOkResponse()
  @HttpCode(HttpStatus.NO_CONTENT)
  @Auth('manageAccounts')
  @Patch(':userId/role')
  async setRole(
    @Param('userId') userId: string,
    @Body() body: UpdateUserRoleDto,
  ): Promise<void> {
    await this.usersService.setRole(userId, body.role || 'user');
  }

  @ApiOperation({
    summary: 'Update user geofence',
  })
  @ApiOkResponse()
  @HttpCode(HttpStatus.NO_CONTENT)
  @Auth('manageAccounts')
  @Patch(':userId/geofence')
  async setGeofence(
    @Param('userId') userId: string,
    @Body() body: UpdateUserGeofenceDto,
  ): Promise<void> {
    await this.usersService.setGeofence(userId, body.geofences);
  }
}
