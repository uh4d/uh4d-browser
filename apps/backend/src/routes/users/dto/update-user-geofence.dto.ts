import { ApiProperty } from '@nestjs/swagger';
import { ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { CreateGeofenceDto } from '@backend/dto';

export class UpdateUserGeofenceDto {
  @ApiProperty({
    type: () => CreateGeofenceDto,
    isArray: true,
  })
  @ValidateNested()
  @Type(() => CreateGeofenceDto)
  geofences: CreateGeofenceDto[];
}
