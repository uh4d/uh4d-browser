import { ApiProperty } from '@nestjs/swagger';
import { ValidateIf, IsIn } from 'class-validator';
import { RoleType } from '@uh4d/config';

export class UpdateUserRoleDto {
  @ApiProperty({
    description: 'User role',
    enum: ['admin', 'moderator'],
  })
  @ValidateIf((object, value) => value !== null)
  @IsIn(['admin', 'moderator', 'creator'])
  role: RoleType | null;
}
