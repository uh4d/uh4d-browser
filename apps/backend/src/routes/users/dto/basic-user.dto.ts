import { PickType } from '@nestjs/swagger';
import { UserDto } from '@backend/dto';

export class BasicUserDto extends PickType(UserDto, [
  'id',
  'name',
  'email',
  'phone',
  'role',
  'geofences',
  'lastLogin',
] as const) {}
