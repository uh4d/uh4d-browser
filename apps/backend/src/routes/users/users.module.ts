import { forwardRef, Module } from '@nestjs/common';
import { AuthModule } from '@backend/auth/auth.module';
import { UserContentModule } from './user-content/user-content.module';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';

@Module({
  imports: [forwardRef(() => AuthModule), UserContentModule],
  providers: [UsersService],
  exports: [UsersService],
  controllers: [UsersController],
})
export class UsersModule {}
