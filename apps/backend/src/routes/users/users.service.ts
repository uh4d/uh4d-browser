import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { nanoid } from 'nanoid';
import { Secret } from 'otpauth';
import { parsePhoneNumber } from 'libphonenumber-js';
import {
  CreateUserDto,
  GeofenceDto,
  UpdateUserDto,
  UserDto,
} from '@backend/dto';
import { RoleType } from '@uh4d/config';
import { UserRoleCache } from '@backend/auth/permission/user-role.cache';

@Injectable()
export class UsersService {
  constructor(
    private readonly neo4j: Neo4jService,
    private readonly userRoleCache: UserRoleCache,
  ) {}

  async query(): Promise<UserDto[]> {
    // language=Cypher
    const q = `
      MATCH (user:User:UH4D)-[:P131]->(name:E82),
            (user)-[:P76]->(contact:E41)
      OPTIONAL MATCH (user)-[:has_geofence]->(fence:E94)
      RETURN user{.*, name: name.value, email: contact.email, phone: contact.phone, geofences: collect(fence) }
    `;

    const results = await this.neo4j.read<{ user: UserDto }>(q);

    return results.map((value) => ({ ...value.user }));
  }

  /**
   * Get user by ID.
   */
  async getById(userId: string): Promise<UserDto | undefined> {
    // language=Cypher
    const q = `
      MATCH (user:User:UH4D { id: $userId })-[:P131]->(name:E82),
            (user)-[:P76]->(contact:E41)
      OPTIONAL MATCH (user)-[:has_geofence]->(fence:E94)
      RETURN user{.*, name: name.value, email: contact.email, phone: contact.phone, geofences: collect(fence) }
    `;

    const results = await this.neo4j.read<{ user: UserDto }>(q, { userId });

    return results[0]?.user;
  }

  /**
   * Find user by name or email.
   */
  async findByNameOrEmail(nameOrEmail: string): Promise<UserDto | undefined> {
    // language=Cypher
    const q = `
      MATCH (user:User:UH4D)-[:P131]->(name:E82),
            (user)-[:P76]->(contact:E41)
      WHERE name.value = $value OR contact.email = $value
      OPTIONAL MATCH (user)-[:has_geofence]->(fence:E94)
      RETURN user{.*, name: name.value, email: contact.email, phone: contact.phone, geofences: collect(fence) }
    `;

    const results = await this.neo4j.read<{ user: UserDto }>(q, {
      value: nameOrEmail,
    });

    return results[0]?.user;
  }

  /**
   * Create user by matching the email address.
   * Password and phone number may be updated if user registration is still pending
   * (i.e., user didn't finish registration).
   */
  async create(user: CreateUserDto): Promise<UserDto> {
    // recheck if user has already been created and registration has not yet been completed (pending)
    // language=Cypher
    const qCheckPending = `
      MATCH (contact:E41:UH4D { email: $email })<-[:P76]-(user:User:E21:UH4D)
      RETURN user
    `;

    const resultCheck = await this.neo4j.read<{ user: UserDto }>(
      qCheckPending,
      { email: user.email },
    );
    if (resultCheck[0] && resultCheck[0].user.pending !== true) {
      // throw normal error (check for pending state should have been done before)
      throw new Error('User already exists and completed registration');
    }

    // validate phone number
    const phoneNumber = parsePhoneNumber(user.phone, 'DE');
    if (!phoneNumber.isValid()) {
      throw new BadRequestException('Invalid phone number');
    }

    // language=Cypher
    const q = `
      MERGE (contact:E41:UH4D { email: $contact.email })<-[:P76]-(user:User:E21:UH4D)-[:P131]->(name:E82:UH4D)
      ON CREATE SET user = $user, contact.id = $contact.id, name.id = $name.id
      SET user.password = $user.password,
          name.value = $name.value,
          contact.phone = $contact.phone
      RETURN user{.*, name: name.value, email: contact.email, phone: contact.phone, geofences: [] }
    `;

    const id = nanoid(9);

    const params = {
      user: {
        id: 'user_' + id,
        password: user.password,
        authSecret: new Secret({ size: 20 }).base32,
        pending: true,
      },
      name: {
        id: 'user_name_' + id,
        value: user.name,
      },
      contact: {
        id: 'user_contact_' + id,
        email: user.email,
        phone: phoneNumber.number,
      },
    };

    const results = await this.neo4j.write<{ user: UserDto }>(q, params);

    if (!results[0]?.user)
      throw new InternalServerErrorException('No user created');

    return results[0].user;
  }

  /**
   * Complete registration by removing `pending` flag.
   */
  async completeRegistration(userId: string) {
    // language=Cypher
    const q = `
      MATCH (user:User:UH4D { id: $userId })
      REMOVE user.pending
    `;

    await this.neo4j.write(q, { userId });
  }

  /**
   * Update refresh token. Each time a new token is set,
   * `lastLogin` will be updated to current datetime.
   */
  async updateRefreshToken(
    userId: string,
    refreshToken: string | null,
  ): Promise<void> {
    // language=Cypher
    let q = `
      MATCH (user:User:UH4D { id: $userId })
      SET user.refreshToken = $refreshToken
    `;
    if (refreshToken) q += ', user.lastLogin = datetime()';

    await this.neo4j.write(q, { userId, refreshToken });
  }

  async updatePassword(userId: string, password: string) {
    // language=Cypher
    const q = `
      MATCH (user:User:UH4D { id: $userId })
      SET user.password = $password
    `;

    await this.neo4j.write(q, { userId, password });
  }

  async setRole(userId: string, role: RoleType): Promise<void> {
    // language=Cypher
    const q = `
      MATCH (user:User:UH4D { id: $userId })
      SET user.role = $role
    `;

    // ToDo: update geofence

    await this.neo4j.write(q, {
      userId,
      role: ['admin', 'moderator', 'creator'].includes(role) ? role : null,
    });

    await this.userRoleCache.invalidateCachedRole(userId);
  }

  async setGeofence(userId: string, geofences: GeofenceDto[]): Promise<void> {
    // language=Cypher
    const q = `
      MATCH (user:User:UH4D { id: $userId })
      OPTIONAL MATCH (user)-[:has_geofence]->(fence:E94)
      DETACH DELETE fence
      WITH user
      UNWIND $geofences AS fence
      CREATE (user)-[:has_geofence]->(f:UH4D:E94)
      SET f = fence
    `;

    await this.neo4j.write(q, { userId, geofences });

    await this.userRoleCache.invalidateCachedRole(userId);
  }

  async update(userId: string, data: UpdateUserDto): Promise<void> {
    // language=Cypher
    let q = `
      MATCH (user:User:UH4D { id: $userId })-[:P131]->(name:E82),
            (user)-[:P76]->(contact:E41) `;
    if (data.name) q += 'SET name.value = $user.name ';
    if (data.email) q += 'SET contact.email = $user.email ';
    if (data.phone) q += 'SET contact.phone = $user.phone ';

    await this.neo4j.write(q, { userId, user: data });
  }
}
