import { forwardRef, Module } from '@nestjs/common';
import { AuthModule } from '@backend/auth/auth.module';
import { ImagesModule } from '@backend/routes/images/images.module';
import { TextsModule } from '@backend/routes/texts/texts.module';
import { ObjectsModule } from '@backend/routes/objects/objects.module';
import { PoisModule } from '@backend/routes/pois/pois.module';
import { UserContentController } from './user-content.controller';

@Module({
  imports: [
    forwardRef(() => AuthModule),
    forwardRef(() => ImagesModule),
    forwardRef(() => TextsModule),
    forwardRef(() => ObjectsModule),
    forwardRef(() => PoisModule),
  ],
  controllers: [UserContentController],
})
export class UserContentModule {}
