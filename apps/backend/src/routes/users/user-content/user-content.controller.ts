import { Controller, Get, Param, Query } from '@nestjs/common';
import { ApiOkResponse, ApiOperation } from '@nestjs/swagger';
import { Auth } from '@backend/auth/auth.decorator';
import { ImageDto, ObjectDto, TextDto, Uh4dPoiDto } from '@backend/dto';
import { ImagesService } from '@backend/routes/images/images.service';
import { TextsService } from '@backend/routes/texts/texts.service';
import { ObjectsService } from '@backend/routes/objects/objects.service';
import { PoisService } from '@backend/routes/pois/pois.service';
import { GetImagesQueryParams } from '@backend/routes/images/dto/get-images-query-params';
import { GetTextsQueryParams } from '@backend/routes/texts/dto/get-texts-query-params';
import { GetObjectsQueryParams } from '@backend/routes/objects/dto/get-objects-query-params';
import { GetPoisQueryParams } from '@backend/routes/pois/dto/get-pois-query-params';

@Controller('users/:userId')
export class UserContentController {
  constructor(
    private readonly imagesService: ImagesService,
    private readonly textsService: TextsService,
    private readonly objectsService: ObjectsService,
    private readonly poisService: PoisService,
  ) {}

  @ApiOperation({
    summary: 'Get user images',
    description: 'Get images uploaded by the user.',
  })
  @ApiOkResponse({
    type: ImageDto,
    isArray: true,
  })
  @Auth()
  @Get('images')
  async getUserImages(
    @Param('userId') userId: string,
    @Query() queryParams: GetImagesQueryParams,
  ): Promise<ImageDto[]> {
    return this.imagesService.query(queryParams, userId);
  }

  @ApiOperation({
    summary: 'Get user texts',
    description: 'Get texts uploaded by the user.',
  })
  @ApiOkResponse({
    type: TextDto,
    isArray: true,
  })
  @Auth()
  @Get('texts')
  async getUserTexts(
    @Param('userId') userId: string,
    @Query() queryParams: GetTextsQueryParams,
  ): Promise<TextDto[]> {
    return this.textsService.query(queryParams, userId);
  }

  @ApiOperation({
    summary: 'Get user objects',
    description: 'Get objects uploaded by the user.',
  })
  @ApiOkResponse({
    type: ObjectDto,
    isArray: true,
  })
  @Auth()
  @Get('objects')
  async getUserObjects(
    @Param('userId') userId: string,
    @Query() queryParams: GetObjectsQueryParams,
  ): Promise<ObjectDto[]> {
    return this.objectsService.query(queryParams, userId);
  }

  @ApiOperation({
    summary: 'Get user POIs',
    description: 'Get points of interests created by the user.',
  })
  @ApiOkResponse({
    type: Uh4dPoiDto,
    isArray: true,
  })
  @Auth()
  @Get('pois')
  async getUserPois(
    @Param('userId') userId: string,
    @Query() queryParams: GetPoisQueryParams,
  ): Promise<Uh4dPoiDto[]> {
    return this.poisService.query(queryParams, userId);
  }
}
