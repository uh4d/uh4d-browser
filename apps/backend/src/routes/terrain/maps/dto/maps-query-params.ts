import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsOptional } from 'class-validator';

export class MapsQueryParams {
  @ApiProperty({
    description: 'Filter by date',
    format: 'YYYY',
    required: false,
  })
  @IsOptional()
  @IsDateString()
  date?: string;
}
