import { Module } from '@nestjs/common';
import { TerrainController } from './terrain.controller';
import { TerrainService } from './terrain.service';
import { MapsModule } from './maps/maps.module';

@Module({
  controllers: [TerrainController],
  providers: [TerrainService],
  imports: [MapsModule],
  exports: [TerrainService],
})
export class TerrainModule {}
