import { ApiProperty } from '@nestjs/swagger';
import { IsLatitude, IsLongitude, IsNumber, IsOptional } from 'class-validator';
import { Type } from 'class-transformer';

export class TerrainQueryParams {
  @ApiProperty({
    description: 'Latitude in decimal degree',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  @IsLatitude()
  @Type(() => Number)
  lat?: number;

  @ApiProperty({
    description: 'Longitude in decimal degree',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  @IsLongitude()
  @Type(() => Number)
  lon?: number;
}
