import { Test, TestingModule } from '@nestjs/testing';
import { TerrainController } from './terrain.controller';

describe('TerrainController', () => {
  let controller: TerrainController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TerrainController],
    }).compile();

    controller = module.get<TerrainController>(TerrainController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
