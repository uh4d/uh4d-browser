import { Body, Controller, Post } from '@nestjs/common';
import { ApiCreatedResponse, ApiOperation } from '@nestjs/swagger';
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { CreateBugDto } from './dto/create-bug.dto';

@Controller('bugs')
export class BugsController {
  constructor(
    private readonly http: HttpService,
    private readonly config: ConfigService,
  ) {}

  @ApiOperation({
    summary: 'Post bug',
    description: 'Open new issue in GitLab repo utilizing GitLab API.',
  })
  @ApiCreatedResponse()
  @Post()
  async createBug(@Body() body: CreateBugDto) {
    const gitlab =
      this.parseGitLabConfig(body.context) ||
      this.parseGitLabConfig('uh4d-browser');
    const url = `${gitlab.apiEndpoint}/projects/${gitlab.projectId}/issues`;

    const response = await this.http.axiosRef.post(
      url,
      {
        title: body.title,
        description: `_Scene:_ ${body.scene || 'any'}\n\n${body.description}`,
      },
      {
        headers: {
          'PRIVATE-TOKEN': gitlab.privateToken,
        },
      },
    );

    return response.data;
  }

  private parseGitLabConfig(
    context: string,
  ):
    | { apiEndpoint: string; projectId: string; privateToken: string }
    | undefined {
    const configValue = this.config.get(
      'GITLAB_' + context.toUpperCase().replace(/-/g, '_'),
    );
    if (!configValue) return undefined;

    const matches = /(\d+):(.+)@(.+)/.exec(configValue);
    if (matches.length < 4) return undefined;

    return {
      apiEndpoint: matches[3],
      projectId: matches[1],
      privateToken: matches[2],
    };
  }
}
