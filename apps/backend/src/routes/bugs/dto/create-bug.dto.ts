import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class CreateBugDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  context: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  title: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  description: string;
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  scene?: string;
}
