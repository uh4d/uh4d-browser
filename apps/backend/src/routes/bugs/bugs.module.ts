import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { BugsController } from './bugs.controller';

@Module({
  imports: [HttpModule],
  controllers: [BugsController],
})
export class BugsModule {}
