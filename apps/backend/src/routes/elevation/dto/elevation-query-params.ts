import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';
import { LocationQueryParams } from '@backend/routes/dto/location-query-params';

export class ElevationQueryParams extends LocationQueryParams {
  @ApiProperty({
    description: 'DEM dataset name',
    required: false,
    default: 'FABDEM',
  })
  @IsOptional()
  dataSet?: string;

  @ApiProperty({
    description: 'Add textures',
    required: false,
    default: 'true',
  })
  @IsOptional()
  textured?: string;

  @ApiProperty({
    description: 'Imagery provider to use for the texture',
    required: false,
    default: 'ThunderForest-Neighbourhood',
  })
  @IsOptional()
  imageryProvider?: string;

  @ApiProperty({
    description: 'Texture quality',
    required: false,
    default: '2',
  })
  @IsOptional()
  textureQuality?: string;
}
