import { Injectable, NestMiddleware } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NextFunction, Request, Response } from 'express';
import { join } from 'node:path';
import { pathExists } from 'fs-extra';

@Injectable()
export class ElevationFileCacheMiddleware implements NestMiddleware {
  constructor(private readonly config: ConfigService) {}

  async use(req: Request, res: Response, next: NextFunction) {
    // check for cached data file
    const filePath = join(
      this.config.get<string>('DIR_CACHE'),
      'elevation',
      req.params.folder,
      req.params.file,
    );

    const exists = await pathExists(filePath);

    if (exists) {
      // send file
      res.sendFile(filePath);
    } else {
      // no file, pass request forward
      next();
    }
  }
}
