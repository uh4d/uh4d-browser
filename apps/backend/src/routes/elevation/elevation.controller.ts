import { Controller, Get, Logger, Param, Query, Res } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import {
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiProduces,
  ApiTags,
} from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';
import { Response } from 'express';
import { createWriteStream, ensureDir, writeJson } from 'fs-extra';
import { join } from 'node:path';
import { Coordinates } from '@uh4d/utils';
import { embedDocFile } from '@backend/utils/ref-file';
import { ElevationQueryParams } from './dto/elevation-query-params';
import { ElevationMetaDto } from './dto/elevation-meta.dto';
import { roundDecimalDegree } from '@backend/utils/round-decimal-degree';

@ApiTags('Terrain')
@Controller('elevation')
export class ElevationController {
  private readonly logger = new Logger(ElevationController.name);

  constructor(
    private readonly http: HttpService,
    private readonly config: ConfigService,
  ) {}

  @ApiOperation({
    summary: 'Request terrain model',
    description:
      'Request a digital terrain model in bounding box as glTF packed binary (GLB) from Elevation API. The response is not the glTF file itself, but asset information including the path to the file.\n\nQuery parameters `lat`, `lon`, `r` are required as search parameter. Optional query parameters control the quality of the model. More detailed information about the parameters can be found at https://api.elevationapi.com/',
  })
  @ApiOkResponse({
    description: 'Requested information about terrain model',
    type: ElevationMetaDto,
  })
  @Get()
  async getMeta(
    @Query() queryParams: ElevationQueryParams,
    @Res() res: Response,
  ): Promise<void> {
    // parse and round position
    const radius = Math.round(queryParams.r);
    const latitude = roundDecimalDegree(queryParams.lat, radius / 10);
    const longitude = roundDecimalDegree(queryParams.lon, radius / 10);

    // parse optional query parameters
    const dataSet = queryParams.dataSet || 'FABDEM'; // 'SRTM_GL1';
    const textured = queryParams.textured || 'true';
    const imageryProvider =
      queryParams.imageryProvider || 'ThunderForest-Neighbourhood';
    const textureQuality = queryParams.textureQuality || '2';
    const meshReduceFactor = '0.5';

    const { west, east, south, north } = new Coordinates(
      latitude,
      longitude,
    ).computeBoundingBox(radius);

    const url = `https://api.elevationapi.com/api/model/3d/bbox/${west},${east},${south},${north}?dataset=${dataSet}&textured=${textured}&imageryProvider=${imageryProvider}&textureQuality=${textureQuality}&centerOnOrigin=false&meshReduceFactor=${meshReduceFactor}&adornments=false`;

    const response = await this.http.axiosRef.get(url);

    try {
      // cache data
      await ensureDir(join(this.config.get<string>('DIR_CACHE'), 'elevation'));
      const file = join(
        this.config.get<string>('DIR_CACHE'),
        'elevation',
        `${latitude}-${longitude}-${radius}.json`,
      );
      await writeJson(file, response.data, { spaces: 2 });
    } catch (e) {
      this.logger.error(e);
    }

    // send
    res.json(response.data);
  }

  @ApiOperation({
    summary: 'Get terrain model',
    description: embedDocFile('elevation-gltf.md'),
  })
  @ApiProduces('model/gltf-binary')
  @ApiOkResponse({
    description: 'glTF model',
    schema: {
      type: 'string',
      format: 'binary',
    },
  })
  @ApiNotFoundResponse()
  @Get('gltf/:folder/:file')
  async getGltf(
    @Param('folder') folder: string,
    @Param('file') file: string,
    @Res() res: Response,
  ): Promise<void> {
    // cache directory
    const filePath = join(
      this.config.get<string>('DIR_CACHE'),
      'elevation',
      folder,
      file,
    );
    await ensureDir(
      join(this.config.get<string>('DIR_CACHE'), 'elevation', folder),
    );

    // request
    const response = await this.http.axiosRef({
      method: 'get',
      url: `https://api.elevationapi.com/gltf/${folder}/${file}`,
      responseType: 'stream',
      headers: { 'Accept-Encoding': 'gzip,deflate,compress' },
    });

    // write to cache
    response.data.pipe(createWriteStream(filePath));

    // send to client
    response.data.pipe(res);
  }
}
