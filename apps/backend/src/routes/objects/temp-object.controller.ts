import {
  Controller,
  Delete,
  HttpCode,
  HttpStatus,
  NotFoundException,
  Param,
  Post,
  Res,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBody,
  ApiConsumes,
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
  ApiUnsupportedMediaTypeResponse,
} from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
import { Express, Response } from 'express';
import { join } from 'node:path';
import * as fs from 'fs-extra';
import { nanoid } from 'nanoid';
import { removeIllegalFileChars } from '@uh4d/utils';
import { BackendConfig } from '@backend/config';
import { Auth } from '@backend/auth/auth.decorator';
import { ObjectMediaService } from '@backend/upload/object-media.service';
import { TempObjectDto } from './dto/temp-object.dto';
import { UploadObjectDto } from './dto/upload-object.dto';

@ApiTags('Objects')
@Controller('objects/tmp')
export class TempObjectController {
  constructor(
    private readonly config: BackendConfig,
    private readonly objectMediaService: ObjectMediaService,
  ) {}

  @ApiOperation({
    summary: 'Upload object',
    description: 'Upload object to temporary location.',
  })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    type: UploadObjectDto,
  })
  @ApiCreatedResponse({
    type: TempObjectDto,
  })
  @ApiUnsupportedMediaTypeResponse()
  @Auth('uploadObjects')
  @Post()
  @UseInterceptors(FileInterceptor('uploadModel'))
  async upload(
    @UploadedFile() file: Express.Multer.File,
  ): Promise<TempObjectDto> {
    const id = nanoid(9);
    const absPath = join(this.config.dirTmp, id);
    const fileName = removeIllegalFileChars(file.originalname);

    try {
      // create tmp folder
      await fs.ensureDir(absPath);
      // copy uploaded file to tmp folder
      await fs.rename(file.path, join(absPath, fileName));

      const generatedGlb = await this.objectMediaService.generateGltfFromFile({
        path: absPath,
        name: fileName,
      });

      return {
        id,
        ...generatedGlb,
        path:
          absPath.slice(
            absPath.indexOf(this.config.dirData) +
              this.config.dirData.length +
              1,
          ) + '/',
        original: fileName,
      };
    } catch (e) {
      // cleanup paths
      await fs.remove(file.path);
      await fs.remove(absPath);

      throw e;
    }
  }

  @ApiOperation({
    summary: 'Delete temporary object',
    description: 'Delete all files of temporary object.',
  })
  @ApiNoContentResponse()
  @ApiNotFoundResponse()
  @HttpCode(HttpStatus.NO_CONTENT)
  @Auth('uploadObjects')
  @Delete(':tmpId')
  async remove(
    @Param('tmpId') tmpId: string,
    @Res() res: Response,
  ): Promise<void> {
    const absPath = join(this.config.dirTmp, tmpId);
    const exists = await fs.pathExists(absPath);

    if (!exists) {
      throw new NotFoundException(`Temporary folder ${tmpId} does not exist!`);
    }

    await fs.remove(absPath);

    res.send();
  }
}
