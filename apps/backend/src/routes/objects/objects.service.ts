import {
  BadRequestException,
  ConflictException,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import * as fs from 'fs-extra';
import { join } from 'node:path';
import { v4 as uuid } from 'uuid';
import { nanoid } from 'nanoid';
import { Euler, Matrix4, Quaternion, Vector3 } from 'three';
import { extractUpdateProperties } from '@backend/utils/body-utils';
import { Coordinates, removeIllegalFileChars } from '@uh4d/utils';
import { computeModelCenter } from '@backend/utils/three-utils';
import { getSpatialParams } from '@backend/utils/spatial-params';
import { ObjectDto } from '@backend/dto/object';
import { BackendConfig } from '@backend/config';
import { AuthUser } from '@backend/auth/user';
import { OsmClientService } from '@backend/microservices/osm-client/osm-client.service';
import { IntersectionsPayload } from '@uh4d/dto/interfaces/custom/osm';
import { GetObjectsQueryParams } from './dto/get-objects-query-params';
import { ObjectFullDto, UpdateObjectFullDto } from './dto/object-full.dto';
import { CreateDuplicateDto } from './dto/create-duplicate.dto';
import { SaveObjectDto } from './dto/save-object.dto';

@Injectable()
export class ObjectsService {
  private readonly logger = new Logger(ObjectsService.name);

  constructor(
    private readonly neo4j: Neo4jService,
    private readonly config: BackendConfig,
    private readonly osmClient: OsmClientService,
  ) {}

  query(query: GetObjectsQueryParams, userId?: string): Promise<ObjectDto[]> {
    const spatialParams = getSpatialParams(query);

    let pending = false;
    let declined = false;

    // first triage
    (query.q ? (Array.isArray(query.q) ? query.q : [query.q]) : []).forEach(
      (value) => {
        if (value === 'pending') {
          pending = true;
        } else if (value === 'declined') {
          declined = true;
        }
      },
    );

    let q = '';

    if (spatialParams) {
      q += `WITH point({latitude: $geo.latitude, longitude: $geo.longitude}) AS userPoint `;
    }

    q += 'MATCH ' + (query.scene ? '(:E53:UH4D {id: $scene})<-[:P89]-' : '');
    q += `(place:E53)-[:P157]->(e22:E22:UH4D)-[:P1]->(name:E41),
          (e22)<-[:P67]-(file:D9)-[:P106]->(object:D1),
          (place)-[:P168]->(geo:E94) `;

    q += `
      WHERE coalesce(e22.pending, FALSE) = $pending
      AND coalesce(e22.declined, FALSE) = $declined
    `;

    if (spatialParams) {
      q += `AND distance(geo.point, userPoint) < $geo.radius `;
    }
    if (userId) {
      q += `AND (e22)-[:uploaded_by]->(:User:UH4D {id: $userId}) `;
    }

    q += `
      OPTIONAL MATCH (e22)<-[:P108]-(:E12)-[:P4]->(:E52)-[:P82]->(from:E61)
      OPTIONAL MATCH (e22)<-[:P13]-(:E6)-[:P4]->(:E52)-[:P82]->(to:E61)`;

    if (query.date) {
      q += `
        WITH e22, name, object, file, from, to, geo, place
        WHERE (from IS NULL OR from.value < date($date)) AND (to IS NULL OR to.value > date($date))`;
    }

    q += `
      OPTIONAL MATCH (e22)-[userEventU:uploaded_by]->(userU:User)-[:P131]->(usernameU:E82)
      OPTIONAL MATCH (e22)-[userEventE:edited_by]->(userE:User)-[:P131]->(usernameE:E82)
      OPTIONAL MATCH (place)-[:P121]->(osmPlace:E53)`;

    q += `
      RETURN e22.id AS id,
        name.value AS name,
        {from: from.value, to: to.value} AS date,
        apoc.map.removeKeys(object, ['id', 'vrcity_forceTextures', 'vrcity_noEdges']) AS origin,
        apoc.map.removeKey(file, 'id') AS file,
        apoc.map.removeKeys(geo, ['id', 'point']) AS location,
        object.vrcity_forceTextures AS vrcity_forceTextures,
        object.vrcity_noEdges AS vrcity_noEdges,
        collect(osmPlace.osm) AS osm_overlap,
        e22.pending AS pending,
        e22.declined AS declined,
        userU{userId: userU.id, username: usernameU.value, timestamp: userEventU.timestamp} AS uploadedBy,
        userE{userId: userE.id, username: usernameE.value, timestamp: userEventE.timestamp} AS editedBy
    `;

    const params = {
      userId,
      scene: query.scene,
      geo: spatialParams,
      date: query.date,
      pending,
      declined,
    };

    return this.neo4j.read(q, params);
  }

  async getById(objectId: string): Promise<ObjectFullDto | undefined> {
    // language=Cypher
    const q = `
      MATCH (e22:E22:UH4D {id: $id})-[:P1]->(name:E41),
            (e22)<-[:P67]-(file:D9)-[:P106]->(object:D1),
            (e22)<-[:P157]-(place:E53)-[:P168]->(geo:E94)
      OPTIONAL MATCH (e22)<-[:P108]-(:E12)-[:P4]->(:E52)-[:P82]->(from:E61)
      OPTIONAL MATCH (e22)<-[:P13]-(:E6)-[:P4]->(:E52)-[:P82]->(to:E61)
      OPTIONAL MATCH (e22)-[:P48]->(identifier:E42)
      OPTIONAL MATCH (e22)-[:P3]->(misc:E62)
      OPTIONAL MATCH (place)-[:P87]->(addr:E45)
      OPTIONAL MATCH (addr)-[:P139]-(fAddr:E45)
      WITH e22, name, misc, from, to, identifier, object, file, geo, addr, place, collect(fAddr.value) AS formerAddresses
      OPTIONAL MATCH (place)-[:P121]->(osmPlace:E53)
      WITH e22, name, misc, from, to, identifier, object, file, geo, addr, formerAddresses, collect(osmPlace.osm) AS osm_overlap
      OPTIONAL MATCH (e22)-[userEventU:uploaded_by]->(userU:User)-[:P131]->(usernameU:E82)
      OPTIONAL MATCH (e22)-[userEventE:edited_by]->(userE:User)-[:P131]->(usernameE:E82)
      OPTIONAL MATCH (e22)-[:has_tag]->(tag:TAG)
      RETURN e22.id AS id,
             name.value AS name,
             {from: from.value, to: to.value} AS date,
             apoc.map.removeKeys(object, ['id', 'vrcity_forceTextures', 'vrcity_noEdges']) AS origin,
             apoc.map.removeKey(file, 'id') AS file,
             apoc.map.removeKeys(geo, ['id', 'point']) AS location,
             object.vrcity_forceTextures AS vrcity_forceTextures,
             object.vrcity_noEdges AS vrcity_noEdges,
             identifier.zenodo_doi AS zenodo_doi,
             misc.value AS misc,
             addr.value as address,
             formerAddresses,
             osm_overlap,
             collect(tag.id) AS tags,
             e22.pending AS pending,
             e22.declined AS declined,
             userU{userId: userU.id, username: usernameU.value, timestamp: userEventU.timestamp} AS uploadedBy,
             userE{userId: userE.id, username: usernameE.value, timestamp: userEventE.timestamp} AS editedBy
   `;

    const params = {
      id: objectId,
    };

    return (await this.neo4j.read(q, params))[0];
  }

  /**
   * Move uploaded files from temp folder to final destination and store in database.
   */
  async create(body: SaveObjectDto, user: AuthUser): Promise<ObjectFullDto> {
    // check if tmp path exists
    const tmpPath = join(this.config.dirTmp, body.id);

    const exists = await fs.pathExists(tmpPath);
    if (!exists) {
      throw new BadRequestException(
        `Temporary folder ${body.id} does not exist!`,
      );
    }

    // check if files exist
    const existsPromises = [
      body.file,
      body.lowRes,
      body.noEdges,
      body.noEdgesLowRes,
    ].map((v) => (v ? fs.pathExists(join(tmpPath, v)) : true));
    if ((await Promise.all(existsPromises)).some((v) => v !== true)) {
      throw new BadRequestException(
        `Some of the temporary files do not exist!`,
      );
    }

    // compute center
    const location = new Coordinates(
      body.latitude,
      body.longitude,
      body.altitude,
    );
    const scale = new Vector3(1, 1, 1);
    const rotation = new Euler(body.omega, body.phi, body.kappa, 'YXZ');
    if (body.scale) {
      const s = body.scale;
      if (typeof s === 'number') {
        scale.set(s, s, s);
      } else if (Array.isArray(s) && s.length === 3) {
        scale.fromArray(s);
      } else {
        body.scale = undefined;
      }
    }
    const matrix = new Matrix4().compose(
      new Vector3(),
      new Quaternion().setFromEuler(rotation),
      scale,
    );
    const center = await computeModelCenter(join(tmpPath, body.file), matrix);
    location.add(center);

    // parameters
    const id = nanoid(9);
    const relPath = `objects/${uuid()}/`;
    const absPath = join(this.config.dirData, relPath);

    const params = {
      userId: user.id,
      pending: user.needsValidation(),
      e22id: 'e22_' + id,
      placeId: 'e53_' + id,
      e41: {
        id: 'e41_' + id,
        value: body.name,
      },
      dobj: {
        id: 'd1_' + id,
        latitude: body.latitude,
        longitude: body.longitude,
        altitude: body.altitude,
        omega: body.omega,
        phi: body.phi,
        kappa: body.kappa,
        scale: body.scale,
      },
      file: {
        id: 'd9_' + id,
        path: relPath,
        file: body.file,
        type: body.type,
        original: body.original,
        lowRes: body.lowRes,
        noEdges: body.noEdges,
        noEdgesLowRes: body.noEdgesLowRes,
      },
      geo: {
        id: 'e94_' + id,
        ...location.toJSON(),
      },
    };

    // language=Cypher
    const q = `
      MATCH (user:User:UH4D { id: $userId })
      CREATE (e22:E22:UH4D {id: $e22id})-[:P1]->(name:E41:UH4D $e41),
             (e22)<-[:P67]-(file:D9:UH4D $file)-[:P106]->(object:D1:UH4D $dobj),
             (e22)<-[:P157]-(place:E53:UH4D {id: $placeId}),
             (place)-[:P168]->(geo:E94:UH4D $geo),
             (e22)-[:uploaded_by { timestamp: datetime() }]->(user)
      SET geo.point = point({latitude: $geo.latitude, longitude: $geo.longitude})

      WITH e22, name, file, object, geo
      CALL apoc.do.when($pending, 'SET e22.pending = TRUE RETURN e22', '', { e22: e22 }) YIELD value

      RETURN e22.id AS id,
             name.value AS name,
             {from: NULL, to: NULL} AS date,
             apoc.map.removeKey(object, 'id') AS origin,
             apoc.map.removeKey(file, 'id') AS file,
             apoc.map.removeKey(geo, 'id') AS location,
             NULL AS misc,
             NULL as address,
             [] AS formerAddresses,
             [] AS tags
    `;

    try {
      // move to final location
      this.logger.log(`Move: ${tmpPath} ${absPath}`);
      await fs.move(tmpPath, absPath);

      const result = (await this.neo4j.write(q, params))[0];

      if (result) {
        // update OSM intersections
        result.osm_overlap = await this.updateOsmIntersections(result.id);
        return result;
      } else {
        throw new Error('No nodes added for ' + body.file);
      }
    } catch (e) {
      // cleanup paths
      await fs.remove(tmpPath);
      await fs.remove(absPath);

      throw e;
    }
  }

  async update(
    objectId: string,
    body: UpdateObjectFullDto,
  ): Promise<ObjectFullDto | undefined> {
    // get current entry
    const object = await this.getById(objectId);
    if (!object) return undefined;

    // build cypher statements for properties that need to be updated
    const updateObject = extractUpdateProperties(object, body);

    const statements = await Promise.all(
      Object.entries(updateObject).map(([key, value]) =>
        this.buildUpdateStatement(object, { [key]: value }),
      ),
    );
    await this.neo4j.multipleStatements(statements);

    // execute tasks after database update
    if (Reflect.has(updateObject, 'origin')) {
      await this.updateOsmIntersections(objectId);
      await this.cleanOsmNodes();
    }

    // get complete updated entry
    return this.getById(objectId);
  }

  private async buildUpdateStatement(
    currentEntry: ObjectFullDto,
    body: UpdateObjectFullDto,
  ): Promise<{ statement: string; parameters: Record<string, any> }> {
    let q = `MATCH (e22:E22:UH4D {id: $id})<-[:P157]-(place:E53) `;

    const params: Record<string, any> = {
      id: currentEntry.id,
    };

    const id = nanoid(9);

    switch (Object.keys(body)[0]) {
      case 'name':
        q += `MATCH (e22)-[:P1]->(name:E41) SET name.value = $name`;
        params.name = body.name;
        break;

      case 'date': {
        const updateFrom =
          body.date.from !== undefined &&
          body.date.from !== currentEntry.date.from;
        const updateTo =
          body.date.to !== undefined && body.date.to !== currentEntry.date.to;

        if (updateFrom && !body.date.from) {
          // remove erection date
          q += `
            OPTIONAL MATCH (e22)<-[:P108]-(e12:E12)-[:P4]->(e52from:E52)-[:P82]->(from:E61)
            DETACH DELETE e12, e52from, from`;
        }
        if (updateTo && !body.date.to) {
          // remove destruction date
          q += `
            OPTIONAL MATCH (e22)<-[:P13]-(e6:E6)-[:P4]->(e52to:E52)-[:P82]->(to:E61)
            DETACH DELETE e6, e52to, to`;
        }
        if (updateFrom && body.date.from) {
          // set erection date
          q += `
            MERGE (e22)<-[:P108]-(e12:E12:UH4D)-[:P4]->(e52from:E52:UH4D)-[:P82]->(dateFrom:E61:UH4D)
            ON CREATE SET e12.id = $e12id, e52from.id = $e52fromId, dateFrom.id = $e61fromId
            SET dateFrom.value = date($dateFrom)`;
          params.e12id = 'e12_' + id;
          params.e52fromId = 'e52_e12_' + id;
          params.e61fromId = 'e61_e52_e12_' + id;
          params.dateFrom = body.date.from;
        }
        if (updateTo && body.date.to) {
          // set destruction date
          q += `
            MERGE (e22)<-[:P13]-(e6:E6:UH4D)-[:P4]->(e52to:E52:UH4D)-[:P82]->(dateTo:E61:UH4D)
            ON CREATE SET e6.id = $e6id, e52to.id = $e52toId, dateTo.id = $e61toId
            SET dateTo.value = date($dateTo)`;
          params.e6id = 'e6_' + id;
          params.e52toId = 'e52_e6_' + id;
          params.e61toId = 'e61_e52_e6_' + id;
          params.dateTo = body.date.to;
        }
        break;
      }

      case 'misc':
        if (body.misc) {
          // set misc
          q += `
            MERGE (e22)-[:P3]->(misc:E62)
            ON CREATE SET misc.id = $misc.id
            SET misc.value = $misc.value`;
          params.misc = { id: 'e62_misc_' + id, value: body.misc };
        } else {
          // remove misc
          q += `OPTIONAL MATCH (e22)-[:P3]->(misc:E62) DETACH DELETE misc`;
        }
        break;

      case 'address':
        q += `
          OPTIONAL MATCH (place)-[r:P87]->(:E45)
          DELETE r
          MERGE (addr:E45:UH4D {value: $address.value})
          ON CREATE SET addr.id = $address.id
          MERGE (place)-[:P87]->(addr)`;
        params.address = { id: 'e45_' + id, value: body.address };
        break;

      case 'tags':
        q += `
          OPTIONAL MATCH (e22)-[r:has_tag]->(:TAG)
          DELETE r
          WITH e22
          FOREACH (tag IN $tags |
            MERGE (t:TAG:UH4D {id: tag})
            MERGE (e22)-[:has_tag]->(t)
          )`;
        params.tags = body.tags || [];
        break;

      case 'origin': {
        // compute new center
        const rotation = new Euler(
          body.origin.omega,
          body.origin.phi,
          body.origin.kappa,
          'YXZ',
        );
        const scale = new Vector3(1, 1, 1);
        if (body.origin.scale) {
          const s = body.origin.scale as number | number[];
          if (typeof s === 'number') {
            scale.set(s, s, s);
          } else if (Array.isArray(s) && s.length === 3) {
            scale.fromArray(s);
          } else {
            body.origin.scale = undefined;
          }
        }
        const matrix = new Matrix4().compose(
          new Vector3(),
          new Quaternion().setFromEuler(rotation),
          scale,
        );
        const filePath = join(
          this.config.dirData,
          currentEntry.file.path,
          currentEntry.file.file,
        );
        const center = await computeModelCenter(filePath, matrix);
        const location = new Coordinates(
          body.origin.latitude,
          body.origin.longitude,
          body.origin.altitude,
        )
          .add(center)
          .toJSON();

        q += `
          MATCH (e22)<-[:P67]-(:D9)-[:P106]->(object:D1), (place)-[:P168]->(geo:E94)
          OPTIONAL MATCH (object)-[:P67]->(:E22)<-[:P157]-(:E53)-[:P168]->(geoDuplicate:E94)
          SET object += $origin,
              geo += $location,
              geoDuplicate += $location,
              geo.point = point({latitude: $location.latitude, longitude: $location.longitude}),
              geoDuplicate.point = point({latitude: $location.latitude, longitude: $location.longitude})
        `;
        params.origin = body.origin;
        params.location = location;
        break;
      }

      case 'zenodo_doi':
        q += `
          MERGE (e22)-[:P48]->(identifier:E42:UH4D)
            ON CREATE SET identifier.id = $e42id
          SET identifier.zenodo_doi = $zenodoDoi
        `;
        params.e42id = 'e42_' + id;
        params.zenodoDoi = body.zenodo_doi;
        break;

      case 'vrcity_forceTextures':
        q += `
          MATCH (e22)<-[:P67]-(:D9)-[:P106]->(object:D1)
          SET object.vrcity_forceTextures = $forceTextures
        `;
        params.forceTextures = !!body.vrcity_forceTextures;
        break;

      case 'vrcity_noEdges':
        q += `
          MATCH (e22)<-[:P67]-(:D9)-[:P106]->(object:D1)
          SET object.vrcity_noEdges = $noEdges
        `;
        params.noEdges = !!body.vrcity_noEdges;
        break;
    }

    q += ' RETURN e22 AS object';

    return { statement: q, parameters: params };
  }

  /**
   * Remove object from database and delete files.
   * However, files won't be deleted if there is a duplicate, i.e. other objects referencing the files.
   */
  async remove(objectId: string): Promise<void> {
    // detach POI from object
    // language=Cypher
    const qPoi = `
      MATCH (e22:E22:UH4D {id: $id})<-[:P67]-(poi:E73)-[:P2]-(:E55 {id: "poi"}),
            (poi)-[:P67]->(e92:E92)
      OPTIONAL MATCH (e22)<-[:P108]-(:E12)-[:P4]->(:E52)-[:P82]->(from:E61)
      OPTIONAL MATCH (e22)<-[:P13]-(:E6)-[:P4]->(:E52)-[:P82]->(to:E61)
      MERGE (e92)-[:P160]->(e52:E52:UH4D)-[:P82]->(date:E61:UH4D)
        ON CREATE SET e52.id = replace(poi.id, "e73_", "e52_"), date.id = replace(poi.id, "e73_", "e61_")
      SET date.from = CASE WHEN from IS NULL THEN NULL ELSE from.value END,
      date.to = CASE WHEN to IS NULL THEN NULL ELSE to.value END
      RETURN poi
    `;

    // language=Cypher
    const qRemove = `
      MATCH (e22:E22:UH4D {id: $id})-[:P1]->(name:E41),
            (e22)<-[:P67]-(file:D9)-[:P106]->(object:D1),
            (e22)<-[:P157]-(place)-[:P168]->(geo:E94)
      OPTIONAL MATCH (object)-[:P67]->(duplicate:E22) WHERE NOT e22 = duplicate
      WITH e22, place, name, object, file, geo, duplicate, file.path AS path
      CALL apoc.do.when(duplicate IS NULL, 'DETACH DELETE object, file RETURN 1 AS check', '', { object: object, file: file }) YIELD value
      OPTIONAL MATCH (e22)<-[:P108]-(e12:E12)-[:P4]->(e12e52:E52)-[:P82]->(from:E61)
      OPTIONAL MATCH (e22)<-[:P13]-(e6:E6)-[:P4]->(e6e52:E52)-[:P82]->(to:E61)
      OPTIONAL MATCH (e22)-[:P3]->(misc:E62)
      DETACH DELETE e22, place, geo, name, e12, e12e52, from, e6, e6e52, to, misc
      RETURN path, duplicate`;

    const params = {
      id: objectId,
    };

    // wrap in custom transaction
    const session = this.neo4j.getWriteSession();
    const txc = session.beginTransaction();

    try {
      // execute cypher queries
      await txc.run(qPoi, params);

      const result = await txc.run(qRemove, params);
      const records = Neo4jService.extractRecords<{
        path: string;
        duplicate: { id: string };
      }>(result.records);

      if (!records[0]) {
        throw new NotFoundException(`Object with ID ${objectId} not found!`);
      }

      // check for corrupted path to not remove wrong directory
      const relPath = records[0].path;
      if (!/^objects\/\S+$/.test(relPath)) {
        throw new ConflictException('Path to delete does not match pattern!');
      }

      // remove if there is no duplicate
      if (!records[0].duplicate) {
        this.logger.log('Remove: ' + relPath);
        await fs.remove(join(this.config.dirData, relPath));
      }

      await txc.commit();
    } catch (e) {
      await txc.rollback();
      throw e;
    } finally {
      await session.close();
    }
  }

  async duplicate(
    objectId: string,
    body: CreateDuplicateDto,
  ): Promise<ObjectFullDto | undefined> {
    // language=Cypher
    const q = `
      MATCH (original:E22:UH4D {id: $id})<-[:P67]-(file:D9)-[:P106]->(object:D1),
            (original)<-[:P157]-(place:E53)-[:P168]->(geo:E94)
      OPTIONAL MATCH (place)-[:P89]->(scene:E53)
      CREATE (e22:E22:UH4D {id: $e22id})-[:P1]->(name:E41:UH4D $e41),
             (e22)<-[:P157]-(placeNew:E53:UH4D {id: $e53id})-[:P89]->(scene),
             (placeNew)-[:P168]->(geoNew:E94 {id: $e94id}),
             (e22)<-[:P67]-(object)
      SET geoNew += apoc.map.removeKey(geo, 'id')
      CALL apoc.do.when(scene IS NOT NULL, 'CREATE (place)-[:P89]->(scene)', '', {place: placeNew, scene: scene}) YIELD value
      RETURN e22.id AS id,
             name.value AS name,
             {from: NULL, to: NULL} AS date,
             apoc.map.removeKeys(object, ['id', 'vrcity_forceTextures', 'noEdges']) AS origin,
             apoc.map.removeKey(file, 'id') AS file,
             apoc.map.removeKey(geoNew, 'id') AS location,
             object.vrcity_forceTextures AS vrcity_forceTextures,
             object.vrcity_noEdges AS vrcity_noEdges,
             NULL AS misc,
             NULL as address,
             [] AS formerAddresses,
             [] AS tags,
             [] AS osm_overlap
    `;

    const id =
      nanoid(9) + '_' + removeIllegalFileChars(body.name, true) + '_duplicate';

    const params = {
      id: objectId,
      e22id: 'e22_' + id,
      e41: {
        id: 'e41_' + id,
        value: body.name + ' Duplicate',
      },
      e53id: 'e53_' + id,
      e94id: 'e94_' + id,
    };

    return (await this.neo4j.write(q, params))[0];
  }

  async updateOsmIntersections(objectId: string): Promise<number[]> {
    // query object
    // language=Cypher
    const q0 = `
      MATCH (e22:E22:UH4D {id: $id})<-[:P67]-(file:D9)-[:P106]->(object:D1),
            (e22)<-[:P157]-(place:E53)-[:P168]->(geo:E94)
      RETURN e22.id AS id, object AS origin, file, geo AS location
    `;

    const item = (await this.neo4j.read(q0, { id: objectId }))[0];

    if (!item) {
      throw new Error(`Cannot find object with ID ${objectId}`);
    }

    // compute intersections in microservice
    const threadData: IntersectionsPayload = {
      modelPath: join(this.config.dirData, item.file.path, item.file.file),
      origin: item.origin,
      location: item.location,
    };

    const osmIds = await this.osmClient.computeIntersections(threadData);

    // update database
    // language=Cypher
    const q1 = `
      MATCH (e22:E22:UH4D {id: $id})<-[:P157]-(place:E53)
      OPTIONAL MATCH (place)-[r:P121]->(:E53)
      DELETE r
      WITH place
      UNWIND $osmIds AS osmId
      MERGE (osmPlace:E53:UH4D {id: 'e53_osm_' + toInteger(osmId), osm: toInteger(osmId)})
      MERGE (place)-[:P121]->(osmPlace)
    `;

    await this.neo4j.write(q1, { id: objectId, osmIds });

    return osmIds;
  }

  /**
   * Delete OSM place nodes that are not connected anymore.
   */
  async cleanOsmNodes(): Promise<void> {
    // language=Cypher
    const q = `
      MATCH (place:E53:UH4D)
      WHERE place.osm IS NOT NULL
      AND NOT (place)<-[:P121]-()
      DELETE place
    `;

    await this.neo4j.write(q);
  }
}
