import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  NotFoundException,
  Param,
  Patch,
  Post,
  Query,
  Res,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { Response } from 'express';
import { ObjectDto } from '@backend/dto/object';
import { Auth } from '@backend/auth/auth.decorator';
import { AuthUser, User } from '@backend/auth/user';
import { CommonService } from '@backend/common/common.service';
import { UpdateValidationStatusDto } from '@backend/routes/dto/update-validation-status.dto';
import { ObjectsService } from './objects.service';
import { GetObjectsQueryParams } from './dto/get-objects-query-params';
import { ObjectFullDto, UpdateObjectFullDto } from './dto/object-full.dto';
import { CreateDuplicateDto } from './dto/create-duplicate.dto';
import { SaveObjectDto } from './dto/save-object.dto';

@ApiTags('Objects')
@Controller('objects')
export class ObjectsController {
  constructor(
    private readonly objectService: ObjectsService,
    private readonly commonService: CommonService,
  ) {}

  @ApiOperation({
    summary: 'Query objects',
    description:
      'Query all objects. Query parameters may be set to filter the data. Only the most essential information gets returned. For more detailed information of an object, refer to `/objects/{objectId}`.\n\n`lat`, `lon`, and `r` works only in combination and may be used to look for objects within a radius at a position.',
  })
  @ApiOkResponse({
    type: ObjectDto,
    isArray: true,
  })
  @Get()
  async query(
    @Query() queryParams: GetObjectsQueryParams,
  ): Promise<ObjectDto[]> {
    return this.objectService.query(queryParams);
  }

  @ApiOperation({
    summary: 'Get object',
    description: 'Get object by ID.',
  })
  @ApiOkResponse({
    type: ObjectFullDto,
  })
  @ApiNotFoundResponse({
    description: 'Object with ID not found!',
  })
  @Get(':objectId')
  async getById(@Param('objectId') objectId: string): Promise<ObjectFullDto> {
    const object = await this.objectService.getById(objectId);

    if (!object) {
      throw new NotFoundException(`Object with ID ${objectId} not found!`);
    }

    return object;
  }

  @ApiOperation({
    summary: 'Save object',
    description:
      'Move uploaded files from temp folder to final destination and store in database.',
  })
  @ApiBody({
    type: SaveObjectDto,
  })
  @ApiCreatedResponse({
    type: ObjectFullDto,
  })
  @ApiBadRequestResponse()
  @Auth('uploadObjects')
  @Post()
  async create(
    @Body() body: SaveObjectDto,
    @User() user: AuthUser,
  ): Promise<ObjectFullDto> {
    return this.objectService.create(body, user);
  }

  @ApiOperation({
    summary: 'Update object',
    description:
      'Update properties of an object. Currently, only following properties can be updated: `name`, `date`, `misc`, `address`, `tags`, `origin`, `vrcity_forceTextures`, `vrcity_noEdges`, `zenodo_doi`.',
  })
  @ApiBody({
    type: UpdateObjectFullDto,
  })
  @ApiOkResponse({
    type: ObjectFullDto,
  })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse()
  @Auth()
  @Patch(':objectId')
  async update(
    @Param('objectId') objectId: string,
    @Body() body: UpdateObjectFullDto,
    @User() user: AuthUser,
  ): Promise<ObjectFullDto> {
    const object = await this.objectService.update(objectId, body);

    if (!object) {
      throw new NotFoundException(`Object with ID ${objectId} not found!`);
    }

    object.editedBy = await this.commonService.updateEditedBy(user, objectId);

    return object;
  }

  @ApiOperation({
    summary: 'Delete object',
    description:
      "Remove object from database and delete files. However, files won't be deleted if there is a duplicate, i.e. other objects referencing the files.",
  })
  @ApiNoContentResponse()
  @ApiNotFoundResponse()
  @ApiConflictResponse()
  @HttpCode(HttpStatus.NO_CONTENT)
  @Auth()
  @Delete(':objectId')
  async remove(
    @Param('objectId') objectId: string,
    @Res() res: Response,
  ): Promise<void> {
    await this.objectService.remove(objectId);
    res.send();
  }

  @ApiOperation({
    summary: 'Confirm/decline object',
    description: 'Set `pending` and/or `declined` flag.',
  })
  @ApiOkResponse({
    type: ObjectFullDto,
  })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse()
  @Auth('validateContent')
  @Patch(':objectId/validation')
  async validateObject(
    @Param('objectId') objectId: string,
    @Body() body: UpdateValidationStatusDto,
  ): Promise<ObjectFullDto> {
    const update = await this.commonService.updateValidationStatus(
      objectId,
      body.pending,
      body.declined,
    );

    if (!update) {
      throw new NotFoundException(`Object with ID ${objectId} not found!`);
    }

    return this.objectService.getById(objectId);
  }

  @ApiOperation({
    summary: 'Duplicate Object',
    description:
      'Duplicate the database entry for the object. However, the link to the 3D asset file will remain the same. Attached metadata, e.g. tags, addresses, will not be duplicated.',
  })
  @ApiBody({
    type: CreateDuplicateDto,
  })
  @ApiCreatedResponse({
    type: ObjectFullDto,
  })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse()
  @Auth('duplicateObjects')
  @Post(':objectId/duplicate')
  async duplicate(
    @Param('objectId') objectId: string,
    @Body() body: CreateDuplicateDto,
  ): Promise<ObjectFullDto> {
    const duplicate = await this.objectService.duplicate(objectId, body);

    if (!duplicate) {
      throw new NotFoundException(`Object with ID ${objectId} not found!`);
    }

    duplicate.osm_overlap = await this.objectService.updateOsmIntersections(
      objectId,
    );

    return duplicate;
  }
}
