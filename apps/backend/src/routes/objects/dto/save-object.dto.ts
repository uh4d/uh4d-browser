import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsLatitude,
  IsLongitude,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { SaveObjectDto as SaveObjectInterface } from '@uh4d/dto/interfaces/custom/object';

export class SaveObjectDto implements SaveObjectInterface {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  name: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  path: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  file: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  original: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  type: string;
  @ApiProperty({
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  lowRes?: string;
  @ApiProperty({
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  noEdges?: string;
  @ApiProperty({
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  noEdgesLowRes?: string;
  @ApiProperty({ format: 'float' })
  @IsNotEmpty()
  @IsNumber()
  @IsLatitude()
  latitude: number;
  @ApiProperty({ format: 'float' })
  @IsNotEmpty()
  @IsNumber()
  @IsLongitude()
  longitude: number;
  @ApiProperty({ format: 'float' })
  @IsNotEmpty()
  @IsNumber()
  altitude: number;
  @ApiProperty({ format: 'float' })
  @IsNotEmpty()
  @IsNumber()
  omega: number;
  @ApiProperty({ format: 'float' })
  @IsNotEmpty()
  @IsNumber()
  phi: number;
  @ApiProperty({ format: 'float' })
  @IsNotEmpty()
  @IsNumber()
  kappa: number;
  @ApiProperty({ type: 'array', items: { type: 'number', format: 'float' } })
  @IsArray()
  @IsNumber({}, { each: true })
  scale: number[];
}
