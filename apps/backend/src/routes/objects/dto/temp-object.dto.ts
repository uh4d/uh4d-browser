import { ApiProperty } from '@nestjs/swagger';
import { TempObjectDto as TempObjectInterface } from '@uh4d/dto/interfaces/custom/object';
import { CreateObjectFileDto } from '@backend/dto';

export class TempObjectDto
  extends CreateObjectFileDto
  implements TempObjectInterface
{
  @ApiProperty({ required: false })
  id: string;
  @ApiProperty({ required: false })
  name: string;
}
