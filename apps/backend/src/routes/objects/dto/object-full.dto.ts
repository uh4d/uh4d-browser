import { IntersectionType } from '@nestjs/swagger';
import { ObjectDto, UpdateObjectDto } from '@backend/dto/object';
import { ObjectMetaDto, UpdateObjectMetaDto } from '@backend/dto/object-meta';

export class ObjectFullDto extends IntersectionType(ObjectDto, ObjectMetaDto) {}

export class UpdateObjectFullDto extends IntersectionType(
  UpdateObjectDto,
  UpdateObjectMetaDto,
) {}
