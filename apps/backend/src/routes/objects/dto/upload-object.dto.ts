import { ApiProperty } from '@nestjs/swagger';

export class UploadObjectDto {
  @ApiProperty({
    type: 'string',
    format: 'binary',
  })
  uploadModel: any;
}
