import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsOptional } from 'class-validator';
import { LocationQueryParams } from '@backend/routes/dto/location-query-params';

export class GetObjectsQueryParams extends LocationQueryParams {
  @ApiProperty({
    description: 'Filter by date',
    format: 'date',
    required: false,
  })
  @IsOptional()
  @IsDateString()
  date?: string;

  @ApiProperty({
    description: 'Filter by specific scene',
    required: false,
  })
  @IsOptional()
  scene?: string;

  @ApiProperty({
    description:
      'Query string to filter results. Currently, only `pending` and `declined` are supported to filter by validation status.',
    required: false,
  })
  @IsOptional()
  q?: string;
}
