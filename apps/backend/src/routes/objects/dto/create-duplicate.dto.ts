import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateDuplicateDto {
  @ApiProperty({
    description: 'Name of the existing object (or any other)',
  })
  @IsNotEmpty()
  @IsString()
  name: string;
}
