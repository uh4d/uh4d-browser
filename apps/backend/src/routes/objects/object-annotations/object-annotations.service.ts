import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { join } from 'path';
import { Box3, Vector3 } from 'three';
import { nanoid } from 'nanoid';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { loadGltfObject } from '@uh4d/backend/utils';
import { ObjectsService } from '@backend/routes/objects/objects.service';
import { ObjectAnnotationDto } from '@backend/generated/dto';
import { NormDataService } from '@backend/routes/normdata/norm-data.service';
import { AnnotationsService } from '@backend/routes/annotations/annotations.service';

@Injectable()
export class ObjectAnnotationsService {
  private readonly logger = new Logger(ObjectAnnotationsService.name);

  constructor(
    private readonly config: ConfigService,
    private readonly neo4j: Neo4jService,
    private readonly objectsService: ObjectsService,
    private readonly normDataService: NormDataService,
    private readonly annotationsService: AnnotationsService,
  ) {}

  queryAnnotationsByObject(objectId: string): Promise<ObjectAnnotationDto[]> {
    // language=Cypher
    const q = `
      MATCH (obj:E22:UH4D {id: $id})-[:P46]->(ann:Annotation)
      OPTIONAL MATCH (ann)-[aatRel:P2]->(aat:AAT)
        WHERE aatRel.confidence IS NOT NULL
      WITH ann, collect(aat{.id, .identifier, .label, weight: aatRel.weight}) AS aat
      OPTIONAL MATCH (ann)-[wikiRel:P2]->(wd:Wikidata)
        WHERE wikiRel.confidence IS NOT NULL
      WITH ann, aat, collect(wd{.id, .identifier, .label, weight: wikiRel.weight}) AS wikidata
      // OPTIONAL MATCH (ann)-[nRel:is_close_to]-(neighbor:Annotation)
      RETURN ann.id AS id,
             ann.objectName AS objectName,
             ann.bbox AS bbox,
             ann.volume AS volume,
             aat,
             wikidata
             // collect(neighbor{.id, distance: nRel.distance, weight: nRel.weight}) AS neighbors
    `;

    const params = {
      id: objectId,
    };

    return this.neo4j.read(q, params);
  }

  /**
   * Parse identifiers from sub-object names and store them as annotations.
   */
  async parseAndSaveAnnotations(objectId: string): Promise<void> {
    const object = await this.objectsService.getById(objectId);
    if (!object) {
      throw new NotFoundException(`Object with ID ${objectId} not found!`);
    }

    // load 3D model
    const obj = await loadGltfObject(
      join(this.config.get('DIR_DATA'), object.file.path, object.file.file),
      object.origin,
    );

    // collect annotated sub-objects
    const preparedAnnotations: {
      id: string;
      name: string;
      bbox: number[];
      volume: number;
      wikiIds: { identifier: string; confidence: number }[];
      aatIds: { identifier: string; confidence: number }[];
    }[] = [];

    obj.traverse((child) => {
      const name = child.name;
      const { wikidata, aat } = this.normDataService.parseIdentifier(name);
      if (wikidata || aat) {
        const bbox = new Box3();
        bbox.expandByObject(child, true);
        const volume = bbox
          .getSize(new Vector3())
          .toArray()
          .reduce((product, curr) => product * curr, 1);

        preparedAnnotations.push({
          id: 'annotation_object_' + nanoid(9),
          name,
          bbox: [...bbox.min.toArray(), ...bbox.max.toArray()],
          volume,
          wikiIds: wikidata ? [{ identifier: wikidata, confidence: 1 }] : [],
          aatIds: aat ? [{ identifier: aat, confidence: 1 }] : [],
        });
      }
    });

    // language=Cypher
    const q = `
      MATCH (obj:E22:UH4D {id: $id})
      CALL {
        WITH obj
        OPTIONAL MATCH (obj)-[:P46]->(oldAnn:Annotation)
        DETACH DELETE oldAnn
      }

      UNWIND $annotations AS a
      MERGE (ann:E22:UH4D:Annotation {id: a.id, objectName: a.name, bbox: a.bbox, volume: a.volume})
      MERGE (obj)-[:P46]->(ann)

      FOREACH (aat IN a.aatIds |
        MERGE (aatType:E55:UH4D:AAT {id: "aat:" + aat.identifier, identifier: aat.identifier})
        MERGE (ann)-[aatRel:P2]->(aatType)
        SET aatRel.confidence = aat.confidence
      )
      FOREACH (wiki IN a.wikiIds |
        MERGE (wikiType:E55:UH4D:Wikidata {id: "wiki:" + wiki.identifier, identifier: wiki.identifier})
        MERGE (ann)-[wikiRel:P2]->(wikiType)
        SET wikiRel.confidence = wiki.confidence
      )

      RETURN DISTINCT obj AS object
    `;

    const params = {
      id: objectId,
      annotations: preparedAnnotations,
    };

    const results = await this.neo4j.write(q, params);
    if (!results[0]?.object) {
      throw new Error('Write query did not return any results');
    }

    // update norm data and annotation weights asynchronously
    this.triggerUpdateChain(objectId, 10)
      .then(() => {
        this.logger.log('Norm data and object annotation update complete.');
      })
      .catch((reason) => {
        this.logger.error(reason);
      });
  }

  async computeSpatialNeighborWeights(
    objectId: string,
    maxDistanceThreshold = 10,
  ) {
    const annotations = await this.queryAnnotationsByObject(objectId);
    if (annotations.length === 0) return;

    const object = await this.objectsService.getById(objectId);
    if (!object) {
      throw new NotFoundException(`Object with ID ${objectId} not found!`);
    }

    // prepare bounding boxes
    const list = annotations.map((ann) => {
      const bbox = new Box3(
        new Vector3().fromArray(ann.bbox),
        new Vector3().fromArray(ann.bbox, 3),
      );

      return {
        id: ann.id,
        bbox,
        center: bbox.getCenter(new Vector3()),
        halfSize: bbox.getSize(new Vector3()).divideScalar(2),
      };
    });

    const weightedRelations: {
      sourceId: string;
      targetId: string;
      distance: number;
      weight: number;
    }[] = [];

    for (let i = 0, l = list.length; i < l; i++) {
      const annCurr = list[i];
      for (let j = i + 1; j < l; j++) {
        const annTest = list[j];

        // compute distance between AABBs
        const vec = new Vector3()
          .fromArray(
            new Vector3()
              .subVectors(annCurr.center, annTest.center)
              .toArray()
              .map((v) => Math.abs(v)),
          )
          .sub(new Vector3().addVectors(annCurr.halfSize, annTest.halfSize))
          .max(new Vector3());
        const distance = vec.length();

        if (distance > maxDistanceThreshold) continue;

        weightedRelations.push({
          sourceId: annCurr.id,
          targetId: annTest.id,
          distance,
          weight: 1 - distance / maxDistanceThreshold,
        });
      }
    }

    // language=Cypher
    const q = `
      MATCH (obj:E22:UH4D {id: $objectId})
      CALL {
        WITH obj
        MATCH (obj)-[:P46]->(ann:Annotation)
        OPTIONAL MATCH (ann)-[oldRel:is_close_to]-(:Annotation)
        DELETE oldRel
      }

      UNWIND $weights AS w
      MATCH (obj)-[:P46]->(source:Annotation:UH4D {id: w.sourceId}),
            (obj)-[:P46]->(target:Annotation:UH4D {id: w.targetId})
      MERGE (source)-[rel:is_close_to]->(target)
      SET rel.distance = w.distance, rel.weight = w.weight

      RETURN source, target, rel.weight AS weight
    `;

    const params = {
      objectId,
      weights: weightedRelations,
    };

    const results = await this.neo4j.write(q, params);
    return results.map((r) => ({
      sourceId: r.source.id,
      targetId: r.target.id,
      weight: r.weight,
    }));
  }

  /**
   * Update identifier nodes and compute weighted relationships
   * between spatial and semantic neighbors.
   */
  async triggerUpdateChain(
    objectIds: string | string[],
    maxDistanceThreshold?: number,
    updateNormData = true,
  ): Promise<void> {
    if (updateNormData) {
      await this.normDataService.updateAll();
    }

    const ids = Array.isArray(objectIds) ? objectIds : [objectIds];
    for (const id of ids) {
      this.logger.debug(`Compute weights for object ${id}`);

      await this.computeSpatialNeighborWeights(id, maxDistanceThreshold);

      const annotations = await this.queryAnnotationsByObject(id);
      for (const ann of annotations) {
        await this.annotationsService.computeSemanticNeighborWeights(ann.id);
      }
    }

    await this.annotationsService.computeEmbeddings();
  }
}
