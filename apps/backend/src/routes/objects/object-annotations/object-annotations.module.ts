import { forwardRef, Module } from '@nestjs/common';
import { AnnotationsModule } from '@backend/routes/annotations/annotations.module';
import { NormDataModule } from '@backend/routes/normdata/norm-data.module';
import { ObjectsModule } from '../objects.module';
import { ObjectAnnotationsController } from './object-annotations.controller';
import { ObjectAnnotationsService } from './object-annotations.service';

@Module({
  imports: [AnnotationsModule, NormDataModule, forwardRef(() => ObjectsModule)],
  controllers: [ObjectAnnotationsController],
  providers: [ObjectAnnotationsService],
})
export class ObjectAnnotationsModule {}
