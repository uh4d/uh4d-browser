import { Test, TestingModule } from '@nestjs/testing';
import { ObjectAnnotationsController } from './object-annotations.controller';

describe('ObjectAnnotationsController', () => {
  let controller: ObjectAnnotationsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ObjectAnnotationsController],
    }).compile();

    controller = module.get<ObjectAnnotationsController>(ObjectAnnotationsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
