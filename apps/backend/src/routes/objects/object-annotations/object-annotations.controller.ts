import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import {
  ApiBody,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { ObjectAnnotationDto } from '@backend/generated/dto';
import { ObjectAnnotationsService } from './object-annotations.service';
import { ObjectAnnotationComputeWeightsDto } from './dto/object-annotation-compute-weights.dto';

@ApiTags('Annotations')
@Controller('objects/:objectId/annotations')
export class ObjectAnnotationsController {
  constructor(
    private readonly objectAnnotationService: ObjectAnnotationsService,
  ) {}

  @ApiOperation({
    summary: 'Get annotations of object',
    description: 'Get all annotation of an object.',
  })
  @ApiOkResponse({
    type: ObjectAnnotationDto,
    isArray: true,
  })
  @Get()
  queryAnnotationsByObject(@Param('objectId') objectId: string) {
    return this.objectAnnotationService.queryAnnotationsByObject(objectId);
  }

  @ApiOperation({
    summary: 'Parse object annotations',
    description:
      'Parse existing object for sub-objects that are named following the annotation syntax and store them as object annotations.',
  })
  @ApiCreatedResponse()
  @ApiNotFoundResponse()
  @Post()
  parseAnnotations(@Param('objectId') objectId: string) {
    return this.objectAnnotationService.parseAndSaveAnnotations(objectId);
  }

  @ApiOperation({
    summary: 'Compute weights (object)',
    description:
      'Compute spatial weights between annotations of a single objects and semantic weights between each annotation and identifier nodes.\n\nThese tasks will be called automatically when uploading/processing annotation data of an object.',
  })
  @ApiBody({
    type: ObjectAnnotationComputeWeightsDto,
    required: false,
  })
  @ApiCreatedResponse()
  @Post('weights')
  computeWeights(
    @Param('objectId') objectId: string,
    @Body() body: ObjectAnnotationComputeWeightsDto,
  ) {
    return this.objectAnnotationService.triggerUpdateChain(
      objectId,
      body.maxDistanceThreshold,
      false,
    );
  }
}
