import { Test, TestingModule } from '@nestjs/testing';
import { ObjectAnnotationsService } from './object-annotations.service';

describe('ObjectAnnotationsService', () => {
  let service: ObjectAnnotationsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ObjectAnnotationsService],
    }).compile();

    service = module.get<ObjectAnnotationsService>(ObjectAnnotationsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
