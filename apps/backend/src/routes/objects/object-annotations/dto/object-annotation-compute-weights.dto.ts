import { IsNotEmpty, IsNumber, IsPositive, ValidateIf } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ObjectAnnotationComputeWeightsDto {
  @ApiProperty({
    description: 'Maximum distance between bounding boxes in meters',
    required: false,
    default: 10,
  })
  @ValidateIf((_object, value) => value !== undefined)
  @IsNotEmpty()
  @IsNumber()
  @IsPositive()
  maxDistanceThreshold?: number;
}
