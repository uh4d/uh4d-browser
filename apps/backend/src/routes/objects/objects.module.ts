import { forwardRef, Module } from '@nestjs/common';
import { AuthModule } from '@backend/auth/auth.module';
import { CommonModule } from '@backend/common/common.module';
import { MicroservicesModule } from '@backend/microservices/microservices.module';
import { ObjectsController } from './objects.controller';
import { ObjectsService } from './objects.service';
import { TempObjectController } from './temp-object.controller';
import { ObjectAnnotationsModule } from './object-annotations/object-annotations.module';

@Module({
  imports: [
    forwardRef(() => AuthModule),
    CommonModule,
    MicroservicesModule,
    ObjectAnnotationsModule,
  ],
  controllers: [ObjectsController, TempObjectController],
  providers: [ObjectsService],
  exports: [ObjectsService],
})
export class ObjectsModule {}
