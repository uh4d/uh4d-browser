import { ApiProperty } from '@nestjs/swagger';

export class CreatedBackupDto {
  @ApiProperty({
    description: 'Name of the created cypher file',
  })
  filename: string;

  @ApiProperty({
    description: 'Full path/url to the created cypher file',
  })
  fullPath: string;
}
