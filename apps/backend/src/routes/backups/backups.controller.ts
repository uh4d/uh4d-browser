import { Controller, Post } from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { formatISO } from 'date-fns';
import { readdir } from 'fs-extra';
import { ApiCreatedResponse, ApiOperation } from '@nestjs/swagger';
import { BackendConfig } from '@backend/config';
import { CreatedBackupDto } from './dto/created-backup.dto';

@Controller('backups')
export class BackupsController {
  constructor(
    private readonly config: BackendConfig,
    private readonly neo4j: Neo4jService,
  ) {}

  @ApiOperation({
    summary: 'Create backup',
    description: 'Create backup of the Neo4j database as cypher file.',
  })
  @ApiCreatedResponse({
    type: CreatedBackupDto,
  })
  @Post()
  async createBackup(): Promise<CreatedBackupDto> {
    // check if there are cypher files with the same date
    const dateString = formatISO(new Date(), { representation: 'date' });
    const fileRegexp = new RegExp(`uh4d-neo4j-${dateString}.+\\.cypher`);

    const files = await readdir(this.config.dirBackups);
    const todayCount = files.filter((file) => fileRegexp.test(file)).length;
    const filename = `uh4d-neo4j-${dateString}-${todayCount + 1}.cypher`;

    // execute APOC procedure
    // language=Cypher
    const q = `
      CALL apoc.export.cypher.all($filename, { ifNotExists: true })
    `;
    const params = { filename };

    await this.neo4j.read(q, params);

    return {
      filename,
      fullPath: `${this.config.host}/backups/${filename}`,
    };
  }
}
