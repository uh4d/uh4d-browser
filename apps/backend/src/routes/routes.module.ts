import { Module } from '@nestjs/common';
import { ImagesModule } from './images/images.module';
import { ObjectsModule } from './objects/objects.module';
import { PoisModule } from './pois/pois.module';
import { ToursModule } from './tours/tours.module';
import { TerrainModule } from './terrain/terrain.module';
import { AddressesModule } from './addresses/addresses.module';
import { TagsModule } from './tags/tags.module';
import { LogsModule } from './logs/logs.module';
import { ElevationModule } from './elevation/elevation.module';
import { MiscModule } from './misc/misc.module';
import { PersonsModule } from './persons/persons.module';
import { LegalBodiesModule } from './legal-bodies/legal-bodies.module';
import { OverpassModule } from './overpass/overpass.module';
import { ScenesModule } from './scenes/scenes.module';
import { BugsModule } from './bugs/bugs.module';
import { VersionModule } from './version/version.module';
import { PipelineModule } from './pipeline/pipeline.module';
import { TextsModule } from './texts/texts.module';
import { RefactorModule } from './refactor/refactor.module';
import { NormDataModule } from './normdata/norm-data.module';
import { AnnotationsModule } from './annotations/annotations.module';
import { RephotoModule } from './rephoto/rephoto.module';
import { OsmModule } from './osm/osm.module';
import { BackupsModule } from './backups/backups.module';
import { DatesModule } from './dates/dates.module';
import { UsersModule } from './users/users.module';
import { TtsModule } from './tts/tts.module';

@Module({
  imports: [
    ImagesModule,
    ObjectsModule,
    PoisModule,
    ToursModule,
    TerrainModule,
    ElevationModule,
    AddressesModule,
    PersonsModule,
    LegalBodiesModule,
    TagsModule,
    MiscModule,
    LogsModule,
    OverpassModule,
    ScenesModule,
    BugsModule,
    VersionModule,
    PipelineModule,
    TextsModule,
    RefactorModule,
    NormDataModule,
    AnnotationsModule,
    RephotoModule,
    OsmModule,
    BackupsModule,
    DatesModule,
    UsersModule,
    TtsModule,
  ],
})
export class RoutesModule {}
