import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Param,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiConsumes,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';
import { FileInterceptor } from '@nestjs/platform-express';
import { Express } from 'express';
import { join } from 'node:path';
import { nanoid } from 'nanoid';
import * as fs from 'fs-extra';
import { format } from 'date-fns';
import { removeIllegalFileChars } from '@uh4d/utils';
import { ImagesService } from '@backend/routes/images/images.service';
import { ImageMediaService } from '@backend/upload/image-media.service';
import { ImageFullDto } from '@backend/routes/images/dto/image-full.dto';
import { Auth } from '@backend/auth/auth.decorator';
import { AuthUser, User } from '@backend/auth/user';
import { CreateRephotoData, RephotoService } from './rephoto.service';
import { UploadRephotoDto } from './dto/upload-rephoto.dto';
import { RephotoWithTypeDto } from './dto/rephoto-with-type.dto';

@ApiTags('Rephoto')
@Controller('rephoto')
export class RephotoController {
  constructor(
    private readonly rephotoService: RephotoService,
    private readonly config: ConfigService,
    private readonly imagesService: ImagesService,
    private readonly imageMediaService: ImageMediaService,
  ) {}

  @ApiOperation({
    summary: 'Get rephotos',
    description:
      'Get all images that are related as rephotos to the given image. Returns a list of images that are of type `has-rephoto` or `rephoto-of`.',
  })
  @ApiOkResponse({
    type: RephotoWithTypeDto,
  })
  @Get(':imageId')
  query(@Param('imageId') imageId: string) {
    return this.rephotoService.queryRephotos(imageId);
  }

  @ApiOperation({
    summary: 'Upload rephoto',
    description:
      "Upload rephotography of a given base image. Not all data necessary for spatialization is provided by the mobile device taking the rephoto. Hence, the camera constant `ck` will be estimated from the photo's EXIF data. The `altitude` will be queried from [OpenTopoData](https://www.opentopodata.org/) (adding 1.7 meters).",
  })
  @ApiConsumes('multipart/form-data')
  @ApiCreatedResponse({
    type: ImageFullDto,
  })
  @ApiBadRequestResponse()
  @Auth()
  @Post()
  @UseInterceptors(FileInterceptor('uploadImage'))
  async upload(
    @UploadedFile() file: Express.Multer.File,
    @Body() body: UploadRephotoDto,
    @User() user: AuthUser,
  ): Promise<ImageFullDto> {
    if (!file) {
      throw new BadRequestException('No file provided via field `uploadImage`');
    }

    const tmpDir = join(this.config.get<string>('DIR_DATA'), 'tmp', nanoid(9));
    const fileName = removeIllegalFileChars(file.originalname);
    const tmpFile = join(tmpDir, fileName);
    let finalPath: string;

    try {
      const histImage = await this.imagesService.getById(body.baseImageId);
      if (!histImage) {
        throw new BadRequestException(
          `Historical image with ID ${body.baseImageId} couldn't be found!`,
        );
      }

      // create tmp folder
      await fs.ensureDir(tmpDir);
      // copy uploaded file to tmp folder
      await fs.rename(file.path, tmpFile);

      const altitude = await this.rephotoService.getAltitudeFromLatLon(
        body.latitude,
        body.longitude,
      );

      const meta: CreateRephotoData = {
        baseImageId: body.baseImageId,
        title: 'Rephoto of ' + histImage.title,
        date: {
          value: format(
            body.timestamp ? new Date(body.timestamp) : new Date(),
            'yyyy.MM.dd',
          ),
        },
        camera: {
          latitude: body.latitude,
          longitude: body.longitude,
          altitude: altitude + 1.7, // add height of a human
          radius: 0,
          omega: body.omega,
          phi: body.phi,
          kappa: body.kappa,
          ck: await this.rephotoService.estimateCameraDistance(tmpFile),
          offset: [0, 0],
        },
        file: await this.imageMediaService.generateImageMedia(tmpFile),
        spatialStatus: 6,
        // tags: ['Rephoto'],
      };

      // copy into image folder
      await fs.copy(
        join(tmpDir, meta.file.path),
        join(this.config.get<string>('DIR_DATA'), meta.file.path),
      );

      finalPath = meta.file.path;

      // write to database
      const result = await this.rephotoService.create(user.id, meta);

      if (!result) {
        // remove generated files
        await fs.remove(
          join(this.config.get<string>('DIR_DATA'), meta.file.path),
        );
        throw new Error('No data written to database!');
      }

      // retrieve image meta data
      return this.imagesService.getById(result.image.id);
    } catch (e) {
      if (finalPath) {
        await fs.remove(join(this.config.get<string>('DIR_DATA'), finalPath));
      }
      throw e;
    } finally {
      // remove temporary folder
      await fs.remove(tmpDir);
    }
  }
}
