import { Test, TestingModule } from '@nestjs/testing';
import { RephotoController } from './rephoto.controller';

describe('RephotoController', () => {
  let controller: RephotoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RephotoController],
    }).compile();

    controller = module.get<RephotoController>(RephotoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
