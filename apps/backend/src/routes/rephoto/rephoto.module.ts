import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { AuthModule } from '@backend/auth/auth.module';
import { ImagesModule } from '@backend/routes/images/images.module';
import { RephotoController } from './rephoto.controller';
import { RephotoService } from './rephoto.service';

@Module({
  imports: [HttpModule, AuthModule, ImagesModule],
  controllers: [RephotoController],
  providers: [RephotoService],
})
export class RephotoModule {}
