import { Test, TestingModule } from '@nestjs/testing';
import { RephotoService } from './rephoto.service';

describe('RephotoService', () => {
  let service: RephotoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RephotoService],
    }).compile();

    service = module.get<RephotoService>(RephotoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
