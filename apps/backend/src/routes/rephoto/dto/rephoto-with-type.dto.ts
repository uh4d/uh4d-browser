import { ApiProperty, PickType } from '@nestjs/swagger';
import { ImageDto } from '@backend/dto';

export class RephotoWithTypeDto extends PickType(ImageDto, [
  'id',
  'title',
  'file',
  'camera',
  'author',
  'date',
  'spatialStatus',
  'pending',
  'declined',
]) {
  @ApiProperty({
    description: 'Type/direction of rephoto',
    enum: ['has-rephoto', 'rephoto-of'],
    required: false,
  })
  type: string;
}
