import { ApiProperty } from '@nestjs/swagger';
import {
  IsDateString,
  IsLatitude,
  IsLongitude,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { Type } from 'class-transformer';

export class UploadRephotoDto {
  @ApiProperty({
    type: 'string',
    format: 'binary',
  })
  uploadImage: any;
  @ApiProperty({
    description: 'ID of the rephotographed image',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  baseImageId: string;
  @ApiProperty({
    description:
      'Date/time the photo has been taken. If not provided, the upload time will be used.',
    type: 'string',
    format: 'date-time',
    required: false,
  })
  @IsOptional()
  @IsDateString()
  timestamp?: string;
  @ApiProperty({
    description:
      'Geographic coordinate that specifies the north-south position',
    type: 'number',
    format: 'float',
  })
  @IsNotEmpty()
  @IsNumber()
  @IsLatitude()
  @Type(() => Number)
  latitude: number;
  @ApiProperty({
    description: 'Geographic coordinate that specifies the east-west position',
    type: 'number',
    format: 'float',
  })
  @IsNotEmpty()
  @IsNumber()
  @IsLongitude()
  @Type(() => Number)
  longitude: number;
  @ApiProperty({
    description: 'Pitch angle (radians)',
    type: 'number',
    format: 'float',
  })
  @IsNotEmpty()
  @IsNumber()
  @Type(() => Number)
  omega: number;
  @ApiProperty({
    description: 'Heading angle (radians)',
    type: 'number',
    format: 'float',
  })
  @IsNotEmpty()
  @IsNumber()
  @Type(() => Number)
  phi: number;
  @ApiProperty({
    description: 'Roll angle (radians)',
    type: 'number',
    format: 'float',
  })
  @IsNotEmpty()
  @IsNumber()
  @Type(() => Number)
  kappa: number;
}
