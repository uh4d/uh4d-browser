import { Injectable, Logger } from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { nanoid } from 'nanoid';
import * as sharp from 'sharp';
import * as exifReader from 'exif-reader';
import { HttpService } from '@nestjs/axios';
import { CreateImageDto, CreateImageFileDto } from '@backend/generated/dto';
import { parseDate } from '@backend/utils/date-utils';

export type CreateRephotoData = CreateImageDto & {
  file: CreateImageFileDto;
  baseImageId: string;
};

interface OpenTopoDataResponse {
  results: {
    dataset: string;
    elevation: number;
    location: { lat: number; lng: number };
  }[];
  status: string;
}

@Injectable()
export class RephotoService {
  private readonly logger = new Logger(RephotoService.name);

  constructor(
    private readonly neo4j: Neo4jService,
    private readonly http: HttpService,
  ) {}

  queryRephotos(imageId: string) {
    // language=Cypher
    const q = `
      MATCH (queryImg:E38:UH4D {id: $imageId})
      CALL {
        WITH queryImg
        OPTIONAL MATCH (queryImg)-[:P67 { type: 'rephoto' }]->(img:E38)<-[:P94]-(e65:E65)
        RETURN img, 'rephoto-of' AS type
      UNION
        WITH queryImg
        OPTIONAL MATCH (queryImg)<-[:P67 { type: 'rephoto' }]-(img:E38)<-[:P94]-(e65:E65)
        RETURN img, 'has-rephoto' AS type
      }

      MATCH (img)<-[:P94]-(e65:E65),
            (img)<-[:P67]-(file:D9),
            (img)-[:P102]->(title:E35),
            (e65)-[:P7]->(:E53)-[:P168]->(geo:E94)
      OPTIONAL MATCH (e65)-[:P16]->(spatial:E22)
      OPTIONAL MATCH (e65)-[:P14]->(:E21)-[:P131]->(author:E82)
      OPTIONAL MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)

      RETURN img.id AS id,
        apoc.map.removeKey(file, 'id') AS file,
        title.value AS title,
        author.value AS author,
        date {.*, from: toString(date.from), to: toString(date.to)} AS date,
        apoc.map.removeKeys(apoc.map.merge(geo, spatial), ['id', 'method', 'point', 'status']) AS camera,
        geo.status AS spatialStatus,
        img.pending AS pending,
        img.pending AS needsValidation,
        img.declined AS declined,
        type
    `;
    // ToDo: remove needsValidation

    const params = {
      imageId,
    };

    return this.neo4j.read(q, params);
  }

  getById(imageId: string) {
    // language=Cypher
    const q = `
      MATCH (img:E38:UH4D {id: $imageId})<-[:P94]-(e65:E65),
            (image)<-[:P67]-(file:D9),
            (image)-[:P102]->(title:E35),
            (e65)-[:P7]->(:E53)-[:P168]->(geo:E94),
            (e65)-[:P16]->(spatial:E22),
            (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
      OPTIONAL MATCH (img)-[:P67 { type: 'rephoto' }]->(baseImg:E38)<-[:P67]-(file:D9)

      RETURN image.id AS id,
             apoc.map.removeKey(file, 'id') AS file,
             title.value AS title,
             date {.*, from: toString(date.from), to: toString(date.to)} AS date,
             apoc.map.removeKeys(apoc.map.merge(geo, spatial), ['id', 'method', 'point', 'status']) AS camera,
             geo.status AS spatialStatus,
             baseImg.id AS rephotoId
    `;

    const params = {
      imageId,
    };

    return this.neo4j.read(q, params);
  }

  async create(
    userId: string,
    data: CreateRephotoData,
  ): Promise<{ image: { id: string } }> {
    // language=Cypher
    const q = `
      MATCH (baseImg:E38:UH4D { id: $baseImageId }), (user:User:UH4D { id: $userId })
      CREATE (image:E38:UH4D { id: $imageId, pending: true }),
             (image)-[:P102]->(title:E35:UH4D $title),
             (image)<-[:P67]-(file:D9:UH4D $file),
             (image)-[:P48]->(identifier:E42:UH4D $identifier),
             (image)<-[:P94]-(e65:E65:UH4D {id: $e65id})-[:P7]->(place:E53:UH4D {id: $placeId})-[:P168]->(geo:E94:UH4D $geo),
             (image)-[:uploaded_by { timestamp: datetime() }]->(user),
             (e65)-[:P14]->(user),
             (e65)-[:P16]->(cam:E22:UH4D $camera),
             (e65)-[:P4]->(:E52:UH4D {id: $e52id})-[:P82]->(date:E61:UH4D {id: $date.id, value: $date.value, from: date($date.from), to: date($date.to), display: $date.display}),
             (image)-[:P67 { type: 'rephoto' }]->(baseImg)
      SET geo.point = point({latitude: $geo.latitude, longitude: $geo.longitude})

      FOREACH (tag IN $tags |
        MERGE (t:TAG:UH4D {id: tag})
        MERGE (image)-[:has_tag]->(t)
      )

      RETURN image
    `;

    const id = nanoid(9) + '_' + data.file.original;

    const params = {
      userId,
      baseImageId: data.baseImageId,
      imageId: id,
      e65id: 'e65_' + id,
      title: {
        id: 'e35_' + id,
        value: data.title,
      },
      identifier: {
        id: 'e42_' + id,
      },
      file: {
        id: 'd9_' + id,
        ...data.file,
      },
      e52id: 'e52_' + id,
      date: {
        id: 'e61_e52_' + id,
        ...parseDate(data.date.value),
      },
      placeId: 'e53_' + id,
      geo: {
        id: '94_' + id,
        latitude: data.camera.latitude,
        longitude: data.camera.longitude,
        altitude: data.camera.altitude,
        radius: data.camera.radius,
        status: 6,
      },
      camera: {
        id: 'camera_' + id,
        omega: data.camera.omega,
        phi: data.camera.phi,
        kappa: data.camera.kappa,
        ck: data.camera.ck,
        offset: data.camera.offset,
        k: data.camera.k || [0],
        method: 'rephoto',
      },
      tags: ['Rephoto'],
    };

    return (await this.neo4j.write(q, params))[0];
  }

  /**
   * Estimate camera distance based on EXIF data.
   * @param file Path to image file
   */
  async estimateCameraDistance(file: string): Promise<number> {
    const metadata = await sharp(file).metadata();
    if (!metadata.exif) return 1;

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const exif = exifReader(metadata.exif);

    this.logger.debug(exif.Photo);

    // https://en.wikipedia.org/wiki/Image_sensor_format
    if (exif.Photo.FocalLengthIn35mmFilm) {
      // full frame sensor (height: 24 mm)
      return exif.Photo.FocalLengthIn35mmFilm / 24;
    }
    if (exif.Photo.FocalLength) {
      // unknown sensor size -> assuming 1/3.2" sensor (height: 3.42 mm)
      return exif.Photo.FocalLength / 3.42;
    }

    return 1;
  }

  /**
   * Query [OpenTopoData](https://www.opentopodata.org/) to get an elevation/altitude for a location.
   */
  async getAltitudeFromLatLon(
    latitude: number,
    longitude: number,
  ): Promise<number> {
    const dataset = 'mapzen';
    const response = await this.http.axiosRef.get<OpenTopoDataResponse>(
      `https://api.opentopodata.org/v1/${dataset}`,
      {
        params: {
          locations: `${latitude},${longitude}`,
        },
        responseType: 'json',
      },
    );

    return response.data.results[0].elevation;
  }
}
