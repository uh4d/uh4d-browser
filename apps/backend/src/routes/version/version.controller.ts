import { Controller, Get, Logger } from '@nestjs/common';
import { ApiOkResponse, ApiOperation } from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { getPkgJson } from '@backend/utils/ref-file';
import { ConverterClientService } from '@backend/microservices/converter-client/converter-client.service';
import { VersionDto } from './dto/version.dto';

@Controller('version')
export class VersionController {
  private readonly logger = new Logger(VersionController.name);

  constructor(
    private readonly config: ConfigService,
    private readonly neo4j: Neo4jService,
    private readonly converterClient: ConverterClientService,
  ) {}

  @ApiOperation({
    summary: 'Print version',
    description:
      'Print version of this API and third-party applications, on which some operations rely on.',
  })
  @ApiOkResponse({
    type: VersionDto,
  })
  @Get()
  async printVersion(): Promise<VersionDto> {
    // Neo4j
    let neo4j = '';
    try {
      const neo4jServerInfo = await this.neo4j.getDriver().getServerInfo({
        database: this.config.get('NEO4J_DATABASE') || undefined,
      });
      neo4j = neo4jServerInfo.agent;
    } catch (e) {
      this.logger.error(e);
    }

    // Assimp, Collada2Gltf
    let assimp = '';
    let collada = '';
    try {
      const converterStatus = await this.converterClient.getStatus();
      assimp = converterStatus.Assimp;
      collada = converterStatus.Collada2Gltf;
    } catch (e) {
      this.logger.error(e);
    }

    return {
      API: `UH4D ${getPkgJson().version}`,
      Neo4j: neo4j || 'FAILED!',
      Assimp: assimp || 'FAILED!',
      Collada2Gltf: collada || 'FAILED!',
    };
  }
}
