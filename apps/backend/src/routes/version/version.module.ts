import { Module } from '@nestjs/common';
import { MicroservicesModule } from '@backend/microservices/microservices.module';
import { VersionController } from './version.controller';

@Module({
  imports: [MicroservicesModule],
  controllers: [VersionController],
})
export class VersionModule {}
