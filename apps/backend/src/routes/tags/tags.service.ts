import { Injectable } from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { escapeRegex } from '@uh4d/utils';
import { TagDto } from './dto/tag.dto';

@Injectable()
export class TagsService {
  constructor(private readonly neo4j: Neo4jService) {}

  async query(search = ''): Promise<TagDto[]> {
    // language=Cypher
    const q = `
      MATCH (tag:TAG:UH4D)
      WHERE tag.id =~ $search
      RETURN tag.id AS value`;

    const params = {
      search: `(?i).*${escapeRegex(search)}.*`,
    };

    return this.neo4j.read(q, params);
  }
}
