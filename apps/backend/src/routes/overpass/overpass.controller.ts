import {
  BadRequestException,
  Controller,
  Get,
  Query,
  Res,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';
import { join } from 'node:path';
import { pathExists, readJson, writeJson } from 'fs-extra';
import { Response } from 'express';
import { roundDecimalDegree } from '@backend/utils/round-decimal-degree';
import { TerrainService } from '@backend/routes/terrain/terrain.service';
import { OsmClientService } from '@backend/microservices/osm-client/osm-client.service';
import { AltitudesPayload } from '@uh4d/dto/interfaces/custom/osm';
import { OverpassDto } from '@uh4d/backend/overpass-api';
import { LocationQueryParams } from '@backend/routes/dto/location-query-params';
import { HeightsQueryParams } from './dto/heights-query-params';
import { OsmAltitudesDto } from './dto/osm-altitudes.dto';
import { OverpassService } from './overpass.service';

@ApiTags('OSM')
@Controller('overpass')
export class OverpassController {
  constructor(
    private readonly overpassService: OverpassService,
    private readonly terrainService: TerrainService,
    private readonly config: ConfigService,
    private readonly osmClient: OsmClientService,
  ) {}

  @ApiOperation({
    summary: 'Request OSM data',
    description:
      'Request building footprints from OpenStreetMap using Overpass API. The building footprints can be processed and extruded to 3D geometry.\n\nSince requesting Overpass API can take up to several seconds, the requested data is cached to serve data faster at future requests. For this reason, the `lat` + `lon` query parameters are rounded with an accuracy about 50 meters.\n\nQuery parameters `lat`, `lon`, `r` are required as search parameter.',
  })
  @ApiOkResponse({
    type: OverpassDto,
  })
  @ApiBadRequestResponse()
  @Get()
  async query(@Query() queryParams: LocationQueryParams): Promise<OverpassDto> {
    return this.overpassService.getOsmData(queryParams);
  }

  @ApiOperation({
    summary: 'Compute altitudes',
    description:
      'Compute altitudes for OSM buildings based on digital elevation model.',
  })
  @ApiOkResponse({
    type: OsmAltitudesDto,
  })
  @ApiBadRequestResponse()
  @Get('altitudes')
  async getAltitudes(
    @Query() queryParams: HeightsQueryParams,
    @Res() res: Response,
  ): Promise<void> {
    // round position
    const r = Math.round(queryParams.r);
    const lat = roundDecimalDegree(queryParams.lat, r / 10);
    const lon = roundDecimalDegree(queryParams.lon, r / 10);

    const cacheFile = join(
      this.config.get<string>('DIR_CACHE'),
      'overpass',
      `${queryParams.terrain.split('/').pop()}-${lat}-${lon}-${r}.json`,
    );

    // send cached file
    if (await pathExists(cacheFile)) {
      const data = await readJson(cacheFile);
      res.json(data);
      return;
    }

    // check osm file
    const osmPath = join(
      this.config.get<string>('DIR_CACHE'),
      'overpass',
      `${lat}-${lon}-${r}.json`,
    );

    if (!(await pathExists(osmPath)))
      throw new BadRequestException(
        "Path to cached Overpass data couldn't be found!",
      );

    // compute altitudes in microservice
    const threadData: AltitudesPayload = {
      lat,
      lon,
      osmPath,
      modelPath: '',
    };

    // if `terrain` is just an ID and not a path with slashes
    if (queryParams.terrain.split('/').length === 1) {
      // custom terrain
      const terrainData = await this.terrainService.getById(
        queryParams.terrain,
      );
      if (!terrainData) {
        throw new BadRequestException(
          `Terrain with ID ${queryParams.terrain} not found!`,
        );
      }

      threadData.terrainData = terrainData;
      threadData.modelPath = join(
        this.config.get<string>('DIR_DATA'),
        terrainData.file.path,
        terrainData.file.file,
      );
      threadData.isCustomTerrain = true;
    } else {
      // elevation api terrain
      const modelPath = join(
        this.config.get<string>('DIR_CACHE'),
        'elevation',
        queryParams.terrain.replace(/.+\/gltf\//, ''),
      );
      if (!(await pathExists(modelPath)))
        throw new BadRequestException("Path to model file couldn't be found!");

      threadData.modelPath = modelPath;
    }

    // run microservice task
    const altitudes = await this.osmClient.computeAltitudes(threadData);

    // send data
    res.json(altitudes);

    // write data to cache
    await writeJson(cacheFile, altitudes, { spaces: 2 });
  }
}
