import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { join } from 'node:path';
import { ensureDir, pathExists, readJson, writeJson } from 'fs-extra';
import { OverpassApiService, OverpassDto } from '@uh4d/backend/overpass-api';
import { roundDecimalDegree } from '@backend/utils/round-decimal-degree';
import { LocationQueryParams } from '@backend/routes/dto/location-query-params';

@Injectable()
export class OverpassService {
  constructor(
    private readonly config: ConfigService,
    private readonly overpassApiService: OverpassApiService,
  ) {}

  /**
   * Get OSM data either from cache or query from Overpass API.
   */
  async getOsmData(queryParams: LocationQueryParams): Promise<OverpassDto> {
    // round position
    const r = Math.round(queryParams.r);
    const lat = roundDecimalDegree(queryParams.lat, r / 10);
    const lon = roundDecimalDegree(queryParams.lon, r / 10);

    // check for cached data file
    const cacheFile = join(
      this.config.get<string>('DIR_CACHE'),
      'overpass',
      `${lat}-${lon}-${r}.json`,
    );
    const exists = await pathExists(cacheFile);

    if (exists) {
      // read file and send
      return await readJson(cacheFile);
    }

    // query Overpass API
    const data = await this.overpassApiService.query(lat, lon, queryParams.r);

    // cache data
    await ensureDir(join(this.config.get<string>('DIR_CACHE'), 'overpass'));
    await writeJson(cacheFile, data, { spaces: 2 });

    return data;
  }
}
