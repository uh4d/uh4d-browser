import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { rateLimitMiddleware } from '@backend/routes/rate-limit.middleware';
import { TerrainModule } from '@backend/routes/terrain/terrain.module';
import { MicroservicesModule } from '@backend/microservices/microservices.module';
import { OverpassApiModule } from '@uh4d/backend/overpass-api';
import { OverpassController } from './overpass.controller';
import { OverpassCacheMiddleware } from './overpass-cache.middleware';
import { OverpassService } from './overpass.service';

@Module({
  imports: [TerrainModule, MicroservicesModule, OverpassApiModule],
  controllers: [OverpassController],
  providers: [OverpassService],
  exports: [OverpassService],
})
export class OverpassModule implements NestModule {
  configure(consumer: MiddlewareConsumer): void {
    consumer
      .apply(OverpassCacheMiddleware, rateLimitMiddleware(1, 1000))
      .forRoutes({ path: 'overpass', method: RequestMethod.GET });
  }
}
