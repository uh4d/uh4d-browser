import { Controller, DefaultValuePipe, Get, Query } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AddressesService } from './addresses.service';
import { AddressDto } from './dto/address.dto';

@ApiTags('Metadata')
@Controller('addresses')
export class AddressesController {
  constructor(private readonly addressService: AddressesService) {}

  @ApiOperation({
    summary: 'Query addresses',
    description:
      'Query all addresses. Use `search` query parameter to filter results.',
  })
  @ApiOkResponse({
    type: AddressDto,
    isArray: true,
  })
  @Get()
  async query(
    @Query('search', new DefaultValuePipe('')) search: string,
  ): Promise<AddressDto[]> {
    return this.addressService.query(search);
  }
}
