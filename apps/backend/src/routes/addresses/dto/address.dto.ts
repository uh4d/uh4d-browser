import { ApiProperty } from '@nestjs/swagger';

export class AddressDto {
  @ApiProperty({
    description: 'Address ID',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Address value',
    required: false,
  })
  value: string;
}
