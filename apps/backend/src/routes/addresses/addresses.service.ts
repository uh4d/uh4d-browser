import { Injectable } from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { escapeRegex } from '@uh4d/utils';
import { AddressDto } from './dto/address.dto';

@Injectable()
export class AddressesService {
  constructor(private readonly neo4j: Neo4jService) {}

  async query(search = ''): Promise<AddressDto[]> {
    // language=Cypher
    const q = `
      MATCH (addr:E45:UH4D)
      WHERE addr.value =~ $search
      RETURN addr.id AS id, addr.value AS value`;

    const params = {
      search: `(?i).*${escapeRegex(search)}.*`,
    };

    return this.neo4j.read(q, params);
  }
}
