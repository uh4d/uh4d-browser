import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  FileTypeValidator,
  Get,
  HttpCode,
  HttpStatus,
  Logger,
  NotFoundException,
  Param,
  ParseFilePipe,
  Patch,
  Post,
  Query,
  Res,
  UnsupportedMediaTypeException,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiConsumes,
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
import { Express, Response } from 'express';
import * as fs from 'fs-extra';
import { join, parse } from 'node:path';
import { nanoid } from 'nanoid';
import * as mime from 'mime';
import { removeIllegalFileChars } from '@uh4d/utils';
import { parseBibTex } from '@backend/utils/bibtex';
import { BackendConfig } from '@backend/config';
import { Auth } from '@backend/auth/auth.decorator';
import { AuthUser, User } from '@backend/auth/user';
import { CommonService } from '@backend/common/common.service';
import { TextMediaService } from '@backend/upload/text-media.service';
import { UpdateValidationStatusDto } from '@backend/routes/dto/update-validation-status.dto';
import { TextAnnotationsService } from './text-annotations/text-annotations.service';
import {
  CreateImageDateDto,
  CreateTextDto,
  CreateTextFileDto,
  TextDto,
  UpdateTextDto,
} from '@backend/dto';
import { TextsService } from './texts.service';
import { GetTextsQueryParams } from './dto/get-texts-query-params';
import { UploadTextDto } from './dto/upload-text.dto';
import { UpdateBibtexDto } from './dto/update-bibtex.dto';
import { TextWithAnnotationsDto } from './dto/text-extended.dto';

@ApiTags('Texts')
@Controller('texts')
export class TextsController {
  private readonly logger = new Logger(TextsController.name);

  constructor(
    private readonly textService: TextsService,
    private readonly config: BackendConfig,
    private readonly textMediaService: TextMediaService,
    private readonly textAnnotationService: TextAnnotationsService,
    private readonly commonService: CommonService,
  ) {}

  @ApiOperation({
    summary: 'Query texts',
    description:
      'Query all texts. Query parameters may be set to filter the data.\n\n`lat`, `lon`, and `r` work only in combination and may be used to look for texts associated with an object (building) within a radius at a position.',
  })
  @ApiOkResponse({
    type: TextWithAnnotationsDto,
    isArray: true,
  })
  @Get()
  async query(
    @Query() queryParams: GetTextsQueryParams,
  ): Promise<TextWithAnnotationsDto[]> {
    return this.textService.query(queryParams);
  }

  @ApiOperation({
    summary: 'Get text',
    description: 'Get text by ID.',
  })
  @ApiOkResponse({
    type: TextDto,
  })
  @ApiNotFoundResponse()
  @Get(':textId')
  async getById(@Param('textId') textId: string): Promise<TextDto> {
    const text = await this.textService.getById(textId);

    if (!text) {
      throw new NotFoundException(`Text with ID ${textId} not found!`);
    }

    return text;
  }

  @ApiOperation({
    summary: 'Upload text',
    description: `
Upload text and metadata. Currently supported file formats are \`.pdf\` as well as \`.zip\` files optionally containing:

- \`.pdf\`
- plain text file \`.txt\`
- BibTex \`.bib\`
- annotation file \`.json\`
`,
  })
  @ApiConsumes('multipart/form-data')
  @ApiCreatedResponse({
    type: TextDto,
  })
  @ApiBadRequestResponse()
  @Auth('uploadTexts')
  @Post()
  @UseInterceptors(FileInterceptor('uploadText'))
  async upload(
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new FileTypeValidator({ fileType: /(pdf|zip|x-zip-compressed)$/ }),
        ],
      }),
    )
    file: Express.Multer.File,
    @Body() body: UploadTextDto,
    @User() user: AuthUser,
  ): Promise<TextDto> {
    const tmpDir = join(this.config.dirTmp, 'tmp', nanoid(9));
    const fileName = removeIllegalFileChars(file.originalname);
    const tmpFile = join(tmpDir, fileName);
    let finalPath: string;

    try {
      // create tmp folder
      await fs.ensureDir(tmpDir);
      // copy uploaded file to tmp folder
      await fs.rename(file.path, tmpFile);

      // generate media files
      let fileMeta: CreateTextFileDto;

      switch (mime.getType(fileName)) {
        case 'application/pdf':
          fileMeta = await this.textMediaService.generateTextMediaFromPdf(
            tmpFile,
          );
          break;
        case 'application/zip':
        case 'application/x-zip-compressed':
          fileMeta = await this.textMediaService.generateTextMediaFromZip(
            tmpFile,
          );
          break;
        default:
          throw new UnsupportedMediaTypeException();
      }

      let meta: CreateTextDto = {
        title: parse(file.originalname).name,
        objects: body.objects.map((objId) => ({ id: objId })),
        authors: [],
        file: fileMeta,
        tags: [],
      };

      // parse bibtex if any
      if (fileMeta.bib) {
        const bibtex = await fs.readFile(
          join(tmpDir, fileMeta.path, fileMeta.bib),
          'utf8',
        );
        try {
          const parsedMeta = parseBibTex(bibtex);
          meta = {
            ...meta,
            bibtex,
            ...parsedMeta,
            date: parsedMeta.date as unknown as CreateImageDateDto | undefined,
          };
        } catch (e) {
          throw new BadRequestException('Errors while parsing BibTex file!');
        }
      }

      this.logger.log(`Directory: ${fileMeta.path} File: ${fileMeta.original}`);

      // copy into texts folder
      await fs.copy(
        join(tmpDir, fileMeta.path),
        join(this.config.dirData, fileMeta.path),
      );

      finalPath = meta.file.path;

      // write to database
      const result = await this.textService.create(meta, user);
      if (!result) {
        throw new Error('No data written to database!');
      }

      // process annotation if any
      if (fileMeta.json) {
        try {
          await this.textAnnotationService.processAnnotationData(
            result.text.id,
          );
          this.textAnnotationService
            .triggerUpdateChain(result.text.id, 100)
            .then(() => {
              this.logger.log('Norm data and text annotation update complete.');
            })
            .catch((reason) => {
              this.logger.error(reason);
            });
        } catch (e) {
          // delete image if annotation processing failed
          await this.textService.remove(result.text.id);
          throw e;
        }
      }

      return this.textService.getById(result.text.id);
    } catch (e) {
      if (finalPath) {
        this.logger.warn('Unlink ' + finalPath);
        await fs.remove(join(this.config.dirData, finalPath));
      }
      throw e;
    } finally {
      // remove temporary folder
      await fs.remove(tmpDir);
    }
  }

  @ApiOperation({
    summary: 'Update text',
    description: 'Update text metadata by providing bibtex data.',
  })
  @ApiOkResponse({
    type: TextDto,
  })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse()
  @Auth()
  @Patch(':textId')
  async update(
    @Param('textId') textId: string,
    @Body() body: UpdateBibtexDto,
    @User() user: AuthUser,
  ): Promise<TextDto> {
    const data: UpdateTextDto = { bibtex: body.bibtex };

    try {
      // parse bibtex
      Object.assign(data, parseBibTex(body.bibtex));
    } catch (e) {
      throw new BadRequestException('Wrong bibtex format!', { cause: e });
    }

    const text = await this.textService.update(textId, data);

    if (!text) {
      throw new NotFoundException(`Text with ID ${textId} not found!`);
    }

    await this.commonService.updateEditedBy(user, textId);

    return this.textService.getById(textId);
  }

  @ApiOperation({
    summary: 'Delete text',
    description: 'Delete text and all associated files and annotations.',
  })
  @ApiNoContentResponse()
  @ApiNotFoundResponse()
  @HttpCode(HttpStatus.NO_CONTENT)
  @Auth()
  @Delete(':textId')
  async remove(
    @Param('textId') textId: string,
    @Res() res: Response,
  ): Promise<void> {
    const removed = await this.textService.remove(textId);

    if (!removed) {
      throw new NotFoundException(`Text with ID ${textId} not found!`);
    }

    res.send();
  }

  @ApiOperation({
    summary: 'Confirm/decline text',
    description: 'Set `pending` and/or `declined` flag.',
  })
  @ApiOkResponse({
    type: TextDto,
  })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse()
  @Auth('validateContent')
  @Patch(':textId/validation')
  async validateText(
    @Param('textId') textId: string,
    @Body() body: UpdateValidationStatusDto,
  ): Promise<TextDto> {
    const update = await this.commonService.updateValidationStatus(
      textId,
      body.pending,
      body.declined,
    );

    if (!update) {
      throw new NotFoundException(`Text with ID ${textId} not found!`);
    }

    return this.textService.getById(textId);
  }
}
