import { Injectable, Logger } from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { nanoid } from 'nanoid';
import * as fs from 'fs-extra';
import { join, parse } from 'node:path';
import { parseDate } from '@backend/utils/date-utils';
import { escapeRegex, removeIllegalFileChars } from '@uh4d/utils';
import { BackendConfig } from '@backend/config';
import { AuthUser } from '@backend/auth/user';
import { CreateTextDto, TextDto, UpdateTextDto } from '@backend/dto/text';
import { getSpatialParams } from '@backend/utils/spatial-params';
import { AnnotationsService } from '@backend/routes/annotations/annotations.service';
import { CombinedSimilarAnnotationsDto } from '@uh4d/dto/interfaces/custom/annotation';
import { GetTextsQueryParams } from './dto/get-texts-query-params';
import { TextWithAnnotationsDto } from './dto/text-extended.dto';

@Injectable()
export class TextsService {
  private readonly logger = new Logger(TextsService.name);

  constructor(
    private readonly neo4j: Neo4jService,
    private readonly config: BackendConfig,
    private readonly annotationsService: AnnotationsService,
  ) {}

  async query(
    query: GetTextsQueryParams,
    userId?: string,
  ): Promise<TextWithAnnotationsDto[]> {
    const stringFilter = [];
    const objFilter = [];
    const annFilter = [];
    const regexPosList = [];
    const regexNegList = [];
    const regexAuthorList = [];

    let pending = false;
    let declined = false;

    // first triage
    (query.q ? (Array.isArray(query.q) ? query.q : [query.q]) : []).forEach(
      (value) => {
        if (/^obj:/.test(value)) {
          objFilter.push(value.replace(/^obj:/, ''));
        } else if (/^ann:/.test(value)) {
          annFilter.push(value.replace(/^ann:/, ''));
        } else if (value === 'pending') {
          pending = true;
        } else if (value === 'declined') {
          declined = true;
        } else {
          stringFilter.push(value);
        }
      },
    );

    const spatialParams = getSpatialParams(query);

    let q = '';

    if (spatialParams) {
      q += `WITH point({latitude: $geo.latitude, longitude: $geo.longitude}) AS userPoint
        MATCH (text:E33:UH4D)-[:P67]->(:E22)<-[:P157]-(:E53)-[:P168]->(geo:E94)
      `;
    } else {
      q += 'MATCH (text:E33:UH4D) ';
    }

    q += `
      WHERE coalesce(text.pending, FALSE) = $pending
      AND coalesce(text.declined, FALSE) = $declined
    `;

    if (spatialParams) {
      q += `AND distance(geo.point, userPoint) < $geo.radius `;
    }
    if (userId) {
      q += `AND (text)-[:uploaded_by]->(:User:UH4D {id: $userId}) `;
    }

    // language=Cypher
    q += `
      MATCH (text)<-[:P94]-(e65:E65),
            (text)-[:P102]->(title:E35),
            (text)-[:P48]->(identifier:E42),
            (text)<-[:P67]-(file:D9)
      OPTIONAL MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
      OPTIONAL MATCH (e65)-[:P14]->(person:E21)-[:P131]->(personName:E82)

      WITH text, title, identifier, file, date, collect({ id: person.id, value: personName.value }) AS authors
      ${
        objFilter.length
          ? 'MATCH (text)-[:P67]->(obj:E22)-[:P1]->(objName:E41) WHERE obj.id IN $objList'
          : 'OPTIONAL MATCH (text)-[:P67]->(obj:E22)-[:P1]->(objName:E41)'
      }
      OPTIONAL MATCH (obj)<-[:P157]-(:E53)-[:P168]->(objGeo:E94)

      WITH text, title, identifier, file, date, authors, collect(obj{ .id, name: objName.value, location: apoc.map.removeKeys(objGeo, ['id', 'point']) }) AS objects
      OPTIONAL MATCH (text)-[:has_tag]->(tag:TAG)
      WITH text, title, identifier, file, date, authors, objects, collect(tag.id) AS tags
      CALL {
        WITH text
        OPTIONAL MATCH (text)-[:P106]->(ann:Annotation)-[:P2]->(annType:Wikidata)
        RETURN annType
        UNION
        WITH text
        OPTIONAL MATCH (text)-[:P106]->(ann:Annotation)-[:P2]->(annType:AAT)
        RETURN annType
      }
      WITH text, title, identifier, file, date, authors, objects, tags, collect(annType) as annTypes
    `;

    stringFilter.forEach((value, index) => {
      const qPrefix = index === 0 ? ' WHERE' : ' AND';

      if (/^author:/.test(value)) {
        if (regexAuthorList.length < 1) {
          q +=
            qPrefix +
            ` any(e IN $regexAuthorList
                WHERE any(
                  person IN authors WHERE person.id = e
                  OR person.value =~ "(?i).*" + e + ".*"
                )
              )`;
        }
        regexAuthorList.push(escapeRegex(value.replace(/^author:/, '')));
      } else if (/^-/.test(value)) {
        if (regexNegList.length < 1) {
          q +=
            qPrefix +
            ` none(e IN $regexNegList
                WHERE coalesce(title.value, "") =~ e
                OR any(person IN authors WHERE coalesce(person.value, "") =~ e)
                OR any(obj IN objects WHERE coalesce(obj.name, "") =~ e)
                OR any(tag IN tags WHERE tag =~ e)
                OR any(ann IN annTypes WHERE ann.label =~ e)
              )`;
        }
        regexNegList.push(
          `(?i).*${escapeRegex(value.replace(/^-/, '').replace(/"/g, ''))}.*`,
        );
      } else {
        if (regexPosList.length < 1) {
          q +=
            qPrefix +
            ` all(e IN $regexPosList
                WHERE title.value =~ e
                OR any(person IN authors WHERE person.value =~ e)
                OR any(obj IN objects WHERE obj.name =~ e)
                OR any(tag IN tags WHERE tag =~ e)
                OR any(ann IN annTypes WHERE ann.label =~ e)
              )`;
        }
        regexPosList.push(`(?i).*${escapeRegex(value.replace(/"/g, ''))}.*`);
      }
    });

    // language=Cypher
    q += `
      OPTIONAL MATCH (text)-[userEventU:uploaded_by]->(userU:User)-[:P131]->(usernameU:E82)
      OPTIONAL MATCH (text)-[userEventE:edited_by]->(userE:User)-[:P131]->(usernameE:E82)

      RETURN text.id AS id,
             title.value AS title,
             [a IN authors WHERE a.value IS NOT NULL | a.value] as authors,
             apoc.map.removeKey(date, 'id') AS date,
             identifier.bibtex AS bibtex,
             identifier.doi AS doi,
             apoc.map.removeKey(file, 'id') AS file,
             objects,
             tags,
             size(annTypes) > 0 AS annotationsAvailable,
             text.pending AS pending,
             text.declined AS declined,
             userU{userId: userU.id, username: usernameU.value, timestamp: userEventU.timestamp} AS uploadedBy,
             userE{userId: userE.id, username: usernameE.value, timestamp: userEventE.timestamp} AS editedBy
    `;

    const params = {
      userId,
      regexPosList,
      regexNegList,
      regexAuthorList,
      objList: objFilter,
      geo: spatialParams,
      pending,
      declined,
    };

    // run Neo4j query
    let results = await this.neo4j.read<TextDto>(q, params);

    // query annotations and filter results
    const annotations: CombinedSimilarAnnotationsDto[] = [];
    if (annFilter.length) {
      annotations.push(
        ...(await this.annotationsService.querySimilarTextsByAnnotations(
          annFilter,
        )),
      );
      results = results.filter((text) =>
        annotations.find((ann) => ann.id === text.id),
      );
    }

    // enhance data with annotation query results
    return results.map((text) => {
      const record = text as TextWithAnnotationsDto;

      if (annFilter.length) {
        const annotation = annotations.find((ann) => ann.id === text.id);
        record.annotations = Object.entries(
          annotation.annotationRecords,
        ).reduce((map, [annId, annRecord]) => {
          annRecord.annotations.sort((a, b) => b.similarity - a.similarity);
          map[annId] = annRecord.annotations;
          return map;
        }, {});
        record.relevance = annotation.combinedSimilarity;
      }

      return record;
    });
  }

  async getById(textId: string): Promise<TextDto> {
    // language=Cypher
    const q = `
      MATCH (text:E33:UH4D {id: $id})<-[:P94]-(e65:E65),
            (text)-[:P102]->(title:E35),
            (text)-[:P48]->(identifier:E42),
            (text)<-[:P67]-(file:D9)
      OPTIONAL MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
      OPTIONAL MATCH (e65)-[:P14]->(:E21)-[:P131]->(person:E82)

      WITH text, title, identifier, file, date, collect(person.value) AS authors
      OPTIONAL MATCH (text)-[:P67]->(obj:E22)-[:P1]->(objName:E41),
                     (obj)<-[:P157]-(:E53)-[:P168]->(objGeo:E94)

      WITH text, title, identifier, file, date, authors,
           collect(obj{ .id, name: objName.value, location: apoc.map.removeKeys(objGeo, ['id', 'point']) }) AS objects
      OPTIONAL MATCH (text)-[userEventU:uploaded_by]->(userU:User)-[:P131]->(usernameU:E82)
      OPTIONAL MATCH (text)-[userEventE:edited_by]->(userE:User)-[:P131]->(usernameE:E82)
      OPTIONAL MATCH (text)-[:has_tag]->(tag:TAG)

      WITH text, title, identifier, file, date, authors, objects,
           userU{userId: userU.id, username: usernameU.value, timestamp: userEventU.timestamp} AS uploadedBy,
           userE{userId: userE.id, username: usernameE.value, timestamp: userEventE.timestamp} AS editedBy,
           collect(tag.id) AS tags
      CALL {
        WITH text
        OPTIONAL MATCH (text)-[:P106]->(ann:Annotation)-[:P2]->(annType:Wikidata)
        RETURN annType
        UNION
        WITH text
        OPTIONAL MATCH (text)-[:P106]->(ann:Annotation)-[:P2]->(annType:AAT)
        RETURN annType
      }

      RETURN text.id AS id,
             title.value AS title,
             authors,
             apoc.map.removeKey(date, 'id') AS date,
             identifier.bibtex AS bibtex,
             identifier.doi AS doi,
             apoc.map.removeKey(file, 'id') AS file,
             objects,
             tags,
             count(annType) > 0 AS annotationsAvailable,
             text.pending AS pending,
             text.declined AS declined,
             uploadedBy,
             editedBy
    `;

    const params = {
      id: textId,
    };

    const results = await this.neo4j.read(q, params);

    return results[0];
  }

  async create(
    data: CreateTextDto,
    user: AuthUser,
  ): Promise<{ text: { id: string } } | undefined> {
    // language=Cypher
    const q = `
      MATCH (user:User:UH4D { id: $userId })
      OPTIONAL MATCH (obj:E22:UH4D)
      WHERE obj.id IN $objects
      WITH user, collect(obj) AS objects

      CREATE (text:E33:UH4D {id: $textId})<-[:P94]-(e65:E65:UH4D {id: $e65id}),
             (text)-[:P102]->(title:E35:UH4D $title),
             (text)-[:P48]->(identifier:E42:UH4D $identifier),
             (text)<-[:P67]-(file:D9:UH4D $file),
             (text)-[:uploaded_by { timestamp: datetime() }]->(user)

      FOREACH (authorMap IN $authors |
        MERGE (author:E21:UH4D)-[:P131]->(authorName:E82:UH4D {value: authorMap.value})
          ON CREATE SET author.id = authorMap.personId, authorName.id = authorMap.nameId
        MERGE (e65)-[:P14]->(author)
      )

      FOREACH (obj IN objects |
        MERGE (text)-[:P67]->(obj)
      )

      FOREACH (tag IN $tags |
        MERGE (t:TAG:UH4D {id: tag})
        MERGE (text)-[:has_tag]->(t)
      )

      WITH text, e65
      CALL apoc.do.when($date IS NOT NULL,
        'CREATE (e65)-[:P4]->(:E52:UH4D {id: e52id})-[:P82]->(date:E61:UH4D {id: dateMap.id, value: dateMap.value, from: date(dateMap.from), to: date(dateMap.to), display: dateMap.display}) RETURN date',
        'RETURN NULL AS date',
        { e65: e65, e52id: $e52id, dateMap: $date }
      ) YIELD value
      WITH text
      CALL apoc.do.when($pending, 'SET text.pending = TRUE RETURN text', '', { text: text }) YIELD value

      RETURN text
    `;

    const id = nanoid(9) + '_' + parse(data.file.original).name;
    const date = parseDate(data.date?.value);

    const params = {
      userId: user.id,
      pending: user.needsValidation(),
      textId: id,
      e65id: 'e65_' + id,
      title: {
        id: 'e35_' + id,
        value: data.title,
      },
      identifier: {
        id: 'e42_' + id,
        doi: data.doi,
        bibtex: data.bibtex,
      },
      file: { id: 'd9_' + id, ...data.file },
      e52id: 'e52_' + id,
      date: { id: 'e61_e52_' + id, ...date },
      authors: data.authors.map((name) => {
        const authorId = `${nanoid(9)}_${removeIllegalFileChars(name, true)}`;
        return {
          personId: 'e21_' + authorId,
          nameId: 'e82_' + authorId,
          value: name,
        };
      }),
      objects: data.objects.map((obj) => obj.id) || [],
      tags: data.tags || [],
    };

    return (await this.neo4j.write(q, params))[0];
  }

  async update(
    textId: string,
    data: UpdateTextDto,
  ): Promise<{ text: { id: string } } | undefined> {
    // language=Cypher
    const q = `
      MATCH (text:E33:UH4D {id: $id})<-[:P94]-(e65:E65),
            (text)-[:P102]->(title:E35),
            (text)-[:P48]->(identifier:E42)

      SET title.value = $title,
          identifier += $identifier

      WITH text, e65
      CALL {
        WITH e65
        OPTIONAL MATCH (e65)-[r:P14]->(:E21)-[:P131]->(person:E82)
        WHERE none(author IN $authors WHERE author.value = person.value)
        DELETE r
        RETURN 1
      }

      FOREACH (authorMap IN $authors |
        MERGE (author:E21:UH4D)-[:P131]->(authorName:E82:UH4D {value: authorMap.value})
          ON CREATE SET author.id = authorMap.personId, authorName.id = authorMap.nameId
        MERGE (e65)-[:P14]->(author)
      )

      WITH text, e65
      OPTIONAL MATCH (e65)-[:P4]->(e52:E52)-[:P82]->(date:E61)
      CALL apoc.do.when(date IS NOT NULL AND $date IS NULL,
        'DETACH DELETE e52, date RETURN NULL as date',
        'RETURN date',
        { e52: e52, date: date }
      ) YIELD value AS value1

      CALL apoc.do.when($date IS NOT NULL,
        'MERGE (e65)-[:P4]->(e52:E52:UH4D)-[:P82]->(date:E61:UH4D) ON CREATE SET e52.id = e52id, date.id = dateMap.id SET date.value = dateMap.value, date.from = date(dateMap.from), date.to = date(dateMap.to), date.display = dateMap.display RETURN date',
        'RETURN NULL AS date',
        { e65: e65, e52id: $e52id, dateMap: $date }
      ) YIELD value AS value2

      RETURN text
    `;

    const params = {
      id: textId,
      title: data.title,
      identifier: {
        bibtex: data.bibtex,
        doi: data.doi,
      },
      e52id: 'e52_' + textId,
      date: { id: 'e61_e52_' + textId, ...data.date },
      authors: data.authors.map((name) => {
        const authorId = `${nanoid(9)}_${removeIllegalFileChars(name, true)}`;
        return {
          personId: 'e21_' + authorId,
          nameId: 'e82_' + authorId,
          value: name,
        };
      }),
    };

    return (await this.neo4j.write(q, params))[0];
  }

  async remove(textId: string): Promise<boolean> {
    // language=Cypher
    const q = `
      MATCH (text:E33:UH4D {id: $id})<-[:P94]-(e65:E65),
            (text)-[:P102]->(title:E35),
            (text)-[:P48]->(identifier:E42),
            (text)<-[:P67]-(file:D9)
      OPTIONAL MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
      OPTIONAL MATCH (text)-[:P106]->(ann:Annotation)

      WITH text, e65, title, identifier, file, date, ann, file.path AS path
      DETACH DELETE text, e65, title, identifier, file, date, ann

      RETURN path
    `;

    const params = {
      id: textId,
    };

    // wrap in custom transaction
    const session = this.neo4j.getWriteSession();
    const txc = session.beginTransaction();

    try {
      // execute cypher query
      const result = await txc.run(q, params);
      const records = Neo4jService.extractRecords<{
        path: string;
      }>(result.records);

      if (!records[0]) {
        await txc.rollback();
        return false;
      }

      // check for corrupted path to not remove wrong directory
      const relPath = records[0].path;
      if (!/^texts\/\S+$/.test(relPath)) {
        throw new Error('Path to delete does not match pattern!');
      }

      // remove files
      this.logger.log('Remove: ' + relPath);
      await fs.remove(join(this.config.dirData, relPath));

      await txc.commit();
      return true;
    } catch (e) {
      await txc.rollback();
      throw e;
    } finally {
      await session.close();
    }
  }
}
