import { Test, TestingModule } from '@nestjs/testing';
import { TextAnnotationsService } from './text-annotations.service';

describe('TextAnnotationsService', () => {
  let service: TextAnnotationsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TextAnnotationsService],
    }).compile();

    service = module.get<TextAnnotationsService>(TextAnnotationsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
