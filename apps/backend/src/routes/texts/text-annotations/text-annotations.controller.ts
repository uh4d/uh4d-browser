import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import {
  ApiBody,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { TextAnnotationDto } from '@backend/generated/dto';
import { TextAnnotationsService } from './text-annotations.service';
import { TextAnnotationComputeWeightsDto } from './dto/text-annotation-compute-weights.dto';

@ApiTags('Annotations')
@Controller('texts')
export class TextAnnotationsController {
  constructor(private readonly textAnnotationService: TextAnnotationsService) {}

  @ApiOperation({
    summary: 'Get annotations of text',
    description:
      'Get all annotations of an text sorted by `start` position. The `start` and `end` positions refer to the uploaded text.',
  })
  @ApiOkResponse({
    type: TextAnnotationDto,
    isArray: true,
  })
  @Get(':textId/annotations')
  queryAnnotationsByText(@Param('textId') textId: string) {
    return this.textAnnotationService.queryAnnotationsByText(textId);
  }

  @ApiOperation({
    summary: 'Compute weights (text)',
    description:
      'Compute spatial weights between annotations of a single text and semantic weights between each annotation and identifier nodes.\n\nThese tasks will be called automatically when uploading/processing annotation data of a text.',
  })
  @ApiBody({
    type: TextAnnotationComputeWeightsDto,
    required: false,
  })
  @ApiCreatedResponse()
  @Post(':textId/annotations/weights')
  computeWeights(
    @Param('textId') textId: string,
    @Body() body: TextAnnotationComputeWeightsDto,
  ) {
    return this.textAnnotationService.triggerUpdateChain(
      textId,
      body.maxDistanceThreshold,
      false,
    );
  }

  @ApiOperation({
    summary: 'Compute weights (all texts)',
    description:
      'For all texts with annotations, compute spatial weights between annotations of each single text and semantic weights between each annotation and identifier nodes.',
  })
  @ApiBody({
    type: TextAnnotationComputeWeightsDto,
    required: false,
  })
  @ApiCreatedResponse()
  @Post('annotations/weights')
  async computeWeightsAll(@Body() body: TextAnnotationComputeWeightsDto) {
    const texts = await this.textAnnotationService.getAllTextsWithAnnotations();

    await this.textAnnotationService.triggerUpdateChain(
      texts.map((text) => text.id),
      body.maxDistanceThreshold,
      false,
    );

    return texts;
  }
}
