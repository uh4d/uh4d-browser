import {
  BadRequestException,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { nanoid } from 'nanoid';
import { readFile, readJson } from 'fs-extra';
import { join } from 'node:path';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { TextAnnotationDto, TextFileDto } from '@backend/generated/dto';
import { NormDataService } from '@backend/routes/normdata/norm-data.service';
import { AnnotationsService } from '@backend/routes/annotations/annotations.service';
import { RawTextAnnotation } from './dto/raw-text-annotion.dto';

interface AnnotationWeight {
  annotationId: string;
  distance: number;
  weight: number;
}

interface TextWordWithAnnotation {
  text: string;
  data?: TextAnnotationDto;
  weights?: AnnotationWeight[];
}

interface WeightedRelation {
  sourceId: string;
  targetId: string;
  distance: number;
  weight: number;
}

@Injectable()
export class TextAnnotationsService {
  private readonly logger = new Logger(TextAnnotationsService.name);

  constructor(
    private readonly neo4j: Neo4jService,
    private readonly config: ConfigService,
    private readonly normDataService: NormDataService,
    private readonly annotationsService: AnnotationsService,
  ) {}

  /**
   * Get all annotations of a text, including Wikidata and AAT identifiers as well as neighbors.
   */
  async queryAnnotationsByText(textId: string): Promise<TextAnnotationDto[]> {
    // language=Cypher
    const q = `
      MATCH (text:E33:UH4D {id: $id})-[:P106]->(ann:Annotation)
      OPTIONAL MATCH (ann)-[aatRel:P2]->(aat:AAT)
        WHERE aatRel.confidence IS NOT NULL
      WITH ann, collect(aat{.id, .identifier, .label, weight: aatRel.weight}) AS aat
      OPTIONAL MATCH (ann)-[wikiRel:P2]->(wd:Wikidata)
        WHERE wikiRel.confidence IS NOT NULL
      WITH ann, aat, collect(wd{.id, .identifier, .label, weight: wikiRel.weight}) AS wikidata
      OPTIONAL MATCH (ann)-[nRel:is_close_to]-(neighbor:Annotation)
      RETURN ann.id AS id,
             ann.value AS value,
             ann.start AS start,
             ann.end AS end,
             aat,
             wikidata,
             collect(neighbor{.id, distance: nRel.distance, weight: nRel.weight}) AS neighbors
      ORDER BY start
    `;
    const params = {
      id: textId,
    };

    return this.neo4j.read(q, params);
  }

  /**
   * Query all texts that have annotations.
   */
  getAllTextsWithAnnotations(): Promise<{ id: string }[]> {
    // language=Cypher
    const q = `
      MATCH (text:E33:UH4D)
      WHERE (text)-[:P106]->(:Annotation)
      RETURN text.id AS id
    `;

    return this.neo4j.read<{ id: string }>(q);
  }

  /**
   * Get file reference information of text.
   */
  private async getTextFileData(textId: string): Promise<TextFileDto> {
    // language=Cypher
    const q = `
      MATCH (text:E33:UH4D {id: $id})<-[:P67]-(file:D9)
      RETURN text.id AS id, file
    `;

    const results = await this.neo4j.read<{ id: string; file: TextFileDto }>(
      q,
      { id: textId },
    );

    if (results[0]) {
      return results[0].file;
    } else {
      throw new NotFoundException(`Text with ID ${textId} not found!`);
    }
  }

  /**
   * Process text segmentation JSON and store annotations.
   */
  async processAnnotationData(textId: string) {
    // get image file data
    const textFileData = await this.getTextFileData(textId);

    // read and validate json file
    if (!textFileData.json) {
      return 'No JSON file with text annotation stored';
    }

    const jsonData: any[] = await readJson(
      join(this.config.get('DIR_DATA'), textFileData.path, textFileData.json),
    );

    const rawAnnotations = jsonData.map((d) =>
      plainToInstance(RawTextAnnotation, d),
    );
    const promises = rawAnnotations.map((d) => validate(d));
    const validationErrors = (await Promise.all(promises)).flat();
    if (validationErrors.length > 0) {
      throw new BadRequestException(
        'JSON annotation file has wrong format\n' +
          JSON.stringify(validationErrors, undefined, 2),
      );
    }

    return this.saveAnnotationDataForText(textId, rawAnnotations);
  }

  /**
   * Save annotation data for a single text.
   */
  private async saveAnnotationDataForText(
    textId: string,
    annotationData: RawTextAnnotation[],
  ): Promise<void> {
    // prepare annotations
    const preparedAnnotations = annotationData.map((a) => {
      const aatIds: { identifier: string; confidence: number }[] = [];
      const wikiIds: { identifier: string; confidence: number }[] = [];
      a.labels.forEach((label) => {
        if (label.aat_id) {
          aatIds.push({
            identifier: label.aat_id.toString(),
            confidence: label.confidence,
          });
        }
        if (label.wd_id) {
          wikiIds.push({
            identifier: label.wd_id,
            confidence: label.confidence,
          });
        }
      });

      return {
        id: 'annotation_text_' + nanoid(9),
        value: a.found_term,
        start: a.start,
        end: a.end,
        aatIds,
        wikiIds,
      };
    });

    // language=Cypher
    const q = `
      MATCH (text:E33:UH4D {id: $id})
      CALL {
        WITH text
        OPTIONAL MATCH (text)-[:P106]->(oldAnn:Annotation)
        DETACH DELETE oldAnn
      }

      UNWIND $annotations AS a
      MERGE (ann:E33:UH4D:Annotation {id: a.id, value: a.value, start: a.start, end: a.end})
      MERGE (text)-[:P106]->(ann)

      FOREACH (aat IN a.aatIds |
        MERGE (aatType:E55:UH4D:AAT {id: "aat:" + aat.identifier, identifier: aat.identifier})
        MERGE (ann)-[aatRel:P2]->(aatType)
        SET aatRel.confidence = aat.confidence
      )
      FOREACH (wiki IN a.wikiIds |
        MERGE (wikiType:E55:UH4D:Wikidata {id: "wiki:" + wiki.identifier, identifier: wiki.identifier})
        MERGE (ann)-[wikiRel:P2]->(wikiType)
        SET wikiRel.confidence = wiki.confidence
      )

      RETURN DISTINCT text
    `;

    const params = {
      id: textId,
      annotations: preparedAnnotations,
    };

    const results = await this.neo4j.write(q, params);
    if (!results[0]?.text) {
      throw new Error('Write query did not return any results');
    }
  }

  /**
   * Compute distance between annotations of a text and save weighted relationships.
   */
  async computeSpatialNeighborWeights(
    textId: string,
    maxDistanceThreshold = 100,
  ) {
    const annotations = await this.queryAnnotationsByText(textId);
    if (annotations.length === 0) return;

    // load text file
    const textFileData = await this.getTextFileData(textId);
    if (!textFileData.txt) throw new Error('No txt file available!');

    const plainText = await readFile(
      join(this.config.get('DIR_DATA'), textFileData.path, textFileData.txt),
      'utf8',
    );

    // compose text and annotations
    const annotatedText: TextWordWithAnnotation[] = [];
    let textIndex = 0;

    annotations.forEach((ann) => {
      if (textIndex < ann.start) {
        const words = plainText
          .slice(textIndex, ann.start)
          .split(/\s+/)
          .filter((w) => w !== '')
          .map((w) => ({ text: w }));
        annotatedText.push(...words);
      }
      annotatedText.push({
        text: plainText.slice(ann.start, ann.end),
        data: ann,
      });
      textIndex = ann.end;
    });

    if (textIndex < plainText.length) {
      const words = plainText
        .slice(textIndex)
        .split(/\s+/)
        .filter((w) => w !== '')
        .map((w) => ({ text: w }));
      annotatedText.push(...words);
    }

    // compute distance between annotations
    annotatedText.forEach((value, index, array) => {
      if (!value.data) return;

      const weights: AnnotationWeight[] = [];

      // only look in one direction from word, otherwise distances would be computed twice
      for (let i = 1, l = maxDistanceThreshold + 1; i < l; i++) {
        if (index - i < 0) continue;
        const word = array[index - i];
        if (word.data) {
          const distance = i - 1;
          weights.push({
            annotationId: word.data.id,
            distance,
            weight: 1 - distance / (maxDistanceThreshold + 1),
          });
        }
      }

      value.weights = weights;
    });

    // save weighted relationships
    const weightedRelations: WeightedRelation[] = [];
    annotatedText.forEach((value) => {
      if (!value.data) return;
      value.weights?.forEach((w) => {
        weightedRelations.push({
          sourceId: value.data.id,
          targetId: w.annotationId,
          distance: w.distance,
          weight: w.weight,
        });
      });
    });

    // language=Cypher
    const q = `
      MATCH (text:E33:UH4D {id: $textId})
      CALL {
        WITH text
        MATCH (text)-[:P106]->(ann:Annotation)
        OPTIONAL MATCH (ann)-[oldRel:is_close_to]-(:Annotation)
        DELETE oldRel
      }

      UNWIND $weights AS w
      MATCH (text)-[:P106]->(source:Annotation:UH4D {id: w.sourceId}),
            (text)-[:P106]->(target:Annotation:UH4D {id: w.targetId})
      MERGE (source)-[rel:is_close_to]->(target)
      SET rel.distance = w.distance, rel.weight = w.weight

      RETURN source, target, rel AS weight
    `;

    const params = {
      textId,
      weights: weightedRelations,
    };

    const results = await this.neo4j.write(q, params);
    return results.map((r) => ({
      sourceId: r.source.id,
      targetId: r.target.id,
      weight: r.weight,
    }));
  }

  /**
   * Update identifier nodes and compute weighted relationships
   * between spatial and semantic neighbors.
   */
  async triggerUpdateChain(
    textIds: string | string[],
    maxDistanceThreshold?: number,
    updateNormData = true,
  ): Promise<void> {
    if (updateNormData) {
      await this.normDataService.updateAll();
    }

    const ids = Array.isArray(textIds) ? textIds : [textIds];
    for (const id of ids) {
      this.logger.debug(`Compute weights for text ${id}`);

      await this.computeSpatialNeighborWeights(id, maxDistanceThreshold);

      const annotations = await this.queryAnnotationsByText(id);
      for (const ann of annotations) {
        await this.annotationsService.computeSemanticNeighborWeights(ann.id);
      }
    }

    await this.annotationsService.computeEmbeddings();
  }
}
