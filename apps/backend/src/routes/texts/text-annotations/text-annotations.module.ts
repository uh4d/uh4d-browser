import { Module } from '@nestjs/common';
import { AnnotationsModule } from '@backend/routes/annotations/annotations.module';
import { NormDataModule } from '@backend/routes/normdata/norm-data.module';
import { TextAnnotationsController } from './text-annotations.controller';
import { TextAnnotationsService } from './text-annotations.service';

@Module({
  imports: [AnnotationsModule, NormDataModule],
  controllers: [TextAnnotationsController],
  providers: [TextAnnotationsService],
  exports: [TextAnnotationsService],
})
export class TextAnnotationsModule {}
