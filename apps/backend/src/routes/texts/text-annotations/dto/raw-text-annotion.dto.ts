import {
  IsArray,
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  Max,
  Min,
  MinLength,
  Validate,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { IsNumberOrString } from '@uh4d/backend/utils';

export class RawTextAnnotation {
  @IsNotEmpty()
  @IsInt()
  start: number;
  @IsNotEmpty()
  @IsInt()
  end: number;
  @IsNotEmpty()
  @IsString()
  found_term: string;
  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => RawTextAnnotationLabel)
  labels: RawTextAnnotationLabel[];
}

export class RawTextAnnotationLabel {
  @IsNotEmpty()
  @IsString()
  search_term: string;
  @IsOptional()
  @IsString()
  @MinLength(1)
  wd_id?: string;
  @IsOptional()
  @Validate(IsNumberOrString)
  aat_id?: string | number;
  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  @Max(1)
  confidence: number;
}
