import { IsNotEmpty, IsNumber, IsPositive, ValidateIf } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class TextAnnotationComputeWeightsDto {
  @ApiProperty({
    description: 'Maximum word distance',
    required: false,
    default: 100,
  })
  @ValidateIf((_object, value) => value !== undefined)
  @IsNotEmpty()
  @IsNumber()
  @IsPositive()
  maxDistanceThreshold?: number;
}
