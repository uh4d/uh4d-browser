import { Test, TestingModule } from '@nestjs/testing';
import { TextAnnotationsController } from './text-annotations.controller';

describe('TextAnnotationsController', () => {
  let controller: TextAnnotationsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TextAnnotationsController],
    }).compile();

    controller = module.get<TextAnnotationsController>(
      TextAnnotationsController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
