import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class UpdateBibtexDto {
  @ApiProperty({
    description: 'File reference information in bibtex format',
  })
  @IsNotEmpty()
  @IsString()
  bibtex: string;
}
