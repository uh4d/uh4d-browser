import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsOptional } from 'class-validator';
import { LocationQueryParams } from '@backend/routes/dto/location-query-params';

export class GetTextsQueryParams extends LocationQueryParams {
  @ApiProperty({
    description:
      'Query string to filter results (looks for title, author, tags). The query string can be prefixed to search more precisely. Currently supported prefixes are: `obj:`, `author:`. A simple dash `-` will exclude query from results.',
    required: false,
  })
  @IsOptional()
  q?: string;

  @ApiProperty({
    description: 'Time-span minimum value',
    required: false,
  })
  @IsOptional()
  @IsDateString()
  from?: string;

  @ApiProperty({
    description: 'Time-span maximum value',
    required: false,
  })
  @IsOptional()
  @IsDateString()
  to?: string;
}
