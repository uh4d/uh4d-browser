import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNotEmpty, IsString } from 'class-validator';

export class UploadTextDto {
  @ApiProperty({
    type: 'string',
    format: 'binary',
  })
  uploadText: any;

  @ApiProperty({
    description: 'List of object IDs the text relates to',
  })
  @IsNotEmpty()
  @IsArray()
  @IsString({ each: true })
  objects: string[];
}
