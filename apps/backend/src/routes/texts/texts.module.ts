import { forwardRef, Module } from '@nestjs/common';
import { AuthModule } from '@backend/auth/auth.module';
import { CommonModule } from '@backend/common/common.module';
import { AnnotationsModule } from '@backend/routes/annotations/annotations.module';
import { TextAnnotationsModule } from './text-annotations/text-annotations.module';
import { TextsController } from './texts.controller';
import { TextsService } from './texts.service';

@Module({
  imports: [
    forwardRef(() => AuthModule),
    CommonModule,
    TextAnnotationsModule,
    AnnotationsModule,
  ],
  controllers: [TextsController],
  providers: [TextsService],
  exports: [TextsService],
})
export class TextsModule {}
