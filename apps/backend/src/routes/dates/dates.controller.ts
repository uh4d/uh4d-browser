import { Controller, Get, Query } from '@nestjs/common';
import { ApiOkResponse, ApiOperation } from '@nestjs/swagger';
import { LocationQueryParams } from '@backend/routes/dto/location-query-params';
import { DatesService } from './dates.service';
import { DateExtentsDto } from './dto/date-extents.dto';

@Controller('dates')
export class DatesController {
  constructor(private readonly dateService: DatesService) {}

  @ApiOperation({
    summary: 'Query date extents',
    description:
      'Query minimum and maximum dates of images and objects (within the given location query).',
  })
  @ApiOkResponse({
    type: DateExtentsDto,
  })
  @Get('extent')
  async getExtent(
    @Query() query: LocationQueryParams,
  ): Promise<DateExtentsDto> {
    return {
      images: await this.dateService.getImagesExtent(query),
      objects: await this.dateService.getObjectsExtent(query),
    };
  }
}
