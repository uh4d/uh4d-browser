import { ApiProperty } from '@nestjs/swagger';
import { MinMaxDatesDto } from './min-max-dates.dto';
import { DateExtents } from '@uh4d/dto/interfaces/custom/dates';

export class DateExtentsDto implements DateExtents {
  @ApiProperty({
    description: 'Minimum/maximum dates of images',
    type: () => MinMaxDatesDto,
    required: false,
    nullable: true,
  })
  images: MinMaxDatesDto | null;

  @ApiProperty({
    description: 'Minimum/maximum dates of objects',
    type: () => MinMaxDatesDto,
    required: false,
    nullable: true,
  })
  objects: MinMaxDatesDto | null;
}
