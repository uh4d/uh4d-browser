import { ApiProperty } from '@nestjs/swagger';

export class MinMaxDatesDto {
  @ApiProperty({
    description: 'Minimum date',
    type: 'string',
    format: 'date',
    required: false,
  })
  min: string;

  @ApiProperty({
    description: 'Maximum date',
    type: 'string',
    format: 'date',
    required: false,
  })
  max: string;
}
