import { Injectable } from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { LocationQueryParams } from '@backend/routes/dto/location-query-params';
import { getSpatialParams } from '@backend/utils/spatial-params';
import { MinMaxDatesDto } from './dto/min-max-dates.dto';

@Injectable()
export class DatesService {
  constructor(private readonly neo4j: Neo4jService) {}

  /**
   * Get minimum and maximum date of all queried images.
   */
  async getImagesExtent(
    query: LocationQueryParams,
  ): Promise<MinMaxDatesDto | null> {
    const spatialParams = getSpatialParams(query);

    let q = '';

    if (spatialParams) {
      q +=
        'WITH point({latitude: $geo.latitude, longitude: $geo.longitude}) AS userPoint ';
    }

    // language=Cypher
    q += `
      MATCH (image:E38:UH4D)<-[:P94]-(e65:E65)-[:P7]->(place:E53)`;

    if (spatialParams) {
      // language=Cypher
      q += `
        MATCH (place)-[:P168]->(geo:E94)
        WHERE distance(geo.point, userPoint) < coalesce(geo.radius, $geo.radius, 0)`;
    }

    // language=Cypher
    q += `
      CALL {
        WITH e65
        MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
        WHERE date.from IS NOT NULL
        RETURN date.from AS d
        UNION
        WITH e65
        MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
        WHERE date.to IS NOT NULL
        RETURN date.to AS d
      }
      RETURN apoc.coll.sort(apoc.coll.toSet(collect(d))) AS dates
    `;

    const params = {
      geo: spatialParams,
    };

    const result = (await this.neo4j.read<{ dates: string[] }>(q, params))[0];

    if (result.dates.length === 0) return null;
    return {
      min: result.dates[0],
      max: result.dates[result.dates.length - 1],
    };
  }

  /**
   * Get minimum and maximum of all construction and deconstruction dates of the queried objects.
   */
  async getObjectsExtent(
    query: LocationQueryParams,
  ): Promise<MinMaxDatesDto | null> {
    const spatialParams = getSpatialParams(query);

    let q = '';

    if (spatialParams) {
      q +=
        'WITH point({latitude: $geo.latitude, longitude: $geo.longitude}) AS userPoint ';
    }

    // language=Cypher
    q += `
      MATCH (obj:E22:UH4D)<-[:P157]-(place:E53)`;

    if (spatialParams) {
      // language=Cypher
      q += `
        MATCH (place)-[:P168]->(geo:E94)
        WHERE distance(geo.point, userPoint) < coalesce(geo.radius, $geo.radius, 0)`;
    }

    // language=Cypher
    q += `
      CALL {
        WITH obj
        OPTIONAL MATCH (obj)<-[:P108]-(:E12)-[:P4]->(:E52)-[:P82]->(date:E61)
        RETURN date.value AS d
        UNION
        WITH obj
        OPTIONAL MATCH (obj)<-[:P13]-(:E6)-[:P4]->(:E52)-[:P82]->(date:E61)
        RETURN date.value AS d
      }
      RETURN apoc.coll.sort(apoc.coll.toSet(collect(d))) AS dates
    `;

    const params = {
      geo: spatialParams,
    };

    const result = (await this.neo4j.read<{ dates: string[] }>(q, params))[0];

    if (result.dates.length === 0) return null;
    return {
      min: result.dates[0],
      max: result.dates[result.dates.length - 1],
    };
  }
}
