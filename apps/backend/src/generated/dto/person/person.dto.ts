import { ApiProperty } from '@nestjs/swagger';

export class PersonDto {
  @ApiProperty({
    description: 'Person ID',
    type: 'string',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Person name',
    type: 'string',
    required: false,
  })
  value: string;
}
