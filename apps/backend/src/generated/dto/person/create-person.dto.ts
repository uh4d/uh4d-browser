import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreatePersonDto {
  @ApiProperty({
    description: 'Person name',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  value: string;
}
