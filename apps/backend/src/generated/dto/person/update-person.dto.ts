import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class UpdatePersonDto {
  @ApiProperty({
    description: 'Person name',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  value?: string;
}
