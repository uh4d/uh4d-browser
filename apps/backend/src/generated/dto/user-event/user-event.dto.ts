import { ApiProperty } from '@nestjs/swagger';

export class UserEventDto {
  @ApiProperty({
    type: 'string',
    required: false,
  })
  userId: string;
  @ApiProperty({
    type: 'string',
    required: false,
  })
  username: string;
  @ApiProperty({
    type: 'string',
    required: false,
  })
  timestamp: string;
}
