import { ApiProperty } from '@nestjs/swagger';

export class TextFileDto {
  @ApiProperty({
    type: 'string',
    required: false,
  })
  path: string;
  @ApiProperty({
    type: 'string',
    required: false,
  })
  file: string;
  @ApiProperty({
    description: 'Down-sampled image with resolution up to 2048px',
    type: 'string',
    required: false,
  })
  preview: string;
  @ApiProperty({
    description: 'Thumbnail image with resolution up to 200px',
    type: 'string',
    required: false,
  })
  thumb: string;
  @ApiProperty({
    description: 'Original uploaded file',
    type: 'string',
    required: false,
  })
  original: string;
  @ApiProperty({
    description: 'File format',
    type: 'string',
    required: false,
  })
  type: string;
  @ApiProperty({
    type: 'string',
    required: false,
    nullable: true,
  })
  pdf: string | null;
  @ApiProperty({
    type: 'string',
    required: false,
    nullable: true,
  })
  txt: string | null;
  @ApiProperty({
    type: 'string',
    required: false,
    nullable: true,
  })
  json: string | null;
  @ApiProperty({
    type: 'string',
    required: false,
    nullable: true,
  })
  bib: string | null;
}
