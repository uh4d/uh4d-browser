import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class CreateTextFileDto {
  @ApiProperty({
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  path: string;
  @ApiProperty({
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  file: string;
  @ApiProperty({
    description: 'Down-sampled image with resolution up to 2048px',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  preview: string;
  @ApiProperty({
    description: 'Thumbnail image with resolution up to 200px',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  thumb: string;
  @ApiProperty({
    description: 'Original uploaded file',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  original: string;
  @ApiProperty({
    description: 'File format',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  type: string;
  @ApiProperty({
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  pdf?: string | null;
  @ApiProperty({
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  txt?: string | null;
  @ApiProperty({
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  json?: string | null;
  @ApiProperty({
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  bib?: string | null;
}
