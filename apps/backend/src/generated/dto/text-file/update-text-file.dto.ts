import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class UpdateTextFileDto {
  @ApiProperty({
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  path?: string;
  @ApiProperty({
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  file?: string;
  @ApiProperty({
    description: 'Down-sampled image with resolution up to 2048px',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  preview?: string;
  @ApiProperty({
    description: 'Thumbnail image with resolution up to 200px',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  thumb?: string;
  @ApiProperty({
    description: 'Original uploaded file',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  original?: string;
  @ApiProperty({
    description: 'File format',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  type?: string;
  @ApiProperty({
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  pdf?: string | null;
  @ApiProperty({
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  txt?: string | null;
  @ApiProperty({
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  json?: string | null;
  @ApiProperty({
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  bib?: string | null;
}
