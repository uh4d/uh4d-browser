import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsInt,
  IsNotEmpty,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { CreateNormDataNodeDto } from '../norm-data-node/create-norm-data-node.dto';

export class CreateTextAnnotationDto {
  @ApiProperty({
    description: 'Content of the annotatino',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  value: string;
  @ApiProperty({
    description: 'Start position index within the whole text string',
    type: 'integer',
    format: 'int32',
  })
  @IsNotEmpty()
  @IsInt()
  start: number;
  @ApiProperty({
    description: 'End position index within the whole text string',
    type: 'integer',
    format: 'int32',
  })
  @IsNotEmpty()
  @IsInt()
  end: number;
  @ApiProperty({
    type: () => CreateNormDataNodeDto,
    isArray: true,
  })
  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateNormDataNodeDto)
  aat: CreateNormDataNodeDto[];
  @ApiProperty({
    type: () => CreateNormDataNodeDto,
    isArray: true,
  })
  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateNormDataNodeDto)
  wikidata: CreateNormDataNodeDto[];
}
