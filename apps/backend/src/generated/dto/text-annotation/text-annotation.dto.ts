import { ApiProperty } from '@nestjs/swagger';
import { NormDataNodeDto } from '../norm-data-node/norm-data-node.dto';

export class TextAnnotationDto {
  @ApiProperty({
    type: 'string',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Content of the annotatino',
    type: 'string',
    required: false,
  })
  value: string;
  @ApiProperty({
    description: 'Start position index within the whole text string',
    type: 'integer',
    format: 'int32',
    required: false,
  })
  start: number;
  @ApiProperty({
    description: 'End position index within the whole text string',
    type: 'integer',
    format: 'int32',
    required: false,
  })
  end: number;
  @ApiProperty({
    type: () => NormDataNodeDto,
    isArray: true,
    required: false,
  })
  aat: NormDataNodeDto[];
  @ApiProperty({
    type: () => NormDataNodeDto,
    isArray: true,
    required: false,
  })
  wikidata: NormDataNodeDto[];
}
