import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsInt,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { UpdateNormDataNodeDto } from '../norm-data-node/update-norm-data-node.dto';

export class UpdateTextAnnotationDto {
  @ApiProperty({
    description: 'Content of the annotatino',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  value?: string;
  @ApiProperty({
    description: 'Start position index within the whole text string',
    type: 'integer',
    format: 'int32',
    required: false,
  })
  @IsOptional()
  @IsInt()
  start?: number;
  @ApiProperty({
    description: 'End position index within the whole text string',
    type: 'integer',
    format: 'int32',
    required: false,
  })
  @IsOptional()
  @IsInt()
  end?: number;
  @ApiProperty({
    type: () => UpdateNormDataNodeDto,
    isArray: true,
    required: false,
  })
  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => UpdateNormDataNodeDto)
  aat?: UpdateNormDataNodeDto[];
  @ApiProperty({
    type: () => UpdateNormDataNodeDto,
    isArray: true,
    required: false,
  })
  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => UpdateNormDataNodeDto)
  wikidata?: UpdateNormDataNodeDto[];
}
