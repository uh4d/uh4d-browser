import { ApiProperty } from '@nestjs/swagger';

export class PoseDto {
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  tx: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  ty: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  tz: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  qx: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  qy: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  qz: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  qw: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    isArray: true,
    required: false,
  })
  params: number[];
}
