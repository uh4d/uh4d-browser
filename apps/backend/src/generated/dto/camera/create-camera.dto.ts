import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  Min,
} from 'class-validator';

export class CreateCameraDto {
  @ApiProperty({
    description:
      'Geographic coordinate that specifies the north-south position',
    type: 'number',
    format: 'float',
  })
  @IsNotEmpty()
  @IsNumber()
  latitude: number;
  @ApiProperty({
    description: 'Geographic coordinate that specifies the east-west position',
    type: 'number',
    format: 'float',
  })
  @IsNotEmpty()
  @IsNumber()
  longitude: number;
  @ApiProperty({
    description: 'Height above sea level (meter)',
    type: 'number',
    format: 'float',
  })
  @IsNotEmpty()
  @IsNumber()
  altitude: number;
  @ApiProperty({
    description:
      'If radius is > 0, than the image can not be positioned, and all following values are not given',
    minimum: 0,
    type: 'number',
    format: 'float',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsNumber()
  @Min(0)
  radius?: number | null;
  @ApiProperty({
    description: 'Pitch angle (radians)',
    type: 'number',
    format: 'float',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsNumber()
  omega?: number | null;
  @ApiProperty({
    description: 'Heading angle (radians)',
    type: 'number',
    format: 'float',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsNumber()
  phi?: number | null;
  @ApiProperty({
    description: 'Roll angle (radians)',
    type: 'number',
    format: 'float',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsNumber()
  kappa?: number | null;
  @ApiProperty({
    description: 'Camera constant',
    type: 'number',
    format: 'float',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsNumber()
  ck?: number | null;
  @ApiProperty({
    description: 'Principal point (offset from midpoint)',
    type: 'number',
    format: 'float',
    isArray: true,
    required: false,
    default: [0, 0],
  })
  @IsOptional()
  @IsArray()
  @IsNumber({}, { each: true })
  offset?: number[];
  @ApiProperty({
    description: 'List of distortion coefficients',
    type: 'number',
    format: 'float',
    isArray: true,
    required: false,
    default: [],
  })
  @IsOptional()
  @IsArray()
  @IsNumber({}, { each: true })
  k?: number[];
}
