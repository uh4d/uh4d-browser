import { ApiProperty } from '@nestjs/swagger';

export class CameraDto {
  @ApiProperty({
    description:
      'Geographic coordinate that specifies the north-south position',
    type: 'number',
    format: 'float',
    required: false,
  })
  latitude: number;
  @ApiProperty({
    description: 'Geographic coordinate that specifies the east-west position',
    type: 'number',
    format: 'float',
    required: false,
  })
  longitude: number;
  @ApiProperty({
    description: 'Height above sea level (meter)',
    type: 'number',
    format: 'float',
    required: false,
  })
  altitude: number;
  @ApiProperty({
    description:
      'If radius is > 0, than the image can not be positioned, and all following values are not given',
    minimum: 0,
    type: 'number',
    format: 'float',
    required: false,
    nullable: true,
  })
  radius: number | null;
  @ApiProperty({
    description: 'Pitch angle (radians)',
    type: 'number',
    format: 'float',
    required: false,
    nullable: true,
  })
  omega: number | null;
  @ApiProperty({
    description: 'Heading angle (radians)',
    type: 'number',
    format: 'float',
    required: false,
    nullable: true,
  })
  phi: number | null;
  @ApiProperty({
    description: 'Roll angle (radians)',
    type: 'number',
    format: 'float',
    required: false,
    nullable: true,
  })
  kappa: number | null;
  @ApiProperty({
    description: 'Camera constant',
    type: 'number',
    format: 'float',
    required: false,
    nullable: true,
  })
  ck: number | null;
  @ApiProperty({
    description: 'Principal point (offset from midpoint)',
    type: 'number',
    format: 'float',
    isArray: true,
    required: false,
  })
  offset: number[];
  @ApiProperty({
    description: 'List of distortion coefficients',
    type: 'number',
    format: 'float',
    isArray: true,
    required: false,
  })
  k: number[];
}
