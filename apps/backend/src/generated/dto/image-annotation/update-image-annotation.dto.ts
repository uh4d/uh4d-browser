import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { UpdateNormDataNodeDto } from '../norm-data-node/update-norm-data-node.dto';

export class UpdateImageAnnotationDto {
  @ApiProperty({
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  polylist?: number[][][];
  @ApiProperty({
    description: 'Normalized bounding box `[x, y, width, height]`',
    minItems: 4,
    maxItems: 4,
    type: 'number',
    format: 'float',
    isArray: true,
    required: false,
  })
  @IsOptional()
  @IsArray()
  @IsNumber({}, { each: true })
  bbox?: number[];
  @ApiProperty({
    description: 'Normalized area of the polygon',
    type: 'number',
    format: 'float',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  area?: number;
  @ApiProperty({
    type: () => UpdateNormDataNodeDto,
    isArray: true,
    required: false,
  })
  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => UpdateNormDataNodeDto)
  aat?: UpdateNormDataNodeDto[];
  @ApiProperty({
    type: () => UpdateNormDataNodeDto,
    isArray: true,
    required: false,
  })
  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => UpdateNormDataNodeDto)
  wikidata?: UpdateNormDataNodeDto[];
}
