import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsNotEmpty,
  IsNumber,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { CreateNormDataNodeDto } from '../norm-data-node/create-norm-data-node.dto';

export class CreateImageAnnotationDto {
  @ApiProperty({
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  polylist: number[][][];
  @ApiProperty({
    description: 'Normalized bounding box `[x, y, width, height]`',
    minItems: 4,
    maxItems: 4,
    type: 'number',
    format: 'float',
    isArray: true,
  })
  @IsNotEmpty()
  @IsArray()
  @IsNumber({}, { each: true })
  bbox: number[];
  @ApiProperty({
    description: 'Normalized area of the polygon',
    type: 'number',
    format: 'float',
  })
  @IsNotEmpty()
  @IsNumber()
  area: number;
  @ApiProperty({
    type: () => CreateNormDataNodeDto,
    isArray: true,
  })
  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateNormDataNodeDto)
  aat: CreateNormDataNodeDto[];
  @ApiProperty({
    type: () => CreateNormDataNodeDto,
    isArray: true,
  })
  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateNormDataNodeDto)
  wikidata: CreateNormDataNodeDto[];
}
