import { ApiProperty } from '@nestjs/swagger';
import { NormDataNodeDto } from '../norm-data-node/norm-data-node.dto';

export class ImageAnnotationDto {
  @ApiProperty({
    type: 'string',
    required: false,
  })
  id: string;
  @ApiProperty({
    type: 'string',
    required: false,
  })
  polylist: number[][][];
  @ApiProperty({
    description: 'Normalized bounding box `[x, y, width, height]`',
    minItems: 4,
    maxItems: 4,
    type: 'number',
    format: 'float',
    isArray: true,
    required: false,
  })
  bbox: number[];
  @ApiProperty({
    description: 'Normalized area of the polygon',
    type: 'number',
    format: 'float',
    required: false,
  })
  area: number;
  @ApiProperty({
    type: () => NormDataNodeDto,
    isArray: true,
    required: false,
  })
  aat: NormDataNodeDto[];
  @ApiProperty({
    type: () => NormDataNodeDto,
    isArray: true,
    required: false,
  })
  wikidata: NormDataNodeDto[];
}
