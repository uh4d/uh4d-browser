import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsNotEmpty,
  IsNumber,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { CreateNormDataNodeDto } from '../norm-data-node/create-norm-data-node.dto';

export class CreateObjectAnnotationDto {
  @ApiProperty({
    description: 'Name to identify the sub-object within the 3D model',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  objectName: string;
  @ApiProperty({
    description:
      'Bounding box in world scale `[minX, minY, minZ, maxX, maxY, maxZ]`',
    minItems: 4,
    maxItems: 4,
    type: 'number',
    format: 'float',
    isArray: true,
  })
  @IsNotEmpty()
  @IsArray()
  @IsNumber({}, { each: true })
  bbox: number[];
  @ApiProperty({
    description: 'Volume of the bounding box',
    type: 'number',
    format: 'float',
  })
  @IsNotEmpty()
  @IsNumber()
  volume: number;
  @ApiProperty({
    type: () => CreateNormDataNodeDto,
    isArray: true,
  })
  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateNormDataNodeDto)
  aat: CreateNormDataNodeDto[];
  @ApiProperty({
    type: () => CreateNormDataNodeDto,
    isArray: true,
  })
  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateNormDataNodeDto)
  wikidata: CreateNormDataNodeDto[];
}
