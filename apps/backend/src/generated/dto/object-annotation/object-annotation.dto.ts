import { ApiProperty } from '@nestjs/swagger';
import { NormDataNodeDto } from '../norm-data-node/norm-data-node.dto';

export class ObjectAnnotationDto {
  @ApiProperty({
    type: 'string',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Name to identify the sub-object within the 3D model',
    type: 'string',
    required: false,
  })
  objectName: string;
  @ApiProperty({
    description:
      'Bounding box in world scale `[minX, minY, minZ, maxX, maxY, maxZ]`',
    minItems: 4,
    maxItems: 4,
    type: 'number',
    format: 'float',
    isArray: true,
    required: false,
  })
  bbox: number[];
  @ApiProperty({
    description: 'Volume of the bounding box',
    type: 'number',
    format: 'float',
    required: false,
  })
  volume: number;
  @ApiProperty({
    type: () => NormDataNodeDto,
    isArray: true,
    required: false,
  })
  aat: NormDataNodeDto[];
  @ApiProperty({
    type: () => NormDataNodeDto,
    isArray: true,
    required: false,
  })
  wikidata: NormDataNodeDto[];
}
