import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { UpdateNormDataNodeDto } from '../norm-data-node/update-norm-data-node.dto';

export class UpdateObjectAnnotationDto {
  @ApiProperty({
    description: 'Name to identify the sub-object within the 3D model',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  objectName?: string;
  @ApiProperty({
    description:
      'Bounding box in world scale `[minX, minY, minZ, maxX, maxY, maxZ]`',
    minItems: 4,
    maxItems: 4,
    type: 'number',
    format: 'float',
    isArray: true,
    required: false,
  })
  @IsOptional()
  @IsArray()
  @IsNumber({}, { each: true })
  bbox?: number[];
  @ApiProperty({
    description: 'Volume of the bounding box',
    type: 'number',
    format: 'float',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  volume?: number;
  @ApiProperty({
    type: () => UpdateNormDataNodeDto,
    isArray: true,
    required: false,
  })
  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => UpdateNormDataNodeDto)
  aat?: UpdateNormDataNodeDto[];
  @ApiProperty({
    type: () => UpdateNormDataNodeDto,
    isArray: true,
    required: false,
  })
  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => UpdateNormDataNodeDto)
  wikidata?: UpdateNormDataNodeDto[];
}
