import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional } from 'class-validator';

export class UpdateLocationDto {
  @ApiProperty({
    description:
      'Geographic coordinate that specifies the north-south position',
    type: 'number',
    format: 'float',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  latitude?: number;
  @ApiProperty({
    description: 'Geographic coordinate that specifies the east-west position',
    type: 'number',
    format: 'float',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  longitude?: number;
  @ApiProperty({
    description: 'Height above sea level (meter)',
    type: 'number',
    format: 'float',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  altitude?: number;
}
