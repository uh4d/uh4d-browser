import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { CreateImageDateDto } from '../image-date/create-image-date.dto';
import { CreateTextFileDto } from '../text-file/create-text-file.dto';
import { CreateTextObjectDto } from '../text-object/create-text-object.dto';

export class CreateTextDto {
  @ApiProperty({
    description: 'Title of the text',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  title: string;
  @ApiProperty({
    description: 'Names of the authors',
    type: 'string',
    isArray: true,
  })
  @IsNotEmpty()
  @IsArray()
  @IsString({ each: true })
  authors: string[];
  @ApiProperty({
    description: 'Publication date',
    type: () => CreateImageDateDto,
    required: false,
    nullable: true,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => CreateImageDateDto)
  date?: CreateImageDateDto | null;
  @ApiProperty({
    description: 'File reference information',
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  bibtex?: string | null;
  @ApiProperty({
    description: 'File reference information',
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  doi?: string | null;
  @ApiProperty({
    description: 'Url to resource',
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  url?: string | null;
  @ApiProperty({
    description: 'File reference information',
    type: () => CreateTextFileDto,
  })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreateTextFileDto)
  file: CreateTextFileDto;
  @ApiProperty({
    description: 'List of object IDs the text relates to',
    type: () => CreateTextObjectDto,
    isArray: true,
  })
  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateTextObjectDto)
  objects: CreateTextObjectDto[];
  @ApiProperty({
    description: 'Associated tags',
    type: 'string',
    isArray: true,
  })
  @IsNotEmpty()
  @IsArray()
  @IsString({ each: true })
  tags: string[];
}
