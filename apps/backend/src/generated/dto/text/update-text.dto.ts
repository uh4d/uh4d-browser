import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsOptional, IsString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { UpdateImageDateDto } from '../image-date/update-image-date.dto';
import { UpdateTextFileDto } from '../text-file/update-text-file.dto';
import { UpdateTextObjectDto } from '../text-object/update-text-object.dto';

export class UpdateTextDto {
  @ApiProperty({
    description: 'Title of the text',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  title?: string;
  @ApiProperty({
    description: 'Names of the authors',
    type: 'string',
    isArray: true,
    required: false,
  })
  @IsOptional()
  @IsArray()
  @IsString({ each: true })
  authors?: string[];
  @ApiProperty({
    description: 'Publication date',
    type: () => UpdateImageDateDto,
    required: false,
    nullable: true,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateImageDateDto)
  date?: UpdateImageDateDto | null;
  @ApiProperty({
    description: 'File reference information',
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  bibtex?: string | null;
  @ApiProperty({
    description: 'File reference information',
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  doi?: string | null;
  @ApiProperty({
    description: 'Url to resource',
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  url?: string | null;
  @ApiProperty({
    description: 'File reference information',
    type: () => UpdateTextFileDto,
    required: false,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateTextFileDto)
  file?: UpdateTextFileDto;
  @ApiProperty({
    description: 'List of object IDs the text relates to',
    type: () => UpdateTextObjectDto,
    isArray: true,
    required: false,
  })
  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => UpdateTextObjectDto)
  objects?: UpdateTextObjectDto[];
  @ApiProperty({
    description: 'Associated tags',
    type: 'string',
    isArray: true,
    required: false,
  })
  @IsOptional()
  @IsArray()
  @IsString({ each: true })
  tags?: string[];
}
