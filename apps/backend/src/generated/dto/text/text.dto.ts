import { ApiProperty } from '@nestjs/swagger';
import { ImageDateDto } from '../image-date/image-date.dto';
import { TextFileDto } from '../text-file/text-file.dto';
import { TextObjectDto } from '../text-object/text-object.dto';
import { UserEventDto } from '../user-event/user-event.dto';

export class TextDto {
  @ApiProperty({
    type: 'string',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Title of the text',
    type: 'string',
    required: false,
  })
  title: string;
  @ApiProperty({
    description: 'Names of the authors',
    type: 'string',
    isArray: true,
    required: false,
  })
  authors: string[];
  @ApiProperty({
    description: 'Publication date',
    type: () => ImageDateDto,
    required: false,
    nullable: true,
  })
  date: ImageDateDto | null;
  @ApiProperty({
    description: 'File reference information',
    type: 'string',
    required: false,
    nullable: true,
  })
  bibtex: string | null;
  @ApiProperty({
    description: 'File reference information',
    type: 'string',
    required: false,
    nullable: true,
  })
  doi: string | null;
  @ApiProperty({
    description: 'Url to resource',
    type: 'string',
    required: false,
    nullable: true,
  })
  url: string | null;
  @ApiProperty({
    description: 'File reference information',
    type: () => TextFileDto,
    required: false,
  })
  file: TextFileDto;
  @ApiProperty({
    description: 'List of object IDs the text relates to',
    type: () => TextObjectDto,
    isArray: true,
    required: false,
  })
  objects: TextObjectDto[];
  @ApiProperty({
    description: 'Associated tags',
    type: 'string',
    isArray: true,
    required: false,
  })
  tags: string[];
  @ApiProperty({
    description: 'Indicates if the text has any annotations.',
    type: 'boolean',
    required: false,
  })
  annotationsAvailable: boolean;
  @ApiProperty({
    description:
      'Indicates if the text uploaded by a user needs to be validated by a moderator.',
    type: 'boolean',
    required: false,
    nullable: true,
  })
  pending: boolean | null;
  @ApiProperty({
    description: 'Indicates if the text has been declined by moderator.',
    type: 'boolean',
    required: false,
    nullable: true,
  })
  declined: boolean | null;
  @ApiProperty({
    type: () => UserEventDto,
    required: false,
    nullable: true,
  })
  uploadedBy: UserEventDto | null;
  @ApiProperty({
    type: () => UserEventDto,
    required: false,
    nullable: true,
  })
  editedBy: UserEventDto | null;
}
