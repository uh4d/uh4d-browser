import { ApiProperty } from '@nestjs/swagger';

export class ObjectMetaDto {
  @ApiProperty({
    description:
      'DOI for Zenodo entry, where this object is stored permanently',
    type: 'string',
    required: false,
    nullable: true,
  })
  zenodo_doi: string | null;
  @ApiProperty({
    description: 'Additional notes on the object',
    type: 'string',
    required: false,
    nullable: true,
  })
  misc: string | null;
  @ApiProperty({
    type: 'string',
    required: false,
    nullable: true,
  })
  address: string | null;
  @ApiProperty({
    type: 'string',
    isArray: true,
    required: false,
  })
  formerAddress: string[];
  @ApiProperty({
    description: 'Tags on the object',
    type: 'string',
    isArray: true,
    required: false,
  })
  tags: string[];
}
