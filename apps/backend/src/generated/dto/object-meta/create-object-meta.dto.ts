import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUrl,
} from 'class-validator';

export class CreateObjectMetaDto {
  @ApiProperty({
    description:
      'DOI for Zenodo entry, where this object is stored permanently',
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  @IsUrl()
  zenodo_doi?: string | null;
  @ApiProperty({
    description: 'Additional notes on the object',
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  misc?: string | null;
  @ApiProperty({
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  address?: string | null;
  @ApiProperty({
    type: 'string',
    isArray: true,
  })
  @IsNotEmpty()
  @IsArray()
  @IsString({ each: true })
  formerAddress: string[];
  @ApiProperty({
    description: 'Tags on the object',
    type: 'string',
    isArray: true,
  })
  @IsNotEmpty()
  @IsArray()
  @IsString({ each: true })
  tags: string[];
}
