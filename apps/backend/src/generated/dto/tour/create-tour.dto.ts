import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateTourDto {
  @ApiProperty({
    description: 'Tour title',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  title: string;
}
