import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class UpdateTourDto {
  @ApiProperty({
    description: 'Tour title',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  title?: string;
}
