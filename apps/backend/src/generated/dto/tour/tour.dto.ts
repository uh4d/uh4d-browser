import { ApiProperty } from '@nestjs/swagger';
import { UserEventDto } from '../user-event/user-event.dto';

export class TourDto {
  @ApiProperty({
    type: 'string',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Tour title',
    type: 'string',
    required: false,
  })
  title: string;
  @ApiProperty({
    type: () => UserEventDto,
    required: false,
    nullable: true,
  })
  createdBy: UserEventDto | null;
  @ApiProperty({
    type: () => UserEventDto,
    required: false,
    nullable: true,
  })
  editedBy: UserEventDto | null;
}
