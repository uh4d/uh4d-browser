import { ApiProperty } from '@nestjs/swagger';
import { ImageDateDto } from '../image-date/image-date.dto';
import { ImageFileDto } from '../image-file/image-file.dto';
import { CameraDto } from '../camera/camera.dto';
import { UserEventDto } from '../user-event/user-event.dto';

export class ImageEntity {
  @ApiProperty({
    type: 'string',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Image title',
    type: 'string',
    required: false,
  })
  title: string;
  @ApiProperty({
    description: 'Name of the photographer',
    type: 'string',
    required: false,
    nullable: true,
  })
  author: string | null;
  @ApiProperty({
    description: 'Rights holder',
    type: 'string',
    required: false,
    nullable: true,
  })
  owner: string | null;
  @ApiProperty({
    description: 'Temporal information defining time-span',
    type: () => ImageDateDto,
    required: false,
    nullable: true,
  })
  date: ImageDateDto | null;
  @ApiProperty({
    description: 'File reference information',
    type: () => ImageFileDto,
    required: false,
  })
  file: ImageFileDto;
  @ApiProperty({
    description:
      'Exterior and interior camera parameters (spatial information)',
    type: () => CameraDto,
    required: false,
  })
  camera: CameraDto;
  @ApiProperty({
    description:
      'Status of spatialization:\n\n* 0 - Not yet spatialized\n* 1 - Spatialization not possible\n* 2 - Spatialization may be possible in future\n* 3 - Position (lat,lon) is known, but no orientation\n* 4 - Manually spatialized\n* 5 - Spatialized by automatic pipeline\n* 6 - Based on rephotography camera data',
    type: 'integer',
    format: 'int32',
    required: false,
  })
  spatialStatus: number;
  @ApiProperty({
    description:
      'Start of timespan the image can be used as texture for buildings.',
    type: 'string',
    format: 'date-time',
    required: false,
    nullable: true,
  })
  vrcity_useAsTextureFrom: Date | null;
  @ApiProperty({
    description:
      'End of timespan the image can be used as texture for buildings.',
    type: 'string',
    format: 'date-time',
    required: false,
    nullable: true,
  })
  vrcity_useAsTextureTo: Date | null;
  @ApiProperty({
    description: 'Maximum distance for projective texturing shader.',
    type: 'number',
    format: 'float',
    required: false,
    nullable: true,
  })
  vrcity_projectionDistance: number | null;
  @ApiProperty({
    description:
      'Access to the image files is restricted due to copyright restrictions.',
    type: 'boolean',
    required: false,
    nullable: true,
  })
  restrictedAccess: boolean | null;
  @ApiProperty({
    description: 'Indicates if the image has any annotations.',
    type: 'boolean',
    required: false,
  })
  annotationsAvailable: boolean;
  @ApiProperty({
    description:
      'Indicates if the image uploaded by a user needs to be validated by a moderator.',
    type: 'boolean',
    required: false,
    nullable: true,
  })
  pending: boolean | null;
  @ApiProperty({
    description: 'Indicates if the image has been declined by moderator.',
    type: 'boolean',
    required: false,
    nullable: true,
  })
  declined: boolean | null;
  @ApiProperty({
    type: () => UserEventDto,
    required: false,
    nullable: true,
  })
  uploadedBy: UserEventDto | null;
  @ApiProperty({
    type: () => UserEventDto,
    required: false,
    nullable: true,
  })
  editedBy: UserEventDto | null;
}
