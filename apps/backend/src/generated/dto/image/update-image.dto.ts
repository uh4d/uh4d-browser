import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDateString,
  IsIn,
  IsInt,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { UpdateImageDateDto } from '../image-date/update-image-date.dto';
import { CreateCameraDto } from '../camera/create-camera.dto';

export class UpdateImageDto {
  @ApiProperty({
    description: 'Image title',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  title?: string;
  @ApiProperty({
    description: 'Name of the photographer',
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  author?: string | null;
  @ApiProperty({
    description: 'Rights holder',
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  owner?: string | null;
  @ApiProperty({
    description: 'Temporal information defining time-span',
    type: () => UpdateImageDateDto,
    required: false,
    nullable: true,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateImageDateDto)
  date?: UpdateImageDateDto | null;
  @ApiProperty({
    description:
      'Exterior and interior camera parameters (spatial information)',
    type: () => CreateCameraDto,
    required: false,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => CreateCameraDto)
  camera?: CreateCameraDto;
  @ApiProperty({
    description:
      'Status of spatialization:\n\n* 0 - Not yet spatialized\n* 1 - Spatialization not possible\n* 2 - Spatialization may be possible in future\n* 3 - Position (lat,lon) is known, but no orientation\n* 4 - Manually spatialized\n* 5 - Spatialized by automatic pipeline\n* 6 - Based on rephotography camera data',
    type: 'integer',
    format: 'int32',
    required: false,
  })
  @IsOptional()
  @IsInt()
  @IsIn([0, 1, 2, 3, 4, 5, 6])
  spatialStatus?: number;
  @ApiProperty({
    description:
      'Start of timespan the image can be used as texture for buildings.',
    type: 'string',
    format: 'date-time',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsDateString()
  vrcity_useAsTextureFrom?: Date | null;
  @ApiProperty({
    description:
      'End of timespan the image can be used as texture for buildings.',
    type: 'string',
    format: 'date-time',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsDateString()
  vrcity_useAsTextureTo?: Date | null;
  @ApiProperty({
    description: 'Maximum distance for projective texturing shader.',
    type: 'number',
    format: 'float',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsNumber()
  vrcity_projectionDistance?: number | null;
  @ApiProperty({
    description:
      'Access to the image files is restricted due to copyright restrictions.',
    type: 'boolean',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsBoolean()
  restrictedAccess?: boolean | null;
}
