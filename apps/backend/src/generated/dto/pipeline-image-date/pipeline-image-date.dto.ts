import { ApiProperty } from '@nestjs/swagger';

export class PipelineImageDateDto {
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    required: false,
  })
  from: Date;
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    required: false,
  })
  to: Date;
}
