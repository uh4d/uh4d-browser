import { ApiProperty } from '@nestjs/swagger';
import { LocationDto } from '../location/location.dto';

export class TextObjectDto {
  @ApiProperty({
    type: 'string',
    required: false,
  })
  id: string;
  @ApiProperty({
    type: 'string',
    required: false,
  })
  name: string;
  @ApiProperty({
    type: () => LocationDto,
    required: false,
  })
  location: LocationDto;
}
