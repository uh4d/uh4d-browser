import { RoleType } from '@uh4d/config';
import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { GeofenceDto } from '../geofence/geofence.dto';

export class UserEntity {
  @ApiProperty({
    type: 'string',
    required: false,
  })
  id: string;
  @ApiProperty({
    type: 'string',
    required: false,
  })
  name: string;
  @ApiProperty({
    type: 'string',
    required: false,
  })
  email: string;
  @ApiProperty({
    type: 'string',
    required: false,
  })
  phone: string;
  @ApiProperty({
    type: 'string',
    required: false,
  })
  password: string;
  @ApiHideProperty()
  lastLogin: Date | null;
  @ApiHideProperty()
  authSecret: string;
  @ApiHideProperty()
  refreshToken: string | null;
  @ApiHideProperty()
  pending: boolean | null;
  @ApiProperty({
    type: 'string',
    required: false,
    nullable: true,
  })
  role: RoleType | null;
  @ApiProperty({
    description:
      'Content creator/moderator priviledges are only valid within these geofenced areas.',
    type: () => GeofenceDto,
    isArray: true,
    required: false,
  })
  geofences: GeofenceDto[];
}
