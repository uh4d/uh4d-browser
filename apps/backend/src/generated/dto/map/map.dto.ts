import { ApiProperty } from '@nestjs/swagger';
import { MapFileDto } from '../map-file/map-file.dto';
import { LocationDto } from '../location/location.dto';

export class MapDto {
  @ApiProperty({
    type: 'string',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Year of the map',
    type: 'string',
    required: false,
  })
  date: string;
  @ApiProperty({
    description: 'File reference information',
    type: () => MapFileDto,
    required: false,
  })
  file: MapFileDto;
  @ApiProperty({
    description: 'Geographic position of the scene',
    type: () => LocationDto,
    required: false,
  })
  location: LocationDto;
}
