import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { CreateMapFileDto } from '../map-file/create-map-file.dto';
import { CreateLocationDto } from '../location/create-location.dto';

export class CreateMapDto {
  @ApiProperty({
    description: 'Year of the map',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  date: string;
  @ApiProperty({
    description: 'File reference information',
    type: () => CreateMapFileDto,
  })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreateMapFileDto)
  file: CreateMapFileDto;
  @ApiProperty({
    description: 'Geographic position of the scene',
    type: () => CreateLocationDto,
  })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreateLocationDto)
  location: CreateLocationDto;
}
