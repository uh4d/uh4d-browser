import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { UpdateMapFileDto } from '../map-file/update-map-file.dto';
import { UpdateLocationDto } from '../location/update-location.dto';

export class UpdateMapDto {
  @ApiProperty({
    description: 'Year of the map',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  date?: string;
  @ApiProperty({
    description: 'File reference information',
    type: () => UpdateMapFileDto,
    required: false,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateMapFileDto)
  file?: UpdateMapFileDto;
  @ApiProperty({
    description: 'Geographic position of the scene',
    type: () => UpdateLocationDto,
    required: false,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateLocationDto)
  location?: UpdateLocationDto;
}
