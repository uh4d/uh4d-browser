import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateSceneDto {
  @ApiProperty({
    description: 'Name of the scene',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  name: string;
  @ApiProperty({
    description:
      'Geographic coordinate that specifies the north-south position (origin of local coordinate system)',
    type: 'number',
    format: 'float',
  })
  @IsNotEmpty()
  @IsNumber()
  latitude: number;
  @ApiProperty({
    description:
      'Geographic coordinate that specifies the east-west position (origin of local coordinate system)',
    type: 'number',
    format: 'float',
  })
  @IsNotEmpty()
  @IsNumber()
  longitude: number;
  @ApiProperty({
    description:
      'Height above sea level (meter) (origin of local coordinate system)',
    type: 'number',
    format: 'float',
  })
  @IsNotEmpty()
  @IsNumber()
  altitude: number;
}
