import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString } from 'class-validator';

export class UpdateSceneDto {
  @ApiProperty({
    description: 'Name of the scene',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  name?: string;
  @ApiProperty({
    description:
      'Geographic coordinate that specifies the north-south position (origin of local coordinate system)',
    type: 'number',
    format: 'float',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  latitude?: number;
  @ApiProperty({
    description:
      'Geographic coordinate that specifies the east-west position (origin of local coordinate system)',
    type: 'number',
    format: 'float',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  longitude?: number;
  @ApiProperty({
    description:
      'Height above sea level (meter) (origin of local coordinate system)',
    type: 'number',
    format: 'float',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  altitude?: number;
}
