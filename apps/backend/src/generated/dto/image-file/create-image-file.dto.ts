import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class CreateImageFileDto {
  @ApiProperty({
    description: 'Path to files',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  path: string;
  @ApiProperty({
    description: 'Original uploaded image',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  original: string;
  @ApiProperty({
    description: 'Down-sampled image with resolution up to 2048px',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  preview: string;
  @ApiProperty({
    description: 'Thumbnail image with resolution up to 200px',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  thumb: string;
  @ApiProperty({
    description: 'Mini thumbnail image with resolution 64px',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  tiny: string;
  @ApiProperty({
    description:
      'Image with width and height with power of 2, but maximum 1024px',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  texture: string;
  @ApiProperty({
    description: 'File format of original image',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  type: string;
  @ApiProperty({
    description: 'Width of original image in pixel',
    type: 'integer',
    format: 'int32',
  })
  @IsNotEmpty()
  @IsInt()
  width: number;
  @ApiProperty({
    description: 'Height of original image in pixel',
    type: 'integer',
    format: 'int32',
  })
  @IsNotEmpty()
  @IsInt()
  height: number;
  @ApiProperty({
    description: '`.pkl` feature file',
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  pkl?: string | null;
}
