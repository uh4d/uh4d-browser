import { ApiProperty } from '@nestjs/swagger';

export class ImageFileDto {
  @ApiProperty({
    description: 'Path to files',
    type: 'string',
    required: false,
  })
  path: string;
  @ApiProperty({
    description: 'Original uploaded image',
    type: 'string',
    required: false,
  })
  original: string;
  @ApiProperty({
    description: 'Down-sampled image with resolution up to 2048px',
    type: 'string',
    required: false,
  })
  preview: string;
  @ApiProperty({
    description: 'Thumbnail image with resolution up to 200px',
    type: 'string',
    required: false,
  })
  thumb: string;
  @ApiProperty({
    description: 'Mini thumbnail image with resolution 64px',
    type: 'string',
    required: false,
  })
  tiny: string;
  @ApiProperty({
    description:
      'Image with width and height with power of 2, but maximum 1024px',
    type: 'string',
    required: false,
  })
  texture: string;
  @ApiProperty({
    description: 'File format of original image',
    type: 'string',
    required: false,
  })
  type: string;
  @ApiProperty({
    description: 'Width of original image in pixel',
    type: 'integer',
    format: 'int32',
    required: false,
  })
  width: number;
  @ApiProperty({
    description: 'Height of original image in pixel',
    type: 'integer',
    format: 'int32',
    required: false,
  })
  height: number;
  @ApiProperty({
    description: '`.pkl` feature file',
    type: 'string',
    required: false,
    nullable: true,
  })
  pkl: string | null;
}
