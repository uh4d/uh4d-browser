import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsOptional, IsString } from 'class-validator';

export class UpdateImageFileDto {
  @ApiProperty({
    description: 'Path to files',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  path?: string;
  @ApiProperty({
    description: 'Original uploaded image',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  original?: string;
  @ApiProperty({
    description: 'Down-sampled image with resolution up to 2048px',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  preview?: string;
  @ApiProperty({
    description: 'Thumbnail image with resolution up to 200px',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  thumb?: string;
  @ApiProperty({
    description: 'Mini thumbnail image with resolution 64px',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  tiny?: string;
  @ApiProperty({
    description:
      'Image with width and height with power of 2, but maximum 1024px',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  texture?: string;
  @ApiProperty({
    description: 'File format of original image',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  type?: string;
  @ApiProperty({
    description: 'Width of original image in pixel',
    type: 'integer',
    format: 'int32',
    required: false,
  })
  @IsOptional()
  @IsInt()
  width?: number;
  @ApiProperty({
    description: 'Height of original image in pixel',
    type: 'integer',
    format: 'int32',
    required: false,
  })
  @IsOptional()
  @IsInt()
  height?: number;
  @ApiProperty({
    description: '`.pkl` feature file',
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  pkl?: string | null;
}
