import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNotEmpty, IsNumber } from 'class-validator';

export class CreateOriginDto {
  @ApiProperty({
    description:
      'Geographic coordinate that specifies the north-south position',
    type: 'number',
    format: 'float',
  })
  @IsNotEmpty()
  @IsNumber()
  latitude: number;
  @ApiProperty({
    description: 'Geographic coordinate that specifies the east-west position',
    type: 'number',
    format: 'float',
  })
  @IsNotEmpty()
  @IsNumber()
  longitude: number;
  @ApiProperty({
    description: 'Height above sea level (meter)',
    type: 'number',
    format: 'float',
  })
  @IsNotEmpty()
  @IsNumber()
  altitude: number;
  @ApiProperty({
    description: 'Pitch angle (radians)',
    type: 'number',
    format: 'float',
  })
  @IsNotEmpty()
  @IsNumber()
  omega: number;
  @ApiProperty({
    description: 'Heading angle (radians)',
    type: 'number',
    format: 'float',
  })
  @IsNotEmpty()
  @IsNumber()
  phi: number;
  @ApiProperty({
    description: 'Roll angle (radians)',
    type: 'number',
    format: 'float',
  })
  @IsNotEmpty()
  @IsNumber()
  kappa: number;
  @ApiProperty({
    description: 'Scale of the model',
    minItems: 1,
    maxItems: 3,
    type: 'number',
    format: 'float',
    isArray: true,
  })
  @IsNotEmpty()
  @IsArray()
  @IsNumber({}, { each: true })
  scale: number[];
}
