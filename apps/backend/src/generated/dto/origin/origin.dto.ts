import { ApiProperty } from '@nestjs/swagger';

export class OriginDto {
  @ApiProperty({
    description:
      'Geographic coordinate that specifies the north-south position',
    type: 'number',
    format: 'float',
    required: false,
  })
  latitude: number;
  @ApiProperty({
    description: 'Geographic coordinate that specifies the east-west position',
    type: 'number',
    format: 'float',
    required: false,
  })
  longitude: number;
  @ApiProperty({
    description: 'Height above sea level (meter)',
    type: 'number',
    format: 'float',
    required: false,
  })
  altitude: number;
  @ApiProperty({
    description: 'Pitch angle (radians)',
    type: 'number',
    format: 'float',
    required: false,
  })
  omega: number;
  @ApiProperty({
    description: 'Heading angle (radians)',
    type: 'number',
    format: 'float',
    required: false,
  })
  phi: number;
  @ApiProperty({
    description: 'Roll angle (radians)',
    type: 'number',
    format: 'float',
    required: false,
  })
  kappa: number;
  @ApiProperty({
    description: 'Scale of the model',
    minItems: 1,
    maxItems: 3,
    type: 'number',
    format: 'float',
    isArray: true,
    required: false,
  })
  scale: number[];
}
