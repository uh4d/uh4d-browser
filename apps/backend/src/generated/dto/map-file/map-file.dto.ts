import { ApiProperty } from '@nestjs/swagger';

export class MapFileDto {
  @ApiProperty({
    description: 'Path to files',
    type: 'string',
    required: false,
  })
  path: string;
}
