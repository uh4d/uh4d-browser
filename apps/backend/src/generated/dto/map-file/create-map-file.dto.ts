import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateMapFileDto {
  @ApiProperty({
    description: 'Path to files',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  path: string;
}
