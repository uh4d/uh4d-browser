import { ApiProperty } from '@nestjs/swagger';

export class ObjectDateDto {
  @ApiProperty({
    description: 'Date of destruction',
    type: 'string',
    format: 'date-time',
    required: false,
    nullable: true,
  })
  from: string | null;
  @ApiProperty({
    description: 'Date of erection',
    type: 'string',
    format: 'date-time',
    required: false,
    nullable: true,
  })
  to: string | null;
}
