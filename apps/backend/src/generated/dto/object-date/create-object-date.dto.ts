import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsOptional } from 'class-validator';

export class CreateObjectDateDto {
  @ApiProperty({
    description: 'Date of destruction',
    type: 'string',
    format: 'date-time',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsDateString()
  from?: string | null;
  @ApiProperty({
    description: 'Date of erection',
    type: 'string',
    format: 'date-time',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsDateString()
  to?: string | null;
}
