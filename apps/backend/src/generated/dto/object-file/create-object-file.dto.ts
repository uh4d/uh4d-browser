import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class CreateObjectFileDto {
  @ApiProperty({
    description: 'Path to files',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  path: string;
  @ApiProperty({
    description:
      'The converted 3D model file (`.gltf`, `.glb`) with embedded textures and generated edges',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  file: string;
  @ApiProperty({
    description: 'The original uploaded file',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  original: string;
  @ApiProperty({
    description: 'File format of the original file',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  type: string;
  @ApiProperty({
    description:
      "Converted 3D model including textures with a maximum resolution of 512px. Null, if there aren't any textures.",
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  lowRes?: string | null;
  @ApiProperty({
    description: 'Converted 3D model without generated edges',
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  noEdges?: string | null;
  @ApiProperty({
    description:
      "Converted 3D model without edges including textures with a maximum resolution of 512px. Null, if there aren't any textures.",
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  noEdgesLowRes?: string | null;
}
