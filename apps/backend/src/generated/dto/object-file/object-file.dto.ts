import { ApiProperty } from '@nestjs/swagger';

export class ObjectFileDto {
  @ApiProperty({
    description: 'Path to files',
    type: 'string',
    required: false,
  })
  path: string;
  @ApiProperty({
    description:
      'The converted 3D model file (`.gltf`, `.glb`) with embedded textures and generated edges',
    type: 'string',
    required: false,
  })
  file: string;
  @ApiProperty({
    description: 'The original uploaded file',
    type: 'string',
    required: false,
  })
  original: string;
  @ApiProperty({
    description: 'File format of the original file',
    type: 'string',
    required: false,
  })
  type: string;
  @ApiProperty({
    description:
      "Converted 3D model including textures with a maximum resolution of 512px. Null, if there aren't any textures.",
    type: 'string',
    required: false,
    nullable: true,
  })
  lowRes: string | null;
  @ApiProperty({
    description: 'Converted 3D model without generated edges',
    type: 'string',
    required: false,
    nullable: true,
  })
  noEdges: string | null;
  @ApiProperty({
    description:
      "Converted 3D model without edges including textures with a maximum resolution of 512px. Null, if there aren't any textures.",
    type: 'string',
    required: false,
    nullable: true,
  })
  noEdgesLowRes: string | null;
}
