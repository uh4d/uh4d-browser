import { ApiProperty } from '@nestjs/swagger';
import { LocationDto } from '../location/location.dto';

export class GeneralPoiDto {
  @ApiProperty({
    type: 'string',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Point of Interest title',
    type: 'string',
    required: false,
  })
  title: string;
  @ApiProperty({
    description:
      'Any text. Can be (but not only) a single link or a Markdown formatted text.',
    type: 'string',
    required: false,
  })
  content: string;
  @ApiProperty({
    description: 'Spatial information',
    type: () => LocationDto,
    required: false,
  })
  location: LocationDto;
  @ApiProperty({
    description:
      'Type of the point of interest (refers to the source/origin), e.g., `uh4d`, `wikidata`',
    type: 'string',
    required: false,
  })
  type: string;
}
