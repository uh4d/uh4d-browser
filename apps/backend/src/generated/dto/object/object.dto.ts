import { ApiProperty } from '@nestjs/swagger';
import { ObjectDateDto } from '../object-date/object-date.dto';
import { OriginDto } from '../origin/origin.dto';
import { LocationDto } from '../location/location.dto';
import { ObjectFileDto } from '../object-file/object-file.dto';
import { UserEventDto } from '../user-event/user-event.dto';

export class ObjectDto {
  @ApiProperty({
    type: 'string',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Object appellation',
    type: 'string',
    required: false,
  })
  name: string;
  @ApiProperty({
    description: 'Time-span of object',
    type: () => ObjectDateDto,
    required: false,
  })
  date: ObjectDateDto;
  @ApiProperty({
    description:
      "Origin of the 3D object's local coordinate system. Used to place the object in the 3D scene.",
    type: () => OriginDto,
    required: false,
  })
  origin: OriginDto;
  @ApiProperty({
    description:
      'Geographic position of the center of the object. `altitude` refers to the lowest point of the geometry. Used to query object by `lat`, `lon`, `r`.',
    type: () => LocationDto,
    required: false,
  })
  location: LocationDto;
  @ApiProperty({
    description: 'File reference information',
    type: () => ObjectFileDto,
    required: false,
  })
  file: ObjectFileDto;
  @ApiProperty({
    description:
      "Flag that signals that application should also load object's textures on object load (only relevant to VRCity app).",
    type: 'boolean',
    required: false,
    nullable: true,
  })
  vrcity_forceTextures: boolean | null;
  @ApiProperty({
    description:
      'Flag that signals that object should be rendered and displayed without generated edges.',
    type: 'boolean',
    required: false,
    nullable: true,
  })
  vrcity_noEdges: boolean | null;
  @ApiProperty({
    description:
      'List of OSM ids of ways/relations that overlap with 3D object',
    type: 'integer',
    format: 'int32',
    isArray: true,
    required: false,
  })
  osm_overlap: number[];
  @ApiProperty({
    description:
      'Indicates if the object uploaded by a user needs to be validated by a moderator.',
    type: 'boolean',
    required: false,
    nullable: true,
  })
  pending: boolean | null;
  @ApiProperty({
    description: 'Indicates if the object has been declined by moderator.',
    type: 'boolean',
    required: false,
    nullable: true,
  })
  declined: boolean | null;
  @ApiProperty({
    type: () => UserEventDto,
    required: false,
    nullable: true,
  })
  uploadedBy: UserEventDto | null;
  @ApiProperty({
    type: () => UserEventDto,
    required: false,
    nullable: true,
  })
  editedBy: UserEventDto | null;
}
