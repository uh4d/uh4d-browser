import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsBoolean,
  IsInt,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { UpdateObjectDateDto } from '../object-date/update-object-date.dto';
import { CreateOriginDto } from '../origin/create-origin.dto';

export class UpdateObjectDto {
  @ApiProperty({
    description: 'Object appellation',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  name?: string;
  @ApiProperty({
    description: 'Time-span of object',
    type: () => UpdateObjectDateDto,
    required: false,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateObjectDateDto)
  date?: UpdateObjectDateDto;
  @ApiProperty({
    description:
      "Origin of the 3D object's local coordinate system. Used to place the object in the 3D scene.",
    type: () => CreateOriginDto,
    required: false,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => CreateOriginDto)
  origin?: CreateOriginDto;
  @ApiProperty({
    description:
      "Flag that signals that application should also load object's textures on object load (only relevant to VRCity app).",
    type: 'boolean',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsBoolean()
  vrcity_forceTextures?: boolean | null;
  @ApiProperty({
    description:
      'Flag that signals that object should be rendered and displayed without generated edges.',
    type: 'boolean',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsBoolean()
  vrcity_noEdges?: boolean | null;
  @ApiProperty({
    description:
      'List of OSM ids of ways/relations that overlap with 3D object',
    type: 'integer',
    format: 'int32',
    isArray: true,
    required: false,
  })
  @IsOptional()
  @IsArray()
  @IsInt({ each: true })
  osm_overlap?: number[];
}
