import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsBoolean,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { CreateObjectDateDto } from '../object-date/create-object-date.dto';
import { CreateOriginDto } from '../origin/create-origin.dto';

export class CreateObjectDto {
  @ApiProperty({
    description: 'Object appellation',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  name: string;
  @ApiProperty({
    description: 'Time-span of object',
    type: () => CreateObjectDateDto,
  })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreateObjectDateDto)
  date: CreateObjectDateDto;
  @ApiProperty({
    description:
      "Origin of the 3D object's local coordinate system. Used to place the object in the 3D scene.",
    type: () => CreateOriginDto,
  })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreateOriginDto)
  origin: CreateOriginDto;
  @ApiProperty({
    description:
      "Flag that signals that application should also load object's textures on object load (only relevant to VRCity app).",
    type: 'boolean',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsBoolean()
  vrcity_forceTextures?: boolean | null;
  @ApiProperty({
    description:
      'Flag that signals that object should be rendered and displayed without generated edges.',
    type: 'boolean',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsBoolean()
  vrcity_noEdges?: boolean | null;
  @ApiProperty({
    description:
      'List of OSM ids of ways/relations that overlap with 3D object',
    type: 'integer',
    format: 'int32',
    isArray: true,
  })
  @IsNotEmpty()
  @IsArray()
  @IsInt({ each: true })
  osm_overlap: number[];
}
