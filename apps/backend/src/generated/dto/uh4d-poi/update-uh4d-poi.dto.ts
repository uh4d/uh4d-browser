import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsDateString,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { UpdateLocationDto } from '../location/update-location.dto';
import { CreatePoiDateDto } from '../poi-date/create-poi-date.dto';

export class UpdateUh4dPoiDto {
  @ApiProperty({
    description: 'Point of Interest title',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  title?: string;
  @ApiProperty({
    description:
      'Any text. Can be (but not only) a single link or a Markdown formatted text.',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  content?: string;
  @ApiProperty({
    description: 'Spatial information',
    type: () => UpdateLocationDto,
    required: false,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateLocationDto)
  location?: UpdateLocationDto;
  @ApiProperty({
    description:
      'Date boundaries in which the point of interest should be visible',
    type: () => CreatePoiDateDto,
    required: false,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => CreatePoiDateDto)
  date?: CreatePoiDateDto;
  @ApiProperty({
    description: 'ID of object the point of interest is attached to',
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  objectId?: string | null;
  @ApiProperty({
    description:
      'Position of preferred perspective to point of interest in scene coordinates',
    minItems: 3,
    maxItems: 3,
    type: 'number',
    format: 'float',
    isArray: true,
    required: false,
  })
  @IsOptional()
  @IsArray()
  @IsNumber({}, { each: true })
  camera?: number[];
  @ApiProperty({
    description: 'Time when point of interest has been created',
    type: 'string',
    format: 'date-time',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsDateString()
  timestamp?: Date | null;
  @ApiProperty({
    description: 'List of damage factors according to preservation order',
    type: 'string',
    isArray: true,
    required: false,
  })
  @IsOptional()
  @IsArray()
  @IsString({ each: true })
  damageFactors?: string[];
}
