import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsDateString,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { CreateLocationDto } from '../location/create-location.dto';
import { CreatePoiDateDto } from '../poi-date/create-poi-date.dto';

export class CreateUh4dPoiDto {
  @ApiProperty({
    description: 'Point of Interest title',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  title: string;
  @ApiProperty({
    description:
      'Any text. Can be (but not only) a single link or a Markdown formatted text.',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  content: string;
  @ApiProperty({
    description: 'Spatial information',
    type: () => CreateLocationDto,
  })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreateLocationDto)
  location: CreateLocationDto;
  @ApiProperty({
    description:
      'Date boundaries in which the point of interest should be visible',
    type: () => CreatePoiDateDto,
  })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreatePoiDateDto)
  date: CreatePoiDateDto;
  @ApiProperty({
    description: 'ID of object the point of interest is attached to',
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  objectId?: string | null;
  @ApiProperty({
    description:
      'Position of preferred perspective to point of interest in scene coordinates',
    minItems: 3,
    maxItems: 3,
    type: 'number',
    format: 'float',
    isArray: true,
  })
  @IsNotEmpty()
  @IsArray()
  @IsNumber({}, { each: true })
  camera: number[];
  @ApiProperty({
    description: 'Time when point of interest has been created',
    type: 'string',
    format: 'date-time',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsDateString()
  timestamp?: Date | null;
  @ApiProperty({
    description: 'List of damage factors according to preservation order',
    type: 'string',
    isArray: true,
  })
  @IsNotEmpty()
  @IsArray()
  @IsString({ each: true })
  damageFactors: string[];
}
