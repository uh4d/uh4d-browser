import { ApiProperty } from '@nestjs/swagger';
import { LocationDto } from '../location/location.dto';
import { PoiDateDto } from '../poi-date/poi-date.dto';
import { UserEventDto } from '../user-event/user-event.dto';

export class Uh4dPoiDto {
  @ApiProperty({
    type: 'string',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Point of Interest title',
    type: 'string',
    required: false,
  })
  title: string;
  @ApiProperty({
    description:
      'Any text. Can be (but not only) a single link or a Markdown formatted text.',
    type: 'string',
    required: false,
  })
  content: string;
  @ApiProperty({
    description: 'Spatial information',
    type: () => LocationDto,
    required: false,
  })
  location: LocationDto;
  @ApiProperty({
    description:
      'Type of the point of interest (refers to the source/origin), e.g., `uh4d`, `wikidata`',
    type: 'string',
    required: false,
  })
  type: string;
  @ApiProperty({
    description:
      'Date boundaries in which the point of interest should be visible',
    type: () => PoiDateDto,
    required: false,
  })
  date: PoiDateDto;
  @ApiProperty({
    description: 'ID of object the point of interest is attached to',
    type: 'string',
    required: false,
    nullable: true,
  })
  objectId: string | null;
  @ApiProperty({
    description:
      'Position of preferred perspective to point of interest in scene coordinates',
    minItems: 3,
    maxItems: 3,
    type: 'number',
    format: 'float',
    isArray: true,
    required: false,
  })
  camera: number[];
  @ApiProperty({
    description: 'Time when point of interest has been created',
    type: 'string',
    format: 'date-time',
    required: false,
    nullable: true,
  })
  timestamp: Date | null;
  @ApiProperty({
    description: 'List of damage factors according to preservation order',
    type: 'string',
    isArray: true,
    required: false,
  })
  damageFactors: string[];
  @ApiProperty({
    description:
      'Indicates if the POI created by a user needs to be validated by a moderator.',
    type: 'boolean',
    required: false,
    nullable: true,
  })
  pending: boolean | null;
  @ApiProperty({
    description: 'Indicates if the POI has been declined by moderator.',
    type: 'boolean',
    required: false,
    nullable: true,
  })
  declined: boolean | null;
  @ApiProperty({
    type: () => UserEventDto,
    required: false,
    nullable: true,
  })
  createdBy: UserEventDto | null;
  @ApiProperty({
    type: () => UserEventDto,
    required: false,
    nullable: true,
  })
  editedBy: UserEventDto | null;
}
