import { ApiProperty } from '@nestjs/swagger';

export class TerrainFileDto {
  @ApiProperty({
    description: 'Path to files',
    type: 'string',
    required: false,
  })
  path: string;
  @ApiProperty({
    description: 'Terrain model file (`.gltf`)',
    type: 'string',
    required: false,
  })
  file: string;
  @ApiProperty({
    description: 'File type',
    type: 'string',
    required: false,
  })
  type: string;
}
