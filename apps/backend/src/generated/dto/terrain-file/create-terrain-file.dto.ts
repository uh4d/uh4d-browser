import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateTerrainFileDto {
  @ApiProperty({
    description: 'Path to files',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  path: string;
  @ApiProperty({
    description: 'Terrain model file (`.gltf`)',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  file: string;
  @ApiProperty({
    description: 'File type',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  type: string;
}
