import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class UpdateTerrainFileDto {
  @ApiProperty({
    description: 'Path to files',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  path?: string;
  @ApiProperty({
    description: 'Terrain model file (`.gltf`)',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  file?: string;
  @ApiProperty({
    description: 'File type',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  type?: string;
}
