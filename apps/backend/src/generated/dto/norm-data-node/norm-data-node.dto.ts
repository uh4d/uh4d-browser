import { ApiProperty } from '@nestjs/swagger';

export class NormDataNodeDto {
  @ApiProperty({
    type: 'string',
    required: false,
  })
  id: string;
  @ApiProperty({
    type: 'string',
    required: false,
  })
  identifier: string;
  @ApiProperty({
    type: 'string',
    required: false,
    nullable: true,
  })
  label: string | null;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
    nullable: true,
  })
  weight: number | null;
}
