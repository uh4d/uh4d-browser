import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUrl,
} from 'class-validator';

export class CreateImageMetaDto {
  @ApiProperty({
    description: 'Image description',
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  description?: string | null;
  @ApiProperty({
    description: 'Additional notes on the image',
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  misc?: string | null;
  @ApiProperty({
    description: 'Capture number (only SLUB images)',
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  captureNumber?: string | null;
  @ApiProperty({
    description: 'Link to original repository',
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  @IsUrl()
  permalink?: string | null;
  @ApiProperty({
    description: 'Tags on the image',
    type: 'string',
    isArray: true,
  })
  @IsNotEmpty()
  @IsArray()
  @IsString({ each: true })
  tags: string[];
}
