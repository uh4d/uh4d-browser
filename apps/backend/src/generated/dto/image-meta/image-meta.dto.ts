import { ApiProperty } from '@nestjs/swagger';

export class ImageMetaDto {
  @ApiProperty({
    description: 'Image description',
    type: 'string',
    required: false,
    nullable: true,
  })
  description: string | null;
  @ApiProperty({
    description: 'Additional notes on the image',
    type: 'string',
    required: false,
    nullable: true,
  })
  misc: string | null;
  @ApiProperty({
    description: 'Capture number (only SLUB images)',
    type: 'string',
    required: false,
    nullable: true,
  })
  captureNumber: string | null;
  @ApiProperty({
    description: 'Link to original repository',
    type: 'string',
    required: false,
    nullable: true,
  })
  permalink: string | null;
  @ApiProperty({
    description: 'Tags on the image',
    type: 'string',
    isArray: true,
    required: false,
  })
  tags: string[];
}
