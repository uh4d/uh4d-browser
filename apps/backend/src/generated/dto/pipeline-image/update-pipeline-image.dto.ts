import { ApiProperty } from '@nestjs/swagger';
import {
  IsIn,
  IsInt,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { UpdatePoseDto } from '../pose/update-pose.dto';
import { UpdatePipelineImageDateDto } from '../pipeline-image-date/update-pipeline-image-date.dto';

export class UpdatePipelineImageDto {
  @ApiProperty({
    description: 'Image width in pixel',
    type: 'integer',
    format: 'int32',
    required: false,
  })
  @IsOptional()
  @IsInt()
  width?: number;
  @ApiProperty({
    description: 'Image height in pixel',
    type: 'integer',
    format: 'int32',
    required: false,
  })
  @IsOptional()
  @IsInt()
  height?: number;
  @ApiProperty({
    description:
      'Pose with relative Cartesian coordinates (extrinsic and intrinsic camera parameters)',
    type: () => UpdatePoseDto,
    required: false,
    nullable: true,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdatePoseDto)
  pose?: UpdatePoseDto | null;
  @ApiProperty({
    description: 'Absolute path to image file',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  image?: string;
  @ApiProperty({
    description: 'Absolute path to pkl feature file',
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  pkl?: string | null;
  @ApiProperty({
    description: 'Time-span in which the photo has been taken',
    type: () => UpdatePipelineImageDateDto,
    required: false,
    nullable: true,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdatePipelineImageDateDto)
  date?: UpdatePipelineImageDateDto | null;
  @ApiProperty({
    description:
      'Status of spatialization:\n\n* 0 - Not yet spatialized\n* 1 - Spatialization not possible\n* 2 - Spatialization may be possible in future\n* 3 - Position (lat,lon) is known, but no orientation\n* 4 - Manually spatialized\n* 5 - Spatialized by automatic pipeline',
    type: 'integer',
    format: 'int32',
    required: false,
  })
  @IsOptional()
  @IsInt()
  @IsIn([0, 1, 2, 3, 4, 5])
  spatialStatus?: number;
}
