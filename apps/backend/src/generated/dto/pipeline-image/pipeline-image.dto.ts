import { ApiProperty } from '@nestjs/swagger';
import { PoseDto } from '../pose/pose.dto';
import { PipelineImageDateDto } from '../pipeline-image-date/pipeline-image-date.dto';

export class PipelineImageDto {
  @ApiProperty({
    description: 'Image ID',
    type: 'string',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Image width in pixel',
    type: 'integer',
    format: 'int32',
    required: false,
  })
  width: number;
  @ApiProperty({
    description: 'Image height in pixel',
    type: 'integer',
    format: 'int32',
    required: false,
  })
  height: number;
  @ApiProperty({
    description:
      'Pose with relative Cartesian coordinates (extrinsic and intrinsic camera parameters)',
    type: () => PoseDto,
    required: false,
    nullable: true,
  })
  pose: PoseDto | null;
  @ApiProperty({
    description: 'Absolute path to image file',
    type: 'string',
    required: false,
  })
  image: string;
  @ApiProperty({
    description: 'Absolute path to pkl feature file',
    type: 'string',
    required: false,
    nullable: true,
  })
  pkl: string | null;
  @ApiProperty({
    description: 'Time-span in which the photo has been taken',
    type: () => PipelineImageDateDto,
    required: false,
    nullable: true,
  })
  date: PipelineImageDateDto | null;
  @ApiProperty({
    description:
      'Status of spatialization:\n\n* 0 - Not yet spatialized\n* 1 - Spatialization not possible\n* 2 - Spatialization may be possible in future\n* 3 - Position (lat,lon) is known, but no orientation\n* 4 - Manually spatialized\n* 5 - Spatialized by automatic pipeline',
    type: 'integer',
    format: 'int32',
    required: false,
  })
  spatialStatus: number;
}
