import { ApiProperty } from '@nestjs/swagger';
import {
  IsIn,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { CreatePoseDto } from '../pose/create-pose.dto';
import { CreatePipelineImageDateDto } from '../pipeline-image-date/create-pipeline-image-date.dto';

export class CreatePipelineImageDto {
  @ApiProperty({
    description: 'Image width in pixel',
    type: 'integer',
    format: 'int32',
  })
  @IsNotEmpty()
  @IsInt()
  width: number;
  @ApiProperty({
    description: 'Image height in pixel',
    type: 'integer',
    format: 'int32',
  })
  @IsNotEmpty()
  @IsInt()
  height: number;
  @ApiProperty({
    description:
      'Pose with relative Cartesian coordinates (extrinsic and intrinsic camera parameters)',
    type: () => CreatePoseDto,
    required: false,
    nullable: true,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => CreatePoseDto)
  pose?: CreatePoseDto | null;
  @ApiProperty({
    description: 'Absolute path to image file',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  image: string;
  @ApiProperty({
    description: 'Absolute path to pkl feature file',
    type: 'string',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  pkl?: string | null;
  @ApiProperty({
    description: 'Time-span in which the photo has been taken',
    type: () => CreatePipelineImageDateDto,
    required: false,
    nullable: true,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => CreatePipelineImageDateDto)
  date?: CreatePipelineImageDateDto | null;
  @ApiProperty({
    description:
      'Status of spatialization:\n\n* 0 - Not yet spatialized\n* 1 - Spatialization not possible\n* 2 - Spatialization may be possible in future\n* 3 - Position (lat,lon) is known, but no orientation\n* 4 - Manually spatialized\n* 5 - Spatialized by automatic pipeline',
    type: 'integer',
    format: 'int32',
  })
  @IsNotEmpty()
  @IsInt()
  @IsIn([0, 1, 2, 3, 4, 5])
  spatialStatus: number;
}
