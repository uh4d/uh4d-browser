import { ApiProperty } from '@nestjs/swagger';
import { TerrainLocationDto } from '../terrain-location/terrain-location.dto';
import { TerrainFileDto } from '../terrain-file/terrain-file.dto';

export class TerrainDto {
  @ApiProperty({
    type: 'string',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Terrain/place appellation',
    type: 'string',
    required: false,
  })
  name: string;
  @ApiProperty({
    description:
      'Geographic position of the center of the terrain. `west`, `east`, `north`, and `south` describe the boundaries of the terrain.',
    type: () => TerrainLocationDto,
    required: false,
  })
  location: TerrainLocationDto;
  @ApiProperty({
    description: 'File reference information',
    type: () => TerrainFileDto,
    required: false,
  })
  file: TerrainFileDto;
}
