import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { UpdateTerrainLocationDto } from '../terrain-location/update-terrain-location.dto';
import { UpdateTerrainFileDto } from '../terrain-file/update-terrain-file.dto';

export class UpdateTerrainDto {
  @ApiProperty({
    description: 'Terrain/place appellation',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  name?: string;
  @ApiProperty({
    description:
      'Geographic position of the center of the terrain. `west`, `east`, `north`, and `south` describe the boundaries of the terrain.',
    type: () => UpdateTerrainLocationDto,
    required: false,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateTerrainLocationDto)
  location?: UpdateTerrainLocationDto;
  @ApiProperty({
    description: 'File reference information',
    type: () => UpdateTerrainFileDto,
    required: false,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateTerrainFileDto)
  file?: UpdateTerrainFileDto;
}
