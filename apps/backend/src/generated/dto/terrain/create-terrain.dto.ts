import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { CreateTerrainLocationDto } from '../terrain-location/create-terrain-location.dto';
import { CreateTerrainFileDto } from '../terrain-file/create-terrain-file.dto';

export class CreateTerrainDto {
  @ApiProperty({
    description: 'Terrain/place appellation',
    type: 'string',
  })
  @IsNotEmpty()
  @IsString()
  name: string;
  @ApiProperty({
    description:
      'Geographic position of the center of the terrain. `west`, `east`, `north`, and `south` describe the boundaries of the terrain.',
    type: () => CreateTerrainLocationDto,
  })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreateTerrainLocationDto)
  location: CreateTerrainLocationDto;
  @ApiProperty({
    description: 'File reference information',
    type: () => CreateTerrainFileDto,
  })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreateTerrainFileDto)
  file: CreateTerrainFileDto;
}
