import { ApiProperty } from '@nestjs/swagger';

export class PoiDateDto {
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    required: false,
    nullable: true,
  })
  from: string | null;
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    required: false,
    nullable: true,
  })
  to: string | null;
  @ApiProperty({
    type: 'boolean',
    required: false,
  })
  objectBound: boolean;
}
