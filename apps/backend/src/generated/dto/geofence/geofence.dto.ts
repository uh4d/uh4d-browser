import { ApiProperty } from '@nestjs/swagger';

export class GeofenceDto {
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  latitude: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  longitude: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  radius: number;
}
