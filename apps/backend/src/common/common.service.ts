import { BadRequestException, Injectable } from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { AuthUser } from '@backend/auth/user';
import { UserEventDto } from '@backend/dto';

@Injectable()
export class CommonService {
  constructor(private readonly neo4j: Neo4jService) {}

  async updateEditedBy(user: AuthUser, itemId: string): Promise<UserEventDto> {
    // language=Cypher
    const q = `
      MATCH (user:User:UH4D { id: $userId }), (item:UH4D { id: $itemId })
      OPTIONAL MATCH (item)-[lastRel:edited_by]->(lastUser:User)
      CREATE (item)-[rel:edited_by { timestamp: datetime() }]->(user)
      DELETE lastRel
      RETURN rel.timestamp AS timestamp
    `;

    const results = await this.neo4j.write<{ timestamp: string }>(q, {
      userId: user.id,
      itemId,
    });
    if (!results[0]) {
      throw new Error(
        `Could not update editedBy with user ${user.id} and item ${itemId}`,
      );
    }

    return {
      userId: user.id,
      username: user.name,
      timestamp: results[0].timestamp,
    };
  }

  async updateValidationStatus(
    itemId: string,
    pending: boolean,
    declined = false,
  ): Promise<
    { id: string; pending?: boolean; declined?: boolean } | undefined
  > {
    if (pending === true && declined === true) {
      throw new BadRequestException(
        "'pending' and 'declined' cannot both be 'true'",
      );
    }

    let q = 'MATCH (item:UH4D {id: $itemId}) ';
    if (pending) q += 'SET item.pending = TRUE ';
    else q += 'REMOVE item.pending ';
    if (declined) q += 'SET item.declined = TRUE ';
    else q += 'REMOVE item.declined ';
    q += 'RETURN item';

    const results = await this.neo4j.write(q, { itemId });
    return results[0];
  }
}
