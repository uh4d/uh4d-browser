import { NestFactory } from '@nestjs/core';
import { RequestMethod, ValidationPipe } from '@nestjs/common';
import { WsAdapter } from '@nestjs/platform-ws';
import * as bodyParser from 'body-parser';
import { ensureDir, pathExistsSync } from 'fs-extra';
import * as sharp from 'sharp';
import { BackendConfig, RedocConfig } from '@backend/config';
import { AppModule } from './app.module';

sharp.cache(false);

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

  app.enableCors({
    origin: '*',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    allowedHeaders: 'Content-type,Accept,X-Access-Token,X-Key,Authorization',
    exposedHeaders: 'Content-Length',
  });

  const config = app.get(BackendConfig);

  // check if directories exists
  if (!pathExistsSync(config.dirData))
    throw new Error(
      `Data directory ${config.dirData} as specified in .env does not exist!`,
    );
  if (!pathExistsSync(config.dirLogs))
    throw new Error(
      `Logs directory ${config.dirLogs} as specified in .env does not exist!`,
    );
  if (!pathExistsSync(config.dirCache))
    throw new Error(
      `Cache directory ${config.dirCache} as specified in .env does not exist!`,
    );

  await ensureDir(config.dirTmp);

  // setup documentation
  await app.get(RedocConfig).setup(app);

  // global settings
  app.useGlobalPipes(new ValidationPipe({ whitelist: true }));
  app.setGlobalPrefix('api', {
    exclude: [{ path: 'data/batch', method: RequestMethod.ALL }],
  });

  app.useWebSocketAdapter(new WsAdapter(app));

  app.enableShutdownHooks();

  await app.listen(config.backendPort);
}
bootstrap()
  .then()
  .catch((err) => {
    throw err;
  });
