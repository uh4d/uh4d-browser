import { INestApplication, Injectable } from '@nestjs/common';
import { RedocModule, RedocOptions } from '@brakebein/nestjs-redoc';
import { ConfigService } from '@nestjs/config';
import { SwaggerConfig } from './swagger.config';

@Injectable()
export class RedocConfig {
  constructor(
    private readonly config: ConfigService,
    private readonly swaggerConfig: SwaggerConfig,
  ) {}

  getConfig(): RedocOptions {
    return {
      logo: {
        url: new URL('api/assets/docs/logo_api.png', this.config.get('HOST'))
          .href,
        backgroundColor: '#4E5D6C',
        altText: 'UH4D Backend API Documentation',
      },
      sortPropsAlphabetically: false,
    };
  }

  async setup(app: INestApplication): Promise<void> {
    return RedocModule.setup(
      '/api',
      app as any,
      this.swaggerConfig.createDocument(app),
      this.getConfig(),
    );
  }
}
