import { Test, TestingModule } from '@nestjs/testing';
import { BackendConfig } from './backend.config';

describe('BackendConfigService', () => {
  let service: BackendConfig;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BackendConfig],
    }).compile();

    service = module.get<BackendConfig>(BackendConfig);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
