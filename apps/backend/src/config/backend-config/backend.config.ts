import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { join } from 'node:path';

@Injectable()
export class BackendConfig {
  constructor(private readonly config: ConfigService) {}

  /**
   * Should be `true`, if application runs in production environment on dedicated server.
   * It resolves `true`, if environment variable `NODE_ENV=production` is set.
   */
  get production(): boolean {
    return this.config.get<string>('NODE_ENV') === 'production';
  }

  /**
   * Host URL.
   */
  get host() {
    return this.config.get<string>('HOST');
  }

  /**
   * Backend port the application is running on.
   */
  get backendPort() {
    return this.config.get<string>('BACKEND_PORT');
  }

  /**
   * Path to data directory.
   */
  get dirData() {
    return this.config.get<string>('DIR_DATA');
  }

  /**
   * Path to logs directory.
   */
  get dirLogs() {
    return this.config.get<string>('DIR_LOGS');
  }

  /**
   * Path to cache directory.
   */
  get dirCache() {
    return this.config.get<string>('DIR_CACHE');
  }

  /**
   * Path to backups directory.
   */
  get dirBackups() {
    return this.config.get<string>('DIR_BACKUPS');
  }

  /**
   * Path to directory for temporary files.
   */
  get dirTmp() {
    return join(this.dirData, 'tmp');
  }

  /**
   * Secret for JWT access token generation and validation.
   */
  get jwtAccessSecret() {
    return this.config.get<string>('JWT_ACCESS_SECRET');
  }

  /**
   * Secret for JWT refresh token generation and validation.
   */
  get jwtRefreshSecret() {
    return this.config.get<string>('JWT_REFRESH_SECRET');
  }

  /**
   * SMS gateway URL.
   */
  get smsGatewayUrl() {
    return this.config.get<string>('SMS_GATEWAY_URL');
  }

  /**
   * SMS gateway username (authid).
   */
  get smsGatewayUser() {
    return this.config.get<string>('SMS_GATEWAY_USER');
  }

  /**
   * SMS gateway user password (authcode).
   */
  get smsGatewayPassword() {
    return this.config.get<string>('SMS_GATEWAY_PASSWORD');
  }
}
