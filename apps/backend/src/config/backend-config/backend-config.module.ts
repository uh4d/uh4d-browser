import { Global, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { BackendConfig } from './backend.config';

@Global()
@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      expandVariables: true,
    }),
  ],
  providers: [BackendConfig],
  exports: [ConfigModule, BackendConfig],
})
export class BackendConfigModule {}
