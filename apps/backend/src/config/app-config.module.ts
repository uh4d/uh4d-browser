import { Module } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { ConfigService } from '@nestjs/config';
import { Neo4jModule } from '@brakebein/nest-neo4j';
import { MorpheusModule } from 'morpheus4j';
import { join } from 'node:path';
import { SwaggerConfig } from './swagger.config';
import { RedocConfig } from './redoc.config';
import { BackendConfigModule } from './backend-config/backend-config.module';
import { BackendConfig } from './backend-config/backend.config';

@Module({
  imports: [
    BackendConfigModule,
    ServeStaticModule.forRootAsync({
      useFactory: (config: BackendConfig) => [
        {
          rootPath: config.dirData,
          serveRoot: '/data',
          exclude: ['data/batch'],
        },
        {
          rootPath: config.dirBackups,
          serveRoot: '/backups',
        },
        {
          rootPath: join(__dirname, 'assets'),
          serveRoot: '/api/assets',
        },
      ],
      inject: [BackendConfig],
    }),
    Neo4jModule.forRootAsync({
      useFactory: (config: ConfigService) => ({
        scheme: config.get('NEO4J_SCHEME'),
        host: config.get<string>('NEO4J_HOST'),
        port: config.get<string>('NEO4J_PORT'),
        username: config.get<string>('NEO4J_USERNAME'),
        password: config.get<string>('NEO4J_PASSWORD'),
        database: config.get<string>('NEO4J_DATABASE'),
        verifyConnectionTimeout: 120000,
      }),
      inject: [ConfigService],
    }),
    MorpheusModule.registerAsync({
      useFactory: (config: ConfigService) => ({
        scheme: config.get('NEO4J_SCHEME'),
        host: config.get<string>('NEO4J_HOST'),
        port: Number(config.get<string>('NEO4J_PORT')),
        username: config.get<string>('NEO4J_USERNAME'),
        password: config.get<string>('NEO4J_PASSWORD'),
        database: config.get<string>('NEO4J_DATABASE'),
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [SwaggerConfig, RedocConfig],
  exports: [
    BackendConfigModule,
    ServeStaticModule,
    Neo4jModule,
    SwaggerConfig,
    RedocConfig,
  ],
})
export class AppConfigModule {}
