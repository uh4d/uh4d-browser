import { Test, TestingModule } from '@nestjs/testing';
import { OsmClientService } from './osm-client.service';

describe('OsmClientService', () => {
  let service: OsmClientService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OsmClientService],
    }).compile();

    service = module.get<OsmClientService>(OsmClientService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
