import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
import {
  AltitudesPayload,
  IntersectionsPayload,
} from '@uh4d/dto/interfaces/custom/osm';

@Injectable()
export class OsmClientService {
  constructor(@Inject('OSM_SERVICE') private readonly client: ClientProxy) {}

  computeAltitudes(payload: AltitudesPayload): Promise<Record<string, number>> {
    return lastValueFrom(
      this.client.send({ context: 'altitudes', action: 'compute' }, payload),
    );
  }

  computeIntersections(payload: IntersectionsPayload): Promise<number[]> {
    return lastValueFrom(
      this.client.send(
        { context: 'intersections', action: 'compute' },
        payload,
      ),
    );
  }
}
