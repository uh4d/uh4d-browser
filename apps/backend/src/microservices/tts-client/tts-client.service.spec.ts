import { Test, TestingModule } from '@nestjs/testing';
import { TtsClientService } from './tts-client.service';

describe('TtsClientService', () => {
  let service: TtsClientService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TtsClientService],
    }).compile();

    service = module.get<TtsClientService>(TtsClientService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
