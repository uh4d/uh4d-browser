import { Inject, Injectable } from '@nestjs/common';
import { lastValueFrom } from 'rxjs';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class TtsClientService {
  constructor(@Inject('TTS_SERVICE') private readonly client: ClientProxy) {}

  sendTtsTask(text: string, targetFile: string) {
    return lastValueFrom(
      this.client.send('tts-mp3', { text, targetFile })
    );
  }
}
