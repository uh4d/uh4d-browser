import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import {
  converterServiceFactory,
  osmServiceFactory,
  projectionServiceFactory,
  ttsServiceFactory,
} from './microservice.config';
import { ConverterClientService } from './converter-client/converter-client.service';
import { ProjectionClientService } from './projection-client/projection-client.service';
import { OsmClientService } from './osm-client/osm-client.service';
import { TtsClientService } from './tts-client/tts-client.service';

@Module({
  imports: [ConfigModule],
  providers: [
    osmServiceFactory,
    projectionServiceFactory,
    converterServiceFactory,
    ttsServiceFactory,
    ConverterClientService,
    ProjectionClientService,
    OsmClientService,
    TtsClientService,
  ],
  exports: [
    ConverterClientService,
    ProjectionClientService,
    OsmClientService,
    TtsClientService,
  ],
})
export class MicroservicesModule {}
