import { Test, TestingModule } from '@nestjs/testing';
import { ProjectionClientService } from './projection-client.service';

describe('ProjectionClientService', () => {
  let service: ProjectionClientService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProjectionClientService],
    }).compile();

    service = module.get<ProjectionClientService>(ProjectionClientService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
