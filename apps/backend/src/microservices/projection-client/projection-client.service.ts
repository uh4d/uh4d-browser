import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
import { ObjectWeights } from '@uh4d/dto/interfaces/custom/linked-objects';
import { ImageAnnotationDto } from '@uh4d/dto/interfaces/image-annotation';
import { ProjectionPayload } from '@uh4d/dto/interfaces/custom/projection';

@Injectable()
export class ProjectionClientService {
  constructor(
    @Inject('PROJECTION_SERVICE') private readonly client: ClientProxy,
  ) {}

  computeObjectWeights(
    payload: ProjectionPayload,
  ): Promise<Record<string, ObjectWeights>> {
    return lastValueFrom(
      this.client.send(
        { context: 'image', action: 'computeObjectWeights' },
        payload,
      ),
    );
  }

  projectAnnotationsFromObject(
    payload: ProjectionPayload,
  ): Promise<Pick<ImageAnnotationDto, 'id' | 'polylist' | 'bbox' | 'area'>[]> {
    return lastValueFrom(
      this.client.send(
        { context: 'image', action: 'projectAnnotationsFromObject' },
        payload,
      ),
    );
  }
}
