import { FactoryProvider } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';

export const osmServiceFactory: FactoryProvider = {
  provide: 'OSM_SERVICE',
  inject: [ConfigService],
  useFactory: (configService: ConfigService) => {
    return ClientProxyFactory.create({
      transport: Transport.RMQ,
      options: {
        urls: [configService.get<string>('RABBIT_MQ_URL')],
        queue: 'osm_queue',
        queueOptions: {
          durable: false,
        },
        prefetchCount: 1,
        socketOptions: {
          heartbeatIntervalInSeconds: 60,
          reconnectTimeInSeconds: 5,
        },
      },
    });
  },
};

export const projectionServiceFactory: FactoryProvider = {
  provide: 'PROJECTION_SERVICE',
  inject: [ConfigService],
  useFactory: (configService: ConfigService) => {
    return ClientProxyFactory.create({
      transport: Transport.RMQ,
      options: {
        urls: [configService.get<string>('RABBIT_MQ_URL')],
        queue: 'projection_queue',
        queueOptions: {
          durable: false,
        },
        prefetchCount: 1,
        socketOptions: {
          heartbeatIntervalInSeconds: 60,
          reconnectTimeInSeconds: 5,
        },
      },
    });
  },
};

export const converterServiceFactory: FactoryProvider = {
  provide: 'CONVERTER_SERVICE',
  inject: [ConfigService],
  useFactory: (configService: ConfigService) => {
    return ClientProxyFactory.create({
      transport: Transport.RMQ,
      options: {
        urls: [configService.get<string>('RABBIT_MQ_URL')],
        queue: 'converter_queue',
        queueOptions: {
          durable: false,
        },
        prefetchCount: 1,
        socketOptions: {
          heartbeatIntervalInSeconds: 60,
          reconnectTimeInSeconds: 5,
        },
      },
    });
  },
};

export const ttsServiceFactory: FactoryProvider = {
  provide: 'TTS_SERVICE',
  inject: [ConfigService],
  useFactory: (configService: ConfigService) => {
    return ClientProxyFactory.create({
      transport: Transport.RMQ,
      options: {
        urls: [configService.get<string>('RABBIT_MQ_URL')],
        queue: 'tts_queue',
        queueOptions: {
          durable: false,
        },
        prefetchCount: 1,
        socketOptions: {
          heartbeatIntervalInSeconds: 60,
          reconnectTimeInSeconds: 5,
        },
      },
    });
  },
};
