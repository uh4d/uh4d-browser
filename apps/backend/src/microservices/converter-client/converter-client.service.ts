import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy, RmqRecordBuilder } from '@nestjs/microservices';
import { catchError, lastValueFrom, of, timeout } from 'rxjs';
import { OsmGenerationPayload } from '@uh4d/dto/interfaces/custom/osm';
import { ConvertedFilesDto } from '@uh4d/dto/interfaces/custom/object';

@Injectable()
export class ConverterClientService {
  constructor(
    @Inject('CONVERTER_SERVICE') private readonly client: ClientProxy,
  ) {}

  /**
   * Send converter task to microservice.
   * @param folder Folder in which original file is located and where converted files will be copied to.
   * @param file Filename of the original file within the folder.
   * @return Promise of filename of the converted file.
   */
  sendConverterTask(folder: string, file: string): Promise<ConvertedFilesDto> {
    return lastValueFrom(
      this.client.send(
        { type: '3d-model', action: 'convert' },
        { folder, file },
      ),
    );
  }

  /**
   * Send OSM converter task to microservice.
   */
  sendOsmConverterTask(
    payload: OsmGenerationPayload,
  ): Promise<ConvertedFilesDto> {
    return lastValueFrom(
      this.client.send({ type: 'osm-model', action: 'convert' }, payload),
    );
  }

  /**
   * Get status of converter-service by checking converter binaries.
   * @return Promise of object with retrieved version numbers of Assimp and Collada2Gltf binaries.
   */
  getStatus(): Promise<{ Assimp: string; Collada2Gltf: string }> {
    const config = new RmqRecordBuilder()
      .setOptions({
        expiration: '10000',
      })
      .build();
    return lastValueFrom(
      this.client.send({ context: 'converter', action: 'status' }, config).pipe(
        timeout(500),
        catchError(() => of({})),
      ),
    );
  }
}
