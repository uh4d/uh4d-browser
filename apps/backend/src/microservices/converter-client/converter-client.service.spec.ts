import { Test, TestingModule } from '@nestjs/testing';
import { ConverterClientService } from './converter-client.service';

describe('ConverterClientService', () => {
  let service: ConverterClientService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ConverterClientService],
    }).compile();

    service = module.get<ConverterClientService>(ConverterClientService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
