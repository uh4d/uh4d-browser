import { Module } from '@nestjs/common';
import { ConverterConfig } from './converter.config';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [ConfigModule],
  providers: [ConverterConfig],
  exports: [ConverterConfig],
})
export class ConverterConfigModule {}
