import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class ConverterConfig {
  public readonly binAssimp: string;
  public readonly binCollada2Gltf: string;

  constructor(configService: ConfigService) {
    this.binAssimp = configService.get('BIN_ASSIMP');
    this.binCollada2Gltf = configService.get('BIN_COLLADA2GLTF');
  }
}
