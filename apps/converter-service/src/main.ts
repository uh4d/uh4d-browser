import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { RmqOptions, RpcException, Transport } from '@nestjs/microservices';
import { ConfigService } from '@nestjs/config';
import { AppModule } from './app.module';

async function bootstrap() {
  const appContext = await NestFactory.createApplicationContext(AppModule);
  const configService = appContext.get(ConfigService);

  const app = await NestFactory.createMicroservice<RmqOptions>(AppModule, {
    transport: Transport.RMQ,
    options: {
      urls: [configService.get<string>('RABBIT_MQ_URL')],
      queue: 'converter_queue',
      queueOptions: {
        durable: false,
      },
      prefetchCount: 1,
      socketOptions: {
        heartbeatIntervalInSeconds: 60,
        reconnectTimeInSeconds: 5,
      },
    },
  });

  await appContext.close();

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      exceptionFactory: (errors) => new RpcException(errors),
    }),
  );

  await app.listen();
}

bootstrap()
  .then()
  .catch((err) => {
    throw err;
  });
