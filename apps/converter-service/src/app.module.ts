import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ConverterModule } from './converter/converter.module';
import { ConverterStatusModule } from './converter-status/converter-status.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      expandVariables: true,
    }),
    ConverterModule,
    ConverterStatusModule,
  ],
})
export class AppModule {}
