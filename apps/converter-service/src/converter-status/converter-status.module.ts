import { Module } from '@nestjs/common';
import { ConverterConfigModule } from '../converter-config/converter-config.module';
import { ConverterStatusController } from './converter-status.controller';

@Module({
  imports: [ConverterConfigModule],
  controllers: [ConverterStatusController],
})
export class ConverterStatusModule {}
