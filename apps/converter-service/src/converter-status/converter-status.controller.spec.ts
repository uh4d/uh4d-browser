import { Test, TestingModule } from '@nestjs/testing';
import { ConverterStatusController } from './converter-status.controller';

describe('ConverterStatusController', () => {
  let controller: ConverterStatusController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ConverterStatusController],
    }).compile();

    controller = module.get<ConverterStatusController>(ConverterStatusController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
