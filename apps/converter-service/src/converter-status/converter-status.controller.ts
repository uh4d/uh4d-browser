import { Controller, Logger } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { execFile } from 'promisify-child-process';
import { ConverterConfig } from '../converter-config/converter.config';

@Controller('converter-status')
export class ConverterStatusController {
  private readonly logger = new Logger(ConverterStatusController.name);

  constructor(private readonly converterConfig: ConverterConfig) {}

  @MessagePattern({ context: 'converter', action: 'status' })
  async getConverterStatus(): Promise<{
    Assimp: string;
    Collada2Gltf: string;
  }> {
    // Assimp
    let assimp = '';
    try {
      const result = await execFile(this.converterConfig.binAssimp, [
        'version',
      ]);
      const matches = result.stdout.toString().match(/Version\s(\S+)\s/);
      assimp = matches[1];
    } catch (e) {
      this.logger.error(e);
    }

    // Collada2Gltf
    let collada = '';
    try {
      // Collada2Gltf has no explicit command to get the version
      // check if executed without command options will output help message
      const version = await execFile(this.converterConfig.binCollada2Gltf, [])
        .catch((reason) => reason)
        .then((result) => {
          const matches = result.stdout?.toString().match(/^(COLLADA2GLTF)/);
          if (matches && matches[1] === 'COLLADA2GLTF') {
            return matches[1];
          } else {
            return Promise.reject(result);
          }
        });
      collada = `${version} (unknown version)`;
    } catch (e) {
      this.logger.error(e);
    }

    return {
      Assimp: assimp || 'FAILED!',
      Collada2Gltf: collada || 'FAILED!',
    };
  }
}
