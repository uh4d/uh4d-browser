import { createReadStream, createWriteStream } from 'fs-extra';
import { Entry, Parse, ParseOne } from 'unzipper';
import { pipeline } from 'stream/promises';
import { escapeRegex } from '@uh4d/utils';

export type ZipObject = Entry['vars'] & { path: string };

/**
 * Parse zip file entries.
 * @param zipFile Path to zip file
 * @param filter Only include entries that match the regular expression
 */
export async function parseZipEntries(
  zipFile: string,
  filter?: RegExp,
): Promise<ZipObject[]> {
  const list: ZipObject[] = [];

  await createReadStream(zipFile)
    .pipe(Parse())
    .on('entry', (entry: Entry) => {
      if (!filter || (filter && filter.test(entry.path))) {
        list.push({
          ...entry.vars,
          path: entry.path,
        });
      }
      entry.autodrain();
    })
    .promise();

  return list;
}

/**
 * Extract a file from a zip object.
 * @param zipFile Path to zip file
 * @param inFile Path within the zip file to file which should be extracted
 * @param outFile Path to where the file will be stored
 */
export function extractFileFromZip(
  zipFile: string,
  inFile: string,
  outFile: string,
): Promise<void> {
  return pipeline(
    createReadStream(zipFile),
    ParseOne(new RegExp(escapeRegex(inFile))),
    createWriteStream(outFile),
  );
}
