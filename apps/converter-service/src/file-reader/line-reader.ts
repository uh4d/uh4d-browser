import { createReadStream, createWriteStream, remove, rename } from 'fs-extra';
import { createStream } from 'byline';

/**
 * Read file line by line and look for patterns defined by a regular expression.
 * If a match is found, the callback is triggered, the string returned replaces
 * the original string in this line.
 */
export function findAndReplaceLine(
  filePath: string,
  regexp: RegExp,
  callback: (matches: RegExpExecArray) => string,
): Promise<void> {
  const tempFile = filePath + '.tmp';
  const readStream = createReadStream(filePath);
  const writeStream = createWriteStream(tempFile);
  const lr = createStream(readStream, { keepEmptyLines: true });

  return new Promise((resolve, reject) => {
    lr.on('error', (err) => {
      writeStream.end(async () => {
        await remove(tempFile);
        reject(err);
      });
    });

    writeStream.on('error', async (err) => {
      readStream.destroy();
      await remove(tempFile);
      reject(err);
    });

    lr.on('data', (line) => {
      const matches = regexp.exec(line);
      if (matches) {
        const replaced = callback(matches);
        writeStream.write(`${replaced}\n`);
      } else {
        writeStream.write(`${line}\n`);
      }
    });

    lr.on('end', () => {
      writeStream.end(async () => {
        try {
          await remove(filePath);
          await rename(tempFile, filePath);
          resolve();
        } catch (e) {
          reject(e);
        }
      });
    });
  });
}

/**
 * Read file line by line and look for patterns defined by a regular expression.
 * If a match is found, the callback is triggered.
 */
export function parseLine(
  filePath: string,
  regexp: RegExp,
  callback: (matches: RegExpExecArray) => void,
): Promise<void> {
  const readStream = createReadStream(filePath);
  const lr = createStream(readStream, { keepEmptyLines: true });

  return new Promise((resolve, reject) => {
    lr.on('error', reject);
    lr.on('end', resolve);

    lr.on('data', (line) => {
      const matches = regexp.exec(line);
      if (matches) {
        callback(matches);
      }
    });
  });
}
