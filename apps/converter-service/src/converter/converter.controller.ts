import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { ConvertedFilesDto } from '@uh4d/dto/interfaces/custom/object';
import { ConverterService } from './converter.service';
import { ConverterPayloadDto } from './dto/converter-payload.dto';
import { OsmGenerationPayloadDto } from './dto/osm-generation-payload.dto';

@Controller('converter')
export class ConverterController {
  constructor(private readonly converterService: ConverterService) {}

  @MessagePattern({ type: '3d-model', action: 'convert' })
  async convertModel(
    @Payload() data: ConverterPayloadDto,
  ): Promise<ConvertedFilesDto> {
    return this.converterService.convertModel(data.folder, data.file);
  }

  @MessagePattern({ type: 'osm-model', action: 'convert' })
  convertOsmDataToModel(
    @Payload() data: OsmGenerationPayloadDto,
  ): Promise<ConvertedFilesDto> {
    return this.converterService.convertOsmDataToModel(data);
  }
}
