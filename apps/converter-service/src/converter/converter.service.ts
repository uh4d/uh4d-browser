import { Injectable, Logger } from '@nestjs/common';
import { join } from 'path';
import { forkProcess } from '@uh4d/backend/utils';
import { ConvertedFilesDto } from '@uh4d/dto/interfaces/custom/object';
import { ConverterConfig } from '../converter-config/converter.config';
import { TemporaryConverterContext } from './temporary-converter-context';
import { OsmGenerationPayloadDto } from './dto/osm-generation-payload.dto';

@Injectable()
export class ConverterService {
  private readonly logger = new Logger(ConverterService.name);

  constructor(private readonly converterConfig: ConverterConfig) {}

  /**
   * Convert 3D model to binary gltf.
   * @param folder Folder in which original file is located and where converted files will be copied to.
   * @param file Filename of the original file within the folder.
   * @return Filename of the converted file.
   */
  async convertModel(folder: string, file: string): Promise<ConvertedFilesDto> {
    try {
      return TemporaryConverterContext.processFile(
        folder,
        file,
        this.converterConfig,
      );
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  /**
   * Convert OSM data to binary glb.
   */
  async convertOsmDataToModel(
    payload: OsmGenerationPayloadDto,
  ): Promise<ConvertedFilesDto> {
    try {
      const objFilename = await forkProcess<string>(
        join(__dirname, 'generate-obj-from-osm.js'),
        [],
        payload,
      );

      return TemporaryConverterContext.processFile(
        payload.folder,
        objFilename,
        this.converterConfig,
        false,
      );
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
}
