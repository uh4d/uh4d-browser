import { IsNotEmpty, IsString } from 'class-validator';

export class ConverterPayloadDto {
  @IsNotEmpty()
  @IsString()
  folder: string;
  @IsNotEmpty()
  @IsString()
  file: string;
}
