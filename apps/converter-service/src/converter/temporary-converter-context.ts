import { basename, extname, join, parse } from 'path';
import * as tmp from 'tmp-promise';
import * as fs from 'fs-extra';
import { spawn as spawnPromise } from 'promisify-child-process';
import * as mime from 'mime';
import * as sharp from 'sharp';
import { nanoid } from 'nanoid';
import {
  EdgesGeometry,
  Group,
  LineBasicMaterial,
  LineSegments,
  MathUtils,
  Mesh,
  Object3D,
} from 'three';
import { Logger } from '@nestjs/common';
import {
  getSupportedFileEndings,
  isFileEndingSupported,
  ResizeImageConfig,
} from '@uh4d/config';
import { NodeIO } from '@gltf-transform/core';
import { ALL_EXTENSIONS } from '@gltf-transform/extensions';
import { draco, prune, textureCompress, weld } from '@gltf-transform/functions';
import * as draco3d from 'draco3dgltf';
import { GLTF, GLTFExporter, loadGltf } from 'node-three-gltf';
import { escapeRegex, removeIllegalFileChars } from '@uh4d/utils';
import { spawnAndLog } from '@uh4d/backend/utils';
import { ConvertedFilesDto } from '@uh4d/dto/interfaces/custom/object';
import { isUtf8 } from 'node:buffer';
import * as iconv from 'iconv-lite';
import { pipeline } from 'stream/promises';
import { extractFileFromZip, parseZipEntries } from '../zip-file/unzipper';
import { findAndReplaceLine, parseLine } from '../file-reader/line-reader';
import { ConverterConfig } from '../converter-config/converter.config';
import { cleanCollada } from './collada-utils/clean-collada';

export class TemporaryConverterContext {
  private readonly logger = new Logger(TemporaryConverterContext.name);

  private tmpDir: tmp.DirectoryResult;

  private originalFile: string;
  private zip?: string;
  private extractedFile: string;
  private daeFile: string;

  private gltfFile: string;
  private noEdgesGltfFile?: string;
  private lowResGltfFile?: string;
  private noEdgesLowResGltfFile?: string;

  private textureFiles = new Map<
    string,
    { originalName: string; newName: string }
  >();

  private gltfTransformIO: NodeIO;

  constructor(
    private readonly folder: string,
    private readonly file: string,
    private readonly converterConfig: ConverterConfig,
  ) {}

  static async processFile(
    folder: string,
    file: string,
    config: ConverterConfig,
    multiThread = true,
  ): Promise<ConvertedFilesDto> {
    const context = new TemporaryConverterContext(folder, file, config);

    // conversion pipeline
    try {
      await context.init();
      await context.parseTextures();
      await context.convertToDae();
      // await context.cleanDae();
      await context.extractAndReplaceTextures();
      // await context.createMobileTextures();
      // await context.generateEdges(multiThread);

      try {
        await context.convertToGltf();
      } catch (e) {
        context.logger.warn(e);
        // if gltf-transform draco-compression fails, try legacy approach
        await context.cleanDae();
        await context.generateEdges(multiThread);
        await context.convertToGltfLegacy();
      }

      // await context.uploadTextures();
      return await context.uploadGltf();
    } finally {
      context.dispose();
    }
  }

  private async init(): Promise<void> {
    this.tmpDir = await tmp.dir({ unsafeCleanup: true });
    this.originalFile = join(
      this.tmpDir.path,
      removeIllegalFileChars(this.file),
    );

    const { name: baseFileName } = parse(this.originalFile);
    this.daeFile = join(this.tmpDir.path, 'temp.dae');
    this.gltfFile = join(
      this.tmpDir.path,
      removeIllegalFileChars(baseFileName, true) + '.glb',
    );

    // copy uploaded file to tmp dir
    await fs.copy(join(this.folder, this.file), this.originalFile);

    const stats = await this.debugFileSize(this.originalFile, 'Uploaded');
    // skip dae if empty
    if (stats.size === 0) {
      this.daeFile = '';
    }

    if (mime.getType(this.originalFile) === 'application/zip') {
      this.zip = this.originalFile;
      await this.extractModelFromZip();
    } else {
      this.extractedFile = this.originalFile;
    }

    await this.checkEncoding();

    // init gltf-transform IO
    this.gltfTransformIO = new NodeIO()
      .registerExtensions(ALL_EXTENSIONS)
      .registerDependencies({
        'draco3d.encoder': await draco3d.createEncoderModule(),
      });
  }

  private async debugFileSize(
    file: string,
    prefix?: string,
  ): Promise<fs.Stats> {
    const stats = await fs.stat(file);
    this.logger.debug(
      `${prefix ? prefix + ': ' : ''}${basename(file)} ${(
        stats.size /
        1024 /
        1024
      ).toFixed(3)} MB`,
    );
    return stats;
  }

  private async extractModelFromZip(): Promise<void> {
    if (!this.zip) throw new Error('No zip file provided!');

    const fileEndings = getSupportedFileEndings('model')
      .filter((ending) => ending !== '.zip')
      .map((ending) => '\\' + ending)
      .join('|');
    const fileRegexp = new RegExp(`(?:${fileEndings})$`, 'i');

    // look for files with given endings, but ignore directories
    const availableModels = await parseZipEntries(this.zip, fileRegexp);

    if (availableModels.length === 0)
      throw new Error('No model file included in zip archive!');

    // take only first match
    const zipObject = availableModels[0];
    const parsedFilename = parse(zipObject.path);
    this.extractedFile = join(
      this.tmpDir.path,
      removeIllegalFileChars(parsedFilename.name, true) + parsedFilename.ext,
    );

    this.logger.debug('Extracting file from zip...');

    await extractFileFromZip(this.zip, zipObject.path, this.extractedFile);

    await this.debugFileSize(this.extractedFile, 'Extracted');

    // if OBJ file, then look for MTL files
    if (parsedFilename.ext.toLowerCase() === '.obj') {
      await this.parseMtlFiles();
    }

    // if GLTF file, then look for potential BIN files
    else if (parsedFilename.ext.toLowerCase() === '.gltf') {
      await this.extractGltfBinFiles();
    }
  }

  /**
   * Check for correct encoding, since special characters may cause incomplete conversion otherwise.
   */
  private async checkEncoding() {
    // do not check for binary files
    if (['.glb'].includes(extname(this.extractedFile))) return;

    const fileBuffer = await fs.readFile(this.extractedFile);

    // if not utf-8 compatible, assume ISO-8859-2 encoding, and try to convert to utf-8
    if (!isUtf8(fileBuffer)) {
      const tmpFile = await tmp.file();
      await pipeline(
        fs.createReadStream(this.extractedFile),
        iconv.decodeStream('ISO-8859-2'),
        iconv.encodeStream('utf-8'),
        fs.createWriteStream(tmpFile.path),
      );
      await fs.copy(tmpFile.path, this.extractedFile);
      await tmpFile.cleanup();
    }
  }

  private async getModelInfo(): Promise<string> {
    try {
      const result = await spawnPromise(
        this.converterConfig.binAssimp,
        ['info', this.extractedFile],
        { encoding: 'utf8', maxBuffer: 1024 * 2000 },
      );
      return result.stdout as string;
    } catch (e) {
      throw e.stderr || e;
    }
  }

  private async parseMtlFiles(): Promise<void> {
    // if it's not a zip file, there are no mtl files
    if (!this.zip) return;

    this.logger.debug('Extracting MTL files...');

    const mtlFiles: string[] = [];

    await parseLine(
      this.extractedFile,
      /^(mtllib)\s+(?:[./]+)?(.+)$/i,
      (matches) => {
        mtlFiles.push(matches[2]);
      },
    );

    for (const mtl of mtlFiles) {
      const mtlFiles = await parseZipEntries(
        this.zip,
        new RegExp(escapeRegex(mtl) + '$'),
      );
      if (mtlFiles[0]) {
        // extract mtl file
        const mtlPath = join(this.tmpDir.path, mtl);
        await extractFileFromZip(this.zip, mtlFiles[0].path, mtlPath);
      }
    }
  }

  private async extractGltfBinFiles(): Promise<void> {
    const binFiles = await parseZipEntries(this.zip, /\.bin$/i);
    for (const entry of binFiles) {
      const binPath = join(this.tmpDir.path, entry.path);
      await extractFileFromZip(this.zip, entry.path, binPath);
    }
  }

  private async parseTextures(): Promise<void> {
    // if it's not a zip file, there are no textures or are embedded
    if (!this.zip) return;

    this.logger.debug('Analyzing model...');

    const info = await this.getModelInfo();
    const matches = /Texture Refs:[\s\S]*Node hierarchy/.exec(info);
    if (matches) {
      const texRegexp = /'([^']+)'/g;
      let texMatches: RegExpExecArray | null;

      while ((texMatches = texRegexp.exec(matches[0])) !== null) {
        if (this.textureFiles.has(texMatches[1])) continue;

        // check if texture file is supported by sharp
        const fileEnding = extname(texMatches[1]);
        if (!isFileEndingSupported(fileEnding, 'sharp')) {
          throw new Error('Unsupported texture file format');
        }

        this.textureFiles.set(texMatches[1], {
          originalName: texMatches[1],
          newName: nanoid() + fileEnding,
        });
      }
    }
  }

  private async convertToDae(): Promise<void> {
    if (this.daeFile === '') return;

    this.logger.debug('Converting to DAE...');

    await spawnAndLog(
      this.converterConfig.binAssimp,
      ['export', this.extractedFile, this.daeFile, '-jiv', '-om', '-tri'],
      this.logger,
    );

    await this.debugFileSize(this.daeFile, 'dae');
  }

  private async cleanDae() {
    this.logger.debug('Cleaning DAE...');

    await cleanCollada(this.daeFile);
  }

  private createTextureRegExp(filePath: string): RegExp {
    const segments = filePath.replace(/\\+/g, '/').split('/');
    const fileName = segments.pop();

    // wrap each path segment into optional non-capturing group
    const regexpStr = segments.reduce(
      (acc, curr) => '(?:' + acc + escapeRegex(curr) + '\\/)?',
      '',
    );

    return new RegExp('^' + regexpStr + escapeRegex(fileName) + '$');
  }

  private async extractAndReplaceTextures(): Promise<void> {
    for (const texture of this.textureFiles.values()) {
      // look for texture in zip with possibly full path name
      let texResults = await parseZipEntries(
        this.zip,
        this.createTextureRegExp(texture.originalName),
      );
      if (!texResults[0]) {
        // if texture could not be found by the path name,
        // just look for file without path
        texResults = await parseZipEntries(
          this.zip,
          new RegExp(escapeRegex(basename(texture.originalName)) + '$'),
        );
      }

      if (texResults[0]) {
        // extract texture file
        const tmpFile = join(
          this.tmpDir.path,
          nanoid() + extname(texture.originalName),
        );
        await extractFileFromZip(this.zip, texResults[0].path, tmpFile);

        const image = sharp(tmpFile);

        // convert to png (will later be converted to webp via gltf-transform)
        texture.newName = parse(texture.newName).name + '.png';

        // resize textures that are otherwise too large
        // resize to the power of two
        const metadata = await image.metadata();
        await image
          .resize({
            width: Math.min(2048, MathUtils.ceilPowerOfTwo(metadata.width)),
            height: Math.min(2048, MathUtils.ceilPowerOfTwo(metadata.height)),
            fit: 'fill',
            withoutEnlargement: false,
          })
          .png()
          .toFile(join(this.tmpDir.path, texture.newName));
      }
    }

    // update references
    if (this.textureFiles.size) {
      await findAndReplaceLine(
        this.daeFile,
        /(<init_from>)(.+)(<\/init_from>)/i,
        (matches) => {
          // assimp (resp. Collada DAE) escapes special characters
          // need to transform back for key matching
          const texture = this.textureFiles.get(decodeURIComponent(matches[2]));
          return (
            matches[1] + (texture ? texture.newName : matches[2]) + matches[3]
          );
        },
      );
    }
  }

  /**
   * @deprecated
   */
  private async createMobileTextures() {
    for (const texture of [...this.textureFiles.values()]) {
      const mobileName = ResizeImageConfig.createFilename(
        texture.newName,
        'mobile',
      );
      const isJpeg = texture.newName.endsWith('.jpg');

      const image = sharp(join(this.tmpDir.path, texture.newName)).resize({
        width: ResizeImageConfig.sizes.mobile.width,
        height: ResizeImageConfig.sizes.mobile.height,
        fit: 'inside',
        withoutEnlargement: true,
      });

      if (isJpeg) {
        await image
          .jpeg({
            quality: ResizeImageConfig.sizes.mobile.quality,
            mozjpeg: true,
          })
          .toFile(join(this.tmpDir.path, mobileName));
      } else {
        await image.toFile(join(this.tmpDir.path, mobileName));
      }

      this.textureFiles.set(mobileName, {
        originalName: texture.newName,
        newName: mobileName,
      });
    }
  }

  /**
   * @deprecated
   */
  private async generateEdges(multiThread = true): Promise<void> {
    this.logger.debug('Generating edges...');

    await spawnAndLog(
      'node',
      [
        join(
          __dirname,
          multiThread ? 'generate-edges.js' : 'generate-edges-osm.js',
        ),
        '-f',
        this.daeFile,
        '-g',
        this.converterConfig.binCollada2Gltf,
      ],
      this.logger,
    );

    await this.debugFileSize(this.daeFile, 'dae with edges');
  }

  /**
   * Convert three.js scene to draco-compressed GLB buffer and save to `filePath`.
   */
  private async convertToDracoGlb(
    filePath: string,
    input: Object3D,
    resize?: [number, number],
  ): Promise<void> {
    const glbBuffer = await new GLTFExporter().parseAsync(input, {
      binary: true,
    });

    const gltfDocument = await this.gltfTransformIO.readBinary(glbBuffer);

    await gltfDocument.transform(
      weld(),
      prune({
        keepAttributes: true,
        keepExtras: true,
      }),
      draco(),
      textureCompress({
        encoder: sharp,
        targetFormat: 'webp',
        quality: ResizeImageConfig.sizes.texture.quality,
        resize,
      }),
    );

    const compressedBuffer = await this.gltfTransformIO.writeBinary(
      gltfDocument,
    );
    await fs.writeFile(filePath, compressedBuffer);
  }

  private async convertToGltf(): Promise<void> {
    this.logger.debug('Converting to GLTF...');

    let gltf = { scene: new Group() } as GLTF;
    gltf.scene.add(new Object3D());

    // if no dae file is set, then an empty glb file will be created
    if (this.daeFile !== '') {
      // convert dae to glb
      await spawnAndLog(
        this.converterConfig.binCollada2Gltf,
        ['-i', this.daeFile, '-o', this.gltfFile, '-b'],
        this.logger,
      );

      gltf = await loadGltf(this.gltfFile);
    }

    const glbBasename = parse(this.gltfFile).name;

    if (gltf.scene.children.length === 0) {
      throw new Error('No children');
    }

    // save without edges
    this.noEdgesGltfFile = join(this.tmpDir.path, glbBasename + '_ne.glb');
    await this.convertToDracoGlb(this.noEdgesGltfFile, gltf.scene);

    if (this.textureFiles.size) {
      this.noEdgesLowResGltfFile = join(
        this.tmpDir.path,
        glbBasename + '_nelr.glb',
      );
      await this.convertToDracoGlb(this.noEdgesLowResGltfFile, gltf.scene, [
        ResizeImageConfig.sizes.mobile.width,
        ResizeImageConfig.sizes.mobile.height,
      ]);
    }

    // generate edges
    this.logger.debug('Generating edges...');

    const meshes: Mesh[] = [];
    gltf.scene.traverse((child) => {
      if (child instanceof Mesh) {
        meshes.push(child);
      }
    });
    const lineMat = new LineBasicMaterial({ color: 0x333333 });
    lineMat.name = 'Uh4dEdgesMaterial';

    meshes.forEach((mesh) => {
      const edgesGeo = new EdgesGeometry(mesh.geometry, 24.0);
      const edges = new LineSegments(edgesGeo, lineMat);
      edges.name = mesh.name + '_edges';
      mesh.add(edges);
    });

    // convert and save "normal" glb file
    await this.convertToDracoGlb(this.gltfFile, gltf.scene);

    if (this.textureFiles.size) {
      this.lowResGltfFile = join(this.tmpDir.path, glbBasename + '_lr.glb');
      await this.convertToDracoGlb(this.lowResGltfFile, gltf.scene, [
        ResizeImageConfig.sizes.mobile.width,
        ResizeImageConfig.sizes.mobile.height,
      ]);
    }

    await this.debugFileSize(this.gltfFile, 'glb');
  }

  private async convertToGltfLegacy() {
    await this.convertTexturesToWebpLegacy();
    await spawnAndLog(
      this.converterConfig.binCollada2Gltf,
      ['-i', this.daeFile, '-o', this.gltfFile, '-b', '-d'],
      this.logger,
    );

    if (this.textureFiles.size) {
      this.lowResGltfFile = join(
        this.tmpDir.path,
        parse(this.gltfFile).name + '_lr.glb',
      );
      await this.convertTexturesToWebpLegacy([
        ResizeImageConfig.sizes.mobile.width,
        ResizeImageConfig.sizes.mobile.height,
      ]);
      await spawnAndLog(
        this.converterConfig.binCollada2Gltf,
        ['-i', this.daeFile, '-o', this.lowResGltfFile, '-b', '-d'],
        this.logger,
      );
    }

    await this.debugFileSize(this.gltfFile, 'glb');
  }

  private async convertTexturesToWebpLegacy(resize?: [number, number]) {
    for (const texture of this.textureFiles.values()) {
      const filePath = join(this.tmpDir.path, texture.newName);
      const buffer = await fs.readFile(filePath);
      const image = sharp(buffer);
      if (resize)
        image.resize({
          width: resize[0],
          height: resize[1],
          fit: 'fill',
        });
      await image
        .webp({
          quality: ResizeImageConfig.sizes.texture.quality,
        })
        .toFile(filePath);
    }
  }

  /**
   * @deprecated
   */
  private async uploadTextures(): Promise<void> {
    // copy textures to input folder
    for (const texture of this.textureFiles.values()) {
      await fs.copy(
        join(this.tmpDir.path, texture.newName),
        join(this.folder, texture.newName),
      );
    }
  }

  private async uploadGltf(): Promise<ConvertedFilesDto> {
    // copy files back to input folder and return basenames
    return {
      type: parse(this.extractedFile).ext.slice(1).toLowerCase(),
      file: await this.uploadToInputFolder(this.gltfFile),
      lowRes: await this.uploadToInputFolder(this.lowResGltfFile),
      noEdges: await this.uploadToInputFolder(this.noEdgesGltfFile),
      noEdgesLowRes: await this.uploadToInputFolder(this.noEdgesLowResGltfFile),
    };
  }

  /**
   * Copy file from tmp folder to input folder and return the basename of the file.
   */
  private async uploadToInputFolder(
    filePath?: string,
  ): Promise<string | undefined> {
    if (!filePath) return undefined;

    const fileBasename = basename(filePath);
    await fs.copy(filePath, join(this.folder, fileBasename));

    return fileBasename;
  }

  private dispose(): void {
    this.tmpDir
      .cleanup()
      .then()
      .catch((err) => {
        throw err;
      });
  }
}
