import * as process from 'process';
import { validate } from 'class-validator';
import { plainToInstance } from 'class-transformer';
import { BufferAttribute, Mesh, Object3D } from 'three';
import { loadGltf } from 'node-three-gltf';
import { OBJExporter } from 'three-addons/exporters/OBJExporter';
import { writeFile } from 'fs-extra';
import { Logger } from '@nestjs/common';
import { join } from 'path';
import { Coordinates } from '@uh4d/utils';
import { OSMGenerator } from '@uh4d/osm';
import { OsmGenerationPayloadDto } from '../dto/osm-generation-payload.dto';

const logger = new Logger('ChildProcess generate-obj-from-osm');

process.on('message', async (data) => {
  // start process with incoming message
  try {
    await generateObjFromOsm(data);
    process.exit();
  } catch (reason) {
    logger.error(reason);
    process.exit(1);
  }
});

async function generateObjFromOsm(payload: unknown) {
  // validate payload
  const data = plainToInstance(OsmGenerationPayloadDto, payload);
  const validationErrors = await validate(data);
  if (validationErrors.length) {
    throw new Error('Payload has wrong format');
  }

  // load model
  const origin = new Coordinates(data.lat, data.lon, 0);
  let terrainModel: Object3D;

  if (data.isCustomTerrain) {
    // load custom dgm model
    const gltf = await loadGltf(data.terrainPath);
    terrainModel = gltf.scene.children[0];
    terrainModel.position.copy(
      new Coordinates(data.terrainData.location).toCartesian(origin),
    );
    terrainModel.updateMatrixWorld(true);
  } else {
    // load cached elevation api terrain
    const gltf = await loadGltf(data.terrainPath);
    const terrain = gltf.scene.getObjectByName('TerrainNode') as Mesh;

    // transform geometry
    const attr = terrain.geometry.getAttribute('position') as BufferAttribute;
    for (let i = 0, l = attr.count; i < l; i++) {
      const vec = Coordinates.fromWebMercator(
        attr.getX(i),
        attr.getZ(i),
        attr.getY(i),
      ).toCartesian(origin);
      attr.setXYZ(i, vec.x, vec.y, vec.z);
    }
    terrain.geometry.computeBoundingBox();
    terrain.geometry.computeBoundingSphere();

    terrainModel = terrain;
  }

  // generate OSM buildings
  const osmGenerator = new OSMGenerator(origin, {
    logger: new Logger('OSMGenerator'),
  });

  await osmGenerator.generate(data.osmData);

  osmGenerator.update(terrainModel);
  osmGenerator.group.updateMatrixWorld(true);

  // export as OBJ file
  const exporter = new OBJExporter();
  const objText = exporter.parse(osmGenerator.group);

  const objFilename = 'tmp.obj';
  await writeFile(join(data.folder, objFilename), objText, {
    encoding: 'utf8',
  });

  process.send(objFilename);
}
