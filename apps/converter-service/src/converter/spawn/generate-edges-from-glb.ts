import * as process from 'process';
import { InvalidArgumentError, program } from 'commander';
import { BufferGeometry, EdgesGeometry, Mesh } from 'three';
import * as BufferGeometryUtils from 'three-addons/utils/BufferGeometryUtils';
import { loadGltf } from 'node-three-gltf';
import { writeFile } from 'fs-extra';
import { js2xml } from '@netless/xml-js';
import { geo2dae } from '../collada-utils/collada-helpers';

program
  .name('generate-edges-from-glb')
  .description('Generate edges from a single glb file')
  .requiredOption('-g, --geoid <string>')
  .requiredOption(
    '-i, --input <string>',
    'path to .glb input fileCOLLADA .dae file',
    (value) => {
      if (!/\.glb$/i.test(value))
        throw new InvalidArgumentError('File needs to be of type .glb.');
      return value;
    },
  )
  .requiredOption(
    '-o, --output <string>',
    'path to COLLADA geometry snippet output file',
  );
program.parse();

const opts = program.opts();

generateEdgesFromGlb(opts.geoid, opts.input, opts.output)
  .then(() => {
    process.exit();
  })
  .catch((reason) => {
    console.error(opts.geoid, reason);
    process.exit(1);
  });

async function generateEdgesFromGlb(
  edgesGeoId: string,
  inputFile: string,
  outputFile: string,
) {
  // load temp geometry model
  const gltf = await loadGltf(inputFile);

  // generate edges for geometry instance
  const geometries: BufferGeometry[] = [];
  gltf.scene.traverse((child) => {
    if (child instanceof Mesh) {
      // threshold angle of around 24 degree ensures that
      // edges will only be generated for "sharp" corners
      const edgesGeo = new EdgesGeometry(child.geometry, 24.0);
      geometries.push(edgesGeo);
    }
  });

  if (geometries.length === 0) return;

  let finalEdgesGeo = geometries[0];
  if (geometries.length > 1) {
    finalEdgesGeo = BufferGeometryUtils.mergeGeometries(geometries);
  }

  // write edges geometry as dae geometry instance to file
  const geoEdgesJs = { geometry: geo2dae(edgesGeoId, finalEdgesGeo) };

  await writeFile(outputFile, js2xml(geoEdgesJs, { compact: true, spaces: 2 }));
}
