import * as process from 'process';
import { InvalidArgumentError, program } from 'commander';
import { createReadStream, createWriteStream, rename } from 'fs-extra';
import { ElementCompact, js2xml, xml2js } from '@netless/xml-js';
import { Color, EdgesGeometry, Matrix4, Mesh } from 'three';
import { createStream } from 'byline';
import * as tmp from 'tmp-promise';
import { loadGltf } from 'node-three-gltf';
import { spawnAndLog } from '@uh4d/backend/utils';
import { geo2dae, getMaterialAsXml } from '../collada-utils/collada-helpers';

enum DaeState {
  None,
  Geometry,
  Scene,
}

program
  .name('generate-edges-osm')
  .description('Generate edges from geometry for OSM buildings')
  .requiredOption(
    '-f, --file <string>',
    'path to COLLADA .dae file',
    (value) => {
      if (!/\.dae$/i.test(value))
        throw new InvalidArgumentError('File needs to be of type .dae.');
      return value;
    },
  )
  .requiredOption(
    '-g, --gltf <string>',
    'path to COLLADA2GLTF converter binary',
    (value) => {
      if (value.length === 0) {
        throw new InvalidArgumentError(
          'Path to COLLADA2GLTF converter must be set.',
        );
      }
      return value;
    },
  );
program.parse();

const opts = program.opts();

tmp.setGracefulCleanup();

generateEdges(opts.file)
  .then(() => {
    process.exit();
  })
  .catch((reason) => {
    console.error(reason);
    process.exit(1);
  });

async function generateEdges(daeFile: string) {
  // convert to glb file
  const tmpGlb = await convertToGltf(daeFile);

  // load temp geometry model
  const gltf = await loadGltf(tmpGlb.path);

  // parse visual scene
  const { visualSceneXml } = await parseDaeFile(daeFile);
  const visualSceneRoot = xml2js(visualSceneXml, {
    compact: true,
  }) as ElementCompact;
  const visualScene = visualSceneRoot.visual_scene;

  const geoEdgesMap = new Map<
    string,
    { geoId: string; edgesGeoId?: string; edgesGeo?: EdgesGeometry }
  >();

  // parse node geometry ids
  traverseColladaScene(visualScene, (node, nodeId, geoInstances) => {
    if (!geoInstances) return;
    // iterate through any geometry instances of a node
    geoInstances.forEach((geo) => {
      const geoId = (geo._attributes.url as string).slice(1);
      geoEdgesMap.set(geoId, { geoId });
    });
  });

  // parse nodes again and add edges node
  traverseColladaScene(visualScene, (node, nodeId, geoInstances) => {
    if (!geoInstances) return;

    const geo = geoInstances[0];
    if (!geo) return;

    const geoId = (geo._attributes.url as string).slice(1);
    const geoObj = geoEdgesMap.get(geoId);
    if (!geoObj) return;

    const mesh = gltf.scene.getObjectByName(
      node._attributes.name.toString(),
    ) as Mesh;
    if (!mesh) return;

    geoObj.edgesGeoId = geoId + '_edges';
    geoObj.edgesGeo = new EdgesGeometry(mesh.geometry, 24.0);

    // add new node for edges as child
    if (!node.node) {
      node.node = [];
    } else if (!Array.isArray(node.node)) {
      node.node = [node.node];
    }

    node.node.push({
      _attributes: {
        id: nodeId + '_edges',
      },
      matrix: {
        _text: new Matrix4().toArray().join(' '),
      },
      instance_geometry: {
        _attributes: {
          url: '#' + geoObj.edgesGeoId,
        },
        bind_material: {
          technique_common: {
            instance_material: {
              _attributes: {
                symbol: 'Uh4dEdgesMaterial',
                target: '#Uh4dEdgesMaterial',
              },
            },
          },
        },
      },
    });
  });

  // add generated xml data to dae file
  await addGeneratedContentToDae(daeFile, visualScene, geoEdgesMap);
}

/**
 * Parse DAE file and extract/collect content relevant for generating edges.
 */
async function parseDaeFile(daeFile: string): Promise<{
  visualSceneXml: string;
}> {
  const readStream = createReadStream(daeFile, 'utf8');
  const lr = createStream(readStream, { keepEmptyLines: false });

  let state = DaeState.None;
  let visualSceneXml = '';

  lr.on('data', async (line) => {
    switch (state) {
      default:
        if (/<visual_scene.+>/.test(line)) {
          // collect visual scene lines
          state = DaeState.Scene;
          visualSceneXml = line;
        }
        break;

      case DaeState.Scene:
        // add visual scene line
        visualSceneXml += line;

        if (/<\/visual_scene>/.test(line)) {
          state = DaeState.None;
        }
        break;
    }
  });

  return new Promise((resolve, reject) => {
    lr.on('end', () => {
      resolve({
        visualSceneXml,
      });
    });

    lr.on('error', async (err) => {
      reject(err);
    });
  });
}

/**
 * Insert/replace new and updated content in DAE file.
 */
async function addGeneratedContentToDae(
  originalFile: string,
  visualScene: ElementCompact,
  edgesGeoMap: Map<
    string,
    { geoId: string; edgesGeoId?: string; edgesGeo?: EdgesGeometry }
  >,
): Promise<void> {
  const updatedFile = await tmp.file();
  const readStream = createReadStream(originalFile);
  const lr = createStream(readStream, { keepEmptyLines: false });
  const writeStream = createWriteStream(updatedFile.path);

  const { effect, material } = getMaterialAsXml(new Color(0x101010));

  let state = DaeState.None;
  let hasLibraryEffects = false;
  let hasLibraryMaterials = false;

  lr.on('data', async (line) => {
    switch (state) {
      default:
        if (/<\/library_geometries/.test(line)) {
          // add edges geometry instances
          lr.pause();
          for (const geoObj of edgesGeoMap.values()) {
            if (geoObj.edgesGeoId && geoObj.edgesGeo) {
              const geoEdgesJs = {
                geometry: geo2dae(geoObj.edgesGeoId, geoObj.edgesGeo),
              };
              writeStream.write(
                js2xml(geoEdgesJs, { compact: true, spaces: 2 }),
              );
            }
          }
          writeStream.write(line);
          lr.resume();
        } else if (/<\/library_materials/.test(line)) {
          // add edges material
          writeStream.write(material);
          writeStream.write(line);
          hasLibraryMaterials = true;
        } else if (/<\/library_effects/.test(line)) {
          // add edges material effect
          writeStream.write(effect);
          writeStream.write(line);
          hasLibraryEffects = true;
        } else if (/<visual_scene.+>/.test(line)) {
          // override visual scene
          state = DaeState.Scene;
        } else if (/<\/COLLADA>/.test(line)) {
          // last line
          // add materials/effects library if they were not present
          if (!hasLibraryEffects) {
            writeStream.write(
              '<library_effects>\n' + effect + '\n</library_effects>',
            );
          }
          if (!hasLibraryMaterials) {
            writeStream.write(
              '<library_materials>\n' + material + '\n</library_materials>',
            );
          }
          writeStream.write(line);
        } else {
          // just normal line
          writeStream.write(line);
        }
        break;

      case DaeState.Scene:
        if (/<\/visual_scene>/.test(line)) {
          // override visual scene
          state = DaeState.None;
          writeStream.write(
            js2xml({ visual_scene: visualScene }, { compact: true, spaces: 2 }),
          );
          writeStream.write(line);
        }
    }

    writeStream.write('\n');
  });

  return new Promise((resolve, reject) => {
    lr.on('end', () => {
      writeStream.end(async () => {
        try {
          await rename(updatedFile.path, originalFile);
          resolve();
        } catch (e) {
          reject(e);
        }
      });
    });

    lr.on('error', (err) => {
      writeStream.end(async () => {
        reject(err);
      });
    });
  });
}

/**
 * Convert temporary DAE geometry file to binary GLB file.
 */
async function convertToGltf(daeFile: string): Promise<tmp.FileResult> {
  const glbFile = await tmp.file({ prefix: 'glb_', postfix: '.glb' });

  await spawnAndLog(
    opts.gltf,
    ['-i', daeFile, '-o', glbFile.path, '-b', '-d'],
    console,
    'error',
  );

  return glbFile;
}

/**
 * Traverse through COLLADA scene nodes.
 */
function traverseColladaScene(
  parent: ElementCompact,
  callback: (
    node: ElementCompact,
    nodeId: string,
    geoInstances: ElementCompact[] | null,
  ) => void,
) {
  if (!parent.node) return;
  (Array.isArray(parent.node) ? parent.node : [parent.node]).forEach((node) => {
    const nodeId = node._attributes.id;
    const geoInstances: ElementCompact[] | null = node.instance_geometry
      ? Array.isArray(node.instance_geometry)
        ? node.instance_geometry
        : [node.instance_geometry]
      : null;
    callback(node, nodeId, geoInstances);
    traverseColladaScene(node, callback);
  });
}
