import { Module } from '@nestjs/common';
import { ConverterConfigModule } from '../converter-config/converter-config.module';
import { ConverterController } from './converter.controller';
import { ConverterService } from './converter.service';

@Module({
  imports: [ConverterConfigModule],
  controllers: [ConverterController],
  providers: [ConverterService],
})
export class ConverterModule {}
