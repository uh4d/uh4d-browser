import { BufferAttribute, BufferGeometry, Color } from 'three';
import { js2xml } from '@netless/xml-js';

/**
 * Generate COLLADA structure for geometry instance.
 */
export function geo2dae(id: string, bufferGeo: BufferGeometry) {
  const posAttr = bufferGeo.getAttribute('position') as BufferAttribute;

  const indexArray: number[] = [];
  for (let i = 0, l = posAttr.count; i < l; i++) {
    indexArray.push(i);
  }

  return {
    _attributes: {
      id: id,
    },
    mesh: {
      source: {
        _attributes: {
          id: id + '_positions',
        },
        float_array: {
          _attributes: {
            count: posAttr.array.length,
            id: id + '_positions_array',
          },
          _text: posAttr.array.join(' '),
        },
        technique_common: {
          accessor: {
            _attributes: {
              count: posAttr.count,
              source: '#' + id + '_positions_array',
              stride: posAttr.itemSize,
            },
          },
          param: [
            { _attributes: { name: 'X', type: 'float' } },
            { _attributes: { name: 'Y', type: 'float' } },
            { _attributes: { name: 'Z', type: 'float' } },
          ],
        },
      },
      vertices: {
        _attributes: {
          id: id + '_vertices',
        },
        input: {
          _attributes: {
            semantic: 'POSITION',
            source: '#' + id + '_positions',
          },
        },
      },
      lines: {
        _attributes: {
          material: 'IdovirEdgesMaterial',
          count: posAttr.count / 2,
        },
        input: {
          _attributes: {
            offset: 0,
            semantic: 'VERTEX',
            source: '#' + id + '_vertices',
          },
        },
        p: {
          _text: indexArray.join(' '),
        },
      },
    },
  };
}

/**
 * Get color material and effect.
 */
export function getMaterialAsXml(color: Color, matId = 'IdovirEdgesMaterial') {
  const effectId = matId + '-fx';

  const effect = {
    _attributes: {
      id: effectId,
      name: matId,
    },
    profile_COMMON: {
      technique: {
        _attributes: {
          sid: 'standard',
        },
        phong: {
          diffuse: {
            color: {
              _attributes: {
                sid: 'diffuse',
              },
              _text: `${color.r} ${color.g} ${color.b} 1`,
            },
          },
        },
      },
    },
  };

  const material = {
    _attributes: {
      id: matId,
      name: matId,
    },
    instance_effect: {
      _attributes: {
        url: '#' + effectId,
      },
    },
  };

  return {
    effect: js2xml({ effect }, { compact: true, spaces: 2 }),
    material: js2xml({ material }, { compact: true, spaces: 2 }),
  };
}
