import { createReadStream, createWriteStream, remove, rename } from 'fs-extra';
import { createStream } from 'byline';

enum DaeState {
  None,
  Node,
  LibraryCameras,
}

/**
 * Clean Collada DAE file by stripping off unnecessary/problematic content, e.g. cameras.
 */
export async function cleanCollada(filePath: string): Promise<void> {
  const tempFile = filePath + '.tmp';
  const readStream = createReadStream(filePath, 'utf8');
  const writeStream = createWriteStream(tempFile, 'utf8');
  const lr = createStream(readStream, { keepEmptyLines: false });

  let state = DaeState.None;
  let nodeXml = '';
  let skipNode = false;

  lr.on('data', async (line) => {
    switch (state) {
      default:
        if (/<node.+>/.test(line)) {
          state = DaeState.Node;
          skipNode = false;
          nodeXml = line + '\n';
        } else if (/<library_cameras>/.test(line)) {
          state = DaeState.LibraryCameras;
        } else {
          // just copy lines
          writeStream.write(line + '\n');
        }
        break;

      case DaeState.Node:
        nodeXml += line + '\n';
        if (/<instance_camera.+>/.test(line)) {
          skipNode = true;
        }
        if (/<\/node>/.test(line)) {
          state = DaeState.None;
          if (!skipNode) {
            writeStream.write(nodeXml);
          }
        }
        break;

      case DaeState.LibraryCameras:
        // don't copy any lines
        if (/<\/library_cameras>/.test(line)) {
          state = DaeState.None;
        }
        break;
    }
  });

  return new Promise((resolve, reject) => {
    lr.on('error', (err) => {
      writeStream.end(async () => {
        await remove(tempFile);
        reject(err);
      });
    });

    writeStream.on('error', async (err) => {
      readStream.destroy();
      await remove(tempFile);
      reject(err);
    });

    lr.on('end', () => {
      writeStream.end(async () => {
        try {
          await remove(filePath);
          await rename(tempFile, filePath);
          resolve();
        } catch (e) {
          reject(e);
        }
      });
    });
  });
}
