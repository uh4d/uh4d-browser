export const colladaFileStart =
  () => `<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<COLLADA xmlns="http://www.collada.org/2005/11/COLLADASchema" version="1.4.1">
  <asset>
    <unit name="meter" meter="1" />
    <up_axis>Y_UP</up_axis>
  </asset>
  <library_geometries>
`;

export const colladaFileEnd = (geoId: string) => `
  </library_geometries>
  <library_visual_scenes>
    <visual_scene id="RootNode" name="RootNode">
      <node id="temp_geo" name="temp_geo" type="NODE">
        <matrix sid="matrix">1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1</matrix>
        <instance_geometry url="#${geoId}"></instance_geometry>
      </node>
    </visual_scene>
  </library_visual_scenes>
  <scene>
    <instance_visual_scene url="#RootNode" />
  </scene>
</COLLADA>
`;
