import {
  IsArray,
  IsNotEmpty,
  IsNumber,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { IntersectionsPayload } from '@uh4d/dto/interfaces/custom/osm';
import { LocationDto, OriginDto } from '@uh4d/dto/interfaces';

class OriginPayload implements OriginDto {
  @IsNotEmpty()
  @IsNumber()
  latitude: number;
  @IsNotEmpty()
  @IsNumber()
  longitude: number;
  @IsNotEmpty()
  @IsNumber()
  altitude: number;
  @IsNotEmpty()
  @IsNumber()
  omega: number;
  @IsNotEmpty()
  @IsNumber()
  phi: number;
  @IsNotEmpty()
  @IsNumber()
  kappa: number;
  @IsNotEmpty()
  @IsArray()
  @IsNumber({}, { each: true })
  scale: number[];
}

class LocationPayload implements LocationDto {
  @IsNotEmpty()
  @IsNumber()
  latitude: number;
  @IsNotEmpty()
  @IsNumber()
  longitude: number;
  @IsNotEmpty()
  @IsNumber()
  altitude: number;
}

export class IntersectionsPayloadDto implements IntersectionsPayload {
  @IsNotEmpty()
  @IsString()
  modelPath: string;
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => OriginPayload)
  origin: OriginPayload;
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => LocationPayload)
  location: LocationPayload;
}
