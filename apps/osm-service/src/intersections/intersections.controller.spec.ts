import { Test, TestingModule } from '@nestjs/testing';
import { IntersectionsController } from './intersections.controller';

describe('IntersectionsController', () => {
  let controller: IntersectionsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [IntersectionsController],
    }).compile();

    controller = module.get<IntersectionsController>(IntersectionsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
