import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { join } from 'path';
import { fork } from 'child_process';
import { IntersectionsPayload } from '@uh4d/dto/interfaces/custom/osm';
import { OverpassApiService } from '@uh4d/backend/overpass-api';

type IntersectionChildMessage =
  | { type: 'radius'; radius: number }
  | { type: 'result'; result: number[] };

@Controller('intersections')
export class IntersectionsController {
  constructor(private readonly overpassApiService: OverpassApiService) {}

  @MessagePattern({ context: 'intersections', action: 'compute' })
  computeIntersections(
    @Payload() data: IntersectionsPayload,
  ): Promise<number[]> {
    const childProcess = fork(join(__dirname, 'compute-intersections.js'), [], {
      execPath: 'node',
      execArgv: [],
    });

    // send payload
    childProcess.on('spawn', () => {
      childProcess.send({ type: 'init', data });
    });

    return new Promise<number[]>((resolve, reject) => {
      childProcess.on('message', async (message: IntersectionChildMessage) => {
        switch (message.type) {
          case 'radius':
            try {
              const overpassData = await this.overpassApiService.query(
                data.location.latitude,
                data.location.longitude,
                message.radius,
              );
              childProcess.send({ type: 'overpass', data: overpassData });
            } catch (reason) {
              childProcess.kill();
              reject(reason);
            }
            break;

          case 'result':
            resolve(message.result);
            break;

          default:
            childProcess.kill();
            reject('Unknown child message');
        }
      });

      childProcess.on('close', (code) => {
        if (code) {
          reject(`childProcess exited with code ${code}`);
        }
      });
      childProcess.on('error', reject);
    });
  }
}
