import { Module } from '@nestjs/common';
import { OverpassApiModule } from '@uh4d/backend/overpass-api';
import { IntersectionsController } from './intersections.controller';

@Module({
  imports: [OverpassApiModule],
  controllers: [IntersectionsController],
})
export class IntersectionsModule {}
