import * as process from 'process';
import { Logger } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { loadGltf } from 'node-three-gltf';
import {
  BufferAttribute,
  BufferGeometry,
  Mesh,
  MeshBasicMaterial,
  Raycaster,
  Vector3,
} from 'three';
import * as BufferGeometryUtils from 'three-addons/utils/BufferGeometryUtils';
import { Coordinates } from '@uh4d/utils';
import { OverpassPayloadDto } from '@uh4d/backend/overpass-api';
import { IntersectionsPayloadDto } from '../dto/intersections-payload.dto';
import { processOsmData } from './osm-processing';

type IntersectionParentMessage = { type: 'init' | 'overpass'; data: unknown };

const logger = new Logger('ChildProcess compute-intersections');

let mesh: Mesh<BufferGeometry, MeshBasicMaterial>;
let intersectionsPayload: IntersectionsPayloadDto;

process.on('message', async (message: IntersectionParentMessage) => {
  // start process step with incoming message
  try {
    switch (message.type) {
      case 'init':
        await loadObject(message.data);
        process.send({
          type: 'radius',
          radius: mesh.geometry.boundingSphere.radius,
        });
        break;

      case 'overpass': {
        const osmIds = await computeIntersections(message.data);
        process.send({ type: 'result', result: osmIds });
        return process.exit();
      }

      default:
        logger.error('Unknown parent message');
        process.exit(2);
    }
  } catch (reason) {
    logger.error(reason);
    process.exit(1);
  }
});

async function loadObject(payload: unknown) {
  // validate payload
  const data = plainToInstance(IntersectionsPayloadDto, payload);
  const validationErrors = await validate(data);
  if (validationErrors.length) {
    throw new Error('Payload has wrong format');
  }

  intersectionsPayload = data;

  // load gltf file
  const gltf = await loadGltf(data.modelPath);

  // merge geometries
  const geometries: BufferGeometry[] = [];
  gltf.scene.traverse((child) => {
    if (child instanceof Mesh) {
      child.geometry.deleteAttribute('normal');
      child.geometry.deleteAttribute('uv');
      child.geometry.deleteAttribute('uv2');
      geometries.push(child.geometry);
    }
  });

  const geo = BufferGeometryUtils.mergeGeometries(geometries);
  mesh = new Mesh(geo, new MeshBasicMaterial());
  mesh.rotation.set(
    data.origin.omega,
    data.origin.phi,
    data.origin.kappa,
    'YXZ',
  );
  if (data.origin.scale) {
    if (Array.isArray(data.origin.scale)) {
      mesh.scale.fromArray(data.origin.scale);
    } else {
      mesh.scale.setScalar(data.origin.scale);
    }
  }
  mesh.updateMatrixWorld(true);
  geo.computeBoundingSphere();
}

async function computeIntersections(payload: unknown): Promise<number[]> {
  // validate payload
  const data = plainToInstance(OverpassPayloadDto, payload);
  const validationErrors = await validate(data);
  if (validationErrors.length) {
    throw new Error('Payload has wrong format');
  }

  if (!mesh) throw new Error('Mesh not available');
  if (!intersectionsPayload) throw new Error('Payload not available');

  const origin = new Coordinates(
    intersectionsPayload.origin.latitude,
    intersectionsPayload.origin.longitude,
    intersectionsPayload.origin.altitude,
  );

  // generate OSM buildings
  const buildings = processOsmData(data, origin);

  // test object and OSM buildings against each other
  const osmIds: number[] = [];

  for (const building of buildings) {
    // test intersection
    if (testIntersection(building, mesh) || testIntersection(mesh, building)) {
      if (!osmIds.includes(building.userData.osmId)) {
        osmIds.push(building.userData.osmId);
      }
    }
  }

  return osmIds;
}

/**
 * Test if two meshes intersect.
 */
export function testIntersection(mesh1: Mesh, mesh2: Mesh): boolean {
  const posAttr = mesh1.geometry.getAttribute('position') as BufferAttribute;
  const raycaster = new Raycaster();

  for (let i = 0; i < posAttr.count; i++) {
    const pos = mesh1.localToWorld(
      new Vector3(posAttr.getX(i), 10000, posAttr.getZ(i)),
    );
    raycaster.set(pos, new Vector3(0, -1, 0));
    const intersections = raycaster.intersectObject(mesh2, true);

    if (intersections[0]) {
      return true;
    }
  }

  return false;
}
