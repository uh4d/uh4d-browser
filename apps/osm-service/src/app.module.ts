import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AltitudesModule } from './altitudes/altitudes.module';
import { IntersectionsModule } from './intersections/intersections.module';

@Module({
  imports: [ConfigModule.forRoot(), AltitudesModule, IntersectionsModule],
})
export class AppModule {}
