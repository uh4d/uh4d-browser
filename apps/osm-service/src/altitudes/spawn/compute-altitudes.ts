import { program } from 'commander';
import { Logger } from '@nestjs/common';
import * as process from 'process';
import { validate } from 'class-validator';
import { plainToInstance } from 'class-transformer';
import { BufferAttribute, Mesh, Object3D } from 'three';
import { loadGltf } from 'node-three-gltf';
import { readJson } from 'fs-extra';
import { Coordinates } from '@uh4d/utils';
import { OSMGenerator } from '@uh4d/osm';
import { AltitudesPayloadDto } from '../dto/altitudes-payload.dto';

program
  .name('compute-altitudes')
  .description('Load terrain and compute altitudes of OSM data.');
program.parse();

const logger = new Logger('ChildProcess compute-altitudes');

process.on('message', async (data) => {
  // start process with incoming message
  try {
    await computeAltitudes(data);
    process.exit();
  } catch (reason) {
    logger.error(reason);
    process.exit(1);
  }
});

async function computeAltitudes(payload: unknown) {
  // validate payload
  const data = plainToInstance(AltitudesPayloadDto, payload);
  const validationErrors = await validate(data);
  if (validationErrors.length) {
    throw new Error('Payload has wrong format');
  }

  // load model
  const origin = new Coordinates(data.lat, data.lon, 0);
  let terrainModel: Object3D;

  if (data.isCustomTerrain) {
    // load custom dgm model
    const gltf = await loadGltf(data.modelPath);
    terrainModel = gltf.scene.children[0];
    terrainModel.position.copy(
      new Coordinates(data.terrainData.location).toCartesian(origin),
    );
    terrainModel.updateMatrixWorld(true);
  } else {
    // load cached elevation api terrain
    const gltf = await loadGltf(data.modelPath);
    const terrain = gltf.scene.getObjectByName('TerrainNode') as Mesh;

    // transform geometry
    const attr = terrain.geometry.getAttribute('position') as BufferAttribute;
    for (let i = 0, l = attr.count; i < l; i++) {
      const vec = Coordinates.fromWebMercator(
        attr.getX(i),
        attr.getZ(i),
        attr.getY(i),
      ).toCartesian(origin);
      attr.setXYZ(i, vec.x, vec.y, vec.z);
    }
    terrain.geometry.computeBoundingBox();
    terrain.geometry.computeBoundingSphere();

    terrainModel = terrain;
  }

  // generate OSM buildings
  const osmData = await readJson(data.osmPath);
  const osmGenerator = new OSMGenerator(origin, {
    logger: new Logger('OSMGenerator'),
  });

  await osmGenerator.generate(osmData);

  osmGenerator.update(terrainModel);

  // send altitudes to parent process
  osmGenerator.buildings.forEach((building) => {
    process.send({
      type: 'data',
      data: { [building.userData.osmId]: building.position.y },
    });
  });

  // send notification to parent process that all messages have been sent
  process.send({ type: 'finish' });

  // small delay to ensure that all messages have been sent
  await new Promise((resolve) => {
    setTimeout(resolve, 500);
  });
}
