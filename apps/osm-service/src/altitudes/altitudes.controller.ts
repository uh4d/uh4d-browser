import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { join } from 'path';
import { fork } from 'child_process';
import { AltitudesPayloadDto } from './dto/altitudes-payload.dto';

type AltitudesChildMessage =
  | { type: 'data'; data: { [osmId: string]: number } }
  | { type: 'finish' };

@Controller('altitudes')
export class AltitudesController {
  @MessagePattern({ context: 'altitudes', action: 'compute' })
  async computeAltitudes(
    @Payload() data: AltitudesPayloadDto,
  ): Promise<Record<string, number>> {
    const childProcess = fork(join(__dirname, 'compute-altitudes.js'), [], {
      execPath: 'node',
      execArgv: [],
    });

    // send payload
    childProcess.on('spawn', () => {
      childProcess.send(data);
    });

    return new Promise<Record<string, number>>((resolve, reject) => {
      const altitudes: Record<string, number> = {};

      childProcess.on('message', (message: AltitudesChildMessage) => {
        switch (message.type) {
          case 'data':
            // collect altitude data
            try {
              Object.assign(altitudes, message.data);
            } catch (reason) {
              childProcess.kill();
              reject(reason);
            }
            break;

          case 'finish':
            resolve(altitudes);
            break;

          default:
            childProcess.kill();
            reject('Unknown child message');
        }
      });

      childProcess.on('close', (code) => {
        if (code) {
          reject(`childProcess exited with code ${code}`);
        }
      });
      childProcess.on('error', reject);
    });
  }
}
