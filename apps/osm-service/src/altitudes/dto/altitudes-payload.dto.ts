import {
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { AltitudesPayload } from '@uh4d/dto/interfaces/custom/osm';

export class TerrainLocationPayload {
  @IsNotEmpty()
  @IsNumber()
  latitude: number;
  @IsNotEmpty()
  @IsNumber()
  longitude: number;
  @IsNotEmpty()
  @IsNumber()
  altitude: number;
  west: number;
  east: number;
  north: number;
  south: number;
}

export class TerrainFilePayload {
  @IsNotEmpty()
  @IsString()
  path: string;
  @IsNotEmpty()
  @IsString()
  file: string;
  @IsNotEmpty()
  @IsString()
  type: string;
}

export class TerrainPayload {
  id: string;
  @IsNotEmpty()
  @IsString()
  name: string;
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => TerrainLocationPayload)
  location: TerrainLocationPayload;
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => TerrainFilePayload)
  file: TerrainFilePayload;
}

export class AltitudesPayloadDto implements AltitudesPayload {
  @IsNotEmpty()
  @IsNumber()
  lat: number;
  @IsNotEmpty()
  @IsNumber()
  lon: number;
  @IsNotEmpty()
  @IsString()
  osmPath: string;
  @IsNotEmpty()
  @IsString()
  modelPath: string;
  @IsOptional()
  @ValidateNested()
  @Type(() => TerrainPayload)
  terrainData?: TerrainPayload;
  @IsOptional()
  @IsBoolean()
  isCustomTerrain?: boolean;
}
