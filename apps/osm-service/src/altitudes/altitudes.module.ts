import { Module } from '@nestjs/common';
import { AltitudesController } from './altitudes.controller';

@Module({
  controllers: [AltitudesController],
})
export class AltitudesModule {}
