import { Test, TestingModule } from '@nestjs/testing';
import { AltitudesController } from './altitudes.controller';

describe('AltitudesController', () => {
  let controller: AltitudesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AltitudesController],
    }).compile();

    controller = module.get<AltitudesController>(AltitudesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
